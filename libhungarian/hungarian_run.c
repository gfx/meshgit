/********************************************************************
 ********************************************************************
 **
 ** libhungarian by Cyrill Stachniss, 2004
 **
 **
 ** Solving the Minimum Assignment Problem using the 
 ** Hungarian Method.
 **
 ** ** This file may be freely copied and distributed! **
 **
 ** Parts of the used code was originally provided by the 
 ** "Stanford GraphGase", but I made changes to this code.
 ** As asked by  the copyright node of the "Stanford GraphGase", 
 ** I hereby proclaim that this file are *NOT* part of the
 ** "Stanford GraphGase" distrubition!
 **
 ** This file is distributed in the hope that it will be useful,
 ** but WITHOUT ANY WARRANTY; without even the implied 
 ** warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR
 ** PURPOSE.  
 **
 ********************************************************************
 ********************************************************************/


#include <stdio.h>
#include <stdlib.h>
#include "hungarian.h"


int main() {

  hungarian_problem_t p;
  
  FILE *fp;
  int rn, rm, i, j, s;
  int **m;
  
    fp = fopen( "costmatrix.mtx", "rt" );
    fscanf( fp, "%d %d", &rn, &rm );
    m = (int**)calloc( rn, sizeof( int* ) );
    for( i = 0; i < rn; i++ ) {
        m[i] = (int*)calloc( rm, sizeof( int ) );
        for( j = 0; j < rm; j++ )
            fscanf( fp, "%d", &m[i][j] );
    }
    fclose(fp);

  /* initialize the gungarian_problem using the cost matrix*/
  int matrix_size = hungarian_init(&p, m , rn,rm, HUNGARIAN_MODE_MINIMIZE_COST) ;

  fprintf(stderr, "assignement matrix has a now a size %d rows and %d columns.\n",  matrix_size,matrix_size);

  /* solve the assignement problem */
  hungarian_solve(&p);

  fp = fopen( "assignment.ind", "wt" );
  s = rn < rm ? rm : rn;
  for( i = 0; i < s; i++ )
      for( j = 0; j < s; j++ )
          if( p.assignment[i][j] == 1 ) { fprintf( fp, "%d\n", j ); break; }
  fclose(fp);

  /* free used memory */
  hungarian_free(&p);
  free(m);

  return 0;
}


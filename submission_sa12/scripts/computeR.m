function R = computeR(g0,g1)
n = length(g0.V);
m = length(g1.V);

R = zeros(n+m,n*m);

k = 1;
for i = 1:n
    for j = 1:m
        R(i,k+j-1) = 1;
    end
    k = k + m;
end

k = 1;
for i = 1:n
    for j = 1:m
        R(i+n,k+(j-1)*n) = 1;
    end
    k = k + 1;
end

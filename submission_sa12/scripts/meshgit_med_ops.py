from meshgit_misc import *
from mesh import *
from vec3f import *

"""
this "module" contains functions that handle mesh edit operations
"""

# NOTE: assuming that mesh1 has been aligned to mesh0
def partition_parts( mesh0, mesh1 ):
    dsu_fp = {} # partitions set
    
    DebugWriter.start( 'partitioning parts' )
    
    mesh0.optimize()
    mesh1.optimize()
    
    su_v0 = set([ v.u_v for v in mesh0.lv ])
    su_f0 = set([ f.u_f for f in mesh0.lf ])
    su_v1 = set([ v.u_v for v in mesh1.lv ])
    su_f1 = set([ f.u_f for f in mesh1.lf ])
    
    su_f  = (su_f0 | su_f1)
    su_fd = (su_f0 - su_f1)
    su_fa = (su_f1 - su_f0)
    su_fu = (su_f0 & su_f1)
    su_fm = set( u_f for u_f in su_fu if len(set(mesh0.opt_get_f(u_f).lu_v) & set(mesh1.opt_get_f(u_f).lu_v)) != max(len(mesh0.opt_get_f(u_f).lu_v),len(mesh1.opt_get_f(u_f).lu_v)) )
    su_fu = su_fu - su_fm
    
    #print( su_fm )
    
    # build adjacency datastructure
    adj_e = {}
    adj_f = {}
    for f in mesh0.lf + mesh1.lf:
        if f.u_f not in adj_f: adj_f[f.u_f] = { 'e': set() }
        lluv = zip( f.lu_v, f.lu_v[1:] + [f.lu_v[0]] )
        lsuv = [ frozenset(luv) for luv in lluv ]
        
        if f.u_f in su_fm: tf = 'm'
        elif f.u_f in su_fd: tf = 'd'
        elif f.u_f in su_fa: tf = 'a'
        else: tf = 'u'
        
        for suv in lsuv:
            if suv not in adj_e: adj_e[suv] = { 'f': set(), 'a':0, 'd':0, 'm':0, 'u':0 }
            adj_e[suv]['f'].add(f.u_f)
            adj_e[suv][tf] = 1
            adj_f[f.u_f]['e'].add(suv)
    
    
    # find border to deleted faces
    su_f_untouched = set( su_fd )
    while su_f_untouched:
        su_fp,su_epb = set(),set()
        
        su_fpdn = set([su_f_untouched.pop()])
        while su_fpdn:
            u_f = su_fpdn.pop()
            su_fp.add(u_f)
            for su_v in adj_f[u_f]['e']:
                e = adj_e[su_v]
                if e['a'] or e['u']:
                    su_epb.add(su_v)
                else:
                    for u_f in e['f']:
                        if u_f not in su_f_untouched: continue
                        su_fpdn.add(u_f)
                        su_f_untouched.remove(u_f)
        
        dsu_fp[ frozenset(su_epb) ] = su_fp
    
    # find border to added faces
    su_f_untouched = set( su_fa )
    while su_f_untouched:
        su_fp,su_epb = set(),set()
        
        su_fpdn = set([su_f_untouched.pop()])
        while su_fpdn:
            u_f = su_fpdn.pop()
            su_fp.add(u_f)
            for su_v in adj_f[u_f]['e']:
                e = adj_e[su_v]
                if e['d'] or e['m']:
                    su_epb.add(su_v)
                else:
                    for u_f in e['f']:
                        if u_f not in su_f_untouched: continue
                        su_fpdn.add(u_f)
                        su_f_untouched.remove(u_f)
        
        # TODO: need to partition disjoint border sets
        
        # if edge-border has been seen before, combine!
        su_epb = frozenset(su_epb)
        if su_epb in dsu_fp: dsu_fp[ su_epb ] |= su_fp
        else: dsu_fp[ su_epb ] = su_fp
    
    DebugWriter.end()
    
    #print( str(dsu_fp) )
    return dsu_fp


########################################################################################################################
# diff commands
########################################################################################################################


# NOTE: assuming that mesh1 has been aligned to mesh0
def diff_commands( mesh0, mesh1, move_epsilon=0.000001 ):
    DebugWriter.start( 'diffing commands' )
    
    mesh0.optimize()
    mesh1.optimize()
    
    su_v0 = set( v.u_v for v in mesh0.lv )
    su_f0 = set( f.u_f for f in mesh0.lf )
    su_v1 = set( v.u_v for v in mesh1.lv )
    su_f1 = set( f.u_f for f in mesh1.lf )
    
    lf_fmod = [ (u_f,mesh0.opt_get_f(u_f).lu_v,mesh1.opt_get_f(u_f).lu_v) for u_f in (su_f0 & su_f1) ]
    lu_fmod = [ u_f for (u_f,luv0,luv1) in lf_fmod if len(set(luv0)&set(luv1)) != max(len(luv0),len(luv1)) ]
    lu_vmov = [ u_v for u_v in (su_v0 & su_v1) if Vertex.distance2( mesh0.opt_get_v(u_v), mesh1.opt_get_v(u_v) ) > move_epsilon ]
    
    cmd_del_f = [ 'delface %s'      % u_f                                       for u_f in (su_f0-su_f1) ]
    cmd_del_v = [ 'delvert %s'      % u_v                                       for u_v in (su_v0-su_v1) ]
    cmd_chg_v = [ 'move %s:[%s]'    % (u_v,','.join([str(c)                     for c in mesh1.opt_get_v(u_v).p])) for u_v in lu_vmov ]
    cmd_mod_f = [ 'modface %s:[%s]' % (u_f,','.join(mesh1.opt_get_f(u_f).lu_v)) for u_f in lu_fmod ]
    cmd_add_v = [ 'addvert %s:[%s]' % (u_v,','.join([str(c)                     for c in mesh1.opt_get_v(u_v).p])) for u_v in (su_v1-su_v0) ]
    cmd_add_f = [ 'addface %s:[%s]' % (u_f,','.join(mesh1.opt_get_f(u_f).lu_v)) for u_f in (su_f1-su_f0) ]
    
    DebugWriter.end()
    
    return cmd_del_f + cmd_del_v + cmd_chg_v + cmd_mod_f + cmd_add_v + cmd_add_f

# NOTE: assuming that mesh1 has been aligned to mesh0
def diff_commands_by_partition( mesh0, mesh1, move_epsilon=0.000001 ):
    
    DebugWriter.start( 'diffing commands by partition' )
    
    mesh0.optimize()
    mesh1.optimize()
    
    dp = partition_parts( mesh0, mesh1 )
    
    su_v0 = set([ v.u_v for v in mesh0.lv ])
    su_f0 = set([ f.u_f for f in mesh0.lf ])
    su_v1 = set([ v.u_v for v in mesh1.lv ])
    su_f1 = set([ f.u_f for f in mesh1.lf ])
    
    su_vd = su_v0 - su_v1
    su_va = su_v1 - su_v0
    su_vu = su_v0 & su_v1
    
    su_fd = su_f0 - su_f1
    su_fa = su_f1 - su_f0
    su_fu = su_f0 & su_f1
    
    vcmds = {
        'uvd': [ 'delvert %s' % u_v for u_v in su_vd ],
        'uva': [ 'addvert %s:[%s]' % (u_v,','.join([str(c) for c in mesh1.opt_get_v(u_v).p])) for u_v in su_va ],
        'uvm': [ 'move %s:[%s]' % (u_v,','.join([str(c) for c in mesh1.opt_get_v(u_v).p])) for u_v in su_vu if Vertex.distance2(mesh0.opt_get_v(u_v),mesh1.opt_get_v(u_v)) > move_epsilon ],
        }
    
    fcmds = []
    for sfp in dp.values():
        fcmds.append( {
            'sf': sfp,
            'ufd': [ 'delface %s' % u_f for u_f in sfp if u_f in su_fd ],
            'ufa': [ 'addface %s:[%s]' % (u_f,','.join(mesh1.opt_get_f(u_f).lu_v)) for u_f in sfp if u_f in su_fa ],
            'ufm': [ 'modface %s:[%s]' % (u_f,','.join(mesh1.opt_get_f(u_f).lu_v)) for u_f in sfp if u_f in su_f0 and u_f in su_f1 and len(set(mesh0.opt_get_f(u_f).lu_v) & set(mesh1.opt_get_f(u_f).lu_v)) != max(len(mesh0.opt_get_f(u_f).lu_v),len(mesh1.opt_get_f(u_f).lu_v)) ]
            } )
    
    DebugWriter.end()
    
    return (vcmds,fcmds)


########################################################################################################################
# perform commands on mesh
########################################################################################################################


def do_commands( mesh, cmds ):
    DebugWriter.start( 'applying commands' )
    
    for cmd in cmds:
        _,op,uid,_,csv,_ = re.split( '^([a-zA-Z]+) ([^:]+)(:\[(.+)\])?$', cmd )
        if csv: csv = csv.split(',')
        
        if op == 'delface':
            mesh.del_face( uid )
        elif op == 'delvert':
            mesh.del_vert( uid )
        elif op == 'move':
            mesh.get_v( uid ).p = [float(v) for v in csv]
        elif op == 'modface':
            su_v = set(mesh.get_lu_v())
            assert all( u_v in su_v for u_v in csv )
            mesh.lf[uid].lu_v = csv
        elif op == 'addvert':
            mesh.add_vert( u_v=uid, p=[float(v) for v in csv] )
        elif op == 'addface':
            su_v = set(mesh.get_lu_v())
            assert all( u_v in su_v for u_v in csv )
            mesh.add_face( u_f=uid, lu_v=csv )
        else:
            assert False, 'unknown command: "%s"' % cmd
    
    try: mesh.validate()
    except:
        DebugWriter.printer( 'mesh did not validate!!' )
        print( cmds )
        exit()
    
    DebugWriter.end()
    
    return mesh



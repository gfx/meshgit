import sys, subprocess, os, scipy.io, itertools

from debugwriter import DebugWriter
from mesh import *
from graph import *

from correspondencebuilder_logic import CorrespondenceBuilder_Logic as CB_Logic
from correspondencebuilder_hungarian import CorrespondenceBuilder_Hungarian as CB_Hungarian

"""
this is a py implementation of the paper
    Approximate Topological Matching of Quadrilateral Meshes
    David Eppstein and Michael T. Goodrich and Ethan Kim and Rasmus Tamstorf
    IEEE Visual Computer 2009, Shape Modeling International 2008
    
    http://www.ics.uci.edu/~goodrich/pubs/approx-match.pdf
    http://www.disneyanimation.com/library/approxmatch_smi_2008.pdf
    http://www.disneyanimation.com/library/approxmatch_journal.pdf

steps:
    1. find starting anchor points:
        1. skeletonize meshes
        2. color skeleton graph
        3. find anchor points as similarly colored nodes
    2. lazy-greedy:
"""


class CorrespondenceBuilder_Disney(object):
    
    @staticmethod
    def build( opts ):
        ci = opts['ci']
        
        costs = opts['costs']
        
        CB_Disney = CorrespondenceBuilder_Disney
        
        CB_Disney.find_anchors( opts )
        bci,bcost = None,float('Inf')
        while True:
            CB_Disney.seed( opts )
            CB_Disney.lazygreedy( opts )
            
            cost = ci.compute_cost_old( opts )
            
            DebugWriter.report( 'len', len(ci.mf01) )
            DebugWriter.report( 'cost', cost )
            
            if cost < bcost:
                DebugWriter.printer( 'new best' )
                bcost = cost
                bci = ci.clone()
            
            if opts['seedinfo']['last']: break
        
        opts['ci'].copyvals( bci )
        
        #CB_Hungarian.build_vertex_correspondences_hungarian_faces( opts )
        CB_Logic.build_vertex_correspondences_faces( opts )
        CB_Logic.remove_vert_matching_all_unmatched_faces( opts )
        
        DebugWriter.printer( 'using len %d' % len(bci.mf01) )
    
    @staticmethod
    def find_anchors( opts ):
        DebugWriter.start( 'finding anchors' )
        
        ci = opts['ci']
        m0,m1 = ci.m0,ci.m1
        sm0,sm1 = m0.skeletonize(), m1.skeletonize()
        
        # get mesh statistics (adjacency info)
        stats0,stats1 = sm0.get_stats(),sm1.get_stats()
        d_adjv0,d_adjv1 = stats0['adj_v'],stats1['adj_v']
        
        # initialize "coloring" by labeling each vertex with its degree
        lcolor0 = [ [len(d_adjv0[v.u_v]['su_nv'])] for v in sm0.lv ]
        lcolor1 = [ [len(d_adjv1[v.u_v]['su_nv'])] for v in sm1.lv ]
        
        k = 0
        while True:
            lcolor0 = [ lcolor0[i_v] + [ c for u_nv in d_adjv0[v.u_v]['su_nv'] for c in lcolor0[sm0._opt_get_i_v(u_nv)] ] for i_v,v in sm0.elv() ]
            lcolor1 = [ lcolor1[i_v] + [ c for u_nv in d_adjv1[v.u_v]['su_nv'] for c in lcolor1[sm1._opt_get_i_v(u_nv)] ] for i_v,v in sm1.elv() ]
            
            lcolor0 = [ sorted(c0) for c0 in lcolor0 ]
            lcolor1 = [ sorted(c1) for c1 in lcolor1 ]
            
            # breaking condition:
            #     break if "at least one pair of uniquely labeled vertices are found, one from each mesh"
            # interpreted as?:
            # --> 1. if a unique label in lcolor0 that's also unique to lcolor1
            #     2. if there is 1+ unique labels in lcolor0 and 1+ unique labels in lcolor1
            
            # get ulabels and usage count
            duc0,duc1 = unique_count_dict( map(tuple,lcolor0) ),unique_count_dict( map(tuple,lcolor1) )
            suc0,suc1 = set(duc0.keys()),set(duc1.keys())
            suc01 = suc0 & suc1                                                                         # ulabel in both
            suc01u = set( uc01 for uc01 in suc01 if duc0[uc01] == 1 and duc1[uc01] == 1 )               # ulabels that are unique (only 1 in each mesh)
            
            # make sure anchor points have same degree
            suc01u = set( itertools.ifilter(
                lambda uc01u:
                    len(d_adjv0[sm0.lv[lcolor0.index(list(uc01u))].u_v]['su_af']) == len(d_adjv1[sm1.lv[lcolor1.index(list(uc01u))].u_v]['su_af']),
                suc01u
                ) )
            
            if len(suc01u) >= 1: break                          # breaking condition
            
            k += 1
            if k == 100: break                                  # uh-oh!
        
        DebugWriter.report( 'iterations', k+1 )
        assert k != 100, 'could not find anchor :('             # not certain what to do here otherwise!!
        
        # NOTE: suc01u is a set of tuples.  need to convert tuples to list to perform lst.index()
        opts['anchors'] = [ (
            sm0.lv[ lcolor0.index( list(c) ) ].u_v,             # use uid of vert, because m0 is skeleton mesh
            sm1.lv[ lcolor1.index( list(c) ) ].u_v,             # use uid of vert, because m1 is skeleton mesh
            ) for c in suc01u ]
        
        # print anchor information, converting labels to alphabet characters, grouping by character, and counting each group
        ls_colorhex = [ ( av0, av1, ''.join( ' ABCDEFGHIJKLMNOPQRSTUVWXYZ'[v] for v in lcolor0[sm0._opt_get_i_v(av0)] ) ) for av0,av1 in opts['anchors'] ]
        ls_colorhex = [ ( av0, av1, ' '.join([ '%sx%03d' % (c,colorhex.count(c)) for c in sorted(set(colorhex)) ]) ) for av0,av1,colorhex in ls_colorhex ]
        DebugWriter.report( 'anchor information', dict( ( '%s %s'%(av0,av1), colorhex ) for av0,av1,colorhex in ls_colorhex ) )
        #DebugWriter.report( 'anchor information', dict( (
            #'%s %s'%(av0,av1),
            #''.join( hex(v)[2:] for v in lcolor0[sm0._opt_get_i_v(av0)] ),
            #) for av0,av1 in opts['anchors'] ) )
        
        DebugWriter.end()
    
    @staticmethod
    def order_faces( mesh, su_af ):
        su_af = set(su_af)
        lu_af = [ su_af.pop() ]
        while su_af:
            u_f0 = lu_af[-1]
            f0 = mesh.opt_get_f( u_f0 )
            su_v0 = set( f0.lu_v )
            found = False
            for u_f1 in su_af:
                f1 = mesh.opt_get_f( u_f1 )
                su_v1 = set( f1.lu_v )
                if len(su_v0 & su_v1) == 2:
                    lu_af.append( u_f1 )
                    su_af.remove( u_f1 )
                    found = True
                    break
            if not found: return None
        return lu_af
    
    @staticmethod
    def seed( opts ):
        CB_Disney = CorrespondenceBuilder_Disney
        
        DebugWriter.start( 'seeding' )
        
        ci = opts['ci']
        anchors = opts[ 'anchors' ]
        m0,m1 = ci.m0,ci.m1
        
        if 'seedinfo' not in opts:
            # first run.  generate initial data
            DebugWriter.start( 'generating seeding lists' )
            
            stats0,stats1 = m0.get_stats(),m1.get_stats()
            d_adjv0,d_adjv1 = stats0['adj_v'],stats1['adj_v']
            
            # find an anchor pair that have the same number of adjacent faces
            anchor = None
            for anchor in anchors:
                u_av0,u_av1 = anchor
                if len(d_adjv0[u_av0]['su_af']) != len(d_adjv1[u_av1]['su_af']):
                    anchor = None
                    continue
                # impose an order on faces by finding faces that share two verts...  may be reverse order
                lu_af0 = CB_Disney.order_faces( m0, d_adjv0[u_av0]['su_af'] )
                lu_af1 = CB_Disney.order_faces( m1, d_adjv1[u_av1]['su_af'] )
                # make sure that there is an order to the adj faces
                if lu_af0 is None or lu_af1 is None:
                    anchor = None
                    continue
                break
            assert anchor is not None, 'did not find an anchor!'
            
            opts['seedinfo'] = {
                'last': False,
                'iteration': 0,
                'lu_af0': lu_af0,
                'lu_af1': lu_af1,
                'stats': ( stats0, stats1 ),
                }
            
            DebugWriter.end()
        
        else:
            
            seedinfo = opts['seedinfo']
            seedinfo['iteration'] += 1
            lu_af0,lu_af1 = seedinfo['lu_af0'],seedinfo['lu_af1']
            if seedinfo['iteration'] == len(lu_af1):
                lu_af1.reverse()
            else:
                lu_af1 = lu_af1[1:] + [lu_af1[0]]
            seedinfo['lu_af1'] = lu_af1
        
        ci.dcfall()
        for u_af0,u_af1 in zip(lu_af0,lu_af1):
            ci.acf( m0._opt_get_i_f( u_af0 ), m1._opt_get_i_f( u_af1 ), increaseorder=False )
        ci.norder += 1
        
        opts['seedinfo']['last'] = ( opts['seedinfo']['iteration'] == len(lu_af1) * 2 - 1 )
        
        ## for now, seed with all anchors
        #for v0,v1 in anchors:
        #    ci.acv( m0._opt_get_i_v(v0), m1._opt_get_i_v(v1) )
        
        DebugWriter.end()
    
    @staticmethod
    def lazygreedy( opts ):
        DebugWriter.start( 'propagating faces using lazy-greedy algorithm' )
        
        edge_adj_only = False
        
        str_eadj = 'su_afe' if edge_adj_only else 'su_afv'
        
        ci = opts['ci']
        m0,m1 = ci.m0,ci.m1
        stats0,stats1 = opts['seedinfo']['stats']
        adj_f0u,adj_f1u = stats0['adj_f'],stats1['adj_f']
        
        # translate to use indices
        adj_f0i = dict( (m0._opt_get_i_f(u_f),v) for u_f,v in adj_f0u.iteritems() )
        adj_f1i = dict( (m1._opt_get_i_f(u_f),v) for u_f,v in adj_f1u.iteritems() )
        
        # init S and S1: faces that have been matched
        su_S = set( m0.lf[i_f].u_f for i_f in ci.mf01.keys() )
        su_S1 = set( m1.lf[i_f].u_f for i_f in ci.mf10.keys() )
        
        # init S' and S1': subset of faces in S and S1 on the boundary of contiguous block (faces that have adj unmatched neighbors)
        # NOTE: if face is edge-adj, it's vert-adj, too
        #su_S_ = set( u_f for u_f in su_S if any( u_af not in su_S for u_af in adj_f0u[u_f]['su_afv'] ) )
        #su_S1_ = set( u_f for u_f in su_S1 if any( u_af not in su_S1 for u_af in adj_f1u[u_f]['su_afv'] ) )
        
        k = 0
        while True:
            
            # init S' and S1': subset of faces in S and S1 on the boundary of contiguous block (faces that have adj unmatched neighbors)
            # NOTE: if face is edge-adj, it's vert-adj, too
            su_S_ = set( u_f for u_f in su_S if any( u_af not in su_S for u_af in adj_f0u[u_f]['su_afv'] ) )
            su_S1_ = set( u_f for u_f in su_S1 if any( u_af not in su_S1 for u_af in adj_f1u[u_f]['su_afv'] ) )
            
            # init A and B: unmatched faces in m0 and m1 that are vert- or edge-adj to face in S' and \mu(S')
            su_A = set( u_fu for u_fm in su_S_ for u_fu in adj_f0u[u_fm][str_eadj] ) - su_S
            su_B = set( u_fu for u_fm in su_S1_ for u_fu in adj_f1u[u_fm][str_eadj] ) - su_S1
            
            # generate L: compatibility graph
            L = Graph_Undirected()
            
            # add nodes for potential matches
            for u_q in su_A:
                # find potential matches for q...
                
                # get matched faces in m0 that are adj to q, then get their matchings in m1
                lu_t = [ u_S_ for u_S_ in su_S_ if u_q in adj_f0u[u_S_]['su_afv'] ]     # list of matched faces adj to q
                lu_mut = [ m1.lf[ ci.mf01[m0._opt_get_i_f(u_t)] ].u_f for u_t in lu_t ] # list of matching faces to faces in lu_t
                
                # r is potential match for q if q is edge adj to t and r is edge adj to mu(t)
                #su_Mq_e = set( u_r for u_r in su_B if any(
                    #u_q in adj_f0u[u_t]['su_afe'] and u_r in adj_f1u[u_mut]['su_afe']
                    #for u_t,u_mut in zip(lu_t,lu_mut)
                    #) )
                su_Mq_e = set( u_r for u_r in su_B if
                    any(
                        u_q in adj_f0u[u_t]['su_afe'] for u_t in lu_t
                    ) and all (
                        u_r in adj_f1u[u_mut]['su_afe']
                        for u_t,u_mut in zip(lu_t,lu_mut) if u_q in adj_f0u[u_t]['su_afe']
                    ) )
                # r is potential match for q if q is vert adj to t and r is vert adj to mu(t)
                #su_Mq_v = set( u_r for u_r in su_B if any(
                    #u_q not in adj_f0u[u_t]['su_afe'] and u_r not in adj_f1u[u_mut]['su_afe'] and
                    #u_q in adj_f0u[u_t]['su_afv'] and u_r in adj_f1u[u_mut]['su_afv']
                    #for u_t,u_mut in zip(lu_t,lu_mut)
                    #) )
                su_Mq_v = set( u_r for u_r in su_B if
                    any(
                        u_q not in adj_f0u[u_t]['su_afe'] and u_q in adj_f0u[u_t]['su_afv'] for u_t in lu_t
                    ) and all(
                        u_r not in adj_f1u[u_mut]['su_afe'] and u_r in adj_f1u[u_mut]['su_afv']
                        for u_t,u_mut in zip(lu_t,lu_mut) if u_q not in adj_f0u[u_t]['su_afe'] and u_q in adj_f0u[u_t]['su_afv']
                    ) )
                
                # add potential matches for q to graph
                for u_Mq in su_Mq_e|su_Mq_v:
                    L.add_node( (u_q,u_Mq) )
                
            # add edges for compatible matches
            for u_qMq,u_sMs in product( L.nodes, repeat=2 ):
                u_q,u_Mq = u_qMq
                u_s,u_Ms = u_sMs
                if u_q == u_s or u_Mq == u_Ms: continue
                
                # 1. q and s must be f-e-f adj
                if u_s not in adj_f0u[u_q]['su_afe']: continue
                
                # 2. adding mappings q->mq and s->ms to mu must maintain topo consistency
                d_new_mu = dict( [ (u_q,u_Mq), (u_s,u_Ms) ] +
                    [ (u_S_,m1.lf[ ci.mf01[m0._opt_get_i_f(u_S_)] ].u_f)
                        for u_S_ in su_S_ & (adj_f0u[u_q]['su_afv'] | adj_f0u[u_s]['su_afv'])
                    ] )
                d_new_mu1 = dict( [ (u_Mq,u_q), (u_Ms,u_s) ] +
                    [ (u_S1_,m0.lf[ ci.mf10[m1._opt_get_i_f(u_S1_)] ].u_f)
                        for u_S1_ in su_S1_ & (adj_f1u[u_Mq]['su_afv'] | adj_f1u[u_Ms]['su_afv'])
                    ] )
                # get subset of m0 that contains q and s and all adjacent faces to q or s
                #su_m0 = set( [ u_q, u_s ] ) | (su_S_ & ( adj_f0u[u_q]['su_afv'] | adj_f0u[u_s]['su_afv'] ) )
                #su_m1 = set( [ u_Mq, u_Ms ] ) | (su_S1_ & ( adj_f1u[u_Mq]['su_afv'] | adj_f1u[u_Ms]['su_afv'] ) )
                # test for topo consistency
                if not all( d_new_mu[u_af0] in adj_f1u[u_Mq]['su_afe'] for u_af0 in (adj_f0u[u_q]['su_afe'] & su_S_) ): continue
                if not all( d_new_mu[u_af0] in adj_f1u[u_Ms]['su_afe'] for u_af0 in (adj_f0u[u_s]['su_afe'] & su_S_) ): continue
                if not all( d_new_mu[u_af0] in adj_f1u[u_Mq]['su_afv'] for u_af0 in (adj_f0u[u_q]['su_afv'] & su_S_) ): continue
                if not all( d_new_mu[u_af0] in adj_f1u[u_Ms]['su_afv'] for u_af0 in (adj_f0u[u_s]['su_afv'] & su_S_) ): continue
                if not all( d_new_mu1[u_af1] in adj_f0u[u_q]['su_afe'] for u_af1 in (adj_f1u[u_Mq]['su_afe'] & su_S1_) ): continue
                if not all( d_new_mu1[u_af1] in adj_f0u[u_s]['su_afe'] for u_af1 in (adj_f1u[u_Ms]['su_afe'] & su_S1_) ): continue
                if not all( d_new_mu1[u_af1] in adj_f0u[u_q]['su_afv'] for u_af1 in (adj_f1u[u_Mq]['su_afv'] & su_S1_) ): continue
                if not all( d_new_mu1[u_af1] in adj_f0u[u_s]['su_afv'] for u_af1 in (adj_f1u[u_Ms]['su_afv'] & su_S1_) ): continue
                
                L.add_edge( (u_q,u_Mq), (u_s,u_Ms) )
            
            #DebugWriter.report( 'graph', L.graph )
            
            slargest = L.get_largest_connected_component()
            #DebugWriter.report( 'slargest', slargest )
            
            su_q = set( u_q for u_q,_ in slargest )
            su_Mq = set( u_Mq for _,u_Mq in slargest )
            slargest = set( (q,Mq) for (q,Mq) in slargest if sum( 1 for _q,_Mq in slargest if _q==q or _Mq==Mq )==1 ) # remove double assignments
            
            if not slargest: break
            #if len(slargest) == 1: break
            
            for u_q,u_Mq in slargest:
                i_q = m0._opt_get_i_f(u_q)
                i_Mq = m1._opt_get_i_f(u_Mq)
                ci.acf( i_q, i_Mq, increaseorder=False )
                su_S.add( u_q )
                su_S1.add( u_Mq )
                #su_S_.add( u_q )
                #su_S1_.add( u_Mq )
            #su_S_ = set( u_f for u_f in su_S_ if any( u_af not in su_S for u_af in adj_f0u[u_f]['su_afv'] ) )
            #su_S1_ = set( u_f for u_f in su_S1_ if any( u_af not in su_S1 for u_af in adj_f1u[u_f]['su_afv'] ) )
            
            #su_S_ = set( itertools.ifilter( lambda u_f: any( u_af not in su_S for u_af in adj_f0u[u_f]['su_afv'] ), su_S_ ) )
            #su_S1_ = set( itertools.ifilter( lambda u_f: any( u_af not in su_S1 for u_af in adj_f1u[u_f]['su_afv'] ), su_S1_ ) )
            #su_S_ = set( u_f for u_f in su_S_ if len( adj_f0u[u_f]['su_afv'] - su_S ) )
            #su_S1_ = set( u_f for u_f in su_S1_ if len( adj_f1u[u_f]['su_afv'] - su_S1 ) )
            
            ci.norder += 1
            
            k += 1
            #if k == 25:
            #    break
        
        DebugWriter.report( 'iterations', k )
        
        DebugWriter.end()





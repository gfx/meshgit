# -*- coding: utf-8 -*-

import os, glob, sys, subprocess, time, re, itertools, struct
from array import array
try:    import json
except: import simplejson as json

################################################################################
### dict functions

def dict_defaults( d, defaults ):
    if d is None: return dict(defaults)
    d2 = dict(d)
    for k,v in defaults.iteritems(): d2.setdefault(k,v)
    return d2

def dict_set_defaults( d, defaults ):
    assert type(d) is dict
    for k,v in defaults.iteritems(): d.setdefault(k,v)
    return d

################################################################################
### iterable functions (list, Vector, tuple, (x)range)

def zip_self_offset( iterable ):
    for e0,e1 in itertools.izip( iterable[:-1], iterable[1:] ):
        yield ( e0, e1 )
    yield ( iterable[-1], iterable[0] )

def map_fn( fn, iterable ):
    for element in iterable:
        fn( element )

# http://rightfootin.blogspot.com/2006/09/more-on-python-flatten.html
def flatten(l, ltypes=(list, tuple)):
    """
    flattens nested lists to a single list :)
    """
    ltype = type(l)
    l = list(l)
    i = 0
    while i < len(l):
        while isinstance(l[i], ltypes):
            if not l[i]:
                l.pop(i)
                i -= 1
                break
            else:
                l[i:i + 1] = l[i]
        i += 1
    return ltype(l)

def string_to_vector( s ):
    sp = s.split(',')
    return (float(sp[0]),float(sp[1]),float(sp[2]))

# determines if two cyclic lists contain the same elements
def same_lists( l0, l1, reverse_check=False ):
    if len(l0) != len(l1): return False
    l = len(l0)
    for i0 in xrange(l):
        for i1 in xrange(l):
            for ia in xrange(l):
                i0c = (i0+ia)%l
                i1c = (i1+ia)%l
            if all( l0[(i0+ia)%l]==l1[(i1+ia)%l] for ia in xrange(l) ): return True
            if reverse_check and all( l0[(i0+ia)%l]==l1[(-(i1+ia))%l] for ia in xrange(l) ): return True
    return False

# generator for permuting a set
# NOTE: this function was added to itertools in python 2.6.  this is here so we can use py2.5
# copied from http://docs.python.org/library/itertools.html#itertools.permutations
def permutations( iterable, r=None ):
    # permutations('ABCD', 2) --> AB AC AD BA BC BD CA CB CD DA DB DC
    # permutations(range(3)) --> 012 021 102 120 201 210
    pool = tuple(iterable)
    n = len(pool)
    r = n if r is None else r
    if r > n:
        return
    indices = range(n)
    cycles = range(n, n-r, -1)
    yield tuple(pool[i] for i in indices[:r])
    while n:
        for i in reversed(range(r)):
            cycles[i] -= 1
            if cycles[i] == 0:
                indices[i:] = indices[i+1:] + indices[i:i+1]
                cycles[i] = n - i
            else:
                j = cycles[i]
                indices[i], indices[-j] = indices[-j], indices[i]
                yield tuple(pool[i] for i in indices[:r])
                break
        else:
            return

# generator for subset combinations
# NOTE: this function was added to itertools in python 2.6.  this is here so we can use py2.5
# copied from http://docs.python.org/library/itertools.html#itertools.combinations
def combinations(iterable, r):
    # combinations('ABCD', 2) --> AB AC AD BC BD CD
    # combinations(range(4), 3) --> 012 013 023 123
    pool = tuple(iterable)
    n = len(pool)
    if r > n:
        return
    indices = range(r)
    yield tuple(pool[i] for i in indices)
    while True:
        for i in reversed(range(r)):
            if indices[i] != i + n - r:
                break
        else:
            return
        indices[i] += 1
        for j in range(i+1, r):
            indices[j] = indices[j-1] + 1
        yield tuple(pool[i] for i in indices)

# generator for cartesian products
# NOTE: this function was added to itertools in python 2.6.  this is here so we can use py2.5
# copied from http://docs.python.org/library/itertools.html#itertools.product
def product(*args, **kwds):
    # product('ABCD', 'xy') --> Ax Ay Bx By Cx Cy Dx Dy
    # product(range(2), repeat=3) --> 000 001 010 011 100 101 110 111
    pools = map(tuple, args) * kwds.get('repeat', 1)
    result = [[]]
    for pool in pools:
        result = [x+[y] for x in result for y in pool]
    for prod in result:
        yield tuple(prod)

def xyrange(x,y):
    for i in xrange(x):
        for j in xrange(y):
            yield (i,j)

def unique_count_dict( iterable ):
    dcounts = dict()
    for element in iterable:
        dcounts[element] = dcounts.get( element, 0 ) + 1
    return dcounts

################################################################################
### process functions

def exec_wait( arglists ):
    return subprocess.Popen( flatten( arglists ), stderr=subprocess.STDOUT, stdout=subprocess.PIPE ).communicate()

################################################################################
### file functions

def file_to_json( jsonfn ):
    if isinstance(jsonfn,list):
        fn = os.path.join(jsonfn[0],jsonfn[1])
    else:
        fn = jsonfn
    j = json.load( open( os.path.abspath( fn ) ) )
    return j

def json_to_file( jsonfn, j ):
    j_string = json.dumps( j, indent=4, sort_keys=True )
    open( jsonfn, 'w' ).write( j_string )

def array_to_file( filename, a, c ):
    header = str.encode('B' + a.typecode)
    l = int(len(a)/c)
    dim = array('i', [l, 1, c])
    
    f = open( filename, "wb" )
    f.write(header)
    dim.tofile(f)
    a.tofile(f)
    f.close()

def file_to_array( filename, array_type, c ):
    a = array( array_type )
    dim = array('i')
    
    f = open(filename, "rb")
    header = f.read(2)
    assert header[0] == 'B' and header[1] == array_type, "unexpected header in positions file (%s): '%s'" % (filename,header)
    dim.read( f, 3 )
    assert dim[1] == 1 and dim[2] == c, "unexpected dim in positions file (%s): %d, %d, %d" % (filename,dim[0],dim[1],dim[2])
    a.read( f, dim[0] * c )
    f.close()
    
    return a

#### refactor code to use above functions (or similar naming conventions)
def loadjson( jsonfn ):
    j = json.load( open( os.path.abspath( jsonfn ) ) )
    return j

def loadjsonfromstepfn( stepfn ):
    return loadjson( "%s.json" % stepfn )

def loadjsonfromblendfn( blendfn ):
    (stepfn,ext) = os.path.splitext( blendfn )
    return loadjsonfromstepfn( stepfn )

def writejson( jsonfn, j ):
    j_string = json.dumps( j, indent=4, sort_keys=True )
    open( jsonfn, 'w' ).write( j_string )

def load_positions( posfn ):
    return file_to_array( posfn, 'f', 3 )

def load_selected( selfn ):
    return file_to_array( selfn, 'b', 1 )
####

# returns true if filename0 and filename1 are identical
def compare_files(filename0, filename1):
    same = True
    
    f0 = open(filename0, "rb" )
    f1 = open(filename1, "rb" )
    
    b0 = f0.read(1)
    b1 = f1.read(1)
    while b0 == b1 and b0 != "" and b1 != "":
        b0 = f0.read(1)
        b1 = f1.read(1)
    if b0 != b1:
        same = False
    
    f0.close()
    f1.close()
    
    return same

# returns count of files that match filepattern
def count_files( filepattern ):
    return len( glob.glob( filepattern ) )

# returns last (alphabetical) file in list
def get_last_stepfn():
    files = glob.glob( allstepfiles )
    if( len(files) == 0 ):
        return None
    files.sort()
    return files[len(files) - 1]

# removes files that match filepattern
def remove_files( filepattern ):
    for fn in glob.glob( filepattern ):
        os.remove( fn )


def readbin_uint( fp ):
    return struct.unpack('I',fp.read(4))[0]
def readbin_int( fp ):
    return struct.unpack('i',fp.read(4))[0]
def readbin_float( fp ):
    return struct.unpack('f',fp.read(4))[0]

########################################################################################################################
### path functions

def find_syspath_to_file( fn ):
    for p in sys.path:
        pfn = os.path.join( p, fn )
        if os.path.exists( pfn ): return pfn
    assert False, 'could not find file (%s) given paths: %s' % (fn,','.join(sys.path))


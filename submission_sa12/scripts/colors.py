import math, random
from vec3f import *

class Colors:
    # "primary" colors
    Red    = [ 1.00, 0.00, 0.00 ]
    Green  = [ 0.00, 1.00, 0.00 ]
    Blue   = [ 0.00, 0.00, 1.00 ]
    
    # mixed colors
    Yellow = [ 1.00, 1.00, 0.00 ]
    Cyan   = [ 0.00, 1.00, 1.00 ]
    Purple = [ 1.00, 0.00, 1.00 ]
    
    # dark, mixed colors
    Brown  = [ 0.50, 0.50, 0.00 ]
    
    # gray shades
    White  = [ 1.00, 1.00, 1.00 ]
    Gray   = [ 0.50, 0.50, 0.50 ]
    Black  = [ 0.00, 0.00, 0.00 ]
    
    # "nice", bright colors: colors that are easy on the eye
    nb_Orange = [ 0.75, 0.40, 0.00 ]
    nb_Yellow = [ 0.90, 0.90, 0.10 ]
    nb_Green  = [ 0.20, 0.90, 0.20 ]
    nb_Red    = [ 0.90, 0.20, 0.20 ]
    nb_Cyan   = [ 0.05, 0.90, 0.70 ]
    
    # "nice", dark colors
    nd_Red    = [ 0.40, 0.05, 0.05 ]
    nd_Green  = [ 0.05, 0.40, 0.05 ]
    
    # desaturated colors
    ds_Blue  = [ 0.50, 0.50, 0.90 ]


# return a random color, avoiding colors (lavoid + black) by threshold
def random_color( lavoid=None, threshold=0.4 ):
    lbc = lavoid or []
    while True:
        co = [ random.uniform(0.2,1.0), random.uniform(0.2,1.0), random.uniform(0.2,1.0) ]
        # are we far enough away from the colors to avoid?
        if all( sum([(c0-c1)*(c0-c1) for c0,c1 in zip(co,bc)]) > threshold for bc in lbc ):
            break
    return co

def random_color_primaries():
    return random.choice( [
        [ 1.0, 0.2, 0.2 ],
        [ 0.2, 1.0, 0.2 ],
        [ 0.2, 0.2, 1.0 ],
        
        [ 1.0, 0.5, 0.0 ],
        [ 1.0, 0.0, 0.5 ],
        [ 0.5, 1.0, 0.0 ],
        [ 0.0, 1.0, 0.5 ],
        [ 0.5, 0.0, 1.0 ],
        [ 0.0, 0.5, 1.0 ],
        
        [ 1.0, 1.0, 0.0 ],
        [ 1.0, 0.0, 1.0 ],
        [ 0.0, 1.0, 1.0 ],
        
        [ 0.5, 0.5, 0.5 ],
        [ 1.0, 0.5, 0.5 ],
        [ 0.5, 1.0, 0.5 ],
        [ 0.5, 0.5, 1.0 ],
        
        [ 1.0, 1.0, 0.7 ],
        [ 1.0, 0.7, 1.0 ],
        [ 0.7, 1.0, 1.0 ],
        ] )


# return a random color from a subset of "primaries"
def random_color_subset():
    c = [ [1,1,1], [1,1,0], [1,0,1], [0,1,1], [1,0,0], [0,1,0], [0,0,1] ]
    return random.choice( c )

def color_ramp( lcolors, v, m ):
    l = float(len(lcolors) - 1)
    p = max( 0.0, min( 1.0, float(v) / float(m) ) )
    i_c0 = int( math.floor( l * p ) )
    i_c1 = min( i_c0+1, int(l) )
    
    return Vec3f.t_lerp( lcolors[i_c0], lcolors[i_c1], p*l - math.floor(p*l) )


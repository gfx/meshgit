import time, subprocess, os, sys, signal
from bisect import insort
from itertools import islice

from mesh import *
from miscfuncs import *

from debugwriter import DebugWriter

from correspondencebuilder_logic import CorrespondenceBuilder_Logic as CB_Logic

"""
NOTE:   using functions with staticmethod decorators forces fully qualified naming, which makes it much easier to find
        where functions are located!!
"""

class CorrespondenceBuilder_Greedy(object):
    
    @staticmethod
    def build_iterative( opts ):
        CB_Greedy = CorrespondenceBuilder_Greedy
        
        ci = opts['ci']
        
        opts.setdefault( 'threshold', 0.10 )
        opts.setdefault( 'done', False )
        opts.setdefault( 'l_prevmatches', list() )
        opts.setdefault( 'epsilon', 0.002 )
        
        costs     = opts['costs']
        maxiters  = opts['maxiters']
        epsilon   = opts['epsilon']
        threshold = opts['threshold']
        
        DebugWriter.start( 'building correspondences using iterative greedy' )
        DebugWriter.report( 'costs', costs )
        DebugWriter.report( 'maxiters', maxiters )
        DebugWriter.report( 'epsilon', epsilon )
        DebugWriter.report( 'threshold', threshold )
        
        time_start = time.time()
        
        DebugWriter.start( 'initial matching' )
        CB_Greedy.build_correspondence_greedy( opts )
        ci.compute_cost( opts )
        DebugWriter.end()
        
        # iterate until converged or max number of runs...
        r = 0
        opts['l_prevmatches'] += [ (dict(ci.mv01),dict(ci.mf01)) ]
        cv0,cf0 = ci.cv0,ci.cf0
        cv1,cf1 = ci.cv1,ci.cf1
        tv = cv0 + cf0 + cv1 + cf1
        
        for r in xrange(1,maxiters+1):
            
            DebugWriter.blank( 2 )
            DebugWriter.start( 'starting iteration %d' % r )
            
            DebugWriter.start( 'backtracking' )                                 # backtrack: kick out of local minimum
            CB_Logic.remove_twisted_faces( opts )
            CB_Logic.remove_mismatched( opts )
            CB_Logic.remove_smaller_connected_faces_threshold( opts )
            CB_Logic.remove_vert_matching_any_unmatched_faces( opts )
            DebugWriter.end()
            
            DebugWriter.start( 'greedy step' )                                  # perform greedy algorithm
            CB_Greedy.build_correspondence_greedy( opts )
            DebugWriter.end()
            
            DebugWriter.end()                                                   # end of iteration
            
            ci.compute_cost( opts )
            
            # have we been "close" to this matching before?
            mv01,mf01 = dict(ci.mv01),dict(ci.mf01)
            lcommon = [
                sum( 1 for i in xrange(cv0) if ( i in mv01 and i not in pv01 ) or ( i not in mv01 and i in pv01 ) or ( i in mv01 and i in pv01 and mv01[i] != pv01[i] ) )
                + sum( 1 for i in xrange(cf0) if ( i in mf01 and i not in pf01 ) or ( i not in mf01 and i in pf01 ) or ( i in mf01 and i in pf01 and mf01[i] != pf01[i] ) )
                for (pv01,pf01) in opts['l_prevmatches']
                ]
            DebugWriter.report( 'common', ','.join( '%d (%0.4f)' % (v,float(v)/float(tv)) for v in lcommon ) )
            if lcommon and float(min(lcommon))/float(tv) <= epsilon:
                DebugWriter.printer( 'small update detected!\nleaving' )
                opts['done'] = True
                break
            opts['l_prevmatches'] += [ (mv01,mf01) ]
            
        
        CB_Logic.remove_vert_matching_all_unmatched_faces( opts )
        CB_Logic.remove_twisted_faces( opts )
        
        DebugWriter.blank( 2 ).divider()
        DebugWriter.printer( 'CONVERGED!!' if opts['done'] else 'NOT converged' )
        DebugWriter.divider()
        
        DebugWriter.blank( 1 ).divider( char='=' )
        DebugWriter.report( 'results', {
            'iters,time':       '%i, % 8.2fs'%(r,time.time() - time_start),
            'counts, verts': {
                'total':        [ len(ci.lv0), len(ci.lv1) ],
                'matches':      len(ci.mv01),
                'unmatch':      [ len(ci.lv0)-len(ci.mv01), len(ci.lv1)-len(ci.mv10) ],
                },
            'counts, faces': {
                'total':        [ len(ci.lf0), len(ci.lf1) ],
                'matches':      len(ci.mf01),
                'unmatch':      [ len(ci.lf0)-len(ci.mf01), len(ci.lf1)-len(ci.mf10) ],
                },
            'converged':        opts['done'],
            } )
        DebugWriter.divider( char='=' ).blank( 1 )
        
        DebugWriter.end()
    
    
    
    @staticmethod
    def write_mesh_data( fname, opts ):
        ci,costs = opts['ci'],opts['costs']
        
        mnlifv0 = max( len(f.lu_v) for f in ci.lf0 )
        mnlifv1 = max( len(f.lu_v) for f in ci.lf1 )
        mnlavv0 = max( len(ci.la_v0[i]['si_v']) for i in xrange(ci.cv0) )
        mnlavv1 = max( len(ci.la_v1[i]['si_v']) for i in xrange(ci.cv1) )
        mnlavf0 = max( len(ci.la_v0[i]['si_f']) for i in xrange(ci.cv0) )
        mnlavf1 = max( len(ci.la_v1[i]['si_f']) for i in xrange(ci.cv1) )
        mnlaff0 = max( len(ci.la_f0[i]['si_f']) for i in xrange(ci.cf0) )
        mnlaff1 = max( len(ci.la_f1[i]['si_f']) for i in xrange(ci.cf1) )
        #mnlavf1 = max( len(v['si_f']) for v in ci.la_v1 )
        #mnlaff0 = max( len(f['si_f']) for f in ci.la_f0 )
        #mnlaff1 = max( len(f['si_f']) for f in ci.la_f1 )
        lcv0 = [ Vertex.average( ci.m0.opt_get_uf_lv(f0.u_f) ) for i_f0,f0 in ci.elf0() ]
        lfn0 = [ ci.m0.opt_face_normal(i0) for i0 in xrange(ci.cf0) ]
        lcv1 = [ Vertex.average( ci.m1.opt_get_uf_lv(f1.u_f) ) for i_f1,f1 in ci.elf1() ]
        lfn1 = [ ci.m1.opt_face_normal(i1) for i1 in xrange(ci.cf1) ]
        
        fp = open( fname, 'wt' )
        
        fp.write( 'costs\n' )
        fp.write( '%f %f\n' % ( costs['cvdel'], costs['cvadd'] ) )
        fp.write( '%f %f\n' % ( costs['cvdist'], costs['cvnorm'] ) )
        fp.write( '%f %f\n' % ( costs['cfdel'], costs['cfadd'] ) )
        fp.write( '%f %f %f\n' % ( costs['cfdist'], costs['cfvunk'], costs['cfvmis'] ) )
        fp.write( '%f %f %f\n' % ( costs['cfnorm'], costs['cfmism'], costs['cfmunk'] ) )
        fp.write( '%f %f\n' % (costs['cfvdist'], costs['cffdist']) )
        
        fp.write( '%d %d\n' % ( costs['k'], costs['maxiters'] ) )
        
        fp.write( 'counts\n' )
        fp.write( '%d %d %d %d\n' % ( ci.cv0, ci.cv1, ci.cf0, ci.cf1 ) )
        fp.write( '%d %d %d %d %d %d %d %d\n' % (mnlifv0,mnlifv1,mnlavv0,mnlavv1,mnlavf0,mnlavf1,mnlaff0,mnlaff1) )
        
        fp.write( 'verts0\n' )
        for v in ci.lv0:
            fp.write( '%f %f %f %f %f %f\n' % (v.p[0], v.p[1], v.p[2], v.n[0], v.n[1], v.n[2]) )
        fp.write( 'verts1\n' )
        for v in ci.lv1:
            fp.write( '%f %f %f %f %f %f\n' % (v.p[0], v.p[1], v.p[2], v.n[0], v.n[1], v.n[2]) )
        fp.write( 'faces0\n' )
        for f_p,f_n in zip(lcv0,lfn0):
            fp.write( '%f %f %f %f %f %f\n' % (f_p[0], f_p[1], f_p[2], f_n[0], f_n[1], f_n[2]) )
        fp.write( 'faces1\n' )
        for f_p,f_n in zip(lcv1,lfn1):
            fp.write( '%f %f %f %f %f %f\n' % (f_p[0], f_p[1], f_p[2], f_n[0], f_n[1], f_n[2]) )
        
        fp.write( 'face0verts\n' )
        for f in ci.lf0:
            fp.write( '%i %s\n' % ( len(f.lu_v), ' '.join(str(e) for e in ci.m0._opt_get_f_li_v(f)) ) )
        fp.write( 'face1verts\n' )
        for f in ci.lf1:
            fp.write( '%i %s\n' % ( len(f.lu_v), ' '.join(str(e) for e in ci.m1._opt_get_f_li_v(f)) ) )
        fp.write( 'vert0verts\n' )
        for v in [ci.la_v0[i] for i in xrange(ci.cv0)]:
            fp.write( '%i %s\n' % ( len(v['si_v']), ' '.join(str(e) for e in v['si_v'] ) ) )
        fp.write( 'vert1verts\n' )
        for v in [ci.la_v1[i] for i in xrange(ci.cv1)]:
            fp.write( '%i %s\n' % ( len(v['si_v']), ' '.join(str(e) for e in v['si_v'] ) ) )
        fp.write( 'vert0faces\n' )
        for v in [ci.la_v0[i] for i in xrange(ci.cv0)]:
            fp.write( '%i %s\n' % ( len(v['si_f']), ' '.join(str(e) for e in v['si_f'] ) ) )
        fp.write( 'vert1faces\n' )
        for v in [ci.la_v1[i] for i in xrange(ci.cv1)]:
            fp.write( '%i %s\n' % ( len(v['si_f']), ' '.join(str(e) for e in v['si_f'] ) ) )
        fp.write( 'face0faces\n' )
        for f in [ci.la_f0[i] for i in xrange(ci.cf0)]:
            fp.write( '%i %s\n' % ( len(f['si_f']), ' '.join(str(e) for e in f['si_f'] ) ) )
        fp.write( 'face1faces\n' )
        for f in [ci.la_f1[i] for i in xrange(ci.cf1)]:
            fp.write( '%i %s\n' % ( len(f['si_f']), ' '.join(str(e) for e in f['si_f'] ) ) )
        
        fp.write( 'vmatch01\n' )
        for i in xrange(ci.cv0):
            fp.write( '%i\n' % (ci.mv01[i] if i in ci.mv01 else -1) )
        fp.write( 'vmatch10\n' )
        for i in xrange(ci.cv1):
            fp.write( '%i\n' % (ci.mv10[i] if i in ci.mv10 else -1) )
        fp.write( 'vmatch01_iter\n' )
        for i in xrange(ci.cv0):
            fp.write( '%i\n' % (-1) )
        
        fp.write( 'fmatch01\n' )
        for i in xrange(ci.cf0):
            fp.write( '%i\n' % (ci.mf01[i] if i in ci.mf01 else -1) )
        fp.write( 'fmatch10\n' )
        for i in xrange(ci.cf1):
            fp.write( '%i\n' % (ci.mf10[i] if i in ci.mf10 else -1) )
        fp.write( 'fmatch01_iter\n' )
        for i in xrange(ci.cf0):
            fp.write( '%i\n' % (ci.dmf01[i] if i in ci.dmf01 else -1) )
        
        fp.write( 'iter_current\n' )
        fp.write( '%i\n' % (ci.norder) )
        
        fp.write( 'end\n' )
        fp.close()
    
    @staticmethod
    def read_mesh_data( fname, opts ):
        ci = opts['ci']
        
        fp = open( fname,'rt')
        idx = [ int(s) for s in fp.read().split('\n') if s ]
        fp.close()
        
        ci.dcvall()
        ci.dcfall()
        for i in xrange(ci.cv0):
            i0,i1 = i,idx[i]
            if i1 != -1: ci.acv( i0, i1 )
        for i in xrange(ci.cf0):
            i0,i1 = i,idx[i+ci.cv0]
            if i1 != -1: ci.acf( i0, i1 )
        for i in xrange(ci.cv0):
            vm01_iter = idx[i+ci.cv0+ci.cf0]
        for i in xrange(ci.cf0):
            fm01_iter = idx[i+ci.cv0+ci.cf0+ci.cv0]
            if fm01_iter != -1:
                ci.dmf01[i] = fm01_iter
        ci.norder = idx[-1]
    
    @staticmethod
    def build_correspondence_greedy( opts ):
        CB_Greedy = CorrespondenceBuilder_Greedy
        #CB_Greedy.build_correspondence_greedy_c( opts )
        CB_Greedy.build_correspondence_greedy_fastc( opts )
        #CB_Greedy.build_correspondence_greedy_py( opts )
    
    @staticmethod
    def build_correspondence_greedy_c( opts ):
        CB_Greedy = CorrespondenceBuilder_Greedy
        
        CB_Greedy.write_mesh_data( 'greedy_input.txt', opts )
        
        restart = True
        
        while True:
            if restart:
                last,restart,skip = None,False,0
                open( 'heartbeat', 'wt' ).write('')
                prog = subprocess.Popen( [ find_syspath_to_file('greedy') ] )  #.communicate()
            
            time.sleep( 0.25 )
            if prog.poll() is not None: break
            
            fp = open( 'heartbeat', 'rt' )
            status = fp.readline()
            if status == 'running\n': v = fp.readline()
            else: v = ''
            fp.close()
            
            if status == 'running\n':
                if last == v:
                    skip += 1
                    if skip == 4:
                        DebugWriter.printer( '^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^' )
                        DebugWriter.printer( 'detected stalled thread... restarting!' )
                        DebugWriter.printer( '^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^' )
                        os.kill( prog.pid, signal.SIGTERM )
                        restart = True
                else:
                    skip = 0
            last = v
        
        CB_Greedy.read_mesh_data( 'greedy_output.txt', opts )
    
    @staticmethod
    def build_correspondence_greedy_fastc( opts ):
        CB_Greedy = CorrespondenceBuilder_Greedy
        CB_Greedy.write_mesh_data( 'greedy_input.txt', opts )
        if os.path.exists( 'greedy_output.txt' ):
            os.remove( 'greedy_output.txt' )
        
        DebugWriter.start( 'greedy_nlogn' )
        for i in xrange(4):
            DebugWriter.divider( char='-' ).divider( char='=' )
            if DebugWriter.quiet_level == 0:
                subprocess.Popen( [ find_syspath_to_file('greedy2_binheap') ] ).communicate()
            else:
                subprocess.Popen( [ find_syspath_to_file('greedy2_binheap') ], stdout=subprocess.PIPE ).communicate()
            if os.path.exists( 'greedy_output.txt' ): break
            DebugWriter.divider( char='*' ).divider( char='*' )
            DebugWriter.printer( 'greedy algorithm did not write output data...' )
        
        assert os.path.exists( 'greedy_output.txt' ), 'greedy algorithm did not write output data'
        DebugWriter.divider( char='=' ).divider( char='-' )
        DebugWriter.end()
        
        CB_Greedy.read_mesh_data( 'greedy_output.txt', opts )
    
    # from: http://mail.python.org/pipermail/python-list/2010-September/1254546.html
    @staticmethod
    def nsmallest_slott_bisect( iterable, n, insort=insort ):
        it = iter(iterable)
        mins = sorted(islice(it,n))
        maxmin = mins[-1]
        for el in it:
            if el < maxmin:
                insort( mins, el )
                mins.pop()
                maxmin = mins[-1]
        return mins
    
    @staticmethod
    def build_correspondence_greedy_py( opts ): #ci, maxruns=None, costs=None ):
        opts.setdefault( 'maxruns', opts['ci'].cv0+opts['ci'].cf0 )
        opts['costcache'] = {
            'di_v01': {},
            'di_f01': {},
            'di_v01_costaccum': {},
            'di_f01_costaccum': {},
            }
        opts['dirty'] = { 'si_v0_dirty': set(), 'si_v1_dirty': set(), 'si_f0_dirty': set(), 'si_f1_dirty': set(), }
        opts.setdefault( 'du_runcount', {} )
        opts.setdefault( 'run_start', 0 )
        
        ci = opts['ci']
        maxruns = opts['maxruns']
        run_start = opts['run_start']
        costs = opts['costs']
        opts_ = ci.get_optimizations( opts )
        costcache = opts['costcache']
        di_v01_costcache = costcache['di_v01']
        di_f01_costcache = costcache['di_f01']
        si_v0_dirty = opts['dirty']['si_v0_dirty']
        si_v1_dirty = opts['dirty']['si_v1_dirty']
        si_f0_dirty = opts['dirty']['si_f0_dirty']
        si_f1_dirty = opts['dirty']['si_f1_dirty']
        
        k = 20
        
        m0,m1 = ci.m0,ci.m1
        
        di_mv01,di_mv10 = ci.mv01,ci.mv10
        di_mf01,di_mf10 = ci.mf01,ci.mf10
        si_uv0 = set( iv0 for iv0,_ in m0.elv() if iv0 not in di_mv01 )
        si_uv1 = set( iv1 for iv1,_ in m1.elv() if iv1 not in di_mv10 )
        si_uf0 = set( if0 for if0,_ in m0.elf() if if0 not in di_mf01 )
        si_uf1 = set( if1 for if1,_ in m1.elf() if if1 not in di_mf10 )
        mvd,mvn,mfd,mfn = opts_['mvd'],opts_['mvn'],opts_['mfd'],opts_['mfn']
        li_va0,li_va1 = opts_['li_va0'],opts_['li_va1']
        li_fa0,li_fa1 = opts_['li_fa0'],opts_['li_fa1']
        
        DebugWriter.start( 'computing initial cost' )
        init_cost = ci.compute_cost( opts )
        DebugWriter.report( 'cost', init_cost )
        DebugWriter.end()
        
        DebugWriter.start( 'running' )
        best_cost = init_cost
        vcosts = []
        fcosts = []
        for run in xrange(maxruns):
            prev_cost = best_cost
            best_action = None
            
            if run % 50 == 0:
                DebugWriter.start( 'determining %i-nn' % k )
                iv0_1 = [ sorted([ (iv1,mvd[iv0][iv1]) for iv1 in si_uv1 ],key=lambda x:x[1])[:k] if iv0 in si_uv0 else [] for iv0 in xrange(ci.cv0) ]
                iv0_1 = [ [ iv1 for iv1,_ in iv0_1[iv0] ] for iv0 in xrange(ci.cv0) ]
                if0_1 = [ sorted([ (if1,mfd[if0][if1]) for if1 in si_uf1 ],key=lambda x:x[1])[:k] if if0 in si_uf0 else [] for if0 in xrange(ci.cf0) ]
                if0_1 = [ [ if1 for if1,_ in if0_1[if0] ] for if0 in xrange(ci.cf0) ]
                DebugWriter.end()
            
            t0 = time.time()
            try:
                i_v0,i_v1,vcost = min( ((iv0,iv1,ci.compute_cost_delta_acv(opts,iv0,iv1)) for iv0 in si_uv0 for iv1 in iv0_1[iv0] if iv1 in si_uv1), key=lambda x:x[2] )
                vcost += prev_cost
            except ValueError:
                vcost = float('inf')
            #vcosts[:] = [ (iv0,iv1,ci.compute_cost_delta_acv(opts,iv0,iv1)) for iv0 in si_uv0 for iv1 in iv0_1[iv0] if iv1 in si_uv1 ]
            t1 = time.time()
            try:
                i_f0,i_f1,fcost = min( ((if0,if1,ci.compute_cost_delta_acf(opts,if0,if1)) for if0 in si_uf0 for if1 in if0_1[if0] if if1 in si_uf1), key=lambda x:x[2] )
                fcost += prev_cost
            except ValueError:
                fcost = float('inf')
            #fcosts[:] = [ (if0,if1,ci.compute_cost_delta_acf(opts,if0,if1)) for if0 in si_uf0 for if1 in if0_1[if0] if if1 in si_uf1 ]
            t2 = time.time()
            
            #i_v0,i_v1,vcost = min( vcosts, key=lambda x:x[2] ) if vcosts else (-1,-1,float('inf'))
            #i_f0,i_f1,fcost = min( fcosts, key=lambda x:x[2] ) if fcosts else (-1,-1,float('inf'))
            #vcost += prev_cost
            #fcost += prev_cost
            
            if vcost >= best_cost and fcost >= best_cost: break         # can do no better
            
            si_v0_dirty.clear()
            si_v1_dirty.clear()
            si_f0_dirty.clear()
            si_f1_dirty.clear()
            
            if vcost <= fcost:
                best_action = ('v',i_v0,i_v1)
                best_cost = vcost
                
                ci.acv( i_v0, i_v1 )
                si_uv0.remove( i_v0 )
                si_uv1.remove( i_v1 )
                
                opts['du_runcount'][m0.lv[i_v0].u_v] = run+run_start
                
                for _i_f0 in li_va0[i_v0]['si_af']: si_f0_dirty.add( _i_f0 )
                for _i_f1 in li_va1[i_v1]['si_af']: si_f1_dirty.add( _i_f1 )
            
            else:
                best_action = ('f',i_f0,i_f1)
                best_cost = fcost
                
                ci.acf( i_f0, i_f1 )
                si_uf0.remove( i_f0 )
                si_uf1.remove( i_f1 )
                
                opts['du_runcount'][m0.lf[i_f0].u_f] = run+run_start
                
                for _i_v0 in li_fa0[i_f0]['si_v']: si_v0_dirty.add( _i_v0 )
                for _i_v1 in li_fa1[i_f1]['si_v']: si_v1_dirty.add( _i_v1 )
                for _i_f0 in li_fa0[i_f0]['si_afv']: si_f0_dirty.add( _i_f0 )
                for _i_f1 in li_fa1[i_f1]['si_afv']: si_f1_dirty.add( _i_f1 )
            
            DebugWriter.printer( 'run=%6i, cost=%6.1f (-%3.3f), op:%s, time:%.2f+%.2f=%.2f' % (run+run_start,best_cost,prev_cost-best_cost,best_action,t1-t0,t2-t1,t2-t0) )
        
        DebugWriter.report( 'delta cost', init_cost-best_cost )
        DebugWriter.end()
        
        opts['run_start'] = run+run_start
        
        return ci
    
    
    #@staticmethod
    #def build_supergreedy( opts ):
        #CB_Greedy = CorrespondenceBuilder_Greedy
        
        #ci = opts['ci']
        
        #CB_Greedy.build_vertex_correspondences_greedy_closestpoints( opts )
        #CB_Logic.build_face_correspondences( ci )
        #CB_Logic.remove_face_conflicts( ci, 1.0 ) #0.75 )
        #CB_Logic.remove_smaller_connected_faces_threshold( ci )
        #ci.dcvall()
        #CB_Logic.build_vertex_correspondences_faces( ci )
    
    #@staticmethod
    #def build_seeded( opts ):
        #CB_Greedy = CorrespondenceBuilder_Greedy
        
        #ci = opts['ci']
        #DebugWriter.divider()
        
        #costs = {
            #'cvadd':   5.0, #5.0,
            #'cvdel':   5.0, #5.0,
            #'cvdist':  7.5, #7.5,
            #'cvnorm':  7.5, #7.5,
            #'cvmism':  0.0,
            
            #'cfadd':   50.0, #50.0
            #'cfdel':   50.0, #50.0
            #'cfvdist': 2.0,  # 2.0
            #'cfvunk':  12.0, #12.0
            #'cfvmis':  25.0, #25.0
            #'cfnorm':  80.0, #80.0
            #'cfmism':  50.0, #50.0
            #'cfmunk':  25.0, #25.0
            #}
        #DebugWriter.report( 'costs', costs )
        
        #build_vertex_correspondences_greedy_closestpoints( opts )#, costs=costs )
        #build_face_correspondences( ci )
        #remove_smaller_connected_faces_threshold( ci )#, threshold=0.05 )
        ##remove_vert_matching_all_unmatched_faces( ci )
        #ci.dcvall()
        
        #build_correspondence_greedy( ci, costs=costs )
        #remove_vert_matching_all_unmatched_faces( ci )
    
    #@staticmethod
    #def build_vertex_correspondences_greedy_closestpoints( opts ) #ci, costs=None ):
        #ci = opts['ci']
        #costs = opts.get( 'costs', None )
        
        #write_mesh_data( ci, 'greedy_input.txt', costs=costs )
        #subprocess.Popen( [ find_syspath_to_file('greedy_closestverts') ] ).communicate()
        #read_mesh_data( ci, 'greedy_output.txt' )
        
        #if False:
            #threshold = 10.0
            #cvert = 7.5
            #cnorm = 7.5
            #ccount = 0.0
            
            #print( "building vertex correspondences (%ix%i) using greedy closest points method..." % (ci.cv0,ci.cv1) )
            
            #data0 = [ (iv0,v0,ci.ln0[iv0],len(ci.la_v0[iv0]['si_f'])) for (iv0,v0) in ci.elv0() ]
            #data1 = [ (iv1,v1,ci.ln1[iv1],len(ci.la_v1[iv1]['si_f'])) for (iv1,v1) in ci.elv1() ]
            
            #l01d = [ (
                    #iv0, iv1,
                    #Vertex.distance( v0, v1 ) * cvert + ( c0 - c1 )**2 * ccount + ( 1.0 - Vec3f.t_dot( n0, n1 ) ) / 2.0 * cnorm
                #) for (iv0,v0,n0,c0) in data0 for (iv1,v1,n1,c1) in data1 ]
            #l01d = filter( lambda x:x[2] < threshold, l01d )
            #l01d = sorted( l01d, key=lambda e:e[2] )
            
            #for (iv0,iv1,d2) in l01d:
                #if iv0 in ci.mv01 or iv1 in ci.mv10: continue
                #ci.acv( iv0, iv1 )
        
        #return ci
    
    
    #@staticmethod
    #def build_vertex_correspondences_same_position_01( ci ):
        #threshold = 0.01
        
        #print( "building vertex correspondences (%ix%i) by same positions m0->m1..." % (ci.cv0,ci.cv1) )
        
        #for (iv0,v0) in ci.elv0():
            #ld = [ (iv1,Vertex.distance2( v0, v1 )) for (iv1,v1) in ci.elv1() if iv1 not in ci.mv10 ]
            #if not ld: continue
            #iv1,d = min( ld, key=lambda x:x[1] )
            #if d >= threshold: continue
            #ci.acv( iv0, iv1 )
        
        #return ci
    
    
    #@staticmethod
    #def build_vertex_correspondences_same_position_10( ci ):
        #threshold = 0.01
        
        #print( "building vertex correspondences (%ix%i) by same positions m1->m0..." % (ci.cv0,ci.cv1) )
        
        #for (iv1,v1) in ci.elv1():
            #ld = [ (iv0,Vertex.distance2( v0, v1 )) for (iv0,v0) in ci.elv0() if iv0 not in ci.mv01 ]
            #if not ld: continue
            #iv0,d = min( ld, key=lambda x:x[1] )
            #if d >= threshold: continue
            #ci.acv( iv0, iv1 )
        
        #return ci

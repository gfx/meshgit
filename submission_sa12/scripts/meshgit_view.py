import math

from mesh import *
from viewer import *
from shapes import *
from vec3f import *

from debugwriter import DebugWriter

from correspondenceinfo import *

from meshgit_misc import *
from meshgit_med_ops import *

def run_view( *lfn, **opts ):
    
    subd = int(opts.get('subd',0))
    
    lm = [ Mesh.fromPLY(fn) for fn in lfn ]
    if subd:
        ls = [ m.catmull_clark_subd( subd ).toShape(smooth=True) for m in lm ]
    else:
        ls = [ m.toShape(smooth=False) for m in lm ]
    lcaps = [ '%s\n%d, %d' % (fn,len(m.lv),len(m.lf)) for fn,m in zip(lfn,lm) ]
    
    print( 'stats:' )
    for fn,m in zip(lfn,lm):
        print( '  %05d %05d: %s' % (len(m.lv),len(m.lf),fn) )
    
    window = Viewer( ls, caption='MeshGit Viewer', lcaptions=lcaps, ncols=5 )
    window.app_run()

def run_change_viewer( fn0, fn1, **opts ):
    method = opts.get( 'method', None )
    maxiters = opts.get( 'maxiters', None )
    
    m0,m1= [ Mesh.fromPLY( fn ) for fn in [fn0,fn1] ]
    scaling_factor = scale( [ m0, m1 ] )
    ci01 = CorrespondenceInfo.build( m0, m1, method=method, maxiters=maxiters )
    scale( [ m0, m1 ], scaling_factor=(1.0/scaling_factor) )
    m1 = ci01.get_corresponding_mesh1( allow_face_sub=False )
    
    for m in [m0,m1]:
        m.optimize()
        m.rebuild_edges()
    
    d_vcmds,l_fcmds = diff_commands_by_partition( m0, m1 )
    
    l_f = [ Vec3f.t_average( [Vec3f.t_average( [v.p for v in m0.opt_get_uf_lv(cmd[8:])] ) for cmd in fcmds['ufd'] if cmd.startswith('delface')] ) for fcmds in l_fcmds ]
    l_f = [ math.atan2(x[0],x[1]) for x in l_f ]
    li_sorted = sorted( xrange(len(l_fcmds)), key=lambda x:l_f[x] )
    l_fcmds = [ l_fcmds[i] for i in li_sorted ]
    
    DebugWriter.start( 'creating intermediate meshes' )
    DebugWriter.start( 'deleting', quiet=True )
    lm0a = []
    for fcmds in l_fcmds:
        if fcmds['ufd']:
            lm0a += [ do_commands( (lm0a[-1] if lm0a else m0).clone(), fcmds['ufd'] ) ]
    DebugWriter.end()
    
    DebugWriter.start( 'moving', quiet=True )
    ma = lm0a[-1].clone() if lm0a else m0.clone()
    mb = do_commands( ma.clone(), d_vcmds['uva'] + d_vcmds['uvm'] )
    DebugWriter.end()
    
    DebugWriter.start( 'adding', quiet=True )
    lmb1 = []
    for fcmds in l_fcmds:
        if fcmds['ufa']:
            lmb1 += [ do_commands( (lmb1[-1] if lmb1 else mb).clone(), fcmds['ufa'] ) ]
    DebugWriter.end()
    
    DebugWriter.report( '0a', len(lm0a) )
    DebugWriter.report( 'b1', len(lmb1) )
    DebugWriter.end()
    
    ma.optimize()
    mb.optimize()
    
    DebugWriter.start( 'creating transitions' )
    c_del = 5
    c_mov = 20
    
    DebugWriter.quiet()
    ls = [ m0.toShape() ]*5
    DebugWriter.loud()
    
    DebugWriter.start( 'deleting', quiet=True )
    for ma_,mb_ in zip([m0]+lm0a[:-1],lm0a):
        suf_a_ = set( ma_.get_lu_f() )
        suf_b_ = set( mb_.get_lu_f() )
        suf_diff = suf_a_ - suf_b_
        
        mc_0 = ma_.clone_filter_faces( suf_diff, False ).trim()
        sc_0 = mc_0.toShape()
        
        mc_1 = ma_.clone_filter_faces( suf_diff, True ).trim()
        for x in xrange(c_del):
           per = float(x+1)/float(c_del)
           sc_1 = mc_1.clone().recolor( color_scheme['-']+[1.0-per] ).toShape( use_alpha=True )
           ls += [ Shape_Combine( [ sc_0, sc_1 ] ) ]
    ls += [ ma.toShape() ]
    DebugWriter.end()
    
    DebugWriter.start( 'moving', quiet=True )
    suv_a = set( ma.get_lu_v() )
    suv_b = set( mb.get_lu_v() )
    suv_intr = suv_a & suv_b
    lip_aligned = [ ( ma._opt_get_i_v(u_v), ma.get_v(u_v).p, mb.get_v(u_v).p ) for u_v in suv_intr ]
    for x in xrange(c_mov):
        per = float(x+1)/float(c_mov)
        mc = ma.clone()
        for (i_v,pa,pb) in lip_aligned:
            mc.lv[i_v].p = Vec3f.t_lerp( pa, pb, per )
        ls += [ mc.toShape() ]
    DebugWriter.end()
    
    DebugWriter.start( 'adding', quiet=True )
    ls += [ mb.toShape() ]
    for ma_,mb_ in zip([mb]+lmb1[:-1],lmb1):
        suf_a_ = set( ma_.get_lu_f() )
        suf_b_ = set( mb_.get_lu_f() )
        suf_diff = suf_b_ - suf_a_
        
        mc_0 = mb_.clone_filter_faces( suf_diff, False ).trim()
        sc_0 = mc_0.toShape()
        
        mc_1 = mb_.clone_filter_faces( suf_diff, True ).trim()
        for x in xrange(c_del):
            per = float(x+1)/float(c_del)
            sc_1 = mc_1.clone().recolor( color_scheme['+']+[per] ).toShape( use_alpha=True )
            ls += [ Shape_Combine( [ sc_0, sc_1 ] ) ]
    DebugWriter.end()
    
    DebugWriter.quiet()
    ls += [ m1.toShape() ]*5
    DebugWriter.loud()
    
    DebugWriter.end()
    
    DebugWriter.quiet()
    suf_0 = set( m0.get_lu_f() )
    suf_1 = set( m1.get_lu_f() )
    m0s = m0.clone_filter_faces( suf_0-suf_1, False ).trim().recolor( color_scheme['='] )
    m0d = m0.clone_filter_faces( suf_0-suf_1, True ).trim().recolor( color_scheme['-'] )
    s0 = Shape_Combine( [ m0s.toShape(), m0d.toShape() ] )
    m1s = m1.clone_filter_faces( suf_1-suf_0, False ).trim().recolor( color_scheme['='] )
    m1a = m1.clone_filter_faces( suf_1-suf_0, True ).trim().recolor( color_scheme['+'] )
    s1 = Shape_Combine( [ m1s.toShape(), m1a.toShape() ] )
    ls = [ s0, Shape_Cycle( ls, 10, True ), s1 ]
    DebugWriter.loud()
    
    #DebugWriter.start( 'constructing viewable shapes from meshes' )
    #ls = [ m0.toShape(), Shape_Cycle( [ m.toShape() for m in lm ], 4, True ), m1.toShape() ]
    #DebugWriter.end()
    
    window = Viewer( ls, caption='MeshGit Change Viewer' )
    window.app_run()



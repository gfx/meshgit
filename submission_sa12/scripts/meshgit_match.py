import os, re, sys, math, random, itertools, pickle, numpy

from mesh import *
from viewer import *
from shapes import *
from vec3f import *
from colors import *

from correspondenceinfo import *

from meshgit_misc import *
from meshgit_med_ops import *

def run_match_viewer2( fn0, fn1, method=None ):
    m0,m1 = [ Mesh.fromPLY( fn ) for fn in [fn0,fn1] ]
    scaling_factor = scale( [ m0, m1 ] )
    ci = CorrespondenceInfo.build( m0, m1, method )
    m1 = ci.get_corresponding_mesh1()
    scale( [m0,m1], 1.0 / scaling_factor )    
    
    print( 'optimizing...' )
    for m in [m0,m1]: m.optimize()
    
    print( 'creating meshes...' )
    suv0,suv1 = set(m0.get_lu_v()),set(m1.get_lu_v())
    suf0,suf1 = set(m0.get_lu_f()),set(m1.get_lu_f())
    suvu,suvi = suv0|suv1, suv0&suv1
    sufu,sufi = suf0|suf1, suf0&suf1
    
    #uvc = dict( (uv,Vec3f.t_scale(m0.opt_get_v(uv).p,0.5)) if uv in suv0 and uv in suv1 else (uv,(1.0,0.0,0.0)) if uv in suv0 else (uv,(0.0,1.0,0.0)) for uv in suvu )
    uvc = dict( (uv,get_color(uv,suv0,suv1,set())) for uv in suvu )
    #lcolors = [ (0.0,0.0,1.0), (0.0,1.0,1.0), (0.0,1.0,0.0), (1.0,0.0,0.0), (1.0,1.0,0.0), (1.0,1.0,1.0) ]
    #du_runcount = ci.opts['du_runcount']
    #maxruns = max( du_runcount.values() )
    #uvc = dict( (uv,color_ramp(lcolors,du_runcount[uv],maxruns)) if uv in suv0 and uv in suv1 else (uv,(0.5,0.0,0.0)) if uv in suv0 else (uv,(0.0,0.5,0.0)) for uv in suvu )
    ufc = dict( (uf,get_color(uf,suf0,suf1,set())) for uf in sufu )
    
    ma,mb = m0.clone(),m1.clone()
    for v in ma.lv: v.c = uvc[v.u_v]
    for v in mb.lv: v.c = uvc[v.u_v]
    ma.trim().recalc_vertex_normals()
    mb.trim().recalc_vertex_normals()
    
    md,me = Mesh(),Mesh()
    for f in m0.lf:
        u_f = f.u_f
        c = ufc[u_f]
        lv = [ m0.opt_get_v(u_v).copy().recolor(c) for u_v in f.lu_v ]
        lu_v = [ v.u_v for v in lv ]
        md.lv += lv
        md.lf += [ Face(u_f=u_f, lu_v=lu_v) ]
    for f in m1.lf:
        u_f = f.u_f
        c = ufc[u_f]
        lv = [ m1.opt_get_v(u_v).copy().recolor(c) for u_v in f.lu_v ]
        lu_v = [ v.u_v for v in lv ]
        me.lv += lv
        me.lf += [ Face(u_f=u_f, lu_v=lu_v) ]
    
    steps = 20
    mf = [ Mesh() for x in xrange(steps) ]
    cv = 0
    for u_f in sufi:
        f0,f1 = m0.opt_get_f(u_f),m1.opt_get_f(u_f)
        if len(f0.lu_v) != len(f1.lu_v): continue
        if not all( u_v0 in suv1 for u_v0 in f0.lu_v ): continue
        if not all( u_v1 in suv0 for u_v1 in f1.lu_v ): continue
        if set(f0.lu_v) != set(f1.lu_v): continue
        clv = len(f0.lu_v)
        l_v0 = m0.opt_get_uf_lv( u_f )
        l_v1 = m1.opt_get_uf_lv( u_f )
        for i,m in enumerate(mf):
            t = float(i) / float(steps-1)
            for j in xrange(clv):
                #p = Vec3f.t_lerp( l_v0[j].p, l_v1[j].p, t )
                #lp = Vec3f.t_distance( l_v0[j].p, l_v1[j].p )
                j1 = m1._opt_get_i_v( l_v0[j].u_v )
                p = Vec3f.t_lerp( l_v0[j].p, m1.lv[j1].p, t )
                lp = Vec3f.t_distance( l_v0[j].p, m1.lv[j1].p ) * 0.5
                c = [ min(1.0,lp*2.5), min(1.0,lp*10.0), min(lp*25.0,1.0) ]
                m.lv.append( Vertex( p=p, c=c ) )
            m.lf.append( Face( lu_v=[ m.lv[j].u_v for j in xrange(cv,cv+clv) ]) )
        cv += clv
    for i in xrange(steps):
        mf[i].recalc_vertex_normals()
    
    shapes = [ mesh.toShape( smooth=False ) if mesh else None for mesh in [ ma,mb,None, md,me,None ] ]
    shapes[2] = Shape_Cycle( [ m.toShape( smooth=False ) for m in mf ], fps=5, bounce=True )
    window = Viewer( shapes, caption='MeshGit Align Viewer: Methods="%s"' % ','.join(ci.lmethods), ncols=3 )
    window.app_run()

def run_match_viewer2_order( fn0, fn1, method=None, stepsize=10, maxiters=None ):
    stepsize = int(stepsize) if stepsize else 10
    m0,m1 = [ Mesh.fromPLY( fn ) for fn in [fn0,fn1] ]
    scaling_factor = scale( [ m0, m1 ] )
    ci = CorrespondenceInfo.build( m0, m1, method, maxiters=maxiters )
    m1 = ci.get_corresponding_mesh1()
    scale( [m0,m1], 1.0 / scaling_factor )    
    
    print( 'optimizing...' )
    for m in [m0,m1]: m.optimize()
    
    suf0,suf1 = set(m0.get_lu_f()),set(m1.get_lu_f())
    sufu,sufi = suf0|suf1, suf0&suf1
    
    print( 'creating meshes...' )
    lsa,lsb,lsc,lsd = [],[],[],[]
    
    l_iters = sorted(list(set( ci.dmf01.values() )))
    
    iter_step = int(stepsize)
    for i0 in xrange(0,len(l_iters),iter_step):
        i1 = min( i0 + iter_step - 1, len(l_iters) - 1 )
        iter_c0 = l_iters[i0]
        iter_c1 = l_iters[i1]
        
        ma = Mesh()
        mb = Mesh()
        mc = Mesh()
        md = Mesh()
        
        for i_f,f in enumerate(m0.lf):
            u_f = f.u_f
            
            if i_f in ci.dmf01: iter_f = ci.dmf01[i_f]
            else: iter_f = float('inf')
            
            if iter_f < iter_c0: c = [ 0.5, 0.5, 0.5 ]
            elif iter_f > iter_c1: c = [ 0.8, 0.2, 0.0 ]
            else: c = [ 1.0, 1.0, 0.0 ]
            
            lv = [ m0.opt_get_v(u_v).copy().recolor(c) for u_v in f.lu_v ]
            lu_v = [ v.u_v for v in lv ]
            ma.lv += lv
            ma.lf += [ Face(u_f=u_f, lu_v=lu_v) ]
            
            if iter_f <= iter_c1:
                mc.lv += lv
                mc.lf += [ Face(u_f=u_f, lu_v=lu_v) ]
        
        for i_f,f in enumerate(m1.lf):
            u_f = f.u_f
            
            if i_f in ci.mf10: iter_f = ci.dmf01[ ci.mf10[i_f] ]
            else: iter_f = float('inf')
            
            if iter_f < iter_c0: c = [ 0.5, 0.5, 0.5 ]
            elif iter_f > iter_c1: c = [ 0.8, 0.2, 0.0 ]
            else: c = [ 1.0, 1.0, 0.0 ]
            
            lv = [ m1.opt_get_v(u_v).copy().recolor(c) for u_v in f.lu_v ]
            lu_v = [ v.u_v for v in lv ]
            mb.lv += lv
            mb.lf += [ Face(u_f=u_f, lu_v=lu_v) ]
            
            if iter_f <= iter_c1:
                md.lv += lv
                md.lf += [ Face(u_f=u_f, lu_v=lu_v) ]
        
        ma.recalc_vertex_normals()
        mb.recalc_vertex_normals()
        mc.recalc_vertex_normals()
        md.recalc_vertex_normals()
        
        lsa += [ ma.toShape( smooth=False ) ]
        lsb += [ mb.toShape( smooth=False ) ]
        lsc += [ mc.toShape( smooth=False ) ]
        lsd += [ md.toShape( smooth=False ) ]
    
    lshapes = [
        Shape_Cycle( lsa, fps=5, bounce=False ),
        Shape_Cycle( lsb, fps=5, bounce=False ),
        Shape_Cycle( lsc, fps=5, bounce=False ),
        Shape_Cycle( lsd, fps=5, bounce=False ),
        ]
    window = Viewer( lshapes, caption='MeshGit Match Viewer, by Order: Methods="%s"' % ','.join(ci.lmethods), ncols=2 )
    window.app_run()

def run_match_viewer2_lines( fn0, fn1, method=None, scheme=None, maxiters=None ):
    scheme = scheme if scheme is not None else 'muted_verts'
    
    m0,m1 = [ Mesh.fromPLY( fn ) for fn in [fn0,fn1] ]
    scaling_factor = scale( [ m0, m1 ] )
    ci = CorrespondenceInfo.build( m0, m1, method=method, maxiters=maxiters )
    m1 = ci.get_corresponding_mesh1()
    scale( [m0,m1], 1.0 / scaling_factor )
    for m in [m0,m1]: m.optimize()
    
    DebugWriter.start( 'generating meshes' )
    
    DebugWriter.start( 'generating matching meshes' )
    
    suv0,suv1 = set(m0.get_lu_v()),set(m1.get_lu_v())
    suf0,suf1 = set(m0.get_lu_f()),set(m1.get_lu_f())
    suvu,suvi = suv0|suv1, suv0&suv1
    sufu,sufi = suf0|suf1, suf0&suf1
    
    if scheme == 'random':
        uvc = dict( (uv,get_color(uv,suv0,suv1,set())) for uv in suvu )
        ufc = dict( (uf,get_color(uf,suf0,suf1,set())) for uf in sufu )
    elif scheme == 'position':
        uvc = dict()
        ufc = dict()
        
        mp,Mp = m0.get_vert_bounding_box()
        mM = Vec3f.t_sub( Mp, mp )
        
        for u_v in suvi:
            p = m0.opt_get_v( u_v ).p
            uvc[u_v] = Vec3f.t_div( Vec3f.t_sub( p, mp ), mM )
        for u_v in suv0-suvi:
            uvc[u_v] = [ 0.5, 0.1, 0.0 ]
        for u_v in suv1-suvi:
            uvc[u_v] = [ 0.0, 0.5, 0.1 ]
        
        for u_f in sufi:
            f = m0.opt_get_f( u_f )
            lp = [ v.p for v in m0.opt_get_f_lv( f ) ]
            p = Vec3f.t_average( lp )
            ufc[u_f] = Vec3f.t_div( Vec3f.t_sub( p, mp), mM )
        for u_f in suf0-sufi:
            ufc[u_f] = [ 0.5, 0.1, 0.0 ]
        for u_f in suf1-sufi:
            ufc[u_f] = [ 0.0, 0.5, 0.1 ]
    elif scheme == 'order':
        uvc = dict( (uv,get_color(uv,suv0,suv1,set())) for uv in suvu )
        ufc = dict()
        
        cfm = max( ci.dmf01.values() )
        #cfm = ci.norder
        
        for u_f in sufi:
            f = m0.opt_get_f( u_f )
            lp = [ v.p for v in m0.opt_get_f_lv( f ) ]
            p = Vec3f.t_average( lp )
            
            imf01 = ci.dmf01[ m0._opt_get_i_f( u_f ) ]
            c = color_ramp( [
                ( 1.0, 0.0, 0.0 ),
                ( 1.0, 0.5, 0.0 ),
                ( 0.5, 1.0, 0.0 ),
                ( 0.0, 1.0, 0.0 ),
                ( 0.0, 1.0, 1.0 ),
                ( 0.0, 0.0, 1.0 ),
                ( 0.0, 0.0, 0.5 ),
                ( 0.2, 0.2, 0.5 ),
                ( 1.0, 1.0, 1.0 ),
                ], imf01, cfm )
            
            #if imf01 == 0: c = [ 1.0, 1.0, 0.0 ]
            
            ufc[u_f] = c
        for u_f in suf0-sufi:
            ufc[u_f] = [ 0.5, 0.1, 0.0 ]
        for u_f in suf1-sufi:
            ufc[u_f] = [ 0.0, 0.5, 0.1 ]
    elif scheme == 'verts':
        uvc = dict( (uv,random_color_primaries()) for uv in suvu )
        ufc = dict( (
            uf,
            Vec3f.t_average( [ uvc[uv] for uv in m0.opt_get_f(uf).lu_v ] ) if uf in sufi else
            [ 0.5, 0.1, 0.0 ] if uf in suf0 else
            [ 0.0, 0.5, 0.1 ]
            ) for uf in sufu )
    elif scheme == 'muted_verts':
        uvc = dict( (uv, Vec3f.t_lerp( color_scheme['='], random_color_primaries(), 0.40 )) for uv in suv0|suv1 )
        ufc = dict()
        ufc.update( dict( ( uf, Vec3f.t_average( [ uvc[uv] for uv in m0.opt_get_f(uf).lu_v ] ) ) for uf in suf0 ) )
        ufc.update( dict( ( uf, Vec3f.t_average( [ uvc[uv] for uv in m1.opt_get_f(uf).lu_v ] ) ) for uf in suf1 ) )
    elif scheme == 'normal':
        uvc = dict( (uv, color_scheme['=']) for uv in suv0|suv1 )
        ufc = dict( (uv, color_scheme['=']) for uv in suf0|suf1 )
    else:
        print 'unknown color scheme: %s' % scheme
        exit(1)
    
    ma,mb = m0.clone(),m1.clone()
    for v in ma.lv: v.c = uvc[v.u_v]
    for v in mb.lv: v.c = uvc[v.u_v]
    ma.trim().recalc_vertex_normals()
    mb.trim().recalc_vertex_normals()
    
    md,me = Mesh(),Mesh()
    for f in m0.lf:
        u_f = f.u_f
        c = ufc[u_f]
        lv = [ m0.opt_get_v(u_v).copy().recolor(c) for u_v in f.lu_v ]
        lu_v = [ v.u_v for v in lv ]
        md.lv += lv
        md.lf += [ Face(u_f=u_f, lu_v=lu_v) ]
    for f in m1.lf:
        u_f = f.u_f
        c = ufc[u_f]
        lv = [ m1.opt_get_v(u_v).copy().recolor(c) for u_v in f.lu_v ]
        lu_v = [ v.u_v for v in lv ]
        me.lv += lv
        me.lf += [ Face(u_f=u_f, lu_v=lu_v) ]
    
    sl_v_lv,sl_v_lc = [],[]
    sl_l_lv,sl_l_lc = [],[]
    for v in m0.lv:
        if v.u_v not in suv1:
            sl_v_lv += v.p
            sl_v_lc += [ 1.0, 0.0, 0.0 ]
        else:
            sl_l_lv += v.p
            sl_l_lv += m1.opt_get_v(v.u_v).p
            sl_l_lc += [ 0.5, 0.5, 0.0 ]
            sl_l_lc += [ 0.5, 0.5, 0.5 ]
            
            #sl_v_lv += v.p
            #sl_v_lv += m1.opt_get_v(v.u_v).p
            #sl_v_lc += [ 0.5, 0.5, 0.5 ]
            #sl_v_lc += [ 0.5, 0.5, 0.5 ]
    for v in m1.lv:
        if v.u_v not in suv0:
            sl_v_lv += v.p
            sl_v_lc += [ 0.0, 1.0, 0.0 ]
    sl_l = Shape( GL_LINES, sl_l_lv, sl_l_lc, use_lights=False ) if sl_l_lv else None
    sl_v = Shape( GL_POINTS, sl_v_lv, sl_v_lc, use_lights=False ) if sl_v_lv else None
    sl = Shape_Combine( [sl_l,sl_v] )
    
    DebugWriter.end()
    
    DebugWriter.start( 'generating motion meshes' )
    
    steps = 20
    mf = [ Mesh() for x in xrange(steps) ]
    cv = 0
    for u_f in sufi:
        f0,f1 = m0.opt_get_f(u_f),m1.opt_get_f(u_f)
        if len(f0.lu_v) != len(f1.lu_v): continue
        #if not all( u_v0 in suv1 for u_v0 in f0.lu_v ): continue
        #if not all( u_v1 in suv0 for u_v1 in f1.lu_v ): continue
        if set(f0.lu_v) != set(f1.lu_v): continue
        clv = len(f0.lu_v)
        l_v0 = m0.opt_get_uf_lv( u_f )
        l_v1 = m1.opt_get_uf_lv( u_f )
        for i,m in enumerate(mf):
            t = float(i) / float(steps-1)
            for j in xrange(clv):
                #p = Vec3f.t_lerp( l_v0[j].p, l_v1[j].p, t )
                #lp = Vec3f.t_distance( l_v0[j].p, l_v1[j].p )
                j1 = m1._opt_get_i_v( l_v0[j].u_v )
                v0 = l_v0[j]
                v1 = m1.lv[j1]
                
                p = Vec3f.t_lerp( v0.p, v1.p, t )
                lp = Vec3f.t_distance( v0.p, v1.p ) * 0.5
                c = [ min(1.0,lp*2.5), min(1.0,lp*10.0), min(lp*25.0,1.0) ]
                m.lv.append( Vertex( p=p, c=c ) )
            m.lf.append( Face( lu_v=[ m.lv[j].u_v for j in xrange(cv,cv+clv) ]) )
        cv += clv
    for i in xrange(steps):
        mf[i].recalc_vertex_normals()
    
    DebugWriter.end()   # generating motion meshes
    
    DebugWriter.end()   # generating meshes
    
    DebugWriter.start( 'converting to shapes' )
    shapes = [ mesh.toShape( smooth=False ) if mesh else None for mesh in [ ma,mb,None, md,me,None ] ]
    shapes[2] = Shape_Cycle( [ m.toShape( smooth=False ) for m in mf ], fps=5, bounce=True )
    shapes[5] = sl
    DebugWriter.end()
    
    window = Viewer( shapes, caption='MeshGit Align Viewer: Methods="%s"' % ','.join(ci.lmethods), ncols=3 )
    window.app_run()

def run_match_viewer2_methods( fn0, fn1, *lmethods, **dparms ):
    scheme = dparms.get( 'scheme', 'muted_verts' )
    maxiters = int(dparms['maxiters']) if 'maxiters' in dparms else None
    
    m0,m1 = [ Mesh.fromPLY( fn ) for fn in [fn0,fn1] ]
    scaling_factor = scale( [ m0, m1 ] )
    m0.optimize()
    m0.rebuild_edges()
    
    suv0,suf0 = set(m0.get_lu_v()),set(m0.get_lu_f())
    suv1,suf1 = set(m1.get_lu_v()),set(m1.get_lu_f())
    #uc = dict( (u,random_color_primaries()) for u in suv0|suf0 )
    
    if scheme == 'random':
        uc = dict( ( u, random_color_primaries() ) for u in suv0 | suf0 | suv1 | suf1 )
    elif scheme == 'position':
        uc = dict()
        
        mp,Mp = m0.get_vert_bounding_box()
        mM = Vec3f.t_sub( Mp, mp )
        
        for u_v in suv0:
            p = m0.opt_get_v( u_v ).p
            uc[u_v] = Vec3f.t_div( Vec3f.t_sub( p, mp ), mM )
        for u_f in suf0:
            f = m0.opt_get_f( u_f )
            lp = [ v.p for v in m0.opt_get_f_lv( f ) ]
            p = Vec3f.t_average( lp )
            uc[u_f] = Vec3f.t_div( Vec3f.t_sub( p, mp), mM )
        for u_v in suv1:
            p = m1.opt_get_v( u_v ).p
            uc[u_v] = Vec3f.t_div( Vec3f.t_sub( p, mp ), mM )
        for u_f in suf1:
            f = m1.opt_get_f( u_f )
            lp = [ v.p for v in m1.opt_get_f_lv( f ) ]
            p = Vec3f.t_average( lp )
            uc[u_f] = Vec3f.t_div( Vec3f.t_sub( p, mp), mM )
    elif scheme == 'verts':
        uc = dict( (uv, random_color_primaries()) for uv in suv0|suv1 )
        uc.update( dict( ( uf, Vec3f.t_average( [ uc[uv] for uv in m0.opt_get_f(uf).lu_v ] ) ) for uf in suf0 ) )
        uc.update( dict( ( uf, Vec3f.t_average( [ uc[uv] for uv in m1.opt_get_f(uf).lu_v ] ) ) for uf in suf1 ) )
    elif scheme == 'muted_verts':
        uc = dict( (uv, Vec3f.t_lerp( color_scheme['='], random_color_primaries(), 0.40 )) for uv in suv0|suv1 )
        uc.update( dict( ( uf, Vec3f.t_average( [ uc[uv] for uv in m0.opt_get_f(uf).lu_v ] ) ) for uf in suf0 ) )
        uc.update( dict( ( uf, Vec3f.t_average( [ uc[uv] for uv in m1.opt_get_f(uf).lu_v ] ) ) for uf in suf1 ) )
    elif scheme == 'normal':
        uc = dict( (uv, color_scheme['=']) for uv in suv0|suv1|suf0|suf1 )
    else:
        print 'unknown color scheme: %s' % scheme
        exit(1)
    
    ma = Mesh()
    for u_f in suf0:
        f = m0.opt_get_f( u_f )
        c = uc[u_f]
        lv = [ m0.opt_get_v(u_v).copy().recolor(c) for u_v in f.lu_v ]
        lu_v = [ v.u_v for v in lv ]
        ma.lv += lv
        ma.lf += [ Face(u_f=u_f, lu_v=lu_v) ]
    
    s0,s1 = m0.toShape(),m1.toShape()
    
    lsl_f0 = [ s0 ]
    lsl_f1 = [ s1 ]
    lsl_f2 = [ s0 ]
    lsl_f3 = [ ma.toShape() ]
    lsl_v  = [ Shape_Combine( [ s0, s1 ] ) ]
    
    for i,method in enumerate(lmethods):
        ci = CorrespondenceInfo.build( m0, m1, method=method, maxiters=maxiters )
        m1m = ci.get_corresponding_mesh1().optimize().rebuild_edges()
        m1n = ci.get_corresponding_mesh1( allow_face_sub=False ).optimize().rebuild_edges()
        
        #scale( [m0,m1], 1.0 / scaling_factor )
        #for m in [m0,m1]: m.optimize()
        
        suv1,suf1 = set(m1m.get_lu_v()),set(m1m.get_lu_f())
        suv1m,suf1m = set(m1m.get_lu_v()),set(m1m.get_lu_f())
        suv1n,suf1n = set(m1n.get_lu_v()),set(m1n.get_lu_f())
        suvu,suvi = suv0|suv1, suv0&suv1
        sufu,sufi = suf0|suf1, suf0&suf1
        
        suv01n = suv0&suv1n
        
        #m1n.lf = [ f for f in m1n.lf if f.u_f in suf0 and all( u_v in suv01 for u_v in f.lu_v ) ]
        #m1n.trim()
        #for v in m1n.lv:
        #    v.p = m0.opt_get_v(v.u_v).p
        #lsl_f0 += [ m1n.toShape() ]
        
        suf01_0 = set( u_f for u_f in suf1n if u_f in suf0 and all( u_v in suv01n for u_v in m1n.opt_get_f(u_f).lu_v ) )
        
        mb,mc = Mesh(),Mesh()
        mbf,mcf = Mesh(),Mesh()
        
        for u_f in suf01_0:
            f = m0.opt_get_f( u_f )
            c = uc[u_f]
            
            lv = [ m0.opt_get_v(u_v).copy().recolor(c) for u_v in f.lu_v ]
            mb.lv += lv
            mb.lf += [ Face(u_f=u_f, lu_v=[v.u_v for v in lv]) ]
            
            lv = [ m1n.opt_get_v(u_v).copy().recolor(c) for u_v in f.lu_v ]
            mc.lv += lv
            mc.lf += [ Face(u_f=u_f, lu_v=[v.u_v for v in lv]) ]
            
            #lv = [ m0.opt_get_v(u_v).copy().recolor(c) for u_v in f.lu_v ]
            #mbf.lv += lv
            #mbf.lf += [ Face(u_f=u_f, lu_v=[v.u_v for v in lv]) ]
            #lv = [ m1m.opt_get_v(u_v).copy().recolor(c) for u_v in f.lu_v ]
            #mcf.lv += lv
            #mcf.lf += [ Face(u_f=u_f, lu_v=[v.u_v for v in lv]) ]
        
        c = color_scheme['-'] # [ 0.5, 0.1, 0.0 ]
        for u_f in suf0 - suf01_0:
            f = m0.opt_get_f( u_f )
            lv = [ m0.opt_get_v(u_v).copy().recolor(c) for u_v in f.lu_v ]
            mb.lv += lv
            mb.lf += [ Face(u_f=u_f, lu_v=[v.u_v for v in lv]) ]
        
        c = color_scheme['+'] # [ 0.0, 0.5, 0.1 ]
        for u_f in suf1n - suf01_0:
            f = m1n.opt_get_f( u_f )
            lv = [ m1n.opt_get_v(u_v).copy().recolor(c) for u_v in f.lu_v ]
            mc.lv += lv
            mc.lf += [ Face(u_f=u_f, lu_v=[v.u_v for v in lv]) ]
        
        cd = color_scheme['-']
        ca = color_scheme['+']
        cms = [0.15,0.15,0.15]
        cma = [0.1,0.1,0.1]
        for u_f in suf0:
            if u_f in uc and u_f in suf1m:
                cf = uc[u_f]
                if u_f not in suf01_0:
                    cf = Vec3f.t_sub( cf, cms )
                else:
                    cf = Vec3f.t_add( cf, cma )
            else: cf = cd
            f = m0.opt_get_f( u_f )
            lv = [ m0.opt_get_v(u_v).copy().recolor(cf) for u_v in f.lu_v ]
            mbf.lv += lv
            mbf.lf += [ Face(u_f=u_f, lu_v=[v.u_v for v in lv]) ]
        for u_f in suf1m:
            if u_f in uc:
                cf = uc[u_f]
                if u_f not in suf01_0:
                    cf = Vec3f.t_sub( cf, cms )
                else:
                    cf = Vec3f.t_add( cf, cma )
            else: cf = ca
            f = m1m.opt_get_f( u_f )
            lv = [ m1m.opt_get_v(u_v).copy().recolor(cf) for u_v in f.lu_v ]
            mcf.lv += lv
            mcf.lf += [ Face(u_f=u_f, lu_v=[v.u_v for v in lv]) ]
        
        sl_v_lv,sl_v_lc,sl_v_ln = [],[],[]
        sl_l_lv,sl_l_lc,sl_l_ln = [],[],[]
        sl_e_lv,sl_e_lc,sl_e_ln = [],[],[]
        
        for u_v in suv0&suv1:
            v0 = m0.opt_get_v(u_v)
            v1 = m1m.opt_get_v(u_v)
            sl_l_lv += v0.p
            sl_l_lv += v1.p
            sl_l_lc += [ 0.2, 0.2, 0.8, 0.6, 0.6, 0.8 ]
            sl_l_ln += v0.n + v1.n
        
        for u_v in suv0-suv1:
            v = m0.opt_get_v(u_v)
            sl_v_lv += v.p
            sl_v_lc += color_scheme['-'] #[ 0.8, 0.2, 0.0 ]
            sl_v_ln += v.n
        for u_v in suv1-suv0:
            v = m1m.opt_get_v(u_v)
            sl_v_lv += v.p
            sl_v_lc += color_scheme['+'] #[ 0.0, 0.8, 0.2 ]
            sl_v_ln += v.n
        
        for e in m0.le:
            u_v0,u_v1 = e.lu_v
            sl_e_lv += m0.opt_get_v(u_v0).p
            sl_e_lv += m0.opt_get_v(u_v1).p
            sl_e_lc += [ 0.9, 0.9, 0.9, 0.9, 0.9, 0.9 ]
            sl_e_ln += m0.opt_get_v(u_v0).n
            sl_e_ln += m0.opt_get_v(u_v1).n
        for e in m1n.le:
            u_v0,u_v1 = e.lu_v
            sl_e_lv += m1n.opt_get_v(u_v0).p
            sl_e_lv += m1n.opt_get_v(u_v1).p
            sl_e_lc += [ 0.9, 0.9, 0.9, 0.9, 0.9, 0.9 ]
            sl_e_ln += m1n.opt_get_v(u_v0).n
            sl_e_ln += m1n.opt_get_v(u_v1).n
        
        sl_l = Shape( GL_LINES, sl_l_lv, sl_l_lc, sl_l_ln, line_width=2.0, use_lights=False )
        sl_v = Shape( GL_POINTS, sl_v_lv, sl_v_lc, sl_v_ln, use_lights=False ) if sl_v_lv else None
        sl_e = Shape( GL_LINES, sl_e_lv, sl_e_lc, sl_e_ln, line_width=0.1, use_lights=False )
        
        #lmoving = []
        #for istep in xrange(21):
            #p = float(istep) / 20.0
            #sl_v_lv,sl_v_lc = [],[]
            #for u_v in suv0&suv1:
                #v0 = m0.opt_get_v(u_v)
                #v1 = m1m.opt_get_v(u_v)
                #sl_v_lv += Vec3f.t_lerp( v0.p, v1.p, p )
                #sl_v_lc += [ 0.75, 0.75, 0.75 ]
            #lmoving += [ Shape( GL_POINTS, sl_v_lv, sl_v_lc ) ]
        
        lsl_f0 += [ mb.toShape() ]
        lsl_f1 += [ mc.toShape() ]
        lsl_f2 += [ mbf.toShape() ]
        lsl_f3 += [ mcf.toShape() ]
        lsl_v += [ Shape_Combine( [ sl_l, sl_v, sl_e ] ) ] #, Shape_Cycle(lmoving,fps=5,bounce=True)] ) ]
    
    lshapes = lsl_f0 + lsl_f1 + lsl_f2 + lsl_f3 + lsl_v
    lcaps   = ['orig'] + list(lmethods) + ['derivative']
    ncols   = len( lsl_f0 )
    
    window = Viewer( lshapes, caption='MeshGit Match Viewer', lcaptions=lcaps, ncols=ncols, size=(200,200) )
    window.app_run()

def run_match_viewer3( fn0, fn1, fn2, method=None, maxiters=None ):
    m0,m1,m2 = [ Mesh.fromPLY( fn ) for fn in [fn0,fn1,fn2] ]
    scaling_factor = scale( [ m0, m1, m2 ] )
    ci01 = CorrespondenceInfo.build( m0, m1, method, maxiters=maxiters )
    ci02 = CorrespondenceInfo.build( m0, m2, method, maxiters=maxiters )
    m1 = ci01.get_corresponding_mesh1( )
    m2 = ci02.get_corresponding_mesh1( )
    scale( [m0,m1,m2], 1.0 / scaling_factor )
    
    m0.optimize()
    m1.optimize()
    m2.optimize()
    
    suv0,suv1,suv2 = set(m0.get_lu_v()),set(m1.get_lu_v()),set(m2.get_lu_v())
    suf0,suf1,suf2 = set(m0.get_lu_f()),set(m1.get_lu_f()),set(m2.get_lu_f())
    suvu,suvi = suv0|suv1|suv2, suv0&suv1&suv2
    sufu,sufi = suf0|suf1|suf2, suf0&suf1&suf2
    
    uvc = dict( (uv,get_color(uv,suv0,suv1,suv2)) for uv in suvu )
    ufc = dict( (uf,get_color(uf,suf0,suf1,suf2)) for uf in sufu )
    
    ma,mb,mc = m0.clone(),m1.clone(),m2.clone()
    for v in ma.lv: v.c = uvc[v.u_v]
    for v in mb.lv: v.c = uvc[v.u_v]
    for v in mc.lv: v.c = uvc[v.u_v]
    ma.trim().recalc_vertex_normals()
    mb.trim().recalc_vertex_normals()
    mc.trim().recalc_vertex_normals()
    
    md,me,mf = Mesh(),Mesh(),Mesh()
    for f in m0.lf:
        u_f = f.u_f
        c = ufc[u_f]
        lv = [ m0.opt_get_v(u_v).copy().recolor(c) for u_v in f.lu_v ]
        lu_v = [ v.u_v for v in lv ]
        md.lv += lv
        md.lf += [ Face(u_f=u_f, lu_v=lu_v) ]
    for f in m1.lf:
        u_f = f.u_f
        c = ufc[u_f]
        lv = [ m1.opt_get_v(u_v).copy().recolor(c) for u_v in f.lu_v ]
        lu_v = [ v.u_v for v in lv ]
        me.lv += lv
        me.lf += [ Face(u_f=u_f, lu_v=lu_v) ]
    for f in m2.lf:
        u_f = f.u_f
        c = ufc[u_f]
        lv = [ m2.opt_get_v(u_v).copy().recolor(c) for u_v in f.lu_v ]
        lu_v = [ v.u_v for v in lv ]
        mf.lv += lv
        mf.lf += [ Face(u_f=u_f,lu_v=lu_v) ]
    #md.lf = [ f for f in m3.lf if 
    
    shapes = [ mesh.toShape( smooth=False ) if mesh else None for mesh in [ mb,ma,mc, me,md,mf ] ]
    window = Viewer( shapes, caption='MeshGit Align Viewer', ncols=3 )
    window.app_run()





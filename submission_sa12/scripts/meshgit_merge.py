import itertools

from vec3f import *
from mesh import *
from shapes import *
from viewer import *

from correspondenceinfo import *

from meshgit_misc import *
from meshgit_med_ops import *


# NOTE: this will only merge not-overlapping edits
def run_merge( fn0, fn1, fn2, fn012 ):
    # load 3 vers
    m0 = Mesh.fromPLY( fn0 )
    m1 = Mesh.fromPLY( fn1 )
    m2 = Mesh.fromPLY( fn2 )
    
    m1a = Mesh.bc_closestverts( m0, m1 )
    m2a = Mesh.bc_closestverts( m0, m2 )
    
    # compute diffs
    #c01 = diff_commands( m0, m1a )
    #c02 = diff_commands( m0, m2a )
    llc01v,llc01f = diff_commands_by_partition( m0, m1a )
    llc02v,llc02f = diff_commands_by_partition( m0, m2a )
    c01,c02 = [],[]
    c01.extend( llc01v['uvm'] )
    c01.extend( llc01v['uva'] )
    c02.extend( llc02v['uvm'] )
    c02.extend( llc02v['uva'] )
    for lc01 in llc01f:
        overlap = False
        for lc02 in llc02f:
            if lc01['sf'] & lc02['sf']:
                overlap = True
                break
        if overlap: continue
        for k,v in lc01.items():
            if k != 'sf': c01.extend(v)
    for lc02 in llc02f:
        overlap = False
        for lc01 in llc01f:
            if lc01['sf'] & lc02['sf']:
                overlap = True
                break
        if overlap: continue
        for k,v in lc02.items():
            if k != 'sf': c02.extend(v)
    c01.extend( llc01v['uvd'] )
    c02.extend( llc02v['uvd'] )
    
    print()
    print( c01 )
    print()
    print( c02 )
    print()
    
    # apply diffs to ancestor
    m012 = m0.clone()  # create exact copy of m0 to edit
    do_commands( m012, c01 )
    do_commands( m012, c02 )
    
    # save merged mesh to file
    m012.toPLY( fn012 )


kp = None
def run_merge_viewer( fn0, fn1, fn2, fn012, **opts ):
    method = opts.get('method',None)
    subd = int(opts.get('subd',0))
    
    global kp
    
    m0,ma,mb = [ Mesh.fromPLY( fn ) for fn in [fn0,fn1,fn2] ]
    scaling_factor = scale( [ m0, ma, mb ] )
    ci0a = CorrespondenceInfo.build( m0, ma, method=method )
    ci0b = CorrespondenceInfo.build( m0, mb, method=method )
    scale( [ m0, ma, mb ], scaling_factor=(1.0/scaling_factor) )
    ma = ci0a.get_corresponding_mesh1( allow_face_sub=False )
    mb = ci0b.get_corresponding_mesh1( allow_face_sub=False )
    
    for m in [m0,ma,mb]:
        m.optimize()
        m.rebuild_edges()
    
    llc0av,llc0af = diff_commands_by_partition( m0, ma )
    llc0bv,llc0bf = diff_commands_by_partition( m0, mb )
    
    c0a,c0b = llc0av['uvm']+llc0av['uva'],llc0bv['uvm']+llc0bv['uva']
    #c0a.extend( lc0a['ufd']+lc0a['ufa'] if not any(lc0a['sf'] & lc0b['sf'] for lc0b in llc0b) for lc0a in llc0a )
    #c0b.extend( lc0b['ufd']+lc0b['ufa'] if not any(lc0a['sf'] & lc0b['sf'] for lc0a in llc0a) for lc0b in llc0b )
    
    # if two edits conflict, show to user
    w,h=len(llc0af),len(llc0bf)
    x,y=w-1,h-1
    
    mconflicts = [ [ 1 if (lc0a['sf'] & lc0b['sf']) else 0 for lc0b in llc0bf ] for lc0a in llc0af ]
    choosea = [ not any(lc0a['sf'] & lc0b['sf'] for lc0b in llc0bf) for lc0a in llc0af ]
    chooseb = [ not any(lc0a['sf'] & lc0b['sf'] for lc0a in llc0af) for lc0b in llc0bf ]
    
    DebugWriter.start( 'commands' )
    DebugWriter.printer( 'llc0av: %s'% llc0av )
    DebugWriter.printer( 'llc0af: %s'% llc0av )
    DebugWriter.printer( 'llc0bv: %s'% llc0bv )
    DebugWriter.printer( 'llc0bf: %s'% llc0bv )
    DebugWriter.start( 'conflicts' )
    DebugWriter.printer( '\n'.join( ''.join( 'X' if c else '.' for c in mcs ) for mcs in mconflicts ) )
    DebugWriter.qend()
    DebugWriter.qend()
    
    m012 = m0.clone()  # create exact copy of m0 to edit
    do_commands( m012, llc0av['uva'] + llc0bv['uva'] + llc0av['uvm'] + llc0bv['uvm'] )
    for i,lc0a in enumerate(llc0af):
        if not any( mconflicts[i] ):
            do_commands( m012, lc0a['ufd'] + lc0a['ufa'] + lc0a['ufm'] )
    for i,lc0b in enumerate(llc0bf):
        if not any( mconflicts[r][i] for r in xrange(w) ):
            do_commands( m012, lc0b['ufd'] + lc0b['ufa'] + lc0b['ufm'] )
    
    window = Viewer( None, ncols=3, caption='MeshGit Merge Viewer', on_key_callback=merge_on_key )
    #window.modelscale = scaling_factor
    
    saveresult = True
    while any( any(r) for r in mconflicts ):
        x = (x + 1) % w
        if x == 0: y = (y + 1) % h
        
        if not mconflicts[x][y]: continue
        
        lc0a = llc0af[x]
        ma2 = do_commands( m0.clone(), lc0a['ufd'] ).trim() #+llc0av['uvm']
        ma3 = do_commands( m0.clone(clone_faces=False), llc0av['uva']+lc0a['ufa']+llc0av['uvm'] ).trim().recolor([0.0,0.75,0.4])
        su_dfa = set([ cmd.split(' ')[1].split(':')[0] for cmd in lc0a['ufd'] ])
        su_afa = set([ cmd.split(' ')[1].split(':')[0] for cmd in lc0a['ufa'] ])
        su_cfa = set(m0.get_lu_f()) - su_dfa
        
        lc0b = llc0bf[y]
        mb2 = do_commands( m0.clone(), lc0b['ufd'] ).trim() #+llc0bv['uvm']
        mb3 = do_commands( m0.clone(clone_faces=False), llc0bv['uva']+lc0b['ufa']+llc0bv['uvm'] ).trim().recolor([0.4,0.75,0.0])
        su_dfb = set([ cmd.split(' ')[1].split(':')[0] for cmd in lc0b['ufd'] ])
        su_afb = set([ cmd.split(' ')[1].split(':')[0] for cmd in lc0b['ufa'] ])
        su_cfb = set(m0.get_lu_f()) - su_dfb
            
        m0c0 = m0.clone().filter_faces( su_dfa | su_dfb, False ).trim()
        m0ca = m0.clone().filter_faces( su_dfa, True ).filter_faces( su_dfb, False ).trim().recolor([ 0.75, 0.00, 0.40 ])
        m0cb = m0.clone().filter_faces( su_dfb, True ).filter_faces( su_dfa, False ).trim().recolor([ 0.75, 0.40, 0.00 ])
        
        DebugWriter.report( 'conflicts', su_dfa & su_dfb )
        
        m0cc0 = m0.clone().filter_faces( su_dfa & su_dfb, True ).trim().recolor([ 0.50, 0.00, 0.00 ])
        m0cc1 = m0.clone().filter_faces( su_dfa & su_dfb, True ).trim().recolor([ 0.60, 0.22, 0.00 ])
        m0cc2 = m0.clone().filter_faces( su_dfa & su_dfb, True ).trim().recolor([ 0.70, 0.45, 0.00 ])
        m0cc3 = m0.clone().filter_faces( su_dfa & su_dfb, True ).trim().recolor([ 0.80, 0.67, 0.00 ])
        m0cc4 = m0.clone().filter_faces( su_dfa & su_dfb, True ).trim().recolor([ 0.90, 0.90, 0.00 ])
        
        for m in [m0ca,m0cb]:
            m.rebuild_edges()
        
        m0c0b = m0c0.extend(m0ca).extend(m0cb)
        
        for m in [ma2,ma3,mb2,mb3,m0c0b,m0cc0,m0cc1,m0cc2,m0cc3,m0cc4]:
            m.rebuild_edges()
        
        shapes = [
            None, m012.toShape(), m0cc0.toShape(),
            ma2.extend(ma3).toShape(),
            Shape_Combine( [
                m0c0b.toShape(),
                Shape_Cycle( [
                    m0cc0.toShape(),
                    m0cc1.toShape(),
                    m0cc2.toShape(),
                    m0cc3.toShape(),
                    m0cc4.toShape(),
                    ], 20, True ),
                ] ),
            mb2.extend(mb3).toShape()
            ]
        
        lcaptions = [
            'Resolve the highlighted conflict:\n1: ver a\n2: ver b\n3: ver 0\n4: merge by hand\n0: next',
            'merged model',
            'the conflicting faces (only)',
            '1: version a',
            '3: version 0',
            '2: version b',
            ]
        
        window.setup( shapes, ncols=3, lcaptions=lcaptions, caption='MeshGit: Resolve Conflict' )
        window.app_run()
        if window.has_exit or window.context is None:
            print( 'quitting!' )
            return 1
        
        if kp == 1:
            print( 'ver a' )
            choosea[x] = True
            do_commands( m012, llc0af[x]['ufd'] + llc0af[x]['ufa'] )
            for i in xrange(h):
                if mconflicts[x][i] == 0: continue
                mconflicts[x][i] = 0
            for i in xrange(w):
                if mconflicts[i][y] == 0: continue
                mconflicts[i][y] = 0
                if not any(mconflicts[i][i2] for i2 in xrange(h)):
                    choosea[i] = True
                    do_commands( m012, llc0af[i]['ufd'] + llc0af[i]['ufa'] )
        
        elif kp == 2:
            print( 'ver b' )
            chooseb[y] = True
            do_commands( m012, llc0bf[y]['ufd'] + llc0bf[y]['ufa'] )
            for i in xrange(w):
                if mconflicts[i][y] == 0: continue
                mconflicts[i][y] = 0
            for i in xrange(h):
                if mconflicts[x][i] == 0: continue
                mconflicts[x][i] = 0
                if not any(mconflicts[i2][i] for i2 in xrange(w)):
                    chooseb[i] = True
                    do_commands( m012, llc0bf[i]['ufd'] + llc0bf[i]['ufa'] )
        
        elif kp == 3:
            print( 'ver 0' )
            for i in xrange(w): mconflicts[i][y] = 0
            for i in xrange(h): mconflicts[x][i] = 0
            #for i in xrange(w):
                #if mconflicts[i][y] == 0: continue
                #if not any(mconflicts[i][i2] for i2 in xrange(h)):
                    #choosea[i] = True
                    #do_commands( m012, llc0af[i]['ufd'] + llc0af[i]['ufa'] )
            #for i in xrange(h):
                #if mconflicts[x][i] == 0: continue
                #if not any(mconflicts[i2][i] for i2 in xrange(w)):
                    #chooseb[i] = True
                    #do_commands( m012, llc0bf[i]['ufd'] + llc0bf[i]['ufa'] )
        
        elif kp == 4:
            print( 'merge-by-hand' )
            
            m0_ = m0.clone()
            m0a_ = m0.clone()
            m0b_ = m0.clone()
            m0ab = m0.clone()
            for lc0a in llc0af:
                m0_ = do_commands( m0_, lc0a['ufd'] )
                m0a_ = do_commands( m0a_, lc0a['ufd'] )
            for lc0b in llc0bf:
                m0_ = do_commands( m0_, lc0b['ufd'] )
                m0b_ = do_commands( m0b_, lc0b['ufd'] )
            m0_.trim()
            m0a_.trim()
            m0b_.trim()
            
            ma_ = ma.clone( clone_faces=False )
            ma_ = do_commands( ma_, llc0av['uva'] )
            m0ab = do_commands( m0ab, llc0av['uva'] )
            for lc0a in llc0af:
                ma_ = do_commands( ma_, lc0a['ufa'] )
                m0ab = do_commands( m0ab, lc0a['ufa'] )
            ma_.trim()
            
            mb_ = mb.clone( clone_faces=False )
            mb_ = do_commands( mb_, llc0bv['uva'] )
            m0ab = do_commands( m0ab, llc0bv['uva'] )
            for lc0b in llc0bf:
                mb_ = do_commands( mb_, lc0b['ufa'] )
                m0ab = do_commands( m0ab, lc0b['ufa'] )
            mb_.trim()
            
            m0ab.trim()
            
            scale( [m0,ma,mb,m0_,ma_,mb_,m0a_,m0b_,m0ab], 1.0/scaling_factor )
            m0.toPLY( 'm0.ply' )
            ma.toPLY( 'ma.ply' )
            mb.toPLY( 'mb.ply' )
            m0_.toPLY( 'm0_.ply' )
            ma_.toPLY( 'ma_.ply' )
            mb_.toPLY( 'mb_.ply' )
            m0a_.toPLY( 'm0a_.ply' )
            m0b_.toPLY( 'm0b_.ply' )
            m0ab.toPLY( 'm0ab.ply' )
            
            if os.path.exists( 'm0ab_.ply' ):
                os.unlink( 'm0ab_.ply' )
            
            subprocess.Popen( ['./merge_blender.sh'] ).communicate()
            
            if os.path.exists( 'm0ab_.ply' ):
                m012 = Mesh.fromPLY( 'm0ab_.ply' )
                saveresult = False
                break
            else:
                x -= 1
                scale( [m0,ma,mb], scaling_factor )
        
        else:
            print( 'skipping' )
    
    if kp != 4:
        for c,lc0a in zip(choosea,llc0af):
            if not c: continue
            c0a.extend( lc0a['ufd']+lc0a['ufa'] )
        for c,lc0b in zip(chooseb,llc0bf):
            if not c: continue
            c0b.extend( lc0b['ufd']+lc0b['ufa'] )
        #c0a.extend( llc0av['uvd'] )
        #c0b.extend( llc0bv['uvd'] )
        
        # apply diffs to ancestor
        m012 = m0.clone()  # create exact copy of m0 to edit
        do_commands( m012, c0a )
        do_commands( m012, c0b )
        m012.trim()
        
        #scale( [m012], 1.0/scaling_factor )
    
    m012.toPLY( fn012 ) # save merged mesh to file
    
    suvm = set([ cmd.split(' ')[1].split(':')[0] for cmd in (llc0av['uvm'] + llc0bv['uvm']) if cmd.startswith('move') ])
    
    #scale( [m0,ma,mb], (1.0/scaling_factor) )
    
    scaling_factor = scale( [m0,m012], scaling_factor='unitcube' )
    su_v0 = set( m0.get_lu_v() )
    dvd = dict( [ ( v.u_v, Vec3f.t_distance(v.p,m0.opt_get_v(v.u_v).p) ) for v in m012.lv if v.u_v in su_v0 ] )
    scale( [m0,m012], (1.0/scaling_factor) )
    
    suf0,sufa,sufb = set(m0.get_lu_f()),set(ma.get_lu_f()),set(mb.get_lu_f())
    m012_0,m012_a,m012_b = [ m012.clone() for i in xrange(3) ]
    m012_0.filter_faces(suf0,True).trim().optimize()
    for v in m012_0.lv:
        if v.u_v in suvm:
            val = min( 1.0, dvd[v.u_v] * 25.0 )
            v.c = Vec3f.t_lerp( color_scheme['='], color_scheme['>'], val )
        else:
            v.c = color_scheme['=']
    m012_a.filter_faces(sufa-suf0,True).trim().recolor( color_scheme['+a'] ).optimize()
    m012_b.filter_faces(sufb-suf0,True).trim().recolor( color_scheme['+b'] ).optimize()
    m012_ = m012_0.extend(m012_a,ignore_edges=True).extend(m012_b,ignore_edges=True)
    m012_.trim().recalc_vertex_normals().optimize()
    
    (m0_c,ma_c,mb_c) = get_colored_meshes_3way( m0, ma, mb )
    
    lcaptions = [
        'subd' if subd else '',
        'merged',
        'merged (colored)',
        'version a',
        'original',
        'version b',
        ]
    
    window.setup( [
            m012.catmull_clark_subd(subd).toShape(smooth=True) if subd else None, m012.toShape(), m012_.toShape(),
            ma_c.toShape(), m0_c.toShape(), mb_c.toShape()
        ], ncols = 3, lcaptions=lcaptions, caption='MeshGit: Final Merge' )
    window.app_run()
    if not (window.has_exit or window.context is None):
        window.close()
    
    return 0

def merge_on_key( key ):
    global kp
    if key == '_1':     # ver a
        kp = 1
        return 1
    elif key == '_2':   # ver b
        kp = 2
        return 1
    elif key == '_3':   # ver 0 (neither)
        kp = 3
        return 1
    elif key == '_4':   # merge by hand
        kp = 4
        return 1
    elif key == '_0':   # skip
        kp = 5
        return 1
    kp = None
    return 0



def run_merge_combinations( fn0, fn1, fn2, **opts ):
    method = opts.get('method',None)
    #subd = int(opts.get('subd',0))
    
    DebugWriter.start( 'loading files', quiet=True )
    m0,ma,mb = [ Mesh.fromPLY( fn ) for fn in [fn0,fn1,fn2] ]
    DebugWriter.end()
    
    DebugWriter.start( 'matching', quiet=True )
    scaling_factor = scale( [ m0, ma, mb ] )
    ci0a = CorrespondenceInfo.build( m0, ma, method=method )
    ci0b = CorrespondenceInfo.build( m0, mb, method=method )
    scale( [ m0, ma, mb ], scaling_factor=(1.0/scaling_factor) )
    ma = ci0a.get_corresponding_mesh1( allow_face_sub=False )
    mb = ci0b.get_corresponding_mesh1( allow_face_sub=False )
    for m in [m0,ma,mb]: m.optimize().rebuild_edges()
    DebugWriter.end()
    
    DebugWriter.start( 'partitioning matches', quiet=True )
    llc0av,llc0af = diff_commands_by_partition( m0, ma )
    llc0bv,llc0bf = diff_commands_by_partition( m0, mb )
    DebugWriter.end()
    
    c0a,c0b = llc0av['uvm']+llc0av['uva'],llc0bv['uvm']+llc0bv['uva']
    w,h=len(llc0af),len(llc0bf)
    
    mconflicts = [ [ 1 if (lc0a['sf'] & lc0b['sf']) else 0 for lc0b in llc0bf ] for lc0a in llc0af ]
    lconflicts = [ (i_a,i_b) for i_a in xrange(w) for i_b in xrange(h) if mconflicts[i_a][i_b] ]
    
    # list of groups to apply, initialized with choosing all non-conflicted groups
    choosea = [ not any(lc0a['sf'] & lc0b['sf'] for lc0b in llc0bf) for lc0a in llc0af ]
    chooseb = [ not any(lc0a['sf'] & lc0b['sf'] for lc0a in llc0af) for lc0b in llc0bf ]
    n_c = sum( sum(mcs) for mcs in mconflicts )
    n_a,n_b = sum( 0 if a else 1 for a in choosea ),sum( 0 if b else 1 for b in chooseb )
    
    DebugWriter.start( 'conflicts of %d x %d partitions' % (w,h) )
    DebugWriter.printer( '  %s' % ( ''.join( '*' if c else ' ' for c in chooseb ) ) )
    DebugWriter.printer( '\n'.join( '%c %s' % ( '*' if choosea[i] else ' ', ''.join('X' if c else '.' for c in mcs)) for i,mcs in enumerate(mconflicts) ) )
    DebugWriter.report( 'loc', lconflicts )
    DebugWriter.report( 'count', '%d (%d,%d)' % (n_c,n_a,n_b) )
    DebugWriter.qend()
    
    DebugWriter.start( 'finding all possible combinations', quiet=True )
    # iterate over all combinations that are possible.  note: some that are not possible
    ll_choices = []
    l_choices = [ 0 for i in xrange(n_c) ]
    for i in xrange( 3**n_c ):
        
        conflicted = False
        for j in xrange(n_c):
            j_a,j_b = lconflicts[j]
            for k in xrange(n_c):
                if j == k: continue
                k_a,k_b = lconflicts[k]
                if (k_a == j_a or k_b == j_b) and l_choices[j] == l_choices[k]:
                    conflicted = True
                    break
            if conflicted: break
        if conflicted: continue
        
        ll_choices += [ list(l_choices) ]
        
        for j in xrange(n_c):
            l_choices[j] = (l_choices[j] + 1) % 3
            if l_choices[j] != 0: break
    DebugWriter.loud().report( 'combinations', ll_choices ).quiet()
    DebugWriter.end()
    
    DebugWriter.start( 'applying non-conflicting edits', quiet=True )                       # build base mesh containing all non-conflicted groups
    mcb = m0.clone()
    do_commands( mcb, llc0av['uva'] + llc0bv['uva'] + llc0av['uvm'] + llc0bv['uvm'] )
    for i,lc0a in enumerate(llc0af):
        if not any( mconflicts[i] ):
            do_commands( mcb, lc0a['ufd'] + lc0a['ufa'] + lc0a['ufm'] )
    for i,lc0b in enumerate(llc0bf):
        if not any( mconflicts[r][i] for r in xrange(w) ):
            do_commands( mcb, lc0b['ufd'] + lc0b['ufa'] + lc0b['ufm'] )
    DebugWriter.end()
    
    DebugWriter.start( 'building visualizations for versions', quiet=True )
    ls_combinations = [
        None,
        get_colored_mesh_3way( m0, ma, mb, ma ).toShape(),
        get_colored_mesh_3way( m0, ma, mb, m0, color_deletions=True ).toShape(),
        get_colored_mesh_3way( m0, ma, mb, mb ).toShape(),
        None,
        ]
    l_captions = [ '', 'version a', 'original', 'version b', '' ]
    DebugWriter.end()
    
    DebugWriter.start( 'building visualizations for conflict combinations', quiet=True )         # build shapes choosing through the conflicted groups
    for l_choices in ll_choices:
        mc = mcb.clone()
        for i,c in enumerate(l_choices):
            i_a,i_b = lconflicts[i]
            if c == 1:
                do_commands( mc, llc0af[i_a]['ufd'] + llc0af[i_a]['ufa'] )
            elif c == 2:
                do_commands( mc, llc0bf[i_b]['ufd'] + llc0bf[i_b]['ufa'] )
        
        mc = get_colored_mesh_3way( m0, ma, mb, mc )
        #if subd: mc = mc.catmull_clark_subd(subd).toShape(smooth=True)
        
        ls_combinations += [ mc.toShape() ]
        l_captions += [ ''.join( 'OAB'[c] for c in l_choices ) ]
    DebugWriter.end()
    
    window = Viewer( ls_combinations, ncols=5, caption='MeshGit Merge Conflict Combinations', lcaptions=l_captions )
    window.app_run()
    
    return 0










from meshgit_diff import *
from meshgit_merge import *

def run_git_diff( *lfn, **__ ):
    # path old_file old_hex old_mode new_file new_hex new_mode
    p = lfn[7]  # lfn[0] ?????
    f0 = os.path.join( p, lfn[1] )
    f1 = os.path.join( p, lfn[4] )
    run_diff_viewer2( f0, f1 )
    exit( 0 )

def run_git_merge( *lfn, **__ ):
    p = lfn[3]
    f0 = os.path.join( p, lfn[0] )
    fa = os.path.join( p, lfn[1] )
    fb = os.path.join( p, lfn[2] )
    sts = run_merge_viewer( f0, fa, fb, fa )
    exit( sts )

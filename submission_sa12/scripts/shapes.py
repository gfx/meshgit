import time, pickle, pyglet
from pyglet.gl import *

import ply
from vec3f import *


"""
in order to speed up using opengl through python, we'll "compile" the meshes to vertex lists

Shape               : wrapper for vertex list (all must be of same type. ex: lines, triangles, quads, etc)
Shape_Cycle         : takes list of shapes and cycles through them (simple, static animations)
Shape_Selectable    : allows vertices of shape to be 'selected'  (crappy hack!)
Shape_Combine       : a union of shapes in a list

NOTE: Shape is not the most memory-efficient way to store meshes, as vertex position and color may be duplicated
      this class is mostly designed for quick-and-dirty development

"""

def calc_vert_normals( lvp, llvi ):
    lfn = [ calc_face_normal( lvp, lvi ) for lvi in llvi ]
    lvfi = [ [] for i_v in xrange(len(lvp)) ]
    for i_f,lvi in enumerate(llvi):
        for i_v in lvi: lvfi[i_v] += [i_f]
    lvn = [ Vec3f.t_norm( Vec3f.t_average( [lfn[i_f] for i_f in li_f] ) ) for li_f in lvfi ]
    return lvn

def calc_face_normal( lvp, lvi ):
    norm = ( 0.0, 0.0, 0.0 )
    l = len(lvi)
    for i in xrange(l):
        i0,i1,i2 = lvi[i],lvi[(i+1)%l],lvi[(i+2)%l]
        v10 = Vec3f.t_sub(lvp[i0],lvp[i1])
        v12 = Vec3f.t_sub(lvp[i2],lvp[i1])
        _norm = Vec3f.t_norm( Vec3f.t_cross( v12,v10 ) )
        norm = Vec3f.t_add( norm, _norm )
    return Vec3f.t_norm( norm )



class UpdatableIndexedShape(object):
    
    def __init__( self, shapetype, indices, verts, colors, norms=None ):
        self.setdata( shapetype, indices, verts, colors, norms )
    
    @classmethod
    def from_ply( cls, fn, color=None ):
        # two lists below must correspond
        ltype = [ GL_POINTS, GL_LINES, GL_TRIANGLES, GL_QUADS ]
        lsize = [ 1, 2, 3, 4 ]
        
        if color == None: color = [ 1.0, 1.0, 1.0 ]
        
        # load object
        print( 'loading %s...' % fn )
        o = ply.ply_load( fn )
        
        lv = [ c for v in o['lv'] for c in v ]  # serialize positional data
        lc = color * len(o['lv'])               # color the verts
        llf = [ [] for t in ltype ]             # vert inds categorized by 'face' size ( pt, ln, tri, quad )
        ls = [ None for t in ltype ]            # list of shapes (one for each 'face' size) to be unioned
        
        # categorize 'face' sizes
        for f in o['lf']:
            assert len(f) in lsize, "unsupported face size %d" % len(f)
            llf[ lsize.index(len(f)) ] += f
        
        # create shapes
        for i,lvi in enumerate(llf):
            if len(lvi) == 0: continue
            t = ltype[i]
            ls[i] = UpdatableIndexedShape( t, lvi, lv, lc )
        
        return Shape_Combine( ls )
    @classmethod
    def from_obj( cls, lf, lvp, lvc=None, lvn=None, add_edges=None ):
        # two lists below must correspond
        ltype = [ GL_POINTS, GL_LINES, GL_TRIANGLES, GL_QUADS ]
        lsize = [ 1, 2, 3, 4 ]
        
        if lvc == None: lvc = [ [ 1.0, 1.0, 1.0 ] ]
        if len(lvc) == 1: lvc *= len(lvp)
        
        # serialize vert data
        lvp = [ c for vp in lvp for c in vp ]
        lvc = [ c for vc in lvc for c in vc ]
        lvn = [ c for vn in lvn for c in vn ] if lvn is not None else None
        
        if add_edges:
            io = len(lvp)
            lvp += lvp
            lvc += [ 0.25 for i in xrange(io) ]
            lvn += [ 0.0 for i in xrange(io) ]
            le = [ [i0+io,i1+io] for f in lf for i0,i1 in zip(f,f[1:]+[f[0]]) ]
            lf += le
        
        llf = [ [] for t in ltype ]             # vert inds categorized by 'face' size ( pt, ln, tri, quad )
        ls = [ None for t in ltype ]            # list of shapes (one for each 'face' size) to be unioned
        
        # categorize 'face' sizes
        for f in lf:
            assert len(f) in lsize, "unsupported face size %d" % len(f)
            llf[ lsize.index(len(f)) ] += f
        
        # create shapes
        for i,lvi in enumerate(llf):
            if len(lvi) == 0: continue
            t = ltype[i]
            ls[i] = UpdatableIndexedShape( t, lvi, lvp, lvc, lvn )
        
        return Shape_Combine( ls )
    
    def setdata( self, shapetype, indices, verts, colors, norms=None ):
        self.szverts = len(verts) / 3
        
        self.shapetype = shapetype
        self.indices = indices
        self.verts = verts
        self.colors = colors if len(colors) != 3 else colors * self.szverts
        self.norms = norms
        
        self._setdata()
    
    def set_verts( self, verts ):
        self.verts = verts
        self.vertex_list.vertices = self.verts
    def set_colors( self, colors ):
        self.colors = colors
        self.vertex_list.colors = self.colors
    def set_normals( self, norms ):
        self.norms = norms
        self.vertex_list.normals = self.norms
    
    def _setdata( self ):
        l = self.szverts
        if self.norms:
            self.vertex_list = pyglet.graphics.vertex_list_indexed( l, self.indices, 'v3f/stream', 'c3f/stream', 'n3f/stream' )
            self.vertex_list.vertices = self.verts
            self.vertex_list.colors = self.colors
            self.vertex_list.normals = self.norms
        else:
            self.vertex_list = pyglet.graphics.vertex_list_indexed( l, self.indices, 'v3f/stream', 'c3f/stream' )
            self.vertex_list.vertices = self.verts
            self.vertex_list.colors = self.colors
    
    def render( self ):
        self.vertex_list.draw( self.shapetype )
    
    def render_wires( self ):
        pass
    def render_verts( self ):
        pass


class Shape(object):
    def __init__( self, shapetype, verts, colors, norms=None, show_edges=False, point_size=None, line_width=None, use_lights=True, use_alpha=False ):
        self.setdata( shapetype, verts, colors, norms, show_edges, point_size, line_width, use_lights, use_alpha )
    
    def clone( self ):
        t = self.shapetype
        v = list( self.verts )
        c = list( self.colors )
        n = list( self.norms ) if n else None
        if n is None:   return Shape( t, v, c )
        else:           return Shape( t, v, c, n )
    
    @classmethod
    def fromCache( cls, fn, color=None ):
        ltype = [ GL_POINTS, GL_LINES, GL_TRIANGLES, GL_QUADS ]
        lsize = [ 1, 2, 3, 4 ]
        
        if color == None: color = [ 1.0, 1.0, 1.0 ]
        
        ls = [ None for t in ltype ]
        
        cache = pickle.load( open( fn, 'r' ) )
        for i,t in enumerate(ltype):
            lp = cache[i]
            if lp is None: continue
            lc = color * (len(lp) / 3)
            ls[i] = Shape( t, lp, lc )
        
        return Shape_Combine( ls )
    
    @classmethod
    def calc_normal( cls, lv, lvi ):
        norm = (0.0,0.0,0.0)
        l = len(lvi)
        for i in xrange(l):
            i0,i1,i2 = lvi[i],lvi[(i+1)%l],lvi[(i+2)%l]
            v10 = Vec3f.t_sub(lv[i0],lv[i1])
            v12 = Vec3f.t_sub(lv[i2],lv[i1])
            _norm = Vec3f.t_norm( Vec3f.t_cross( v10,v12 ) )
            norm = Vec3f.t_add( norm, _norm )
        return Vec3f.t_norm( norm )
    
    @classmethod
    def fromPLY( cls, fn, color=None ):
        # two lists below must correspond
        ltype = [ GL_POINTS, GL_LINES, GL_TRIANGLES, GL_QUADS ]
        lsize = [ 1, 2, 3, 4 ]
        
        if color == None: color = [ 1.0, 1.0, 1.0 ]
        
        # load object
        print( 'loading %s...' % fn )
        o = ply.ply_load( fn )
        
        lv = o['lv'] #[ c for v in o['lv'] for c in v ]
        llf = [ [] for t in ltype ]
        llfn = [ [] for t in ltype ]
        ls = [ None for t in ltype ]
        
        # categorize into lengths of 'faces'
        for f in o['lf']:
            assert len(f) in lsize, "unsupported face size %d" % len(f)
            llf[ lsize.index(len(f)) ] += f
            llfn[ lsize.index(len(f)) ] += [ cls.calc_normal( lv, f ) ]
        lnon0len = [ len(f)!=0 for f in llf ]
        assert sum(lnon0len), "no data loaded?"
        
        for i,t in enumerate(ltype):
            if not lnon0len[i]: continue
            s = lsize[i]
            lvi = llf[i]
            #lvi = [ vi for f in llf[i] for vi in f ]
            lp = [ c for vi in lvi for c in lv[vi] ]
            lc = color * len(lvi)
            ln = [ c for n in [ lfn * s for lfn in llfn[i] ] for c in n ]
            #ln = [1.0,0.0,0.0] * len(lvi )
            ls[i] = Shape( t, lp, lc, norms=ln )
        
        return Shape_Combine( ls )
    
    def setdata( self, shapetype, verts, colors, norms=None, show_edges=False, point_size=None, line_width=None, use_lights=True, use_alpha=False ):
        self.shapetype = shapetype
        self.verts = verts
        self.colors = colors
        self.norms = norms
        self.show_edges = show_edges
        self.point_size = point_size
        self.line_width = line_width
        self.use_lights = use_lights
        self.use_alpha = use_alpha
        
        self._setdata()
        
    def _setdata( self ):
        l = len(self.verts) / 3
        if len(self.colors) == 3:
            self.colors = self.colors*l
        
        if self.use_alpha: ct = 'c4f\static'
        else: ct = 'c3f\static'
        
        if self.norms:
            self.vertex_list = pyglet.graphics.vertex_list( l, ('v3f\static',self.verts), (ct,self.colors), ('n3f\static',self.norms) )
        else:
            self.vertex_list = pyglet.graphics.vertex_list( l, ('v3f\static',self.verts), (ct,self.colors) )
        
        self.lv_wires = None
        if True:
            # auto-gen wires for object
            if self.shapetype == GL_QUADS:
                q = l / 4
                iw = [ [
                    i*12+0,i*12+1,i*12+2, i*12+3,i*12+4,i*12+5,
                    i*12+3,i*12+4,i*12+5, i*12+6,i*12+7,i*12+8,
                    i*12+6,i*12+7,i*12+8, i*12+9,i*12+10,i*12+11,
                    i*12+9,i*12+10,i*12+11, i*12+0,i*12+1,i*12+2,
                    ] for i in xrange(q) ]
                lw = [ self.verts[c] for x in iw for c in x ]
                lew = len(lw) / 3
                self.lv_wires = pyglet.graphics.vertex_list( lew, ('v3f\static',lw), ('c3f\static',[0.20]*(lew*3)) )
            elif self.shapetype == GL_TRIANGLES:
                q = l / 3
                iw = [ [
                    i*9+0,i*9+1,i*9+2, i*9+3,i*9+4,i*9+5,
                    i*9+3,i*9+4,i*9+5, i*9+6,i*9+7,i*9+8,
                    i*9+6,i*9+7,i*9+8, i*9+0,i*9+1,i*9+2,
                    ] for i in xrange(q) ]
                lw = [ self.verts[c] for x in iw for c in x ]
                lew = len(lw) / 3
                self.lv_wires = pyglet.graphics.vertex_list( lew, ('v3f\static',lw), ('c3f\static',[0.20]*(lew*3)) )
            else:
                self.lv_wires = None
        
    
    
    def render( self ):
        #pyglet.graphics.draw( self.l, self.shapetype, ('v3f', self.verts ), ( 'c3f', self.colors ) )
        if self.line_width:
            glLineWidth( self.line_width )
        if self.point_size:
            glPointSize( self.point_size )
        if not self.use_lights:
            bl = glIsEnabled( GL_LIGHTING )
            glDisable( GL_LIGHTING )
        
        self.vertex_list.draw( self.shapetype )
        if self.show_edges:
            self.render_wires()
        
        if not self.use_lights and bl:
            glEnable( GL_LIGHTING )
    
    def render_wires( self ):
        if not self.lv_wires: return
        bl = glIsEnabled( GL_LIGHTING )
        glDisable( GL_LIGHTING )
        self.lv_wires.draw( GL_LINES )
        if bl: glEnable( GL_LIGHTING )
    
    def render_verts( self ):
        bl = glIsEnabled( GL_LIGHTING )
        glDisable( GL_LIGHTING )
        self.vertex_list.draw( GL_POINTS )
        if bl: glEnable( GL_LIGHTING )



class Shape_Cycle( object ):
    def __init__( self, lshapes, fps, bounce ):
        self.lshapes = lshapes
        self.nshapes = len(lshapes)
        self.fps = fps
        self.bounce = bounce
    
    def _get_i( self ):
        i = int(time.time() * self.fps)
        if self.bounce:
            i = i % (self.nshapes*2-2)
            if i >= self.nshapes:
                i = self.nshapes*2-2 - i
            return i
        return i % self.nshapes
    
    def render( self ):
        i = self._get_i()
        if self.lshapes[i]:
            self.lshapes[i].render()
    
    def render_wires( self ):
        i = self._get_i()
        if self.lshapes[i]:
            self.lshapes[i].render_wires()
    
    def render_verts( self ):
        i = self._get_i()
        if self.lshapes[i]:
            self.lshapes[i].render_verts()



class Shape_Selectable( object ):
    def __init__( self, shape ):
        self.shape_orig = shape
        self.shape = shape.clone()
    
    def render( self ):
        self.shape.render()
    
    def render_wires( self ):
        self.shape.render_wires()
    
    def render_verts( self ):
        self.shape.render_verts()
    
    def clear_selection( self ):
        for i in xrange(len(self.shape.colors)):
            self.shape.colors[i] = self.shape_orig.colors[i]
        self.shape._setdata()
    
    def select( self, i ):
        self.shape.colors[i*3+0] = 1
        self.shape.colors[i*3+1] = 1
        self.shape.colors[i*3+1] = 0
        self.shape._setdata()



class Shape_Combine( object ):
    def __init__( self, lshapes ):
        self.lshapes = lshapes
    
    def render( self ):
        for s in self.lshapes:
            if s is None: continue
            s.render()
    
    def render_wires( self ):
        for s in self.lshapes:
            if s is None: continue
            s.render_wires()
    
    def render_verts( self ):
        for s in self.lshapes:
            if s is None: continue
            s.render_verts()


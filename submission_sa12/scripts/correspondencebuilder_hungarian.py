from mesh import *
from hungarian import *
from debugwriter import *

from correspondencebuilder_logic import CorrespondenceBuilder_Logic as CB_Logic

class CorrespondenceBuilder_Hungarian( object ):
    
    @staticmethod
    def build_verts( opts ):
        CB_Hungarian = CorrespondenceBuilder_Hungarian
        
        DebugWriter.start( 'matching verts using bipartite graph matching' )
        
        ci = opts['ci']
        
        CB_Hungarian.build_vertex_correspondences_hungarian( opts )
        CB_Logic.build_face_correspondences( opts )
        
        #CB_Logic.remove_smaller_connected_faces_threshold( opts )
        #CB_Logic.remove_vert_matching_any_unmatched_faces( opts )
        #CB_Logic.remove_mismatched( dict_set_defaults( opts, {'verts':False} ) )
        #CB_Hungarian.build_vertex_correspondences_hungarian( opts )
        #CB_Logic.build_face_correspondences( opts )
        
        DebugWriter.end()
    
    @staticmethod
    def build_faces( opts ):
        CB_Hungarian = CorrespondenceBuilder_Hungarian
        
        DebugWriter.start( 'matching faces using bipartite graph matching' )
        
        ci = opts['ci']
        
        CB_Hungarian.build_face_correspondences_hungarian( opts )
        CB_Logic.build_vertex_correspondences( opts )
        
        for i in xrange(0):
            #CB_Logic.remove_smaller_connected_faces_threshold( opts )
            CB_Logic.remove_vert_matching_all_unmatched_faces( opts )
            CB_Logic.remove_mismatched( dict_set_defaults( opts, {'verts':False} ) )
            
            CB_Hungarian.build_face_correspondences_hungarian( opts )
            CB_Logic.build_vertex_correspondences( opts )
        
        DebugWriter.end()
    
    @staticmethod
    def build_verts_adapt( opts ):
        CB_Hungarian = CorrespondenceBuilder_Hungarian
        
        ci = opts['ci']
        
        opts_defaults = { 'mult_area': 500.0, 'mult_vert': 1000.0, 'mult_norm': 700.0, 'mult_count': 0.0 }
        opts = dict_defaults( opts, opts_defaults )
        opts['per'] = 0.75
        
        found = 0.0
        for threshold in [ 250.0, 500.0, 1000.0, 1500.0  ]:
            print( 'threshold = %f' % threshold )
            opts['threshold'] = opts['cost_adddel'] = threshold
            CB_Hungarian.build_face_correspondences_hungarian( opts )
            if len(ci.mf01) == 0:
                print( '  no correspondences found! re-running!' )
                continue
            CB_Logic.remove_face_conflicts( opts ) # 0.75
            if len(ci.mf01) == 0:
                print( '  no correspondences left! re-running!' )
                continue
            CB_Logic.remove_smaller_connected_faces( opts )
            
            found = float(len(ci.mf01)) / float(len(ci.lf0))
            print( '  found %f' % found )
            if found > 0.05: break
            print( '  too few corresponding faces! re-running!' )
        
        if found <= 0.01:
            print( '  running without removing' )
            opts['threshold'] = opts['cost_adddel'] = 1500.0
            CB_Hungarian.build_face_correspondences_hungarian( opts )
        
        lh = None
        for runs in xrange(5):
            h = ci.__hash__()
            if h == lh: continue
            lh = h
            ci.dcvall()
            CB_Logic.build_vertex_correspondences_faces( opts )
            CB_Hungarian.build_vertex_correspondences_hungarian_faces( opts )
            CB_Logic.build_face_correspondences( opts )
            if runs < 3: CB_Logic.remove_smaller_connected_faces( opts )
            CB_Logic.propagate_face_correspondences( opts )
            CB_Logic.remove_face_conflicts( opts )
            opts['mult_area'] = 1.0
            opts['mult_vert'] = 10.0
            opts['mult_norm'] = 4.0
            opts['mult_count'] = 0.0
            opts['threshold'] = 12.0
            opts['cost_adddel'] = 12.0
            CB_Hungarian.build_face_correspondences_hungarian( opts )
            
            CB_Logic.remove_face_conflicts( opts )
    
    @staticmethod
    def build_vertex_correspondences_hungarian( opts ):
        
        opts_defaults = {
            'threshold' : float('inf'),
            'cost_add'  : 2.5,
            'cost_del'  : 2.5,
            'cost_move' : 1.0,
            'cost_turn' : 1.0,
            'cost_degr' : 0.0,
            }
        opts = dict_set_defaults( opts, opts_defaults )
        
        ci = opts['ci']
        threshold = opts['threshold']
        cost_add = opts['cost_add']
        cost_del = opts['cost_del']
        cost_move = opts['cost_move']
        cost_turn = opts['cost_turn']
        cost_degr = opts['cost_degr']
        
        DebugWriter.start( 'building %i^2 (%i+%i) cost matrix' % (ci.cv0+ci.cv1,ci.cv0,ci.cv1) )
        
        # init distance matrix to be 0
        m = np.zeros( (ci.cv0+ci.cv1, ci.cv0+ci.cv1) )
        
        # Q1 cost: v0 -> @
        for (i,v0,j,v1) in ci.evq1():
            m[i][j] = cost_add if i==(j-ci.cv1) and i not in ci.mv01 else float('inf')
        
        # Q2 cost: v0 -> v1
        for (i,v0,j,v1) in ci.evq2():
            if i in ci.mv01 or j in ci.mv10:
                m[i][j] = 0.0 if i in ci.mv01 and ci.mv01[i] == j and j in ci.mv10 and ci.mv10[j] == i else float('inf')
            else:
                n0,n1 = ci.ln0[i],ci.ln1[j]
                caf0,caf1 = ci.la_v0[i]['ci_f'],ci.la_v1[j]['ci_f']
                
                dv2 = Vertex.distance( v0, v1 )
                dv2 = dv2 / (1.0 + dv2)
                dn2 = 1.0 - Vec3f.t_dot( n0 ,n1 ) # abs((n0[0]*n1[0])+(n0[1]*n1[1])+(n0[2]*n1[2]))
                dc2 = (caf0-caf1)*(caf0-caf1)
                # TODO: include mis-alignment of faces
                
                #cost = dv2 * 100.0 + dc2 * 0.1 + dn2 * 0.2
                cost = dv2 * cost_move + dn2 * cost_turn + dc2 * cost_degr
                
                m[i][j] = cost if cost < threshold else float('inf')
        
        # Q3 cost: @ -> v1
        for (i,v0,j,v1) in ci.evq3():
            m[i][j] = cost_del if (i-ci.cv0)==j and j not in ci.mv10 else float('inf')
        
        #for r in m:
        #    print( ','.join('%1.1f'%e for e in r) )
        
        DebugWriter.end()
        
        DebugWriter.start( 'running' )
        #inds = hungarian( np.array( m ) )
        inds = hungarian( m )
        DebugWriter.end()
        
        for i,j in enumerate(inds):
            if i >= ci.cv0 or j >= ci.cv1: continue
            if i in ci.mv01 or j in ci.mv10:
                assert ci.mv01[i] == j, 'ci.mv01[%i] == %i != %i' % (i,ci.mv01[i],j)
                assert ci.mv10[j] == i, 'ci.mv10[%i] == %i != %i' % (j,ci.mv10[j],i)
            else:
                ci.acv( i, j )
    
    @staticmethod
    def build_vertex_correspondences_hungarian_faces( opts ):
        ci = opts['ci']
        if False:
            threshold   = 1000.0    # 1000.0
            cost_adddel = 100.0   # 100.0
            mult_vert   = 50.0     # 50.0
            mult_count  = 0.0    # 0.0
            mult_norm   = 500.0     # 500.0
            mult_face   = 200.0
        else:
            threshold   = float('inf')    # 1000.0
            cost_adddel = 2.0   # 100.0
            mult_vert   = 3.0     # 50.0
            mult_count  = 0.0    # 0.0
            mult_norm   = 3.0     # 500.0
            mult_face   = 0.0
        
        print( "building vertex correspondences using hungarian method with corresponding faces..." )
        
        # init distance matrix to be 0
        m = [[0.0]*(ci.cv1+ci.cv0) for i in xrange(ci.cv0+ci.cv1)]
        
        # Q1 cost: v0 -> @
        for (i,v0,j,v1) in ci.evq1(): m[i][j] = cost_adddel if i==(j-ci.cv1) else float('inf')
        
        # Q2 cost: v0 -> v1
        for (i,v0,j,v1) in ci.evq2():
            if i in ci.mv01 or j in ci.mv10:
                m[i][j] = 0.0 if i in ci.mv01 and j in ci.mv10 and ci.mv01[i] == j and ci.mv10[j] == i else float('inf')
            else:
                # get adj faces to v0
                # get adj faces to v1
                # if 
                laf0 = ci.la_v0[i]['si_f']
                laf1 = [ ci.mf01[if0] for if0 in laf0 if if0 in ci.mf01 ]
                cff0 = len(set(laf1) & ci.la_v1[j]['si_f'])
                
                laf1 = ci.la_v1[j]['si_f']
                laf0 = [ ci.mf10[if1] for if1 in laf1 if if1 in ci.mf10 ]
                cffd1 = len(set(laf0) & ci.la_v0[i]['si_f'])
                
                n0,n1 = ci.ln0[i],ci.ln1[j]
                caf0,caf1 = ci.la_v0[i]['ci_f'],ci.la_v1[j]['ci_f']
                
                dv2 = Vertex.distance2( v0, v1 )
                #dn2 = 1.0 - abs((n0[0]*n1[0])+(n0[1]*n1[1])+(n0[2]*n1[2]))
                dn2 = 2.0 - (1.0 + Vec3f.t_dot(n0,n1))
                dc2 = (caf0-caf1)*(caf0-caf1)
                
                dmult = 1.0
                dv2 = dmult * dv2 / ( dmult * dv2 + 1.0 )
                
                cost = dv2 * mult_vert + dc2 * mult_count + dn2 * mult_norm
                cost += mult_face / (1.0 + float(cff0))
                
                m[i][j] = cost if cost < threshold else float('inf')
        
        # Q3 cost: @ -> v1
        for (i,v0,j,v1) in ci.evq3(): m[i][j] = cost_adddel if (i-ci.cv0)==j else float('inf')
        
        print( "  running..." )
        t = time.time()
        inds = hungarian( np.array( m ) )
        print( "    finished in %f" % (time.time()-t) )
        
        for i,j in enumerate(inds):
            if i >= ci.cv0 or j >= ci.cv1: continue
            if i in ci.mv01 or j in ci.mv10:
                assert ci.mv01[i] == j, 'ci.mv01[%i] == %i != %i' % (i,ci.mv01[i],j)
                assert ci.mv10[j] == i, 'ci.mv10[%i] == %i != %i' % (j,ci.mv10[j],i)
            else:
                ci.acv( i, j )
        
        return ci
    
    @staticmethod
    def build_face_correspondences_hungarian( opts ): #ci, opts=None ):
        ci = opts['ci']
        
        if not opts: opts = { }
        if False:
            threshold   = opts.setdefault( 'threshold',     1000.0 )    # 1000.0
            cost_adddel = opts.setdefault( 'cost_adddel',    100.0 )    #    5.0
            mult_vert   = opts.setdefault( 'mult_vert',       50.0 )    #   50.0
            mult_count  = opts.setdefault( 'mult_count',       0.0 )    #   50.0
            mult_norm   = opts.setdefault( 'mult_norm',      500.0 )    #  200.0
            mult_area   = opts.setdefault( 'mult_area',       25.0 )    #   25.0
        else:
            threshold   = float('inf')
            cost_adddel = 2.5
            mult_vert   = 1.0
            mult_count  = 0
            mult_norm   = 1.0
            mult_area   = 0.0
            
        print( "building face correspondences (%ix%i) using hungarian method..." % (ci.cf0,ci.cf1) )
        t0 = time.time()
        
        print( '  computing cost matrix...' )
        t = time.time()
        
        # calc avg vert point
        lcv0 = [ Vertex.average( ci.m0.opt_get_uf_lv(f0.u_f) ) for f0 in ci.m0.lf ]
        lcv1 = [ Vertex.average( ci.m1.opt_get_uf_lv(f1.u_f) ) for f1 in ci.m1.lf ]
        
        # calc area of faces
        laf0 = [ Face.calc_area( [ v.p for v in ci.m0.opt_get_uf_lv(f0.u_f) ] ) for f0 in ci.lf0 ]
        laf1 = [ Face.calc_area( [ v.p for v in ci.m1.opt_get_uf_lv(f1.u_f) ] ) for f1 in ci.lf1 ]
        
        # init distance matrix to be 0
        m = [[0.0]*(ci.cf1+ci.cf0) for i in xrange(ci.cf0+ci.cf1)]
        
        # Q1 cost: v0 -> @
        for (i,f0,j,f1) in ci.efq1(): m[i][j] = float('inf') if i in ci.mf01 else cost_adddel if i==(j-ci.cf1) else float('inf')
        
        # Q2 cost: v0 -> v1
        for (i,f0,j,f1) in ci.efq2():
            
            if i in ci.mf01 or j in ci.mf10:
                m[i][j] = 0.0 if i in ci.mf01 and j in ci.mf10 and ci.mf01[i] == j and ci.mf10[j] == i else float('inf')
            else:
                #dv2 = Vec3f.t_distance2( lcv0[i], lcv1[j] )
                #dn2 = 2.0 - (1.0 + (Vec3f.t_dot(ci.m0.opt_face_normal(f0),ci.m1.opt_face_normal(f1))))
                dv2 = Vec3f.t_distance( lcv0[i], lcv1[j] )
                dn2 = 1.0 - Vec3f.t_dot( ci.m0.opt_face_normal(f0), ci.m1.opt_face_normal(f1) )
                dc2 = (len(ci.la_f0[i]['si_f']) - len(ci.la_f1[j]['si_f']))**2
                da2 = abs( laf0[i] - laf1[j] )
                
                dmult = 1.0
                dv2 = dmult * dv2 / ( dmult * dv2 + 1.0 )
                
                cost = dv2 * mult_vert + dc2 * mult_count + dn2 * mult_norm + da2 * mult_area
                
                m[i][j] = cost if cost < threshold else float('inf')
        
        # Q3 cost: @ -> v1
        for (i,f0,j,f1) in ci.efq3(): m[i][j] = float('inf') if j in ci.mf10 else cost_adddel if (i-ci.cf0)==j else float('inf')
        
        print( '    finished in %f' % (time.time()-t) )
        
        print( "  running..." )
        t = time.time()
        inds = hungarian( np.array( m ) )
        print( "    finished in %f" % (time.time()-t) )
        
        for i,j in enumerate(inds):
            if i >= ci.cf0 or j >= ci.cf1: continue
            if i in ci.mf01 or j in ci.mf10:
                assert ci.mf01[i] == j, 'ci.mf01[%i] == %i != %i' % (i,ci.mf01[i],j)
                assert ci.mf10[j] == i, 'ci.mf10[%i] == %i != %i' % (j,ci.mf10[j],i)
            else:
                ci.acf( i, j )
        
        print( '  finished in %f' % (time.time()-t0) )
        
        return ci
    
    @staticmethod
    def build_face_correspondences_hungarian_verts( opts ): #ci, opts=None ):
        ci = opts['ci']
        m0,m1 = ci.m0,ci.m1
        
        if not opts: opts = { }
        if False:
            threshold   = opts.setdefault( 'threshold',     1000.0 )    # 1000.0
            cost_adddel = opts.setdefault( 'cost_adddel',    100.0 )    #    5.0
            mult_vert   = opts.setdefault( 'mult_vert',       50.0 )    #   50.0
            mult_count  = opts.setdefault( 'mult_count',       0.0 )    #   50.0
            mult_norm   = opts.setdefault( 'mult_norm',      500.0 )    #  200.0
            mult_area   = opts.setdefault( 'mult_area',       25.0 )    #   25.0
        else:
            threshold   = float('inf')
            cost_adddel = 50.0
            mult_vert   = 1.0
            mult_count  = 0
            mult_norm   = 1.0
            mult_area   = 0.0
            
        print( "building face correspondences (%ix%i) using hungarian method..." % (ci.cf0,ci.cf1) )
        t0 = time.time()
        
        print( '  computing cost matrix...' )
        t = time.time()
        
        # calc avg vert point
        lcv0 = [ Vertex.average( ci.m0.opt_get_uf_lv(f0.u_f) ) for f0 in ci.m0.lf ]
        lcv1 = [ Vertex.average( ci.m1.opt_get_uf_lv(f1.u_f) ) for f1 in ci.m1.lf ]
        
        # calc area of faces
        laf0 = [ Face.calc_area( [ v.p for v in ci.m0.opt_get_uf_lv(f0.u_f) ] ) for f0 in ci.lf0 ]
        laf1 = [ Face.calc_area( [ v.p for v in ci.m1.opt_get_uf_lv(f1.u_f) ] ) for f1 in ci.lf1 ]
        
        # init distance matrix to be 0
        m = [[0.0]*(ci.cf1+ci.cf0) for i in xrange(ci.cf0+ci.cf1)]
        
        # Q1 cost: v0 -> @
        for (i,f0,j,f1) in ci.efq1(): m[i][j] = float('inf') if i in ci.mf01 else cost_adddel if i==(j-ci.cf1) else float('inf')
        
        # Q2 cost: v0 -> v1
        for (i,f0,j,f1) in ci.efq2():
            
            if i in ci.mf01 or j in ci.mf10:
                m[i][j] = 0.0 if i in ci.mf01 and j in ci.mf10 and ci.mf01[i] == j and ci.mf10[j] == i else float('inf')
            else:
                liv0 = m0._opt_get_f_li_v(i)
                liv1 = m1._opt_get_f_li_v(j)
                liv10 = [ ci.mv10[iv1] for iv1 in liv1 if iv1 in ci.mv10 ]
                siv0 = set(liv0)
                siv10 = set(liv10)
                dmv = ( 1.0 - float(len(siv0 & siv10)) / float(len(liv0)+len(liv1)) )
                
                dv2 = Vec3f.t_distance2( lcv0[i], lcv1[j] )
                dn2 = 2.0 - (1.0 + (Vec3f.t_dot(ci.m0.opt_face_normal(f0),ci.m1.opt_face_normal(f1))))
                #dc2 = (len(ci.la_f0[i]['si_f']) - len(ci.la_f1[j]['si_f']))**2
                #da2 = abs( laf0[i] - laf1[j] )
                
                dmult = 1.0
                dv2 = dmult * dv2 / ( dmult * dv2 + 1.0 )
                
                cost = dv2 * mult_vert + dn2 * mult_norm # + dmv * cost_adddel
                
                m[i][j] = cost
        
        # Q3 cost: @ -> v1
        for (i,f0,j,f1) in ci.efq3(): m[i][j] = float('inf') if j in ci.mf10 else cost_adddel if (i-ci.cf0)==j else float('inf')
        
        print( '    finished in %f' % (time.time()-t) )
        
        print( "  running..." )
        t = time.time()
        inds = hungarian( np.array( m ) )
        print( "    finished in %f" % (time.time()-t) )
        
        for i,j in enumerate(inds):
            if i >= ci.cf0 or j >= ci.cf1: continue
            if i in ci.mf01 or j in ci.mf10:
                assert ci.mf01[i] == j, 'ci.mf01[%i] == %i != %i' % (i,ci.mf01[i],j)
                assert ci.mf10[j] == i, 'ci.mf10[%i] == %i != %i' % (j,ci.mf10[j],i)
            else:
                ci.acf( i, j )
        
        print( '  finished in %f' % (time.time()-t0) )
        
        return ci
    
    @staticmethod
    def build_both( opts ):
        print( 'using "vfhungarian" method...' )
        opts = {
            #'v_add': 5.0, 'v_del': 5.0, 'v_vdist': 7.5, 'v_vnorm': 7.5,
            #'f_add': 50.0, 'f_del': 50.0, 'f_vdist': 8.0, 'f_fnorm': 80.0,
            'v_add': 15.0, 'v_del': 15.0, 'v_vdist': 2.5, 'v_vnorm': 2.5,
            'f_add': 50.0, 'f_del': 50.0, 'f_vdist': 2.0, 'f_fnorm': 80.0,
            }
        
        v_adddel = 1.0 #10.0
        f_adddel = 4.0 #30.0
        geo      = 1.0 # 2.5
        topo     = 10.0 # 9.0
        
        opts['v_add'] = opts['v_del'] = v_adddel
        opts['f_add'] = opts['f_del'] = f_adddel
        opts['v_vdist'] = geo
        opts['v_vnorm'] = 0.0
        opts['f_vdist'] = opts['f_fnorm'] = geo
        
        build_vertface_correspondences_hungarian( ci, opts=opts )
    
    @staticmethod
    def build_vertface_correspondences_hungarian( ci, opts=None ):
        
        if not opts: opts = { }
        v_add       = opts.setdefault( 'v_add', 5.0 )
        v_del       = opts.setdefault( 'v_del', 5.0 )
        v_vdist     = opts.setdefault( 'v_vdist', 7.5 )
        v_vnorm     = opts.setdefault( 'v_vnorm', 7.5 )
        f_add       = opts.setdefault( 'f_add', 50.0 )
        f_del       = opts.setdefault( 'f_del', 50.0 )
        f_vdist     = opts.setdefault( 'f_vdist', 8.0 )
        f_fnorm     = opts.setdefault( 'f_fnorm', 80.0 )
            
        print( "building vert,face correspondences (%ix%i) using hungarian method..." % (ci.cv0+ci.cf0,ci.cv1+ci.cf1) )
        t0 = time.time()
        
        print( '  computing cost matrix...' )
        t = time.time()
        
        # calc face info
        lcv0 = [ Vertex.average( ci.m0.opt_get_uf_lv(f0.u_f) ) for f0 in ci.m0.lf ]
        lcv1 = [ Vertex.average( ci.m1.opt_get_uf_lv(f1.u_f) ) for f1 in ci.m1.lf ]
        ln0 = [ ci.m0.opt_face_normal(f0) for f0 in ci.m0.lf ]
        ln1 = [ ci.m1.opt_face_normal(f1) for f1 in ci.m1.lf ]
        
        cv = ci.cv0+ci.cv1
        cf = ci.cf0+ci.cf1
        
        # init distance matrix to be 0
        m = [[0.0]*(cv+cf) for i in xrange(cv+cf)]
        
        # vert-vert quadrant
        for (i,v0,j,v1) in evq1(ci): m[i][j] = float('inf') if i in ci.mv01 else v_add if i==(j-ci.cv1) else float('inf')
        for (i,v0,j,v1) in evq2(ci):
            if i in ci.mv01 or j in ci.mv10:
                m[i][j] = 0.0 if i in ci.mv01 and j in ci.mv10 and ci.mv01[i] == j else float('inf')
            else:
                dv2 = Vec3f.t_distance( v0.p, v1.p )
                dn2 = ( 1.0 - Vec3f.t_dot( v0.n, v1.n ) )# / 2.0
                m[i][j] = dv2 * v_vdist + dn2 * v_vnorm
        for (i,v0,j,v1) in evq3(ci): m[i][j] = float('inf') if j in ci.mv10 else v_del if (i-ci.cv0)==j else float('inf')
        
        # vert-face quadrant
        for i in xrange(cv):
            for j in xrange(cf):
                m[i][j+cv] = float('inf')
        
        # face-vert quadrant
        for i in xrange(cf):
            for j in xrange(cv):
                m[i+cv][j] = float('inf')
        
        # face-face quadrant
        for (i,f0,j,f1) in efq1(ci): m[i+cv][j+cv] = float('inf') if i in ci.mf01 else f_add if i==(j-ci.cf1) else float('inf')
        for (i,f0,j,f1) in efq2(ci):
            if i in ci.mf01 or j in ci.mf10:
                m[i+cv][j+cv] = 0.0 if i in ci.mf01 and j in ci.mf10 and ci.mf01[i] == j else float('inf')
            else:
                dv2 = Vec3f.t_distance( lcv0[i], lcv1[j] )
                dn2 = ( 1.0 - Vec3f.t_dot( ln0[i],ln1[j] ) )# / 2.0
                m[i+cv][j+cv] = dv2 * f_vdist + dn2 * f_fnorm
        for (i,f0,j,f1) in efq3(ci): m[i+cv][j+cv] = float('inf') if j in ci.mf10 else f_del if (i-ci.cf0)==j else float('inf')
        
        print( '    finished in %f' % (time.time()-t) )
        
        print( "  running..." )
        t = time.time()
        inds = hungarian( np.array( m ) )
        print( "    finished in %f" % (time.time()-t) )
        
        for i,j in enumerate(inds):
            assert (i < cv and j < cv) or (i >= cv and j >= cv)
            if i < cv:
                iv0,iv1 = i,j
                if iv0 >= ci.cv0 or iv1 >= ci.cv1: continue
                ci.acv(iv0,iv1)
            
            else:
                if0,if1 = i-cv,j-cv
                if if0 >= ci.cf0 or if1 >= ci.cf1: continue
                ci.acf(if0,if1)
        
        print( '  finished in %f' % (time.time()-t0) )
        
        return ci


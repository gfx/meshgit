#!/bin/bash

# get current and script paths
cp=`pwd`
sp="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# go to algorithm directory
pushd "${sp}/smac/"

# copy data locally so smac algorithm can load it
cp "${cp}/SMAC_data.mat" .

# hackish attempt to find the Matlab executable
# NOTE: this may need to be configured!
case `uname` in
    Darwin|*BSD) matlabs="/Applications/MATLAB_R201*.app/bin/matlab" ;;
    #Linux) sizes() { /bin/ps -o rss= -$1; } ;;
    *) echo "`uname`: unsupported operating system" >&2; exit 2 ;;
esac
# may be multiple versions... use the "last" one
matlabs=(`echo ${matlabs}`)
for x in "${matlabs[@]}"; do
    matlab="${x}"
done
echo "Using: ${matlab}"

# run algorithm
#./graphmatch.sh
#/Applications/MATLAB_R2011a.app/bin/matlab -nosplash -nodesktop -nojvm -r 'meshgit_SMAC(); exit();'
${matlab} -nosplash -nodesktop -nojvm -r 'meshgit_SMAC(); exit();'

# copy results back
cp SMAC_ind.txt "${cp}"

popd

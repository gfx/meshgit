from vec3f import *
from mesh import *
from shapes import *
from viewer import *
from miscfuncs import *

from correspondenceinfo import *

from meshgit_misc import *
from meshgit_med_ops import *


def run_diff( fn0, fn1, fnd, fnc, fna ):
    m0 = Mesh.fromPLY( fn0 )
    m1 = Mesh.fromPLY( fn1 )
    
    m1a = Mesh.bc_closestverts( m0, m1 )
    
    c01 = diff_commands( m0, m1a )
    su_df = set([ cmd.split(' ')[1].split(':')[0] for cmd in c01 if cmd.startswith('delface') ])
    su_af = set([ cmd.split(' ')[1].split(':')[0] for cmd in c01 if cmd.startswith('addface') ])
    su_cf = set(m0.get_lu_f()) - su_df
    
    md = m0.clone()
    md.lf = [ f for f in md.lf if f.u_f in su_df ]
    md.trim()
    
    mc = m0.clone()
    mc.lf = [ f for f in mc.lf if f.u_f in su_cf ]
    mc.trim()
    
    ma = m0.clone(clone_faces=False)
    do_commands( ma, [ cmd for cmd in c01 if cmd.startswith('add') ] )
    ma.trim()
    
    md.toPLY( fnd )
    mc.toPLY( fnc )
    ma.toPLY( fna )


def run_diff_viewer2( fn0, fn1, method=None, maxiters=None ):
    DebugWriter.start( 'loading meshes' )
    m0,m1 = [ Mesh.fromPLY( fn ) for fn in [fn0,fn1] ]
    DebugWriter.end()
    
    DebugWriter.start( 'matching meshes' )
    scaling_factor = scale( [ m0, m1 ] )
    ci = CorrespondenceInfo.build( m0, m1, method=method, maxiters=maxiters )
    scale( [m0,m1], 1.0 / scaling_factor )
    m1 = ci.get_corresponding_mesh1( allow_face_sub=False )
    for m in [m0,m1]: m.optimize().rebuild_edges()
    DebugWriter.end()
    
    c01 = diff_commands( m0, m1 )
    su_df = set([ cmd.split(' ')[1].split(':')[0] for cmd in c01 if cmd.startswith('delface') ])
    su_af = set([ cmd.split(' ')[1].split(':')[0] for cmd in c01 if cmd.startswith('addface') ])
    su_mf = set([ cmd.split(' ')[1].split(':')[0] for cmd in c01 if cmd.startswith('modface') ])
    su_cf = set(m0.get_lu_f()) - su_df - su_mf
    su_mv = set([ cmd.split(' ')[1].split(':')[0] for cmd in c01 if cmd.startswith('move') ])
    
    DebugWriter.start( 'coloring meshes' )
    
    m0.recolor( color_scheme['='] )
    m1.recolor( color_scheme['='] )
    
    mc = m0.clone_filter_faces( su_cf | su_mf, True ).trim()
    md = m0.clone_filter_faces( su_df, True ).trim().recolor( color_scheme['-'] )
    
    mc2 = m1.clone_filter_faces( su_cf, True ).trim()
    ma2 = m1.clone_filter_faces( su_af, True ).trim().recolor( color_scheme['+'] )
    mm2 = m1.clone_filter_faces( su_mf, True ).trim().recolor( color_scheme['='] )
    mm2.optimize()
    
    scaling_factor = scale( [ m0, m1 ], scaling_factor='unitcube' )
    su_v0 = set( m0.get_lu_v() )
    dvd1 = dict( [ ( v.u_v, Vec3f.t_distance(v.p,m0.opt_get_v(v.u_v).p) ) for v in m1.lv  if v.u_v in su_v0 ] )
    scale( [m0,m1], 1.0 / scaling_factor )
    for v in mc2.lv + mm2.lv:
        if v.u_v in su_mv:
            val = min( 1.0, dvd1[v.u_v] * 25.0 )
            v.c = Vec3f.t_lerp( color_scheme['='], color_scheme['>'], val )
    
    DebugWriter.end()
    
    
    DebugWriter.start( 'generating visualizations', quiet=True )
    lshapes = [
        Shape_Combine( [ mc.toShape(), md.toShape() ] ),
        Shape_Combine( [ mc2.toShape(), ma2.toShape(), mm2.toShape() ] )
        ]
    DebugWriter.end()
    
    lcaptions = [ fn0, fn1 ]
    window = Viewer( lshapes, caption='MeshGit Diff2 Viewer: %s -> %s' % (fn0,fn1), lcaptions=lcaptions )
    window.app_run()


def run_diff_viewer2_iter_old( fn0, fn1, method=None ):
    m0,m1 = [ Mesh.fromPLY( fn ) for fn in [fn0,fn1] ]
    scaling_factor = scale( [ m0, m1 ] )
    
    lmf10 = []
    mf10 = {}
    c01 = None
    
    for maxiters in xrange(10):
        print( '*' * 20 )
        print( 'maxiters = %i' % maxiters )
        print( '*' * 20 )
        print( '' )
        print( '' )
        
        if maxiters == 20: c01.lastpass = True
        #c01 = build_correspondence( m0, m1, method=method, maxiters=min(maxiters,1), ci=c01 )
        c01 = CorrespondenceInfo.build( m0, m1, method=method, maxiters=min(maxiters,1) )
        
        for if1,if0 in c01.mf10.iteritems():
            if if1 in mf10 and mf10[if1][0] == if0:
                mf10[if1][1] += 1
            else:
                mf10[if1] = [if0,1]
        for if1,d in mf10.iteritems():
            if if1 in c01.mf10 and c01.mf10[if1] == d[0]: continue
            mf10[if1] = [ d[0],0 ]
        
        #print( mf10 )
        lmf10 += [ copy.deepcopy(mf10) ]
        
        if c01.done:
            print( '*' * 20 )
            print( 'CONVERGED!!!' )
            print( '*' * 20 )
            print( '' )
            print( '' )
            break
    
    scale( [m0,m1], 1.0 / scaling_factor )
    m1 = c01.get_corresponding_mesh1( allow_face_sub=False )
    for m in [m0,m1]:
        m.optimize()
        m.rebuild_edges()
    lcmds = diff_commands( m0, m1 )
    
    M = max([ x[1][1] for x in mf10.items() ])
    L = len(lmf10)
    
    color_ramp = [
        [ 0.35, 0.35, 0.35 ],
        [ 0.90, 0.90, 0.10 ],
        [ 0.70, 0.70, 0.70 ],
        #[ 0.75, 0.50, 0.00 ],
        ]
    #color_ramp = [
        #[ 0.5, 0.5, 0.5 ],
        #[ 1.0, 1.0, 0.0 ],
        #[ 1.0, 0.5, 0.0 ],
        #[ 0.6, 0.25, 0.5 ],
        #[ 0.4, 0.1, 0.6 ],
        #[ 0.0, 0.0, 0.6 ],
        #]
    
    lm2 = []
    lm3 = []
    for i,mf10 in enumerate(lmf10):
        mf01 = dict( (d[0], [if1,d[1]]) for if1,d in mf10.iteritems() )
        
        m2 = Mesh()
        for i_f,f in enumerate(m0.lf):
            if i_f in mf01 and mf01[i_f] != 0:
                val = mf01[i_f][1]
            else:
                val = 0
            c = color_ramp[min(val,len(color_ramp)-1)]
            lv = [ m0.opt_get_v(u_v).copy().recolor(c) for u_v in f.lu_v ]
            lu_v = [ v.u_v for v in lv ]
            m2.lv += lv
            m2.lf += [ Face(u_f=f.u_f,lu_v=lu_v) ]
        m2.rebuild_edges()
        lm2 += [m2]
        
        m3 = Mesh()
        for i_f,f in enumerate(m1.lf):
            if i_f in mf10 and mf10[i_f] != 0:
                val = mf10[i_f][1]
            else:
                val = 0
            c = color_ramp[min(val,len(color_ramp)-1)]
            lv = [ m1.opt_get_v(u_v).copy().recolor(c) for u_v in f.lu_v ]
            lu_v = [ v.u_v for v in lv ]
            m3.lv += lv
            m3.lf += [ Face(u_f=f.u_f,lu_v=lu_v) ]
        m3.rebuild_edges()
        lm3 += [m3]
    
    su_df = set([ cmd.split(' ')[1].split(':')[0] for cmd in lcmds if cmd.startswith('delface') ])
    su_af = set([ cmd.split(' ')[1].split(':')[0] for cmd in lcmds if cmd.startswith('addface') ])
    su_cf = set(m0.get_lu_f()) - su_df
    su_mv = set([ cmd.split(' ')[1].split(':')[0] for cmd in lcmds if cmd.startswith('move') ])
    
    m0.recolor( color_scheme['='] )
    m1.recolor( color_scheme['='] )
    
    mc = m0.clone().filter_faces( su_cf, True ).trim()
    md = m0.clone().filter_faces( su_df, True ).trim().recolor( color_scheme['-'] )
    
    mc2 = m1.clone().filter_faces( su_cf, True ).trim()
    ma2 = m1.clone().filter_faces( su_af, True ).trim().recolor( color_scheme['+'] )
    
    for v in mc2.lv:
        if v.u_v in su_mv:
            v.c = color_scheme['>']
    
    m0.rebuild_edges()
    m1.rebuild_edges()
    mc.rebuild_edges()
    mc2.rebuild_edges()
    md.rebuild_edges()
    ma2.rebuild_edges()
    
    mc.extend(md)
    mc2.extend(ma2)
    
    #shapes = [ mesh.toShape() for mesh in ([m0]+lm2+[mc,m1]+lm3+[mc2]) ]
    #window = Viewer( shapes, caption='MeshGit Diff2 Iter Viewer: %s -> %s' % (fn0,fn1), ncols=L+2, size=(300,300) )
    cycle0 = Shape_Cycle( [ m.toShape() for m in lm2 ], 4.0, False )
    cycle1 = Shape_Cycle( [ m.toShape() for m in lm3 ], 4.0, False )
    shapes = [ m0.toShape(), cycle0, mc.toShape(), m1.toShape(), cycle1, mc2.toShape() ]
    
    lcaptions = [ fn0, fn1 ]
    
    window = Viewer( shapes, caption='MeshGit Diff2 Iter Viewer: %s -> %s' % (fn0,fn1), ncols=3, lcaptions=lcaptions )
    window.app_run()

def run_diff_viewer2_methods( fn0, fn1, *lmethods, **dparms ):
    DebugWriter.start( 'two-way diff viewer with methods' )
    
    DebugWriter.start( 'loading meshes')
    m0,m1 = [ Mesh.fromPLY( fn ) for fn in [fn0,fn1] ]
    m0.optimize().rebuild_edges()
    DebugWriter.end()
    
    ls0,lc0 = [ m0.toShape() ],[ 'original' ]
    ls1,lc1 = [ m1.toShape() ],[ 'derivative' ]
    
    for i,method in enumerate(lmethods):
        DebugWriter.start( 'building correspondences with method: %s' % method )
        scaling_factor = scale( [ m0, m1 ] )
        ci = CorrespondenceInfo.build( m0, m1, method=method )
        scale( [m0,m1], scaling_factor=1.0/scaling_factor )
        DebugWriter.end()
        
        m1m = ci.get_corresponding_mesh1(allow_face_sub=False).optimize().rebuild_edges()
        m0c,m1c = get_colored_meshes_2way( m0, m1m )
        
        ls0 += [ m0c.toShape() ]
        ls1 += [ m1c.toShape() ]
        
        lc0 += [ method ]
        lc1 += [ method ]
    
    lshapes = ls0 + ls1
    lcaps   = lc0 + lc1
    ncols   = len(lmethods)+1
    
    window = Viewer( lshapes, caption='MeshGit Diff Viewer', lcaptions=lcaps, ncols=ncols, size=(200,200) )
    window.app_run()

def run_diff_viewer2_iter( fn0, fn1, **dparms ):
    DebugWriter.start( 'two-way diff viewer, showing iterations' )
    
    DebugWriter.start( 'loading meshes')
    m0,m1 = [ Mesh.fromPLY( fn ) for fn in [fn0,fn1] ]
    m0.optimize().rebuild_edges()
    DebugWriter.end()
    
    ls0,lc0 = [ m0.toShape() ],[ 'original' ]
    ls1,lc1 = [ m1.toShape() ],[ 'derivative' ]
    
    i = 0
    while True:
        DebugWriter.start( 'building correspondences for iteration %d' % i )
        scaling_factor = scale( [ m0, m1 ] )
        ci = CorrespondenceInfo.build( m0, m1, maxiters=i )
        scale( [m0,m1], scaling_factor=1.0/scaling_factor )
        DebugWriter.end()
        
        m1m = ci.get_corresponding_mesh1(allow_face_sub=False).optimize().rebuild_edges()
        m0c,m1c = get_colored_meshes_2way( m0, m1m )
        
        ls0 += [ m0c.toShape() ]
        ls1 += [ m1c.toShape() ]
        
        lc0 += [ str(i) ]
        lc1 += [ '' ]
        
        if ci.done: break
        
        i += 1
    
    lshapes = ls0 + ls1
    lcaps   = lc0 + lc1
    ncols   = i+2
    
    window = Viewer( lshapes, caption='MeshGit Diff Viewer', lcaptions=lcaps, ncols=ncols, size=(200,200) )
    window.app_run()


def run_diff_viewer3( fn0, fn1, fn2, method=None, maxiters=None ):
    m0,ma,mb = [ Mesh.fromPLY( fn ) for fn in [fn0,fn1,fn2] ]
    scaling_factor = scale( [ m0, ma, mb ] )
    
    ci0a = CorrespondenceInfo.build( m0, ma, method=method, maxiters=maxiters )
    ci0b = CorrespondenceInfo.build( m0, mb, method=method, maxiters=maxiters )
    
    ma = ci0a.get_corresponding_mesh1( allow_face_sub=False )
    mb = ci0b.get_corresponding_mesh1( allow_face_sub=False )
    
    for m in [m0,ma,mb]:
        m.optimize()
        m.rebuild_edges()
    
    scale( [m0,ma,mb], 1.0 / scaling_factor )
    
    scaling_factor = scale( [m0,ma,mb], scaling_factor='unitcube' )
    su_v0 = set( m0.get_lu_v() )
    dvda = dict( [ ( v.u_v, Vec3f.t_distance(v.p,m0.opt_get_v(v.u_v).p) ) for v in ma.lv if v.u_v in su_v0 ] )
    dvdb = dict( [ ( v.u_v, Vec3f.t_distance(v.p,m0.opt_get_v(v.u_v).p) ) for v in mb.lv if v.u_v in su_v0 ] )
    scale( [m0,ma,mb], 1.0 / scaling_factor )
    
    c0a = diff_commands( m0, ma )
    su_dfa = set([ cmd.split(' ')[1].split(':')[0] for cmd in c0a if cmd.startswith('delface') ])
    su_afa = set([ cmd.split(' ')[1].split(':')[0] for cmd in c0a if cmd.startswith('addface') ])
    su_mfa = set([ cmd.split(' ')[1].split(':')[0] for cmd in c0a if cmd.startswith('modface') ])
    su_cfa = set(m0.get_lu_f()) - su_dfa - su_mfa
    su_mva = set([ cmd.split(' ')[1].split(':')[0] for cmd in c0a if cmd.startswith('move') ])
    su_ava = set([ cmd.split(' ')[1].split(':')[0] for cmd in c0a if cmd.startswith('addvert') ])
    
    c0b = diff_commands( m0, mb )
    su_dfb = set([ cmd.split(' ')[1].split(':')[0] for cmd in c0b if cmd.startswith('delface') ])
    su_afb = set([ cmd.split(' ')[1].split(':')[0] for cmd in c0b if cmd.startswith('addface') ])
    su_mfb = set([ cmd.split(' ')[1].split(':')[0] for cmd in c0b if cmd.startswith('modface') ])
    su_cfb = set(m0.get_lu_f()) - su_dfb - su_mfb
    su_mvb = set([ cmd.split(' ')[1].split(':')[0] for cmd in c0b if cmd.startswith('move') ])
    su_avb = set([ cmd.split(' ')[1].split(':')[0] for cmd in c0b if cmd.startswith('addvert') ])
    
    
    mda = m0.clone().filter_faces( su_dfa, True ).trim().recolor( color_scheme['-a'] )
    mca = ma.clone().filter_faces( su_cfa, True ).trim().recolor( color_scheme['='] )
    maa = ma.clone(clone_faces=False)
    do_commands( maa, [ cmd for cmd in c0a if cmd.startswith('add') ] )
    maa.trim().recolor( color_scheme['+a'] )
    mma = ma.clone().filter_faces( su_mfa, True ).trim().recolor( color_scheme['='] )
    mma.optimize()
    
    mdb = m0.clone().filter_faces( su_dfb, True ).trim().recolor( color_scheme['-b'] )
    mcb = mb.clone().filter_faces( su_cfb, True ).trim().recolor( color_scheme['='] )
    mab = mb.clone(clone_faces=False)
    do_commands( mab, [ cmd for cmd in c0b if cmd.startswith('add') ] )
    mab.trim().recolor( color_scheme['+b'] ) #mab.recolor( [0.4,0.75,0.0] )
    mmb = mb.clone().filter_faces( su_mfb, True ).trim().recolor( color_scheme['='] )
    mmb.optimize()
    
    
    # color moved verts
    for v in mca.lv + mma.lv:
        if v.u_v in su_mva:
            val = min( 1.0, dvda[v.u_v] * 25.0 )
            v.c = Vec3f.t_lerp( color_scheme['='], color_scheme['>'], val )
    for v in mcb.lv + mmb.lv:
        if v.u_v in su_mvb:
            val = min( 1.0, dvdb[v.u_v] * 25.0 )
            v.c = Vec3f.t_lerp( color_scheme['='], color_scheme['>'], val )
    
    # color sub'd faces
    for u_f in su_mfa:
        lu_v0 = m0.opt_get_f(u_f).lu_v
        lu_va = ma.opt_get_f(u_f).lu_v
        for u_v in set(lu_va) - set(lu_v0):
            mma.opt_get_v(u_v).c = color_scheme['!']
    for u_f in su_mfb:
        lu_v0 = m0.opt_get_f(u_f).lu_v
        lu_vb = mb.opt_get_f(u_f).lu_v
        for u_v in set(lu_vb) - set(lu_v0):
            mmb.opt_get_v(u_v).c = color_scheme['!']
    
    m0c0 = m0.clone().filter_faces( su_dfa | su_dfb | su_mfa | su_mfb, False ).trim()
    m0cad = m0.clone().filter_faces( su_dfa, True ).filter_faces( su_dfb | su_mfb, False ).trim().recolor( color_scheme['-a'] )
    m0cam = m0.clone().filter_faces( su_mfa, True ).filter_faces( su_dfb | su_mfb, False ).trim().recolor( color_scheme['='] )
    m0cbd = m0.clone().filter_faces( su_dfb, True ).filter_faces( su_dfa | su_mfa, False ).trim().recolor( color_scheme['-b'] )
    m0cbm = m0.clone().filter_faces( su_mfb, True ).filter_faces( su_dfa | su_mfa, False ).trim().recolor( color_scheme['-b'] )
    m0cc = m0.clone().filter_faces( (su_dfa | su_mfa) & (su_dfb | su_mfb), True).trim().recolor( color_scheme['*'] )
    
    for m in [maa,mca,m0c0,m0cad,m0cam,mma,m0cbd,m0cbm,m0cc,mcb,mab,mmb]:
        m.rebuild_edges()
    
    meshes = [
        #ma.copy(),m0.copy(),mb.copy(),
        #maa.copy().extend(mca), #maa.copy().extend(mda),
        mca.copy().extend(maa).extend(mma),
        m0c0.copy().extend(m0cad).extend(m0cam).extend(m0cbd).extend(m0cbm).extend(m0cc),
        mcb.copy().extend(mab).extend(mmb), #mab.copy().extend(mdb),
        ]
    lcaptions = [ fn1, fn0, fn2 ]
    shapes = [ mesh.toShape() if mesh else None for mesh in meshes ]
    window = Viewer( shapes, ncols=3, caption='MeshGit Diff3 Viewer: %s <- %s -> %s' % (fn1,fn0,fn2), lcaptions=lcaptions )
    window.app_run()


def run_diff_viewer_series( *lfn, **dopts ):
    method = dopts.get( 'method', None )
    
    cm = len(lfn)
    lm = [ Mesh.fromPLY(fn) for fn in lfn ]
    
    # get list of commands to edit each mesh to become the mesh that follows
    lc01 = []
    for i in xrange(len(lm)-1):
        DebugWriter.printer( 'comparing %s -> %s' % (lfn[i],lfn[i+1]) ) # %i -> %i' % (i,i+1) )
        scaling_factor = scale( [ lm[i], lm[i+1] ] )
        ci = CorrespondenceInfo.build( lm[i], lm[i+1], method=method )
        lm[i+1] = ci.get_corresponding_mesh1( allow_face_sub=False )
        scale( [ lm[i], lm[i+1] ], 1.0 / scaling_factor )
        lc01 += [ diff_commands( lm[i], lm[i+1] ) ]
    
    # get distance that each vertex moves between mesh snapshots
    ldv1 = []
    for i in xrange(len(lm)-1):
        scaling_factor = scale( [lm[i],lm[i+1]], scaling_factor='unitcube' )
        m0,m1 = lm[i],lm[i+1]
        su_v0 = set( m0.get_lu_v() )
        ldv1 += [ dict( [ ( v.u_v, Vec3f.t_distance(v.p,m0.opt_get_v(v.u_v).p) ) for v in m1.lv if v.u_v in su_v0 ] ) ]
        scale( [ lm[i], lm[i+1] ], 1.0 / scaling_factor )
    
    map_fn( Mesh.optimize, lm )
    map_fn( Mesh.rebuild_edges, lm )
    
    lsu_df = [ set( cmd.split(' ')[1].split(':')[0] for cmd in c01 if cmd.startswith('delface') ) for c01 in lc01 ]
    lsu_af = [ set( cmd.split(' ')[1].split(':')[0] for cmd in c01 if cmd.startswith('addface') ) for c01 in lc01 ]
    lsu_mv = [ set( cmd.split(' ')[1].split(':')[0] for cmd in c01 if cmd.startswith('move') ) for c01 in lc01 ]
    
    ls = []
    lcaptions = []
    for i,m in enumerate(lm):
        su_df = lsu_df[i] if i < cm-1 else set()
        su_af = lsu_af[i-1] if i > 0 else set()
        su_mv = lsu_mv[i-1] if i > 0 else set()
        c01 = lc01[i] if i < cm-1 else []
        
        m.recolor( color_scheme['='] )
        
        mc = m.clone().filter_faces( su_af | su_df, False ).trim().recolor( color_scheme['='] )
        md = m.clone().filter_faces( su_df - su_af, True ).trim().recolor( color_scheme['-'] )
        ma = m.clone().filter_faces( su_af - su_df, True ).trim().recolor( color_scheme['+'] )
        mad = m.clone().filter_faces( su_df & su_af, True ).trim(). recolor( color_scheme['/'] )
        if i > 0:
            for v in mc.lv:
                if v.u_v not in su_mv: continue
                val = min( 1.0, ldv1[i-1][v.u_v] * 25.0 )
                v.c = Vec3f.t_lerp( color_scheme['='], color_scheme['>'], val )
        
        #for m in [mc,md,ma,mad]:
        #    m.rebuild_edges()
        map_fn( Mesh.rebuild_edges, [mc,md,ma,mad] )
        
        ls += [ mc.extend(md).extend(ma).extend(mad).toShape() ]
        lcaptions += [ 'series diff:\n%s' % ' -> '.join( lfn[_i] if _i>=0 and _i<=cm-1 else '(none)' for _i in [i-1,i,i+1] ) ]
    
    window = Viewer( ls, caption='MeshGit Series Diff Viewer: %s' % ' -> '.join( lfn ), lcaptions=lcaptions, ncols=5 )
    window.app_run()


import random

from vec3f import *
from mesh import *
from colors import *


# the color scheme for the viewer
color_scheme = {
    '=':  Colors.Gray,      # substitution with same positions
    '+':  Colors.nb_Green,  # added
    '-':  Colors.nb_Red,    # deleted
    '>':  Colors.ds_Blue,   # substitution with translation
    '+a': Colors.nb_Green,  # added to ver_a
    '-a': Colors.nb_Red,    # deled from ver_a
    '+b': Colors.nd_Green,  # added to ver_b
    '-b': Colors.nd_Red,    # deled from ver_b
    '*':  Colors.nb_Yellow, # conflict: deled from both versions (ver0->vera and ver0->verb)
    '/':  Colors.nb_Orange, # in series, added (ver0->ver1) then deleted (ver1->ver2)
    '!':  Colors.nb_Cyan,   # substituted faces (matching faces with mismatched verts)
    }

# return a color based on whether a val (u) is in at least two of the three sets of vals (su0,su1,su2)
#   if u is in at least two sets, return a random color
#   otherwise, return the "primary" color based on which set it is in (su0-red, su1-green, su2-blue, none-black)
def get_color( u, su0, su1, su2 ):
    lavoid = [ [1,0,0],[0,1,0],[0,0,1],[1,1,0],[0,1,1],[1,0,1],[0,0,0] ]
    lavoid += [ [0.5,0,0], [0,0.5,0], [0,0,0.5] ]
    r,g,b = [ 1 if u in su else 0 for su in [su0,su1,su2] ]
    if r+g+b >= 2:
        #return random_color( lavoid )
        return random_color_primaries()
    return [r*0.5,g*0.5,b*0.5]


# scales the meshes in lm by scaling_factor, which can be a float, None, or 'unitcube'
#   None      : scaling_factor is determined as the average length of edges
#   'unitcube': scaling_factor will scale the bounding box of all meshes to fit in a unit cube
def scale( lm, scaling_factor=None ):
    if scaling_factor is None: scaling_factor = 'avglen'
    
    if scaling_factor == 'unitcube':
        md = 0.0
        for m in lm:
            if len(m.lv) == 0: continue
            vm = vM = m.lv[0].p
            for v in m.lv:
                vm = [ min(vm[i],v.p[i]) for i in xrange(3) ]
                vM = [ max(vM[i],v.p[i]) for i in xrange(3) ]
            bb = [ vM[0]-vm[0], vM[1]-vm[1], vM[2]-vm[2] ]
            md = max(md,max(bb))
        assert md > 0.0
        scaling_factor = 1.0 / md
    
    elif scaling_factor == 'avglen':
        
        ma = 0.0
        for m in lm:
            m.rebuild_edges()
            m.optimize()
            s = 0.0
            c = 0
            for e in m.le:
                u_v0,u_v1 = e.lu_v
                s += Vec3f.t_distance( m.opt_get_v(u_v0).p, m.opt_get_v(u_v1).p )
                c += 1
            ma = max(ma,s/float(c))
        assert c != 0
        
        scaling_factor = 1.0 / ma
    
    for m in lm:
        for v in m.lv:
            v.p = Vec3f.t_scale( v.p, scaling_factor )
    
    return scaling_factor


def split_mesh( mesh, l_sufo ):
    return [ mesh.clone().filter_faces(su,fo) for su,fo in l_sufo ]


def get_colored_meshes_2way( m0, m1, epsilon=0.0000001 ):
    
    su_v0,su_v1 = set(m0.get_lu_v()),set(m1.get_lu_v())
    su_f0,su_f1 = set(m0.get_lu_f()),set(m1.get_lu_f())
    
    # get vert travel distance
    scaling_factor = scale( [m0,m1], scaling_factor='unitcube' )
    dvd = dict( [ ( v.u_v, Vec3f.t_distance(v.p,m0.opt_get_v(v.u_v).p) if v.u_v in su_v0 else 0 ) for v in m1.lv ] )
    scale( [m0,m1], (1.0/scaling_factor) )
    
    m0_u = m0.clone_filter_faces( su_f0 & su_f1, True ).recolor( color_scheme['='] )
    m0_d = m0.clone_filter_faces( su_f0 - su_f1, True ).recolor( color_scheme['-'] )
    
    m1_u = m1.clone_filter_faces( su_f1 & su_f0, True ).recolor( color_scheme['='] )
    m1_a = m1.clone_filter_faces( su_f1 - su_f0, True ).recolor( color_scheme['+'] )
    for v in m1_u.lv:
        val = min( 1.0, dvd[v.u_v] * 25.0 )
        v.c = Vec3f.t_lerp( color_scheme['='], color_scheme['>'], val )
    
    m0_c = m0_u.extend(m0_d,ignore_edges=True).trim().recalc_vertex_normals().optimize()
    m1_c = m1_u.extend(m1_a,ignore_edges=True).trim().recalc_vertex_normals().optimize()
    
    return (m0_c,m1_c)

def get_colored_meshes_3way( m0, ma, mb, epsilon=0.0000001 ):
    
    su_v0,su_va,su_vb = set(m0.get_lu_v()),set(ma.get_lu_v()),set(mb.get_lu_v())
    su_f0,su_fa,su_fb = set(m0.get_lu_f()),set(ma.get_lu_f()),set(mb.get_lu_f())
    
    # get vert travel distance
    scaling_factor = scale( [m0,ma,mb], scaling_factor='unitcube' )
    dvd0a = dict( [ ( v.u_v, Vec3f.t_distance(v.p,m0.opt_get_v(v.u_v).p) if v.u_v in su_v0 else 0 ) for v in ma.lv ] )
    dvd0b = dict( [ ( v.u_v, Vec3f.t_distance(v.p,m0.opt_get_v(v.u_v).p) if v.u_v in su_v0 else 0 ) for v in mb.lv ] )
    scale( [m0,ma,mb], (1.0/scaling_factor) )
    
    m0_u = m0.clone_filter_faces( su_f0 & su_fa & su_fb, True ).recolor( color_scheme['='] )
    m0_da = m0.clone_filter_faces( (su_f0 - su_fa) - (su_f0 - su_fb), True ).recolor( color_scheme['-a'] )
    m0_db = m0.clone_filter_faces( (su_f0 - su_fb) - (su_f0 - su_fa), True ).recolor( color_scheme['-b'] )
    m0_dc = m0.clone_filter_faces( (su_f0 - su_fa) & (su_f0 - su_fb), True ).recolor( color_scheme['*'] )
    
    ma_u = ma.clone_filter_faces( su_fa & su_f0, True ).recolor( color_scheme['='] )
    ma_a = ma.clone_filter_faces( su_fa - su_f0, True ).recolor( color_scheme['+a'] )
    for v in ma_u.lv:
        val = min( 1.0, dvd0a[v.u_v] * 25.0 )
        v.c = Vec3f.t_lerp( color_scheme['='], color_scheme['>'], val )
    
    mb_u = mb.clone_filter_faces( su_fb & su_f0, True ).recolor( color_scheme['='] )
    mb_a = mb.clone_filter_faces( su_fb - su_f0, True ).recolor( color_scheme['+b'] )
    for v in mb_u.lv:
        val = min( 1.0, dvd0b[v.u_v] * 25.0 )
        v.c = Vec3f.t_lerp( color_scheme['='], color_scheme['>'], val )
    
    m0_c = m0_u.extend(m0_da,ignore_edges=True).extend(m0_db,ignore_edges=True).extend(m0_dc,ignore_edges=True).trim().recalc_vertex_normals().optimize()
    ma_c = ma_u.extend(ma_a,ignore_edges=True).trim().recalc_vertex_normals().optimize()
    mb_c = mb_u.extend(mb_a,ignore_edges=True).trim().recalc_vertex_normals().optimize()
    
    return (m0_c,ma_c,mb_c)

def get_colored_mesh_3way( m0, ma, mb, mm, epsilon=0.0000001, color_deletions=False ):
    scaling_factor = scale( [m0,mm], scaling_factor='unitcube' )
    su_v0 = set( m0.get_lu_v() )
    dvd = dict( [ ( v.u_v, Vec3f.t_distance(v.p,m0.opt_get_v(v.u_v).p) ) for v in mm.lv if v.u_v in su_v0 ] )
    scale( [m0,mm], (1.0/scaling_factor) )
    
    suvm = set( v.u_v for v in mm.lv if v.u_v in su_v0 and Vec3f.t_distance2(v.p,m0.opt_get_v(v.u_v).p) > epsilon )
    
    suf0,sufa,sufb,sufm = set(m0.get_lu_f()),set(ma.get_lu_f()),set(mb.get_lu_f()),set(mm.get_lu_f())
    mm_0,mm_a,mm_b = [ mm.clone() for i in xrange(3) ]
    mm_0.filter_faces(suf0,True).trim().optimize()
    for v in mm_0.lv:
        if v.u_v in suvm:
            val = min( 1.0, dvd[v.u_v] * 25.0 )
            v.c = Vec3f.t_lerp( color_scheme['='], color_scheme['>'], val )
        else:
            v.c = color_scheme['=']
    
    if color_deletions:
        s_same = suf0 & sufa & sufb
        s_del_ab = suf0 - ( sufa | sufb )
        s_del_a = suf0 - sufa - s_del_ab
        s_del_b = suf0 - sufb - s_del_ab
        
        mm_00 = mm_0.clone_filter_faces( s_same, True ).trim().optimize()
        mm_0a = mm_0.clone_filter_faces( s_del_a, True ).trim().recolor( color_scheme['-a'] ).optimize()
        mm_0b = mm_0.clone_filter_faces( s_del_b, True ).trim().recolor( color_scheme['-b'] ).optimize()
        mm_0c = mm_0.clone_filter_faces( s_del_ab, True ).trim().recolor( color_scheme['*'] ).optimize()
        
        mc = mm_00
        mc.extend( mm_0a, ignore_edges=True )
        mc.extend( mm_0b, ignore_edges=True )
        mc.extend( mm_0c, ignore_edges=True )
    else:
        mc = mm_0
        
    mm_a.filter_faces(sufa-suf0,True).trim().recolor( color_scheme['+a'] ).optimize()
    mm_b.filter_faces(sufb-suf0,True).trim().recolor( color_scheme['+b'] ).optimize()
    mc.extend(mm_a,ignore_edges=True).extend(mm_b,ignore_edges=True)
    
    mc.trim().recalc_vertex_normals().optimize()
    
    return mc




#!/usr/bin/env python

import sys, os

from ply import *

if len(sys.argv) != 2:
    print( 'Usage: %s [plyfilename]' % sys.argv[0] )
    exit( 1 )

fnply = sys.argv[1]
fn = os.path.splitext( fnply )[0]
fnoff = '%s.off' % fn

obj = ply_load( fnply )

l_v = obj['lv']
l_f = obj['lf']
s_e = set( (i_v0,i_v1) for li_v in l_f for i_v0,i_v1 in zip(li_v,li_v[1:]+[li_v[0]]) )
c_v = len(l_v)
c_f = len(l_f)
c_e = len(s_e)

# http://people.sc.fsu.edu/~jburkardt/data/off/off.html
fp = open( fnoff, 'wt' )

fp.write( 'OFF\n' )
fp.write( '%d\t%d\t%d\n' % (c_v,c_f,c_e) )
for v in l_v:
    fp.write( '%f\t%f\t%f\n' % tuple(v) )
for li_v in l_f:
    ls_i_v = [ '%d'%i_v for i_v in li_v ]
    fp.write( '%d\t%s\n' % ( len(li_v), ' '.join(ls_i_v) ) )

fp.close()

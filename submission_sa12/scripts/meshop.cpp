#include <vector>
#include <stdio.h>
#include <stdlib.h>

#include "meshop.h"
#include "quicksort.h"

using namespace std;

// forward declarations of static variables
MeshOp *MeshOp::head_cost;
MeshOp *MeshOp::head_pull;
MeshOp **MeshOp::head_i0;
MeshOp **MeshOp::head_i1;
int MeshOp::c_i0;
int MeshOp::c_i1;



void MeshOp::init( int c_i0, int c_i1 )
{
    MeshOp::head_cost = 0;
    MeshOp::head_pull = 0;
    MeshOp::head_i0 = (MeshOp**) calloc( c_i0, sizeof(MeshOp*) );
    MeshOp::head_i1 = (MeshOp**) calloc( c_i1, sizeof(MeshOp*) );
    MeshOp::c_i0 = c_i0;
    MeshOp::c_i1 = c_i1;
}

MeshOp *MeshOp::create( int i0, int i1 )
{
    MeshOp *m = get_meshop( i0, i1 );
    if( m != 0 ) return m;
    return new MeshOp( i0, i1 );
}
MeshOp *MeshOp::create( int i0, int i1, bool adddel )
{
    MeshOp *m = get_meshop( i0, i1 );
    if( m != 0 ) return m;
    return new MeshOp( i0, i1, adddel );
}


#define ASSERT( cond, statement )       if( !(cond) ) { printf( statement ); exit(1); }
void MeshOp::validate()
{
    MeshOp *m;
    
    for( m = MeshOp::head_cost; m != 0; m = m->n_cost )
    {
        if( m == MeshOp::head_cost )
            ASSERT( m->p_cost == 0, "head_cost->p_cost != 0\n" );
        if( m->n_cost != 0 )
            ASSERT( m->n_cost->p_cost == m, "m->n_cost->p_cost != m\n" )
    }
    for( m = MeshOp::head_pull; m != 0; m = m->n_pull )
    {
        if( m == MeshOp::head_pull )
            ASSERT( m->p_pull == 0, "head_pull->p_pull != 0\n" );
        if( m->n_pull != 0 )
            ASSERT( m->n_pull->p_pull == m, "m->n_pull->p_pull != m\n" )
    }
    for( int i = 0; i < MeshOp::c_i0; i++ )
    {
        for( m = MeshOp::head_i0[i]; m != 0; m = m->n_i0 )
        {
            if( m == MeshOp::head_i0[i] )
                ASSERT( m->p_i0 == 0, "head_i0->p_i0 != 0\n" );
            if( m->n_i0 != 0 )
                ASSERT( m->n_i0->p_i0 == m, "m->n_i0->p_i0 != m\n" )
        }
    }
    for( int i = 0; i < MeshOp::c_i1; i++ )
    {
        for( m = MeshOp::head_i1[i]; m != 0; m = m->n_i1 )
        {
            if( m == MeshOp::head_i1[i] )
                ASSERT( m->p_i1 == 0, "head_i1->p_i1 != 0\n" );
            if( m->n_i1 != 0 )
                ASSERT( m->n_i1->p_i1 == m, "m->n_i1->p_i1 != m\n" )
        }
    }
}

MeshOp *MeshOp::get_meshop( int i0, int i1 )
{
    for( MeshOp *m = MeshOp::head_i0[i0]; m != 0; m=m->n_i0 ) if( m->i1 == i1 ) return m;
    return 0;
}

int MeshOp::get_cost_length()
{
    int i = 0;
    for( MeshOp *_m = MeshOp::head_cost; _m != 0; _m = _m->n_cost ) i++;
    return i;
}
int MeshOp::get_pull_length()
{
    int i = 0;
    for( MeshOp *_m = MeshOp::head_pull; _m != 0; _m = _m->n_pull ) i++;
    return i;
}
MeshOp *MeshOp::get_pull_head() { return MeshOp::head_pull; }
MeshOp *MeshOp::get_i0_head( int i0 ) { return MeshOp::head_i0[i0]; }
MeshOp *MeshOp::get_i1_head( int i1 ) { return MeshOp::head_i1[i1]; }




void MeshOp::insort_pull()
{
    while( MeshOp::head_pull != 0 ) MeshOp::insort( MeshOp::head_pull );
}
void MeshOp::insort( MeshOp* meshop )
{
    if( meshop == 0 ) return;
    
    if( MeshOp::head_cost == 0 ) {
        MeshOp::head_cost = meshop;
        meshop->remove_pull();
        return;
    }
    
    MeshOp *m0 = MeshOp::head_cost;
    while( m0->cost <= meshop->cost && m0->n_cost != 0 ) m0 = m0->n_cost;
    
    if( m0->cost <= meshop->cost && m0->n_cost == 0 ) {
        m0->n_cost = meshop;
        meshop->remove_pull();
        return;
    }
    
    if( m0->p_cost != 0 ) m0->p_cost->n_cost = meshop;
    meshop->p_cost = m0->p_cost;
    m0->p_cost = meshop;
    meshop->n_cost = m0;
    
    meshop->remove_pull();
    
    if( MeshOp::head_cost == m0 ) MeshOp::head_cost = meshop;
}

// uses quicksort.  typical: O(n log n), worst: O(n^2)
MeshOp *MeshOp::sort_pull_quicksort()
{
    MeshOp *meshop = MeshOp::head_pull;
    if( meshop == 0 ) return meshop;
    int l = MeshOp::get_pull_length();
    
    MeshOp **array = (MeshOp**) calloc( l, sizeof(MeshOp*) );
    float *costs   = (float*)   calloc( l, sizeof(float) );
    unsigned int *inds      = (unsigned int*)     calloc( l, sizeof(unsigned int) );
    
    for( unsigned int i = 0; i < l; i++, meshop=meshop->n_pull )
    {
        array[i] = meshop;
        costs[i] = meshop->cost;
        inds[i] = i;
    }
    
    quicksort_inds( costs, inds, l );
    MeshOp *head = array[inds[0]];
    
    for( int i = 0; i < l; i++ )
    {
        meshop = array[inds[i]];
        meshop->p_cost = i==0   ? 0 : array[inds[i-1]];
        meshop->n_cost = i==l-1 ? 0 : array[inds[i+1]];
        meshop->p_pull = meshop->n_pull = 0;
        meshop->pulled = false;
    }
    free( array ); free( costs ); free( inds );
    
    MeshOp::head_pull = 0;
    return head;
}

MeshOp *MeshOp::sort_pull_mergesort()
{
    if( MeshOp::head_pull == 0 ) return 0;
    
    MeshOp *head = sort_pull_mergesort( MeshOp::head_pull, 0, 0 );
    
    // convert sorted pull-chain into cost-chain
    head->p_cost = 0;
    for( MeshOp *m = head; m != 0; )
    {
        MeshOp *n = m->n_pull;
        m->n_cost = n; if( n != 0 ) n->p_cost = m;
        m->n_pull = m->p_pull = 0; m->pulled = false;
        m = n;
    }
    
    MeshOp::head_pull = 0;
    return head;
}
// below is from: http://www.c.happycodings.com/Sorting_Searching/code10.html
// this link seems easier to read and seems correct: http://www.geeksforgeeks.org/archives/7740
MeshOp *MeshOp::sort_pull_mergesort( MeshOp *head, int istart, int depth )
{
    MeshOp *head1, *head2, *m;
    
    if( head == 0 || head->n_pull == 0 ) return head;       // no need to sort
    
    // find half-way point
    head1 = head; head2 = head->n_pull;
    int count = 0;
    while( head2 != 0 && head2->n_pull != 0 )
    {
        head = head->n_pull;
        head2 = head2->n_pull->n_pull;          // corrected from: head2 = head->n_pull->n_pull
        count++;
    }
    head2 = head->n_pull;
    if( head2 != 0 ) { head->n_pull = 0; head2->p_pull = 0; }
    
    //printf( "%d: %d -- %d -- ~%d\n", depth, istart, istart+count, istart+(count*2) );
    
    // sort two halves
    head1 = sort_pull_mergesort( head1, istart, depth+1 ); head2 = sort_pull_mergesort( head2, istart+count, depth+1 );
    
    // find cheaper of two
    if( head1->cost < head2->cost ) {
        head = head1; head1 = head1->n_pull;
    } else {
        head = head2; head2 = head2->n_pull;
    }
    head->p_pull = 0;
    m = head;
    
    // mergesort
    while( head1 != 0 || head2 != 0 )
    {
        if( head2 == 0 || head1 != 0 && head1->cost < head2->cost ) {
            m->n_pull = head1; head1->p_pull = m;
            m = head1; head1 = head1->n_pull;
        } else {
            m->n_pull = head2; head2->p_pull = m;
            m = head2; head2 = head2->n_pull;
        }
    }
    //if( head1 != 0 ) { e->n_pull = head1; head1->p_pull = e; }
    //else { e->n_pull = head2; head2->p_pull = e; }
    
    return head;
}

void MeshOp::insort_cost( MeshOp *sorted )
{
    if( sorted == 0 ) return;
    if( MeshOp::head_cost == 0 ) { MeshOp::head_cost = sorted; return; }
    
    MeshOp *m0 = MeshOp::head_cost;
    MeshOp *m1 = sorted;
    
    MeshOp *m;
    if( m0->cost <= m1->cost ) {
        m = m0; m0 = m0->n_cost;
    } else {
        m = m1; m1 = m1->n_cost;
    }
    MeshOp::head_cost = m;
    
    while( m0 != 0 && m1 != 0 )
    {
        if( m0->cost <= m1->cost ) {
            m0->p_cost = m;
            m->n_cost = m0;
            while( m0 != 0 && m0->cost <= m1->cost ) m0 = m0->n_cost;
            if( m0 != 0 ) m = m0->p_cost;
        } else {
            m1->p_cost = m;
            m->n_cost = m1;
            while( m1 != 0 && m1->cost < m0->cost ) m1 = m1->n_cost;
            if( m1 != 0 ) m = m1->p_cost;
        }
    }
}

void MeshOp::insort_sorted_pull()
{
    MeshOp *sorted = sort_pull_quicksort();
    insort_cost( sorted );
}




void MeshOp::prepend_i0( MeshOp *meshop )
{
    int i0 = meshop->i0;
    if( MeshOp::head_i0[i0] == 0 ) {
        MeshOp::head_i0[i0] = meshop;
    } else {
        MeshOp *ohead = MeshOp::head_i0[i0];
        MeshOp::head_i0[i0] = meshop;
        meshop->n_i0 = ohead;
        ohead->p_i0 = meshop;
    }
}
void MeshOp::prepend_i1( MeshOp *meshop )
{
    int i1 = meshop->i1;
    if( MeshOp::head_i1[i1] == 0 ) {
        MeshOp::head_i1[i1] = meshop;
    } else {
        MeshOp *ohead = MeshOp::head_i1[i1];
        MeshOp::head_i1[i1] = meshop;
        meshop->n_i1 = ohead;
        ohead->p_i1 = meshop;
    }
}
void MeshOp::prepend_pull( MeshOp *meshop )
{
    if( meshop->pulled ) return;
    if( meshop->killed ) { printf( "pulling a killed MeshOp!\n" ); exit(1); }
    
    if( MeshOp::head_pull == 0 ) {
        MeshOp::head_pull = meshop;
    } else {
        MeshOp *ohead = MeshOp::head_pull;
        MeshOp::head_pull = meshop;
        meshop->n_pull = ohead;
        ohead->p_pull = meshop;
    }
    meshop->remove_cost();
    meshop->pulled = true;
}

MeshOp *MeshOp::pop_cost()
{
    MeshOp *ohead = MeshOp::head_cost;
    if( ohead == 0 ) return 0;
    MeshOp *nhead = ohead->n_cost;
    MeshOp::head_cost = nhead;
    if( nhead != 0 ) nhead->p_cost = 0;
    ohead->p_cost = ohead->n_cost = 0;
    return ohead;
}
void MeshOp::pull_i0( int i0 )
{
    for( MeshOp *_m = MeshOp::head_i0[i0]; _m != 0; _m = _m->n_i0 ) prepend_pull( _m );
}
void MeshOp::pull_i1( int i1 )
{
    for( MeshOp *_m = MeshOp::head_i1[i1]; _m != 0; _m = _m->n_i1 ) prepend_pull( _m );
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

MeshOp::MeshOp( int i0, int i1 ) { init( i0, i1, true ); }
MeshOp::MeshOp( int i0, int i1, bool adddel ) { init( i0, i1, adddel ); }

void MeshOp::init( int i0, int i1, bool adddel )
{
    this->i0 = i0;
    this->i1 = i1;
    this->adddel = adddel;
    
    cost = 0.0f;
    pulled = false;
    killed = false;
    
    n_cost = p_cost = 0;
    n_pull = p_pull = 0;
    n_i0   = p_i0   = 0;
    n_i1   = p_i1   = 0;
    
    // add this to head_i0 and head_i1
    MeshOp::prepend_pull( this );
    MeshOp::prepend_i0( this );
    MeshOp::prepend_i1( this );
}

void MeshOp::set_cost( float cost ) { this->cost = cost; }
float MeshOp::get_cost() { return this->cost; }
int MeshOp::get_i0() { return this->i0; }
int MeshOp::get_i1() { return this->i1; }
bool MeshOp::get_adddel() { return this->adddel; }

MeshOp *MeshOp::get_n_cost() { return this->n_cost; }
MeshOp *MeshOp::get_n_pull() { return this->n_pull; }
MeshOp *MeshOp::get_n_i0() { return this->n_i0; }
MeshOp *MeshOp::get_n_i1() { return this->n_i1; }

void MeshOp::clear_pull()
{
    while( MeshOp::head_pull ) MeshOp::head_pull->remove_pull();
}
void MeshOp::kill_pull()
{
    while( MeshOp::head_pull != 0 ) MeshOp::head_pull->remove_all();
}
void MeshOp::kill_i0( int i0 )
{
    while( MeshOp::head_i0[i0] != 0 ) MeshOp::head_i0[i0]->remove_all();
}
void MeshOp::kill_i1( int i1 )
{
    while( MeshOp::head_i1[i1] != 0 ) MeshOp::head_i1[i1]->remove_all();
}

// O(1)
void MeshOp::remove_all()
{
    remove_cost();
    remove_pull();
    remove_i0();
    remove_i1();
    killed = true;
}
// O(1)
void MeshOp::remove_cost()
{
    if( MeshOp::head_cost == this ) MeshOp::head_cost = n_cost;
    if( p_cost != 0 ) p_cost->n_cost = n_cost;
    if( n_cost != 0 ) n_cost->p_cost = p_cost;
    p_cost = n_cost = 0;
}
// O(1)
void MeshOp::remove_pull()
{
    if( MeshOp::head_pull == this ) MeshOp::head_pull = n_pull;
    if( p_pull != 0 ) p_pull->n_pull = n_pull;
    if( n_pull != 0 ) n_pull->p_pull = p_pull;
    p_pull = n_pull = 0;
    pulled = false;
}
// O(1)
void MeshOp::remove_i0()
{
    if( MeshOp::head_i0[i0] == this ) MeshOp::head_i0[i0] = n_i0;
    if( p_i0 != 0 ) p_i0->n_i0 = n_i0;
    if( n_i0 != 0 ) n_i0->p_i0 = p_i0;
    p_i0 = n_i0 = 0;
}
// O(1)
void MeshOp::remove_i1()
{
    if( MeshOp::head_i1[i1] == this ) MeshOp::head_i1[i1] = n_i1;
    if( p_i1 != 0 ) p_i1->n_i1 = n_i1;
    if( n_i1 != 0 ) n_i1->p_i1 = p_i1;
    p_i1 = n_i1 = 0;
}


import math, sys, time, pyglet
from pyglet.gl import *
from pyglet.window import key

from vec3f import *
from camera import *
from shapes import *
from miscfuncs import *
from enum import Enum, EnumVal

from debugwriter import DebugWriter

# The code below is based on the following:
#   original c-based: Richard Campbell '99, http://nehe.gamedev.net
#   python/pyopengl port: John Ferguson '00, hakuin@voicenet.com
#   pyglet port: Jess Hill (Jestermon) '09, jestermon.weebly.com, jestermonster@gmail.com

# refs:
#   http://pyglet.org/doc/programming_guide.pdf
#   http://swiftcoder.wordpress.com/2008/12/08/pong/
#   http://tartley.com/files/stretching_pyglets_wings/presentation/
#   http://jestermon.weebly.com/nehe-pyglet-tutorials.html


LightingModes    = Enum( 'NO_LIGHT', 'SIMPLE', 'SIMPLE_FALLOFF' )
BackgroundColors = Enum( 'BLACK', 'GRAY', 'WHITE' )
ShapeViews       = Enum( 'VERTS', 'WIRES', 'VERTS_WIRES', 'FACES', 'WIRES_FACES' )
OffsetValues     = Enum( 'NONE', 'SMALL', 'LARGE' )


class Viewer( pyglet.window.Window ):
    
    def __init__( self, shapes, ncols=None, caption=None, lcaptions=None, on_key_callback=None, size=None, on_draw_start_callback=None, on_draw_end_callback=None ):
        self.on_draw_start_callback = on_draw_start_callback
        self.on_draw_end_callback = on_draw_end_callback
        self.on_key_callback = on_key_callback
        
        config = Config( sample_buffers=2, samples=4, depth_size=32, double_buffer=True )
        try:
            super(Viewer,self).__init__(resizable=True, config=config)
        except:
            super(Viewer,self).__init__(resizable=True)
        
        self.shapes = [None]
        self.ncols = 1
        self.nrows = 1
        self.lastshot = None
        self.shots = 0
        self.lcaptions = lcaptions
        
        self._setup()
        self._initgl()
        
        pyglet.clock.schedule_interval( self.update, 1.0/60.0 )
        
        if shapes:
            self.setup( shapes, ncols=ncols, size=size, lcaptions=lcaptions, caption=caption )
        else:
            if caption is not None: self.set_caption( caption )
    
    def app_run( self ):
        pyglet.app.run()
        #self.close()
    
    def setup( self, shapes, ncols=None, size=None, lcaptions=None, caption=None ):
        if caption is None: caption = "pyglet 3D Viewer"
        if lcaptions is None: lcaptions = []
        if not ncols: ncols = len(shapes)
        if size is None: size = (100,100)
        
        self.set_caption( caption )
        self.lcaptions = lcaptions
        
        self.ncols = min(ncols,len(shapes))
        self.nrows = max(1,int(math.ceil(float(len(shapes))/float(ncols))))
        self.shapes = shapes
        
        self.vw,self.vh = size
        self.width,self.height = self.vw*self.ncols,self.vh*self.nrows
        self.set_location( (self.screen.width - self.width) / 2, (self.screen.height - self.height) / 2 )
        self.lcaptions = lcaptions
        self._setup_labels()
    
    def _setup( self ):
        self.tx,self.ty,self.cd = -math.pi/4.0,math.pi/4.0+0.05,6.0
        self.camera = Camera( (0.0,0.0,-6.0), (0.0,0.0,0.0), (0.0,0.0,1.0) )
        
        self.light = LightingModes.newval( LightingModes.SIMPLE )
        self.axes = False
        self.grid = False
        self.shapeview = ShapeViews.newval( ShapeViews.WIRES_FACES )
        self.separated = True
        self.bg = BackgroundColors.newval( BackgroundColors.WHITE )
        self.mouse = True
        self.dividers = False
        self.labels = True
        self.offset = OffsetValues.newval( OffsetValues.LARGE )
        self.showonly = -1
        self.line_width = 1.0
        
        self._lighting = True
        
        self.mx,self.my,self.mb = -50,50,0
        
        self.keymap = pyglet.window.key.KeyStateHandler()
        self.push_handlers(self.keymap)
        
        self.shape_axes = Shape( GL_LINES, [ 0,0,0, 10,0,0, 0,0,0, 0,10,0, 0,0,0, 0,0,10 ], [ 1,0,0, 1,0,0, 0,1,0, 0,1,0, 0,0,1, 0,0,1 ] )
        gridlines = [ c for x in [ [ i,10,0, i,-10,0, 10,i,0, -10,i,0 ] for i in range(-10,11) ] for c in x ]
        self.shape_grid = Shape( GL_LINES, gridlines, [ 0.15, 0.15, 0.15 ] )
        
        pyglet.font.add_file( find_syspath_to_file('ProggyTiny.ttf') )
    
    def _setup_labels( self ):
        if self.lcaptions is None: self.lcaptions = []
        self.label = pyglet.text.Label(self.caption, x=0,y=0,color=(255,255,0,255), anchor_x='left',anchor_y='top', font_size=12,font_name='ProggyTinyTT' )
        self.llabels = [ pyglet.text.Label( cap, x=0,y=0,color=(255,255,0,255), anchor_x='left',anchor_y='top',multiline=True,width=1000, font_size=12,font_name='ProggyTinyTT' ) for cap in self.lcaptions ]
    
    def _initgl( self ):
        glClearDepth( 1.0 )
        
        glDepthFunc( GL_LEQUAL )
        glEnable( GL_DEPTH_TEST )
        glBlendFunc( GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA )
        glEnable( GL_BLEND )
        #glPolygonOffset( 10.0, 10.0 )
        
        glShadeModel( GL_SMOOTH )
        #glEnable(GL_LINE_SMOOTH )
        
        glHint( GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST )
        glLightfv( GL_LIGHT1, GL_AMBIENT, self.vec( 0.4, 0.4, 0.4, 1.0 ) )
        #glLightfv( GL_LIGHT1, GL_DIFFUSE, self.vec( 0.5, 0.5, 0.5, 1.0 ) )
        glLightfv( GL_LIGHT1, GL_DIFFUSE, self.vec( 0.75, 0.75, 0.75, 1.0 ) )
        glLightfv( GL_LIGHT1, GL_SPECULAR, self.vec( 0.75, 0.75, 0.75, 1.0 ) )
        glLightfv( GL_LIGHT1, GL_POSITION, self.vec( 0.0, 0.1, 0.0, 1.0 ) )
        glEnable( GL_LIGHT1 )
        
        glLightModeli( GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE )
        glDisable( GL_CULL_FACE )
        
        glEnable(GL_COLOR_MATERIAL)
        glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE )
    
    def update( self, dt ):
        self.handle_keys()
        self.draw_scene()
    
    def on_draw(self):
        self.draw_scene()
    
    def on_resize( self, w, h ):
        self.ReSizeGLScene( w, h )
    
    def UpdateGL( self ):
        if self.light: glEnable( GL_LIGHTING )
        else: glDisable( GL_LIGHTING )
        
        e = (self.cd,0,0)
        e = self.rot_about_direction( e, (0,1,0), self.tx )
        e = self.rot_about_direction( e, (0,0,1), self.ty )
        e = (e[0]+self.camera.fx,e[1]+self.camera.fy,e[2]+self.camera.fz)
        self.camera.ex,self.camera.ey,self.camera.ez = e
        
        #ux,uy,uz = self.camera.get_up()
        #e = [ self.camera.ex+ux*0.1, self.camera.ey+uy*0.1, self.camera.ez+uz*0.1 ]
        #e = [ self.camera.ex, self.camera.ey, self.camera.ez ]
        glLightfv( GL_LIGHT1, GL_POSITION, self.vec( 0.0, 0.1, 0.0, 1.0 ) )
        
        if self.bg == BackgroundColors.BLACK:   glClearColor( 0.0, 0.0, 0.0, 1.0 )
        elif self.bg == BackgroundColors.GRAY:  glClearColor( 0.25, 0.25, 0.25, 1.0 )
        elif self.bg == BackgroundColors.WHITE: glClearColor( 1.0, 1.0, 1.0, 1.0 )
        
        if self.light == LightingModes.NO_LIGHT:
            glDisable( GL_LIGHTING )
        elif self.light == LightingModes.SIMPLE:
            glEnable( GL_LIGHTING )
            glLightf( GL_LIGHT1, GL_CONSTANT_ATTENUATION, 1.0 )
            glLightf( GL_LIGHT1, GL_QUADRATIC_ATTENUATION, 0.0 )
            glLightfv( GL_LIGHT1, GL_AMBIENT, self.vec( 0.4, 0.4, 0.4, 1.0 ) )
            glLightfv( GL_LIGHT1, GL_DIFFUSE, self.vec( 0.75, 0.75, 0.75, 1.0 ) ) # 0.5
        elif self.light == LightingModes.SIMPLE_FALLOFF:
            glEnable( GL_LIGHTING )
            glLightf( GL_LIGHT1, GL_CONSTANT_ATTENUATION, 1.0 )
            glLightf( GL_LIGHT1, GL_QUADRATIC_ATTENUATION, 0.025 ) # 0.075
            glLightfv( GL_LIGHT1, GL_AMBIENT, self.vec( 0.8, 0.8, 0.8, 1.0 ) )
            glLightfv( GL_LIGHT1, GL_DIFFUSE, self.vec( 1.0, 1.0, 1.0, 1.0 ) )
    
    def ReSizeGLScene( self, width, height ):
        if height == 0: height = 1
        self.vw,self.vh = width/self.ncols,height/self.nrows
    
    def center_window( self ):
        l = max( self.screen.width/2 - self.width/2, 0 )
        t = max( self.screen.height/2 - self.height/2, 50 )
        self.set_location(l,t)
    
    
    def screenshot( self ):
        # http://docs.python.org/library/time.html#time.struct_time
        # struct_time: ( year mon mday hour min sec wday yday isdst )
        t = time.localtime()
        self.shots = self.shots+1 if t==self.lastshot else 0
        self.lastshot = t
        fn = 'shot_%04i%02i%02i_%02i%02i%02i_%02i.png' % (t[0],t[1],t[2],t[3],t[4],t[5],self.shots)
        print( 'saving screenshot: %s' % fn )
        pyglet.image.get_buffer_manager().get_color_buffer().save( fn )
    
    def save_state( self ):
        c = {
            'ver': 1.2,
            'e': (self.camera.ex,self.camera.ey,self.camera.ez),
            'f': (self.camera.fx,self.camera.fy,self.camera.fz),
            'u': (self.camera.ux,self.camera.uy,self.camera.uz),
            'angle': self.camera.angle,
            'scale': self.camera.scale,
            't': (self.tx,self.ty,self.cd),
            'light': self.light.val,
            'axes': self.axes,
            'grid': self.grid,
            'shapeview': self.shapeview.val,
            'separated': self.separated,
            'bg': self.bg.val,
            'mouse': self.mouse,
            'dividers': self.dividers,
            'labels': self.labels,
            'winsize': (self.width, self.height),
            'offset': self.offset.val,
            }
        
        pickle.dump( c, open( 'state.dat', 'w' ) )
        print( 'saved state' )
    
    def load_state( self ):
        c = pickle.load( open( 'state.dat', 'r' ) )
        
        if c['ver'] not in [1.0, 1.1, 1.2]:
            print( 'unknown state version' )
            return
        
        if c['ver'] in [1.0, 1.1, 1.2]:
            (self.camera.ex,self.camera.ey,self.camera.ez) = c['e']
            (self.camera.fx,self.camera.fy,self.camera.fz) = c['f']
            (self.camera.ux,self.camera.uy,self.camera.uz) = c['u']
            self.camera.angle = c['angle']
            self.camera.scale = c['scale']
            (self.tx,self.ty,self.cd) = c['t']
            self.light.val = c['light']
            self.axes = c['axes']
            self.grid = c['grid']
            self.shapeview.val = c['shapeview']
            self.separated = c['separated']
            self.bg.val = c['bg']
            self.mouse = c['mouse']
            self.dividers = c['dividers']
            self.labels = c['labels']
        
        if c['ver'] in [ 1.1, 1.2 ]:
            self.width,self.height = c['winsize']
        
        if c['ver'] in [ 1.2 ]:
            self.offset.val = c['offset']
        
        print( 'loaded state' )
    
    # turns tuple into GL vector
    def vec( self, *args ):
        return (GLfloat * len(args))(*args)
    
    def _screen_to_gl( self, x, y, b ):
        return ( x, y - self.height, b )
    
    
    
    def rot_about_direction( self, p, d, theta ):
        x,y,z = p
        u,v,w = d
        uxvywz,uvw,uvw2 = (u*x+v*y+w*z),math.sqrt(u*u+v*v+w*w),u*u+v*v+w*w
        ct,st = math.cos(theta),math.sin(theta)
        return (
            (u*uxvywz*(1-ct) + uvw2*x*ct + uvw*(v*z-w*y)*st) / uvw2,
            (v*uxvywz*(1-ct) + uvw2*y*ct + uvw*(w*x-u*z)*st) / uvw2,
            (w*uxvywz*(1-ct) + uvw2*z*ct + uvw*(u*y-v*x)*st) / uvw2,
            )
    
    def tumble( self, dtx, dty ):
        self.tx = max( -math.pi/2+0.01, min( math.pi/2-0.01, self.tx+dtx ) )
        self.ty += dty
    
    def dolly( self, dinout ):
        self.cd *= dinout
    
    def move( self, up, rt, fd ):
        rx,ry,rz = self.camera.get_right()
        ux,uy,uz = self.camera.get_up()
        fx,fy,fz = self.camera.get_fwd()
        d = self.camera.get_dist()
        
        rv = rt * d * 0.002
        uv = up * d * 0.002
        fv = fd * d * 0.002
        
        self.camera.fx += rx*rv + ux*uv + fx*fv
        self.camera.fy += ry*rv + uy*uv + fy*fv
        self.camera.fz += rz*rv + uz*uv + fz*fv
    
    
    def _light_toggle( self ):
        self._lighting = not self._lighting
        if self._lighting:
            if self.light != LightingModes.NO_LIGHT:
                glEnable( GL_LIGHTING )
        else:
            if self.light != LightingModes.NO_LIGHT:
                glDisable( GL_LIGHTING )
    
    def no_lights_decorator( func ):
        def toggle_lights( self, *args, **kw ):
            self._light_toggle()
            v = func( self, *args, **kw )
            self._light_toggle()
            return v
        return toggle_lights
    
    
    
    
    
    def draw_scene( self ):
        if self.context is None or self.has_exit: return
        
        self.UpdateGL()
        glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT )
        
        ls = self.shapes
        ll = self.llabels
        if self.showonly != -1:
            ls = [ s if i==self.showonly else None for i,s in enumerate(ls) ]
            ll = [ l if i==self.showonly else None for i,l in enumerate(ll) ]
        
        if self.separated:
            self.camera.resize( self.vw, self.vh )
            for i,shape in enumerate(ls):
                if not shape: continue
                x,y = i % self.ncols,(self.nrows-1-int(i/self.ncols))
                glViewport( self.vw*x, self.vh*y, self.vw, self.vh )
                self.draw_axes()
                self.draw_grid()
                self.draw_shape( shape )
                
        else:
            self.camera.resize( self.width, self.height )
            glViewport( 0, 0, self.width, self.height )
            self.draw_axes()
            self.draw_grid()
            for i,shape in enumerate(ls):
                if not shape: continue
                self.draw_shape( shape )
                
        
        glClear( GL_DEPTH_BUFFER_BIT )
        glViewport( 0, 0, self.width, self.height )
        glMatrixMode( GL_PROJECTION )
        glLoadIdentity()
        glOrtho( 0, self.width, -self.height, 0, -1, 1 )
        glMatrixMode( GL_MODELVIEW )
        glLoadIdentity()
        
        self.draw_dividers()
        self.draw_textinfo( ll )
        self.draw_mouse()
    
    @no_lights_decorator
    def draw_dividers( self ):
        if not self.dividers: return
        if not self.separated: return
        
        glLineWidth( 3.0 )
        glBegin( GL_LINES )
        glColor3f( 0.15, 0.15, 0.15 )
        for x in xrange(self.ncols):
            glVertex2f( x*self.vw, 0 )
            glVertex2f( x*self.vw, -self.height )
        for y in xrange(self.nrows):
            glVertex2f( 0, -y*self.vh )
            glVertex2f( self.width, -y*self.vh )
        glEnd()
    
    @no_lights_decorator
    def draw_textinfo( self, ll ):
        if not ll: return
        if not self.labels: return
        
        #self.label.draw()
        c = dict( [
            ( BackgroundColors.BLACK, (255, 255, 0, 255) ),
            ( BackgroundColors.GRAY,  (255, 255, 0, 255) ),
            ( BackgroundColors.WHITE, (  0,   0, 0, 255) ),
            ] )[ self.bg.val ]
        for i,l in enumerate(ll):
            if not l: continue
            
            ix = i % self.ncols
            iy = int(math.floor(float(i) / float(self.ncols)))
            
            l.begin_update()
            l.x = ix * self.vw
            l.y = -iy * self.vh
            l.color = c
            l.width = self.vw
            l.end_update()
            
            l.draw()
    
    @no_lights_decorator
    def draw_axes( self ):
        if not self.axes: return
        glLineWidth( 2.0 )
        self.shape_axes.render()
    
    @no_lights_decorator
    def draw_grid( self ):
        if not self.grid: return
        glLineWidth( 0.25 )
        self.shape_grid.render()
    
    def draw_shape( self, shape ):
        glLineWidth( self.line_width )
        glPointSize( 20.0 )
        glPointParameterfv( GL_POINT_DISTANCE_ATTENUATION, self.vec( 0.0, 2.0, 0.0 ) )
        
        if self.shapeview in [ ShapeViews.WIRES, ShapeViews.VERTS_WIRES, ShapeViews.WIRES_FACES ]:
            if self.offset == OffsetValues.NONE:
                glDepthRange( 0.0, 1.0 )
            elif self.offset == OffsetValues.SMALL:
                #glDepthRange( 0.0, 0.99998 )
                glDepthRange( 0.0, 0.99999 )
            elif self.offset == OffsetValues.LARGE:
                glDepthRange( 0.0, 0.99995 )
            shape.render_wires()
            glDepthRange( 0.0, 1.0 )
        
        if self.shapeview in [ ShapeViews.VERTS, ShapeViews.VERTS_WIRES ]:
            shape.render_verts()
        
        if self.shapeview in [ ShapeViews.FACES, ShapeViews.WIRES_FACES ]:
            if self.offset == OffsetValues.NONE:
                glDepthRange( 0.0, 1.0 )
            elif self.offset == OffsetValues.SMALL:
                glDepthRange( 0.00001, 1.0 )
                #glDepthRange( 0.00002, 1.0 )
            elif self.offset == OffsetValues.LARGE:
                glDepthRange( 0.00005, 1.0 )
            if self.on_draw_start_callback:
                self.on_draw_start_callback()
            shape.render()
            if self.on_draw_end_callback:
                self.on_draw_end_callback()
            glDepthRange( 0.0, 1.0 )
    
    @no_lights_decorator
    def draw_mouse( self ):
        if not self.mouse: return
        if self.mx < 0 or self.mx >= self.width: return
        if self.my > 0 or self.my <= -self.height: return
        if self.mb != 4: return
        
        glPointSize( 10.0 )
        glPointParameterfv( GL_POINT_DISTANCE_ATTENUATION, self.vec( 1.0, 0.0, 0.0 ) )
        glBegin( GL_POINTS )
        if self.separated:
            for x in xrange(self.ncols):
                for y in xrange(self.nrows):
                    if x == 0 and y == 0:   glColor3f( 1, 1, 0 )
                    else:                   glColor3f( 0, 1, 1 )
                    glVertex2f( self.mx + self.vw*x, self.my + self.vh*y )
                    glVertex2f( self.mx + self.vw*x, self.my - self.vh*y )
                    glVertex2f( self.mx - self.vw*x, self.my - self.vh*y )
                    glVertex2f( self.mx - self.vw*x, self.my + self.vh*y )
        else:
            glColor3f( 1, 1, 0 )
            glVertex2f( self.mx, self.my )
        glEnd()
    
    
    
    
    def on_key_press( self, symbol, modifiers ):
        ctrl = bool( modifiers & key.MOD_CTRL )
        shft = bool( modifiers & key.MOD_SHIFT )
        
        if self.on_key_callback:
            if self.on_key_callback( key.symbol_string( symbol ) ):
                pyglet.app.exit()
        
        if symbol == key.F1:
            DebugWriter.divider()
            DebugWriter.report( 'Keys / Mouse', {
                'F1'    : 'print this help to console',
                'F2'    : 'save GUI state',
                'F3'    : 'load GUI state',
                'F4'    : 'double window size           (be careful!)',
                'F5'    : 'halve window size            (be careful!)',
                'F6'    : 'manually set width,height    (in console!)',
                'F7'    : 'manually set FoV             (in console!)',
                'F8'    : 'cycle line offset amount',
                'F9'    : 'manually set line width      (in console!)',
                'Esc'   : 'quit',
                'A'     : 'toggle axes',
                'D'     : 'toggle dividers',
                'E'     : 'toggle labels',
                'G'     : 'toggle grid',
                'O'     : 'toggle separation / overlay',
                'Y'     : 'cycle show only              (+ctrl:reverse)',
                'B'     : 'cycle background color       (+ctrl:reverse)',
                'L'     : 'cycle lighting mode          (+ctrl:reverse)',
                'V'     : 'cycle viewing mode           (+ctrl:reverse)',
                'S'     : 'save screenshot',
                'Num1'  : 'rotate camera to front       (+ctrl:reverse)',
                'Num2'  : 'move camera backward',
                'Num3'  : 'rotate camera to right       (+ctrl:reverse)',
                'Num7'  : 'rotate camera to top         (+ctrl:reverse)',
                'Num8'  : 'move camera and focus forward',
                'Num.'  : 'move camera and focus to focus to center',
                'Num*'  : 'increase camera FoV',
                'Num/'  : 'decrease camera FoV',
                'Num+'  : 'move camera toward focus',
                'Num-'  : 'move camera away from focus',
                'Up'    : 'tumble camera up             (+shift:slow)',
                'Down'  : 'tumble camera down           (+shift:slow)',
                'Left'  : 'tumble camera left           (+shift:slow)',
                'Right' : 'tumble camera right          (+shift:slow)',
                'MMB'   : 'tumble camera                (+shift:move)',
                'RMB'   : 'draw mouse cursor',
                }, dict_order=[
                    'F1','F2','F3','F4','F5','F6','F7', 'F8', 'F9','Esc',
                    'A','D','E','G','O','Y',
                    'B','L','V',
                    'S',
                    'Num1','Num2','Num3','Num7','Num8',
                    'Num.','Num*', 'Num/', 'Num+', 'Num-',
                    'Up','Down','Left','Right',
                    'MMB','RMB',
                ], space=10 )
            DebugWriter.divider()
        if symbol == key.F2:    self.save_state()
        if symbol == key.F3:
            self.load_state()
            self.center_window()
        if symbol == key.F4:
            self.width,self.height = self.width*2,self.height*2
            self.center_window()
        if symbol == key.F5:
            self.width,self.height = self.width/2,self.height/2
            self.center_window()
        if symbol == key.F6:
            print( 'grid = %dx%d' % (self.ncols, self.nrows) )
            nw = raw_input( 'Enter width (%d): ' % self.width )
            nw = int(nw) if nw != '' else self.width
            nh = raw_input( 'Enter height (%d): ' % self.height )
            nh = int(nh) if nh != '' else self.height
            self.width,self.height = nw,nh
            self.center_window()
        if symbol == key.F7:
            nf = raw_input( 'Enter FoV (%f): ' % self.camera.angle )
            nf = float(nf) if nf != '' else self.camera.angle
            self.camera.angle = nf
        if symbol == key.F8:    self.offset.cycle()
        if symbol == key.F9:
            nw = raw_input( 'Enter line width (%f): ' % self.line_width )
            nw = float(nw) if nw != '' else self.line_width
            self.line_width = nw
        if symbol == key.A:     self.axes = not self.axes
        if symbol == key.B:
            if not ctrl:
                self.bg.cycle()
            else:
                self.by.cycle()
        if symbol == key.D:     self.dividers = not self.dividers
        if symbol == key.E:     self.labels = not self.labels
        if symbol == key.G:     self.grid = not self.grid
        if symbol == key.L:
            if not ctrl:
                self.light.cycle()
            else:
                self.light.cycle_reverse()
        if symbol == key.O:     self.separated = not self.separated
        if symbol == key.S:     self.screenshot()
        if symbol == key.V:
            if not ctrl:
                self.shapeview.cycle()
            else:
                self.shapeview.cycle_reverse()
        if symbol == key.Y:
            if not ctrl:
                self.showonly = ((self.showonly+1+1) % (len(self.shapes)+1)) - 1
            else:
                self.showonly = ((self.showonly-1+1) % (len(self.shapes)+1)) - 1
        if symbol == key.ESCAPE:self.close()    #pyglet.app.exit()
        if symbol == key.NUM_1:
            self.tx,self.ty = (0.0,0.0)             if not ctrl else (math.pi,0.0)
        if symbol == key.NUM_3:
            self.tx,self.ty = (0.0,math.pi/2.0)     if not ctrl else (0.0,3.0*math.pi/2.0)
        if symbol == key.NUM_7:
            self.tx,self.ty = (-math.pi/2.0,0.0)    if not ctrl else (math.pi/2.0,0.0)
        if symbol == key.NUM_DECIMAL:
            self.camera.ex,self.camera.ey,self.camera.ez = self.camera.fx-self.camera.ex,self.camera.fy-self.camera.ey,self.camera.fz-self.camera.ez
            self.camera.fx,self.camera.fy,self.camera.fz = 0,0,0
    
    def handle_keys( self ):
        mult = 1.0 if not self.keymap[key.LSHIFT] else 0.2
        if self.keymap[key.UP]:             self.tumble(  0.03*mult,  0.00 )
        if self.keymap[key.DOWN]:           self.tumble( -0.03*mult,  0.00 )
        if self.keymap[key.LEFT]:           self.tumble(  0.00,  0.05*mult )
        if self.keymap[key.RIGHT]:          self.tumble(  0.00, -0.05*mult )
        if self.keymap[key.NUM_2]:          self.move( 0.00, 0.00, -10.0 )
        if self.keymap[key.NUM_8]:          self.move( 0.00, 0.00,  10.0 )
        if self.keymap[key.NUM_ADD]:        self.dolly( 0.952380952381 )
        if self.keymap[key.NUM_SUBTRACT]:   self.dolly( 1.05 )
        if self.keymap[key.NUM_MULTIPLY]:   self.camera.angle *= 1.05 #scale *= 1.05
        if self.keymap[key.NUM_DIVIDE]:     self.camera.angle /= 1.05 #scale /= 1.05
    
    def on_mouse_press( self, x, y, button, modifiers ):
        self.mx,self.my,self.mb = self._screen_to_gl( x, y, button )
    
    def on_mouse_release( self, x, y, button, modifiers ):
        self.mx,self.my,self.mb = self._screen_to_gl( x, y, 0 )
    
    def on_mouse_drag( self, x, y, dx, dy, buttons, modifiers ):
        shift = modifiers & key.MOD_SHIFT
        self.mx,self.my,self.mb = self._screen_to_gl( x, y, buttons )
        if buttons == 2:
            if shift:
                rx,ry,rz = self.camera.get_right()
                ux,uy,uz = self.camera.get_up()
                d = self.camera.get_dist()
                rv = -dx * d * 0.002
                uv = -dy * d * 0.002
                self.camera.fx += rx*rv + ux*uv
                self.camera.fy += ry*rv + uy*uv
                self.camera.fz += rz*rv + uz*uv
            else:
                self.tumble( dy / 100.0, -dx / 100.0 )
    
    def on_mouse_motion( self, x, y, dx, dy ):
        self.mx,self.my,self.mb = self._screen_to_gl( x, y, 0 )
    
    def on_mouse_leave( self, x, y ):
        self.mx,self.my,self.mb = self._screen_to_gl( x, y, 0 )
        #self.mx,self.my = -50,-50
    
    def on_mouse_scroll( self, x, y, scroll_x, scroll_y ):
        if scroll_y > 0: r = 0.952380952381
        else: r = 1.05
        for i in xrange(abs(scroll_y) * 3):
            self.dolly( r )



if __name__ == '__main__':
    shapes = [
        Shape( GL_POLYGON, [ 0.0,1.0,0.0, 1.0,-1.0,0.0, -1.0,-1.0,0.0 ], [ 1.0,0.0,0.0, 0.0,1.0,0.0, 0.0,0.0,1.0 ] ),
        Shape( GL_QUADS, [ -1.0,1.0,0.0, 1.0,1.0,0.0, 1.0,-1.0,0.0, -1.0,-1.0,0.0 ], [ 0.3,0.5,1.0 ] ),
        Shape( GL_QUADS, [
                 1.0, 1.0, 1.0,   1.0,-1.0, 1.0,  -1.0,-1.0, 1.0,  -1.0, 1.0, 1.0,   # front
                 1.0, 1.0,-1.0,   1.0,-1.0,-1.0,  -1.0,-1.0,-1.0,  -1.0, 1.0,-1.0,   # back
                 1.0, 1.0, 1.0,   1.0, 1.0,-1.0,  -1.0, 1.0,-1.0,  -1.0, 1.0, 1.0,   # right
                 1.0,-1.0, 1.0,   1.0,-1.0,-1.0,  -1.0,-1.0,-1.0,  -1.0,-1.0, 1.0,   # left
                 1.0, 1.0, 1.0,   1.0,-1.0, 1.0,   1.0,-1.0,-1.0,   1.0, 1.0,-1.0,   # top
                -1.0, 1.0, 1.0,  -1.0,-1.0, 1.0,  -1.0,-1.0,-1.0,  -1.0, 1.0,-1.0,   # bottom
            ], [ 0.5,0.5,0.5 ], norms=[
                 0.0, 0.0,-1.0,   0.0, 0.0,-1.0,   0.0, 0.0,-1.0,   0.0, 0.0,-1.0,
                 0.0, 0.0,-1.0,   0.0, 0.0,-1.0,   0.0, 0.0,-1.0,   0.0, 0.0,-1.0,
                 0.0, 1.0, 0.0,   0.0, 1.0, 0.0,   0.0, 1.0, 0.0,   0.0, 1.0, 0.0,
                 0.0, 1.0, 0.0,   0.0, 1.0, 0.0,   0.0, 1.0, 0.0,   0.0, 1.0, 0.0,
                 1.0, 0.0, 0.0,   1.0, 0.0, 0.0,   1.0, 0.0, 0.0,   1.0, 0.0, 0.0,
                 1.0, 0.0, 0.0,   1.0, 0.0, 0.0,   1.0, 0.0, 0.0,   1.0, 0.0, 0.0,
            ]),
        ]
    window = Viewer( shapes )
    window.app_run()


#include <vector>

#ifndef MESHOP
#define MESHOP

class MeshOp {
    
public:
    
    static void init( int c_i0, int c_i1 );                         // initialize cost-chain, i0-chain, i1-chain, and pull-chain
    static MeshOp *create( int i0, int i1 );                        // defaults to add
    static MeshOp *create( int i0, int i1, bool adddel );
    
    static void validate();                                 // rough assertion of chain invariants
    
    static MeshOp *get_pull_head();                         // returns MeshOp::head_pull
    static MeshOp *get_i0_head( int i0 );
    static MeshOp *get_i1_head( int i1 );
    static int get_cost_length();                           // returns length of cost-chain
    static int get_pull_length();                           // returns length of pulled-chain
    static MeshOp *get_meshop( int i0, int i1 );
    
    static void insort_pull();                              // inserts pull-chain into cost-chain maintaining sorted order
    static void insort( MeshOp *meshop );                   // insorts meshop (one at a time)
    static void insort_cost( MeshOp *sorted );              // insorts cost-chain starting with sorted
    static void insort_sorted_pull();
    
    static MeshOp *sort_pull_quicksort();                   // sorts cost-chain for meshop, updating head_pull
    static MeshOp *sort_pull_mergesort();
    
    static void clear_pull();
    static void kill_pull();                                // kills all MeshOps on pull-chain
    static void kill_i0( int i0 );                          // kills all MeshOps on i0-chain
    static void kill_i1( int i1 );                          // kills all MeshOps on i1-chain
    
    static MeshOp *pop_cost();                              // pops off cheapest MeshOp (first on cost-chain)
    static void pull_i0( int i0 );                          // pulls all MeshOps from cost-chain that have specific i0
    static void pull_i1( int i1 );                          // pulls all MeshOps from cost-chain that have specific i1
    
    void set_cost( float cost );                            // updates cost of MeshOp (NOTE: does not maintain sorted order if on cost-chain!)
    float get_cost();                                       // getter for cost
    int get_i0();                                           // getter for i0
    int get_i1();                                           // getter for i1
    bool get_adddel();
    
    MeshOp *get_n_cost();                                   // returns next MeshOp on cost-chain
    MeshOp *get_n_pull();                                   // returns next MeshOp on pull-chain
    MeshOp *get_n_i0();
    MeshOp *get_n_i1();
    
    void remove_all();                                      // removes this from all chains
    void remove_cost();                                     // removes this from cost-chain
    void remove_i0();                                       // removes this from i0-chain
    void remove_i1();                                       // removes this from i1-chain
    void remove_pull();                                     // removes this from pull-chain
    
    void *extra;
    
private:
    
    MeshOp( int i0, int i1 );
    MeshOp( int i0, int i1, bool adddel );                  // creates new MeshOp and adds to i0-chain, i1-chain, and pull-chain
    void init( int i0, int i1, bool adddel );
    
    int i0, i1;
    bool adddel;
    float cost;
    bool pulled;                                            // is on pull-chain?
    bool killed;                                            // has been killed?
    
    MeshOp *n_cost, *p_cost;                                // next,prev for cost-chain
    MeshOp *n_i0, *p_i0;                                    // next,prev for i0-chain
    MeshOp *n_i1, *p_i1;                                    // next,prev for i1-chain
    MeshOp *n_pull, *p_pull;                                // next,prev for pulled-chain
    
    static MeshOp *head_cost;                               // head of cost-chain
    static MeshOp *head_pull;                               // head of pulled-chain
    static MeshOp **head_i0;                                // array of heads of i0-chain
    static MeshOp **head_i1;                                // array of heads of i1-chain
    static int c_i0, c_i1;                                  // greatest i0 and i1 values
    
    static MeshOp *sort_pull_mergesort( MeshOp *head, int istart, int count );
    
    static void prepend_i0( MeshOp *meshop );               // adds meshop to i0-chain
    static void prepend_i1( MeshOp *meshop );               // adds meshop to i1-chain
    static void prepend_pull( MeshOp *meshop );             // adds meshop to pulled-chain
    
};


#endif

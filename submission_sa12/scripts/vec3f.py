import math

"""
a simple vector class

NOTE: prefix/suffix types
    t : vector is given as tuple,    v : vector is given as Vec3f
if type is prefix, then it's a class method (t_add).  if type is suffix, then it's a instance method (add_t).
    except with the to-/from-convert methods
instance methods modify current instance, but overloaded operators do not (return result in new instance).

*CAREFUL*: not consistently checking for div0's

TODO: i did not write a tuple or vector equivalent function for all operations...
TODO: also, these are not optimized!!
"""

class Vec3f(object):
    __slots__ = ('x','y','z')
    
    def __init__( self, x, y, z ):
        self.x,self.y,self.z = x,y,z
    
    def set_xyz( self, x, y, z ):
        self.x,self.y,self.z = x,y,z
    def set_t( self, t ):
        self.x,self.y,self.z = t
    def set_v( self, v ):
        self.x,self.y,self.z = v.x,v.y,v.z
    
    # convert to/from tuple or list
    @classmethod
    def from_tuple( cls, t ):
        x,y,z = t
        return Vec3f( x, y, z )
    @classmethod
    def from_t( cls, t ):
        x,y,z = t
        return Vec3f( x, y, z )
    def to_list( self ):
        return [ self.x, self.y, self.z ]
    def to_tuple( self ):
        return ( self.x, self.y, self.z )
    def to_t( self ):
        return ( self.x, self.y, self.z )
    
    # return length of vector
    @classmethod
    def t_length( cls, t ):
        x,y,z = t
        return math.sqrt( x*x+y*y+z*z )
    def length( self ):
        x,y,z = self.x,self.y,self.z
        return math.sqrt( x*x+y*y+z*z )
    def __len__( self ):
        x,y,z = self.x,self.y,self.z
        return math.sqrt( x*x+y*y+z*z )
    
    # compute distance between two positions
    @classmethod
    def t_distance( cls, u, v ):
        return math.sqrt( (u[0]-v[0])**2 + (u[1]-v[1])**2 + (u[2]-v[2])**2 )
    @classmethod
    def t_distance2( cls, u, v ):
        dx,dy,dz = u[0]-v[0],u[1]-v[1],u[2]-v[2]
        return dx*dx+dy*dy+dz*dz
    
    # return the k-nearest neighbors to a position (u) given list of other positions (lv)
    # note: not optimized!!
    @classmethod
    def t_knn( cls, k, u, lv ):
        k = min( k, len(lv) )
        lv_ = [ (i,Vec3f.t_distance2( u, v )) for i,v in enumerate(lv) ]
        lv_ = sorted( lv_, key=lambda x:x[1] )
        return [ (i,lv[i]) for i,_ in lv_[:k] ]
    
    # add two vectors
    @classmethod
    def t_add( cls, u, v ):
        ux,uy,uz = u
        vx,vy,vz = v
        return ( ux+vx, uy+vy, uz+vz )
    @classmethod
    def v_add( cls, u, v ):
        return Vec3f( u.x+v.x, u.y+v.y, u.z+v.z )
    def add_v( self, v ):
        self.x += v.x
        self.y += v.y
        self.z += v.z
        return self
    def add_t( self, v ):
        vx,vy,vz = v
        self.x += vx
        self.y += vy
        self.z += vz
        return self
    def __add__( self, v ):
        ux,uy,uz = self.x,self.y,self.z
        if type(v) is Vec3f: vx,vy,vz = v.x,v.y,v.z
        else: vx,vy,vz = v
        return Vec3f( ux+vx, uy+vy, uz+vz )
    
    # subtract two vectors
    @classmethod
    def t_sub( cls, u, v ):
        ux,uy,uz = u
        vx,vy,vz = v
        return ( ux-vx, uy-vy, uz-vz )
    @classmethod
    def v_sub( cls, u, v ):
        return Vec3f( u.x-v.x, u.y-v.y, u.z-v.z )
    def sub_v( self, v ):
        self.x -= v.x
        self.y -= v.y
        self.z -= v.z
        return self
    def sub_t( self, v ):
        vx,vy,vz = v
        self.x -= vx
        self.y -= vy
        self.z -= vz
        return self
    def __sub__( self, v ):
        ux,uy,uz = self.x,self.y,self.z
        if type(v) is Vec3f: vx,vy,vz = v.x,v.y,v.z
        else: vx,vy,vz = v
        return Vec3f( ux+vx, uy+vy, uz+vz )
    
    # linearly interpolate two vectors
    @classmethod
    def t_lerp( cls, u, v, t ):
        tu = 1 - t
        return ( u[0]*tu+v[0]*t,u[1]*tu+v[1]*t,u[2]*tu+v[2]*t )
    
    # scale a vector by a scalar value (s)
    @classmethod
    def t_scale( cls, u, s ):
        x,y,z = u
        return (x*s,y*s,z*s)
    @classmethod
    def v_scale( cls, u, s ):
        return Vec3f( u.x*s,u.y*s,u.z*s )
    def scale( self, s ):
        self.x *= s
        self.y *= s
        self.z *= s
        return self
    def __mul__( self, s ):
        return Vec3f( self.x*s, self.y*s, self.z*s )
    @classmethod
    def t_mult( cls, u, v ):
        ux,uy,uz = u
        vx,vy,vz = v
        return ( ux*vx, uy*vy, uz*vz )
    @classmethod
    def t_div( cls, u, v ):
        ux,uy,uz = u
        vx,vy,vz = v
        return ( ux/vx, uy/vy, uz/vz )
    
    # compute average of list of vectors (lu)
    @classmethod
    def t_average( cls, lu ):
        ax,ay,az = 0.0,0.0,0.0
        c = float(max(len(lu),1))
        for u in lu:
            ax += u[0]
            ay += u[1]
            az += u[2]
        return ( ax/c, ay/c, az/c )
    
    @classmethod
    def t_min( cls, u, v ):
        ux,uy,uz = u
        vx,vy,vz = v
        return ( min( ux, vx ), min( uy, vy ), min( uz, vz ) )
    @classmethod
    def t_max( cls, u, v ):
        ux,uy,uz = u
        vx,vy,vz = v
        return ( max( ux, vx ), max( uy, vy ), max( uz, vz ) )
    @classmethod
    def t_max( cls, lu ):
        return ( max( lu, key=lambda x:x[0] )[0],max( lu, key=lambda x:x[1] )[1],max( lu, key=lambda x:x[2] )[2] )
    
    # negate the vector
    @classmethod
    def t_negate( cls, u ):
        x,y,z = u
        return ( -x, -y, -z )
    @classmethod
    def v_negate( cls, u ):
        return Vec3f( -u.x, -u.y, -u.z )
    def negate( self ):
        self.x = -self.x
        self.y = -self.y
        self.z = -self.z
        return self
    def __neg__( self ):
        return Vec3f( -self.x, -self.y, -self.z )
    
    # cross product of two vectors
    @classmethod
    def t_cross( cls, u, v ):
        ux,uy,uz = u
        vx,vy,vz = v
        return ( uy*vz-uz*vy, uz*vx-ux*vz, ux*vy-uy*vx )
    @classmethod
    def v_cross( cls, u, v ):
        return Vec3f( u.y*v.z-u.z*v.y, u.z*v.x-u.x*v.z, u.x*v.y-u.y*v.x )
    
    # dot product of two vectors
    @classmethod
    def t_dot( cls, u, v ):
        ux,uy,uz = u
        vx,vy,vz = v
        return ux*vx + uy*vy + uz*vz
    @classmethod
    def v_dot( cls, u, v ):
        return u.x*v.x + u.y*v.y + u.z*v.z
    
    # return the vector with unit length
    @classmethod
    def t_norm( cls, u ):
        x,y,z = u
        l = math.sqrt(x*x+y*y+z*z)
        if l == 0.0: return (0.0,0.0,0.0)
        return ( x/l, y/l, z/l )
    @classmethod
    def v_norm( cls, u ):
        l = math.sqrt(u.x*u.x+u.y*u.y+u.z*u.z)
        return Vec3f( u.x/l, u.y/l, u.z/l )
    def norm( self ):
        l = math.sqrt(self.x*self.x+self.y*self.y+self.z*self.z)
        self.x /= l
        self.y /= l
        self.z /= l
        return self
    
    @classmethod
    def t_transform( cls, xform_rot, xform_trans, u ):
        v = [ sum( xr*pe for (xr,pe) in zip(xform_rot[i*3:i*3+3],u) ) + xt for i,xt in enumerate(xform_trans) ]
        # sum( u[j]*xform_rot[i*3+j] for j in xrange(3) ) + xform_trans[i] for x in xrange(3) ]
        return v
        
        #vx = xform_rot[0] * u[0] + xform_rot[3] * u[1] + xform_rot[6] * u[2] + xform_trans[0]
        #vy = xform_rot[3] * u[0] + xform_rot[4] * u[1] + xform_rot[5] * u[2] + xform_trans[1]
        #vz = xform_rot[6] * u[0] + xform_rot[7] * u[1] + xform_rot[8] * u[2] + xform_trans[2]
        #return ( vx, vy, vz )
    
    # project a point (u) to a plane defined by v0,v1,v2
    @classmethod
    def t_project_to_triangle( cls, u, v0, v1, v2 ):
        # TODO: write!
        pass

// from http://community.topcoder.com/tc?module=Static&d1=tutorials&d2=hungarianAlgorithm

#include <memory>
#include <iostream>
#include <algorithm>
//#include <math.h>
#include <stdio.h>

#include "munkres.h"

using namespace std;

int main()
{
    int r,c,i,j;
    FILE *fp;
    float t;
    
    fp = fopen("costmatrix.mtx","rt");
    fscanf( fp, "%d %d", &r, &c );
    printf( "size: %dx%d\n", r, c );
    Matrix<double> costs(r,c);
    printf( "loading...\n" );
    for( i = 0; i < r; i++ )
    {
        for( j = 0; j < c; j++ )
        {
            fscanf( fp, "%f", &t );
            costs(i,j) = (double)t;
        }
    }
    fclose(fp);
    
    printf( "running...\n" );
    Munkres m;
    m.solve(costs);
    
    fp = fopen( "assignment.ind", "wt" );
    for( i = 0; i < r; i++ )
    {
        for( j = 0; j < c; j++ )
            if( costs(i,j) == 0 ) break;                // a match
        if( j == c ) {
            printf( "PROBLEM!\n" );
            exit(1);
        }
        printf( "%d: %d, ", i, j );
        fprintf( fp, "%d\n", j );
    }
    fclose( fp );
    printf( "\b \n" );
    
    return 0;
}

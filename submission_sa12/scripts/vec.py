from math import *

# based on: http://www.kokkugia.com/wiki/index.php5?title=Python_vector_class

class vec2f(object):
    def __init__(self,x,y):
        self.x = float(x)
        self.y = float(y)
    def __repr__(self):
        return "vec2f(%s,%s)" % (self.x,self.y)
    
    @staticmethod
    def from_string(s):
        sp = s.split(',')
        return vec2f(float(sp[0]),float(sp[1]))
    def to_string(self):
        x = ('%f' % self.x).rstrip('0').rstrip('.')
        y = ('%f' % self.y).rstrip('0').rstrip('.')
        return "%s,%s" % (x,y)
    
    @staticmethod
    def from_list(lst):
        return vec2f( lst[0], lst[1] )
    def to_list(self):
        return [self.x,self.y]
    
    def clone(self):
        return vec2f(self.x,self.y)
    
    def length(self):
        return sqrt( self.x*self.x + self.y*self.y )
    def lengthSquared(self):
        return ( self.x*self.x + self.y*self.y )
    
    def setv( self, x, y ):
        self.x = x
        self.y = y
        return self
    def zero(self):
        self.x = 0.0
        self.y = 0.0
        return self
    def invert(self):
        self.x = -(self.x)
        self.y = -(self.y)
        return self
    
    def resize(self, newlength):
        mult = float(newlength / self.length())
        self.x *= mult
        self.y *= mult
        return self
    def normalize(self):
        return self.resize( 1.0 )
    
    def minus(self, t):
        self.x -= t.x
        self.y -= t.y
        return self
    
    def plus(self, t):
        self.x += t.x
        self.y += t.y
        return self
    
    def min(self, t):
        return vec2f( min(self.x,t.x), min(self.y,t.y) )
    def max(self, t):
        return vec2f( max(self.x,t.x), max(self.y,t.y) )
    
    def scale(self, s):
        self.x *= s
        self.y *= s
        return self
    
    def dot(self, t):
        return( self.x * t.x + self.y * t.y )
    def angle(self, t):
        d = self.dot(t) / (self.length() * t.length())
        d = min( 1.0, max( -1.0, d ) )
        return float(acos(d))
    def dist(self, t):
        dx = self.x - t.x
        dy = self.y - t.y
        return sqrt( dx*dx + dy*dy )
    def distSquared(self, t):
        dx = self.x - t.x
        dy = self.y - t.y
        return( dx*dx + dy*dy )


class vec3f(object):
    def __init__(self,x,y,z):
        self.x = float(x)
        self.y = float(y)
        self.z = float(z)
    def __repr__(self):
        return "vec3f(%s,%s,%s)" % (self.x,self.y,self.z)
    
    @staticmethod
    def from_string(s):
        (x,y,z) = [float(sp) for sp in s.split(',')]
        return vec3f(x,y,z)
    def to_string(self):
        x = ('%f' % self.x).rstrip('0').rstrip('.')
        y = ('%f' % self.y).rstrip('0').rstrip('.')
        z = ('%f' % self.z).rstrip('0').rstrip('.')
        return "%s,%s" % (x,y,z)
    
    @staticmethod
    def from_list(lst):
        return vec3f( lst[0], lst[1], lst[2] )
    def to_list(self):
        return [self.x,self.y,self.z]
    
    def clone(self):
        return vec3f(self.x,self.y,self.z)
    
    def length(self):
        return sqrt( self.x*self.x + self.y*self.y + self.z*self.z )
    def lengthSquared(self):
        return ( self.x*self.x + self.y*self.y + self.z*self.z )
    
    def setv( self, x, y, z ):
        self.x = x
        self.y = y
        self.z = z
        return self
    def zero(self):
        self.x = 0.0
        self.y = 0.0
        self.z = 0.0
        return self
    def invert(self):
        self.x = -(self.x)
        self.y = -(self.y)
        self.z = -(self.z)
        return self
    
    def resize(self, newlength):
        mult = float(newlength / self.length())
        self.x *= mult
        self.y *= mult
        self.z *= mult
        return self
    def normalize(self):
        return self.resize( 1.0 )
    
    def minus(self, t):
        self.x -= t.x
        self.y -= t.y
        self.z -= t.z
        return self
    
    def plus(self, t):
        self.x += t.x
        self.y += t.y
        self.z += t.z
        return self
    
    @staticmethod
    def min(s, t):
        return vec3f( min(s.x,t.x), min(s.y,t.y), min(s.z,t.z) )
    @staticmethod
    def max(s, t):
        return vec3f( max(s.x,t.x), max(s.y,t.y), max(s.z,t.z) )
    def minElement(self):
        return max( self.x, self.y, self.z )
    def maxElement(self):
        return max( self.x, self.y, self.z )
    
    def scale(self, s):
        self.x *= s
        self.y *= s
        self.z *= s
        return self
    
    def dot(self, t):
        return( self.x * t.x + self.y * t.y + self.z * t.z )
    def angle(self, t):
        d = self.dot(t) / (self.length() * t.length())
        d = min( 1.0, max( -1.0, d ) )
        return float(acos(d))
    def dist(self, t):
        dx = self.x - t.x
        dy = self.y - t.y
        dz = self.z - t.z
        return sqrt( dx*dx + dy*dy + dz*dz )
    def distSquared(self, t):
        dx = self.x - t.x
        dy = self.y - t.y
        dz = self.z - t.z
        return( dx*dx + dy*dy + dz*dz )
    
    @staticmethod
    def dist( v0, v1 ):
        dx = v0.x - v1.x
        dy = v0.y - v1.y
        dz = v0.z - v1.z
        return( dx*dx + dy*dy + dz*dz )

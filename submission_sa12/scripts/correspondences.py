import math, sys, time, copy, numpy, md5, scipy.io, os, array, signal

from mesh import *
from vec3f import *
from camera import *
from shapes import *
from kdtree import *
from hungarian import *
from miscfuncs import *
from debugwriter import *

from correspondenceinfo import *



###############################################################################################




def build_correspondence( mesh0, mesh1, method=None, maxiters=None, ci=None ):
    
    if method is None: method = 'itergreedy'
    if maxiters is None: maxiters = 20
    
    ci_new = (ci is None)
    if ci_new: ci = CorrespondenceInfo( mesh0, mesh1 )
    t = time.time()
    
    DebugWriter.start( 'building correspondences' )
    DebugWriter.report( 'stats', {
        'verts': [ ci.cv0, ci.cv1 ],
        'faces': [ ci.cf0, ci.cf1 ],
        } )
    
    DebugWriter.report( 'method', method )
    
    
    elif method == 'mixed':
        build_vertex_correspondences_greedy_closestpoints( ci )
        build_face_correspondences( ci )
        remove_face_conflicts( ci, 1.0 ) #0.75 )
        remove_smaller_connected_faces_threshold( ci )
        ci.dcvall()
        build_vertex_correspondences_faces( ci )
        build_correspondence_greedy( ci )
    
    elif method == 'seediter':
        DebugWriter.divider()
        
        costs = {
            'cvadd':   15.0, #5.0,
            'cvdel':   15.0, #5.0,
            'cvdist':  2.5, #7.5,
            'cvnorm':  2.5, #7.5,
            'cvmism':  0.0,
            
            'cfadd':   50.0, #50.0
            'cfdel':   50.0, #50.0
            'cfvdist': 2.0,  # 2.0
            'cfvunk':  12.0, #12.0
            'cfvmis':  40.0, #25.0
            'cfnorm':  80.0, #80.0
            'cfmism':  80.0, #50.0
            'cfmunk':  20.0, #25.0
            
            'k': 50.0,
            }
        DebugWriter.report( 'costs', costs )
        
        if ci_new:
            # seed the matches
            build_vertex_correspondences_greedy_closestpoints( ci )#, costs=costs )
            build_face_correspondences( ci )
            
            ci.lha = None
            ci.lastpass = False
            ci.threshold = 0.1
        
        # iterate until converged or max number of runs...
        for r in xrange(maxiters):
            ha = ci.__hash__()
            if ha == ci.lha:
                ci.done = True
                break
            ci.lha = ha
            
            # remove matches that are likely mismatched
            # - remove relatively small patches of matched faces
            remove_smaller_connected_faces_threshold( ci, threshold=ci.threshold )
            # - remove vert matches that have unmatched faces
            for iv0,v0 in ci.elv0():
                if iv0 not in ci.mv01: continue
                si_f0 = ci.la_v0[iv0]['si_f']
                if all( if0 in ci.mf01 for if0 in si_f0 ): continue
                ci.dcv0(iv0)
            # - remove face matches that have mismatched verts
            remove_mismatched( ci )
            
            # perform greedy algorithm
            build_correspondence_greedy( ci, costs=costs )
            
            ci.threshold *= 0.85
        
        # one final clean-up pass
        if ci.done or ci.lastpass:
            remove_vert_matching_all_unmatched_faces( ci )
    
    elif method == 'steppedgreedy':
        DebugWriter.divider()
        
        settings = [
            # geo   v+    v-    f+    f-    vd    vn    fd    fn    nvu   nvm   nfu   nfm
            [ 1.0, 20.0, 20.0, 50.0, 50.0, 30.0, 30.0, 30.0, 30.0, 10.0, 20.0, 10.0, 20.0 ],
            ][0]
        
        geo = settings[0]
        costs = {
            'cvadd':   settings[1] / geo,
            'cvdel':   settings[2] / geo,
            'cfadd':   settings[3] / geo,
            'cfdel':   settings[4] / geo,
            'cvdist':  settings[5],
            'cvnorm':  settings[6],
            'cfvdist': settings[7],
            'cfnorm':  settings[8],
            'cfvunk':  settings[9] / geo,
            'cfvmis':  settings[10] / geo,
            'cfmunk':  settings[11] / geo,
            'cfmism':  settings[12] / geo,
            'k': 20,
            }
        DebugWriter.report( 'costs', costs )
        
        time_start = time.time()
        
        if ci_new:
            ci.sha = set()
            ci.lha = None
            ci.lastpass = False
            ci.threshold = 0.05
            ci.backtrack = False
            ci.cheapest = (float('inf'),None,None)
        
        if ci.backtrack:
            DebugWriter.divider( char='#', lines=3 )
            
            cost = compute_cost_noopt( ci, costs )
            ha = ci.__hash__()
            if ha in ci.sha:
                ci.done = True
            else:
                ci.sha.add( ha )
                
                cost = compute_cost_noopt( ci, costs )
                
                if cost < ci.cheapest[0]:
                    ci.cheapest = ( cost, copy.deepcopy( ci.mv01 ), copy.deepcopy( ci.mf01 ) )
                
                remove_smaller_connected_faces_threshold( ci, threshold=ci.threshold )
                remove_vert_matching_any_unmatched_faces( ci )
                remove_mismatched( ci, verts=False )
                ci.threshold *= 0.50
                
                ci.backtrack = False
            
        else:
            build_correspondence_greedy( ci, costs=costs )
            ha = ci.__hash__()
            if ha == ci.lha:
                ci.done = True
            else:
                ci.lha = ha
                ci.backtrack = True
        
        cost = compute_cost_noopt( ci, costs )
        if ci.done and cost > ci.cheapest[0]:
            ci.dcvall()
            ci.dcfall()
            for iv0,iv1 in ci.cheapest[1].iteritems():
                ci.acv(iv0,iv1)
            for if0,if1 in ci.cheapest[2].iteritems():
                ci.acf(if0,if1)
        
    
        
    
    elif method == 'itergreedyopts':
        DebugWriter.divider()
        
        fp = open( 'opts.txt', 'rt' )
        opts = [ float(s) for s in fp.read().split('\n') if s ]
        fp.close()
        
        v_adddel,f_adddel,vdist,vnorm,fdist,fnorm,vunm,vmis,funm,fmis = opts
        
        costs = {
            'cvadd':   v_adddel,
            'cvdel':   v_adddel,
            'cfadd':   f_adddel,
            'cfdel':   f_adddel,
            'cvdist':  vdist,
            'cvnorm':  vnorm,
            'cfvdist': fdist,
            'cfnorm':  fnorm,
            'cfvunk':  vunm,
            'cfmunk':  funm,
            'cfvmis':  vmis,
            'cfmism':  fmis,
            
            'cvmism':  0.0,     # ignored!
            'k': 20.0,
            }
        DebugWriter.report( 'costs', costs )
        
        time_start = time.time()
        
        build_correspondence_greedy( ci, costs=costs )
        
        sha = set()
        threshold = 0.05
        cheapest = (float('inf'),None,None)
        
        # iterate until converged or max number of runs...
        for r in xrange(maxiters):
            ha = ci.__hash__()
            if ha in sha:
                ci.done = True
                break
            sha.add( ha )
            
            cost = compute_cost_noopt( ci, costs )
            if cost < cheapest[0]:
                cheapest = ( cost, copy.deepcopy(ci.mv01), copy.deepcopy(ci.mf01) )
            
            # kick out of local minimum
            remove_smaller_connected_faces_threshold( ci, threshold=threshold )
            remove_vert_matching_any_unmatched_faces( ci )
            remove_mismatched( ci, verts=False )
            
            # perform greedy algorithm
            build_correspondence_greedy( ci, costs=costs )
            
            threshold *= 0.5  #0.85
        
        if ci.done:
            # return the operation set with the cheapest cost
            cost = compute_cost_noopt( ci, costs )
            if cost > cheapest[0]:
                ci.dcvall()
                ci.dcfall()
                for iv0,iv1 in cheapest[1].iteritems():
                    ci.acv(iv0,iv1)
                for if0,if1 in cheapest[2].iteritems():
                    ci.acf(if0,if1)
            #remove_vert_matching_all_unmatched_faces( ci )
            #build_vertex_correspondences_faces( ci )
        
        
        print( '\n\n' )
        print( '=' * 40 )
        #print( 'costs:\n  v_adddel: %.2f\n  f_adddel: %.2f\n  geo:      %.2f\n  topo_u:   %.2f\n  topo_m:   %.2f' % (v_adddel,f_adddel,geo,topo_u,topo_m) )
        print( 'costs:' )
        print( '  cvadd:    %.2f' %   v_adddel )
        print( '  cvdel:    %.2f' %   v_adddel )
        print( '  cfadd:    %.2f' %   f_adddel )
        print( '  cfdel:    %.2f' %   f_adddel )
        print( '  cvdist:   %.2f' %  vdist )
        print( '  cvnorm:   %.2f' %  vnorm )
        print( '  cfvdist:  %.2f' % fdist )
        print( '  cfnorm:   %.2f' %  fnorm )
        print( '  cfvunk:   %.2f' %  vunm )
        print( '  cfmunk:   %.2f' %  funm )
        print( '  cfvmis:   %.2f' %  vmis )
        print( '  cfmism:   %.2f' % fmis )
        print( 'iterations: %i' % r )
        print( 'time:       %.2fs' % (time.time() - time_start) )
        print( 'vert counts:' )
        print( '  total   = %04d, %04d' % (len(ci.lv0),len(ci.lv1)) )
        print( '  matches = %04d' % len(ci.mv01) )
        print( '  unmatch = %04d, %04d' % (len(ci.lv0)-len(ci.mv01),len(ci.lv1)-len(ci.mv10)) )
        print( 'face counts:' )
        print( '  total   = %04d, %04d' % (len(ci.lf0),len(ci.lf1)) )
        print( '  matches = %04d' % len(ci.mf01) )
        print( '  unmatch = %04d, %04d' % (len(ci.lf0)-len(ci.mf01),len(ci.lf1)-len(ci.mf10)) )
        print( '=' * 40 )
        print( '\n\n' )
    
    elif method == 'itergreedyvote':
        print( '*' * 50 )
        print( 'using "iterative-greedy with votes" method...' )
        
        v_adddel = 5.0   #  5.0
        f_adddel = 40.0   # 20.0
        geo      = 0.1   #  0.125
        topo_u   =  0.0
        topo_v   =  1.0   #  4.0
        topo_f   =  15.0   #  4.0
        costs = {
            'cvadd':   v_adddel,
            'cvdel':   v_adddel,
            'cfadd':   f_adddel,
            'cfdel':   f_adddel,
            'cvdist':  geo,
            'cvnorm':  0.0, #geo,
            'cfvdist': geo,
            'cfnorm':  geo,
            'cfvunk':  topo_v * topo_u,
            'cfmunk':  topo_f * topo_u,
            'cfvmis':  topo_v,
            'cfmism':  topo_f,
            
            'cvmism':  0.0,     # ignored!
            'k': 20.0,
            }
        
        time_start = time.time()
        #build_correspondence_greedy( ci, costs=costs )
        
        sha = set()
        for r in xrange(maxiters):
            ha = ci.__hash__()
            if ha in sha:
                ci.done = True
                break
            sha.add( ha )
            
            ci01 = ci.clone()
            ci10 = ci.clone().reverse()
            build_correspondence_greedy( ci01, costs=costs )
            build_correspondence_greedy( ci10, costs=costs )
            ci10.reverse()
            
            for iv0 in xrange(ci.cv0):
                if iv0 not in ci01.mv01 or iv0 not in ci10.mv01: continue
                if ci01.mv01[iv0] != ci10.mv01[iv0]: continue
                iv1 = ci01.mv01[iv0]
                if iv0 in ci.mv01:
                    assert iv1 == ci.mv01[iv0]
                else: ci.acv( iv0, iv1 )
            for if0 in xrange(ci.cf0):
                if if0 not in ci01.mf01 or if0 not in ci10.mf01: continue
                if ci01.mf01[if0] != ci10.mf01[if0]: continue
                if1 = ci01.mf01[if0]
                if if0 in ci.mf01:
                    assert if1 == ci.mf01[if0]
                else: ci.acf( if0, if1 )
            
            for iv0 in xrange(ci.cv0):
                if iv0 not in ci.mv01: continue
                iv1 = ci.mv01[iv0]
                si_f0 = ci.la_v0[iv0]['si_f']
                si_f10 = set( ci.mf10[if1] for if1 in ci.la_v1[iv1]['si_f'] if if1 in ci.mf10 )
                if len(si_f0 & si_f10): continue
                ci.dcv0(iv0)
            #remove_vert_matching_all_unmatched_faces( ci )
        
        
        print( '\n\n' )
        print( '=' * 40 )
        #print( 'costs:\n  v_adddel: %.2f\n  f_adddel: %.2f\n  geo:      %.2f\n  topo_u:   %.2f\n  topo_m:   %.2f' % (v_adddel,f_adddel,geo,topo_u,topo_m) )
        print( 'costs:' )
        print( '  v_adddel: %.2f' % v_adddel )
        print( '  f_adddel: %.2f' % f_adddel )
        print( '  geo:      %.2f' % geo )
        print( '  topo_u:   %.2f' % topo_u )
        print( '  topo_v:   %.2f' % topo_v )
        print( '  topo_f:   %.2f' % topo_f )
        #print( 'iterations: %i' % r )
        print( 'time:       %.2fs' % (time.time() - time_start) )
        print( 'vert counts:' )
        print( '  total   = %04d, %04d' % (len(ci.lv0),len(ci.lv1)) )
        print( '  matches = %04d' % len(ci.mv01) )
        print( '  unmatch = %04d, %04d' % (len(ci.lv0)-len(ci.mv01),len(ci.lv1)-len(ci.mv10)) )
        print( 'face counts:' )
        print( '  total   = %04d, %04d' % (len(ci.lf0),len(ci.lf1)) )
        print( '  matches = %04d' % len(ci.mf01) )
        print( '  unmatch = %04d, %04d' % (len(ci.lf0)-len(ci.mf01),len(ci.lf1)-len(ci.mf10)) )
        print( '=' * 40 )
        print( '\n\n' )
        
    
    elif method == 'seededgreedy2':
        print( '*' * 50 )
        print( 'using "seededgreedy2" method...' )
        
        costs = {
            'cvadd':   5.0, #5.0,
            'cvdel':   5.0, #5.0,
            'cvdist':  20.0, #7.5,
            'cvnorm':  15.0, #7.5,
            'cvmism':  0.0,
            
            'cfadd':   50.0, #50.0
            'cfdel':   50.0, #50.0
            'cfvdist': 5.0,  # 2.0
            'cfvunk':  5.0, #12.0
            'cfvmis':  25.0, #25.0
            'cfnorm':  50.0, #80.0
            'cfmism':  50.0, #50.0
            'cfmunk':  20.0, #25.0
            }
        
        build_vertex_correspondences_greedy_closestpoints( ci, costs=costs )
        build_face_correspondences( ci )
        remove_smaller_connected_faces_threshold( ci )#, threshold=0.05 )
        #remove_vert_matching_all_unmatched_faces( ci )
        ci.dcvall()
        
        costs['cvadd'] = costs['cvdel'] = 20.0
        costs['cfnorm'] = 2.0
        costs['cvdist'] = 2.0
        costs['cvnorm'] = 2.0
        costs['cfvdist'] = 1.0
        costs['cfmunk'] = 10.0 # 5.0
        
        build_correspondence_greedy( ci, costs=costs )
        remove_smaller_connected_faces_threshold( ci, threshold=0.05 )
        #remove_vert_matching_all_unmatched_faces( ci )
        ci.dcvall()
        
        costs['cvadd'] = costs['cvdel'] = 30.0
        costs['cfmunk'] = 5.0
        #costs['cfadd'] = costs['cfdel'] = 75.0
        #propagate_face_correspondences( ci )
        
        build_correspondence_greedy( ci, costs=costs )
    
    elif method == 'greedy':
        print( 'using "greedy" method...' )
        #costs = {
            #'cvadd':   5.0,
            #'cvdel':   5.0,
            #'cvdist':  7.5,
            #'cvnorm':  7.5,
            #'cvmism':  0.0,
            
            #'cfadd':   50.0,
            #'cfdel':   50.0,
            #'cfvdist': 2.0,
            #'cfvunk':  12.0, #6.0
            #'cfvmis':  25.0, #15.0
            #'cfnorm':  80.0,
            #'cfmism':  50.0, #30.0
            #'cfmunk':  25.0, #20.0
            #}
        
        costs = {
            'cvadd':   1.0,
            'cvdel':   1.0,
            'cfadd':   4.0,
            'cfdel':   4.0,
            
            'cvdist':  4.0,
            'cvnorm':  0.0,
            'cfvdist': 4.0,
            'cfnorm':  4.0,
            
            'cfvunk':  0.5,
            'cfvmis':  0.5,
            'cfmunk':  1.0,
            'cfmism':  1.0,
            
            'k': 20.0,
            }
        
        build_correspondence_greedy( ci, costs=costs )
        remove_vert_matching_all_unmatched_faces( ci )
    
    elif method == 'greedy2':
        print( 'using "greedy2" method...' )
        build_vertex_correspondences_greedy_closestpoints( ci )
        build_face_correspondences( ci )
        remove_smaller_connected_faces_threshold( ci )#, threshold=0.05 )
        ci.dcvall()
        
        costs = {
            'cvadd':   5.0,
            'cvdel':   5.0,
            'cvdist':  7.5,
            'cvnorm':  7.5,
            'cvmism':  0.0,
            'cfadd':   50.0,
            'cfdel':   50.0,
            'cfvdist': 2.0,
            'cfvunk':  12.0, #6.0
            'cfvmis':  25.0, #15.0
            'cfnorm':  80.0,
            'cfmism':  50.0, #30.0
            'cfmunk':  25.0, #20.0
            }
        
        build_correspondence_greedy( ci, costs=costs )
        ci.dcvall()
        remove_mismatched( ci )
        build_correspondence_greedy( ci, costs=costs )
        ci.dcvall()
        build_correspondence_greedy( ci, costs=costs )
        
        remove_vert_matching_all_unmatched_faces( ci )
        build_correspondence_greedy( ci, costs=costs )
        #remove_vert_matching_all_unmatched_faces( ci )
        
        ci.dcvall()
        build_vertex_correspondences_faces( ci )
        build_correspondence_greedy( ci, costs=costs )
        remove_vert_matching_all_unmatched_faces( ci )
        
    elif method == 'greedy3':
        print( 'using "greedy3" method...' )
        build_vertex_correspondences_greedy_closestpoints( ci )
        build_face_correspondences( ci )
        remove_smaller_connected_faces_threshold( ci )#, threshold=0.05 )
        ci.dcvall()
        
        costs = {
            'cvadd':   5.0,
            'cvdel':   5.0,
            'cvdist':  7.5,
            'cvnorm':  7.5,
            'cvmism':  0.0,
            'cfadd':   50.0,
            'cfdel':   50.0,
            'cfvdist': 2.0,
            'cfvunk':  12.0, #6.0
            'cfvmis':  25.0, #15.0
            'cfnorm':  80.0,
            'cfmism':  50.0, #30.0
            'cfmunk':  25.0, #20.0
            }
        
        build_correspondence_greedy( ci, costs=costs )
        ci.dcvall()
        remove_mismatched( ci )
        build_correspondence_greedy( ci, costs=costs )
        ci.dcvall()
        build_correspondence_greedy( ci, costs=costs )
        
        remove_vert_matching_all_unmatched_faces( ci )
        build_correspondence_greedy( ci, costs=costs )
        #remove_vert_matching_all_unmatched_faces( ci )
        
        ci.dcvall()
        build_vertex_correspondences_faces( ci )
        build_correspondence_greedy( ci, costs=costs )
        remove_mismatched( ci )
        remove_vert_matching_all_unmatched_faces( ci )
        
        build_correspondence_greedy( ci, costs=costs )
        propagate_face_correspondences( ci )
        remove_vert_matching_all_unmatched_faces( ci )
        
    elif method == 'greedy4':
        print( 'using "greedy4" method...' )
        build_vertex_correspondences_greedy_closestpoints( ci )
        build_face_correspondences( ci )
        remove_smaller_connected_faces_threshold( ci, threshold=0.05 )
        ci.dcvall()
        
        costs = {
            'cvadd':   0.0,
            'cvdel':   0.0,
            'cvdist':  7.5,
            'cvnorm':  7.5,
            'cvmism':  0.0,
            'cfadd':   50.0,
            'cfdel':   50.0,
            'cfvdist': 0.0,
            'cfvunk':  0.0, #6.0
            'cfvmis':  0.0, #15.0
            'cfnorm':  0.0,
            'cfmism':  50.0, #30.0
            'cfmunk':  20.0, #20.0
            }
        build_correspondence_greedy( ci, costs=costs )
        remove_mismatched( ci )

        costs = {
            'cvadd':   0.0,
            'cvdel':   0.0,
            'cvdist':  7.5,
            'cvnorm':  7.5,
            'cvmism':  0.0,
            'cfadd':   50.0,
            'cfdel':   50.0,
            'cfvdist': 0.0,
            'cfvunk':  0.0, #6.0
            'cfvmis':  25.0, #15.0
            'cfnorm':  0.0,
            'cfmism':  20.0, #30.0
            'cfmunk':  15.0, #20.0
            }
        build_correspondence_greedy( ci )
        remove_vert_matching_all_unmatched_faces( ci )
        
    elif method == 'closest':
        print( 'using "closest" method...' )
        build_vertex_correspondences_greedy_closestpoints( ci )
        build_face_correspondences( ci )
        remove_face_conflicts( ci, 0.75 )
        remove_smaller_connected_faces_threshold( ci )
        propagate_face_correspondences( ci )
        remove_face_conflicts( ci, 0.75 )
        return ci
        ci.dcvall()
        build_vertex_correspondences_faces( ci )
        build_vertex_correspondences_hungarian_faces( ci )
        build_vertex_correspondences_greedy_closestpoints( ci )
        build_face_correspondences_hungarian( ci )
        remove_face_conflicts( ci, 0.75 )
        propagate_face_correspondences( ci )
        remove_face_conflicts( ci, 0.75 )
    
    
    else:
        print( 'unknown method="%s"' % method )
        exit( 1 )
    
    DebugWriter.end().blank(3)
    ci.mismatch = compute_mismatch_cost( ci )
    return ci

########################################################################################################################

########################################################################################################################

def warp_meshes( m0, m1 ):
#def build_vertex_correspondences_icpcuts( ci ):
    assert False, 'do not use this function until it has been updated'
    (xforms,src,tar) = load_icpcuts_correspondences( os.path.expanduser( '~/Projects/meshgit.new/models/match_001.data' ) )
    
    lc = [ [random.uniform(0.2,1.0),random.uniform(0.2,1.0),random.uniform(0.2,1.0)] for i in xrange(len(xforms)) ]
    
    m2 = m0.clone()
    m3 = m1.clone()
    
    for i,v in enumerate(m2.lv):
        lbl = src[i]
        rot,tra,clist = xforms[lbl]
        c = lc[lbl]
        
        x,y,z = v.p
        nx,ny,nz = v.p
        #nx = x * rot[0] + y * rot[3] + z * rot[6] + tra[0]
        #ny = x * rot[1] + y * rot[4] + z * rot[7] + tra[1]
        #nz = x * rot[2] + y * rot[5] + z * rot[8] + tra[2]
        
        for i_s,p_s,i_t,p_t in clist:
            if i_s == i:
                nx,ny,nz = p_s
                v.p=nx,ny,nz
                v.c=c
        
        #if x != nx or y != ny or z != nz:
        #    print( 'moved: %i,%i,%i -> %i,%i,%i' % (x,y,z,nx,ny,nz) )
        
        #v.p = nx,ny,nz
        #v.c = c
        m0.lv[i].c = c
    
    for i,v in enumerate(m3.lv):
        lbl = tar[i]
        rot,tra,clist = xforms[lbl]
        c = lc[lbl]
        
        x,y,z = v.p
        nx,ny,nz = v.p
        #nx = x * rot[0] + y * rot[3] + z * rot[6] + tra[0]
        #ny = x * rot[1] + y * rot[4] + z * rot[7] + tra[1]
        #nz = x * rot[2] + y * rot[5] + z * rot[8] + tra[2]
        
        for i_s,p_s,i_t,p_t in clist:
            if i_s == i:
                nx,ny,nz = p_s
                v.p = nx,ny,nz
                v.c = c
        
        #if x != nx or y != ny or z != nz:
        #    print( 'moved: %f,%f,%f -> %f,%f,%f' % (x,y,z,nx,ny,nz) )
        
        #v.p = nx,ny,nz
        #v.c = c
        m1.lv[i].c = c
    
    return (m2,m3)

def bin_read( stype, fp ):
    v = array.array(stype)
    v.fromfile( fp, 1 )
    return v[0]

def load_icpcuts_correspondences( fn ):
    fp = open( fn, 'rb' ) # fn
    c_transformations = bin_read( 'I', fp )
    #print( 'c_transformations,', c_transformations )
    
    trans = []
    
    for i_t in xrange(c_transformations):
        #print( 'i_t', i_t )
        # read xform
        rot = [ bin_read( 'f', fp ) for i_e in xrange(9) ]
        tra = [ bin_read( 'f', fp ) for i_e in xrange(3) ]
        
        # read correspondence list
        c_matched = bin_read( 'I', fp )
        #print( '  c_matched', c_matched )
        
        clist = []
        for i_m in xrange(c_matched):
            src_i = bin_read( 'I', fp )-1
            src_p = [ bin_read( 'f', fp ) for i_e in xrange(3) ]
            tar_i = bin_read( 'I', fp )-1
            tar_p = [ bin_read( 'f', fp ) for i_e in xrange(3) ]
            clist += [(src_i,src_p,tar_i,tar_p)]
        
        trans += [(rot,tra,clist)]
        
    minverifiedregionsize = bin_read( 'I', fp )
    maxnumverifiedregions = bin_read( 'i', fp )
    c_sourcepoints = bin_read( 'I', fp )
    c_targetpoints = bin_read( 'I', fp )
    print( 'c_sourcepoints', c_sourcepoints )
    print( 'c_targetpoints', c_targetpoints )
    src_labels = [ bin_read( 'I', fp )-1 for  i_s in xrange(c_sourcepoints) ]
    tar_labels = [ bin_read( 'I', fp )-1 for  i_t in xrange(c_targetpoints) ]
    #print( 'src_labels', src_labels )
    #print( 'tar_labels', tar_labels )
    fp.close()
    
    return (trans,src_labels,tar_labels)

#load_icpcuts_correspondences(fn)

########################################################################################################################


# spectral matching with affine constraint (SMAC)





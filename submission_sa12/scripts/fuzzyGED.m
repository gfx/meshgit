function X = fuzzyGED(g0,g1)
n = length(g0.V);
m = length(g1.V);

Q = computeQ(g0,g1);
%disp(Q)
c = zeros(n*m,1);

%A = eye(n*m);
%b = ones(n*m,1);

R = computeR(g0,g1);
%u = ones(n+m,1);
u = ones(size(R,1),1);

lb = zeros(n*m,1);
ub = ones(n*m,1);

% trust-region-reflective, interior-point-convex, active-set
%opts = optimset('Algorithm','interior-point-convex','Diagnostics','on','Display','iter');
opts = optimset('Algorithm','interior-point-convex');

[X,fval,exitflag,output,lambda] = QUADPROG(Q,c,[],[],R,u,lb,ub,[],opts);

%disp(Q);
%disp(R);
disp(fval);
disp(exitflag);
disp(output);
%disp(lambda)


class Enum(object):
    def __init__( self, *sequential, **named ):
        self.denums = dict(zip(sequential, range(len(sequential))), **named)
        self.__dict__.update(self.denums)
        self.length = len( self.denums )
    def newval( self, val=0 ):
        return EnumVal( self, val=val )
    def getvalname( self, val ):
        for k,v in self.denums.iteritems():
            if v == val:
                return k
        return None

class EnumVal(object):
    def __init__( self, enumtype, val=0 ):
        self.enumtype = enumtype
        self.denums = self.enumtype.denums
        self.val = val
        self.length = self.enumtype.length
    def __repr__( self ):
        return '%s (%d)' % (self.enumtype.getvalname(self.val),self.val)
    def __eq__( self, other ):
        if type(other) is EnumVal or type(other) is Enum:
            return self.val == other.val
        if type(other) is str:
            return self.enumtype.getvalname(self.val) == other
        if type(other) is int:
            return self.val == other
        assert False, 'unhandled comparison to %s' % other
    def __ne__( self, other ):
        return not self == other
    def cycle( self ):
        self.val = (self.val+1)%self.length
        return self
    def cycle_reverse( self ):
        self.val = (self.val + self.length - 1) % self.length
        return self
    def setval( self, val ):
        if type(val) is EnumVal:
            self.val = val.val
        elif type(val) is str:
            self.val = self.denums[val]
        elif type(val) is int:
            self.val = val % self.length
        return self
    def getval( self ):
        return self.val

class EnumOld(object):
    # http://stackoverflow.com/questions/36932/whats-the-best-way-to-implement-an-enum-in-python
    @staticmethod
    def enum_builder(*sequential, **named):
        enums = dict(zip(sequential, range(len(sequential))), **named)
        enums['__length__'] = len(enums)
        return type('EnumCustom', (), enums)
    @staticmethod
    def enum_cycle( v, e ):
        return (v+1) % e.__length__


if __name__ == '__main__':
    Modes = Enum( 'NO', 'YES', 'MAYBE' )
    mode = Modes.newval()
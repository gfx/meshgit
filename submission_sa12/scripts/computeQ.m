function Q = computeQ(g0,g1)
n = length(g0.V);
m = length(g1.V);

disp( ['computing ', int2str(n), ' x ', int2str(m), ' cost matrix'] )

%Q = zeros(n*m,n*m);
Q = sparse(n*m,n*m);

e0i = sum(g0.E,2);
e1j = sum(g1.E,2);

for ij = 1:(n*m)
    i = floor((ij-1)/m) + 1;
    j = mod((ij-1),m) + 1;
    % map i -> j
    
    % compute cost of assigning edges
    for pq = 1:(n*m)
        p = floor((pq-1)/m) + 1;
        q = mod((pq-1),m) + 1;
        % map p -> q
        
        if i == p && j == q
            %d = abs(sum(g0.E(i,:)) - sum(g1.E(j,:)));
            d = abs(e0i(i) - e1j(j));
        else
            if i == p
                %e0 = sum(g0.E(i,:));
                %e1 = sum(g1.E(j,:)) + sum(g1.E(q,:));
                e0 = e0i(i);
                e1 = e1j(j) + e1j(q);
                d = abs(e0 - e1);
                d = d + abs(g0.E(i,p)-g1.E(j,q));
            else
                if j == q
                    %e0 = sum(g0.E(i,:)) + sum(g0.E(p,:));
                    %e1 = sum(g1.E(j,:));
                    e0 = e0i(i) + e0i(p);
                    e1 = e1j(j);
                    d = abs(e0 - e1);
                    d = d + abs(g0.E(i,p)-g1.E(j,q));
                else
                    %e0 = sum(g0.E(i,:)) + sum(g0.E(p,:));
                    %e1 = sum(g1.E(j,:)) + sum(g1.E(q,:));
                    e0 = e0i(i) + e0i(p);
                    e1 = e1j(j) + e1j(q);
                    d = abs(e0 - e1);
                end
            end
        end
        
        Q(ij,pq) = 10 * d;
        if mod(pq,100) == 0
            %disp( pq )
        end
    end
    
    % compute cost of assigning verts
    v0 = g0.V(:,i);
    v1 = g1.V(:,j);
    vd = v0 - v1;
    Q(ij,ij) = vd' * vd;
    
    %if mod(ij,100) == 0
        disp( ij )
    %end
end

disp( 'done!' )

#ifndef KDTREE
#define KDTREE

//#include <fstream.h>
//#include <iostream.h>


class KDBranch {
    
public:
    
    KDBranch( float *p, int i, int d );
    void insert( float *p, int i );
    void knn( float *p, int k, int *knn, float *knn_dist2, int &found, float &max2 );
    
    //static KDBranch *load_cache( ifstream &fp );
    //void save_cache( ofstream &fp );
    
    int i;
    float p[3];
    int d;
    
    KDBranch *l,*r;
    
};


class KDTree {

public:
    
    KDTree();
    void insert( float *p, int i );
    void insert_shuffle( float *p, int n );
    int knn( float *p, int k, int *knn, float *knn_dist2 );
    
    //static KDTree *load_cache( char *fn );
    //void save_cache( char *fn );
    
private:
    
    KDBranch *top;
    
};


#endif


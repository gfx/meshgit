#!/usr/bin/env python

import os, sys
import miscfuncs
import ply
from colors import *
from vec3f import Vec3f
from mesh import *
from viewer import *
from shapes import *
from debugwriter import DebugWriter

"""
converts results from icpgc algorithm to something usable by meshgit
"""

mesh0 = ply.ply_load( 'durano03.ply' )
mesh1 = ply.ply_load( 'durano04_mod.ply' )

fp = open( 'match_000.data', 'rb' ) # durano02_durano13.icpgcdata
num_xforms = miscfuncs.readbin_uint(fp)
print( 'num_xforms: %d' % num_xforms )
lxforms = []
llmatches = []
for i in xrange(num_xforms):
    xr = [ miscfuncs.readbin_float(fp) for j in xrange(9) ]
    xt = [ miscfuncs.readbin_float(fp) for j in xrange(3) ]
    xform = ( xr, xt )
    
    num_matches = miscfuncs.readbin_uint(fp)
    
    dmatch = {}
    lmatches = []
    for j in xrange(num_matches):
        i_src = miscfuncs.readbin_uint(fp)
        p_src = [ miscfuncs.readbin_float(fp) for k in xrange(3) ]
        i_tar = miscfuncs.readbin_uint(fp)
        p_tar = [ miscfuncs.readbin_float(fp) for k in xrange(3) ]
        
        #if Vec3f.t_distance( p_src, mesh0['lv'][i_src] ) > 0.001 :
        #    print( '   %s,%s != %s' % (p_src,p_tar, mesh0['lv'][i_src] ) )
        
        lmatches += [ (i_src,p_src,i_tar,p_tar) ]
    
    print( 'i_x = %d' % i )
    print( 'xform = %s + %s' % (','.join( '%0.2f'%e for e in xr ), ','.join( '%0.2f'%e for e in xt )) )
    print( 'i_src = %s' % ','.join( str(i_src) for (i_src,p_src,i_tar,p_tar) in lmatches ) )
    print( 'i_tar = %s' % ','.join( str(i_tar) for (i_src,p_src,i_tar,p_tar) in lmatches ) )
    #print( lmatches )
    print( '' )
    
    lxforms += [ xform ]
    llmatches += [ lmatches ]
    #print( dmatch )

min_verified_rgn_sz = miscfuncs.readbin_uint(fp)
max_num_verified_rgns = miscfuncs.readbin_int(fp)
num_src = miscfuncs.readbin_uint(fp)
num_tar = miscfuncs.readbin_uint(fp)

llabels_src = [ miscfuncs.readbin_uint(fp) for i in xrange(num_src) ]
llabels_tar = [ miscfuncs.readbin_uint(fp) for i in xrange(num_tar) ]

print( '' )
print( 'num_src: %d' % num_src )
print( llabels_src )
print( '' )
print( 'num_tar: %d' % num_tar )
print( llabels_tar )

fp.close()

lsiv = [ set([ i_s for (i_s,_,_,_) in lmatches ]) for lmatches in llmatches ]




m0,m1 = [ Mesh.fromPLY(fn) for fn in ['durano03.ply','durano04_mod.ply'] ]
s0,s1 = [ m.recalc_vertex_normals().toShape(smooth=False) for m in [m0,m1] ]
m0.optimize().rebuild_edges()
m1.optimize().rebuild_edges()

DebugWriter.report( 'verts', '%d %d' % (len(m0.lv), len(m1.lv)) )





DebugWriter.start( 'writing out map data' )
fp = open( 'durano03_durano04_mod.icpmap', 'wt' )
for i_v,i_x in enumerate( llabels_src ):
    xr,xt = lxforms[i_x]
    v = i_v in lsiv[i_x]
    p = m0.lv[i_v].p
    if v:
        p = Vec3f.t_transform( xr, xt, p )
    fp.write( '%f %f %f\n' % tuple(p) )
fp.close()
DebugWriter.end()





DebugWriter.start( 'visualizing')

DebugWriter.start( 'creating edge shapes' )
lv,lc,ln = [],[],[]
for e in m0.le:
    u_v0,u_v1 = e.lu_v
    lv += m0.opt_get_v(u_v0).p + m0.opt_get_v(u_v1).p
    lc += [ 0.9, 0.9, 0.9, 0.9, 0.9, 0.9 ]
    ln += m0.opt_get_v(u_v0).n + m0.opt_get_v(u_v1).n
s0e = Shape( GL_LINES, lv, lc, ln, line_width=0.1, use_lights=False )

lv,lc,ln = [],[],[]
for e in m1.le:
    u_v0,u_v1 = e.lu_v
    lv += m1.opt_get_v(u_v0).p + m1.opt_get_v(u_v1).p
    lc += [ 0.9, 0.9, 0.9, 0.9, 0.9, 0.9 ]
    ln += m1.opt_get_v(u_v0).n + m1.opt_get_v(u_v1).n
s1e = Shape( GL_LINES, lv, lc, ln, line_width=0.1, use_lights=False )
DebugWriter.end()


DebugWriter.start( 'creating gc parts', quiet=True )
ls = []
for i_x,x in enumerate(lxforms):
    lmatches = llmatches[i_x]
    xr,xt = x
    
    lv,lc = [],[]
    lvl,lcl = [],[]
    
    for i_v,i_l in enumerate( llabels_src ):
        if i_l != i_x: continue
        p = m0.lv[i_v].p
        px = Vec3f.t_transform( xr, xt, p )
        lv += list(p) #+ list(px)
        lc += [ 1.0, 0.5, 0.0 ]#, 0.5, 0.5, 0.5 ]
        lvl += list(p) + list(px)
        lcl += [ 0.5, 0.5, 0.5, 0.7, 0.7, 0.7 ]
    for i_v,i_l in enumerate( llabels_tar ):
        if i_l != i_x: continue
        lv += m1.lv[i_v].p
        lc += [ 0.2, 0.2, 1.0 ]
    
    for (i_s,p_s,i_t,p_t) in lmatches:
        lvl += p_s + p_t
        lcl += [ 0.7, 0.7, 0.7, 0.7, 0.7, 0.7 ]
    
    if not lv: # and not lvl:
        continue
    ls += [ Shape_Combine( [
        Shape( GL_POINTS, lv, lc, use_lights=False ) if lv else None,
        Shape( GL_LINES, lvl, lcl, use_lights=False ) if lvl else None,
        ] ) ]
s4 = Shape_Combine( [ s0e, s1e, Shape_Cycle( ls, .2, False ) ] )
DebugWriter.end()

DebugWriter.start( 'creating xform parts', quiet=True )
lv,lc = [],[]
lvl,lcl = [],[]
for i_v,i_x in enumerate(llabels_src):
    xr,xt = lxforms[i_x]
    p = m0.lv[i_v].p
    px = Vec3f.t_transform( xr, xt, p )
    v = i_v in lsiv[i_x]
    print( 'p:[%s],\tix:%d,\txr:[%s],\txt:[%s],\tpx:[%s],\t%s' % (
        ','.join('% 0.2f'%e for e in p),
        i_x,
        ','.join('% 0.2f'%e for e in xr),
        ','.join('% 0.2f'%e for e in xt),
        ','.join('% 0.2f'%e for e in px),
        ':)' if v else '--',
        ) )
    if v:
        lv += list(px)
        lvl += list(p) + list(px)
        lcl += [ 0.8, 0.8, 0.8, 0.8, 0.8, 0.8 ]
    else:
        lv += list(p)
    lc += [ 0.5, 0.5, 0.5 ]
s5 = Shape_Combine( [ s1e, Shape( GL_POINTS, lv, lc, use_lights=False), Shape( GL_LINES, lvl,lcl,use_lights=False) ] )
DebugWriter.end()

DebugWriter.start( 'labeling parts' )
lc = [ random_color() for i_x in xrange(num_xforms) ]
m2,m3 = m0.clone(),m1.clone()
for i_v,v in enumerate(m2.lv): v.c = lc[ llabels_src[i_v] ]
#for i_v,v in enumerate(m3.lv): v.c = lc[ llabels_tar[i_v] ]
s2,s3 = m2.toShape(smooth=False),m3.toShape(smooth=False)
DebugWriter.end()

DebugWriter.end() # visualizing

window = Viewer( [ s0, s1, s4, s2, s3, s5 ], caption='ICP+GC Viewer', ncols=3 )
window.app_run()

exit(0)



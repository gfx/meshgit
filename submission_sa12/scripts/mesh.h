#include <vector>

#ifndef MESH
#define MESH

// mesh data
extern int nv0, nv1, nf0, nf1;                          // geometry counts
extern int mnlifv0, mnlifv1, mnlavv0, mnlavv1, mnlavf0, mnlavf1, mnlaff0, mnlaff1;   // counts: face verts, vert adj, face adj

extern int *nlsiv01, **lsiv01, *nlsif01, **lsif01;      // indices of geometry in sorted order (by distance)
extern int *nlsiv10, **lsiv10, *nlsif10, **lsif10;      // indices of geometry in sorted order (by distance)

extern int *nlifv0, **lifv0;                            // face vert inds
extern int *nlifv1, **lifv1;
extern int *nlavv0, **lavv0;
extern int *nlavv1, **lavv1;
extern int *nlavf0, **lavf0;                            // faces adjacent to verts
extern int *nlavf1, **lavf1;
extern int *nlaff0, **laff0;                            // faces adjacent to faces
extern int *nlaff1, **laff1;

// mesh matches
extern int *vmatch01;
extern int *vmatch10;
extern int *fmatch01;
extern int *fmatch10;

extern int *vmatch01_iter;
extern int *fmatch01_iter;
extern int iter_current;

// costs
extern float cost_vdel, cost_vadd, cost_vdist, cost_vnorm;
extern float cost_fdel, cost_fadd, cost_fdist, cost_fvunk, cost_fvmis, cost_fnorm, cost_fmism, cost_fmunk;
extern float cost_fvdist, cost_ffdist;
extern int cost_k, cost_maxiters;

extern int knn;

void vm01_add( int iv0, int iv1 );
void vm01_del( int iv0, int iv1 );
void fm01_add( int if0, int if1 );
void fm01_del( int if0, int if1 );

float v_dist( int iv0, int iv1 );
float v_norm( int iv0, int iv1 );
float f_dist( int iv0, int iv1 );
float f_norm( int iv0, int iv1 );

float v0v1_ddist( int iv0, int iv1 );
float v1v0_ddist( int iv1, int iv0 );
float f0f1_ddist( int if0, int if1 );
float f1f0_ddist( int if1, int if0 );

float v0v0_ddist( int iv0, int iv1 );
float v0f0_ddist( int i_v, int i_f );
float f0v0_ddist( int i_f, int i_v );
float f0f0_ddist( int if0, int if1 );

float v1v1_ddist( int iv1, int iv0 );
float v1f1_ddist( int i_v, int i_f );
float f1v1_ddist( int i_f, int i_v );
float f1f1_ddist( int if1, int if0 );

float v0v1_dist( int iv0, int iv1 );
float v1v0_dist( int iv1, int iv0 );
float f0f1_dist( int if0, int if1 );
float f1f0_dist( int if1, int if0 );

float v0v1_norm( int iv0, int iv1 );
float v1v0_norm( int iv1, int iv0 );
float f0f1_norm( int if0, int if1 );
float f1f0_norm( int if1, int if0 );

float vv0_dist( int iv0, int iv1 );
float vv1_dist( int iv0, int iv1 );

float vf0_dist( int i_v, int i_f );
float vf1_dist( int i_v, int i_f );

float fv0_dist( int i_f, int i_v );
float ff0_dist( int if0, int if1 );

float fv1_dist( int i_f, int i_v );
float ff1_dist( int if0, int if1 );

void compute_knn();

void load_mesh_data( const char *fname );
void save_matching_data( const char *fname );



#endif

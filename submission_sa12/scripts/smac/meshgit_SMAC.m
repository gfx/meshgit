function meshgit_SMAC()
% [authors of MeshGit], 2011-Dec-26
% Modified:
%   Timothee Cour, 21-Apr-2008 17:31:23
%   This software is made publicly for research use only.
%   It may be modified and redistributed under the terms of the GNU General Public License.



fid = fopen('SMAC_ind.txt','wt');
fclose( fid );


% add paths
init();

%% load data from files
disp( 'loading data...' )
drawnow('update') % flush stdout
%[n1,n2,E12,~,W] = meshgit_SMAC_loaddata();
X = load('SMAC_data.mat');
n1 = X.n1;
n2 = X.n2;
E12 = sparse( double(X.E12i), double(X.E12j), 1 );
W = sparse(tril(X.W));
%W = X.W + X.W';

%  %% kronecker normalization
%  W = sparse(tril(X.W));
disp( 'normalizing...' )
drawnow('update') % flush stdout
normalization='iterative';
nbIter=10;
[W,~,~]=normalizeMatchingW(W,E12,normalization,nbIter);


%% options for graph matching (discretization, normalization)
options.constraintMode='both';                          % 'both' for 1-1 graph matching
options.isAffine=1;                                     % affine constraint
options.isOrth=1;                                       % orthonormalization before discretization
options.normalization='iterative';                      % bistochastic kronecker normalization
% options.normalization='none';                         % we can also see results without normalization
options.discretisation=@discretisationGradAssignment;   % function for discretization
options.is_discretisation_on_original_W=1;
%put options.is_discretisation_on_original_W=1 if you want to discretize on original W 
%1: might be better for raw objective (based orig W), but should be worse for accuracy


%% graph matching computation
disp( 'computing graph matching...' )
drawnow('update') % flush stdout
[X12,~,timing]=compute_graph_matching_SMAC(W,E12,options);
disp( timing );

%% results evaluation
if n1>n2
    dim=1;
else
    dim=2;
end
[~,X12_ind]=max(X12,[],dim);
%score=computeObjectiveValue(W,X12(E12>0));

disp( 'writing...' )
drawnow('update') % flush stdout
fid = fopen('SMAC_ind.txt','wt');
for x = X12_ind
    fprintf( fid, '%d\n', floor(x) );
end
fclose( fid );

%timing for SMAC (excluding discretization, which is not optimized)
% timing
% nbErrors
0;


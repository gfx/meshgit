function [nbErrors,score]=meshgit_normalizeMatchingW_potspans()
% Timothee Cour, 21-Apr-2008 17:31:23
% This software is made publicly for research use only.
% It may be modified and redistributed under the terms of the GNU General Public License.

init();

%% load data from files
[v0,e0,f0] = meshgit_potspansblend_veraply();
[va,ea,fa] = meshgit_potspansblend_ver0ply();
[vb,eb,fb] = meshgit_potspansblend_verbply();

n1 = size(v0,2);
n2 = size(va,2);

%% generate feasible matches and affinity between matches
dmat = pdist2(v0',va');

[~,idx] = sort(dmat,2);
idxle10 = idx > 10;
idxge15 = idx > 15;

E12 = sparse((dmat .* idxle10 + idxge15) <= 0.5);

% E12_ = sparse(max(n1,n2)+n1,max(n1,n2)+n2);
% E12_(1:n1,1:n2) = E12;
% E12_(n1+1:n1+max(n1,n2),n2+1:n2+max(n1,n2)) = speye(max(n1,n2));
% E12_(n1+1:n1+n2,1:n2) = speye(n2);
% E12_(1:n1,n2+1:n1+n2) = speye(n1);
% E12 = E12_;

n12 = nnz(E12);
W = zeros( n12, n12 );
[r,c] = find(E12);

for p = 1:n12
    i = r(p);
    j = c(p);
    W(p,:) = (e0(i,r) & ea(j,c))*10 + (idxge15(i,j)>0).*(15-idxge15(i,j));
    %W(p,:) = (e0(i,r) == ea(j,c)) * 10 + (e0(i,r) ~= ea(j,c)) .* abs(e0(i,r)-ea(j,c));
end

% for p = 1:n12
%     i0 = r(p);
%     j0 = c(p);
%     for q = 1:n12
%         ia = r(q);
%         ja = c(q);
%         if i0 < n1 && j0 < n2 && ia < n1 && ja < n2
%             W(p,q) = (e0(i0,ia) & ea(j0,ja))*10 + (idxge15(i0,j0)>0)*(15-idxge15(i0,j0));
%         else
%             if i0 >= n1 && j0 >= n2 && ia >= n1 && ja >= n2
%                 W(p,q) = 10;
%             else
%                 W(p,q) = 0.0001;
%             end
%         end
%     end
%     disp( p );
% end

W = ( W + W' ) / 2;

% %% create a random graph matching matrix with n1 nodes in 1st graph, n2 in 2nd graph
% n1=10;
% n2=15;
% 
% %% generate random feasible matches between 2 graphs
% E12=rand(n1,n2)>0.3; % E12=ones(n1,n2) for full matching
% n12=nnz(E12); %nb of feasible matches
% 
% %% W(e,e') = affinity between match (i1,i2) and match (i1',i2')
% % (we only need to have a n12 x n12 matrix, as opposed to a (n1*n2) x (n1*n2) matrix for memory efficiency and performance)
% W=rand(n12,n12);
% W=(W+W')/2;
% %requires Wij>=0

%% assuming symmetric affinity, only need to keep upper triangular part
W=sparse(tril(W));
% in practice, memory-efficient code will directly generate a sparse triangular matrix

%% kronecker normalization
normalization='iterative';
nbIter=10;
% [W,D1,D2]=normalizeMatchingW(W,E12,normalization,nbIter);
[W,~,~]=normalizeMatchingW(W,E12,normalization,nbIter);

%% verify that kronecker normalization worked (not really needed, just for the demo)
% W=trilW2W(W); %inverse operation from tril(W)
% [I12(:,1),I12(:,2)]=find(E12);
% indexes1=classes2indexes(I12(:,1),n1); %indexes1{i1}=set of matches e=i1i2 in I12 (across i2)
% indexes2=classes2indexes(I12(:,2),n2); %indexes2{i2}=set of matches e=i1i2 in I12 (across i1)
% Dn1=compute_D(W,indexes1);%Dn1(i1,j1)=sum of W(i1i2,j1j2) (across i2,j2) 
% Dn2=compute_D(W,indexes2);%Dn2(i2,j2)=sum of W(i1i2,j1j2) (across i1,j1) 
% %verify that it is near-constant
% std(Dn1(:))
% std(Dn2(:))
% 0;

%% options for graph matching (discretization, normalization)
options.constraintMode='both'; %'both' for 1-1 graph matching
options.isAffine=1;% affine constraint
options.isOrth=1;%orthonormalization before discretization
options.normalization='iterative';%bistochastic kronecker normalization
% options.normalization='none'; %we can also see results without normalization
options.discretisation=@discretisationGradAssignment; %function for discretization
options.is_discretisation_on_original_W=0;

%put options.is_discretisation_on_original_W=1 if you want to discretize on original W 
%1: might be better for raw objective (based orig W), but should be worse for accuracy

%% graph matching computation
[X12,X_SMAC,timing]=compute_graph_matching_SMAC(W,E12,options);

%% results evaluation
if n1>n2
    dim=1;
else
    dim=2;
end
%[ignore,target_ind]=max(target,[],dim);
[~,X12_ind]=max(X12,[],dim);
%nbErrors=sum(X12_ind~=target_ind)/length(target_ind);

score=computeObjectiveValue(W,X12(E12>0));

%timing for SMAC (excluding discretization, which is not optimized)
% timing
% nbErrors
0;


function D=compute_D(W,indexes)
n=length(indexes);
D=zeros(n);
for j=1:n
    for i=1:n
        D(i,j)=sum(sum(W(indexes{i},indexes{j})));
    end
end

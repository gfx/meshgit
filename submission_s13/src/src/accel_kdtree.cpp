#include "accel_kdtree.h"
#include "ext/common/stdmath.h"

#include "vec.h"
#include "std_utils.h"

using std::numeric_limits;



KDTree::KDTree() { }

KDBranch::KDBranch( vec3f &p, int i, int d )
{
    this->p = p;
    this->i = i;
    this->d = d;
}

KDTree::~KDTree() {
    if( root ) delete root;
}

KDBranch::~KDBranch() {
    if( l ) delete l;
    if( r ) delete r;
}


void KDTree::insert( vec3f &p, int i )
{
    if( root ) root->insert( p, i );
    else root = new KDBranch( p, i, 0 );
}
void KDBranch::insert( vec3f &p, int i )
{
    float dist_partition = this->p[d] - p[d];
    
    if( dist_partition >= 0 ) {
        if( l ) l->insert( p, i );
        else l = new KDBranch( p, i, (d+1)%3 );
    } else {
        if( r ) r->insert( p, i );
        else r = new KDBranch( p, i, (d+1)%3 );
    }
}

void KDTree::insert_shuffle( vector<vec3f> &l_p ) {
    int n = l_p.size();
    vector<int> inds; shuffled_indices(n,inds);
    for( int i : inds ) insert( l_p[i], i );
}


int KDTree::knn( const vec3f &p, int k, vector<int> &l_i, vector<float> &l_dist2 ) const
{
    l_i.clear();
    l_i.reserve(k);
    l_dist2.clear();
    l_dist2.reserve(k);
    
    if( root ) {
        float max2 = numeric_limits<float>::max();
        root->knn( p, k, l_i, l_dist2, max2 );
    }
    
    return l_i.size();
}

void KDBranch::knn( const vec3f &p, int k, vector<int> &knn, vector<float> &knn_dist2, float &max2 ) const
{
    int found = knn.size();
    
    float dist_partition = (&this->p.x)[d] - (&p.x)[d];
    float dist_partition2 = dist_partition * dist_partition;
    
    vec3f diff = this->p - p;
    float dist2 = diff % diff;
    
    if( found < k || dist2 <= max2 ) {
        if( found == k ) {
            // find and replace max distance
            int imax = 0;
            float vmax2 = knn_dist2[0];
            for( int i = 1; i < found; i++ ) if( knn_dist2[i] >= vmax2 ) { imax = i; vmax2 = knn_dist2[i]; }
            knn[imax] = this->i;
            knn_dist2[imax] = dist2;
        } else {
            knn.push_back(this->i);
            knn_dist2.push_back(dist2);
            found++;
        }
        
        // find new max
        max2 = *max_element(knn_dist2.begin(),knn_dist2.end());
    }
    
    if( dist_partition == 0 ) {
        if( l ) l->knn( p, k, knn, knn_dist2, max2 );
        if( r ) r->knn( p, k, knn, knn_dist2, max2 );
    } else if( dist_partition > 0 ) {
        if( l ) l->knn( p, k, knn, knn_dist2, max2 );
        if( ( found < k || dist_partition2 <= max2 ) && r ) r->knn( p, k, knn, knn_dist2, max2 );
    } else {
        if( r ) r->knn( p, k, knn, knn_dist2, max2 );
        if( ( found < k || dist_partition2 <= max2 ) && l ) l->knn( p, k, knn, knn_dist2, max2 );
    }
    
}

int KDTree::rball( const vec3f &p, float rad, vector<int> &l_i, vector<float> &l_dist2 ) const {
    l_i.clear();
    l_dist2.clear();
    
    if( root ) return root->rball( p, rad*rad, l_i, l_dist2 );
    
    return 0;
}
int KDTree::rball( const vec3f &p, float rad ) const {
    if( root ) return root->rball( p, rad*rad );
    
    return 0;
}

int KDBranch::rball( const vec3f &p, float rad2, vector<int> &l_i, vector<float> &l_dist2 ) const {
    float dist_partition = (&this->p.x)[d] - (&p.x)[d];
    float dist_partition2 = dist_partition * dist_partition;
    
    vec3f diff = this->p - p;
    float dist2 = diff % diff;
    
    if( dist2 <= rad2 ) {
        l_i.push_back( this->i );
        l_dist2.push_back( dist2 );
    }
    
    if( dist_partition == 0 ) {
        if( l ) l->rball( p, rad2, l_i, l_dist2 );
        if( r ) r->rball( p, rad2, l_i, l_dist2 );
    } else if( dist_partition > 0 ) {
        if( l ) l->rball( p, rad2, l_i, l_dist2 );
        if( dist_partition2 <= rad2 && r ) r->rball( p, rad2, l_i, l_dist2 );
    } else {
        if( r ) r->rball( p, rad2, l_i, l_dist2 );
        if( dist_partition2 <= rad2 && l ) l->rball( p, rad2, l_i, l_dist2 );
    }
    
    return l_i.size();
}

int KDBranch::rball( const vec3f &p, float rad2 ) const {
    int c = 0;
    float dist_partition = (&this->p.x)[d] - (&p.x)[d];
    float dist_partition2 = dist_partition * dist_partition;
    
    vec3f diff = this->p - p;
    float dist2 = diff % diff;
    
    if( dist2 <= rad2 ) c++;
    
    if( dist_partition == 0 ) {
        if( l ) c += l->rball( p, rad2 );
        if( r ) c += r->rball( p, rad2 );
    } else if( dist_partition > 0 ) {
        if( l ) c+= l->rball( p, rad2 );
        if( dist_partition2 <= rad2 && r ) c += r->rball( p, rad2 );
    } else {
        if( r ) c += r->rball( p, rad2 );
        if( dist_partition2 <= rad2 && l ) c += l->rball( p, rad2 );
    }
    
    return c;
}



#ifndef _SHAPE_EVOMESH_H_
#define _SHAPE_EVOMESH_H_

#include "debug.h"
#include "depgraph.h"
#include "shape.h"
#include "vec.h"
#include "object.h"
#include "std.h"
#include "geom.h"
#include "gls.h"
#include "accel_grid.h"
#include "ext/common/frame.h"
#include "evomesh.h"
#include "globals.h"
#include "mesh_utils.h"

#include "io_json.h"

#include <set>
#include <map>


struct Shape;
struct TriangleMesh;
struct ViewPanel;



struct PickerData {
    int idx;
    vec3f p;
    float a, b, dist;
};

struct HistogramData {
    bool b_edit = false;
    int n_histogram_bins = 101;
    vector<float> histogram; vector<bool> empty;
    vector<float> vals, weights;
    float minv=0.0, maxv=1.0;
    float vavg=0.0, vstd=0.0;
    float wsum=0.0;
    
    HistogramData( int n, int m, int M ) : n_histogram_bins(n), minv(m), maxv(M) { }
    
    inline void start( int n ) { n_histogram_bins = n; start(); }
    void start() {
        error_if_not( !b_edit, "cannot start editing histogram" );
        b_edit = true;
        vals.clear();
        weights.clear();
    }
    
    void end() {
        error_if_not( b_edit, "cannot stop editing histogram" );
        b_edit = false;
        
        empty.assign( n_histogram_bins, true );
        histogram.assign( n_histogram_bins, 0.0f );
        vavg = vstd = wsum = 0.0f;
        
        int n = vals.size();
        
        for( int i = 0; i < n; i++ ) {
            float v = vals[i], w = weights[i];
            
            vavg += v * w;
            wsum += w;
            
            float p = (v - minv) / (maxv - minv);
            int i_h = (int)( p * (float) (n_histogram_bins-1) );
            histogram[i_h] += w;
            empty[i_h] = false;
        }
        if( wsum < 0.000001f ) return;
        
        vavg = vavg / wsum;
        
        for( int i = 0; i < n_histogram_bins; i++ ) {
            histogram[i] /= wsum;
        }
        
        for( int i = 0; i < n; i++ ) {
            float v = vals[i], w = weights[i];
            float d = v-vavg;
            vstd += d * d * w;
        }
        vstd = sqrt(vstd/wsum) / (maxv-minv);
    }
    
    void add( float v, float w ) {
        error_if_not( b_edit, "cannot add to histogram without starting" );
        error_if_not( w >= 0.0f, "cannot add negatively weighted value" );
        
        v = max( minv, min( maxv, v ) );
        vals.push_back(v);
        weights.push_back(w);
    }
};

namespace VertexColorMode {
    enum Enum {
        Normal, MeanCurvature, MinDistance, VertDensity, AmbientOcclusion
    };
    const vector<string> Names {
        "Normal", "Mean Curvature", "Minimum Distance", "Vertex Density", "Ambient Occlusion"
    };
};

struct GeoStatistics {
    vec3f norm = zero3f;
    vec3f color = { 0.7f, 0.7f, 0.7f };
    vec3f proj = zero3f;
    float md_val = -1.0f; char md_side = 0;
    float mc_val = 1.0f;
};

struct EvoMeshView : Shape {
    string _type = "EvoMeshView";
    
    EvoMesh *evomesh;
    const vector<vec3f> &pos;
    const vector<vec2i> &edges;
    const vector<vec3i> &faces;
    const vector<vec3f> &fnorms;
    const vector<float> &fareas;
    const int &n_ops;
    const vector<OpNode*> &nodes;
    const vector<vec3i> &map_faces;
    
    vector<OpNode*> nodes_clustered;
    
    EvoMeshState *ems = nullptr;
    EvoMeshStateChange *emsc = nullptr;
    TriangleMesh *mesh_cur = nullptr, *mesh_cur_chg = nullptr;
    TriangleMesh *mesh_pre = nullptr, *mesh_pre_chg = nullptr;
    Grid *grid_cur = nullptr, *grid_cur_chg = nullptr;
    Grid *grid_pre = nullptr, *grid_pre_chg = nullptr;
    
    vector<GeoStatistics> vstats;
    vector<GeoStatistics> fstats;
    
    int n_histogram_bins = 51;
    HistogramData *hist_meancurve_cur   = new HistogramData( n_histogram_bins,  0.0,  1.0 );
    HistogramData *hist_meancurve_pre   = new HistogramData( n_histogram_bins,  0.0,  1.0 );
    HistogramData *hist_hausdorff_cur   = new HistogramData( n_histogram_bins, -1.0,  1.0 );
    HistogramData *hist_hausdorff_pre   = new HistogramData( n_histogram_bins, -1.0,  1.0 );
    HistogramData *hist_facedensity_pre = new HistogramData( n_histogram_bins,  0.0, 10.0 );
    HistogramData *hist_facedensity_cur = new HistogramData( n_histogram_bins,  0.0, 10.0 );
    HistogramData *hist_hddir_cur       = new HistogramData( n_histogram_bins, -1.0,  1.0 );
    HistogramData *hist_hddir_pre       = new HistogramData( n_histogram_bins, -1.0,  1.0 );
    vec3f v_hddir_pre = zero3f, v_hddir_cur = zero3f;
    
    
    void *data_callback;
    void (*fn_change)(void *data) = nullptr;
    
    
    // picker stuff -- TODO: clean-up!
    OpNode *n_over = nullptr;
    int i_over_f = -1;
    int i_over_f_ = -1;
    float mc_min = -1.0f, mc_max = -1.0f;
    vector<int> l_ie_over_add, l_ie_over_del;
    unordered_set<int> s_if_selected;
    
    
    EvoMeshView( EvoMesh *evomesh ) :
    evomesh(evomesh),
    pos(evomesh->pos),
    edges(evomesh->edges),
    faces(evomesh->faces),
    fnorms(evomesh->fnorms),
    fareas(evomesh->fareas),
    n_ops(evomesh->n_ops),
    nodes(evomesh->nodes),
    map_faces(evomesh->map_faces)
    {
        vstats.resize( pos.size() );
        fstats.resize( faces.size() );
        ems = new EvoMeshState();
        reset( evomesh->ems_init );
        
        cluster_reset();
    }
    
    virtual const string &type() { return _type; }
    virtual void apply( fn_apply fn ) { fn( _type, *this ); }
    virtual void apply( const string &type, fn_apply fn ) {
        if( _type == type ) fn( _type, *this );
    }
    virtual void apply( const string &type, function<void (const string&,Shape &)> fn ) {
        if( _type == type ) fn( _type, *this );
    }
    
    virtual void init() { }
    
    virtual void draw() { }
    
    
    
    void play( int i_node ) { play(nodes[i_node]); }
    void play( OpNode *node ) {
        delete emsc;
        dlog.start("Playing");
        dlog.fn("Finding unplayed nodes to play",[&]{ emsc = evomesh->get_unplayed(node,ems); } );
        dlog.fn("Playing nodes",[&]{ ems->apply(emsc); });
        dlog.fn("Updating",[&]{ handle_update(); });
        dlog.end();
    }
    void play( unordered_set<int> si_nodes ) {
        delete emsc;
        emsc = evomesh->get_unplayed(si_nodes,ems);
        ems->apply(emsc);
        handle_update();
    }
    void playmore( int i_n ) { playmore(nodes[i_n]); }
    void playmore( OpNode *node ) {
        evomesh->add_unplayed(node,ems,emsc);
        ems->apply(emsc);
        handle_update();
    }
    void playfast( int i_n ) { playfast(nodes[i_n]); }
    void playfast( OpNode *node ) {
        delete emsc; emsc = evomesh->get_unplayed(node,ems);
        ems->apply(emsc);
        delete emsc; emsc = new EvoMeshStateChange(true);
        handle_update();
    }
    
    void unplay( int i_node ) { unplay(nodes[i_node]); }
    void unplay( OpNode *node ) {
        delete emsc;
        dlog.fn("Finding played nodes to unplay",[&]{ emsc = evomesh->get_played(node,ems); });
        dlog.fn("Unplaying nodes",[&]{ ems->apply(emsc); });
        dlog.fn("Updating",[&]{ handle_update(); });
    }
    void unplay( unordered_set<int> si_nodes ) {
        delete emsc;
        emsc = evomesh->get_played(si_nodes,ems);
        ems->apply(emsc);
        handle_update();
    }
    void unplaymore( int i_n ) { unplaymore(nodes[i_n]); }
    void unplaymore( OpNode *node ) {
        evomesh->add_played(node,ems,emsc);
        ems->apply(emsc);
        handle_update();
    }
    void unplayfast( int i_n ) { unplayfast(nodes[i_n]); }
    void unplayfast( OpNode *node ) {
        delete emsc; emsc = evomesh->get_played(node,ems);
        ems->apply(emsc);
        delete emsc; emsc = new EvoMeshStateChange(true);
        handle_update();
    }
    
    void undo() {
        emsc->fwd = !emsc->fwd;
        ems->apply(emsc);
        handle_update();
    }
    
    
    void reset( const EvoMeshState *ems_next ) {
        ems->copy_from(ems_next);
        delete emsc; emsc = new EvoMeshStateChange(true);
        handle_update();
    }
    
    void cluster_unselected() {
        dlog.start("Cluster: Unselected");
        
        OpNode *root = OpNode::graph_layer(nodes_clustered[0]);
        
        unordered_set<int> repr_verts;
        for( int i_f : s_if_selected ) {
            const vec3i &f = map_faces[i_f];
            repr_verts.insert(f.x);
            repr_verts.insert(f.y);
            repr_verts.insert(f.z);
        }
        function<bool(OpNode*node)> is_selected = [&repr_verts](OpNode*node)->bool {
            for( ReprMoveVert &rmv : node->reprchanges ) {
                if( repr_verts.count(rmv.i_v) ) return true;
            }
            return false;
        };
        
        bool changed = true;
        while(changed) {
            changed = false;
            
            unordered_set<OpNode*> crawl;
            
            dlog.start_("depth");
            crawl.insert(root);
            while(!crawl.empty()) {
                OpNode *node = pop(crawl);
                
                if( !is_selected(node) ) {
                    unordered_set<OpNode*> cluster; cluster.insert(node);
                    unordered_set<OpNode*> grow = node->children;
                    while( !grow.empty() ) {
                        OpNode *n = pop(grow);
                        
                        if( cluster.count(n) ) continue;
                        if( is_selected(n) ) continue;
                        if( !OpNode::is_cluster_valid(n,cluster) ) continue;
                        
                        cluster.insert(n);
                        for( OpNode *c : n->children ) grow.insert(c);
                    }
                    
                    if( cluster.size() > 1 ) {
                        changed = true;
                        if( cluster.count(root) ) root = nullptr;
                        for( OpNode *n : cluster ) crawl.erase(n);
                        node = OpNode::cluster_delete_set(cluster);
                        if( !root ) root = node;
                    }
                    node->clear_excessive_parenting(false);
                }
                for( OpNode *c : node->children ) crawl.insert(c);
            }
            dlog.end();
            
            dlog.start_("breadth");
            crawl.insert(root);
            unordered_set<OpNode*> touched;
            while(!crawl.empty()) {
                OpNode *node = pop(crawl);
                if( touched.count(node) ) continue; touched.insert(node);
                
                unordered_set<OpNode*> children = node->children;
                while( !children.empty() ) {
                    OpNode *child = *children.begin();
                    unordered_set<OpNode*> cluster; cluster.insert(child);
                    bool chg = true;
                    while( chg ) {
                        chg=false;
                        for( OpNode *n : children ) {
                            if( cluster.count(n) ) continue;
                            if( is_selected(n) ) continue;
                            if( OpNode::is_cluster_valid(n,cluster) ) {
                                chg=true;
                                cluster.insert(n);
                            }
                        }
                    }
                    for( OpNode *n : cluster ) children.erase(n);
                    if( cluster.size() > 1 ) {
                        changed = true;
                        for( OpNode *n : cluster ) crawl.erase(n);
                        OpNode::cluster_delete_set(cluster);
                    }
                }
                
                node->clear_excessive_parenting(false);
                for( OpNode *c : node->children ) crawl.insert(c);
            }
            dlog.end();
        }
        
        evomesh->_cluster_update(root,nodes_clustered);
        
        dlog.end();
    }
    
    
    void cluster_clean_parenting() {
        OpNode *root = nodes_clustered[0];
        root->clear_excessive_parenting(true);
        dlog.fn_("Computing positions and colors",[&] {
            root->update_position([&](const vector<int> &l_if) { return evomesh->_average_face_pos(l_if); });
            root->update_color();
        });
    }
    
    void parenting_index_order() {
        OpNode *root = nodes_clustered[0];
        unordered_set<OpNode*> crawl; crawl.insert(root);
        unordered_set<OpNode*> touched;
        while(!crawl.empty()) {
            OpNode *node = pop(crawl);
            if( touched.count(node) ) continue; touched.insert(node);
            for( OpNode *c : node->children ) crawl.insert(c);
            for( OpNode *p : node->parents ) p->children.erase(node);
            node->parents.clear();
            if( node->idx > 0 ) node->add_parent( nodes_clustered[node->idx-1] );
        }
        dlog.fn_("Computing positions and colors",[&] {
            root->update_position([&](const vector<int> &l_if) { return evomesh->_average_face_pos(l_if); });
            root->update_color();
        });
    }
    
    void cluster_reset() {
        evomesh->cluster_reset(nodes_clustered);
        reset_repr_strokes();
    }
    void cluster_runs() {
        evomesh->cluster_runs(nodes_clustered);
        reset_repr_strokes();
    }
    void cluster_same_brush() {
        evomesh->cluster_same_brush(nodes_clustered);
        reset_repr_strokes();
    }
    void cluster_similar_brush() {
        evomesh->cluster_similar_brush(nodes_clustered);
        reset_repr_strokes();
    }
    void cluster_revg( float threshold ) {
        evomesh->cluster_revg(nodes_clustered,threshold);
        reset_repr_strokes();
    }
    void cluster_revg_vi( float threshold ) {
        evomesh->cluster_revg_vi(nodes_clustered,threshold);
        reset_repr_strokes();
    }
    void cluster_subgraphs( int level ) {
        evomesh->cluster_subgraphs(nodes_clustered,level);
        reset_repr_strokes();
    }
    void cluster_zip() {
        evomesh->cluster_zip(nodes_clustered);
        reset_repr_strokes();
    }
    void cluster_cams( float dot_threshold ) {
        evomesh->cluster_cams( nodes_clustered, dot_threshold );
        reset_repr_strokes();
    }
    void cluster_cam_locs( float threshold ) {
        evomesh->cluster_cam_locations( nodes_clustered, threshold );
        reset_repr_strokes();
    }
    
    void reset_repr_strokes() {
        int n_n = nodes_clustered.size();
#       pragma omp parallel for default(none) shared(n_n)
        for( int i_n = 0; i_n < n_n; i_n++ )
            nodes_clustered[i_n]->reset_repr_stroke();
    }
    void compute_repr_strokes( int n_iters, float ke, float ksl, float kso, float timestep ) {
        int n_n = nodes_clustered.size();
#       pragma omp parallel for default(none) shared(n_n,n_iters,ke,ksl,kso,timestep)
        for( int i_n = 0; i_n < n_n; i_n++ )
            nodes_clustered[i_n]->compute_repr_stroke( n_iters, ke, ksl, kso, timestep );
    }
    void update_repr_strokes() {
        int n_n = nodes_clustered.size();
#       pragma omp parallel for default(none) shared(n_n)
        for( int i_n = 0; i_n < n_n; i_n++ )
            nodes_clustered[i_n]->update_repr_stroke();
    }
    
    
    
    PickerData get_face( const ray3f &r );
    PickerData get_face_repr( const ray3f &r, EvoMeshState *ems );
    PickerData get_face_final( const ray3f &r );
    
    void handle_update() {
        
        dlog.start( "Updating" );
        
        dlog.fn_( "Meshes and Grids", [&] {
            delete mesh_pre; delete grid_pre;
            delete mesh_cur_chg; delete grid_cur_chg;
            delete mesh_pre_chg; delete grid_pre_chg;
            
            mesh_pre = ( mesh_cur ? mesh_cur : new TriangleMesh() );
            grid_pre = ( grid_cur ? grid_cur : new Grid(mesh_pre) );
            dlog.fn_( "cur.mesh", [&] { mesh_cur = evomesh->to_TriangleMesh(ems); } );
            dlog.fn_( "cur.grid", [&] { grid_cur = new Grid( mesh_cur ); } );
            dlog.fn_( "change meshes and grids", [&] {
                mesh_cur_chg = evomesh->to_TriangleMesh(emsc,true);
                grid_cur_chg = new Grid( mesh_cur_chg );
                mesh_pre_chg = evomesh->to_TriangleMesh(emsc,false);
                grid_pre_chg = new Grid( mesh_pre_chg );
            } );
        } );
        
        dlog.fn_( "Updating vertex normals", [&] {
            vector<int> &l_if_ = ems->l_if;
            unordered_set<int> s_iv;
            for( int i_f : l_if_ ) {
                const vec3i &f = faces[i_f];
                s_iv.insert(f.x); s_iv.insert(f.y); s_iv.insert(f.z);
            }
            for( int i_v : s_iv ) vstats[i_v].norm = zero3f;
            for( int i_f : l_if_ ) {
                const vec3i &f = faces[i_f];
                vstats[f.x].norm += fnorms[i_f];
                vstats[f.y].norm += fnorms[i_f];
                vstats[f.z].norm += fnorms[i_f];
            }
            for( int i_v : s_iv ) vstats[i_v].norm = normalize(vstats[i_v].norm);
        } );
        
        dlog.fn_( "Computing geometric statistics", [&]() {
            if( glob.view3d_automc ) update_meancurvatures_data();
            
            dlog.start( "Updating Hausdorff Distances" ); {
                for( int i_f : ems->l_if ) {
                    fstats[i_f].md_val = -1.0f;
                    fstats[i_f].md_side = 0;
                    fstats[i_f].proj = zero3f;
                }
                for( int i_v = 0; i_v < pos.size(); i_v++ ) {
                    vstats[i_v].md_val = -1.0f;
                    vstats[i_v].md_side = 0;
                }
                if( emsc->fwd ) {
                    compute_hausdorff( emsc->faces.li_del, grid_cur_chg );
                    compute_histogram_hausdorff( emsc->faces.li_del, hist_hausdorff_pre, v_hddir_pre, hist_hddir_pre, -1.0f );
                    compute_histogram_facedensity( emsc->faces.li_del, hist_facedensity_pre );
                    compute_hausdorff( emsc->faces.li_add, grid_pre_chg );
                    compute_histogram_hausdorff( emsc->faces.li_add, hist_hausdorff_cur, v_hddir_cur, hist_hddir_cur, 1.0f );
                    compute_histogram_facedensity( emsc->faces.li_add, hist_facedensity_cur );
                } else {
                    compute_hausdorff( emsc->faces.li_add, grid_cur_chg );
                    compute_histogram_hausdorff( emsc->faces.li_add, hist_hausdorff_pre, v_hddir_pre, hist_hddir_pre, -1.0f );
                    compute_histogram_facedensity( emsc->faces.li_add, hist_facedensity_pre );
                    compute_hausdorff( emsc->faces.li_del, grid_pre_chg );
                    compute_histogram_hausdorff( emsc->faces.li_del, hist_hausdorff_cur, v_hddir_cur, hist_hddir_cur, 1.0f );
                    compute_histogram_facedensity( emsc->faces.li_del, hist_facedensity_cur );
                }
            } dlog.end();
        } );
        
        if( fn_change ) fn_change(data_callback);
        
        dlog.end();
    }
    
    void handle_selection( ray3f &r, bool add, bool on_final_mesh ) {
        EvoMeshState *ems = (on_final_mesh ? evomesh->ems_final : this->ems);
        PickerData p = ( on_final_mesh ? get_face_final(r) : get_face(r) );
        if( p.idx != -1 ) {
            int n_f = map_faces.size();
            vector<int> &lif = ems->map_if;
            for( int i_f = 0; i_f < n_f; i_f++ ) {
                const vec3i &f = map_faces[i_f];
                if( lif[f.x] != p.idx && lif[f.y] != p.idx && lif[f.z] != p.idx ) continue;
                if( add ) s_if_selected.insert(i_f);
                else s_if_selected.erase(i_f);
            }
        }
        
        PickerData pr = get_face_repr(r,ems);
        if( pr.idx != -1 ) {
            if( add ) s_if_selected.insert(pr.idx);
            else s_if_selected.erase(pr.idx);
        }
    }
    
    
    void compute_histogram_hausdorff( vector<int> &l_if_, HistogramData *hist, vec3f &v_dir, HistogramData *h_vdir, float mult ) {
        hist->start(n_histogram_bins);
        for( int i_f : l_if_ ) {
            auto &fstat = fstats[i_f];
            float hd = fstat.md_val;
            float hd_ = powf( hd, glob.hist_power );
            float a = fareas[i_f];
            hist->add( hd_ * (float)fstat.md_side * mult, a );
        }
        hist->end();
        
        v_dir = zero3f;
        for( int i_f : l_if_ ) {
            const vec3i &f = faces[i_f];
            vec3f v = ((pos[f.x]+pos[f.y]+pos[f.z]) - (vstats[f.x].proj+vstats[f.y].proj+vstats[f.z].proj))/3.0f;
            v_dir += v*fareas[i_f];
        }
        if( !l_if_.empty() ) v_dir = normalize(v_dir);
        
        h_vdir->start(n_histogram_bins);
        for( int i_f : l_if_ ) {
            const vec3i &f = faces[i_f];
            vec3f v = ((pos[f.x]+pos[f.y]+pos[f.z]) - (vstats[f.x].proj+vstats[f.y].proj+vstats[f.z].proj))/3.0f;
            float d = dot(normalize(v),v_dir);
            float ds = (d<0.0?-1.0f:(d>0.0?1.0f:0.0f));
            float d_ = powf( fabs(d), glob.hist_power ) * ds * fareas[i_f];
            h_vdir->add( d_, fareas[i_f] );
        }
        h_vdir->end();
        
    }
    
    void compute_histogram_facedensity( vector<int> &l_if_, HistogramData *hist ) {
        hist->start(n_histogram_bins);
        for( int i_f : l_if_ ) {
            float a_ = powf( 1.0f / fareas[i_f], glob.hist_power );
            hist->add( a_, 1.0f );
        }
        hist->end();
    }
    
    void compute_histogram_meancurvature( vector<int> &l_if_, HistogramData *hist ) {
        hist->start(n_histogram_bins);
        for( int i_f : l_if_ ) {
            float mc = fstats[i_f].mc_val;
            float mc_ = powf( fabs(1.0f-mc), glob.hist_power );
            float a = fareas[i_f];
            hist->add( mc_, a );
        }
        hist->end();
    }
    
    void compute_meancurvatures( vector<int> &l_if_, Grid *g ) {
        if( !g ) return;
        
        float r = glob.view3d_radius;
        
        // find verts to compute
        unordered_set<int> s_iv;
        for( int i_f : l_if_ ) {
            const vec3i &f = faces[i_f];
            s_iv.insert(f.x); s_iv.insert(f.y); s_iv.insert(f.z);
        }
        int n_iv = s_iv.size();
        vector<int> l_iv(s_iv.begin(),s_iv.end());
        
        int n_if = l_if_.size();
        
        // compute mean curvatures for each vertex
        dlog.start_( "Computing Mean Curvature: %d verts, %d faces", n_iv, n_if );
        #pragma omp parallel for default(none) \
            shared(n_iv,s_iv,g,l_iv,r)
        for( int i = 0; i < n_iv; i++ ) {
            int i_v = l_iv[i];
            vstats[i_v].mc_val = g->compute_mean_curvature(pos[i_v],r);
        }
        
        #pragma omp parallel for default(none) \
            shared( n_if, l_if_, g, r )
        for( int i = 0; i < n_if; i++ ) {
            int i_f = l_if_[i];
            const vec3i &f=faces[i_f];
            const vec3f &v0=pos[f.x], &v1=pos[f.y], &v2=pos[f.z];
            const vec3f v=(v0+v1+v2)/3.0f;
            fstats[i_f].mc_val = g->compute_mean_curvature(v,r);
            //fstats[i_f].mc_val = (fstats[i_f].mc_val+vstats[f.x].mc_val+vstats[f.y].mc_val+vstats[f.z].mc_val)/4.0f;
        }
        
        dlog.end();
    }
    void compute_meancurvatures() {
        compute_meancurvatures( ems->l_if, grid_cur );
    }
    
    void update_meancurvatures_data( bool clear=false ) {
        dlog.start("Updating Mean Curvatures");
        if( glob.view3d_clearmc || clear ) {
            for( auto &stat : fstats ) stat.mc_val = -1.0f;
            for( auto &stat : vstats ) stat.mc_val = -1.0f;
        }
        
        if( emsc->fwd ) {
            compute_meancurvatures( emsc->faces.li_del, grid_pre );
            compute_histogram_meancurvature( emsc->faces.li_del, hist_meancurve_pre );
            compute_meancurvatures( emsc->faces.li_add, grid_cur );
            compute_histogram_meancurvature( emsc->faces.li_add, hist_meancurve_cur );
        } else {
            compute_meancurvatures( emsc->faces.li_add, grid_pre );
            compute_histogram_meancurvature( emsc->faces.li_add, hist_meancurve_pre );
            compute_meancurvatures( emsc->faces.li_del, grid_cur );
            compute_histogram_meancurvature( emsc->faces.li_del, hist_meancurve_cur );
        }
        dlog.end();
    }
    
    
    
    void compute_hausdorff( vector<int> &l_if_, Grid *g ) {
        if( !g || l_if_.empty() ) return;
        
        TriangleMesh *m = g->_tmesh;
        
        // find verts to compute
        unordered_set<int> s_iv;
        for( int i_f : l_if_ ) {
            const vec3i &f = faces[i_f];
            s_iv.insert(f.x); s_iv.insert(f.y); s_iv.insert(f.z);
        }
        int n_iv = s_iv.size();
        vector<int> l_iv; l_iv.reserve(n_iv); for( int i_v : s_iv ) l_iv.push_back(i_v);
        
        dlog.start_( "Computing Hausdorff Distance: %d verts", n_iv );
        #pragma omp parallel for default(none) \
            shared( l_iv, g, m, n_iv )
        for( int i = 0; i < n_iv; i++ ) {
            int i_v = l_iv[i];
            
            int i_f; float ba, bb;
            const vec3f &v1 = pos[i_v];
            vec3f v0 = g->get_nearest_surface_point(v1, i_f, ba, bb);
            if( i_f == -1 ) {
                vstats[i_v].proj = v1;
                vstats[i_v].md_val = 0.0f;
                vstats[i_v].md_side = 0;
            } else {
                vec3f vd = v1-v0;
                float d = dot( m->l_fn[i_f], normalize(vd) );
                vstats[i_v].proj = v0;
                vstats[i_v].md_val = length(vd);
                vstats[i_v].md_side = (fabs(d)<0.0001?0:(d>0.0?1:-1));
            }
        }
        dlog.end();
        
        for( int i_f : l_if_ ) {
            const vec3i &f = faces[i_f];
            float d0 = vstats[f.x].md_val, d1 = vstats[f.y].md_val, d2 = vstats[f.z].md_val;
            char o0 = vstats[f.x].md_side, o1 = vstats[f.y].md_side, o2 = vstats[f.z].md_side;
            float d = d0*(float)o0 + d1*(float)o1 + d2*(float)o2;
            fstats[i_f].md_val = (d0+d1+d2)/3.0f;
            fstats[i_f].md_side = (char)(fabs(d)<0.0001?0:(d>0.0?1:-1));
        }
    }
    void compute_hausdorff() {
        compute_hausdorff( ems->l_if, grid_pre );
    }
    
    
    
    TriangleMesh *to_TriangleMesh() {
        return evomesh->to_TriangleMesh( ems->l_ie, ems->l_if );
    }
    TriangleMesh *to_TriangleMesh( vector<int> &l_iedges, vector<int> &l_ifaces ) {
        return evomesh->to_TriangleMesh( l_iedges, l_ifaces );
    }
    
    void draw_mesh_faces();
    void draw_mesh_lines();
    void draw_global_verts();
    
    void draw_depgraph_dag( const vec3f &viewd );
    void draw_depgraph_dag( const vec3f &viewd, OpNode *root );
    
    void draw_brush_stats();
    
    void draw_camera();
    void draw_faces( const vector<int> &l_if_ );
    void draw_faces( const set<int> &l_if_ );
    void draw_face( const int i_f );
    void draw_face_repr( unordered_set<int> &s_ifr, EvoMeshState *ems );
    void draw_edges( const vector<int> &l_ie_ );
    void draw_changes_add();
    void draw_changes_del();
    void draw_brush_strokes();
    void draw_brush_strokes( vector<vec3f> &pos_repr );
    void draw_repr_brush_strokes();
    //void draw_changes_morphing();
};



#endif





#include "evomesh.h"
#include "geom.h"
#include "mesh_utils.h"
#include "accel_grid.h"
#include "io_bin.h"
#include "std.h"

#include <queue>
#include <unordered_map>

EvoMesh::EvoMesh( const char *fn_json ) {
    vector<int> l_init_ie, l_init_if;
    vector<int> l_final_ie, l_final_if;
    
    dlog.start("Creating EvoMesh");
    
    dlog.start_("Loading JSON file");
    JsonNavigator nav = JsonNavigator(fn_json);
    dlog.end();
    
    dlog.fn_("Loading binary mesh data", [&] {
        LoadBinaryFile( nav["fn_glvp"].get_string(), pos );
        LoadBinaryFile( nav["fn_glev"].get_string(), edges );
        LoadBinaryFile( nav["fn_glfv"].get_string(), faces );
        LoadBinaryFile( nav["fn_liie"].get_string(), l_init_ie );
        LoadBinaryFile( nav["fn_liif"].get_string(), l_init_if );
        LoadBinaryFile( nav["fn_lfie"].get_string(), l_final_ie );
        LoadBinaryFile( nav["fn_lfif"].get_string(), l_final_if );
    });
    
    n_p = pos.size();
    n_e = edges.size();
    n_f = faces.size();
    
    dlog.fn_("Computing normals and areas", [&] {
        for( auto face : faces ) {
            vec3f &v0=pos[face.x],&v1=pos[face.y],&v2=pos[face.z];
            fnorms.push_back(triangle_normal(v0,v1,v2));
            fareas.push_back(compute_triangle_area(v0,v1,v2));
        }
    });
    
    dlog.fn_("Creating initial and final states", [&] {
        ems_init = new EvoMeshState();
        ems_init->l_ie = l_init_ie;
        ems_init->l_if = l_init_if;
        
        ems_final = new EvoMeshState();
        ems_final->l_ie = l_final_ie;
        ems_final->l_if = l_final_if;
    });
    
    string s_depgraph_cache = to_string("%s.depgraph",fn_json);
    if( fileexists( s_depgraph_cache ) ) {
        dlog.start("Loading DepGraph from cache file");
        _depgraph_load( s_depgraph_cache.c_str() );
        dlog.end();
    } else {
        dlog.start("Creating DepGraph from binary change data");
        _depgraph_create( nav );
        dlog.end();
        
        dlog.start("Saving DepGraph to cache file");
        _depgraph_save( s_depgraph_cache.c_str() );
        dlog.end();
    }
    
    dlog.start("Mirroring Strokes");
    for( OpNode *n : nodes ) {
        auto &s = n->stroke;
        
        if( s.mirror == 0 || s.pos.size()==0 ) continue;
        s.brk[s.pos.size()-1] = true;
        
        if( s.mirror & 1 ) {
            int n_p = s.pos.size();
            for( int i = 0; i < n_p; i++ ) {
                vec3f p = s.pos[i];
                p.x *= -1.0f;
                s.pos.push_back(p);
                s.brk.push_back(i == 0 ? true : s.brk[i]);
            }
        }
        if( s.mirror & 2 ) {
            int n_p = s.pos.size();
            for( int i = 0; i < n_p; i++ ) {
                vec3f p = s.pos[i];
                p.y *= -1.0f;
                s.pos.push_back(p);
                s.brk.push_back(i == 0 ? true : s.brk[i]);
            }
        }
        if( s.mirror & 4 ) {
            int n_p = s.pos.size();
            for( int i = 0; i < n_p; i++ ) {
                vec3f p = s.pos[i];
                p.z *= -1.0f;
                s.pos.push_back(p);
                s.brk.push_back(i == 0 ? true : s.brk[i]);
            }
        }
    }
    dlog.end();
    
    //_compute_revg_vi();
    
    ems_init->l_in.push_back(0); ems_init->s_in.insert(0);
    for( int i_n = 0; i_n < n_ops; i_n++ ) { ems_final->l_in.push_back(i_n); ems_final->s_in.insert(i_n); }
    
    dlog.end();
}

void EvoMesh::_depgraph_load( const char *fn_cache ) {
    FILE *fp = fopen(fn_cache,"rb"); assert(fp);
    int ver = LoadBinaryFile_int(fp);
    error_if_not_va( ver == 1, "Expected version 1, but got %d", ver );
    
    assert( LoadBinaryFile_int(fp) == n_e );
    etimes.resize(n_e,{-1,-1});
    for( StartEndData &sed : etimes ) {
        LoadBinaryFile(fp,sed.i0);
        LoadBinaryFile(fp,sed.i1);
    }
    
    assert( LoadBinaryFile_int(fp) == n_f );
    ftimes.resize(n_f,{-1,-1});
    for( StartEndData &sed : ftimes ) {
        LoadBinaryFile(fp,sed.i0);
        LoadBinaryFile(fp,sed.i1);
    }
    
    LoadBinaryFile(fp,n_ops);
    for( int i_n = 0; i_n < n_ops; i_n++ ) {
        OpNode *node = new OpNode();
        nodes.push_back(node);
        
        assert( LoadBinaryFile_int(fp) == i_n );
        node->idx = i_n;
        
        LoadBinaryFile(fp,node->pos0);
        LoadBinaryFile(fp,node->pos1);
        LoadBinaryFile(fp,node->color);
        LoadBinaryFile(fp,node->bookmarked);
        
        LoadBinaryFile(fp,node->camera.frame);
        LoadBinaryFile(fp,node->camera.dist);
        LoadBinaryFile(fp,node->camera.ortho);
        
        node->stroke.brush = (BrushName::Enum)LoadBinaryFile_int(fp);
        node->stroke.mod = (BrushModifier::Enum)LoadBinaryFile_int(fp);
        LoadBinaryFile(fp,node->stroke.pos);
        LoadBinaryFile(fp,node->stroke.brk);
        LoadBinaryFile(fp,node->stroke.mirror);
        node->stroke.rpos.resize(LoadBinaryFile_int(fp));
        for( auto &rpos : node->stroke.rpos ) {
            LoadBinaryFile(fp,rpos.i_f);
            LoadBinaryFile(fp,rpos.ba);
            LoadBinaryFile(fp,rpos.bb);
            LoadBinaryFile(fp,rpos.brk);
            LoadBinaryFile(fp,rpos.n);
        }
        
        LoadBinaryFile(fp,node->edges.li_del);
        LoadBinaryFile(fp,node->edges.li_add);
        LoadBinaryFile(fp,node->faces.li_del);
        LoadBinaryFile(fp,node->faces.li_add);
        
        node->reprchanges.resize(LoadBinaryFile_int(fp));
        for( auto &repr : node->reprchanges ) {
            LoadBinaryFile(fp,repr.i_v);
            LoadBinaryFile(fp,repr.i_f0);
            LoadBinaryFile(fp,repr.i_f1);
            LoadBinaryFile(fp,repr.v0);
            LoadBinaryFile(fp,repr.v1);
        }
        
        LoadBinaryFile(fp,node->stats.area_post);
        LoadBinaryFile(fp,node->stats.area_pre);
        LoadBinaryFile(fp,node->stats.avgnorm_post);
        LoadBinaryFile(fp,node->stats.avgnorm_pre);
        node->stats.classification = (BrushClassification::Enum)LoadBinaryFile_int(fp);
        LoadBinaryFile(fp,node->stats.stddevnorm_post);
        LoadBinaryFile(fp,node->stats.stddevnorm_pre);
        LoadBinaryFile(fp,node->stats.vol_delta);
        
        int n_parents = LoadBinaryFile_int(fp);
        for( int i_p = 0; i_p < n_parents; i_p++ ) {
            int i_np = LoadBinaryFile_int(fp);
            node->add_parent(nodes[i_np]);
        }
    }
    
    LoadBinaryFile(fp,map_faces);
    LoadBinaryFile(fp,ems_init->map_pos);
    LoadBinaryFile(fp,ems_init->map_if);
    LoadBinaryFile(fp,ems_final->map_pos);
    LoadBinaryFile(fp,ems_final->map_if);
    
    fclose(fp);
}
void EvoMesh::_depgraph_save( const char *fn_cache ) {
    FILE *fp = fopen(fn_cache,"wb"); assert( fp );
    
    SaveBinaryFile(fp,1);
    
    SaveBinaryFile(fp,(int)etimes.size());
    for( StartEndData &sed : etimes ) {
        SaveBinaryFile(fp,sed.i0);
        SaveBinaryFile(fp,sed.i1);
    }
    SaveBinaryFile(fp,(int)ftimes.size());
    for( StartEndData &sed : ftimes ) {
        SaveBinaryFile(fp,sed.i0);
        SaveBinaryFile(fp,sed.i1);
    }
    
    SaveBinaryFile(fp,(int)nodes.size());
    for( OpNode *node : nodes ) {
        SaveBinaryFile(fp,node->idx);
        SaveBinaryFile(fp,node->pos0);
        SaveBinaryFile(fp,node->pos1);
        SaveBinaryFile(fp,node->color);
        SaveBinaryFile(fp,node->bookmarked);
        
        SaveBinaryFile(fp,node->camera.frame);
        SaveBinaryFile(fp,node->camera.dist);
        SaveBinaryFile(fp,node->camera.ortho);
        
        SaveBinaryFile(fp,node->stroke.brush);
        SaveBinaryFile(fp,node->stroke.mod);
        SaveBinaryFile(fp,node->stroke.pos);
        SaveBinaryFile(fp,node->stroke.brk);
        SaveBinaryFile(fp,node->stroke.mirror);
        SaveBinaryFile(fp,(int)node->stroke.rpos.size());
        for( auto &rpos : node->stroke.rpos ) {
            SaveBinaryFile(fp,rpos.i_f);
            SaveBinaryFile(fp,rpos.ba);
            SaveBinaryFile(fp,rpos.bb);
            SaveBinaryFile(fp,rpos.brk);
            SaveBinaryFile(fp,rpos.n);
        }
        
        SaveBinaryFile(fp,node->edges.li_del);
        SaveBinaryFile(fp,node->edges.li_add);
        SaveBinaryFile(fp,node->faces.li_del);
        SaveBinaryFile(fp,node->faces.li_add);
        
        SaveBinaryFile(fp,(int)node->reprchanges.size());
        for( auto &repr : node->reprchanges ) {
            SaveBinaryFile(fp,repr.i_v);
            SaveBinaryFile(fp,repr.i_f0);
            SaveBinaryFile(fp,repr.i_f1);
            SaveBinaryFile(fp,repr.v0);
            SaveBinaryFile(fp,repr.v1);
        }
        
        SaveBinaryFile(fp,node->stats.area_post);
        SaveBinaryFile(fp,node->stats.area_pre);
        SaveBinaryFile(fp,node->stats.avgnorm_post);
        SaveBinaryFile(fp,node->stats.avgnorm_pre);
        SaveBinaryFile(fp,node->stats.classification);
        SaveBinaryFile(fp,node->stats.stddevnorm_post);
        SaveBinaryFile(fp,node->stats.stddevnorm_pre);
        SaveBinaryFile(fp,node->stats.vol_delta);
        
        SaveBinaryFile(fp,(int)node->parents.size());
        for( auto p : node->parents ) SaveBinaryFile(fp,p->idx);
    }
    
    SaveBinaryFile(fp,map_faces);
    SaveBinaryFile(fp,ems_init->map_pos);
    SaveBinaryFile(fp,ems_init->map_if);
    SaveBinaryFile(fp,ems_final->map_pos);
    SaveBinaryFile(fp,ems_final->map_if);
    
    fclose(fp);
}

void EvoMesh::_depgraph_create( JsonNavigator nav ) {
    vector<vector<vector<int>>> eops, fops;
    vector<float> cam_dists;
    vector<bool> cam_orthos;
    vector<frame3f> cam_frames;
    vector<string> str_brushes;
    vector<string> str_mods;
    vector<vector<vec3f>> str_pos;
    vector<vector<bool>> str_brks;
    vector<int> str_mirror;
    
    const float dist_break = 100.0f; //0.1f;
    
    dlog.fn("Change data",[&]{
        dlog.fn_("Loading binary change data",[&]{
            LoadBinaryFile( nav["fn_leop"].get_string(), eops );
            LoadBinaryFile( nav["fn_lfop"].get_string(), fops );
            LoadBinaryFile( nav["fn_glcd"].get_string(), cam_dists );
            LoadBinaryFile( nav["fn_glco"].get_string(), cam_orthos );
            LoadBinaryFile( nav["fn_glcf"].get_string(), cam_frames );
            LoadBinaryFile( nav["fn_glsb"].get_string(), str_brushes );
            LoadBinaryFile( nav["fn_glsm"].get_string(), str_mods );
            LoadBinaryFile( nav["fn_glsv"].get_string(), str_pos );
            LoadBinaryFile( nav["fn_glss"].get_string(), str_mirror );
            
            assert( eops.size() == fops.size() );
            // more assertions for robustness?
        });
        
        dlog.fn_("Filtering out non-changes",[&]{
            vector<int> i_ops;
            vector<int> li_skip;
            for( int i_op = 0; i_op < fops.size(); i_op++ ) {
                if( (fops[i_op][0].size()+fops[i_op][1].size()+eops[i_op][0].size()+eops[i_op][1].size()) == 0 ) {
                    li_skip.push_back(i_op);
                    continue;
                } else {
                    i_ops.push_back(i_op);
                }
            }
            reorder(eops,i_ops);
            reorder(fops,i_ops);
            reorder(cam_dists,i_ops);
            reorder(cam_orthos,i_ops);
            reorder(cam_frames,i_ops);
            reorder(str_brushes,i_ops);
            reorder(str_mods,i_ops);
            reorder(str_pos,i_ops);
            reorder(str_mirror,i_ops);
            dlog.print("Skipping: ", li_skip);
        });
        
        dlog.fn("Filtering and breaking brush strokes",[&]{
            dlog.print("dist_break: %f",dist_break);
            for( int i_op = 0; i_op < str_pos.size(); i_op++ ) {
                vector<vec3f> pos_;
                vector<bool> brks_;
                vec3f v_; bool first=true;
                for( vec3f &v : str_pos[i_op] ) {
                    if( fabs(v.x)>100.0f || fabs(v.y)>100.0f || fabs(v.z)>100.0f ) continue;
                    
                    pos_.push_back(v);
                    brks_.push_back( !first && (lengthSqr(v_-v) > dist_break) );
                    
                    v_ = v;
                    first = false;
                }
                swap( str_pos[i_op], pos_ );
                str_brks.push_back( brks_ );
            }
        });
    });
    
    
    n_ops = fops.size();
    nodes.resize( n_ops, nullptr );
    etimes.resize(n_e,{0,n_ops});
    ftimes.resize(n_f,{0,n_ops});
    
    
    dlog.start("Initializing operation dependency graph"); {
        
        dlog.start_("Constructing %d nodes", n_ops); {
            for( int i_op = 0; i_op < n_ops; i_op++ ) {
                nodes[i_op] = new OpNode();
                nodes[i_op]->idx = i_op;
                
                swap( nodes[i_op]->edges.li_del, eops[i_op][0] );
                swap( nodes[i_op]->edges.li_add, eops[i_op][1] );
                swap( nodes[i_op]->faces.li_del, fops[i_op][0] );
                swap( nodes[i_op]->faces.li_add, fops[i_op][1] );
                
                nodes[i_op]->camera.dist = cam_dists[i_op];
                nodes[i_op]->camera.ortho = cam_orthos[i_op];
                nodes[i_op]->camera.frame = cam_frames[i_op];
                
                nodes[i_op]->stroke.brush = BrushName::Map_Name_Enum[str_brushes[i_op]];
                nodes[i_op]->stroke.mod = BrushModifier::Map_Name_Enum[str_mods[i_op]];
                nodes[i_op]->stroke.mirror = str_mirror[i_op];
                swap( nodes[i_op]->stroke.pos, str_pos[i_op] );
                swap( nodes[i_op]->stroke.brk, str_brks[i_op] );
            }
        } dlog.end();
        
        dlog.start_("Assigning start and end times"); {
            for( int i_op = 0; i_op < n_ops; i_op++ ) {
                for( int i_e : nodes[i_op]->edges.li_add ) etimes[i_e].i0 = i_op;
                for( int i_e : nodes[i_op]->edges.li_del ) etimes[i_e].i1 = i_op;
                for( int i_f : nodes[i_op]->faces.li_add ) ftimes[i_f].i0 = i_op;
                for( int i_f : nodes[i_op]->faces.li_del ) ftimes[i_f].i1 = i_op;
            }
        } dlog.end();
        
        dlog.start("Determining node dependencies"); {
            dlog.progress_start();
            for( int i_op = 0; i_op < n_ops; i_op++ ) {
                dlog.progress_update((float)i_op / (float)(n_ops-1));
                OpNode *opnode = nodes[i_op];
                unordered_set<int> si_nodes;
                for( int i_e : opnode->edges.li_del ) si_nodes.insert( etimes[i_e].i0 );
                for( int i_f : opnode->faces.li_del ) si_nodes.insert( ftimes[i_f].i0 );
                vector<int> li_nodes( si_nodes.begin(), si_nodes.end() );
                sort( li_nodes.begin(), li_nodes.end() );
                reverse( li_nodes.begin(), li_nodes.end() );
                for( int i_n : li_nodes ) opnode->add_parent( nodes[i_n] );
            }
            dlog.progress_end();
        } dlog.end();
        
        //dlog.start_( "Computing position for each node" ); {
        //    nodes[0]->update_position([&](const vector<int> &l_if) { return _average_face_pos(l_if); });
        //} dlog.end();
        
        dlog.start_( "Coloring branches" ); {
            srand(0);
            nodes[0]->update_color();
        } dlog.end();
        
    } dlog.end();
    
    //dlog.start("Computing statistics and classifying"); {
    //    _compute_brush_volume_deltas();
    //    _compute_brush_areas_pre();
    //    _compute_brush_areas_post();
    //    _compute_brush_avgnorms_pre();
    //    _compute_brush_avgnorms_post();
    //    _compute_brush_stddevnorms_pre();
    //    _compute_brush_stddevnorms_post();
    //    _compute_brush_classifications();
    //} dlog.end();
    
    //dlog.start("Building parameterizing mesh"); {
    //    _face_mapping_compute( nav["fn_rlvp"].get_string(), nav["fn_rlfv"].get_string() );
    //} dlog.end();
}




void EvoMesh::_cluster_update( OpNode *root, vector<OpNode*> &l_nodes ) {
    dlog.start("Updating nodes");
    l_nodes.clear();
    dlog.fn_("Graph to List",[&]{ OpNode::graph_to_list(root,l_nodes,true); });
    dlog.fn_("Updating operations",[&] {
        //dlog.print("Skipping"); return;
        int n_n = l_nodes.size();
#       pragma omp parallel for default(none) shared(l_nodes,n_n)
        for( int i_n = 0; i_n < n_n; i_n++ ) l_nodes[i_n]->update_to_sub();
    });
    dlog.fn_("Computing positions and colors",[&] {
        root->update_position([&](const vector<int> &l_if) { return _average_face_pos(l_if); });
        root->update_color();
    });
    dlog.fn_("Computing repr stroke for 0 steps",[&]{
        int n_n = l_nodes.size();
#       pragma omp parallel for default(none) shared(l_nodes,n_n)
        for( int i_n = 0; i_n < n_n; i_n++ ) {
            l_nodes[i_n]->reset_repr_stroke();
            l_nodes[i_n]->compute_repr_stroke(0,0.0f,0.0f,1.0f,0.0f);
        }
    });
    dlog.end();
}


void EvoMesh::cluster_reset( vector<OpNode*> &l_nodes ) {
    dlog.start("Cluster: Reset");
    for( OpNode *n : l_nodes ) delete n; l_nodes.clear();
    OpNode *root = OpNode::graph_layer(nodes[0]);
    l_nodes.resize(nodes.size());
    //_cluster_update(root,l_nodes);
    root->traverse_dfs([&](OpNode*n) {l_nodes[n->idx] = n;});
    dlog.fn_("Updating operations",[&] {
        //dlog.print("Skipping"); return;
        int n_n = l_nodes.size();
#       pragma omp parallel for default(none) shared(l_nodes,n_n)
        for( int i_n = 0; i_n < n_n; i_n++ ) l_nodes[i_n]->update_to_sub();
    });
    dlog.fn_("Computing positions and colors",[&] {
        root->update_position([&](const vector<int> &l_if) { return _average_face_pos(l_if); });
        root->update_color();
    });
    dlog.end();
}

void EvoMesh::cluster_runs( vector<OpNode*> &l_nodes ) {
    dlog.start("Cluster: Runs of nodes with one parent and one child");
    
    unordered_set<OpNode*> crawl; crawl.insert(l_nodes[0]);
    unordered_set<OpNode*> touched;
    OpNode *root_ = nullptr;
    
    while( !crawl.empty() ) {
        OpNode *node = pop(crawl);
        if( touched.count(node) ) continue;
        
        unordered_set<OpNode*> cluster; cluster.insert(node);
        while( node->children.size() == 1 && (*node->children.begin())->parents.size() == 1 ) {
            OpNode *c = *node->children.begin();
            if( c->parents.size() != 1 || c->children.size() != 1 ) break;
            node = c;
            cluster.insert(node);
        }
        OpNode *node_ = OpNode::cluster_delete_set(cluster);
        
        touched.insert(node_);
        for( OpNode *c : node_->children ) crawl.insert(c);
        if( !root_ ) root_ = node_;
    }
    
    _cluster_update( root_, l_nodes );
    
    dlog.end();
}

void EvoMesh::cluster_zip( vector<OpNode*> &l_nodes ) {
    dlog.start("Cluster: Zip");
    
    unordered_set<OpNode*> crawl; crawl.insert(l_nodes[0]);
    unordered_set<OpNode*> touched;
    OpNode *root = nullptr;
    
    while( !crawl.empty() ) {
        unordered_set<OpNode*> cluster;
        unordered_set<int> si_cluster;
        
        bool chg = true;
        while(chg) {
            chg = false;
            for( OpNode *n : crawl ) {
                if( !OpNode::is_cluster_valid(n,cluster) ) continue;
                bool any_parent = (n==l_nodes[0]);
                bool all_in = true;
                for( OpNode *p : n->parents ) {
                    if( touched.count(p) ) {
                        any_parent = true;
                    } else {
                        if( !cluster.count(p) ) all_in = false;
                    }
                }
                if( !any_parent || !all_in ) continue;
                
                cluster.insert(n);
                si_cluster.insert(n->idx);
                chg = true;
            }
            for( OpNode *n : cluster ) crawl.erase(n);
        }
        
        dlog.print( "Clustering: ", si_cluster );
        
        OpNode *node = OpNode::cluster_delete_set(cluster);
        touched.insert(node);
        if( !root ) root = node;
        
        //for( OpNode *n : cluster ) crawl.erase(n);
        for( OpNode *c : node->children ) crawl.insert(c);
    }
    
    _cluster_update( root, l_nodes );
    
    dlog.end();
}

void EvoMesh::cluster_same_brush( vector<OpNode*> &l_nodes ) {
    dlog.start("Cluster: Same Brush");
    
    OpNode *root = l_nodes[0];
    
    bool changed = true;
    while(changed) {
        changed = false;
        
        unordered_set<OpNode*> crawl;
        
        dlog.start_("depth");
        crawl.insert(root);
        while(!crawl.empty()) {
            OpNode *node = pop(crawl);
            
            int brush = node->stroke.brush;
            
            unordered_set<OpNode*> cluster; cluster.insert(node);
            unordered_set<OpNode*> grow = node->children;
            while( !grow.empty() ) {
                OpNode *n = pop(grow);
                
                if( cluster.count(n) ) continue;
                if( n->stroke.brush != brush ) continue;
                if( !OpNode::is_cluster_valid(n,cluster) ) continue;
                
                cluster.insert(n);
                for( OpNode *c : n->children ) grow.insert(c);
            }
            
            if( cluster.size() > 1 ) {
                changed = true;
                if( cluster.count(root) ) root = nullptr;
                for( OpNode *n : cluster ) crawl.erase(n);
                node = OpNode::cluster_delete_set(cluster);
                if( !root ) root = node;
            }
            //node->clear_excessive_parenting(false);
            for( OpNode *c : node->children ) crawl.insert(c);
        }
        dlog.end();
        
        if(false) {
            dlog.start_("breadth");
            crawl.insert(root);
            unordered_set<OpNode*> touched;
            while(!crawl.empty()) {
                OpNode *node = pop(crawl);
                if( touched.count(node) ) continue; touched.insert(node);
                
                unordered_set<OpNode*> children = node->children;
                while( !children.empty() ) {
                    OpNode *child = *children.begin();
                    unordered_set<OpNode*> cluster; cluster.insert(child);
                    bool chg = true;
                    while( chg ) {
                        chg=false;
                        for( OpNode *n : children ) {
                            if( cluster.count(n) ) continue;
                            if( n->stroke.brush != child->stroke.brush ) continue;
                            if( OpNode::is_cluster_valid(n,cluster) ) {
                                chg=true;
                                cluster.insert(n);
                            }
                        }
                    }
                    for( OpNode *n : cluster ) children.erase(n);
                    if( cluster.size() > 1 ) {
                        changed = true;
                        for( OpNode *n : cluster ) crawl.erase(n);
                        OpNode::cluster_delete_set(cluster);
                    }
                }
                
                //node->clear_excessive_parenting(false);
                for( OpNode *c : node->children ) crawl.insert(c);
            }
            dlog.end();
        }
    }
    
    _cluster_update(root, l_nodes);
    dlog.print("Node Count: %d",l_nodes.size() );
    
    dlog.end();
}

void EvoMesh::cluster_similar_brush( vector<OpNode*> &l_nodes ) {
    dlog.start("Cluster: Similar Brush");
    
    //BrushName::Enum _;
    const vector<int> br_class = {
        0, // Unspecified,
        2, // Blob,             1
        2, // Brush,
        2, // Clay,
        2, // ClayStrips,
        3, // Crease,
        2, // Draw,
        4, // FillDeepen,
        4, // FlattenContrast,
        5, // Grab,
        2, // InflateDeflate,   1
        0, // Layer,
        0, // Mask,
        5, // Nudge,
        3, // PinchMagnify,
        4, // Polish,
        4, // ScrapePeaks,
        2, // SculptDraw,
        4, // Smooth,
        5, // SnakeHook,
        5, // Thumb,
        5, // Twist,
        0, // Mixed
    };
    
    OpNode *root = l_nodes[0];
    
    bool changed = true;
    while(changed) {
        changed = false;
        
        unordered_set<OpNode*> crawl;
        
        dlog.start_("depth");
        crawl.insert(root);
        while(!crawl.empty()) {
            OpNode *node = pop(crawl);
            
            int brush = br_class[node->stroke.brush];
            
            unordered_set<OpNode*> cluster; cluster.insert(node);
            unordered_set<OpNode*> grow = node->children;
            while( !grow.empty() ) {
                OpNode *n = pop(grow);
                
                if( cluster.count(n) ) continue;
                if( br_class[n->stroke.brush] != brush ) continue;
                if( !OpNode::is_cluster_valid(n,cluster) ) continue;
                
                cluster.insert(n);
                for( OpNode *c : n->children ) grow.insert(c);
            }
            
            if( cluster.size() > 1 ) {
                changed = true;
                if( cluster.count(root) ) root = nullptr;
                for( OpNode *n : cluster ) crawl.erase(n);
                node = OpNode::cluster_delete_set(cluster);
                if( !root ) root = node;
            }
            node->clear_excessive_parenting(false);
            for( OpNode *c : node->children ) crawl.insert(c);
        }
        dlog.end();
        
        dlog.start_("breadth");
        crawl.insert(root);
        unordered_set<OpNode*> touched;
        while(!crawl.empty()) {
            OpNode *node = pop(crawl);
            if( touched.count(node) ) continue; touched.insert(node);
            
            unordered_set<OpNode*> children = node->children;
            while( !children.empty() ) {
                OpNode *child = *children.begin();
                unordered_set<OpNode*> cluster; cluster.insert(child);
                bool chg = true;
                while( chg ) {
                    chg=false;
                    for( OpNode *n : children ) {
                        if( cluster.count(n) ) continue;
                        if( br_class[n->stroke.brush] != child->stroke.brush ) continue;
                        if( OpNode::is_cluster_valid(n,cluster) ) {
                            chg=true;
                            cluster.insert(n);
                        }
                    }
                }
                for( OpNode *n : cluster ) children.erase(n);
                if( cluster.size() > 1 ) {
                    changed = true;
                    for( OpNode *n : cluster ) crawl.erase(n);
                    OpNode::cluster_delete_set(cluster);
                }
            }
            
            node->clear_excessive_parenting(false);
            for( OpNode *c : node->children ) crawl.insert(c);
        }
        dlog.end();
    }
    
    _cluster_update(root, l_nodes);
    dlog.print("Node Count: %d",l_nodes.size() );
    
    dlog.end();
}

void EvoMesh::cluster_revg( vector<OpNode*> &l_nodes, float threshold ) {
    
    dlog.start_("Clustering by RevG rules");
    dlog.print("threshold = %f", threshold);
    
    OpNode *root = l_nodes[0];
    unordered_set<OpNode*> crawl; crawl.insert(root);
    unordered_set<OpNode*> touched;
    while(!crawl.empty()) {
        OpNode *node = pop(crawl);
        if( touched.count(node) ) continue;
        float vi = node->stats.revg_visual_importance;
        
        unordered_set<OpNode*> cluster; cluster.insert(node);
        unordered_set<OpNode*> grow = node->children;
        while(!grow.empty()) {
            OpNode *n_ = pop(grow);
            
            if( cluster.count(n_) ) continue;
            if( !OpNode::is_cluster_valid(n_,cluster) ) continue;
            
            float vi_ = n_->stats.revg_visual_importance;
            if( vi+vi_ > threshold ) continue;
            
            vi += vi_;
            cluster.insert(n_);
            for( auto c : n_->children ) grow.insert(c);
        }
        
        if( cluster.size() > 1 ) {
            for( auto n : cluster ) crawl.erase(n);
            if( cluster.count(root) ) root = nullptr;
            node = OpNode::cluster_delete_set(cluster);
            node->stats.revg_visual_importance = vi;
            if( !root ) root = node;
        }
        
        touched.insert(node);
        for( auto c : node->children ) crawl.insert(c);
    }
    
    _cluster_update(root, l_nodes);
    dlog.print("Node Count: %d",l_nodes.size() );
    
    dlog.end();
}
void EvoMesh::cluster_revg_vi( vector<OpNode*> &l_nodes, float threshold ) {
    
    for( OpNode *n : l_nodes ) delete n; l_nodes.clear();
    
    dlog.start_("Clustering to RevG Visual Importance");
    dlog.print("threshold = %f", threshold);
    
    /*OpNode *root = OpNode::create_layer(n_root);
    unordered_set<OpNode*> desc; root->get_descendents(desc); desc.insert(root);
    root = nullptr;
    
    priority_queue<OpNode*,vector<OpNode*>,OpNodeSmallestIndex> q; for( OpNode *n : desc ) q.push(n);
    unordered_set<OpNode*> touched;
    
    dlog.start("Clustering");
    while(!q.empty()) {
        OpNode *node = q.top(); q.pop();
        if( touched.count(node) ) continue;
        
        float vi = node->stats.revg_visual_importance;
        touched.insert(node);
        
        // find all nodes to be clustered
        unordered_set<OpNode*> cluster; cluster.insert(node);
        
        if( node->idx == 0 ) {
            touched.insert(node);
        } else {
            vector<OpNode*> dfs(node->children.begin(),node->children.end());
            while( !dfs.empty() ) {
                OpNode *n_ = dfs.back(); dfs.pop_back();
                
                if( touched.count(n_) ) continue;           // already in cluster?
                
                unordered_set<OpNode*> cluster_add; cluster_add.insert(n_);
                unordered_set<OpNode*> check(n_->parents.begin(),n_->parents.end());
                while(!check.empty()) {
                    OpNode *p = pop(check);
                    if( touched.count(p) || cluster_add.count(p) ) continue;
                    for( auto p_ : p->parents ) check.insert(p_);
                    cluster_add.insert(p);
                }
                bool all_good = true;
                for( auto ca : cluster_add ) if( ca->stats.revg_visual_importance > threshold ) {all_good=false; break;}
                if( !all_good ) continue;           // can we add to cluster w/o tripping over threshold?
                
                for( auto ca : cluster_add ) {
                    cluster.insert(ca);
                    touched.insert(ca);
                    if( ca != n_ ) {
                        for( auto ca_child : ca->children ) if( !touched.count(ca_child) ) dfs.push_back(ca_child);
                    }
                }
                for( auto child : n_->children ) {
                    if( !touched.count(child) ) dfs.push_back(child);
                }
            }
        }
        
        OpNode *c = OpNode::cluster_delete_set(cluster);
        touched.insert(c);
        c->idx = l_nodes.size(); l_nodes.push_back(c);
        
        if( !root ) root = c;
    }
    dlog.end();
    
    for( OpNode *n : desc ) { n->parents.clear(); n->children.clear(); delete n; }
    
    dlog.fn_("Updating nodes",[&]{
        //OpNode::graph_to_list(root,l_nodes);
        int n_n = l_nodes.size();
#       pragma omp parallel for default(none) shared(l_nodes,n_n)
        for( int i_n = 0; i_n < n_n; i_n++ ) l_nodes[i_n]->update_to_sub();
        root->update_position([&](const vector<int> &l_if) { return _average_face_pos(l_if); });
        root->update_color();
    });
    
    dlog.print( "Clustered %d into %d nodes", desc.size(), l_nodes.size() );
    //dlog.print( "l_nodes[0]->parents.size() = %d, l_nodes[1]->children.size() = %d", l_nodes[0]->parents.size(), l_nodes[1]->children.size() );
    */
    
    dlog.end();
}

void EvoMesh::cluster_subgraphs( vector<OpNode*> &l_nodes, int level ) {
    class OpNodeSmallestIndex {
    public:
        bool operator() (const OpNode *l, const OpNode *r) { return l->idx > r->idx; }
    };
    
    for( OpNode *n : l_nodes ) delete n; l_nodes.clear();
    
    dlog.start_("Clustering subgraphs");
    dlog.print("level = %d", level);
    
    /*OpNode *root = OpNode::create_layer(n_root);
    unordered_set<OpNode*> desc; root->get_descendents(desc); desc.insert(root);
    root = nullptr;
    
    priority_queue<OpNode*,vector<OpNode*>,OpNodeSmallestIndex> q; for( OpNode *n : desc ) q.push(n);
    unordered_set<OpNode*> touched;
    
    dlog.start("Clustering");
    while(!q.empty()) {
        OpNode *node = q.top(); q.pop();
        if( touched.count(node) ) continue;
        
        float vi = node->stats.revg_visual_importance;
        touched.insert(node);
        
        // find all nodes to be clustered
        unordered_set<OpNode*> cluster; cluster.insert(node);
        
        if( node->idx == 0 ) {
            touched.insert(node);
        } else {
            vector<OpNode*> dfs(node->children.begin(),node->children.end());
            while( !dfs.empty() ) {
                OpNode *n_ = dfs.back(); dfs.pop_back();
                
                if( touched.count(n_) ) continue;           // already in cluster?
                
                unordered_set<OpNode*> cluster_add; cluster_add.insert(n_);
                unordered_set<OpNode*> check(n_->parents.begin(),n_->parents.end());
                while(!check.empty()) {
                    OpNode *p = pop(check);
                    if( touched.count(p) || cluster_add.count(p) ) continue;
                    for( auto p_ : p->parents ) check.insert(p_);
                    cluster_add.insert(p);
                }
                bool all_good = true;
                for( auto ca : cluster_add ) if( ca->stats.revg_visual_importance > threshold ) {all_good=false; break;}
                if( !all_good ) continue;           // can we add to cluster w/o tripping over threshold?
                
                for( auto ca : cluster_add ) {
                    cluster.insert(ca);
                    touched.insert(ca);
                    if( ca != n_ ) {
                        for( auto ca_child : ca->children ) if( !touched.count(ca_child) ) dfs.push_back(ca_child);
                    }
                }
                for( auto child : n_->children ) {
                    if( !touched.count(child) ) dfs.push_back(child);
                }
            }
        }
        
        OpNode *c = OpNode::cluster_delete_set(cluster);
        touched.insert(c);
        c->idx = l_nodes.size(); l_nodes.push_back(c);
        
        if( !root ) root = c;
    }
    dlog.end();
    
    for( OpNode *n : desc ) { n->parents.clear(); n->children.clear(); delete n; }
    
    dlog.fn_("Updating nodes",[&]{
        //OpNode::graph_to_list(root,l_nodes);
        int n_n = l_nodes.size();
#       pragma omp parallel for default(none) shared(l_nodes,n_n)
        for( int i_n = 0; i_n < n_n; i_n++ ) l_nodes[i_n]->update_to_sub();
        root->update_position([&](const vector<int> &l_if) { return _average_face_pos(l_if); });
        root->update_color();
    });
    
    dlog.print( "Clustered %d into %d nodes", desc.size(), l_nodes.size() );
    //dlog.print( "l_nodes[0]->parents.size() = %d, l_nodes[1]->children.size() = %d", l_nodes[0]->parents.size(), l_nodes[1]->children.size() );
    */
    
    dlog.end();
}

vec3f get_avg_camera( OpNode *node ) {
    vec3f d = zero3f;
    unordered_set<OpNode*> sub; node->get_sub_nodes(sub);
    for( OpNode *n : sub ) d += n->camera.frame.z;
    return normalize(d);
}
void EvoMesh::cluster_cams( vector<OpNode*> &l_nodes, float dot_threshold ) {
    dlog.start("Cluster: Camera Directions");
    dlog.print("Dot Threshold: %0.2f", dot_threshold);
    
    OpNode *root = l_nodes[0];
    
    unordered_set<OpNode*> crawl; crawl.insert(root);
    while( !crawl.empty() ) {
        OpNode *node = pop(crawl);
        vec3f dir = get_avg_camera(node);
        
        unordered_set<OpNode*> cluster; cluster.insert(node);
        unordered_set<OpNode*> pot = node->children;
        while(!pot.empty()) {
            OpNode *n = pop(pot);
            vec3f d = get_avg_camera(n);
            float dot = dir % d;
            if( dot < dot_threshold ) continue;
            if( !OpNode::is_cluster_valid(n,cluster) ) continue;
            cluster.insert(n);
            for( OpNode *c : n->children ) pot.insert(c);
        }
        
        if( cluster.size() > 1 ) {
            if( cluster.count(root) ) root = nullptr;
            for( OpNode *n : cluster ) crawl.erase(n);
            node = OpNode::cluster_delete_set(cluster);
            if( !root ) root = node;
        }
        
        for( OpNode *c : node->children ) crawl.insert(c);
    }
    
    _cluster_update(root, l_nodes);
    dlog.print("Node Count: %d",l_nodes.size() );
    
    dlog.end();
}


void EvoMesh::cluster_cam_locations( vector<OpNode*> &l_nodes, float threshold ) {
    dlog.start("Cluster: Camera Locations");
    dlog.print("Threshold: %0.2f", threshold);
    
    function<vec3f(OpNode*)> get_avg_cam_loc = [](OpNode*node)->vec3f {
        vec3f l = zero3f;
        unordered_set<OpNode*> sub; node->get_sub_nodes(sub);
        for( OpNode *n : sub ) l += n->camera.frame.o;
        l /= (float)sub.size();
        return l;
    };
    
    OpNode *root = l_nodes[0];
    
    unordered_set<OpNode*> crawl; crawl.insert(root);
    while( !crawl.empty() ) {
        OpNode *node = pop(crawl);
        vec3f loc = get_avg_cam_loc(node);
        vec3f avg = loc;
        
        unordered_set<OpNode*> cluster; cluster.insert(node);
        unordered_set<OpNode*> pot = node->children;
        while(!pot.empty()) {
            OpNode *n = pop(pot);
            vec3f n_loc = get_avg_cam_loc(n);
            
            float dist = length(n_loc-avg);
            if( dist < threshold ) continue;
            if( !OpNode::is_cluster_valid(n,cluster) ) continue;
            cluster.insert(n);
            loc += n_loc; avg = loc / (float)cluster.size();
            for( OpNode *c : n->children ) pot.insert(c);
        }
        
        if( cluster.size() > 1 ) {
            if( cluster.count(root) ) root = nullptr;
            for( OpNode *n : cluster ) crawl.erase(n);
            node = OpNode::cluster_delete_set(cluster);
            if( !root ) root = node;
        }
        
        for( OpNode *c : node->children ) crawl.insert(c);
    }
    
    _cluster_update(root, l_nodes);
    dlog.print("Node Count: %d",l_nodes.size() );
    
    dlog.end();
}



vec3f EvoMesh::_average_brush_pos( int i_op ) {
    vec3f p = zero3f;
    int count = 0;
    for( vec3f &v : nodes[i_op]->stroke.pos ) {
        p += v;
        count++;
    }
    
    if( count ) p *= (1.0f / (float)count);
    return p;
}

float EvoMesh::compute_hausdorff_dist( OpNode *n ) {
    TriangleMesh *tmesh0 = to_TriangleMesh(n->faces.li_del);
    TriangleMesh *tmesh1 = to_TriangleMesh(n->faces.li_add);
    Grid *grid0 = new Grid(tmesh0);
    Grid *grid1 = new Grid(tmesh1);
    
    float hd = 0.0f;
    for( vec3f &v : tmesh0->pos ) {
        vec3f v_ = grid1->get_nearest_surface_point(v);
        hd = max(hd, length(v-v_));
    }
    for( vec3f &v : tmesh1->pos ) {
        vec3f v_ = grid0->get_nearest_surface_point(v);
        hd = max(hd, length(v-v_));
    }
    
    delete grid1; delete grid0;
    delete tmesh0; delete tmesh1;
    
    return hd;
}

float EvoMesh::compute_node_volume_pre( OpNode *n ) {
    float vol = 0.0f;
    for( int i_f : n->faces.li_del ) {
        const vec3i &f = faces[i_f];
        vol += compute_signed_tetrahedron_volume(pos[f.x], pos[f.y], pos[f.z]);
    }
    return vol;
}
float EvoMesh::compute_node_volume_post( OpNode *n ) {
    float vol = 0.0f;
    for( int i_f : n->faces.li_add ) {
        const vec3i &f = faces[i_f];
        vol += compute_signed_tetrahedron_volume(pos[f.x], pos[f.y], pos[f.z]);
    }
    return vol;
}
float EvoMesh::compute_node_volume_delta( OpNode *n ) {
    return compute_node_volume_post(n) - compute_node_volume_pre(n);
}
float EvoMesh::compute_node_area_pre( OpNode *n ) {
    float a = 0.0f;
    for( int i_f : n->faces.li_del ) a += fareas[i_f];
    return a;
}
float EvoMesh::compute_node_area_post( OpNode *n ) {
    float a = 0.0f;
    for( int i_f : n->faces.li_add ) a += fareas[i_f];
    return a;
}
float EvoMesh::compute_node_area_delta( OpNode *n ) {
    return compute_node_area_post(n) - compute_node_area_pre(n);
}



void EvoMesh::_compute_brush_volume_deltas() {
    for( auto node : nodes )
        node->stats.vol_delta = compute_node_volume_delta(node);
}

float EvoMesh::_compute_face_area( int i_f ) { return fareas[i_f]; }

void EvoMesh::_compute_brush_areas_pre() {
    for( int i_op = 0; i_op < n_ops; i_op++ ) {
        float a = 0.0f;
        for( int i_f : nodes[i_op]->faces.li_del ) a += fareas[i_f];
        nodes[i_op]->stats.area_pre = a;
    }
}
void EvoMesh::_compute_brush_areas_post() {
    for( int i_op = 0; i_op < n_ops; i_op++ ) {
        float a = 0.0f;
        for( int i_f : nodes[i_op]->faces.li_add ) a += fareas[i_f];
        nodes[i_op]->stats.area_post = a;
    }
}

void EvoMesh::_compute_brush_avgnorms_pre() {
    for( int i_op = 0; i_op < n_ops; i_op++ ) {
        vec3f n_avg = zero3f;
        float a_tot = 0.0f;
        for( int i_f : nodes[i_op]->faces.li_del ) {
            float a = fareas[i_f];
            vec3f n = fnorms[i_f];
            n_avg += (n * a);
            a_tot += a;
        }
        nodes[i_op]->stats.avgnorm_pre = n_avg * (1.0f / a_tot); //normalize(n_avg);
    }
}
void EvoMesh::_compute_brush_avgnorms_post() {
    for( int i_op = 0; i_op < n_ops; i_op++ ) {
        vec3f n_avg = zero3f;
        float a_tot = 0.0f;
        for( int i_f : nodes[i_op]->faces.li_add ) {
            float a = fareas[i_f];
            vec3f n = fnorms[i_f];
            n_avg += (n * a);
            a_tot += a;
        }
        nodes[i_op]->stats.avgnorm_post = n_avg * (1.0f / a_tot); //normalize(n_avg);
    }
}

float EvoMesh::_compute_normdev( vector<int> &l_if ) {
    struct edge_info {
        int if0 = -1, if1 = -1;
        float l = -1;
    };
    
    struct edge_compare {
        bool operator() ( const vec2i &v0, const vec2i &v1 ) const {
            // HACK!!!!!!
            return (v0.x*1000000+v0.y) < (v1.x*1000000+v1.y);
        }
    };
    
    map<vec2i,edge_info,edge_compare> m_e_info;
    vec2i e, er;
    
    for( int i_f : l_if ) {
        vec3i &f = faces[i_f];
        
        e.x = f.x; e.y = f.y; er.x = e.y; er.y = e.x;
        if( m_e_info.count( er ) == 0 ) {
            edge_info ei;
            ei.if0 = i_f;
            ei.l = length( pos[er.x] - pos[er.y] );
            m_e_info[e] = ei;
        } else {
            m_e_info[er].if1 = i_f;
        }
        
        e.x = f.y; e.y = f.z; er.x = e.y; er.y = e.x;
        if( m_e_info.count( er ) == 0 ) {
            edge_info ei;
            ei.if0 = i_f;
            ei.l = length( pos[er.x] - pos[er.y] );
            m_e_info[e] = ei;
        } else {
            m_e_info[er].if1 = i_f;
        }
        
        e.x = f.z; e.y = f.x; er.x = e.y; er.y = e.x;
        if( m_e_info.count( er ) == 0 ) {
            edge_info ei;
            ei.if0 = i_f;
            ei.l = length( pos[er.x] - pos[er.y] );
            m_e_info[e] = ei;
        } else {
            m_e_info[er].if1 = i_f;
        }
    }
    
    float t = 0.0f;
    float tl = 0.0f;
    
    for( auto edgeiter : m_e_info ) {
        edge_info &ei = edgeiter.second;
        int if0 = ei.if0, if1 = ei.if1;
        
        if( if1 == -1 ) continue;
        
        vec3f fn0 = fnorms[if0];
        float fa0 = fareas[if0];
        vec3f fn1 = fnorms[if1];
        float fa1 = fareas[if1];
        t += (fn0%fn1) * (fa0+fa1);
        tl += fa0+fa1;
    }
    
    return t / tl;
}

void EvoMesh::_compute_brush_stddevnorms_pre() {
    for( int i_op = 0; i_op < n_ops; i_op++ ) {
        nodes[i_op]->stats.stddevnorm_pre = _compute_normdev( nodes[i_op]->faces.li_del );
    }
}
void EvoMesh::_compute_brush_stddevnorms_post() {
    for( int i_op = 0; i_op < n_ops; i_op++ ) {
        nodes[i_op]->stats.stddevnorm_post = _compute_normdev( nodes[i_op]->faces.li_add );
    }
}

void EvoMesh::_compute_brush_classifications() {
    for( int i_op = 0; i_op < n_ops; i_op++ ) {
        auto &stats = nodes[i_op]->stats;
        BrushClassification::Enum c = BrushClassification::None;
        
        float v = stats.vol_delta;
        float a = stats.area_post - stats.area_pre;
        float n = ( stats.stddevnorm_post - stats.stddevnorm_pre ) * 100.0f;
        float va = fabs(v), aa = fabs(a), na = fabs(n);
        
        if( na <= 0.1f && va > 0.1f && aa > 0.1f ) {
            c = BrushClassification::Move;
        } else if( v < -0.1f && a < -0.1f ) {
            c = BrushClassification::Vol_Del;
        } else if( v > 0.1f && a > 0.1f ) {
            c = BrushClassification::Vol_Add;
        } else if( a > 0.1f && va <= 0.1f ) {
            c = BrushClassification::Feat_Add;
        } else if( a < -0.1f && va <= 0.1f ) {
            c = BrushClassification::Feat_Del;
        } else if( n < -0.1f ) {
            c = BrushClassification::Crease;
        } else if( n > 0.1f ) {
            c = BrushClassification::Smooth;
        }
        
        stats.classification = c;
    }
}

void EvoMesh::_compute_revg_vi() {
    int w = 2;
    
    dlog.start("Computing RevG visual importance values for each node");
    dlog.print("w = %d", w);
    
    //dlog.progress_start();
#   pragma omp parallel for default(none) shared(w,dlog)
    for( int i_op = 0; i_op < n_ops; i_op++ ) {
        //dlog.progress_update((float)i_op/(float)(n_ops-1));
        auto n = nodes[i_op];
        
        // get neighborhood around node
        map<OpNode*,int> map_node_depth;
        unordered_set<OpNode*> neighborhood_children, neighborhood_parents;
        unordered_set<OpNode*> grow_children, grow_parents;
        grow_children.insert(n->children.begin(),n->children.end());
        grow_parents.insert(n->parents.begin(),n->parents.end());
        for( int d = 1; d < w+1; d++ ) {
            unordered_set<OpNode*> next_parents, next_children;
            for( OpNode *neighbor : grow_parents ) {
                if( neighborhood_parents.count(neighbor) ) continue;
                neighborhood_parents.insert(neighbor);
                map_node_depth[neighbor] = d;
                for( OpNode *p : neighbor->parents ) next_parents.insert(p);
            }
            for( OpNode *neighbor : grow_children ) {
                if( neighborhood_children.count(neighbor) ) continue;
                neighborhood_children.insert(neighbor);
                map_node_depth[neighbor] = d;
                for( OpNode *c : neighbor->children ) next_children.insert(c);
            }
            grow_parents.insert(next_parents.begin(),next_parents.end());
            grow_children.insert(next_children.begin(),next_children.end());
        }
        
        float vi = 0.0f;
        if(false) {
            for( OpNode *m : neighborhood_children ) {
                unordered_set<OpNode*> family;
                n->get_descendents_to(m,family);
                OpNode *cluster = OpNode::get_clustered(family);
                
                float hd = compute_hausdorff_dist(cluster);
                float vol_pre = compute_node_volume_pre(cluster);
                float vol_post = compute_node_volume_post(cluster);
                float vol_delta = vol_post - vol_pre;
                float area_pre = compute_node_area_pre(cluster);
                float area_post = compute_node_area_pre(cluster);
                float area_delta = area_post - area_pre;
                float I0 = fabs(vol_delta) / (fabs(area_pre)+0.0001f) / 10.0f;
                float I1 = fabs(area_delta) / (fabs(area_pre)+0.0001f) * 1000.0f;
                float I = hd / fabs(area_pre+0.0001f);
                
                delete cluster;
                
                float A = 1.0f;
                if( n->stroke.brush != m->stroke.brush ) {// || n->stroke.mod != m->stroke.mod ) {
                    A = powf(10.0f,(float)map_node_depth[m]);
                }
                
                vi += I * A;
            }
            for( OpNode *m : neighborhood_parents ) {
                unordered_set<OpNode*> family;
                m->get_descendents_to(n,family);
                OpNode *cluster = OpNode::get_clustered(family);
                
                float hd = compute_hausdorff_dist(cluster);
                float vol_pre = compute_node_volume_pre(cluster);
                float vol_post = compute_node_volume_post(cluster);
                float vol_delta = vol_post - vol_pre;
                float area_pre = compute_node_area_pre(cluster);
                float area_post = compute_node_area_pre(cluster);
                float area_delta = area_post - area_pre;
                float I0 = fabs(vol_delta) / (fabs(area_pre)+0.0001f) / 10.0f;
                float I1 = fabs(area_delta) / (fabs(area_pre)+0.0001f) * 1000.0f;
                float I = hd / fabs(area_pre+0.0001f);
                
                delete cluster;
                
                float A = 1.0f;
                if( n->stroke.brush != m->stroke.brush ) { // || n->stroke.mod != m->stroke.mod ) {
                    A = powf(10.0f,(float)map_node_depth[m]);
                }
                
                vi += I * A;
            }
        } else {
            vi += neighborhood_children.size() + neighborhood_parents.size();
        }
        
        n->stats.revg_visual_importance = vi / (float)w;
    }
    //dlog.progress_end();
    
    vector<float> l_vis; for( auto n : nodes ) l_vis.push_back(n->stats.revg_visual_importance);
    float max_vi = *max_element(l_vis.begin(),l_vis.end());
    //dlog.print("RevG Visual Importance: ", l_vis);
    dlog.print("Max = %f",max_vi);
    
    dlog.end();
}

vec3f EvoMesh::_average_face_pos( const vector<int> &l_if ) {
    vec3f p = zero3f;
    int count = 0;
    
    for( int i_f : l_if ) {
        vec3i &f=faces[i_f];
        vec3f &v0=pos[f.x], &v1=pos[f.y], &v2=pos[f.z];
        p += v0 + v1 + v2;
        count += 3;
    }
    
    if( count ) p *= (1.0f / (float)count);
    return p;
}
vec3f EvoMesh::_average_face_norm( const vector<int> &l_if ) {
    vec3f n = zero3f;
    for( int i_f : l_if ) n += fnorms[i_f];
    return normalize(n);
}


TriangleMesh *EvoMesh::to_TriangleMesh( vector<int> &l_iedges, vector<int> &l_ifaces ) {
    TriangleMesh *m = new TriangleMesh();
    
    unordered_map<int,int> mi_p; // translates EvoMesh vert inds to new TriangleMesh vert inds
    
    vector<vec3f> &pos_n = m->pos;
    vector<vec2i> &edges_n = m->edge;
    vector<vec3i> &faces_n = m->face;
    
    // collect only used verts, edges, and faces
    for( int i_e : l_iedges ) {
        vec2i &e = edges[i_e];
        if( mi_p.count( e.x ) == 0 ) { mi_p[e.x] = pos_n.size(); pos_n.push_back( pos[e.x] ); }
        if( mi_p.count( e.y ) == 0 ) { mi_p[e.y] = pos_n.size(); pos_n.push_back( pos[e.y] ); }
        edges_n.push_back( makevec2i( mi_p[e.x], mi_p[e.y] ) );
    }
    for( int i_f : l_ifaces ) {
        vec3i &f = faces[i_f];
        if( mi_p.count( f.x ) == 0 ) { mi_p[f.x] = pos_n.size(); pos_n.push_back( pos[f.x] ); }
        if( mi_p.count( f.y ) == 0 ) { mi_p[f.y] = pos_n.size(); pos_n.push_back( pos[f.y] ); }
        if( mi_p.count( f.z ) == 0 ) { mi_p[f.z] = pos_n.size(); pos_n.push_back( pos[f.z] ); }
        faces_n.push_back( makevec3i( mi_p[f.x], mi_p[f.y], mi_p[f.z] ) );
    }
    
    m->update_normals();
    return m;
}
TriangleMesh *EvoMesh::to_TriangleMesh( vector<int> &l_ifaces ) {
    TriangleMesh *m = new TriangleMesh();
    
    unordered_map<int,int> mi_p; // translates EvoMesh vert inds to new TriangleMesh vert inds
    
    vector<vec3f> &pos_n = m->pos;
    vector<vec3i> &faces_n = m->face;
    
    // collect only used verts and faces
    for( int i_f : l_ifaces ) {
        vec3i &f = faces[i_f];
        if( mi_p.count( f.x ) == 0 ) { mi_p[f.x] = pos_n.size(); pos_n.push_back( pos[f.x] ); }
        if( mi_p.count( f.y ) == 0 ) { mi_p[f.y] = pos_n.size(); pos_n.push_back( pos[f.y] ); }
        if( mi_p.count( f.z ) == 0 ) { mi_p[f.z] = pos_n.size(); pos_n.push_back( pos[f.z] ); }
        faces_n.push_back( makevec3i( mi_p[f.x], mi_p[f.y], mi_p[f.z] ) );
    }
    
    m->update_normals();
    return m;
}




void EvoMesh::_face_mapping_compute( const string &fn_rlvp, const string &fn_rlfv ) {
    dlog.fn_("Loading reprmesh",[&]{
        FILE *fp;
        fp = fopen(fn_rlvp.c_str(),"rb"); LoadBinaryFile(fp,ems_final->map_pos); fclose(fp);
        fp = fopen(fn_rlfv.c_str(),"rb"); LoadBinaryFile(fp,map_faces); fclose(fp);
    });
    
    int n_v = ems_final->map_pos.size();
    dlog.print("Verts: %d", n_v);
    
    dlog.fn_("Initial projection",[&]{
        vector<int> &l_if = ems_final->l_if;
        TriangleMesh *m_final = to_TriangleMesh(l_if);
        Grid *g_final = new Grid(m_final);
        for( int i_v = 0; i_v < n_v; i_v++ ) {
            vec3f &v = ems_final->map_pos[i_v];
            int i_f;
            v = g_final->get_nearest_surface_point( v, i_f );
            ems_final->map_if.push_back(l_if[i_f]);
        }
        delete g_final; delete m_final;
    });
    
    TriangleMesh *m_repr = new TriangleMesh();
    m_repr->face = map_faces;
    m_repr->pos = ems_final->map_pos;
    vector<int> map_iv_if = ems_final->map_if;
    
    
    dlog.start("Projecting representative mesh and strokes over %d ops", n_ops);
    dlog.progress_start();
    for( int i_n = n_ops-1; i_n >= 0; i_n-- ) {
        dlog.progress_update((float)(n_ops-1-i_n) / (float)(n_ops-1));
        
        auto node = nodes[i_n];
        auto &edits = node->reprchanges;
        auto &l_if_next = node->faces.li_del;
        auto &l_if_prev = node->faces.li_add;
        
        //if( l_if_next.size() == 0 ) continue;
        if( i_n == 0 ) continue;
        
        // subdivide and project strokes
        m_repr->update_normals();
        Grid *g_repr = new Grid(m_repr);
        int n_pos = node->stroke.pos.size();
        vec3f v_prev;
        for( int i_pos = 0; i_pos < n_pos; i_pos++ ) {
            vec3f &v = node->stroke.pos[i_pos];
            
            StrokeReprData rp;
            rp.brk = node->stroke.brk[i_pos] || (i_pos==0);
            
            vec3f v_proj = g_repr->get_nearest_surface_point( v, rp.i_f, rp.ba, rp.bb );
            vec3i &f = m_repr->face[rp.i_f];
            vec3f &v0 = m_repr->pos[f.x], &v1=m_repr->pos[f.y], &v2=m_repr->pos[f.z];
            rp.n = dot(m_repr->l_fn[rp.i_f],v-v_proj);
            
            float ba=rp.ba, bb=rp.bb, bc=1.0f-ba-bb;
            vec3f v_cur = (v0*ba) + (v1*bb) + (v2*bc);
            
            if( rp.brk ) v_prev = v_cur;
            
            node->stroke.rpos.push_back(rp);
            v_prev = v_cur;
        }
        delete g_repr;
        
        // find verts to warp
        unordered_set<int> s_if_del(l_if_prev.begin(),l_if_prev.end());
        unordered_set<int> s_iv_edit;
        for( int i_v = 0; i_v < n_v; i_v++ ) if( s_if_del.count(map_iv_if[i_v]) ) s_iv_edit.insert(i_v);
        int n_iv_edit = s_iv_edit.size();
        vector<int> l_iv_edit; l_iv_edit.assign(s_iv_edit.begin(),s_iv_edit.end());
        
        // warp
        TriangleMesh *m_next = to_TriangleMesh( l_if_next );
        Grid *g_next = new Grid(m_next);
        for( int i = 0; i < n_iv_edit; i++ ) {
            int i_v = l_iv_edit[i];
            
            int i_f_prev = map_iv_if[i_v];
            vec3f v_prev = m_repr->pos[i_v];
            
            int i_f;
            vec3f v_next = g_next->get_nearest_surface_point(v_prev,i_f);
            int i_f_next = l_if_next[i_f];
            
            edits.push_back( { i_v, i_f_next, i_f_prev, v_next, v_prev } );
            
            m_repr->pos[i_v] = v_next;
            map_iv_if[i_v] = i_f_next;
        }
        delete g_next; delete m_next;
        
    }
    dlog.progress_end();
    dlog.end();
    
    ems_init->map_pos = m_repr->pos;
    ems_init->map_if = map_iv_if;
    
    delete m_repr;
    
}


int EvoMesh::get_num_played( const OpNode *node, const EvoMeshState *ems ) const {
    if( node->nodes.size() == 0 ) return ems->s_in.count(node->idx);
    int c = 0;
    for( OpNode *snode : node->nodes ) c += get_num_played(snode,ems);
    return c;
}
bool EvoMesh::is_all_played( const OpNode *node, const EvoMeshState *ems ) const {
    if( node->nodes.size() == 0 ) return (ems->s_in.count(node->idx)!=0);
    for( OpNode *snode : node->nodes ) if( !is_all_played(snode,ems) ) return false;
    return true;
}
bool EvoMesh::is_all_unplayed( const OpNode *node, const EvoMeshState *ems ) const {
    if( node->nodes.size() == 0 ) return (ems->s_in.count(node->idx)==0);
    for( OpNode *snode : node->nodes ) if( !is_all_unplayed(snode,ems) ) return false;
    return true;
}


EvoMeshStateChange *EvoMesh::get_unplayed( OpNode *node, const EvoMeshState *ems ) {
    auto emsc = new EvoMeshStateChange(true);
    unordered_set<OpNode*> touched;
    _add_unplayed(node,ems,emsc,touched);
    _update_emsc(emsc);
    return emsc;
}
void EvoMesh::add_unplayed( OpNode *node, const EvoMeshState *ems, EvoMeshStateChange *emsc ) {
    if( !emsc->fwd ) return;
    unordered_set<OpNode*> touched;
    _add_unplayed(node,ems,emsc,touched);
    _update_emsc(emsc);
}
void EvoMesh::_add_unplayed( OpNode *node, const EvoMeshState *ems, EvoMeshStateChange *emsc, unordered_set<OpNode*> &touched ) {
    if( touched.count(node) ) return; touched.insert(node);
    if( is_all_played(node,ems) ) return;
    unordered_set<int> s_in; node->get_sub_indices(s_in);
    for( int i_n : s_in ) if( !ems->s_in.count(i_n) ) emsc->s_in.insert(i_n);
    for( auto p : node->parents ) _add_unplayed(p,ems,emsc,touched);
}

EvoMeshStateChange *EvoMesh::get_played( OpNode *node, const EvoMeshState *ems ) {
    auto emsc = new EvoMeshStateChange(false);
    unordered_set<OpNode*> touched;
    _add_played(node,ems,emsc,touched);
    _update_emsc(emsc);
    return emsc;
}
void EvoMesh::add_played( OpNode *node, const EvoMeshState *ems, EvoMeshStateChange *emsc ) {
    if( emsc->fwd ) return;
    unordered_set<OpNode*> touched;
    _add_played(node,ems,emsc,touched);
    _update_emsc(emsc);
}
void EvoMesh::_add_played( OpNode *node, const EvoMeshState *ems, EvoMeshStateChange *emsc, unordered_set<OpNode*> &touched ) {
    if( touched.count(node) ) return; touched.insert(node);
    if( is_all_unplayed(node,ems) ) return;
    unordered_set<int> s_in; node->get_sub_indices(s_in);
    for( int i_n : s_in ) if( ems->s_in.count(i_n) ) emsc->s_in.insert(i_n);
    for( auto c : node->children ) _add_played(c,ems,emsc,touched);
}


void EvoMesh::_update_emsc( EvoMeshStateChange *emsc ) {
    dlog.start("Updating EvoMeshStateChange");
    emsc->l_in.assign( emsc->s_in.begin(), emsc->s_in.end() );
    sort( emsc->l_in.begin(), emsc->l_in.end() );
    
    emsc->edges.li_add.clear(); emsc->edges.li_del.clear();
    emsc->faces.li_add.clear(); emsc->faces.li_del.clear();
    emsc->s_if_change.clear();
    int n = emsc->l_in.size();
    for( int i = 0; i < n; i++ ) {
        int i_n0 = emsc->l_in[i], i_n1 = emsc->l_in[n-1-i];
        sorted_del_add( emsc->edges.li_add, nodes[i_n0]->edges.li_del, nodes[i_n0]->edges.li_add );
        sorted_del_add( emsc->edges.li_del, nodes[i_n1]->edges.li_add, nodes[i_n1]->edges.li_del );
        sorted_del_add( emsc->faces.li_add, nodes[i_n0]->faces.li_del, nodes[i_n0]->faces.li_add );
        sorted_del_add( emsc->faces.li_del, nodes[i_n1]->faces.li_add, nodes[i_n1]->faces.li_del );
        emsc->s_if_change.insert( nodes[i_n0]->faces.li_del.begin(), nodes[i_n0]->faces.li_del.end() );
        emsc->s_if_change.insert( nodes[i_n0]->faces.li_add.begin(), nodes[i_n0]->faces.li_add.end() );
    }
    
    emsc->map_edits.clear();
    map<int,int> map_iv_ime;
    for( int i_n : emsc->l_in ) {
        for( ReprMoveVert &rmv : nodes[i_n]->reprchanges ) {
            if( map_iv_ime.count(rmv.i_v) == 0 ) {
                map_iv_ime[rmv.i_v] = emsc->map_edits.size();
                emsc->map_edits.push_back( (const ReprMoveVert)rmv );
            } else {
                int i_me = map_iv_ime[rmv.i_v];
                ReprMoveVert &rmv_ = emsc->map_edits[i_me];
                rmv_.i_f1 = rmv.i_f1;
                rmv_.v1 = rmv.v1;
            }
        }
    }
    dlog.end();
}


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



EvoMeshStateChange *EvoMesh::get_unplayed( int i_node, const EvoMeshState *ems_cur ) {
    EvoMeshStateChange *emsc = new EvoMeshStateChange(true);
    add_unplayed(i_node,ems_cur,emsc);
    return emsc;
}
EvoMeshStateChange *EvoMesh::get_unplayed( unordered_set<int> si_nodes, const EvoMeshState *ems_cur ) {
    EvoMeshStateChange *emsc = new EvoMeshStateChange(true);
    add_unplayed(si_nodes,ems_cur,emsc);
    return emsc;
}
EvoMeshStateChange *EvoMesh::get_played( int i_node, const EvoMeshState *ems_cur ) {
    EvoMeshStateChange *emsc = new EvoMeshStateChange(false);
    add_played(i_node,ems_cur,emsc);
    return emsc;
}
EvoMeshStateChange *EvoMesh::get_played( unordered_set<int> si_nodes, const EvoMeshState *ems_cur ) {
    EvoMeshStateChange *emsc = new EvoMeshStateChange(false);
    add_played(si_nodes,ems_cur,emsc);
    return emsc;
}
void EvoMesh::add_unplayed( int i_node, const EvoMeshState *ems_cur, EvoMeshStateChange *emsc ) {
    if( !emsc->fwd ) return;
    _add_unplayed(i_node,ems_cur,emsc);
    _update_emsc(emsc);
}
void EvoMesh::add_unplayed( unordered_set<int> si_nodes, const EvoMeshState *ems_cur, EvoMeshStateChange *emsc ) {
    if( !emsc->fwd ) return;
    for( int i_node : si_nodes ) _add_unplayed( i_node, ems_cur, emsc );
    _update_emsc(emsc);
}
void EvoMesh::_add_unplayed( int i_node, const EvoMeshState *ems_cur, EvoMeshStateChange *emsc ) {
    if( emsc->s_in.count(i_node) || ems_cur->s_in.count(i_node) ) return;
    emsc->s_in.insert(i_node);
    for( auto parent : nodes[i_node]->parents ) _add_unplayed(parent->idx,ems_cur,emsc);
}
void EvoMesh::add_played( int i_node, const EvoMeshState *ems_cur, EvoMeshStateChange *emsc ) {
    if( emsc->fwd ) return;
    _add_played(i_node,ems_cur,emsc);
    _update_emsc(emsc);
}
void EvoMesh::add_played( unordered_set<int> si_nodes, const EvoMeshState *ems_cur, EvoMeshStateChange *emsc ) {
    if( !emsc->fwd ) return;
    for( int i_node : si_nodes ) _add_played( i_node, ems_cur, emsc );
    _update_emsc(emsc);
}
void EvoMesh::_add_played( int i_node, const EvoMeshState *ems_cur, EvoMeshStateChange *emsc ) {
    if( emsc->s_in.count(i_node) || !ems_cur->s_in.count(i_node) ) return;
    emsc->s_in.insert(i_node);
    for( auto child : nodes[i_node]->children ) _add_played(child->idx,ems_cur,emsc);
}









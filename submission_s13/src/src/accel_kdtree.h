#ifndef _KDTREE_H_
#define _KDTREE_H_

#include "std.h"
#include "vec.h"

using namespace std;


class KDBranch {
    
public:
    
    KDBranch( vec3f &p, int i, int d );
    ~KDBranch();
    
    void insert( vec3f &p, int i );
    
    void knn( const vec3f &p, int k, vector<int> &l_i, vector<float> &l_dist2, float &max2 ) const;
    
    int rball( const vec3f &p, float rad2, vector<int> &l_i, vector<float> &l_dist2 ) const;
    int rball( const vec3f &p, float rad2 ) const;
    
    int i;
    vec3f p;
    int d;
    
    KDBranch *l = nullptr;
    KDBranch *r = nullptr;
    
};


class KDTree {

public:
    
    KDTree();
    ~KDTree();
    
    //inline void insert( float *p, int i ) {
        //vec3f v = {p[0],p[1],p[2]};
        //insert(v,i);
    //}
    void insert( vec3f &p, int i );
    void insert_shuffle( vector<vec3f> &l_p );
    
    int knn( const vec3f &p, int k, vector<int> &l_i, vector<float> &l_dist2 ) const;
    inline int knn( const vec3f &p, int k, vector<int> &l_i ) const {
        vector<float> l_dist2(k,0.0f); return knn( p, k, l_i, l_dist2 );
    }
    
    int rball( const vec3f &p, float rad, vector<int> &l_i, vector<float> &l_dist2 ) const;
    int rball( const vec3f &p, float rad ) const;
    
private:
    
    KDBranch *root = nullptr;
    
};


#endif


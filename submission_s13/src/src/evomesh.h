#ifndef _EVOMESH_H_
#define _EVOMESH_H_

#include "debug.h"
#include "depgraph.h"
#include "shape.h"
#include "vec.h"
#include "object.h"
#include "std.h"
#include "geom.h"
#include "gls.h"
#include "accel_grid.h"
#include "ext/common/frame.h"

#include <set>
#include <map>

class EvoMeshStateChange {
public:
    bool fwd;
    vector<int> l_in; unordered_set<int> s_in;
    AddDelData edges;
    AddDelData faces; unordered_set<int> s_if_change;
    vector<ReprMoveVert> map_edits;
    
    EvoMeshStateChange( bool fwd ) : fwd(fwd) { }
};

class EvoMeshState {
public:
    vector<int> l_in; unordered_set<int> s_in;
    vector<int> l_ie, l_if;
    vector<vec3f> map_pos;
    vector<int> map_if;
    
    EvoMeshState() { }
    EvoMeshState( const EvoMeshState *ems_clone ) { copy_from(ems_clone); }
    
    EvoMeshState *clone() { return new EvoMeshState(this); }
    
    void copy_from( const EvoMeshState *ems ) {
        l_in = ems->l_in; s_in = ems->s_in;
        l_ie = ems->l_ie; l_if = ems->l_if;
        map_pos = ems->map_pos;
        map_if = ems->map_if;
    }
    
    void apply( EvoMeshStateChange *chg ) {
        vector<int> blank;
        if( chg->fwd ) {
            sorted_del_add( l_ie, chg->edges.li_del, chg->edges.li_add );
            sorted_del_add( l_if, chg->faces.li_del, chg->faces.li_add );
            sorted_del_add( l_in, blank, chg->l_in );
            s_in.insert( chg->s_in.begin(), chg->s_in.end() );
            for( ReprMoveVert &rmv : chg->map_edits ) {
                map_pos[rmv.i_v] = rmv.v1;
                map_if[rmv.i_v] = rmv.i_f1;
            }
        } else {
            sorted_del_add( l_ie, chg->edges.li_add, chg->edges.li_del );
            sorted_del_add( l_if, chg->faces.li_add, chg->faces.li_del );
            sorted_del_add( l_in, chg->l_in, blank );
            for( int i_n : chg->s_in ) s_in.erase(i_n);
            for( ReprMoveVert &rmv : chg->map_edits ) {
                map_pos[rmv.i_v] = rmv.v0;
                map_if[rmv.i_v] = rmv.i_f0;
            }
        }
    }
};


class EvoMesh {
public:
    int n_p, n_e, n_f, n_ops;
    vector<vec3f> pos;
    vector<vec2i> edges;
    vector<vec3i> faces;
    vector<vec3f> fnorms;
    vector<float> fareas;
    vector<vec3i> map_faces;        // reprmesh
    
    vector<OpNode*> nodes;
    
    vector<StartEndData> etimes;
    vector<StartEndData> ftimes;
    
    EvoMeshState *ems_init = nullptr;
    EvoMeshState *ems_final = nullptr;
    
    
    EvoMesh( const char *fn_json );
    
    void _depgraph_load( const char *fn_cache );
    void _depgraph_save( const char *fn_cache );
    void _depgraph_create( JsonNavigator nav );
    
    void cluster_reset( vector<OpNode*> &l_nodes );
    void cluster_runs( vector<OpNode*> &l_nodes );
    void cluster_same_brush( vector<OpNode*> &l_nodes );
    void cluster_similar_brush( vector<OpNode*> &l_nodes );
    void cluster_revg(  vector<OpNode*> &l_nodes, float threshold );
    void cluster_revg_vi( vector<OpNode*> &l_nodes, float threshold );
    void cluster_subgraphs( vector<OpNode*> &l_nodes, int level );
    void cluster_zip( vector<OpNode*> &l_nodes );
    void cluster_cams( vector<OpNode*> &l_nodes, float dot_threshold );
    void cluster_cam_locations( vector<OpNode*> &l_nodes, float threshold );
    
    void _cluster_update(OpNode *root,vector<OpNode*> &l_nodes);
    void _print_inds(const char *beg, unordered_set<OpNode*> l_nodes) {
        set<int> l_inds; for( auto *n : l_nodes ) l_inds.insert(n->idx);
        dlog.print(beg,l_inds);
    };
    
    bool _is_dep_on( int i_op0, int i_op1 ) {
        return nodes[i_op1]->is_dep_on(nodes[i_op0]);
    }
    
    float compute_hausdorff_dist( OpNode *n );
    float compute_node_volume_pre( OpNode *n );
    float compute_node_volume_post( OpNode *n );
    float compute_node_volume_delta( OpNode *n );
    float compute_node_area_pre( OpNode *n );
    float compute_node_area_post( OpNode *n );
    float compute_node_area_delta( OpNode *n );
    
    vec3f _average_brush_pos( int i_op );
    void _compute_brush_volume_deltas();
    void _compute_brush_areas_pre();
    void _compute_brush_areas_post();
    void _compute_brush_avgnorms_pre();
    void _compute_brush_avgnorms_post();
    float _compute_normdev( vector<int> &l_if );
    void _compute_brush_stddevnorms_pre();
    void _compute_brush_stddevnorms_post();
    void _compute_brush_classifications();
    void _compute_revg_vi();
    
    float _compute_face_area( const vec3i &f );
    float _compute_face_area( int i_f );
    
    vec3f _average_face_pos( const vector<int> &l_if );
    vec3f _average_face_norm( const vector<int> &l_if );
    
    void _face_mapping_compute( const string &fn_rlvp, const string &fn_rlfv );
    
    void _reorder( vector<int> l_inds );
    
    
    TriangleMesh *to_TriangleMesh( EvoMeshState *ems, bool show_edges=true ) {
        if( !show_edges ) return to_TriangleMesh(ems->l_if);
        return to_TriangleMesh(ems->l_ie,ems->l_if);
    }
    TriangleMesh *to_TriangleMesh( EvoMeshStateChange *emsc, bool same_dir=true, bool show_edges=true ) {
        if( same_dir == emsc->fwd ) {
            if( !show_edges ) return to_TriangleMesh(emsc->faces.li_add);
            return to_TriangleMesh(emsc->edges.li_add,emsc->faces.li_add);
        } else {
            if( !show_edges ) return to_TriangleMesh(emsc->faces.li_del);
            return to_TriangleMesh(emsc->edges.li_del,emsc->faces.li_del);
        }
    }
    TriangleMesh *to_TriangleMesh( vector<int> &l_iedges, vector<int> &l_ifaces );
    TriangleMesh *to_TriangleMesh( vector<int> &l_ifaces );
    
    
    int get_num_played( const OpNode *node, const EvoMeshState *ems ) const;
    bool is_all_played( const OpNode *node, const EvoMeshState *ems ) const;
    bool is_all_unplayed( const OpNode *node, const EvoMeshState *ems ) const;
    
    
    EvoMeshStateChange *get_unplayed( OpNode *node, const EvoMeshState *ems );
    void add_unplayed( OpNode *node, const EvoMeshState *ems, EvoMeshStateChange *emsc );
    void _add_unplayed( OpNode *node, const EvoMeshState *ems, EvoMeshStateChange *emsc, unordered_set<OpNode*> &touched );
    
    EvoMeshStateChange *get_played( OpNode *node, const EvoMeshState *ems );
    void add_played( OpNode *node, const EvoMeshState *ems, EvoMeshStateChange *emsc );
    void _add_played( OpNode *node, const EvoMeshState *ems, EvoMeshStateChange *emsc, unordered_set<OpNode*> &touched );
    
    void _update_emsc( EvoMeshStateChange *emsc );
    
    
    EvoMeshStateChange *get_unplayed( int i_node, const EvoMeshState *ems_cur );
    EvoMeshStateChange *get_unplayed( unordered_set<int> si_nodes, const EvoMeshState *ems_cur );
    EvoMeshStateChange *get_played( int i_node, const EvoMeshState *ems_cur );
    EvoMeshStateChange *get_played( unordered_set<int> si_nodes, const EvoMeshState *ems_cur );
    void add_unplayed( int i_node, const EvoMeshState *ems_cur, EvoMeshStateChange *emsc );
    void add_unplayed( unordered_set<int> si_nodes, const EvoMeshState *ems_cur, EvoMeshStateChange *emsc );
    void _add_unplayed( int i_node, const EvoMeshState *ems_cur, EvoMeshStateChange *emsc );
    void add_played( int i_node, const EvoMeshState *ems_cur, EvoMeshStateChange *emsc );
    void add_played( unordered_set<int> si_nodes, const EvoMeshState *ems_cur, EvoMeshStateChange *emsc );
    void _add_played( int i_node, const EvoMeshState *ems_cur, EvoMeshStateChange *emsc );
    
    
};


#endif

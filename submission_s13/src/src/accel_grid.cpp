#include "accel_grid.h"
#include "geom.h"
#include "ext/common/range.h"
#include "ext/common/vec.h"
#include "mesh_utils.h"
#include "shape.h"

#include <queue>

Grid::Grid( vector<vec3f> &pos, vector<vec3i> &faces, int n_cells_per_dim ) :
_tmesh(nullptr),
pos(pos),
faces(faces),
fnorms(fnorms_blank)
{
    if( n_cells_per_dim == -1 ) {
        int n = faces.size() / 500;
        n_cells_per_dim = max( 10, min( 50, n ) );
    }
    
    _n_cells_per_dim = n_cells_per_dim;
    _bbuvw = { { 0,0,0 }, {n_cells_per_dim-1,n_cells_per_dim-1,n_cells_per_dim-1} };
    _n_cells = makevec3f(n_cells_per_dim,n_cells_per_dim,n_cells_per_dim);
    int n_v = pos.size();
    int n_f = faces.size();
    
    if( n_v ) {
        vec3f bbm = pos[0];
        vec3f bbM = pos[0];
        for( auto v : pos ) { bbm = min_component(bbm,v); bbM = max_component(bbM,v); }
        _bb = makerange3f( bbm-makevec3f(0.001,0.001,0.001), bbM+makevec3f(0.001,0.001,0.001) );
    } else {
        _bb = makerange3f( makevec3f(-0.0001,-0.0001,-0.0001), makevec3f(0.0001,0.0001,0.0001) );
    }
    _sz_bb = _bb.max - _bb.min;
    _sz_cell = _sz_bb / (float)n_cells_per_dim;
    
    _grid.resize( n_cells_per_dim*n_cells_per_dim*n_cells_per_dim );
    _grid_iv.resize( n_cells_per_dim*n_cells_per_dim*n_cells_per_dim );
    
    
    vector<bool> b_added; b_added.resize(n_v,false);
    vector<bool> face_skip; for( vec3i &f : faces ) {
        const vec3f &v0=pos[f.x], &v1=pos[f.y], &v2=pos[f.z];
        face_skip.push_back( compute_triangle_area(v0, v1, v2) < 0.00001f );
    }
    
    //for( int i_v = 0; i_v < n_v; i_v++ ) {
    //    _grid_iv[get_ind(tmesh->pos[i_v])].push_back(i_v);
    //}
    
    for( int i_f = 0; i_f < n_f; i_f++ ) {
        if( face_skip[i_f] ) continue;
        vec3i &liv = faces[i_f];
        assert( liv.x >= 0 && liv.y >= 0 && liv.z >= 0 );
        vec3f &v0=pos[liv.x], &v1=pos[liv.y], &v2=pos[liv.z];
        vec3i u0 = get_uvw(v0);
        vec3i u1 = get_uvw(v1);
        vec3i u2 = get_uvw(v2);
        
        if( !b_added[liv.x] ) {
            b_added[liv.x] = true;
            int ind = get_ind(u0);
            _grid_iv[ind].push_back(liv.x);
        }
        if( !b_added[liv.y] ) {
            b_added[liv.y] = true;
            int ind = get_ind(u1);
            _grid_iv[ind].push_back(liv.y);
        }
        if( !b_added[liv.z] ) {
            b_added[liv.z] = true;
            int ind = get_ind(u2);
            _grid_iv[ind].push_back(liv.z);
        }
    }
    
//#   pragma omp parallel for default(none) shared(n_f,faces,pos,face_skip)
    for( int i_f = 0; i_f < n_f; i_f++ ) {
        if( face_skip[i_f] ) continue;
        vec3i &liv = faces[i_f];
        //assert( liv.x >= 0 && liv.y >= 0 && liv.z >= 0 );
        vec3f &v0=pos[liv.x], &v1=pos[liv.y], &v2=pos[liv.z];
        vec3i u0 = get_uvw(v0);
        vec3i u1 = get_uvw(v1);
        vec3i u2 = get_uvw(v2);
        
        if( true ) {
            unordered_set<int> s_inds; _get_inds(v0,v1,v2,s_inds);
//#           pragma omp critical
//            {
                for( int ind : s_inds ) _grid[ind].push_back(i_f);
//            }
        } else {
            range3i bb = makerange3i(min_component(min_component(u0,u1),u2), max_component(max_component(u0,u1),u2));
            for( int u = bb.min.x; u <= bb.max.x; u++ )
                for( int v = bb.min.y; v <= bb.max.y; v++ )
                    for( int w = bb.min.z; w <= bb.max.z; w++ ) {
//#                       pragma omp critical
                        _grid[get_ind(u,v,w)].push_back(i_f);
                    }
        }
    }
    
    b_added.clear();}

Grid::Grid( TriangleMesh *tmesh, int n_cells_per_dim ) :
_tmesh(tmesh),
pos(tmesh->pos),
faces(tmesh->face),
fnorms(tmesh->l_fn)
{
    if( n_cells_per_dim == -1 ) {
        int n = tmesh->face.size() / 500;
        n_cells_per_dim = max( 10, min( 50, n ) );
    }
    
    _tmesh = tmesh;
    _n_cells_per_dim = n_cells_per_dim;
    _bbuvw = { { 0,0,0 }, {n_cells_per_dim-1,n_cells_per_dim-1,n_cells_per_dim-1} };
    _n_cells = makevec3f(n_cells_per_dim,n_cells_per_dim,n_cells_per_dim);
    int n_v = tmesh->pos.size();
    int n_f = tmesh->face.size();
    
    if( n_v ) {
        vec3f bbm = tmesh->pos[0];
        vec3f bbM = tmesh->pos[0];
        for( auto v : tmesh->pos ) { bbm = min_component(bbm,v); bbM = max_component(bbM,v); }
        _bb = makerange3f( bbm-makevec3f(0.001,0.001,0.001), bbM+makevec3f(0.001,0.001,0.001) );
    } else {
        _bb = makerange3f( makevec3f(-0.0001,-0.0001,-0.0001), makevec3f(0.0001,0.0001,0.0001) );
    }
    _sz_bb = _bb.max - _bb.min;
    _sz_cell = _sz_bb / (float)n_cells_per_dim;
    
    _grid.resize( n_cells_per_dim*n_cells_per_dim*n_cells_per_dim );
    _grid_iv.resize( n_cells_per_dim*n_cells_per_dim*n_cells_per_dim );
    
    
    vector<bool> b_added; b_added.resize(n_v,false);
    vector<bool> face_skip; for( vec3i &f : tmesh->face ) {
        const vec3f &v0=tmesh->pos[f.x], &v1=tmesh->pos[f.y], &v2=tmesh->pos[f.z];
        face_skip.push_back( compute_triangle_area(v0, v1, v2) < 0.00001f );
    }
    
    //for( int i_v = 0; i_v < n_v; i_v++ ) {
    //    _grid_iv[get_ind(tmesh->pos[i_v])].push_back(i_v);
    //}
    
    for( int i_f = 0; i_f < n_f; i_f++ ) {
        if( face_skip[i_f] ) continue;
        vec3i &liv = tmesh->face[i_f];
        assert( liv.x >= 0 && liv.y >= 0 && liv.z >= 0 );
        vec3f &v0=tmesh->pos[liv.x], &v1=tmesh->pos[liv.y], &v2=tmesh->pos[liv.z];
        vec3i u0 = get_uvw(v0);
        vec3i u1 = get_uvw(v1);
        vec3i u2 = get_uvw(v2);
        
        if( !b_added[liv.x] ) {
            b_added[liv.x] = true;
            int ind = get_ind(u0);
            _grid_iv[ind].push_back(liv.x);
        }
        if( !b_added[liv.y] ) {
            b_added[liv.y] = true;
            int ind = get_ind(u1);
            _grid_iv[ind].push_back(liv.y);
        }
        if( !b_added[liv.z] ) {
            b_added[liv.z] = true;
            int ind = get_ind(u2);
            _grid_iv[ind].push_back(liv.z);
        }
    }
    
    //#pragma omp parallel for default(none) shared(n_f,face_skip)
    for( int i_f = 0; i_f < n_f; i_f++ ) {
        if( face_skip[i_f] ) continue;
        vec3i &liv = faces[i_f];
        //assert( liv.x >= 0 && liv.y >= 0 && liv.z >= 0 );
        vec3f &v0=pos[liv.x], &v1=pos[liv.y], &v2=pos[liv.z];
        vec3i u0 = get_uvw(v0);
        vec3i u1 = get_uvw(v1);
        vec3i u2 = get_uvw(v2);
        
        if( true ) {
            unordered_set<int> s_inds; _get_inds(v0,v1,v2,s_inds);
            //#pragma omp critical
            //{
                for( int ind : s_inds ) _grid[ind].push_back(i_f);
            //}
        } else {
            range3i bb = makerange3i(min_component(min_component(u0,u1),u2), max_component(max_component(u0,u1),u2));
            for( int u = bb.min.x; u <= bb.max.x; u++ )
                for( int v = bb.min.y; v <= bb.max.y; v++ )
                    for( int w = bb.min.z; w <= bb.max.z; w++ ) {
//                        #pragma omp critical
                        _grid[get_ind(u,v,w)].push_back(i_f);
                    }
        }
    }
    
    b_added.clear();
}


void Grid::_get_inds(const vec3f &v0, const vec3f &v1, const vec3f &v2, unordered_set<int> &s_inds ) {
    _get_inds(v0,get_uvw(v0),v1,get_uvw(v1),v2,get_uvw(v2),s_inds);
}
void Grid::_get_inds( const vec3f &v0, const vec3i &u0, const vec3f &v1, const vec3i &u1, const vec3f &v2, const vec3i &u2, unordered_set<int> &s_inds ) {
    int d01 = max(max(abs(u0.x-u1.x),abs(u0.y-u1.y)),abs(u0.z-u1.z));
    int d12 = max(max(abs(u1.x-u2.x),abs(u1.y-u2.y)),abs(u1.z-u2.z));
    int d20 = max(max(abs(u2.x-u0.x),abs(u2.y-u0.y)),abs(u2.z-u0.z));
    
    if( d01 == 0 && d12 == 0 && d20 == 0 ) {
        s_inds.insert(get_ind(u0));
    } else if( d01 <= 1 && d12 <= 1 && d20 <= 1 ) {
        int um = min(min(u0.x,u1.x),u2.x), uM = max(max(u0.x,u1.x),u2.x);
        int vm = min(min(u0.y,u1.y),u2.y), vM = max(max(u0.y,u1.y),u2.y);
        int wm = min(min(u0.z,u1.z),u2.z), wM = max(max(u0.z,u1.z),u2.z);
        for( int u=um; u<=uM; u++ )
            for( int v=vm; v<=vM; v++ )
                for( int w=wm; w<=wM; w++ )
                    s_inds.insert(get_ind(u,v,w));
    } else {
        vec3f v01=(v0+v1)/2.0f, v12=(v1+v2)/2.0f, v20=(v0+v2)/2.0f;
        vec3i u01=get_uvw(v01),u12=get_uvw(v12),u20=get_uvw(v20);
        _get_inds(v0,u0,v01,u01,v20,u20,s_inds);
        _get_inds(v1,u1,v12,u12,v01,u01,s_inds);
        _get_inds(v2,u2,v20,u20,v12,u12,s_inds);
        _get_inds(v01,u01,v12,u12,v20,u20,s_inds);
    }
}

Grid::~Grid() { _grid.clear(); _grid_iv.clear(); }

inline vec3i Grid::get_uvw( const vec3f &xyz ) const { return floor( ( xyz - _bb.min ) / _sz_bb * _n_cells ); }
inline vec3f Grid::get_rel( const vec3f &xyz ) const { vec3f r = (xyz - _bb.min ) / _sz_bb; return add(floor(r),-r); }
inline vec3f Grid::get_xyz( const vec3i &uvw, const vec3f &rel ) const { return _sz_cell * add( uvw, rel ) + _bb.min; }
inline int Grid::get_ind( const vec3i &uvw ) const {
    assert( uvw.x >= 0 && uvw.x < _n_cells_per_dim );
    assert( uvw.y >= 0 && uvw.y < _n_cells_per_dim );
    assert( uvw.z >= 0 && uvw.z < _n_cells_per_dim );
    return (uvw.x*_n_cells_per_dim + uvw.y)*_n_cells_per_dim + uvw.z;
}
inline int Grid::get_ind( int u, int v, int w ) const {
    assert( u >= 0 && u < _n_cells_per_dim );
    assert( v >= 0 && v < _n_cells_per_dim );
    assert( w >= 0 && w < _n_cells_per_dim );
    return (u*_n_cells_per_dim + v)*_n_cells_per_dim + w;
}
inline int Grid::get_ind( const vec3f &xyz ) const {
    vec3i uvw = get_uvw(xyz);
    assert( uvw.x >= 0 && uvw.x < _n_cells_per_dim );
    assert( uvw.y >= 0 && uvw.y < _n_cells_per_dim );
    assert( uvw.z >= 0 && uvw.z < _n_cells_per_dim );
    return get_ind(uvw);
}
inline vec3i Grid::get_uvw( int ind ) const {
    int w = ind % _n_cells_per_dim;
    ind = (ind - w) / _n_cells_per_dim;
    int v = ind % _n_cells_per_dim;
    ind = (ind - v) / _n_cells_per_dim;
    int u = ind % _n_cells_per_dim;
    return makevec3i(u,v,w);
}

void Grid::iterate( range3i bb, fn_iter fn ) const {
    vec3i uvw;
    for( uvw.x = bb.min.x; uvw.x <= bb.max.x; uvw.x++ )
        for( uvw.y = bb.min.y; uvw.y <= bb.max.y; uvw.y++ )
            for( uvw.z = bb.min.z; uvw.z <= bb.max.z; uvw.z++ )
                fn( uvw );
}


bool Grid::has_moved( const vec3f &xyz, const float d2 ) const {
    //if( xyz.x < _bb.min.x || xyz.x > _bb.max.x ) return true;
    //if( xyz.y < _bb.min.y || xyz.y > _bb.max.y ) return true;
    //if( xyz.z < _bb.min.z || xyz.z > _bb.max.z ) return true;
    vec3i uvw = get_uvw(xyz);
    if( uvw.x < 0 || uvw.x >= _n_cells_per_dim ) return true;
    if( uvw.y < 0 || uvw.y >= _n_cells_per_dim ) return true;
    if( uvw.z < 0 || uvw.z >= _n_cells_per_dim ) return true;
    int ind = get_ind(uvw);
    for( int i_v : _grid_iv[ind] ) {
        vec3f p = pos[i_v];
        if( lengthSqr(xyz-p) <= d2 ) return false;
    }
    for( int i_f : _grid[ind] ) {
        vec3i &liv = faces[i_f];
        vec3f &v0 = pos[liv.x];
        vec3f &v1 = pos[liv.y];
        vec3f &v2 = pos[liv.z];
        vec3f p = closest_point_point_triangle(xyz,v0,v1,v2);
        if( lengthSqr(xyz-p) <= d2 ) return false;
    }
    return true;
}

#define add(xt,yt,zt,m,l)      {             \
    int x = (xt), y = (yt), z = (zt);        \
    if( x >= 0 && x < m &&                   \
        y >= 0 && y < m &&                   \
        z >= 0 && z < m )                    \
        l.push_back( makevec3i( x, y, z ) ); \
    }
void surround( const vec3i &uvw, int r, int m, vector<vec3i> &l_uvws ) {
    for( int i0 = -r; i0 <= r; i0++ ) {
        for( int i1 = -r; i1 <= r; i1++ ) {
            add( uvw.x+i0, uvw.y+i1, uvw.z-r, m, l_uvws );
            add( uvw.x+i0, uvw.y+i1, uvw.z+r, m, l_uvws );
            if( i1 > -r && i1 < r ) {
                add( uvw.x+i0, uvw.y-r, uvw.z+i1, m, l_uvws );
                add( uvw.x+i0, uvw.y+r, uvw.z+i1, m, l_uvws );
                if( i0 > -r && i0 < r ) {
                    add( uvw.x-r, uvw.y+i0, uvw.z+i1, m, l_uvws );
                    add( uvw.x+r, uvw.y+i0, uvw.z+i1, m, l_uvws );
                }
            }
        }
    }
}

struct cellinfo {
    int i; vec3i uvw; float d2;
    bool operator() (const cellinfo &lhs, const cellinfo &rhs) const { return lhs.d2 > rhs.d2; }
};

inline void insertcellinfo( const vec3f &xyz_from, const vec3i &uvw_from, const vec3i &uvw_to, const int M, const range3f &bb, const vec3f &cellsz, priority_queue<cellinfo,vector<cellinfo>,cellinfo> &next, unordered_set<int> &touched, float d2_min ) {
    if( uvw_to.x < 0 || uvw_to.x >= M || uvw_to.y < 0 || uvw_to.y >= M || uvw_to.z < 0 || uvw_to.z >= M ) return;
    
    int i = (uvw_to.x*M + uvw_to.y)*M + uvw_to.z;
    if( touched.count(i) ) return;
    touched.insert( i );
    
    vec3f closest = xyz_from;
    if( uvw_from.x < uvw_to.x ) {
        closest.x = bb.min.x + cellsz.x * (float)uvw_to.x;
    } else if( uvw_from.x > uvw_to.x ) {
        closest.x = bb.min.x + cellsz.x * (float)(uvw_to.x+1);
    }
    
    if( uvw_from.y < uvw_to.y ) {
        closest.y = bb.min.y + cellsz.y * (float)uvw_to.y;
    } else if( uvw_from.y > uvw_to.y ) {
        closest.y = bb.min.y + cellsz.y * (float)(uvw_to.y+1);
    }
    
    if( uvw_from.z < uvw_to.z ) {
        closest.z = bb.min.z + cellsz.z * (float)uvw_to.z;
    } else if( uvw_from.z > uvw_to.z ) {
        closest.z = bb.min.z + cellsz.z * (float)(uvw_to.z+1);
    }
    
    float d2 = lengthSqr( xyz_from - closest );
    if( d2 < d2_min + 0.01 ) next.push( { i, uvw_to, d2 } );
}

vec3f Grid::get_nearest_surface_point( const vec3f &xyz ) const {
    int i_f_closest; return get_nearest_surface_point( xyz, i_f_closest );
}
vec3f Grid::get_nearest_surface_point( const vec3f &xyz, int &i_f_closest ) const {
    vec3i uvw = get_uvw( xyz );
    vec3i uvw0 = max_component( _bbuvw.min, min_component( _bbuvw.max, uvw ) );
    
    float d2_min = numeric_limits<float>::max();
    i_f_closest = -1;
    vec3f p_closest = makevec3f(d2_min,d2_min,d2_min);
    
    if( faces.size() == 0 ) return p_closest;
    
    priority_queue<cellinfo,vector<cellinfo>,cellinfo> next;
    unordered_set<int> touched;
    
    insertcellinfo(xyz, uvw, uvw0, _n_cells_per_dim, _bb, _sz_cell, next, touched, d2_min );
    while( !next.empty() ) {
        auto c = next.top(); next.pop();
        if( c.d2 > d2_min + 0.000001 ) break;
        
        for( int i_f : _grid[c.i] ) {
            const vec3i &liv = faces[i_f];
            const vec3f &v0 = pos[liv.x];
            const vec3f &v1 = pos[liv.y];
            const vec3f &v2 = pos[liv.z];
            vec3f p = closest_point_point_triangle(xyz,v0,v1,v2);
            float d2 = lengthSqr(xyz-p);
            if( d2 < d2_min ) {
                i_f_closest = i_f;
                d2_min = d2;
                p_closest = p;
            }
        }
        
        // add neighbors
        for( vec3i n : neighbors ) insertcellinfo( xyz, uvw, c.uvw+n, _n_cells_per_dim, _bb, _sz_cell, next, touched, d2_min );
    }
    
    return p_closest;
}


vec3f Grid::get_nearest_surface_point( const vec3f &xyz, int &i_f_closest, float &ba, float &bb ) const {
    vec3i uvw = get_uvw( xyz );
    vec3i uvw0 = max_component( _bbuvw.min, min_component( _bbuvw.max, uvw ) );
    
    float d2_min = numeric_limits<float>::max();
    i_f_closest = -1; ba = bb = 0.0f;
    vec3f p_closest = makevec3f(d2_min,d2_min,d2_min);
    
    if( faces.size() == 0 ) return p_closest;
    
    priority_queue<cellinfo,vector<cellinfo>,cellinfo> next;
    unordered_set<int> touched;
    
    bool n_opt = (fnorms.size()==faces.size());
    float a012,ap12,ap20;
    
    insertcellinfo(xyz, uvw, uvw0, _n_cells_per_dim, _bb, _sz_cell, next, touched, d2_min );
    while( !next.empty() ) {
        auto c = next.top(); next.pop();
        if( c.d2 > d2_min + 0.000001 ) break;
        
        for( int i_f : _grid[c.i] ) {
            const vec3i &liv = faces[i_f];
            const vec3f &v0 = pos[liv.x];
            const vec3f &v1 = pos[liv.y];
            const vec3f &v2 = pos[liv.z];
            vec3f p = closest_point_point_triangle(xyz,v0,v1,v2);
            float d2 = lengthSqr(xyz-p);
            
            if( d2 > d2_min ) continue;
            
            i_f_closest = i_f;
            d2_min = d2;
            p_closest = p;
            if( n_opt ) {
                vec3f &n = fnorms[i_f];// normalize( cross( v1-v0, v2-v0 ));
                a012 = n % cross( v1-v0, v2-v0 );
                ap12 = n % cross( v1-p, v2-p );
                ap20 = n % cross( v2-p, v0-p );
            } else {
                vec3f n = normalize( cross( v1-v0, v2-v0 ));
                a012 = n % cross( v1-v0, v2-v0 );
                ap12 = n % cross( v1-p, v2-p );
                ap20 = n % cross( v2-p, v0-p );
            }
            ba = ap12 / a012;
            bb = ap20 / a012;
            ba = max(0.0f,min(1.0f,ba));
            bb = max(0.0f,min(1.0f,bb));
            float bc = 1.0f - ba - bb;
            ba += min(bc,0.0f) / 2.0f; bb += min(bc,0.0f)/2.0f;
        }
        
        // add neighbors
        for( vec3i n : neighbors ) insertcellinfo( xyz, uvw, c.uvw+n, _n_cells_per_dim, _bb, _sz_cell, next, touched, d2_min );
    }
    
    return p_closest;
}

void Grid::get_nearest_surface_points( const vector<vec3f> &l_xyz, vector<vec3f> &l_xyz_ ) const {
    int n = l_xyz.size(); l_xyz_.clear(); l_xyz_.reserve( n );
    for( int i = 0; i < n; i++ ) l_xyz_.push_back( get_nearest_surface_point( l_xyz[i] ) );
}


void Grid::get_faces_rball( const vec3f &xyz, const float &r, set<int> &s_if ) const {
    float r2 = r*r;
    vec3i uvw = get_uvw( xyz );
    vec3i uvw0 = max_component( _bbuvw.min, min_component( _bbuvw.max, uvw ) );
    
    //s_if.clear();
    if( faces.size() == 0 ) return;
    
    priority_queue<cellinfo,vector<cellinfo>,cellinfo> next;
    unordered_set<int> touched;
    
    insertcellinfo(xyz, uvw, uvw0, _n_cells_per_dim, _bb, _sz_cell, next, touched, r2 );
    while( !next.empty() ) {
        auto c = next.top(); next.pop();
        
        for( int i_f : _grid[c.i] ) {
            vec3i &f = faces[i_f];
            vec3f &v0 = pos[f.x];
            vec3f &v1 = pos[f.y];
            vec3f &v2 = pos[f.z];
            vec3f p = closest_point_point_triangle(xyz,v0,v1,v2);
            float d2 = lengthSqr(xyz-p);
            if( d2 <= r2 ) s_if.insert( i_f );
        }
        
        // add neighbors
        for( vec3i n : neighbors ) insertcellinfo( xyz, uvw, c.uvw+n, _n_cells_per_dim, _bb, _sz_cell, next, touched, r2 );
    }
}
void Grid::get_faces_rball( const vec3f &xyz, const float &r, unordered_set<int> &s_if ) const {
    float r2 = r*r;
    vec3i uvw = get_uvw( xyz );
    vec3i uvw0 = max_component( _bbuvw.min, min_component( _bbuvw.max, uvw ) );
    
    //s_if.clear();
    if( faces.size() == 0 ) return;
    
    priority_queue<cellinfo,vector<cellinfo>,cellinfo> next;
    unordered_set<int> touched;
    
    insertcellinfo(xyz, uvw, uvw0, _n_cells_per_dim, _bb, _sz_cell, next, touched, r2 );
    while( !next.empty() ) {
        auto c = next.top(); next.pop();
        
        for( int i_f : _grid[c.i] ) {
            vec3i &f = faces[i_f];
            vec3f &v0 = pos[f.x];
            vec3f &v1 = pos[f.y];
            vec3f &v2 = pos[f.z];
            vec3f p = closest_point_point_triangle(xyz,v0,v1,v2);
            float d2 = lengthSqr(xyz-p);
            if( d2 <= r2 ) s_if.insert( i_f );
        }
        
        // add neighbors
        for( vec3i n : neighbors ) insertcellinfo( xyz, uvw, c.uvw+n, _n_cells_per_dim, _bb, _sz_cell, next, touched, r2 );
    }
}

void Grid::get_faces_rball_all( const vec3f &xyz, const float &r, set<int> &s_if ) const {
    float r2 = r*r;
    vec3i uvw = get_uvw( xyz );
    vec3i uvw0 = max_component( _bbuvw.min, min_component( _bbuvw.max, uvw ) );
    
    //s_if.clear();
    if( faces.size() == 0 ) return;
    
    priority_queue<cellinfo,vector<cellinfo>,cellinfo> next;
    unordered_set<int> touched;
    
    insertcellinfo(xyz, uvw, uvw0, _n_cells_per_dim, _bb, _sz_cell, next, touched, r2 );
    while( !next.empty() ) {
        auto c = next.top(); next.pop();
        
        for( int i_f : _grid[c.i] ) s_if.insert( i_f );
        
        // add neighbors
        for( vec3i n : neighbors ) insertcellinfo( xyz, uvw, c.uvw+n, _n_cells_per_dim, _bb, _sz_cell, next, touched, r2 );
    }
}

float Grid::compute_mean_curvature( const vec3f &v, const float &r ) {
    unordered_set<int> s_if; get_faces_rball(v,r,s_if);
    //printf( "rball: %d\n", (int)s_if.size() );
    return compute_mean_curvature_( v, r, pos, faces, s_if );
}











#include "shape.h"
#include "shape_evomesh.h"
#include "draw_utils.h"
#include "io_json.h"
#include "io_bin.h"
#include "debug.h"

CameraShape * CameraFromJson( JsonNavigator nav );
Scene * SceneFromJson( JsonNavigator nav );
ShapeList * ShapeListFromJson( JsonNavigator nav );
TransformedShape * TransformedShapeFromJson( JsonNavigator nav );
PointSet * PointSetFromJson( JsonNavigator nav );
TriangleMesh * TriangleMeshFromJson( JsonNavigator nav );
LineSet * LineSetFromJson( JsonNavigator nav );
LineStrip * LineStripFromJson( JsonNavigator nav );
SnapshotShape * SnapshotShapeFromJson( JsonNavigator nav );
BrushStroke * BrushStrokeFromJson( JsonNavigator nav );



Shape *ShapeFromJson( JsonNavigator nav ) {
    string s_type;
    
    if( nav.is_null() ) return new NullShape();
    
    nav.get_child_value("_type",s_type);
    
    if( s_type == "file" ) return ShapeFromJson(JsonNavigator( nav["file"].get_string() ));
    
    //if( s_type == "EvoMesh" )           return EvoMeshFromJson(nav);
    if( s_type == "Scene" )             return SceneFromJson(nav);
    if( s_type == "TriangleMesh" )      return TriangleMeshFromJson(nav);
    if( s_type == "PointSet" )          return PointSetFromJson(nav);
    if( s_type == "LineSet" )           return LineSetFromJson(nav);
    if( s_type == "ShapeList" )         return ShapeListFromJson(nav);
    if( s_type == "SnapshotShape" )     return SnapshotShapeFromJson(nav);
    if( s_type == "TransformedShape" )  return TransformedShapeFromJson(nav);
    if( s_type == "LineStrip" )         return LineStripFromJson(nav);
    if( s_type == "BrushStroke" )       return BrushStrokeFromJson(nav);
    if( s_type == "Camera" )            return CameraFromJson(nav);
    
    error( (string("unknown type")+s_type).c_str() );
    return nullptr;
}


Scene * SceneFromJson( JsonNavigator nav ) {
    auto scene = new Scene();
    scene->shape = ShapeFromJson(nav["shape"]);
    scene->stroke = ShapeFromJson(nav["stroke"]);
    scene->camera = ShapeFromJson(nav["camera"]);
    return scene;
}

CameraShape * CameraFromJson( JsonNavigator nav ) {
    auto shape = new CameraShape();
    
    auto nav_f = nav["frame"];
    nav_f.get_child_value("o",shape->frame.o,zero3f);
    nav_f.get_child_value("x",shape->frame.x,x3f);
    nav_f.get_child_value("y",shape->frame.y,y3f);
    nav_f.get_child_value("z",shape->frame.z,z3f);
    
    nav.get_child_value("dist",shape->dist);
    nav.get_child_value("pers",shape->perspective);
    
    return shape;
}

BrushStroke * BrushStrokeFromJson( JsonNavigator nav ) {
    auto stroke = new BrushStroke();
    
    if( nav.has_child("fn_lv") ) LoadBinaryFile( nav["fn_lv"].get_string(), stroke->pos );
    else nav["lv"].get_value( stroke->pos );
    
    return stroke;
}




ShapeList * ShapeListFromJson( JsonNavigator nav ) {
    auto shape = new ShapeList();
    
    nav = nav["shapes"];
    int n = nav.get_children_count();
    for( int i = 0; i < n; i++ ) {
        shape->shapes.push_back(ShapeFromJson(nav[i]));
    }
    
    return shape;
}

SnapshotShape * SnapshotShapeFromJson( JsonNavigator nav ) {
    auto shape = new SnapshotShape();
    
    nav = nav["shapes"];
    int n = nav.get_children_count();
    for( int i = 0; i < n; i++ ) {
        shape->shapes.push_back(ShapeFromJson(nav[i]));
    }
    
    return shape;
}

TransformedShape * TransformedShapeFromJson( JsonNavigator nav ) {
    auto shape = new TransformedShape();
    auto nav_f = nav["frame"];
    nav_f.get_child_value("o",shape->frame.o,zero3f);
    nav_f.get_child_value("x",shape->frame.x,x3f);
    nav_f.get_child_value("y",shape->frame.y,y3f);
    nav_f.get_child_value("z",shape->frame.z,z3f);
    shape->shape = ShapeFromJson(nav["shape"]);
    
    return shape;
}

PointSet * PointSetFromJson( JsonNavigator nav ) {
    auto mesh = new PointSet();
    
    if( nav.has_child("fn_lvp") ) LoadBinaryFile( nav["fn_lvp"].get_string(), mesh->pos );
    else nav["lvp"].get_value(mesh->pos);
    
    if( nav.has_child("fn_lvr") ) {
        LoadBinaryFile( nav["fn_lvr"].get_string(), mesh->radius );
    } else if( nav.has_child("lvr") ) {
        nav["lvr"].get_value(mesh->radius);
    } else {
        int n_v = mesh->pos.size();
        mesh->radius.clear();
        mesh->radius.resize( n_v, 1.0f );
    }
    
    return mesh;
}

LineSet * LineSetFromJson( JsonNavigator nav ) {
    auto lineset = new LineSet();
    
    if( nav.has_child("fn_lvp") ) LoadBinaryFile( nav["fn_lvp"].get_string(), lineset->pos );
    else nav["lvp"].get_value(lineset->pos);
    
    if( nav.has_child("fn_lev") ) LoadBinaryFile( nav["fn_lev"].get_string(), lineset->lines );
    else nav["lev"].get_value(lineset->lines);
    
    return lineset;
}

LineStrip * LineStripFromJson( JsonNavigator nav ) {
    auto linestrip = new LineStrip();
    
    if( nav.has_child("fn_lvp") ) LoadBinaryFile( nav["fn_lvp"].get_string(), linestrip->pos );
    else nav["lvp"].get_value(linestrip->pos);
    
    return linestrip;
}

TriangleMesh * TriangleMeshFromJson( JsonNavigator nav ) {
    auto mesh = new TriangleMesh();
    
    if( nav.has_child("fn_lvp") ) LoadBinaryFile( nav["fn_lvp"].get_string(), mesh->pos );
    else nav["lvp"].get_value(mesh->pos);
    
    if( nav.has_child("fn_lvn") ) LoadBinaryFile( nav["fn_lvn"].get_string(), mesh->norm );
    else if(nav.has_child("lvn")) nav["lvn"].get_value(mesh->norm);
    
    if( nav.has_child("fn_lvc") ) LoadBinaryFile( nav["fn_lcv"].get_string(), mesh->l_vc );
    else if(nav.has_child("lvc")) nav["lvc"].get_value(mesh->l_vc);
    
    if( nav.has_child("fn_lev") ) LoadBinaryFile( nav["fn_lev"].get_string(), mesh->edge );
    else if(nav.has_child("lev")) nav["lev"].get_value(mesh->edge);
    
    if( nav.has_child("fn_lfv") ) LoadBinaryFile( nav["fn_lfv"].get_string(), mesh->face );
    else nav["lfv"].get_value(mesh->face);
    
    return mesh;
}








#ifndef _GLOBALS_H_
#define _GLOBALS_H_

#include "std_utils.h"
#include "camera.h"
#include "panel.h"

namespace CameraType {
    enum Enum { Custom, Data };
    const vector<string> Names = { "Custom", "Data" };
}

namespace FaceColorType {
    enum Enum {
        Neutral, NeutralDark, NeutralSmooth,
        Highlight,
        GradDel, GradAdd,
        Brush, BrushClass,
        DepDel, DepAdd,
        Hausdorff, HausdorffVerts,
        MeanCurvature, MeanCurvatureVerts, MeanCurvatureMixed,
        Density,
        Wet
    };
    const vector<string> Names = {
        "Neutral", "Dark", "Smooth",
        "Highlight",
        "Grad:Del", "Grad:Add",
        "Brush", "BrushClass",
        "Dep:Del", "Dep:Add",
        "Hausdorff", "Haus Verts",
        "MC Faces", "MC Verts", "MC Mixed",
        "Density",
        "Wet"
    };
}

namespace BackgroundType {
    enum Enum { Gradient, Black, White };
    const vector<string> Names = { "Gradient", "Black", "White" };
}

namespace CurvatureColorType {
    enum Enum { Centered, Absolute };
    const vector<string> Names = { "Centered", "Absolute" };
}

namespace View3DHoverMode {
    enum Enum { Nothing, Changes, SurfaceArea };
    const vector<string> Names = { "Nothing", "Changes", "Surface Area" };
}

namespace BrushColorType {
    enum Enum { Brush, Classified };
    const vector<string> Names = { "Brush", "BrushClass" };
}

namespace HistogramGraphType {
    enum Enum { Bar, Line };
    const vector<string> Names = { "Bar", "Line" };
}
namespace HistogramGraphMode {
    enum Enum { Full, Difference };
    const vector<string> Names = { "Full", "Diff" };
}

namespace StrokesMode {
    enum Enum { Changes, Played, All };
    const vector<string> Names = { "Changes", "Played", "All" };
}

struct global_data {
    string filename;
    
    timer uptimer;
    unsigned long i_frame = 0;
    
    string *_status = nullptr;
    void set_status( const char *fmt, ... ) {
        if( _status == nullptr ) return;
        char b[256];
        va_list args; va_start(args, fmt); vsprintf( b, fmt, args ); va_end(args);
        _status->assign(b);
    }
    void set_status( string s ) {
        if( _status == nullptr ) return;
        _status->assign(s);
    }
    
    int   auto_play = 0;
    bool  play_stopped = true;
    bool  play_stopat_move = false;
    bool  play_save_screenshots = false;
    int   play_save_index = 0;
    
    int   view3d_turntable_rotz = 0;
    int   view3d_background = BackgroundType::Gradient;
    bool  view3d_toy = false;
    
    bool  view3d_faces = true;
    bool  view3d_faces_onlyadded = false;
    bool  view3d_faces_onlysame = false;
    bool  view3d_faces_finalproj = false;
    bool  view3d_faces_curvatureviz = false;
    bool  view3d_edges = false;
    bool  view3d_edges_add = false;
    bool  view3d_edges_del = false;
    bool  view3d_verts = false;
    bool  view3d_previous = false;
    
    bool  view3d_strokes = true;
    int   view3d_strokes_mode = StrokesMode::Changes;
    int   view3d_strokes_color = BrushColorType::Brush;
    bool  view3d_strokes_alpha = true;
    bool  view3d_strokes_occluded = true;
    bool  view3d_strokes_projected = false;
    bool  view3d_strokes_norm = false;
    float view3d_strokes_proj_norm = 0.0f;
    
    bool  view3d_cameras = false;
    bool  view3d_changes_arrows = true;
    bool  view3d_changes_meandir = false;
    bool  view3d_changes_add = false;
    bool  view3d_changes_del = false;
    bool  view3d_changes_proj = false;
    bool  view3d_changes_projverts = false;
    bool  view3d_highlight_next = false;
    bool  view3d_highlight_prev = false;
    float view3d_morph_fps = 1.0f;
    bool  view3d_axis = false;
    bool  view3d_grid = false;
    int   view3d_hover = View3DHoverMode::Nothing;
    int   view3d_camtype = CameraType::Custom;
    int   view3d_color = FaceColorType::HausdorffVerts;
    VPCamera *view3d_camera;
    bool  view3d_camera_artist = false;
    bool  view3d_camera_artist_smooth = true;
    
    bool  view3d_automd = false;
    bool  view3d_automc = false;
    bool  view3d_clearmc = false;
    float view3d_radius = 0.10f;
    int   view3d_curvaturecolor = CurvatureColorType::Absolute;
    
    int   hist_graph = HistogramGraphType::Bar;
    int   hist_mode = HistogramGraphMode::Full;
    float hist_power = 0.50f;
    float hist_power_vert = 0.50f;
    VPCamera *camera_histogram_mc; // mean curvature
    VPCamera *camera_histogram_hd; // hausdorff distance
    VPCamera *camera_histogram_fd; // face density
    VPCamera *camera_histogram_hvd; // vert directions
    
    bool  stats_brtypes = true;
    bool  stats_brarea = false;
    bool  stats_coded = false;
    bool  stats_vol = false;
    bool  stats_area = false;
    bool  stats_norm = false;
    VPCamera *stats_camera;
    
    VP_CallBacks *depgraph_panel = nullptr;
    bool  depgraph_filter_unselected = true;
    bool  depgraph_auto_reset = false;
    float depgraph_revg_threshold = 100.0f;
    float depgraph_revg_vi_threshold = 1.0f;
    float depgraph_cams_dot_threshold = 0.70f;
    float depgraph_cam_loc_threshold = 0.5f;
    bool  depgraph_depedges = true;
    bool  depgraph_plot_nedges = false;
    bool  depgraph_artist = false;
    bool  depgraph_3dview = false;
    float depgraph_xmult = 1.0f;
    float depgraph_ymult = 1.0f;
    float depgraph_zmult = 1.0f;
    int   depgraph_color = BrushColorType::Brush;
    float depgraph_p = 0.0f;
    bool  depgraph_mesh = false;
    float depgraph_mesh_xmult = 0.1f;
    
    int   depgraph_niters = 0;
    float depgraph_ke = 0.0f;
    float depgraph_ksl = 0.0f;
    float depgraph_kso = 1.0f;
    float depgraph_timestep = 0.01f;
    float depgraph_alpha_add = 50.0f;
    float depgraph_alpha_pow = 2.0f;
    float depgraph_alpha_mult_pow = 0.5f;
    
    VPCamera *depgraph_camera;
};

extern global_data glob;


#endif


#include "shape.h"
#include "shape_evomesh.h"
#include "draw_utils.h"
#include "panel.h"
#include "color_gradient.h"

#include <map>




void PointSet::draw() {
    glDisable(GL_LIGHTING);
    glPointSize(2.0);
    glBegin(GL_POINTS);
	for( int i = 0; i < pos.size(); i++ ) {
        //glPointSize(radius[i]);
        glsVertex(pos[i]);
		//draw_sphere(pos[i],radius[i],4,4);
    }
    glEnd();
    glEnable(GL_LIGHTING);
}

void LineSet::draw() {
    glsCheckError();
    glDisable(GL_LIGHTING);
    glLineWidth(5.0);
    glsColor(makevec3f(0.5,0.5,0.5));
    glBegin(GL_LINES);
    for( auto l : lines ) {
        glsVertex(pos[l.x]);
        glsVertex(pos[l.y]);
    }
    glEnd();
    glEnable(GL_LIGHTING);
    glsCheckError();
}

void LineStrip::draw() {
    glsCheckError();
    glDisable(GL_LIGHTING);
    glLineWidth(5.0);
    glsColor(makevec3f(0.5,0.5,0.5));
    glBegin(GL_LINE_STRIP);
    for( auto v : pos ) glsVertex(v);
    glEnd();
    glEnable(GL_LIGHTING);
    glsCheckError();
}

void BrushStroke::draw() {
    glsCheckError();
    glDisable(GL_LIGHTING);
    glLineWidth(7.5);
    glsColor(makevec3f(0.5,0.5,1.0));
    glBegin(GL_LINE_STRIP);
    for( auto v : pos ) {
        if( fabs(v.x) < 10.0 && fabs(v.y) < 10.0 && fabs(v.z) < 10.0 )
            glsVertex(v);
    }
    glEnd();
    glEnable(GL_LIGHTING);
    glsCheckError();
}

void CameraShape::draw( ) {
    glsCheckError();
    glDisable(GL_LIGHTING);
    
    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glsMultMatrix(frame_to_matrix(frame));
    
    glColor3f(0.5,1.0,0.5);
    glLineWidth(5.0);
    
    glBegin(GL_LINE_STRIP);
    glVertex3f(0,0,0);
    glVertex3f(0,0,dist);
    if( perspective == "ORTHO" ) {
        glVertex3f(0,1,dist);
        glVertex3f(0,1,dist-1);
    } else {
        glVertex3f(0,1,dist-1);
        glVertex3f(0,0,dist-1);
    }
    glEnd();
    
    glPopMatrix();
    
    glEnable(GL_LIGHTING);
    glsCheckError();
}

void TriangleMesh::draw() {
    draw_mesh_faces();
    draw_mesh_lines();
}

void TriangleMesh::draw_mesh_faces() {
    if(norm.empty()) {
		glBegin(GL_TRIANGLES);
        if(uv.empty()) {
            for(auto f : face) {
                glsNormal(face_normal(f));
                glsVertex(pos[f.x]);
                glsVertex(pos[f.y]);
                glsVertex(pos[f.z]);
            }
        } else {
            for(auto f : face) {
                glsNormal(face_normal(f));
                glsTexCoord(uv[f.x]);
                glsVertex(pos[f.x]);
                glsTexCoord(uv[f.y]);
                glsVertex(pos[f.y]);
                glsTexCoord(uv[f.z]);
                glsVertex(pos[f.z]);
            }
        }
		glEnd();
    } else {
        draw_faces(face, pos, norm, uv, l_vc);
    }
}

void TriangleMesh::draw_mesh_lines() {
    glBegin(GL_LINES);
    for( auto e : edge ) {
        glsVertex(pos[e.x]);
        glsVertex(pos[e.y]);
    }
    glEnd();
}







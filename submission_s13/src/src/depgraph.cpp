#include "depgraph.h"


namespace BrushName {
    map<string,Enum> Map_Name_Enum {
        { "", Unspecified },
        { "Blob", Blob },
        { "Brush", Brush },
        { "Clay", Clay },
        { "Clay strips", ClayStrips },
        { "Clay Strips", ClayStrips },
        { "Crease", Crease },
        { "Draw", Draw },
        { "Fill/Deepen", FillDeepen },
        { "Flatten/Contrast", FlattenContrast },
        { "Grab", Grab },
        { "Inflate/Deflate", InflateDeflate },
        { "Layer", Layer },
        { "Mask", Mask },
        { "Nudge", Nudge },
        { "Pinch/Magnify", PinchMagnify },
        { "Polish", Polish },
        { "Scrape/Peaks", ScrapePeaks },
        { "SculptDraw", SculptDraw },
        { "Smooth", Smooth },
        { "Snake Hook", SnakeHook },
        { "Thumb", Thumb },
        { "Twist", Twist }
    };
};


namespace BrushModifier {
    map<string,Enum> Map_Name_Enum {
        { "Normal", Normal },
        { "Invert", Invert },
        { "Smooth", Smooth }
    };
};


void OpNode::reset_repr_stroke() {
    if( !is_init ) {
        pos_orig.clear();
        int c = 0;
        
        // collect strokes of sub_nodes
        unordered_set<OpNode*> nodes; get_sub_nodes(nodes);
        for( auto node : nodes ) {
            auto &stroke = node->stroke;
            for( int i = 0, n = stroke.pos.size(); i < n; i++ ) {
                pos_orig.push_back(stroke.pos[i]); c++;
                if( i>0 && !stroke.brk[i] ) strokes.push_back( { c-2, c-1 } );
            }
        }
        
        pos = pos_orig;
        
        /*int n_str = strokes.size();
        for( int i0 = 0; i0 < n_str; i0++ ) {
            auto &s0 = strokes[i0];
            const vec3f &v00 = pos[s0.i0];
            const vec3f &v01 = pos[s0.i1];
            vec3f vm0 = (v00+v01)/2.0f;
            for( int i1 = i0+1; i1 < n_str; i1++ ) {
                auto &s1 = strokes[i1];
                const vec3f &v10 = pos[s1.i0];
                const vec3f &v11 = pos[s1.i1];
                vec3f vm1 = (v10+v11)/2.0f;
                
                if( lengthSqr(vm0-vm1) <= 0.2f ) {
                    s0.near.push_back(i1);
                    s1.near.push_back(i0);
                }
            }
        }*/
        
        stroke.rpos.resize(pos.size()); // TODO!!
        stroke.brk.clear();
        stroke.brushes.clear();
        stroke.mods.clear();
        
        for( auto node : nodes ) {
            bool first = true;
            auto bru = node->stroke.brush;
            auto mod = node->stroke.mod;
            for( bool b : node->stroke.brk ) {
                stroke.brk.push_back(b | first); first=false;
                stroke.brushes.push_back(bru);
                stroke.mods.push_back(mod);
            }
        }
        
        is_init = true;
    } else {
        pos = pos_orig;
    }
    update_repr_stroke();
}

void OpNode::compute_repr_stroke( int n_iters, float ke, float ksl, float kso, float time_step ) {
    int n_pos = pos.size();
    int n_str = strokes.size();
    
    for( auto s : strokes ) s.near.clear();
    for( int i0 = 0; i0 < n_str; i0++ ) {
        auto &s0 = strokes[i0];
        const vec3f &v00 = pos[s0.i0];
        const vec3f &v01 = pos[s0.i1];
        vec3f vm0 = (v00+v01)/2.0f;
        for( int i1 = i0+1; i1 < n_str; i1++ ) {
            auto &s1 = strokes[i1];
            const vec3f &v10 = pos[s1.i0];
            const vec3f &v11 = pos[s1.i1];
            vec3f vm1 = (v10+v11)/2.0f;
            
            if( lengthSqr(vm0-vm1) <= 0.2f ) {
                s0.near.push_back(i1);
                s1.near.push_back(i0);
            }
        }
    }
    
    vector<vec3f> vel( n_pos, zero3f );
    vector<vec3f> acc( n_pos, zero3f );
    
    for( int iters = 0; iters < n_iters; iters++ ) {
        for( int i0 = 0; i0 < n_str; i0++ ) {
            const auto &s0 = strokes[i0];
            const vec3f &v00 = pos[s0.i0];
            const vec3f &v01 = pos[s0.i1];
            
            vec3f v0m = (v00+v01)/2.0f;
            float l0 = length(v01-v00);
            vec3f d0 = (v01-v00) / l0;
            
            // approx electrostatic force to nearby segments
            for( int i1 : s0.near ) {
                const auto &s1 = strokes[i1];
                const vec3f &v10 = pos[s1.i0];
                const vec3f &v11 = pos[s1.i1];
                
                vec3f v1m = (v10+v11)/2.0f;
                float l1 = length(v11-v10);
                vec3f d1 = (v11-v10)/l1;
                
                vec3f dm = v0m-v1m;
                float l = length(dm);
                dm /= l;
                float cskew = fabs(dot(dm,d0)) * fabs(dot(dm,d1));
                
                float lavg = (l0+l1)/2.0f;
                
                float F = min( ke / (l*l), 10.0f );
                
                float ca = powf( fabs(dot(d0,d1)), 3.0f );
                float cp = lavg / (lavg + l);
                
                vec3f a0 = (ca * cp * cskew) * F * ((v1m-v0m) / l);
                vec3f a1 = (ca * cp * cskew) * F * ((v0m-v1m) / l);
                
                acc[s0.i0] += a0; acc[s0.i1] += a0;
                acc[s1.i0] += a1; acc[s1.i1] += a1;
            }
            
            // spring force for maintaining segment length
            {
                const vec3f &o0 = pos_orig[s0.i0];
                const vec3f &o1 = pos_orig[s0.i1];
                float l_orig = length(o0-o1);
                float l_move = length(v00-v01);
                
                float F = ksl * (l_orig-l_move);
                
                float l = length(v01-v00);
                vec3f a00 = F * ((v00-v01)/l);
                vec3f a01 = F * ((v01-v00)/l);
                
                acc[s0.i0] += a00;
                acc[s0.i1] += a01;
            }
            
            // spring force back to orig position
            {
                const vec3f &o0 = pos_orig[s0.i0];
                vec3f a = kso * (o0-v00);
                acc[s0.i0] += a;
            }
        }
        
        for( int i = 0; i < n_pos; i++ ) {
            vel[i] += acc[i] * time_step / 100.0f;
            pos[i] += vel[i] * time_step / 100.0f;
            acc[i] = zero3f;
        }
    }
    
    update_repr_stroke();
}

void OpNode::update_repr_stroke() {
    stroke.pos = pos;
    
    pos_alpha.assign( pos.size(), 0.0f );
    int n = strokes.size();
    float n_ = powf(n,glob.depgraph_alpha_mult_pow);
    
    for( int i0 = 0; i0 < n; i0++ ) {
        auto &s0 = strokes[i0];
        float &a00 = pos_alpha[s0.i0];
        float &a01 = pos_alpha[s0.i1];
        vec3f p0 = (pos[s0.i0]+pos[s0.i1])/2.0f;
        for( int i1 : s0.near ) {
            auto &s1 = strokes[i1];
            float &a10 = pos_alpha[s1.i0];
            float &a11 = pos_alpha[s1.i1];
            vec3f p1 = (pos[s1.i0]+pos[s1.i1])/2.0f;
            float a = 1.0f / powf(glob.depgraph_alpha_add + n_ * length(p0-p1), glob.depgraph_alpha_pow);
            a00 = min(1.00f,max(0.05f,a00+a));
            a01 = min(1.00f,max(0.05f,a01+a));
            a10 = min(1.00f,max(0.05f,a10+a));
            a11 = min(1.00f,max(0.05f,a11+a));
        }
    }
}


















#ifndef _ACCEL_GRID_H_
#define _ACCEL_GRID_H_

#include "std.h"
#include "shape.h"
#include "ext/common/vec.h"

using namespace std;

typedef void (fn_iter)( const vec3i &uvw );

class Grid {
public:
    Grid( TriangleMesh *tmesh, int n_cells_per_dim=-1 );
    Grid( vector<vec3f> &pos, vector<vec3i> &faces, int n_cells_per_dim=-1 );
    ~Grid();
    
    vec3i get_uvw( const vec3f &xyz ) const;
    vec3f get_rel( const vec3f &xyz ) const;
    vec3f get_xyz( const vec3i &uvw, const vec3f &rel ) const;
    int   get_ind( const vec3i &uvw ) const;
    int   get_ind( int u, int v, int w ) const;
    int   get_ind( const vec3f &xyz ) const;
    vec3i get_uvw( int ind ) const;
    
    void iterate( range3i bb, fn_iter fn ) const;
    
    bool has_moved( const vec3f &xyz, const float d2=0.000001f ) const;
    
    vec3f get_nearest_surface_point( const vec3f &xyz ) const;
    vec3f get_nearest_surface_point( const vec3f &xyz, int &i_f_closest, float &ba, float &bb ) const;
    vec3f get_nearest_surface_point( const vec3f &xyz, int &i_f_closest ) const;
    
    void get_nearest_surface_points( const vector<vec3f> &l_xyz, vector<vec3f> &l_xyz_ ) const;
    
    void get_faces_rball( const vec3f &xyz, const float &r, set<int> &s_if ) const;
    void get_faces_rball( const vec3f &xyz, const float &r, unordered_set<int> &s_if ) const;
    void get_faces_rball_all( const vec3f &xyz, const float &r, set<int> &s_if ) const;
    
    float compute_mean_curvature( const vec3f &v, const float &r );
    
    TriangleMesh *_tmesh;
    vector<vec3f> &pos;
    vector<vec3i> &faces;
    vector<vec3f> &fnorms;  vector<vec3f> fnorms_blank;
    
private:
    void _get_inds( const vec3f &v0, const vec3f &v1, const vec3f &v2, unordered_set<int> &s_inds );
    void _get_inds( const vec3f &v0, const vec3i &u0, const vec3f &v1, const vec3i &u1, const vec3f &v2, const vec3i &u2, unordered_set<int> &s_inds );
    
    const vector<vec3i> neighbors = {
        { -1,  0,  0 }, {  1,  0,  0 },
        {  0, -1,  0 }, {  0,  1,  0 },
        {  0,  0, -1 }, {  0,  0,  1 },
        { -1, -1,  0 }, {  1, -1,  0 }, {  1,  1,  0 }, { -1,  1,  0 },
        {  0, -1, -1 }, {  0,  1, -1 }, {  0,  1,  1 }, {  0, -1,  1 },
        { -1,  0, -1 }, { -1,  0,  1 }, {  1,  0,  1 }, {  1,  0, -1 }
    };
    
    range3f _bb;
    range3i _bbuvw;
    vec3f _sz_bb;
    vec3f _sz_cell;
    vec3f _n_cells;
    int _n_cells_per_dim;
    
    vector<vector<int>> _grid;
    vector<vector<int>> _grid_iv;
    //vector<vector<vector<vector<int>>>> _grid;
    //vector<vector<vector<vector<int>>>> _grid_iv;
};



#endif


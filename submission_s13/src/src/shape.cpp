#include "shape.h"
#include "std.h"
#include "draw_utils.h"
#include "io_json.h"

#include <set>
#include <queue>

#include "accel_kdtree.h"



float area( const vector<vec3f> &pos, const vec3i &face ) {
    vec3f A = pos[face.x];
    vec3f B = pos[face.y];
    vec3f C = pos[face.z];
    float a = length(B - C);
    float b = length(C - A);
    float c = length(A - B);
    float s = (a+b+c)/2.0;
    return sqrt(s*(s-a)*(s-b)*(s-c));
}

void TriangleMesh::recolor_geo_density( int k ) {
    l_vc.clear();
    l_vc.reserve(pos.size());
    
    int n_verts = pos.size();
    vector<float> neighborarea;
    neighborarea.resize(n_verts, 0);
    
    KDTree *kdt = new KDTree();
    for( int i_v = 0; i_v < n_verts; i_v++ ) kdt->insert( pos[i_v], i_v );
    
    vector<int> l_i;
    vector<float> l_dist2;
    #pragma omp parallel for
    for( int i_v = 0; i_v < n_verts; i_v++ ) {
        kdt->knn( pos[i_v], k, l_i, l_dist2 );
        float d = 0.0;
        for( float _d : l_dist2 ) d += _d;
        neighborarea[i_v] = d;
    }
    delete kdt;
    
    float m = *min_element(neighborarea.begin(),neighborarea.end());
    float M = *max_element(neighborarea.begin(),neighborarea.end());
    
    for( int i_v = 0; i_v < n_verts; i_v++ ) {
        float c = neighborarea[i_v];
        float p = (float)(c - m) / ((float)(M - m) + 0.000001);
        p = pow(p,0.3);
        float r = 1.0 - p;
        float g = p;
        float b = p;
        l_vc.push_back(makevec3f(r,g,b));
    }
}

void TriangleMesh::recolor_neutral() {
    l_vc.clear();
    l_vc.resize(pos.size(),makevec3f(0.5,0.5,0.5));
}

















#include "shape_evomesh.h"
#include "std.h"
#include "draw_utils.h"
#include "io_json.h"
#include "geom.h"
#include "color_gradient.h"
#include "globals.h"

#include <set>
#include <queue>

#include "accel_grid.h"


#define CLAMP(x,m,M) ( (x) < (m) ? (m) : ( (x) > (M) ? (M) : (x) ) )


vector<vec3f> distance_colors = {
    {0.3,0.3,0.3},{0.25,0.25,0.35},{0.0,0.0,1.0},
    {0.0,0.7,0.7},{0.0,1.0,0.0},{0.7,0.7,0.0},
    {1.0,0.0,0.0},{1.0,1.0,0.0},{1.0,1.0,1.0}
};
vector<float> distance_marks = { -1.0, 0.0, 0.001, 0.01, 0.10, 0.15, 0.25, 0.50, 1.0 };

vector<vec3f> distance_colors_out = {
    {0.3,0.3,0.3}, {0.5,0.5,0.5},{0.5,0.6,0.5},{0.2,0.5,0.2},{0.0,1.0,0.0},{0.0,1.0,1.0},{0.8,1.0,0.9}
};
vector<vec3f> distance_colors_in = {
    {0.3,0.3,0.3}, {0.5,0.5,0.5},{0.6,0.5,0.5},{0.5,0.2,0.2},{1.0,0.0,0.0},{1.0,0.0,1.0},{1.0,0.8,0.9}
};
vector<float> distance_marks_inout = { -1.0, 0.0, 0.01, 0.1, 0.25, 0.5, 1.0 };



vector<vec3f> rainbow = {
    { 0.0, 0.0, 1.0 },{ 0.0, 0.5, 0.5 },{ 0.0, 1.0, 0.0 },{ 1.0, 0.0, 0.0 },{ 1.0, 1.0, 0.0 },{ 1.0, 1.0, 1.0 },
};
vector<vec3f> colors_centered = {
    {0.3,0.3,0.3},
    {0.5,1.0,0.0},{0.0,1.0,0.0},{0.0,1.0,1.0},{0.0,0.5,1.0},
    {0.2,0.2,0.5},
    {0.5,0.0,1.0},{1.0,0.0,1.0},{1.0,0.0,0.0},{1.0,0.5,0.0}
};
vector<vec3f> colors_absolute = {
    {0.3,0.3,0.3},
    {1.0,1.0,1.0},{1.0,1.0,0.0},{1.0,0.0,0.0},{0.0,1.0,0.0},{0.0,0.5,1.0},
    {0.2,0.2,0.5},
    {0.0,0.5,1.0},{0.0,1.0,0.0},{1.0,0.0,0.0},{1.0,1.0,0.0},{1.0,1.0,1.0}
};

vector<vec3f> rainbow2x = {
    { 0.0, 0.0, 1.0 },{ 0.0, 0.5, 0.5 },{ 0.0, 1.0, 0.0 },{ 1.0, 0.0, 0.0 },{ 1.0, 1.0, 0.0 },{ 1.0, 1.0, 1.0 },
    { 0.0, 0.0, 1.0 },{ 0.0, 0.5, 0.5 },{ 0.0, 1.0, 0.0 },{ 1.0, 0.0, 0.0 },{ 1.0, 1.0, 0.0 },{ 1.0, 1.0, 1.0 }
};

vector<vec3f> bc_colors = {
    { 0.5, 0.5, 0.5 }, // none
    { 0.1, 1.0, 0.1 }, // vol add
    { 1.0, 0.1, 0.1 }, // vol del
    { 0.1, 0.1, 1.0 }, // smooth
    { 1.0, 0.5, 0.1 }, // crease
    { 0.1, 0.7, 0.7 }, // move
    { 0.5, 1.0, 0.5 }, // feat add
    { 1.0, 0.5, 0.5 }, // feat del
};

vector<float> curvature_marks_9 = { -1.0, 0.00, 0.50, 0.90, 0.99, 1.00, 1.01, 1.10, 1.50, 2.00 };
vector<float> curvature_marks_11 = { -1.0, 0.00, 0.50, 0.75, 0.90, 0.99, 1.00, 1.01, 1.10, 1.25, 1.50, 2.00 };

//ColorGradient grad_curvature = { colors_centered }; //{ rainbow };
ColorGradient_NonLinear grad_curvature_centered = { colors_centered, curvature_marks_9 };
ColorGradient_NonLinear grad_curvature_absolute = { colors_absolute, curvature_marks_11 };

ColorGradient grad_timeline = { rainbow2x };
ColorGradient_NonLinear grad_distance = { distance_colors, distance_marks };
ColorGradient_NonLinear grad_distance_in = { distance_colors_in, distance_marks_inout };
ColorGradient_NonLinear grad_distance_out = { distance_colors_out, distance_marks_inout };
ColorGradient grad_density = { rainbow };

map<BrushName::Enum,vec3f> map_brush_color = {
    { BrushName::Unspecified,       { 0.5, 0.5, 0.5 } },
    { BrushName::Blob,              { 0.5, 0.2, 0.2 } },
    { BrushName::Brush,             { 0.5, 0.2, 0.2 } },
    { BrushName::Clay,              { 0.2, 1.0, 0.5 } },
    { BrushName::ClayStrips,        { 0.5, 1.0, 0.2 } },
    { BrushName::Crease,            { 1.0, 0.5, 1.0 } },
    { BrushName::Draw,              { 0.5, 0.2, 0.2 } },
    { BrushName::FillDeepen,        { 0.2, 1.0, 0.2 } },
    { BrushName::FlattenContrast,   { 0.1, 0.1, 0.5 } },
    { BrushName::Grab,              { 1.0, 1.0, 0.2 } },
    { BrushName::InflateDeflate,    { 0.5, 0.2, 0.2 } },
    { BrushName::Layer,             { 0.2, 1.0, 0.2 } },
    { BrushName::Mask,              { 0.2, 0.2, 0.4 } },
    { BrushName::Nudge,             { 0.8, 0.8, 0.2 } },
    { BrushName::PinchMagnify,      { 1.0, 0.5, 1.0 } },
    { BrushName::Polish,            { 0.1, 0.1, 0.5 } },
    { BrushName::ScrapePeaks,       { 0.2, 1.0, 0.2 } },
    { BrushName::SculptDraw,        { 0.2, 1.0, 0.5 } },
    { BrushName::Smooth,            { 0.5, 0.7, 1.0 } },
    { BrushName::SnakeHook,         { 1.0, 1.0, 1.0 } },
    { BrushName::Thumb,             { 1.0, 1.0, 0.2 } },
    { BrushName::Twist,             { 1.0, 1.0, 0.2 } },
    { BrushName::Mixed,             { 0.3, 0.3, 0.3 } },
};





PickerData EvoMeshView::get_face( const ray3f &r ) {
    PickerData hit = { -1, zero3f, 0.0f, 0.0f, 0.0f };
    
    for( int i_f : ems->l_if ) {
        const vec3i &f = faces[i_f];
        const vec3f &p0 = pos[f.x];
        const vec3f &p1 = pos[f.y];
        const vec3f &p2 = pos[f.z];
        float t, ba, bb;
        if( !intersect_triangle( r, p0, p1, p2, t, ba, bb ) ) continue;
        if( hit.idx==-1 || t < hit.dist ) {
            hit.idx = i_f;
            hit.a = ba;
            hit.b = bb;
            hit.dist = t;
            hit.p = r.o + (r.d*t);
        }
    }
    
    return hit;
}
PickerData EvoMeshView::get_face_final( const ray3f &r ) {
    PickerData hit = { -1, zero3f, 0.0f, 0.0f, 0.0f };
    
    for( int i_f : evomesh->ems_final->l_if ) {
        const vec3i &f = faces[i_f];
        const vec3f &p0 = pos[f.x];
        const vec3f &p1 = pos[f.y];
        const vec3f &p2 = pos[f.z];
        float t, ba, bb;
        if( !intersect_triangle( r, p0, p1, p2, t, ba, bb ) ) continue;
        if( hit.idx==-1 || t < hit.dist ) {
            hit.idx = i_f;
            hit.a = ba;
            hit.b = bb;
            hit.dist = t;
            hit.p = r.o + (r.d*t);
        }
    }
    
    return hit;
}

PickerData EvoMeshView::get_face_repr( const ray3f &r, EvoMeshState *ems ) {
    PickerData hit = { -1, zero3f, 0.0f, 0.0f, 0.0f };
    
    int n = map_faces.size();
    for( int i_f = 0; i_f < n; i_f++ ) {
        const vec3i &f = map_faces[i_f];
        vec3f &p0 = ems->map_pos[f.x];
        vec3f &p1 = ems->map_pos[f.y];
        vec3f &p2 = ems->map_pos[f.z];
        float t, ba, bb;
        if( !intersect_triangle( r, p0, p1, p2, t, ba, bb ) ) continue;
        if( hit.idx==-1 || t < hit.dist ) {
            hit.idx = i_f;
            hit.a = ba;
            hit.b = bb;
            hit.dist = t;
            hit.p = r.o + (r.d*t);
        }
    }
    
    return hit;
}




void EvoMeshView::draw_mesh_faces() {
    vec3f c = { 0.7, 0.7, 0.7 }, c_dark = { 0.1, 0.1, 0.1 };
    vec3f half3f = { 0.5, 0.5, 0.5 };
    
    vector<int> l_if_same;
    if( glob.view3d_faces_onlysame ) {
        l_if_same.reserve(ems->l_if.size());
        int i = 0, n = emsc->faces.li_add.size();
        for( int i_f : ems->l_if ) {
            while( i < n && emsc->faces.li_add[i] < i_f ) i++;
            if( i < n && emsc->faces.li_add[i] == i_f ) { i++; continue; }
            l_if_same.push_back(i_f);
        }
    }
    
    vector<int> &l_if_ = ( glob.view3d_faces_onlysame ? l_if_same : ( glob.view3d_faces_onlyadded ? emsc->faces.li_add : ems->l_if ) );
    
    
    glBegin( GL_TRIANGLES );
    
    glColor3f(0,0,0);
    glsMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT,one3f);
    glsMaterialfv(GL_FRONT_AND_BACK,GL_DIFFUSE,c);
    glsMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,zero3f);
    glsMaterialfv(GL_FRONT_AND_BACK,GL_EMISSION,zero3f);
    glMaterialf(GL_FRONT_AND_BACK,GL_SHININESS,0.0f);
    
    if( glob.view3d_color == FaceColorType::Neutral || glob.view3d_color == FaceColorType::NeutralDark ) {
        if( glob.view3d_color == FaceColorType::Neutral ) {
            glsMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, c );
        } else {
            glsMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, c_dark );
        }
        for( auto i_f : l_if_ ) {
            const vec3i &f = faces[i_f];
            glsNormal(fnorms[i_f]);
            glsVertex(pos[f.x]);
            glsVertex(pos[f.y]);
            glsVertex(pos[f.z]);
        }
    } else if( glob.view3d_color == FaceColorType::NeutralSmooth ) {
        glsMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, c );
        for( auto i_f : l_if_ ) {
            const vec3i &f = faces[i_f];
            glsNormal(vstats[f.x].norm);
            glsVertex(pos[f.x]);
            glsNormal(vstats[f.y].norm);
            glsVertex(pos[f.y]);
            glsNormal(vstats[f.z].norm);
            glsVertex(pos[f.z]);
        }
    } else {
        ColorGradient_NonLinear &grad_curvature = ( glob.view3d_curvaturecolor == CurvatureColorType::Centered ? grad_curvature_centered : grad_curvature_absolute );
        
        for( auto i_f : l_if_ ) {
            const vec3i &f = faces[i_f];
            
            switch( glob.view3d_color ) {
                case FaceColorType::Highlight: {
                    if( fstats[i_f].md_val >= 0.01f ) {
                        c = { 0.8, 0.8, 0.6 };
                    } else {
                        c = { 0.7, 0.7, 0.7 };
                    }
                } break;
                case FaceColorType::GradDel: {
                    c = grad_timeline.get_color( evomesh->ftimes[i_f].i1, 0, n_ops );
                } break;
                case FaceColorType::GradAdd: {
                    c = grad_timeline.get_color( evomesh->ftimes[i_f].i0, 0, n_ops );
                } break;
                case FaceColorType::Brush: {
                    int i_fop = evomesh->ftimes[i_f].i0;
                    BrushName::Enum &s_brush = evomesh->nodes[i_fop]->stroke.brush; // l_str_brush[i_fop];
                    BrushModifier::Enum &s_mod = evomesh->nodes[i_fop]->stroke.mod;
                    if(s_mod == BrushModifier::Normal ) c = map_brush_color[s_brush];
                    else if(s_mod == BrushModifier::Invert) c = one3f - map_brush_color[s_brush];
                    else if(s_mod == BrushModifier::Smooth) c = map_brush_color[BrushName::Smooth];
                } break;
                case FaceColorType::BrushClass: {
                    
                    
                    
                    
                    //c = bc_colors[ evomesh->nodes[i_f]->stats.classification l_op_classified[ evomesh->ftimes[i_f].i0 ];
                    
                    
                    
                    
                    
                } break;
                case FaceColorType::DepDel: {
                    int i_op = evomesh->ftimes[i_f].i1;
                    c = i_op >= n_ops ? half3f : evomesh->nodes[i_op]->color * 2.0f;
                } break;
                case FaceColorType::DepAdd: {
                    int i_op = evomesh->ftimes[i_f].i0;
                    c = i_op == n_ops ? half3f : evomesh->nodes[i_op]->color * 2.0f;
                } break;
                case FaceColorType::Hausdorff: {
                    if( fstats[i_f].md_side == -1 ) c = grad_distance_in.get_color( fstats[i_f].md_val );
                    else c = grad_distance_out.get_color( fstats[i_f].md_val );
                } break;
                case FaceColorType::HausdorffVerts: {
                    float d0 = vstats[f.x].md_val, d1 = vstats[f.y].md_val, d2 = vstats[f.z].md_val;
                    char ov0 = vstats[f.x].md_side, ov1 = vstats[f.y].md_side, ov2 = vstats[f.z].md_side;
                    //if( ov0 ) { if( ov1 == 0 ) ov1 = ov0; if( ov2 == 0 ) ov2 = ov0; }
                    //if( ov1 ) { if( ov0 == 0 ) ov0 = ov1; if( ov2 == 0 ) ov2 = ov1; }
                    //if( ov2 ) { if( ov0 == 0 ) ov0 = ov2; if( ov1 == 0 ) ov1 = ov2; }
                    bool o0 = ov0>0, o1 = ov1>0, o2 = ov2>0;
                    bool i0 = ov0<0, i1 = ov1<0, i2 = ov2<0;
                    int outside = (o0?1:0) + (o1?1:0) + (o2?1:0);
                    int inside = (i0?1:0) + (i1?1:0) + (i2?1:0);
                    
                    //vec3f c_ni = {0.6,0.5,0.5}, c_no = {0.5,0.6,0.5};
                    vec3f c_ni = {0.5,0.5,0.5}, c_no = {0.5,0.5,0.5};
                    vec3f c_h = {1.2,1.2,0.3};
                    
                    if( inside == 2 || outside == 1 ) {
                        if( o0 ) { i1 = i2 = true; }
                        if( o1 ) { i0 = i2 = true; }
                        if( o2 ) { i0 = i1 = true; }
                        int i_i0 = ( i0 ? f.x : f.y );
                        int i_i1 = ( i2 ? f.z : f.y );
                        int i_o = ( !i0 ? f.x : ( !i1 ? f.y : f.z ) );
                        
                        float d_i0 = vstats[i_i0].md_val, d_i1 = vstats[i_i1].md_val, d_o = vstats[i_o].md_val;
                        float d_i0o = d_o + d_i0, d_i1o = d_o + d_i1;
                        const vec3f &v_i0 = pos[i_i0], &v_i1 = pos[i_i1], &v_o = pos[i_o];
                        vec3f v_i0o = v_i0*(d_o / d_i0o) + v_o*((d_i0o-d_o) / d_i0o);
                        vec3f v_i1o = v_i1*(d_o / d_i1o) + v_o*((d_i1o-d_o) / d_i1o);
                        vec3f c_i0 = grad_distance_in.get_color( d_i0 );
                        vec3f c_i1 = grad_distance_in.get_color( d_i1 );
                        vec3f c_o = grad_distance_out.get_color( d_o );
                        
                        if( i1 ) glsNormal(-fnorms[i_f]);
                        else glsNormal(fnorms[i_f]);
                        
                        vec3f v_i0_ = (v_i0o*0.98f)+(v_i0*0.02f);
                        vec3f v_i1_ = (v_i1o*0.98f)+(v_i1*0.02f);
                        vec3f v_o0_ = (v_i0o*0.98f)+(v_o*0.02f);
                        vec3f v_o1_ = (v_i1o*0.98f)+(v_o*0.02f);
                        
                        glsMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, c_h );
                        glsVertex(v_i1_); glsVertex(v_i0_); glsVertex(v_o1_);
                        glsVertex(v_o1_); glsVertex(v_i0_); glsVertex(v_o0_);
                        
                        glsMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, c_i0 ); glsVertex( v_i0 );
                        glsMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, c_ni ); glsVertex( v_i1_ );
                        glsMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, c_i1 ); glsVertex( v_i1 );
                        
                        glsMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, c_i0 ); glsVertex( v_i0 );
                        glsMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, c_ni ); glsVertex( v_i0_ );
                        glsMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, c_ni ); glsVertex( v_i1_ );
                        
                        glsMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, c_o ); glsVertex( v_o );
                        glsMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, c_no ); glsVertex( v_o1_ );
                        glsMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, c_no ); glsVertex( v_o0_ );
                    } else if( outside == 2 || inside == 1 ) {
                        if( i0 ) { o1 = o2 = true; }
                        if( i1 ) { o0 = o2 = true; }
                        if( i2 ) { o0 = o1 = true; }
                        int i_o0 = ( o0 ? f.x : f.y );
                        int i_o1 = ( o2 ? f.z : f.y );
                        int i_i = ( !o0 ? f.x : ( !o1 ? f.y : f.z ) );
                        
                        float d_o0 = vstats[i_o0].md_val, d_o1 = vstats[i_o1].md_val, d_i = vstats[i_i].md_val;
                        float d_o0i = d_o0 + d_i, d_o1i = d_o1 + d_i;
                        const vec3f &v_o0 = pos[i_o0], &v_o1 = pos[i_o1], &v_i = pos[i_i];
                        vec3f v_o0i = v_o0*((d_o0i-d_o0) / d_o0i) + v_i*(d_o0 / d_o0i);
                        vec3f v_o1i = v_o1*((d_o1i-d_o1) / d_o1i) + v_i*(d_o1 / d_o1i);
                        vec3f c_o0 = grad_distance_out.get_color( d_o0 );
                        vec3f c_o1 = grad_distance_out.get_color( d_o1 );
                        vec3f c_i = grad_distance_in.get_color( d_i );
                        
                        if( !o1 ) glsNormal(-fnorms[i_f]);
                        else glsNormal(fnorms[i_f]);
                        
                        vec3f v_o0_ = (v_o0i*0.98f)+(v_o0*0.02f);
                        vec3f v_o1_ = (v_o1i*0.98f)+(v_o1*0.02f);
                        vec3f v_i0_ = (v_o0i*0.98f)+(v_i*0.02f);
                        vec3f v_i1_ = (v_o1i*0.98f)+(v_i*0.02f);
                        
                        glsMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, c_h );
                        glsVertex(v_o0_); glsVertex(v_o1_); glsVertex(v_i1_);
                        glsVertex(v_o0_); glsVertex(v_i1_); glsVertex(v_i0_);
                        
                        glsMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, c_o0 ); glsVertex( v_o0 );
                        glsMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, c_o1 ); glsVertex( v_o1 );
                        glsMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, c_no ); glsVertex( v_o1_ );
                        
                        glsMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, c_o0 ); glsVertex( v_o0 );
                        glsMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, c_no ); glsVertex( v_o1_ );
                        glsMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, c_no ); glsVertex( v_o0_ );
                        
                        glsMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, c_i ); glsVertex( v_i );
                        glsMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, c_ni ); glsVertex( v_i0_ );
                        glsMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, c_ni ); glsVertex( v_i1_ );
                    } else if( outside == 0 && inside == 0 ) {
                        vec3f c = {0.3,0.3,0.3};
                        glsNormal(fnorms[i_f]);
                        glsMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, c );
                        glsVertex(pos[f.x]); glsVertex(pos[f.y]); glsVertex(pos[f.z]);
                    } else {
                        ColorGradient_NonLinear &cg = ( o0 ? grad_distance_out : grad_distance_in );
                        vec3f c0 = cg.get_color(d0), c1 = cg.get_color(d1), c2 = cg.get_color(d2);
                        glsNormal(fnorms[i_f]);
                        glsMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, c0 ); glsVertex( pos[f.x] );
                        glsMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, c1 ); glsVertex( pos[f.y] );
                        glsMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, c2 ); glsVertex( pos[f.z] );
                    }
                    continue;
                } break;
                case FaceColorType::MeanCurvature: {
                    float mc0 = fstats[i_f].mc_val;
                    float mc1 = vstats[f.x].mc_val;
                    float mc2 = vstats[f.y].mc_val;
                    float mc3 = vstats[f.z].mc_val;
                    if( glob.view3d_curvaturecolor == CurvatureColorType::Absolute ) {
                        if( mc0 > 1.0f ) mc0 = 2.0 - mc0;
                        if( mc1 > 1.0f ) mc1 = 2.0 - mc1;
                        if( mc2 > 1.0f ) mc2 = 2.0 - mc2;
                        if( mc3 > 1.0f ) mc3 = 2.0 - mc3;
                    }
                    
                    float mc = -1.0f;
                    if( mc0 > -0.99f && mc1 > -0.99f && mc2 > -0.99f && mc3 > -0.99f ) {
                        mc = (mc0+mc1+mc2+mc3)/4.0f;
                    }
                    c = grad_curvature.get_color( mc );
                } break;
                case FaceColorType::MeanCurvatureVerts: {
                    float mc1 = vstats[f.x].mc_val;
                    float mc2 = vstats[f.y].mc_val;
                    float mc3 = vstats[f.z].mc_val;
                    if( glob.view3d_curvaturecolor == CurvatureColorType::Absolute ) {
                        if( mc1 > 1.0f ) mc1 = 2.0 - mc1;
                        if( mc2 > 1.0f ) mc2 = 2.0 - mc2;
                        if( mc3 > 1.0f ) mc3 = 2.0 - mc3;
                    }
                    glsNormal(fnorms[i_f]);
                    vec3f c0 = grad_curvature.get_color( mc1 );//, 0.0, 2.0 );//, meancurve_min, meancurve_max );
                    vec3f c1 = grad_curvature.get_color( mc2 );//, 0.0, 2.0 );//, meancurve_min, meancurve_max );
                    vec3f c2 = grad_curvature.get_color( mc3 );//, 0.0, 2.0 );//, meancurve_min, meancurve_max );
                    const vec3f &v0 = pos[f.x];
                    const vec3f &v1 = pos[f.y];
                    const vec3f &v2 = pos[f.z];
                    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, &c0.x ); glsVertex(v0);
                    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, &c1.x ); glsVertex(v1);
                    glMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, &c2.x ); glsVertex(v2);
                    continue;
                } break;
                case FaceColorType::MeanCurvatureMixed: {
                    float mc0 = fstats[i_f].mc_val;
                    float mc1 = vstats[f.x].mc_val;
                    float mc2 = vstats[f.y].mc_val;
                    float mc3 = vstats[f.z].mc_val;
                    if( glob.view3d_curvaturecolor == CurvatureColorType::Absolute ) {
                        if( mc0 > 1.0f ) mc0 = 2.0 - mc0;
                        if( mc1 > 1.0f ) mc1 = 2.0 - mc1;
                        if( mc2 > 1.0f ) mc2 = 2.0 - mc2;
                        if( mc3 > 1.0f ) mc3 = 2.0 - mc3;
                    }
                    glsNormal(fnorms[i_f]);
                    c = grad_curvature.get_color( mc0 );//,0.0,2.0); //, meancurve_min, meancurve_max );
                    vec3f c0 = grad_curvature.get_color( mc1 );//,0.0,2.0); //, meancurve_min, meancurve_max );
                    vec3f c1 = grad_curvature.get_color( mc2 );//,0.0,2.0); //, meancurve_min, meancurve_max );
                    vec3f c2 = grad_curvature.get_color( mc3 );//,0.0,2.0); //, meancurve_min, meancurve_max );
                    const vec3f &v0 = pos[f.x];
                    const vec3f &v1 = pos[f.y];
                    const vec3f &v2 = pos[f.z];
                    vec3f v = (v0+v1+v2) / 3.0f;
                    glsMaterialfv( GL_FRONT_AND_BACK, GL_DIFFUSE, c0 ); glsVertex(v0);
                    glsMaterialfv( GL_FRONT_AND_BACK, GL_DIFFUSE, c1 ); glsVertex(v1);
                    glsMaterialfv( GL_FRONT_AND_BACK, GL_DIFFUSE, c  ); glsVertex(v);
                    glsMaterialfv( GL_FRONT_AND_BACK, GL_DIFFUSE, c1 ); glsVertex(v1);
                    glsMaterialfv( GL_FRONT_AND_BACK, GL_DIFFUSE, c2 ); glsVertex(v2);
                    glsMaterialfv( GL_FRONT_AND_BACK, GL_DIFFUSE, c  ); glsVertex(v);
                    glsMaterialfv( GL_FRONT_AND_BACK, GL_DIFFUSE, c2 ); glsVertex(v2);
                    glsMaterialfv( GL_FRONT_AND_BACK, GL_DIFFUSE, c0 ); glsVertex(v0);
                    glsMaterialfv( GL_FRONT_AND_BACK, GL_DIFFUSE, c  ); glsVertex(v);
                    continue;
                } break;
                case FaceColorType::Density: {
                    float a_ = powf( 1.0f / fareas[i_f], glob.hist_power );
                    c = grad_density.get_color( a_, 0.0f, 10.0f );
                } break;
                case FaceColorType::Wet: {
                    if( emsc->s_if_change.count(i_f) == 0 ) {
                        c = {0.7f,0.7f,0.7f};
                        glsMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT,one3f);
                        glsMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,zero3f);
                        glMaterialf(GL_FRONT_AND_BACK,GL_SHININESS,0.0f);
                    } else {
                        float d = fstats[i_f].md_val;
                        d = pow( min(fabs(d),1.0f), 0.25f );
                        c = { 0.7f-0.4f*d, 0.7f-0.4f*d, 0.7f-0.4f*d };
                        vec3f s = { d, d, d };
                        vec3f a = { 1.0f-d, 1.0f-d, 1.0f-d };
                        float v = 64.0f * d;
                        glsMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT,a);
                        glsMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,s);
                        glMaterialf(GL_FRONT_AND_BACK,GL_SHININESS,v);
                    }
                } break;
            }
            
            glsMaterialfv(GL_FRONT_AND_BACK, GL_DIFFUSE, c );
            glsNormal(fnorms[i_f]);
            glsVertex(pos[f.x]);
            glsVertex(pos[f.y]);
            glsVertex(pos[f.z]);
        }
    }
    
    glsMaterialfv(GL_FRONT_AND_BACK,GL_AMBIENT,one3f);
    glsMaterialfv(GL_FRONT_AND_BACK,GL_DIFFUSE,c_dark);
    glsMaterialfv(GL_FRONT_AND_BACK,GL_SPECULAR,zero3f);
    glMaterialf(GL_FRONT_AND_BACK,GL_SHININESS,0.0f);
    glEnd();
}

void EvoMeshView::draw_mesh_lines() {
    glBegin(GL_LINES);
    for( auto i_e : ems->l_ie ) {
        const vec2i &e = edges[i_e];
        glsVertex(pos[e.x]);
        glsVertex(pos[e.y]);
    }
    glEnd();
}

void EvoMeshView::draw_camera() {
    glPushAttrib(GL_ENABLE_BIT);
    glDisable(GL_LIGHTING);
    
    glMatrixMode(GL_MODELVIEW);
    
    glColor3f(0.2,0.5,0.2);
    glLineWidth(5.0);
    
    for( int i_op : emsc->l_in ) {
        glPushMatrix();
        float dist = evomesh->nodes[i_op]->camera.dist;
        float sz = min( 1.0f, dist*0.2f );
        glsMultMatrix(frame_to_matrix(evomesh->nodes[i_op]->camera.frame));
        
        glBegin(GL_LINE_STRIP);
        glVertex3f(0,0,0);
        glVertex3f(0,0,dist);
        if( evomesh->nodes[i_op]->camera.ortho ) {
            glVertex3f(0,sz,dist);
            glVertex3f(0,sz,dist-sz);
        } else {
            glVertex3f(0,sz,dist-sz);
            glVertex3f(0,0,dist-sz);
        }
        glEnd();
        
        glPopMatrix();
    }
    
    glPopAttrib();
}

void EvoMeshView::draw_edges( const vector<int> &l_ie_ ) {
    glBegin( GL_LINES );
    for( int i_e : l_ie_ ) {
        const vec2i &e = edges[i_e];
        glsVertex( pos[e.x] );
        glsVertex( pos[e.y] );
    }
    glEnd();
}

void EvoMeshView::draw_faces( const vector<int> &l_if_ ) {
    glBegin(GL_TRIANGLES);
    for( auto i_f : l_if_ ) {
        const vec3i &f = faces[i_f];
        glsNormal(fnorms[i_f]);
        glsVertex(pos[f.x]);
        glsVertex(pos[f.y]);
        glsVertex(pos[f.z]);
    }
    glEnd();
}
void EvoMeshView::draw_faces( const set<int> &l_if_ ) {
    glBegin(GL_TRIANGLES);
    for( auto i_f : l_if_ ) {
        const vec3i &f = faces[i_f];
        glsNormal(fnorms[i_f]);
        glsVertex(pos[f.x]);
        glsVertex(pos[f.y]);
        glsVertex(pos[f.z]);
    }
    glEnd();
}
void EvoMeshView::draw_face( const int i_f ) {
    glBegin(GL_TRIANGLES);
    const vec3i &f = faces[i_f];
    glsNormal(fnorms[i_f]);
    glsVertex(pos[f.x]);
    glsVertex(pos[f.y]);
    glsVertex(pos[f.z]);
    glEnd();
}

void EvoMeshView::draw_face_repr( unordered_set<int> &s_ifr, EvoMeshState *ems ) {
    vector<int> &map_iv_if = ems->map_if;
    vector<vec3f> &map_pos = ems->map_pos;
    unordered_set<int> s_if;
    for( int i_fr : s_ifr ) {
        const vec3i &f = map_faces[i_fr];
        s_if.insert(map_iv_if[f.x]);
        s_if.insert(map_iv_if[f.y]);
        s_if.insert(map_iv_if[f.z]);
    }
    glBegin(GL_TRIANGLES);
    for( int i_f : s_if ) {
        const vec3i &f = faces[i_f];
        const vec3f &v0 = pos[f.x];
        const vec3f &v1 = pos[f.y];
        const vec3f &v2 = pos[f.z];
        glsNormal( fnorms[i_f] );
        glsVertex(v0); glsVertex(v1); glsVertex(v2);
    }
    for( int i_fr : s_ifr ) {
        const vec3i &f = map_faces[i_fr];
        vec3f &v0 = map_pos[f.x];
        vec3f &v1 = map_pos[f.y];
        vec3f &v2 = map_pos[f.z];
        glsNormal( triangle_normal(v0,v1,v2) );
        glsVertex(v0); glsVertex(v1); glsVertex(v2);
    }
    glEnd();
}

void EvoMeshView::draw_changes_add() {
    vec3f c_add = makevec3f( 0.2, 1.0, 0.2 );
    vec3f c_add_ = c_add * 0.5f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, &c_add.x );
    glMaterialfv(GL_BACK, GL_DIFFUSE, &c_add_.x );
    draw_faces( emsc->faces.li_add );
}
void EvoMeshView::draw_changes_del() {
    vec3f c_del = makevec3f( 1.0, 0.2, 0.2 );
    vec3f c_del_ = c_del * 0.5f;
    glMaterialfv(GL_FRONT, GL_DIFFUSE, &c_del.x );
    glMaterialfv(GL_BACK, GL_DIFFUSE, &c_del_.x );
    draw_faces( emsc->faces.li_del );
}


void EvoMeshView::draw_brush_strokes() {
    draw_brush_strokes( ems->map_pos );
}

void EvoMeshView::draw_repr_brush_strokes() {
    glEnable(GL_BLEND);
    glEnable(GL_LINE_SMOOTH);
    
    glBegin(GL_LINES);
    for( OpNode *n : nodes_clustered ) {
        int n_pos = n->stroke.pos.size();
        vec3f l = zero3f;
        for( int i = 0; i < n_pos; i++ ) {
            if( i>0 && !n->stroke.brk[i] ) {
                vec3f c;
                const BrushModifier::Enum &s_mod = n->stroke.mods[i];
                const BrushName::Enum &s_brush = ( s_mod == BrushModifier::Smooth ? BrushName::Smooth : n->stroke.brushes[i] );
                c = map_brush_color[ s_brush ];
                if( s_mod == BrushModifier::Invert ) c = one3f - c;
                
                glColor4f(c.x,c.y,c.z,n->pos_alpha[i]);
                glsVertex(l);
                glsVertex(n->stroke.pos[i]);
                
            }
            l = n->stroke.pos[i];
        }
    }
    glEnd();
    
    glDisable(GL_LINE_SMOOTH);
    glDisable(GL_BLEND);
}

void EvoMeshView::draw_brush_strokes( vector<vec3f> &pos_repr ) {
    glEnable(GL_BLEND);
    glEnable(GL_LINE_SMOOTH);
    
    glBegin(GL_LINES);
    for( OpNode *n : nodes ) {
        vec3f c;
        const BrushModifier::Enum &s_mod = n->stroke.mod;
        const BrushName::Enum &s_brush = ( s_mod == BrushModifier::Smooth ? BrushName::Smooth : n->stroke.brush );
        c = map_brush_color[ s_brush ];
        if( s_mod == BrushModifier::Invert ) c = one3f - c;
        
        glsColor(c);
        int n_pos = n->stroke.pos.size();
        vec3f l = zero3f;
        for( int i = 0; i < n_pos; i++ ) {
            if( i>0 && !n->stroke.brk[i] ) {
                glsVertex(l);
                glsVertex(n->stroke.pos[i]);
                
            }
            l = n->stroke.pos[i];
        }
    }
    glEnd();
    
    glDisable(GL_LINE_SMOOTH);
    glDisable(GL_BLEND);
}

/*void EvoMeshView::draw_brush_strokes( vector<vec3f> &pos_repr ) {
    // render strokes
    vector<int> l_in;
    switch( glob.view3d_strokes_mode ) {
        case StrokesMode::Changes: {
            l_in = emsc->l_in;
        } break;
        case StrokesMode::Played: {
            l_in.assign( ems->l_in.begin(), ems->l_in.end() );
        } break;
        case StrokesMode::All: {
            for( int i_ = 0; i_ < n_ops; i_++ ) l_in.push_back(i_);
        } break;
    }
    
    glEnable(GL_BLEND);
    glEnable(GL_LINE_SMOOTH);
    
    for( int i_op : l_in ) {
        vec3f c;
        switch( glob.view3d_strokes_color ) {
            case BrushColorType::Brush: {
                const BrushModifier::Enum &s_mod = evomesh->nodes[i_op]->stroke.mod;
                const BrushName::Enum &s_brush = ( s_mod == BrushModifier::Smooth ? BrushName::Smooth : evomesh->nodes[i_op]->stroke.brush );
                c = map_brush_color[ s_brush ];
                if( s_mod == BrushModifier::Invert ) c = one3f - c;
            } break;
            case BrushColorType::Classified: {
                
                
                
                
                //c = bc_colors[evomesh->l_op_classified[i_op]];
                
                
                
            } break;
        }
        float d = ( glob.view3d_strokes_alpha ? powf( min(1.0f,fabs(evomesh->nodes[i_op]->stats.vol_delta)), 0.25f ) : 1.0f );
        float alpha = powf(d,2.0f);
        float width = max( powf( evomesh->nodes[i_op]->stats.area_post, 0.2f ), 3.0f );
        int n = evomesh->nodes[i_op]->stroke.pos.size();
        
        glLineWidth( width+2.5f ); glColor4f(0,0,0,alpha);
        glPointSize( width+2.5f );
        glBegin(GL_LINE_STRIP);
        if( glob.view3d_strokes_projected ) {
            for( StrokeReprData &rp : evomesh->nodes[i_op]->stroke.rpos ) {
                const vec3i &f=map_faces[rp.i_f];
                vec3f &v0=pos_repr[f.x], &v1=pos_repr[f.y], &v2=pos_repr[f.z];
                float ba=rp.ba, bb=rp.bb, bc=1.0-ba-bb;
                vec3f v = (v0*ba) + (v1*bb) + (v2*bc);
                vec3f norm = triangle_normal(v0,v1,v2);
                if( glob.view3d_strokes_norm ) v += rp.n * norm;
                v += glob.view3d_strokes_proj_norm * norm;
                if( rp.brk ) { glEnd(); glBegin(GL_LINE_STRIP); }
                glsVertex( v );
            }
        } else {
            for( int i = 0; i < n; i++ ) {
                if(evomesh->nodes[i_op]->stroke.brk[i] ) { glEnd(); glBegin(GL_LINE_STRIP); }
                glsVertex(evomesh->nodes[i_op]->stroke.pos[i]);
            }
        }
        glEnd();
        glBegin(GL_POINTS);
        if( glob.view3d_strokes_projected ) {
            for( StrokeReprData &rp : evomesh->nodes[i_op]->stroke.rpos ) {
                const vec3i &f=map_faces[rp.i_f];
                vec3f &v0=pos_repr[f.x], &v1=pos_repr[f.y], &v2=pos_repr[f.z];
                float ba=rp.ba, bb=rp.bb, bc=1.0-ba-bb;
                vec3f v = (v0*ba) + (v1*bb) + (v2*bc);
                vec3f norm = triangle_normal(v0,v1,v2);
                if( glob.view3d_strokes_norm ) v += rp.n * norm;
                v += glob.view3d_strokes_proj_norm * norm;
                glsVertex( v );
            }
        } else {
            for( int i = 0; i < n; i++ ) {
                glsVertex(evomesh->nodes[i_op]->stroke.pos[i]);
            }
        }
        glEnd();
        
        glLineWidth( width ); glColor4f(c.x,c.y,c.z,alpha);
        glPointSize( width );
        glBegin(GL_LINE_STRIP);
        if( glob.view3d_strokes_projected ) {
            for( StrokeReprData &rp : evomesh->nodes[i_op]->stroke.rpos ) {
                const vec3i &f = map_faces[rp.i_f];
                vec3f &v0=pos_repr[f.x], &v1=pos_repr[f.y], &v2=pos_repr[f.z];
                float ba=rp.ba, bb=rp.bb, bc=1.0-ba-bb;
                vec3f v = (v0*ba) + (v1*bb) + (v2*bc);
                vec3f norm = triangle_normal(v0,v1,v2);
                if( glob.view3d_strokes_norm ) v += rp.n * norm;
                v += glob.view3d_strokes_proj_norm * norm;
                if( rp.brk ) { glEnd(); glBegin(GL_LINE_STRIP); }
                glsVertex( v );
            }
        } else {
            for( int i = 0; i < n; i++ ) {
                if(evomesh->nodes[i_op]->stroke.brk[i]) { glEnd(); glBegin(GL_LINE_STRIP); }
                glsVertex(evomesh->nodes[i_op]->stroke.pos[i]);
            }
        }
        glEnd();
        glBegin(GL_POINTS);
        if( glob.view3d_strokes_projected ) {
            for( StrokeReprData &rp : evomesh->nodes[i_op]->stroke.rpos ) {
                const vec3i &f = map_faces[rp.i_f];
                vec3f &v0=pos_repr[f.x], &v1=pos_repr[f.y], &v2=pos_repr[f.z];
                float ba=rp.ba, bb=rp.bb, bc=1.0-ba-bb;
                vec3f v = (v0*ba) + (v1*bb) + (v2*bc);
                vec3f norm = triangle_normal(v0,v1,v2);
                if( glob.view3d_strokes_norm ) v += rp.n * norm;
                v += glob.view3d_strokes_proj_norm * norm;
                glsVertex( v );
            }
        } else {
            for( int i = 0; i < n; i++ ) {
                glsVertex(evomesh->nodes[i_op]->stroke.pos[i]);
            }
        }
        glEnd();
    }
    
    if(false) {
        glPointSize(7.5);
        glBegin(GL_POINTS);
        for( int i_op : l_in_chg ) {
            glsColor( bc_colors[evomesh->l_op_classified[i_op]] );
            if( glob.view3d_strokes_projected ) {
                for( ReprPos &rp : evomesh->l_str_rpos[i_op] ) {
                    vec3i &f = map_faces[rp.i_f];
                    vec3f &v0=pos_repr[f.x], &v1=pos_repr[f.y], &v2=pos_repr[f.z];
                    float ba=rp.ba, bb=rp.bb, bc=1.0-ba-bb;
                    vec3f v = (v0*ba) + (v1*bb) + (v2*bc);
                    glsVertex(v);
                }
            } else {
                for( auto v : evomesh->l_str_pos[i_op] ) {
                    glsVertex(v);
                }
            }
        }
        glEnd();
    }
    
    glDisable(GL_LINE_SMOOTH);
    glDisable(GL_BLEND);
}*/

void EvoMeshView::draw_global_verts() {
    // draw all global verts
    glPointSize(2.0);
    glColor3f( 1.0, 0.5, 0.5 );
    glBegin(GL_POINTS);
    for( int i = 0; i < pos.size(); i++ ) glsVertex(pos[i]);
    glEnd();
}

void EvoMeshView::draw_depgraph_dag( const vec3f &viewd ) {
    draw_depgraph_dag( viewd, nodes_clustered[0] );
}

void EvoMeshView::draw_depgraph_dag( const vec3f &viewd, OpNode *root ) {
    
    const float size_max = 0.5f;
    const float edge_squeeze = 0.2f;
    
    const float px = 1.0f - sin(glob.depgraph_p*PIf/2.0f);
    const float pyz = cos(glob.depgraph_p*PIf/2.0f);
    
    float tr_4fps = glob.uptimer.elapsed_saw( 4.0, true );
    
    glDisable(GL_LIGHTING);
    
    if( n_over ) {
        // highlight node under mouse
        vec3f v = n_over->get_pos(px,pyz);
        glPointSize( 16.0f+tr_4fps*5.0f );
        glBegin(GL_POINTS);
        glColor3f( 1, 1, 0.5 ); glsVertex( v );
        glEnd();
        glPointSize( 13.0f+tr_4fps*5.0f );
        glBegin(GL_POINTS);
        glColor3f( 0,0,0 ); glsVertex( v );
        glEnd();
    }
    
    if( i_over_f != -1 ) {
        // highlight nodes that start / end the face under mouse
        int i_s = evomesh->ftimes[i_over_f].i0;
        int i_e = evomesh->ftimes[i_over_f].i1;
        
        vec3f vs = evomesh->nodes[i_s]->get_pos(px,pyz);
        glPointSize( 16.0f+tr_4fps*5.0f );
        glBegin(GL_POINTS);
        glColor3f( 0.5, 1, 0.5 ); glsVertex( vs );
        glEnd();
        glPointSize( 13.0f+tr_4fps*5.0f );
        glBegin(GL_POINTS);
        glColor3f( 0,0,0 ); glsVertex( vs );
        glEnd();
        
        if( i_e < n_ops ) {
            vec3f ve = evomesh->nodes[i_e]->get_pos(px,pyz);
            glPointSize( 16.0f+tr_4fps*5.0f );
            glBegin(GL_POINTS);
            glColor3f( 1, 0.5, 0.5 ); glsVertex( ve );
            glEnd();
            glPointSize( 13.0f+tr_4fps*5.0f );
            glBegin(GL_POINTS);
            glColor3f( 0,0,0 ); glsVertex( ve );
            glEnd();
        }
    }
    
    glLineWidth(1.0);
    vector<OpNode*> l_nodes; unordered_set<OpNode*> touched;
    l_nodes.push_back(root);
    while( !l_nodes.empty() ) {
        OpNode *node = pop_back(l_nodes);
        if( touched.count(node) ) continue; touched.insert(node);
        
        vec3f v_node = node->get_pos(px,pyz);
        float size = size_max;
        unordered_set<int> inds; node->get_sub_indices(inds);
        float rot = 0.0f;
        vec3f cf,ce;
        
        //if( node->nodes.size() == 0 ) {
        //    size = max( 0.1f, min( 1.0f, node->stats.area_post / 50.0f ) ) * size_max;
        //} else {
        //    size = max( 0.1f, min( 1.0f, (float)node->nodes.size() / 20.0f ) ) * size_max;
        //}
        
        int n_played = 0; bool justplayed=false;
        for( int idx : inds ) {
            if( ems->s_in.count(idx) ) n_played++;
            if( emsc->s_in.count(idx) ) justplayed=true;
        }
        
        if( glob.depgraph_color == BrushColorType::Brush ) {
            BrushName::Enum s_brush = node->stroke.brush;
            BrushModifier::Enum s_mod = node->stroke.mod;
            cf = map_brush_color[s_brush];
            if( s_mod == BrushModifier::Invert ) cf = one3f - cf;
            else if(s_mod == BrushModifier::Smooth ) cf = map_brush_color[BrushName::Smooth];
        } else if( glob.depgraph_color == BrushColorType::Classified ) {
            cf = bc_colors[node->stats.classification];
        }
        cf *= 1.0f - max( 0.0f, min( 1.0f, (v_node.y + 2.5f) / 5.0f ) ) * 0.6f;
        
        if( n_played ) {
            if( n_played == inds.size() ) {
                // all played
                cf *= 0.6f;
            } else {
                // same played
                cf.y *= 0.7f;
                cf.z *= 0.7f;
            }
        }
        
        if( justplayed ) {
            ce = { 0.5, 1.0, 1.0 };
            rot = glob.uptimer.elapsed(0.2f) * (PIf*2.0f);
        } else {
            if( n_played == 0 ) ce = (one3f+cf) * 0.5f;
            else ce = cf * 0.50f;
        }
        
        if( node->bookmarked ) {
            glColor3f( 1.2, 1.2, 0.6 );
            draw_cube_edges( v_node, size*2.0f );
        }
        
        glsColor( cf );
        draw_cube( v_node, size, rot );
        glsColor( ce );
        draw_cube_edges( v_node, size, rot );
        
        unordered_set<OpNode*> children;
        if( glob.depgraph_filter_unselected && !s_if_selected.empty() ) {
            unordered_set<int> verts_selected;
            for( int i_f : s_if_selected ) {
                const vec3i &f = map_faces[i_f];
                verts_selected.insert(f.x);
                verts_selected.insert(f.y);
                verts_selected.insert(f.z);
            }
            node->traverse_dfs_stoppable([&](OpNode*n)->bool {
                for( ReprMoveVert &rmv : n->reprchanges ) {
                    if( verts_selected.count(rmv.i_v) ) {
                        children.insert(n);
                        return false;
                    }
                }
                return true;
            });
        } else {
            for( auto c : node->children ) children.insert(c);
        }
        
        if( glob.depgraph_depedges ) {
            glEnable(GL_BLEND);
            vec3f p0 = v_node;
            vec3f c0 = node->color;
            
            for( auto child : children ) {
                vec3f p1 = child->get_pos( px, pyz );
                vec3f o = normalize(p1-p0)*edge_squeeze;
                vec3f p0_ = p0+o;
                vec3f p1_ = p1-o;
                vec3f c;
                
                if( child->parents.size() == 1 ) c = child->color;
                else c = c0;
                
                bool all_played = evomesh->is_all_played(child,ems);
                bool all_unplayed = evomesh->is_all_unplayed(child,ems);
                if( all_unplayed ) c *= 1.0f;
                else if( !all_played ) { c.y *= 0.7f; c.z *= 0.7f; }
                else c *= 0.6f;
                //c *= ems->s_in.count(child->idx) ? 0.6f : 1.0f;
                
                //float a_len = 0.05f + 0.8f / (1.0f + powf(p1_.x - p0_.x,0.5f));
                float a_len = 0.05f + 0.8f / (1.0f + length(p1_-p0_));
                
                glColor4f( c.x, c.y, c.z, a_len );
                draw_arrow( p0_, p1_, viewd, 0.25f, 0.8f );
                
                if( all_unplayed ) glColor4f( 0.5f+c.x*0.5f, 0.5f+c.y*0.5f, 0.5f+c.z*0.5f, a_len );
                else if( !all_played ) glColor4f( 0.5f+c.x*0.5f, c.y*0.5f, c.z*0.5f, a_len );
                else glColor4f( c.x*0.5f, c.y*0.5f, c.z*0.5f, a_len );
                draw_arrow_edges( p0_, p1_, viewd, 0.25f, 0.8f );
            }
            
            glDisable(GL_BLEND);
        }
        
        for( auto child : children ) l_nodes.push_back(child);
    }
    
    /*// render nodes
    glLineWidth(1.0);
    for( int i_op = 0; i_op < n_ops; i_op++ ) {
        vec3f c;
        auto node = evomesh->nodes[i_op];
        vec3f v_node = node->get_pos(px,pyz);
        
        float size = max( 0.1f, min( node->stats.area_post / 50.0f, 1.0f ) ) * size_max;
        
        if( glob.depgraph_color == BrushColorType::Brush ) {
            BrushName::Enum s_brush = node->stroke.brush;
            BrushModifier::Enum s_mod = node->stroke.mod;
            c = map_brush_color[s_brush];
            if( s_mod == BrushModifier::Invert ) c = one3f - c;
            else if(s_mod == BrushModifier::Smooth ) c = map_brush_color[BrushName::Smooth];
        } else if( glob.depgraph_color == BrushColorType::Classified ) {
            c = bc_colors[node->stats.classification];
        }
        
        c *= 1.0f - max( 0.0f, min( 1.0f, (v_node.y + 2.5f) / 5.0f ) ) * 0.6f;
        
        if( node->bookmarked ) {
            glColor3f( 1.2, 1.2, 0.6 );
            //glLineWidth(3.0);
            draw_cube_edges( v_node, size*2.0f );
            //glLineWidth(1.0);
        }
        
        if( emsc->s_in.count(i_op) ) {
            if( emsc->s_in.count(i_op) ) {
                glsColor( c * 0.3f );
                draw_cube( v_node, size, glob.uptimer.elapsed(0.2f) * (PIf*2.0f) );
                glColor3f(0.5,1.0,1.0);
                draw_cube_edges( v_node, size, glob.uptimer.elapsed(0.2f) * (PIf*2.0f) );
            } else {
                glsColor( c );
                draw_cube( v_node, size, glob.uptimer.elapsed(0.2f) * (PIf*2.0f) );
                glColor3f(0.5,1.0,1.0);
                draw_cube_edges( v_node, size, glob.uptimer.elapsed(0.2f) * (PIf*2.0f) );
            }
        } else {
            if( ems->s_in.count(i_op) ) {
                glsColor( c*0.3f );
                draw_cube( v_node, size );
                glsColor( c*0.15f );
                draw_cube_edges( v_node, size );
            } else {
                bool b_tail = false, b_allplayed = true;
                for( auto parent : node->parents ) {
                    if( emsc->s_in.count(parent->idx) == 0 ) { b_allplayed = false; }
                    else { b_tail=true; }
                }
                
                if( !b_tail ) {
                    glsColor( c );
                    draw_cube( v_node, size );
                    glsColor( c*0.5f );
                    draw_cube_edges( v_node, size );
                } else {
                    glsColor( c );
                    draw_cube( v_node, size, glob.uptimer.elapsed(1.0f) * (PIf*2.0f) );
                    if( b_allplayed ) glColor3f( 0.5, 1.0, 0.5 );
                    else glColor3f( 1.0, 0.5, 0.5 );
                    draw_cube_edges( v_node, size, glob.uptimer.elapsed(1.0f) * (PIf*2.0f) );
                }
            }
        }
    }
    
    
    // draw edges
    if( glob.depgraph_depedges ) {
        glEnable(GL_BLEND);
        for( auto node : evomesh->nodes ) {
            vec3f p0 = node->get_pos( px, pyz );
            vec3f c0 = node->color;
            
            int n_children = node->children.size();
            for( int i_child = 0; i_child < n_children; i_child++ ) {
                OpNode *child = node->children[i_child];
                
                vec3f p1 = child->get_pos( px, pyz );
                vec3f o = normalize(p1-p0)*edge_squeeze;
                vec3f p0_ = p0+o;
                vec3f p1_ = p1-o;
                vec3f c;
                
                if( child->parents.size() == 1 ) c = child->color;
                else c = c0;
                
                c *= emsc->s_in.count(child->idx) ? 0.6f : 1.0f;
                
                float a_len = 0.25f + 0.75f / (1.0f + powf(p1_.x - p0_.x,2.0f));
                glColor4f( c.x, c.y, c.z, a_len );
                draw_arrow( p0_, p1_, viewd, 0.25f, 0.8f );
            }
        }
        glDisable(GL_BLEND);
    }*/
    
    /*if( glob.depgraph_artist ) {
        glLineWidth( 3.0f );
        for( int i_op = 0; i_op < n_ops-1; i_op++ ) {
            auto node0 = l_dp_nodes[i_op];
            auto node1 = l_dp_nodes[i_op+1];
            
            vec3f p0 = node0->get_pos( px, pyz );
            vec3f p1 = node1->get_pos( px, pyz );
            vec3f o = normalize(p1-p0)*edge_squeeze;
            vec3f p0_ = p0+o;
            vec3f p1_ = p1-o;
            
            if( s_in_played.count( i_op ) ) glColor3f( 0.25, 0.25, 0.25 );
            else glColor3f( 0.5, 0.5, 0.5 );
            
            //draw_arrow( l_dp_nodes[i_op]->get_pos( px, pyz ), l_dp_nodes[i_op+1]->get_pos( px, pyz ), 0.5f );
            draw_arrow( p0_, p1_, viewd );
        }
    }*/
    
    /*if( glob.depgraph_mesh ) {
        float xmult = glob.depgraph_mesh_xmult;
        glEnable(GL_LIGHTING);
        glPointSize(1.0f);
        //glBegin(GL_POINTS);
        glBegin(GL_TRIANGLES);
        for( int i_op = 1; i_op < n_ops-1; i_op++ ) {
            auto &node = l_dp_nodes[i_op];
            vec3f v = node->get_pos(px,pyz);
            vec3f c = node->color;
            glsMaterialfv(GL_FRONT_AND_BACK,GL_DIFFUSE,c);
            //glsColor( c );
            for( int i_f : l_fops[i_op][1] ) {
                vec3i &f = faces[i_f];
                vec3f v0=pos[f.x], v1=pos[f.y], v2=pos[f.z];
                glsNormal( fnorms[i_f] );
                glVertex3f( v.x+v0.x*xmult, v0.y, v0.z );
                glVertex3f( v.x+v1.x*xmult, v1.y, v1.z );
                glVertex3f( v.x+v2.x*xmult, v2.y, v2.z );
                //glVertex3f( v.x+(v0.x+v1.x+v2.x)/3.0f*0.1f, (v0.y+v1.y+v2.y)/3.0f, (v0.z+v1.z+v2.z)/3.0f );
            }
        }
        glEnd();
        glDisable(GL_LIGHTING);
    }*/
    
    /*if( glob.depgraph_plot_nedges ) {
        // viz number of edges
        struct node_x {
            int i_n; float x;
            bool operator() (const node_x &lhs, const node_x &rhs) const { return lhs.x > rhs.x; }
        };
        const float z_shift = -5.0f;
        priority_queue<node_x,vector<node_x>,node_x> x_nodes;
        set<int> touched;
        float mx = l_dp_nodes[0]->get_pos(px,pyz).x, Mx = mx, Mz = 1;
        x_nodes.push( { 0, mx } );
        touched.insert(0);
        glLineWidth(1.0); glColor3f(0.3,0.3,0.3);
        glBegin(GL_TRIANGLES);
        float lx,lz; bool first = true;
        while( !x_nodes.empty() ) {
            node_x nx = x_nodes.top(); x_nodes.pop();
            float cx=nx.x, cz=x_nodes.size()+1;
            if( !first ) {
                glVertex3f( lx, 20, z_shift ); glVertex3f( cx, 20, z_shift ); glVertex3f( lx, 20, lz+z_shift );
                glVertex3f( cx, 20, z_shift ); glVertex3f( lx, 20, lz+z_shift ); glVertex3f( cx, 20, cz+z_shift );
            } else first = false;
            lx = cx; lz = cz;
            
            if( cx > Mx ) Mx = cx;
            if( cz > Mz ) Mz = cz;
            
            for( DepNode *dchild : l_dp_nodes[nx.i_n]->dp_children ) {
                if( touched.count( dchild->_i ) ) continue;
                touched.insert( dchild->_i );
                x_nodes.push( { dchild->_i, dchild->get_pos(px,pyz).x } );
            }
        }
        glEnd();
        
        glColor3f(0.2,0.2,0.2);
        glBegin(GL_LINES);
        for( int z = 0; z < Mz; z+= 5 ) {
            glVertex3f(mx,19.9f,z+z_shift); glVertex3f(Mx,19.9f,z+z_shift);
        }
        glEnd();
    }*/
    
}


void EvoMeshView::draw_brush_stats() {
    // viz brush stats
    
    float tr_4fps = glob.uptimer.elapsed_saw( 4.0, true );
    float tr_1fps = glob.uptimer.elapsed_saw( 1.0, true );
    
    float scale_x = 0.1f;
    
    //glDisable(GL_LIGHTING);
    //glDisable(GL_DEPTH_TEST);
    glDisable(GL_CULL_FACE);
    
    
    if( glob.stats_brtypes ) {
        glBegin(GL_TRIANGLES);
        for( int i_op = 0; i_op < n_ops; i_op++ ) {
            OpNode *node = nodes[i_op];
            float m = ems->s_in.count(i_op) ? 0.5f : 1.0f; //(i_op<idx) ? 0.5f : 1.0f;
            BrushName::Enum s_brush = node->stroke.brush;
            BrushModifier::Enum s_mod = node->stroke.mod;
            
            if( s_mod == BrushModifier::Smooth ) s_brush = BrushName::Smooth;
            vec3f c = map_brush_color[s_brush];
            
            float x0 = ((float)i_op-0.5f) * scale_x;
            float x1 = ((float)i_op+0.5f) * scale_x;
            
            if( s_mod != BrushModifier::Invert ) {
                glsColor( c*m );
                glVertex3f( x0, 0.0f, -1.0f );
                glVertex3f( x1, 0.0f, -1.0f );
                glVertex3f( x1, 0.0f, 1.0 );
                glVertex3f( x0, 0.0f, -1.0f );
                glVertex3f( x1, 0.0f, 1.0 );
                glVertex3f( x0, 0.0f, 1.0 );
            } else {
                glsColor( c*m );
                glVertex3f( x0, 0.0f, 0.0f );
                glVertex3f( x1, 0.0f, 0.0f );
                glVertex3f( x1, 0.0f, 1.0 );
                glVertex3f( x0, 0.0f, 0.0f );
                glVertex3f( x1, 0.0f, 1.0 );
                glVertex3f( x0, 0.0f, 1.0 );
                glsColor( (one3f - c)*m );
                glVertex3f( x0, 0.0f, -1.0f );
                glVertex3f( x1, 0.0f, -1.0f );
                glVertex3f( x1, 0.0f, 0.0 );
                glVertex3f( x0, 0.0f, -1.0f );
                glVertex3f( x1, 0.0f, 0.0 );
                glVertex3f( x0, 0.0f, 0.0 );
            }
        }
        glEnd();
    }
    
    if( glob.stats_coded ) {
        // draw classified affects types
        glBegin(GL_TRIANGLES);
        for( int i_op = 0; i_op < n_ops; i_op++ ) {
            float m = emsc->s_in.count(i_op) ? 0.5f : 1.0f;
            float x0 = (float)(i_op-0.5f) * scale_x;
            float x1 = (float)(i_op+0.5f) * scale_x;
            
            /*float vol_del = evomesh->l_op_volume_deltas[i_op];
            float area_del = evomesh->l_op_post_area[i_op] - evomesh->l_op_pre_area[i_op];
            float norm_del = ( evomesh->l_op_post_stddevnorm[i_op] - evomesh->l_op_pre_stddevnorm[i_op] ) * 100.0f;
            
            r = CLAMP( vol_del + 0.5f, 0.0f, 1.0f );
            g = CLAMP( area_del + 0.5f, 0.0f, 1.0f );
            b = CLAMP( norm_del + 0.5f, 0.0f, 1.0f );
            glColor3f( r*m,g*m,b*m );
            glVertex3f( x0, -0.00005f, 0 );
            glVertex3f( x1, -0.00005f, 0 );
            glVertex3f( x1, -0.00005f, 0.5 );
            glVertex3f( x0, -0.00005f, 0 );
            glVertex3f( x1, -0.00005f, 0.5 );
            glVertex3f( x0, -0.00005f, 0.5 );
            
            r = (r<0.4) ? 0.0 : ( (r>0.6) ? 1.0 : 0.5 );
            g = (g<0.4) ? 0.0 : ( (g>0.6) ? 1.0 : 0.5 );
            b = (b<0.4) ? 0.0 : ( (b>0.6) ? 1.0 : 0.5 );
            glColor3f( r*m,g*m,b*m );
            glVertex3f( x0, -0.00005f, -0.5 );
            glVertex3f( x1, -0.00005f, -0.5 );
            glVertex3f( x1, -0.00005f, 0 );
            glVertex3f( x0, -0.00005f, -0.5 );
            glVertex3f( x1, -0.00005f, 0 );
            glVertex3f( x0, -0.00005f, 0 );*/
            
            glsColor( bc_colors[nodes[i_op]->stats.classification]*m );
            glVertex3f( x0, -0.00005f, -0.5 );
            glVertex3f( x1, -0.00005f, -0.5 );
            glVertex3f( x1, -0.00005f, 0.5 );
            glVertex3f( x0, -0.00005f, -0.5 );
            glVertex3f( x1, -0.00005f, 0.5 );
            glVertex3f( x0, -0.00005f, 0.5 );
        }
        glEnd();
    }
    
    if( glob.stats_brarea ) {
        glLineWidth( 5.0f );
        glColor3f( 0.2, 0.1, 0.2 );
        glBegin(GL_LINE_STRIP);
        for( int i_op = 0; i_op < n_ops; i_op++ ) {
            float da = nodes[i_op]->stats.area_pre * 0.01f;
            float x = (float)i_op * scale_x;
            float y = -0.0001f;
            float z = da;
            
            glVertex3f( x, y, z );
        }
        glEnd();
        glLineWidth(2.0f);
        glColor3f( 0.5, 0.2, 0.5 );
        glBegin(GL_LINE_STRIP);
        for( int i_op = 0; i_op < n_ops; i_op++ ) {
            float da = nodes[i_op]->stats.area_pre * 0.01f;
            float x = (float)i_op * scale_x;
            float y = -0.00015f;
            float z = da;
            
            glVertex3f( x, y, z );
        }
        glEnd();
    }
    
    if( glob.stats_vol ) {
        glLineWidth( 5.0f );
        glColor3f( 0.2, 0.5, 0.5 );
        glBegin(GL_LINE_STRIP);
        for( int i_op = 0; i_op < n_ops; i_op++ ) {
            float v = nodes[i_op]->stats.vol_delta;
            float x = (float)i_op * scale_x;
            float y = -0.0001f;
            float z = v;
            
            glVertex3f( x, y, z );
        }
        glEnd();
        glLineWidth(2.0f);
        glBegin(GL_LINE_STRIP);
        for( int i_op = 0; i_op < n_ops; i_op++ ) {
            float v = nodes[i_op]->stats.vol_delta;
            
            if( v >= 0.0f ) glColor3f( 0.5, 1.0, 1.0 );
            else glColor3f( 0.3, 0.6, 0.6 );
            
            float x = (float)i_op * scale_x;
            float y = -0.0002f;
            float z = v;
            
            glVertex3f( x, y, z );
        }
        glEnd();
    }
    
    if( glob.stats_area ) {
        glLineWidth( 5.0f );
        glColor3f( 0.5, 0.2, 0.5 );
        glBegin(GL_LINE_STRIP);
        for( int i_op = 0; i_op < n_ops; i_op++ ) {
            float da = nodes[i_op]->stats.area_post - nodes[i_op]->stats.area_pre;
            float x = (float)i_op * scale_x;
            float y = -0.0001f;
            float z = da;
            
            glVertex3f( x, y, z );
        }
        glEnd();
        glLineWidth(2.0f);
        glBegin(GL_LINE_STRIP);
        for( int i_op = 0; i_op < n_ops; i_op++ ) {
            float da = nodes[i_op]->stats.area_post - nodes[i_op]->stats.area_pre;
            
            if( da >= 0.0f ) glColor3f( 1, 0.5, 1 );
            else glColor3f( 0.6, 0.3, 0.6 );
            
            float x = (float)i_op * scale_x;
            float y = -0.0003f;
            float z = da;
            
            glVertex3f( x, y, z );
        }
        glEnd();
    }
    
    if( glob.stats_norm ) {
        glLineWidth( 5.0f );
        glColor3f( 0.5, 0.5, 0.2 );
        glBegin(GL_LINE_STRIP);
        for( int i_op = 0; i_op < n_ops; i_op++ ) {
            float da = nodes[i_op]->stats.stddevnorm_post - nodes[i_op]->stats.stddevnorm_pre;
            da *= 100.0f;
            //da = da * ( l_op_post_area[i_op] + l_op_pre_area[i_op] );
            float x = (float)i_op * scale_x;
            float y = -0.0001f;
            float z = da;
            
            glVertex3f( x, y, z );
        }
        glEnd();
        glLineWidth(2.0f);
        glBegin(GL_LINE_STRIP);
        for( int i_op = 0; i_op < n_ops; i_op++ ) {
            float da = nodes[i_op]->stats.stddevnorm_post - nodes[i_op]->stats.stddevnorm_pre;
            da *= 100.0f;
            //da = da * ( l_op_post_area[i_op] + l_op_pre_area[i_op] );
            float x = (float)i_op * scale_x;
            float y = -0.0004f;
            float z = da;
            
            if( da >= 0.0f ) glColor3f( 1.0, 1.0, 0.5 );
            else glColor3f( 0.6, 0.6, 0.3 );
            
            glVertex3f( x, y, z );
        }
        glEnd();
    }
    
    if( true ) {
        // draw breaks of linearity
        glLineWidth( 3.0f );
        glColor3f( 0.5, 0.1, 0.1 );
        glBegin(GL_LINES);
        for( int i_op = 1; i_op < n_ops; i_op++ ) {
            if( nodes[i_op-1]->is_child(nodes[i_op])) continue;
            //if( evomesh->_is_dep_on(i_op,i_op-1) ) continue;
            glVertex3f( ((float)i_op-0.5f)*scale_x, -0.0025f, -0.2f );
            glVertex3f( ((float)i_op-0.5f)*scale_x, -0.0025f, 0.2f );
        }
        glEnd();
        glLineWidth( 1.0f );
        glColor3f( 1.0, 0.2, 0.2 );
        glBegin(GL_LINES);
        for( int i_op = 1; i_op < n_ops; i_op++ ) {
            if( nodes[i_op-1]->is_child(nodes[i_op])) continue;
            //if( evomesh->_is_dep_on(i_op,i_op-1) ) continue;
            glVertex3f( ((float)i_op-0.5f)*scale_x, -0.0025f, -0.2f );
            glVertex3f( ((float)i_op-0.5f)*scale_x, -0.0025f, 0.2f );
        }
        glEnd();
    }
    
    /*if( !n_over ) {
        glLineWidth( 3.0f ); glColor3f( 0.5, 0.5, 0.1 );
        glBegin(GL_LINES);
        glVertex3f( i_over_point*scale_x, -0.0001f, -0.3f - 0.5f*tr_4fps );
        glVertex3f( i_over_point*scale_x, -0.0001f,  0.3f + 0.5f*tr_4fps );
        glEnd();
        glLineWidth( 1.0f ); glColor3f( 1.0, 1.0, 0.2 );
        glBegin(GL_LINES);
        glVertex3f( i_over_point*scale_x, -0.0001f, -0.3f - 0.5f*tr_4fps );
        glVertex3f( i_over_point*scale_x, -0.0001f,  0.3f + 0.5f*tr_4fps );
        glEnd();
    }*/
    
    if(true) {
        glLineWidth( 3.0f ); glColor3f( 0.1, 0.5, 0.5 );
        glBegin(GL_LINES);
        for( int i_n : emsc->l_in ) {
            glVertex3f( i_n*scale_x, -0.0001f, -0.3f - 0.3f*tr_1fps );
            glVertex3f( i_n*scale_x, -0.0001f,  0.3f + 0.3f*tr_1fps );
        }
        glEnd();
        glLineWidth( 1.0f ); glColor3f( 0.2, 1.0, 1.0 );
        glBegin(GL_LINES);
        for( int i_n : emsc->l_in ) {
            glVertex3f( i_n*scale_x, -0.0001f, -0.3f - 0.3f*tr_1fps );
            glVertex3f( i_n*scale_x, -0.0001f,  0.3f + 0.3f*tr_1fps );
        }
        glEnd();
    }
    
    if(false) {
        glColor3f( 0.5, 0.5, 0.1 );
        glLineWidth( 3.0f );
        glBegin(GL_LINES);
        glVertex3f( 0.0f, -0.0001f, -0.3f );
        glVertex3f( 0.0f, -0.0001f, +0.3f );
        glEnd();
        
        glLineWidth( 1.0f );
        glBegin(GL_LINES);
        glColor3f( 0, 0, 0 );
        glVertex3f( 0 * scale_x, -0.0001f, 0.0 );
        glVertex3f( n_ops * scale_x, -0.0001f, 0.0 );
        //glVertex3f( (float)(-idx) / (float)n_ops * scale_x, -0.0001f, offset_z );
        //glVertex3f( (float)(n_ops-idx) * scale_x, -0.0001f, offset_z );
        glColor3f( 1, 1, 0.2 );
        glVertex3f( 0.0f, -0.0001f, -0.3f );
        glVertex3f( 0.0f, -0.0001f, +0.3f );
        glEnd();
    }
    
    glLineWidth( 1.0f );
    glBegin(GL_LINES);
    glColor3f( 0, 0, 0 );
    glVertex3f( 0 * scale_x, -0.0001f, 0.0 );
    glVertex3f( n_ops * scale_x, -0.0001f, 0.0 );
    glColor3f( 0.1, 0.1, 0.1 );
    glVertex3f( 0 * scale_x, -0.0001f, 0.1 );
    glVertex3f( n_ops * scale_x, -0.0001f, 0.1 );
    glVertex3f( 0 * scale_x, -0.0001f, -0.1 );
    glVertex3f( n_ops * scale_x, -0.0001f, -0.1 );
    glEnd();
}















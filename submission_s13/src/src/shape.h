struct Shape;
struct TriangleMesh;
struct ViewPanel;

#ifndef _SHAPE_H_
#define _SHAPE_H_

#include "debug.h"
#include "vec.h"
#include "object.h"
#include "std.h"
#include "gls.h"
#include "geom.h"
#include "ext/common/frame.h"
#include "io_json.h"



typedef void (*fn_apply)(const string &type, Shape &s);


struct Shape : object {
    virtual ~Shape() { }
    
    virtual const string &type() = 0;
    virtual void apply( fn_apply fn ) = 0;
    virtual void apply( const string &type, fn_apply fn ) = 0;
    virtual void apply( const string &type, function<void (const string&,Shape &)> fn ) = 0;
    
    vector<Shape*> get_shapes( const string &type ) {
        vector<Shape*> l_shapes;
        
        apply( type, [&l_shapes](const string &type, Shape &s) {
            l_shapes.push_back(&s);
        } );
        
        return l_shapes;
    }
    
    virtual void init() { }
    
    virtual void draw() { not_implemented_error(); }
};

struct NullShape : Shape {
    string _type = "NullShape";
    
    virtual const string &type() { return _type; }
    virtual void apply( fn_apply fn ) { fn( _type, *this ); }
    virtual void apply( const string &type, fn_apply fn ) {
        if( _type == type ) fn( _type, *this );
    }
    virtual void apply( const string &type, function<void (const string&,Shape &)> fn ) {
        if( _type == type ) fn( _type, *this );
    }
    
    virtual void draw() { }
};


struct TriangleMesh : Shape {
    string _type = "TriangleMesh";
    vector<vec3f> pos;
    vector<vec3f> norm;
    vector<vec2f> uv;
    vector<vec3f> l_vc;
    
    vector<vec2i> edge;
    
    vector<vec3i> face;
    vector<vec3f> l_fc;
    vector<vec3f> l_fn;
    
    virtual TriangleMesh * clone() { return new TriangleMesh(*this); }
    
    virtual const string &type() { return _type; }
    virtual void apply( fn_apply fn ) { fn( _type, *this ); }
    virtual void apply( const string &type, fn_apply fn ) { if( _type == type ) fn( _type, *this ); }
    virtual void apply( const string &type, function<void (const string&,Shape &)> fn ) {
        if( _type == type ) fn( _type, *this );
    }
    
    vec3f face_normal(const vec3i& f) { return triangle_normal(pos[f.x],pos[f.y],pos[f.z]); }
    void update_normals() {
        int n = face.size();
        l_fn.assign(n,zero3f);
        for( int i = 0; i < n; i++ ) {
            vec3i &f = face[i];
            l_fn[i] = triangle_normal(pos[f.x],pos[f.y],pos[f.z]);
        }
    }
    
    void recolor_geo_density( int k=50 );
    void recolor_neutral();
    
    virtual void draw();
    
    void draw_mesh_faces();
    void draw_mesh_lines();
};



struct BrushStroke : Shape {
    string _type = "BrushStroke";
    vector<vec3f> pos;
    
    virtual const string &type() { return _type; }
    virtual void apply( fn_apply fn ) { fn( _type, *this ); }
    virtual void apply( const string &type, fn_apply fn ) { if( _type == type ) fn( _type, *this ); }
    virtual void apply( const string &type, function<void (const string&,Shape &)> fn ) {
        if( _type == type ) fn( _type, *this );
    }
    
    virtual void draw();
};

struct CameraShape : Shape {
    string _type = "Camera";
    
    frame3f frame;
    float dist;
    string perspective;
    
    virtual const string &type() { return _type; }
    virtual void apply( fn_apply fn ) { fn( _type, *this ); }
    virtual void apply( const string &type, fn_apply fn ) { if( _type == type ) fn( _type, *this ); }
    virtual void apply( const string &type, function<void (const string&,Shape &)> fn ) {
        if( _type == type ) fn( _type, *this );
    }
    
    virtual void draw();
};

struct Scene : Shape {
    string _type = "Scene";
    Shape * shape;
    Shape * stroke;
    Shape * camera;
    
    virtual const string &type() { return _type; }
    virtual void apply( fn_apply fn ) {
        fn( _type, *this );
        shape->apply( fn );
        stroke->apply( fn );
        camera->apply( fn );
    }
    virtual void apply( const string &type, fn_apply fn ) {
        if( _type == type ) fn( _type, *this );
        shape->apply( type, fn );
        stroke->apply( type, fn );
        camera->apply( type, fn );
    }
    virtual void apply( const string &type, function<void (const string&,Shape &)> fn ) {
        if( _type == type ) fn( _type, *this );
        shape->apply( type, fn );
        stroke->apply( type, fn );
        camera->apply( type, fn );
    }
    
    virtual void init() { shape->init(); stroke->init(); camera->init(); }
    
    virtual void draw() { shape->draw(); }
};

struct ShapeList : Shape {
    string _type = "ShapeList";
	vector<Shape*> shapes;
	
    virtual const string &type() { return _type; }
    virtual void apply( fn_apply fn ) {
        fn( _type, *this );
        for( auto shape : shapes ) shape->apply(fn);
    }
    virtual void apply( const string &type, fn_apply fn ) {
        if( _type == type ) fn( _type, *this );
        for( auto shape : shapes ) shape->apply( type, fn );
    }
    virtual void apply( const string &type, function<void (const string&,Shape &)> fn ) {
        if( _type == type ) fn( _type, *this );
        for( auto shape : shapes ) shape->apply( type, fn );
    }
    
    virtual void init() { for( auto shape : shapes ) shape->init(); }
    
	virtual void draw() { for( auto shape : shapes ) shape->draw(); }
};

struct SnapshotShape : ShapeList {
    string _type = "SnapshotShape";
    int _i = 0;
    float fps = 30.0;
    float _elapsed = 0.0;
    
    virtual const string &type() { return _type; }
    virtual void apply( fn_apply fn ) {
        fn( _type, *this );
        for( auto shape : shapes ) shape->apply(fn);
    }
    virtual void apply( const string &type, fn_apply fn ) {
        if( _type == type ) fn( _type, *this );
        for( auto shape : shapes ) shape->apply( type, fn );
    }
    virtual void apply( const string &type, function<void (const string&,Shape &)> fn ) {
        if( _type == type ) fn( _type, *this );
        for( auto shape : shapes ) shape->apply( type, fn );
    }
    
    virtual void init() { for( auto shape : shapes ) shape->init(); }
    
    void i_next() { i_set(_i+1); }
    void i_prev() { i_set(_i-1); }
    void i_set( int i ) { int s = shapes.size(); _i = ((i%s)+s)%s; }
    void i_set() { int s = shapes.size(); _i = ((_i%s)+s)%s; }
    void i_set_elapsed( float elapsed ) {
        float d = elapsed - _elapsed;
        int s = (int)(d * fps);
        
        if( s == 0 ) return;
        i_set(_i + s);
        _elapsed = elapsed;
    }
    
	virtual void draw() {
        i_set();
        shapes[_i]->draw();
    }
};

struct TransformedShape : Shape {
    string _type = "TransformedShape";
    Shape *shape;
    frame3f frame;
    
    virtual const string &type() { return _type; }
    virtual void apply( fn_apply fn ) {
        fn( _type, *this );
        shape->apply(fn);
    }
    virtual void apply( const string &type, fn_apply fn ) {
        if( _type == type ) fn( _type, *this );
        shape->apply( type, fn );
    }
    virtual void apply( const string &type, function<void (const string&,Shape &)> fn ) {
        if( _type == type ) fn( _type, *this );
        shape->apply( type, fn );
    }
    
    virtual void init() {
        shape->init();
    }

    virtual void draw() {
        glMatrixMode(GL_MODELVIEW);
        glPushMatrix();
        glsMultMatrix(frame_to_matrix(frame));
        shape->draw();
        glPopMatrix();
    }
};


struct PointSet : Shape {
    string _type = "PointSet";
    vector<vec3f> pos;
    vector<float> radius;
    
    virtual const string &type() { return _type; }
    virtual void apply( fn_apply fn ) { fn( _type, *this ); }
    virtual void apply( const string &type, fn_apply fn ) { if( _type == type ) fn( _type, *this ); }
    virtual void apply( const string &type, function<void (const string&,Shape &)> fn ) {
        if( _type == type ) fn( _type, *this );
    }
    
    virtual PointSet * clone() { return new PointSet(*this); }
    
    virtual void draw();
};

struct LineSet : Shape {
    string _type = "LineSet";
    vector<vec3f> pos;
    vector<vec2i> lines;
    
    virtual const string &type() { return _type; }
    virtual void apply( fn_apply fn ) { fn( _type, *this ); }
    virtual void apply( const string &type, fn_apply fn ) { if( _type == type ) fn( _type, *this ); }
    virtual void apply( const string &type, function<void (const string&,Shape &)> fn ) {
        if( _type == type ) fn( _type, *this );
    }
    
    virtual void draw();
};

struct LineStrip : Shape {
    string _type = "LineStrip";
    vector<vec3f> pos;
    
    virtual const string &type() { return _type; }
    virtual void apply( fn_apply fn ) { fn( _type, *this ); }
    virtual void apply( const string &type, fn_apply fn ) { if( _type == type ) fn( _type, *this ); }
    virtual void apply( const string &type, function<void (const string&,Shape &)> fn ) {
        if( _type == type ) fn( _type, *this );
    }
    
    virtual void draw();
};



Shape * ShapeFromJson( JsonNavigator nav );



#endif

#include "stdio.h"

#include "meshgit_lib/mesh.h"
#include "meshgit_lib/binheap.h"

#include "viewer.h"
#include "panel.h"

namespace MeshGitViews {
    enum Enum {
        None,
        Diff,
        Diff_Explore,
        Diff_Sequence,
        Diff_Compare,
        Diff_Timed,
        Merge,
        Info,
    };
};

VP_PropertyView *pnl_properties = nullptr;
VP_Container *pnl_main = nullptr;



bool  glob_mesh_edges = true;
int   glob_mesh_edges_darkness = 2;
bool  glob_mesh_faces = true;
bool  glob_mesh_faces_chgsonly = false;
bool  glob_mesh_faces_sameonly = false;
bool  glob_mesh_faces_twotoned = false;
bool  glob_mesh_components = false;
bool  glob_overlay_edges = false;
bool  glob_morph_verts = false;
bool  glob_morph_faces = false;
bool  glob_connect_matched_verts = false;
bool  glob_connect_matched_faces = false;

float glob_part_offset = 0.0f;

bool  glob_knn_vert = false;
bool  glob_knn_face = false;

int   glob_patch_size = 10;
float glob_patch_per = 0.1f;
int   glob_exact_knn = 1;
float glob_exact_dist = 0.1f;

vec3f glob_offset;
float glob_scale;


material mat_neutral;
material mat_match, mat_match_;
material mat_unmatch0, mat_unmatch1, mat_unmatch01, mat_unmatch0_, mat_unmatch1_, mat_unmatch01_;
material mat_unmatch0a, mat_unmatch0b, mat_unmatch0ab, mat_unmatcha0, mat_unmatchb0;
material mat_unmatch0a_, mat_unmatch0b_, mat_unmatch0ab_, mat_unmatcha0_, mat_unmatchb0_;



void draw_mesh( Mesh *m, vector<ivert> &vm, vector<iface> &fm, material &mat_unmatch, material &mat_unmatch_ );
void draw_mesh( Mesh *m, Mesh *m_prev, vector<ivert> &vm, vector<iface> &fm, material &mat_unmatch, material &mat_unmatch_ );
void draw_mesh( Mesh *m, vector<ivert> &vm0, vector<iface> &fm0, vector<ivert> &vm1, vector<iface> &fm1, bool ab );
void draw_mesh( Mesh *m, Mesh *m_prev, vector<ivert> &vm0, vector<iface> &fm0, vector<ivert> &vm1, vector<iface> &fm1, bool ab );

void draw_mesh( Mesh *m, vector<ivert> &vm, vector<iface> &fm, vec3f c_e_unm, Mesh *m_, vector<vector<ivert>> &vknn, vector<vector<ivert>> &fknn );


void diff_2way( Viewer *viewer, vector<Mesh*> &meshes );
void diff_2way_explore( Viewer *viewer, vector<Mesh*> &meshes );
void diff_3way( Viewer *viewer, vector<Mesh*> &meshes );
void diff_seq( Viewer *viewer, vector<Mesh*> &meshes );
void diff_compare( Viewer *viewer, vector<Mesh*> &meshes, vector<string> &fn_maps );
void merge( Viewer *viewer, vector<Mesh*> &meshes );


inline void material_setdarker( material &mat, material &mat_) {
    mat_ = mat;
    mat_.diffuse *= 0.5f;
}
inline vec3f get_move_color( vec3f &v0, vec3f &v1, vec3f &color0, vec3f &color1 ) {
    float d = 0.1f / (0.1f + lengthSqr(v0-v1));
    return color0 * d + color1 * (1.0f-d);
}
inline vec3f get_move_color( float dist, vec3f &color0, vec3f &color1 ) {
    float d = 0.1f / (0.1f + dist*dist);
    return color0 * d + color1 * (1.0f-d);
}

inline void set_material( material &mat, material &mat_ ) {
    if( glob_mesh_faces_twotoned ) glsMaterial( mat, mat_ );
    else glsMaterial( mat, mat );
}
inline void set_material( GLenum pname, vec3f c, vec3f c_ ) {
    if( glob_mesh_faces_twotoned ) glsMaterialfv( pname, c, c_ );
    else glsMaterialfv( pname, c, c );
}


inline void draw_face( vector<ivert> &vinds, vec3f &norm, vector<vec3f> &vpos ) {
    int n = vinds.size();
    glsNormal(norm);
    for( int i = 1, i0 = 0; i < n; i++ ) {
        int i1 = i%n, i2 = (i+1)%n;
        glsVertex(vpos[vinds[i0]]);
        glsVertex(vpos[vinds[i1]]);
        glsVertex(vpos[vinds[i2]]);
    }
}

inline void draw_face( vector<ivert> &vinds, vector<vec3f> &vpos ) {
    int n = vinds.size(); if( n < 3 ) return;
    glsNormal( triangle_normal(vpos[vinds[0]],vpos[vinds[1]],vpos[vinds[2]]) );
    for( int i = 1, i0 = 0; i < n; i++ ) {
        int i1 = i%n, i2 = (i+1)%n;
        glsVertex(vpos[vinds[i0]]);
        glsVertex(vpos[vinds[i1]]);
        glsVertex(vpos[vinds[i2]]);
    }
}

inline void draw_face( vector<ivert> &vinds, vec3f &norm, vector<vec3f> &vpos, vector<int> &vm, vector<vec3f> &vpos_, vec3f &c0, vec3f &c0_, vec3f &c1, vec3f &c1_ ) {
    vec3f c_bad = { 1.0, 0.0, 0.0 }, c_bad_ = c_bad * 0.5f;
    int n = vinds.size();
    glsNormal(norm);
    for( int i = 1, i0 = 0; i < n; i++ ) {
        int i1 = i%n, i2 = (i+1)%n;
        vec3f &v0=vpos[vinds[i0]], &v1=vpos[vinds[i1]], &v2=vpos[vinds[i2]];
        int iv0_ = vm[vinds[i0]], iv1_=vm[vinds[i1]], iv2_=vm[vinds[i2]];
        if( iv0_ == -1 ) {
            set_material(GL_DIFFUSE, c_bad, c_bad_ );
        } else {
            vec3f &v0_ = vpos_[iv0_];
            set_material(GL_DIFFUSE, get_move_color(v0,v0_,c0,c1), get_move_color(v0,v0_,c0_,c1_));
        }
        glsVertex(v0);
        
        if( iv1_ == -1 ) {
            set_material(GL_DIFFUSE, c_bad, c_bad_ );
        } else {
            vec3f &v1_ = vpos_[iv1_];
            set_material(GL_DIFFUSE, get_move_color(v1,v1_,c0,c1), get_move_color(v1,v1_,c0_,c1_));
        }
        glsVertex(v1);
        
        if( iv2_ == -1 ) {
            set_material(GL_DIFFUSE, c_bad, c_bad_ );
        } else {
            vec3f &v2_ = vpos_[iv2_];
            set_material(GL_DIFFUSE, get_move_color(v2,v2_,c0,c1), get_move_color(v2,v2_,c0_,c1_));
        }
        glsVertex(v2);
    }
}
inline void draw_face( vector<ivert> &vinds, vec3f &norm, vector<vec3f> &vpos, vector<vec3f> &vpos_, vec3f &c0, vec3f &c0_, vec3f &c1, vec3f &c1_ ) {
    int n = vinds.size();
    glsNormal(norm);
    for( int i = 1, i0 = 0; i < n; i++ ) {
        int i1 = i%n, i2 = (i+1)%n;
        vec3f &v0=vpos[vinds[i0]], &v1=vpos[vinds[i1]], &v2=vpos[vinds[i2]];
        vec3f &v0_=vpos_[vinds[i0]],&v1_=vpos_[vinds[i1]],&v2_=vpos_[vinds[i2]];
        set_material(GL_DIFFUSE, get_move_color(v0,v0_,c0,c1), get_move_color(v0,v0_,c0_,c1_));
        glsVertex(v0);
        set_material(GL_DIFFUSE, get_move_color(v1,v1_,c0,c1), get_move_color(v1,v1_,c0_,c1_));
        glsVertex(v1);
        set_material(GL_DIFFUSE, get_move_color(v2,v2_,c0,c1), get_move_color(v2,v2_,c0_,c1_));
        glsVertex(v2);
    }
}

inline void draw_face( vector<ivert> &vinds, vec3f &norm, vector<vec3f> &vpos, vec3f &offset ) {
    int n = vinds.size();
    glsNormal(norm);
    for( int i = 1, i0 = 0; i < n; i++ ) {
        int i1 = i%n, i2 = (i+1)%n;
        glsVertex(vpos[vinds[i0]]+offset);
        glsVertex(vpos[vinds[i1]]+offset);
        glsVertex(vpos[vinds[i2]]+offset);
    }
}


struct VPCameraSettings {
    string name;
    frame3f f;
    float d;
    float h;
    float l;
    bool ortho;
    Property *prop = nullptr;
    
    VPCameraSettings( FILE *fp ) { from_file(fp); }
    VPCameraSettings( VPCamera *camera, string name ) : name(name) { from_camera(camera); }
    
    void from_file( FILE *fp ) {
        char buf[80]; int c,i;
        for(i=0;;i++) if((buf[i]=fgetc(fp))=='\n') { buf[i]='\0'; break; } name=buf;
        //fscanf(fp,"%d\n",&c); for(i=0;i<c;i++) buf[i]=fgetc(fp); buf[c]='\0'; name=buf;
        fscanf(fp,"%f %f %f\n", &f.o.x, &f.o.y, &f.o.z);
        fscanf(fp,"%f %f %f\n", &f.x.x, &f.x.y, &f.x.z);
        fscanf(fp,"%f %f %f\n", &f.y.x, &f.y.y, &f.y.z);
        fscanf(fp,"%f %f %f\n", &f.z.x, &f.z.y, &f.z.z);
        fscanf(fp,"%f %f %f %d\n", &d, &h, &l, &c); ortho = (c != 0);
    }
    
    void to_file( FILE *fp ) {
        fprintf(fp,"%s\n",name.c_str() );
        //fprintf(fp,"%d\n%s\n",(int)name.length(), name.c_str() );
        fprintf(fp," %f %f %f\n", f.o.x, f.o.y, f.o.z);
        fprintf(fp," %f %f %f\n", f.x.x, f.x.y, f.x.z);
        fprintf(fp," %f %f %f\n", f.y.x, f.y.y, f.y.z);
        fprintf(fp," %f %f %f\n", f.z.x, f.z.y, f.z.z);
        fprintf(fp," %f %f %f %d\n", d, h, l, (ortho?1:0) );
    }
    
    void from_camera( VPCamera *camera ) {
        f = camera->f;
        d = camera->d;
        h = camera->h;
        l = camera->l;
        ortho = camera->orthographic;
    }
    
    void to_camera( VPCamera *camera ) {
        camera->f = f;
        camera->d = d;
        camera->h = h;
        camera->l = l;
        camera->orthographic = ortho;
    }
};

vector<VPCameraSettings*> settings;
void add_camera_properties( VPCamera *camera ) {
    FILE *fp = fopen("cameras.txt","rt");
    if( fp ) {
        int n; fscanf(fp,"%d\n¡",&n);
        dlog.print("Loading %d camera settings", n);
        for( int i = 0; i < n; i++ )
            settings.push_back( new VPCameraSettings(fp) );
        fclose(fp);
    }
    
    auto f_save = []{
        FILE *fp = fopen("cameras.txt","wt");
        fprintf(fp,"%d\n",(int)settings.size());
        for( auto setting : settings ) setting->to_file(fp);
        fclose(fp);
    };
    
    Property *prop_div = new Prop_Text("Saved Settings", TEXT_LEFT);
    
    pnl_properties->add( new Prop_Text("Camera Settings",TEXT_CENTER) );
    string *s_name = new string();
    auto prop_name = new Prop_Bind_String("Name: ", s_name);
    pnl_properties->add( prop_name );
    prop_name->valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-_., <>{}[]!@#$%^&*()";
    pnl_properties->add( new Prop_Button(" Save",nullptr,[s_name,camera,prop_div,&f_save](void*){
        for( auto camset : settings ) {
            if( camset->name == *s_name ) {
                camset->from_camera(camera);
                f_save();
                return;
            }
        }
        
        auto prop_prev = (!settings.empty() ? settings[settings.size()-1]->prop : nullptr );
        auto camset = new VPCameraSettings(camera,*s_name);
        settings.push_back( camset );
        camset->prop = new Prop_Button( to_string(" %s",camset->name.c_str()),nullptr,[camera,camset,s_name,prop_div](void*) {
            camset->to_camera(camera);
            s_name->assign( camset->name );
        });
        
        int insert_at = -1;
        for( int i_prop = 0; i_prop < pnl_properties->properties.size(); i_prop++ ) {
            auto prop_search = pnl_properties->properties[i_prop];
            if( prop_search == prop_div ) insert_at = i_prop+1;
            if( prop_search == prop_prev ) { insert_at = i_prop+1; break; }
        }
        if( insert_at != -1 ) {
            pnl_properties->properties.insert(pnl_properties->properties.begin()+insert_at, camset->prop);
        }
        f_save();
    }));
    pnl_properties->add( new Prop_Button(" Delete",nullptr,[s_name,&f_save](void*){
        Property *prop = nullptr;
        for( int i_camset = 0; i_camset < settings.size(); i_camset++ ) {
            auto camset = settings[i_camset];
            if( camset->name == *s_name ) {
                settings.erase(settings.begin()+i_camset);
                prop = camset->prop;
                delete camset;
                break;
            }
        }
        if( !prop ) return;
        
        int remove_at = -1;
        for( int i_prop = 0; i_prop < pnl_properties->properties.size(); i_prop++ ) {
            auto prop_search = pnl_properties->properties[i_prop];
            if( prop_search == prop ) { remove_at = i_prop; break; }
        }
        if( remove_at != -1 ) {
            pnl_properties->properties.erase(pnl_properties->properties.begin()+remove_at);
        }
        f_save();
        
    }));
    
    pnl_properties->add( prop_div );
    for( VPCameraSettings *camset : settings ) {
        camset->prop = new Prop_Button( to_string(" %s",camset->name.c_str()),nullptr,[camera,camset,s_name](void*) {
            camset->to_camera(camera);
            s_name->assign( camset->name );
        });
        pnl_properties->add( camset->prop );
    }
}



int main( int argc, char **argv ) {
    vector<Mesh*> meshes; vector<string> fn_meshes; vector<string> l_map_fns;
    MeshGitViews::Enum view = MeshGitViews::None;
    
    mat_unmatch0.diffuse   = { 0.90f, 0.10f, 0.10f };
    mat_unmatch1.diffuse   = { 0.10f, 0.90f, 0.10f };
    mat_unmatch01.diffuse  = { 0.90f, 0.40f, 0.10f };
    mat_unmatch0a.diffuse  = { 1.00f, 0.10f, 0.10f }; mat_unmatch0a.ambient = { 1.00f, 1.00f, 1.00f };
    mat_unmatcha0.diffuse  = { 0.10f, 1.00f, 0.10f }; mat_unmatcha0.ambient = { 1.00f, 1.00f, 1.00f };
    mat_unmatch0b.diffuse  = { 0.45f, 0.05f, 0.05f }; mat_unmatch0b.ambient = { 0.70f, 0.70f, 0.70f };
    mat_unmatchb0.diffuse  = { 0.05f, 0.45f, 0.05f }; mat_unmatchb0.ambient = { 0.70f, 0.70f, 0.70f };
    mat_unmatch0ab.diffuse = { 0.90f, 0.90f, 0.10f };
    
    material_setdarker(mat_match,mat_match_);
    material_setdarker(mat_unmatch0, mat_unmatch0_); material_setdarker(mat_unmatch1, mat_unmatch1_);
    material_setdarker(mat_unmatch01, mat_unmatch01_);
    material_setdarker(mat_unmatch0a,mat_unmatch0a_); material_setdarker(mat_unmatcha0,mat_unmatcha0_);
    material_setdarker(mat_unmatch0b,mat_unmatch0b_); material_setdarker(mat_unmatchb0,mat_unmatchb0_);
    material_setdarker(mat_unmatch0ab,mat_unmatch0ab_);
    
    int w=1920, h=1080;
    bool reterror = false;
    bool printhelp = false;
    string window_title;
    
    map<string,MeshGitViews::Enum> map_cmd_view = {
        { "diff",           MeshGitViews::Diff },
        { "diff-sequence",  MeshGitViews::Diff_Sequence },
        { "diff-explore",   MeshGitViews::Diff_Explore },
        { "diff-compare",   MeshGitViews::Diff_Compare },
        { "diff-timed",     MeshGitViews::Diff_Timed },
        { "merge",          MeshGitViews::Merge },
        { "info",           MeshGitViews::Info },
    };
    if( argc >= 2 ) {
        if( map_cmd_view.count(argv[1]) ) {
            view = map_cmd_view[argv[1]];
        } else {
            printf( "Error! Unknown command '%s'\n", argv[1] );
            reterror = true;
        }
    } else {
        printf( "Error! Expected command\n" );
        reterror = true;
    }
    
    switch( view ) {
        case MeshGitViews::None: {
            reterror = true;
        } break;
        case MeshGitViews::Diff: {
            if( argc == 4 ) {
                fn_meshes.push_back(argv[2]);
                fn_meshes.push_back(argv[3]);
                window_title = to_string("MeshGit.Diff2: %s -> %s",argv[2],argv[3]);
            } else if( argc == 5 ) {
                fn_meshes.push_back(argv[2]);
                fn_meshes.push_back(argv[3]);
                fn_meshes.push_back(argv[4]);
                window_title = to_string("MeshGit.Diff3: %s <- %s -> %s",argv[3],argv[2],argv[4]);
            } else {
                printf( "Error! Expected either 2 or 3 mesh files\n" );
            }
        } break;
        case MeshGitViews::Diff_Explore: {
            if( argc == 4 ) {
                fn_meshes.push_back(argv[2]);
                fn_meshes.push_back(argv[3]);
                window_title = to_string("MeshGit.Diff_Explore: %s -> %s",argv[2],argv[3]);
            } else {
                printf( "Error! Expected 2 mesh files\n" );
            }
        } break;
        case MeshGitViews::Diff_Sequence: {
            if( argc >= 4 ) {
                window_title = "";
                for( int i = 2; i < argc; i++ ) {
                    fn_meshes.push_back(argv[i]);
                    if( window_title.length() ) {
                        window_title = window_title + ", " + argv[i];
                    } else {
                        window_title = argv[i];
                    }
                }
                window_title = "MeshGit.Diff_Sequence: " + window_title;
            } else {
                printf( "Error! Expected at least 2 mesh files\n" );
            }
        } break;
        case MeshGitViews::Diff_Compare: {
            if( argc >= 5 ) {
                fn_meshes.push_back(argv[2]);
                fn_meshes.push_back(argv[3]);
                window_title = to_string("MeshGit.Diff_Compare: %s -> %s",argv[2],argv[3]);
                for( int i = 4; i < argc; i++ ) {
                    l_map_fns.push_back( argv[i] );
                }
            } else {
                printf( "Error! Expected 2 mesh files and at least 1 map filename\n" );
            }
        } break;
        case MeshGitViews::Diff_Timed: {
            if( argc == 4 ) {
                fn_meshes.push_back(argv[2]);
                fn_meshes.push_back(argv[3]);
                window_title = "";
            } else {
                printf( "Error! Expected 2 mesh files\n" );
            }
        } break;
        case MeshGitViews::Merge: {
            if( argc == 5 ) {
                fn_meshes.push_back(argv[2]);
                fn_meshes.push_back(argv[3]);
                fn_meshes.push_back(argv[4]);
                window_title = to_string("MeshGit.Merge: %s <- %s -> %s",argv[3],argv[2],argv[4]);
            } else {
                printf( "Error! Expected 3 mesh files\n" );
            }
        } break;
        case MeshGitViews::Info: {
            window_title = "";
            for( int i = 2; i < argc; i++ ) {
                fn_meshes.push_back(argv[i]);
                if( window_title.length() ) {
                    window_title = window_title + ", " + argv[i];
                } else {
                    window_title = argv[i];
                }
            }
        } break;
        default: { assert(false); } break;
    }
    
    if( printhelp || reterror ) {
        printf( "Usage: %s <command> <mesh0.ply> <mesh1.ply> <...>\n", argv[0] );
        printf( "    Loads given meshes (as .ply files) and performs given command\n" );
        printf( "Commands\n" );
        printf( "  diff <0.ply> <1.ply>\n" );
        printf( "    Performs 2-way diff\n" );
        printf( "  diff <0.ply> <a.ply> <b.ply>\n" );
        printf( "    Performs 3-way diff\n" );
        printf( "  diff-sequence <0.ply> <1.ply> <...>\n" );
        printf( "    Performs a sequence diff.  Same as 2-way diff if given only two mesh files\n");
        printf( "  diff-compare <0.ply> <1.ply> <comp0.map> <...>\n" );
        printf( "    Compares different matching algorithms\n" );
        printf( "  diff-timed <0.ply> <1.ply>\n" );
        printf( "    Reports timing for computing a 2-way diff\n" );
        printf( "  merge <0.ply> <a.ply> <b.ply>\n" );
        printf( "    Merges changes 0->a and 0->b onto 0\n" );
        printf( "  info <0.ply> <...>\n" );
        printf( "    Dumps information on mesh(es) to terminal\n" );
        exit( (reterror ? 1 : 0) );
    }
    
    
    dlog.start("Loading and initializing meshes");
    for( string &fn : fn_meshes ) meshes.push_back( new Mesh(fn) );
    MeshMatch::init_meshes(meshes, glob_offset, glob_scale);
    dlog.end();
    
    if( view == MeshGitViews::Info ) {
        dlog.start("Mesh Information");
        int n_meshes = meshes.size();
        for( int i_mesh = 0; i_mesh < n_meshes; i_mesh++ ) {
            auto mesh = meshes[i_mesh];
            dlog.start( "Mesh: %s", mesh->filename.c_str() );
            
            dlog.start(" Counts" );
            map<int,const char*> ngon_names = { {1,"verts"}, {2,"edges"}, {3," tris"}, {4,"quads"}, {5,"5gons"}, {6,"6gons"} };
            
            map<int,int> ngon;
            //ngon[1] = mesh->n_v;
            for( auto &inds : mesh->finds ) ngon[inds.size()] += 1;
            
            dlog.print( "verts: % 7d", mesh->n_v );
            dlog.start( "ngons: % 7d", mesh->n_f );
            for( auto c : ngon ) dlog.print( "%s: % 7d", ngon_names[c.first], c.second );
            dlog.end_quiet();
            dlog.print( "comps: % 7d", mesh->components.size() );
            
            dlog.end_quiet();
            dlog.end_quiet();
        }
        dlog.end_quiet();
        return 0;
    }
    
    if( view == MeshGitViews::Diff_Timed ) {
        dlog.start("Diff Timing");
        auto mm = new MeshMatch(meshes[0],meshes[1]);
        mm->algorithm();
        dlog.end();
        return 0;
    }
    
    
    
    glutInit( &argc, argv );
    
    h = ( glutGet(GLUT_SCREEN_WIDTH)>2200 ? 1080 : 720 );
    if( h == 1080 ) { w = 1920; }
    else if( h == 720 ) { w = 960; }
    else w = h;
    
    
    
    
    // setup window
    
    // TODO: UPDATE!!!!
    auto pnl_help = new VP_ScrollTextView();
    pnl_help->s_display = {
        "Help Menu",
        "",
        "General",
        "  esc     - exit",
        "  f1      - toggle help menu",
        "  f2      - save screenshot",
        "   +shift - screenshot with alpha=0 for background",
        "  f11     - toggle properties",
        "  f12     - toggle layout: maximize panel under mouse / default",
        "",
        "Properties Panel",
        "  checkbox",
        "    lmb   - toggle value: on/off",
        "  enumeration",
        "    lmb   - change to next",
        "    rmb   - change to prev",
        "  float / integer",
        "    lmb   - change value",
        "    home  - set to minimum value",
        "    end   - set to maximum value",
        "  button",
        "    lmb   - execute"
        "",
        "3D View",
        "",
        "General Camera Commands (if applicable)",
        "  mmb     - rotate (turntable)",
        "   +shift - pan",
        "   +ctrl  - dolly / zoom",
        "  .       - center camera to origin",
        "   +shift - fly to origin",
        "  1/3/7   - front / right / top",
        "   +shift - back / left / bottom",
        "  5       - toggle projection: perspective / orthographic",
    };
    pnl_help->background = pbg_DarkGray;
    
    pnl_properties = new VP_PropertyView();
    pnl_properties->name = "properties";
    pnl_properties->background = pbg_DarkGray;
    
    pnl_main = new VP_Container();
    //auto pnl_viewprops = new VP_Split( "main", pnl_main, pnl_properties, 179, SPLIT_RIGHT );
    
    Viewer *viewer = new Viewer( &argc, argv, window_title, pnl_main, pnl_properties, pnl_help, w, h );
    viewer->fn_idle = [](void *data) { };
    
    switch( view ) {
        case MeshGitViews::None: {
            reterror = true;
        } break;
        case MeshGitViews::Diff_Explore: {
            diff_2way_explore( viewer, meshes );
        } break;
        case MeshGitViews::Diff: {
            if( meshes.size() == 2 ) diff_2way( viewer, meshes );
            else diff_3way( viewer, meshes );
        } break;
        case MeshGitViews::Diff_Sequence: {
            diff_seq( viewer, meshes );
        } break;
        case MeshGitViews::Diff_Compare: {
            diff_compare( viewer, meshes, l_map_fns );
        } break;
        case MeshGitViews::Merge: {
            merge( viewer, meshes );
        } break;
        default: { assert(false); } break;
    }
    
    return 0;
}



void diff_2way_explore( Viewer *viewer, vector<Mesh*> &meshes ) {
    Mesh *m0 = meshes[0];
    Mesh *m1 = meshes[1];
    
    dlog.start("Creating initial mesh matching");
    auto mm = new MeshMatch(m0,m1);
    mm->assign_exact( 1, 0.01f );
    mm->cleanup();
    dlog.end();
    
    
    VPCamera *camera = new VPCamera();
    camera->set_default_lights();
    camera->lookat( makevec3f(30,-50,30), makevec3f(0,0,0), makevec3f(0,0,1));
    camera->d = 0.2f;
    
    
    auto pnl_3dview0 = new VP_CallBacks();
    pnl_3dview0->name = "mesh0";
    pnl_3dview0->background = pbg_White;// Maya;
    pnl_3dview0->data = mm;
    pnl_3dview0->camera = camera;
    pnl_3dview0->s_display = {
        to_string("File: %s",m0->filename.c_str()),
        to_string("Counts: %d %d", (int)m0->n_v, (int)m0->n_f )
    };
    pnl_3dview0->fndraw = [](VP_CallBacks *vp) {
        MeshMatch *mm = (MeshMatch*)vp->data;
        draw_mesh( mm->m0, mm->vm01, mm->fm01, mat_unmatch0, mat_unmatch0_ );
        //draw_mesh( mm->m0, mm->vm01, mm->fm01, { 0.5f, 0.05f, 0.05f }, mm->m1, mm->v0_knn1, mm->f0_knn1 );
    };
    
    auto pnl_3dview1 = new VP_CallBacks();
    pnl_3dview1->name = "mesh1";
    pnl_3dview1->background = pbg_White;
    pnl_3dview1->data = mm;
    pnl_3dview1->camera = camera;
    pnl_3dview1->s_display = {
        to_string("File: %s",m1->filename.c_str()),
        to_string("Counts: %d %d", (int)m1->n_v, (int)m1->n_f )
    };
    pnl_3dview1->fndraw = [](VP_CallBacks *vp) {
        MeshMatch *mm = (MeshMatch*)vp->data;
        //draw_mesh( mm->m1, mm->vm10, mm->fm10, { 0.05f, 0.5f, 0.05f }, mm->m0, mm->v1_knn0, mm->f1_knn0 );
        draw_mesh( mm->m1, mm->m0, mm->vm10, mm->fm10, mat_unmatch1, mat_unmatch1_ );
    };
    
    
    
    pnl_properties->add( new Prop_Text("View Options",TEXT_CENTER));
    pnl_properties->add( new Prop_Bind_Float("Lens", &camera->d, 0.001f, 1.0f ));
    bool b_drawtext = true;
    auto prop_drawtext = new Prop_Bind_Bool("Text", &b_drawtext);
    prop_drawtext->lfn_change = [&](void*) {
        pnl_3dview0->b_display = pnl_3dview1->b_display = b_drawtext;
    };
    pnl_properties->add( prop_drawtext );
    pnl_properties->add( new Prop_Text("Mesh",TEXT_LEFT));
    pnl_properties->add( new Prop_Bind_Bool(" Edges", &glob_mesh_edges));
    pnl_properties->add( new Prop_Bind_Int("  Darkness", &glob_mesh_edges_darkness, 1, 5));
    pnl_properties->add( new Prop_Bind_Bool(" Faces", &glob_mesh_faces));
    pnl_properties->add( new Prop_Bind_Bool("  Same Only", &glob_mesh_faces_sameonly));
    pnl_properties->add( new Prop_Bind_Bool("  Changes Only", &glob_mesh_faces_chgsonly));
    pnl_properties->add( new Prop_Bind_Bool("  Two-Toned", &glob_mesh_faces_twotoned));
    pnl_properties->add( new Prop_Bind_Bool(" Components", &glob_mesh_components));
    pnl_properties->add( new Prop_Bind_Float(" Part Offset", &glob_part_offset, 0.0f, 10.0f));
    pnl_properties->add( new Prop_Text("Overlay",TEXT_LEFT));
    pnl_properties->add( new Prop_Bind_Bool(" Edges", &glob_overlay_edges));
    pnl_properties->add( new Prop_Bind_Bool(" VKNN", &glob_knn_vert));
    pnl_properties->add( new Prop_Bind_Bool(" FKNN", &glob_knn_face));
    pnl_properties->add( new Prop_Text("Matches",TEXT_LEFT));
    pnl_properties->add( new Prop_Bind_Bool(" V0-V1", &glob_connect_matched_verts));
    pnl_properties->add( new Prop_Bind_Bool(" F0-F1", &glob_connect_matched_faces));
    pnl_properties->add( new Prop_Bind_Bool(" Morph Verts", &glob_morph_verts));
    pnl_properties->add( new Prop_Bind_Bool(" Morph Faces", &glob_morph_faces));
    pnl_properties->add( new Prop_Divider(' ') );
    
    add_camera_properties( camera );
    
    pnl_properties->add( new Prop_Divider('_') );
    pnl_properties->add( new Prop_Text("Matches",TEXT_CENTER) );
    pnl_properties->add( new Prop_Divider('-') );
    pnl_properties->add( new Prop_Divider(' ') );
    
    pnl_properties->add( new Prop_Text("Logic Algorithms",TEXT_CENTER) );
    pnl_properties->add( new Prop_Button("Reset", mm, [](void*data) { ((MeshMatch*)data)->reset(); } ));
    pnl_properties->add( new Prop_Button("- Face:Patches", mm, [](void*data) { ((MeshMatch*)data)->unassign_small_patches(glob_patch_size); }));
    pnl_properties->add( new Prop_Bind_Int(" size", &glob_patch_size, 1, 1000 ));
    pnl_properties->add( new Prop_Button("- Face:Patches", mm, [](void*data) { ((MeshMatch*)data)->unassign_small_patches(glob_patch_per); }));
    pnl_properties->add( new Prop_Bind_Float(" per", &glob_patch_per, 0.0f, 1.0f ));
    pnl_properties->add( new Prop_Button("- Face:Twisted", mm, [](void*data) { ((MeshMatch*)data)->unassign_twisted(); }));
    pnl_properties->add( new Prop_Button("- Face:Any Mis", mm, [](void*data) { ((MeshMatch*)data)->unassign_mismatched(); }));
    pnl_properties->add( new Prop_Button("- Face:Any Unm", mm, [](void*data) { ((MeshMatch*)data)->unassign_faces_with_unmatched_verts(); }));
    pnl_properties->add( new Prop_Button("- Vert:All Unm", mm, [](void*data) { ((MeshMatch*)data)->unassign_verts_with_all_unmatched_faces(); }));
    pnl_properties->add( new Prop_Button("- Vert:Any Unm", mm, [](void*data) { ((MeshMatch*)data)->unassign_verts_with_any_unmatched_faces(); }));
    pnl_properties->add( new Prop_Button("+ All:Exact", mm, [](void*data) { ((MeshMatch*)data)->assign_no_geo_cost(); })); //(glob_exact_knn,glob_exact_dist); }));
    pnl_properties->add( new Prop_Bind_Int(" knn", &glob_exact_knn, 1, 100 ));
    pnl_properties->add( new Prop_Bind_Float(" dist", &glob_exact_dist, 0.01f, 1.0f ));
    pnl_properties->add( new Prop_Button("Clean Up", mm, [](void*data) { ((MeshMatch*)data)->cleanup(); }));
    pnl_properties->add( new Prop_Divider(' ') );
    
    pnl_properties->add( new Prop_Text("Greedy Algorithm",TEXT_CENTER) );
    pnl_properties->add( new Prop_Button("Set0",mm,[](void*data){
        auto mm = (MeshMatch*)data;
        mm->costs.alpha = 2.5f;
        mm->costs.dist = 1.0f;
        mm->costs.norm = 1.0f;
        mm->costs.unm = 0.8f;
        mm->costs.mis = 3.5f;
        mm->costs.valence = 0.0f;
        mm->costs.stretch = 0.0f;
        mm->costs.knn = 20;
        mm->costs.srball = 1;
        mm->costs.expand_knn = false;
    }));
    float a = 1.0, b = 0.08;
    for( int iter = 0; iter < 5; iter++ ) {
        pnl_properties->add( new Prop_Button(to_string("iter%d",iter),nullptr,[iter,a,b,&mm](void*){
            mm->costs.alpha = 1.0f;
            mm->costs.dist = mm->costs.norm = a;
            mm->costs.unm = 1.0f;
            mm->costs.mis = 1.0f;
            mm->costs.valence = 0.0f;
            mm->costs.stretch = 1.0f;
            mm->costs.knn = 10;
            mm->costs.srball = 2;
            mm->costs.expand_knn = true;
            glob_patch_per = b;
        }));
        a *= 0.75f;
        b *= 0.50f;
    }
    pnl_properties->add( new Prop_Text("Parameters",TEXT_LEFT));
    pnl_properties->add( new Prop_Bind_Float(" add/del", &mm->costs.alpha, 0.0, 10.0 ));
    pnl_properties->add( new Prop_Bind_Float(" dist", &mm->costs.dist, 0.0, 10.0 ));
    pnl_properties->add( new Prop_Bind_Float("  alpha", &mm->costs.dist_mult, 0.0, 1.0 ));
    pnl_properties->add( new Prop_Bind_Float(" norm", &mm->costs.norm, 0.0, 10.0 ));
    pnl_properties->add( new Prop_Bind_Float("  alpha", &mm->costs.norm_mult, 0.0, 1.0 ));
    pnl_properties->add( new Prop_Bind_Float(" unmatch", &mm->costs.unm, 0.0, 10.0 ));
    pnl_properties->add( new Prop_Bind_Float(" unmatch", &mm->costs.mis, 0.0, 10.0 ));
    pnl_properties->add( new Prop_Bind_Float(" valence", &mm->costs.valence, 0.0, 2.0));
    pnl_properties->add( new Prop_Bind_Float(" stretch", &mm->costs.stretch, 0.0, 10.0));
    pnl_properties->add( new Prop_Bind_Int(" knn", &mm->costs.knn, 0, 100));
    pnl_properties->add( new Prop_Bind_Int(" srball", &mm->costs.srball, 0, 10));
    pnl_properties->add( new Prop_Bind_Bool(" expand knn", &mm->costs.expand_knn ) );
    pnl_properties->add( new Prop_Button("Run", mm, [](void*data) { ((MeshMatch*)data)->assign_greedy(); } ));
    pnl_properties->add( new Prop_Button("Step", mm, [](void*data) { ((MeshMatch*)data)->assign_greedy_step(); } ));
    pnl_properties->add( new Prop_Button("Cleanup", nullptr, [&](void*) { mm->cleanup(); }));
    pnl_properties->add( new Prop_Button("Step+Cleanup", nullptr, [&](void*) {
        mm->assign_greedy_step();
        mm->cleanup();
    } ));
    pnl_properties->add( new Prop_Button("Cleanup", nullptr, [&](void*) { mm->cleanup(); }));
    pnl_properties->add( new Prop_Button("Backtrack", mm, [](void*data) { ((MeshMatch*)data)->greedy_backtrack(glob_patch_per); } ));
    pnl_properties->add( new Prop_Button("Reset+Run", mm, [](void*data) {
        MeshMatch *mm = (MeshMatch*)data;
        mm->reset();
        mm->assign_greedy();
    }));
    pnl_properties->add( new Prop_Button("Reset+Step", mm, [](void*data) {
        MeshMatch *mm = (MeshMatch*)data;
        mm->reset();
        mm->assign_greedy_step();
    }));
    pnl_properties->add( new Prop_Divider(' ') );
    
    
    
    pnl_main->panel = new VP_Divide("3dviews", pnl_3dview0, pnl_3dview1, 0.5f, DivideType::LEFTRIGHT, true);
    
    viewer->run();
}


void diff_2way( Viewer *viewer, vector<Mesh*> &meshes ) {
    Mesh *m0 = meshes[0];
    Mesh *m1 = meshes[1];
    
    
    dlog.start("Mesh matching");
    auto mm = new MeshMatch(m0,m1);
    mm->algorithm();
    dlog.end();
    
    int num_del_faces = 0, num_add_faces = 0;
    dlog.start("Partitioning");
    vector<MeshMatchPartition*> l_mmps = MeshMatchPartition::partition(mm);
    vector<vec3f> l_mmp_norms0, l_mmp_norms1;
    for( auto mmp : l_mmps ) {
        vec3f n0 = zero3f, n1 = zero3f;
        for( int i_f0 : mmp->sif0 ) n0 += m0->fnorms[i_f0];
        n0 = normalize(n0);
        if( n0%n0<0.1f ) n0 = { 1,0,0 };
        l_mmp_norms0.push_back(n0);
        num_del_faces += mmp->sif0.size();
        
        for( int i_f1 : mmp->sif1 ) n1 += m1->fnorms[i_f1];
        n1 = normalize(n1);
        if( n1%n1<0.1f ) n1 = { 1,0,0 };
        l_mmp_norms1.push_back(n1);
        num_add_faces += mmp->sif1.size();
    }
    dlog.end();
    
    
    VPCamera *camera = new VPCamera();
    camera->set_default_lights();
    camera->lookat( makevec3f(30,-50,30), makevec3f(0,0,0), makevec3f(0,0,1));
    camera->d = 0.2f;
    
    
    auto pnl_3dview0 = new VP_CallBacks();
    pnl_3dview0->name = "mesh0";
    pnl_3dview0->background = pbg_White;// Maya;
    pnl_3dview0->data = mm;
    pnl_3dview0->camera = camera;
    pnl_3dview0->s_display = {
        to_string("File: %s",m0->filename.c_str()),
        to_string("Counts: %d verts, %d faces", (int)m0->n_v, (int)m0->n_f ),
        to_string("Changes: %d parts, -%d faces", (int)l_mmps.size(), num_del_faces),
    };
    pnl_3dview0->lfn_draw = [&]{
        draw_mesh( mm->m0, mm->vm01, mm->fm01, mat_unmatch0, mat_unmatch0_ );
        
        if( glob_part_offset > 0.0f ) {
            for( int i = 0; i < l_mmps.size(); i++ ) {
                auto mmp = l_mmps[i];
                auto n0 = l_mmp_norms0[i] * glob_part_offset;
                
                glBegin(GL_TRIANGLES);
                set_material(mat_unmatch0,mat_unmatch0_);
                for( int i_f0 : mmp->sif0 ) draw_face( m0->finds[i_f0], m0->fnorms[i_f0], m0->vpos, n0 );
                glEnd();
                
                glDisable(GL_LIGHTING);
                glBegin(GL_LINES);
                glColor3f( 0.7f, 0.7f, 0.7f );
                for( int i_v0 : mmp->siv0_border ) {
                    glsVertex(m0->vpos[i_v0]);
                    glsVertex(m0->vpos[i_v0]+n0);
                }
                glEnd();
                glEnable(GL_LIGHTING);
            }
        }
    };
    
    auto pnl_3dview1 = new VP_CallBacks();
    pnl_3dview1->name = "mesh1";
    pnl_3dview1->background = pbg_White;
    pnl_3dview1->data = mm;
    pnl_3dview1->camera = camera;
    pnl_3dview1->s_display = {
        to_string("File: %s",m1->filename.c_str()),
        to_string("Counts: %d verts, %d faces", (int)m1->n_v, (int)m1->n_f ),
        to_string("Changes: %d parts, +%d faces", (int)l_mmps.size(), num_add_faces),
    };
    pnl_3dview1->lfn_draw = [&]{
        draw_mesh( mm->m1, mm->m0, mm->vm10, mm->fm10, mat_unmatch1, mat_unmatch1_ );
        
        if( glob_part_offset > 0.0f ) {
            for( int i = 0; i < l_mmps.size(); i++ ) {
                auto mmp = l_mmps[i];
                auto n1 = l_mmp_norms1[i] * glob_part_offset;
                
                glBegin(GL_TRIANGLES);
                set_material(mat_unmatch1,mat_unmatch1_);
                for( int i_f1 : mmp->sif1 ) draw_face( m1->finds[i_f1], m1->fnorms[i_f1], m1->vpos, n1 );
                glEnd();
                
                glDisable(GL_LIGHTING);
                glBegin(GL_LINES);
                glColor3f( 0.7f, 0.7f, 0.7f );
                for( int i_v1 : mmp->siv1_border ) {
                    glsVertex(m1->vpos[i_v1]);
                    glsVertex(m1->vpos[i_v1]+n1);
                }
                glEnd();
                glEnable(GL_LIGHTING);
            }
        }
    };
    
    pnl_main->panel = new VP_Divide("3dviews", pnl_3dview0, pnl_3dview1, 0.5f, DivideType::LEFTRIGHT, true);
    
    
    pnl_properties->add( new Prop_Text("View Options",TEXT_CENTER));
    pnl_properties->add( new Prop_Bind_Float("Lens", &camera->d, 0.001f, 1.0f ));
    bool b_drawtext = true;
    auto prop_drawtext = new Prop_Bind_Bool("Text", &b_drawtext);
    prop_drawtext->lfn_change = [&](void*) {
        pnl_3dview0->b_display = pnl_3dview1->b_display = b_drawtext;
    };
    pnl_properties->add( prop_drawtext );
    pnl_properties->add( new Prop_Text("Mesh",TEXT_LEFT));
    pnl_properties->add( new Prop_Bind_Bool(" Edges", &glob_mesh_edges));
    pnl_properties->add( new Prop_Bind_Int("  Darkness", &glob_mesh_edges_darkness, 1, 5));
    pnl_properties->add( new Prop_Bind_Bool(" Faces", &glob_mesh_faces));
    pnl_properties->add( new Prop_Bind_Bool("  Same Only", &glob_mesh_faces_sameonly));
    pnl_properties->add( new Prop_Bind_Bool("  Changes Only", &glob_mesh_faces_chgsonly));
    pnl_properties->add( new Prop_Bind_Bool("  Two-Toned", &glob_mesh_faces_twotoned));
    pnl_properties->add( new Prop_Bind_Float(" Part Offset", &glob_part_offset, 0.0f, 10.0f));
    
    pnl_properties->add( new Prop_Divider() );
    add_camera_properties( camera );
    
    
    viewer->run();
}

void diff_3way( Viewer *viewer, vector<Mesh*> &meshes ) {
    Mesh *m0 = meshes[0];
    Mesh *ma = meshes[1];
    Mesh *mb = meshes[2];
    
    dlog.start("Mesh matching");
    auto mm0a = new MeshMatch(m0,ma);
    auto mm0b = new MeshMatch(m0,mb);
    mm0a->algorithm();
    mm0b->algorithm();
    dlog.end();
    
    vector<MeshMatch*> l_mm = { mm0a, mm0b };
    
    dlog.start("Partitioning");
    int num_del_faces0a = 0, num_add_faces0a = 0;
    vector<MeshMatchPartition*> l_mmps0a = MeshMatchPartition::partition(mm0a);
    vector<vec3f> l_mmp0a_norms0, l_mmp0a_norms1;
    for( auto mmp : l_mmps0a ) {
        vec3f n0 = zero3f, n1 = zero3f;
        for( int i_f0 : mmp->sif0 ) n0 += m0->fnorms[i_f0];
        n0 = normalize(n0);
        if( n0%n0<0.1f ) n0 = { 1,0,0 };
        l_mmp0a_norms0.push_back(n0);
        num_del_faces0a += mmp->sif0.size();
        
        for( int i_f1 : mmp->sif1 ) n1 += ma->fnorms[i_f1];
        n1 = normalize(n1);
        if( n1%n1<0.1f ) n1 = { 1,0,0 };
        l_mmp0a_norms1.push_back(n1);
        num_add_faces0a += mmp->sif1.size();
    }
    
    int num_del_faces0b = 0, num_add_faces0b = 0;
    vector<MeshMatchPartition*> l_mmps0b = MeshMatchPartition::partition(mm0b);
    vector<vec3f> l_mmp0b_norms0, l_mmp0b_norms1;
    for( auto mmp : l_mmps0b ) {
        vec3f n0 = zero3f, n1 = zero3f;
        for( int i_f0 : mmp->sif0 ) n0 += m0->fnorms[i_f0];
        n0 = normalize(n0);
        if( n0%n0<0.1f ) n0 = { 1,0,0 };
        l_mmp0b_norms0.push_back(n0);
        num_del_faces0b += mmp->sif0.size();
        
        for( int i_f1 : mmp->sif1 ) n1 += mb->fnorms[i_f1];
        n1 = normalize(n1);
        if( n1%n1<0.1f ) n1 = { 1,0,0 };
        l_mmp0b_norms1.push_back(n1);
        num_add_faces0b += mmp->sif1.size();
    }
    dlog.end();
    
    
    VPCamera *camera = new VPCamera();
    camera->set_default_lights();
    camera->lookat( makevec3f(30,-50,30), makevec3f(0,0,0), makevec3f(0,0,1));
    camera->d = 0.2f;
    
    
    
    auto pnl_3dview0 = new VP_CallBacks();
    pnl_3dview0->name = "mesh0";
    pnl_3dview0->background = pbg_White;// Maya;
    pnl_3dview0->data = &l_mm;
    pnl_3dview0->camera = camera;
    pnl_3dview0->s_display = {
        to_string("File: %s",m0->filename.c_str()),
        to_string("Counts: %d verts, %d faces", (int)m0->n_v, (int)m0->n_f ),
        to_string("Changes: %d,%d parts, -%d,-%d faces", (int)l_mmps0a.size(), (int)l_mmps0b.size(), num_del_faces0a, num_del_faces0b),
    };
    pnl_3dview0->lfn_draw = [&] {
        draw_mesh( mm0a->m0, mm0a->vm01, mm0a->fm01, mm0b->vm01, mm0b->fm01, true );
        
        if( glob_part_offset > 0.0f ) {
            glBegin(GL_TRIANGLES);
            for( int i = 0; i < l_mmps0a.size(); i++ ) {
                auto mmp = l_mmps0a[i];
                auto n0 = l_mmp0a_norms0[i] * glob_part_offset;
                set_material(mat_unmatch0a,mat_unmatch0a_);
                for( int i_f0 : mmp->sif0 ) draw_face( m0->finds[i_f0], m0->fnorms[i_f0], m0->vpos, n0 );
            }
            for( int i = 0; i < l_mmps0b.size(); i++ ) {
                auto mmp = l_mmps0b[i];
                auto n0 = l_mmp0b_norms0[i] * glob_part_offset * 2.0f;
                set_material(mat_unmatch0b,mat_unmatch0b_);
                for( int i_f0 : mmp->sif0 ) draw_face( m0->finds[i_f0], m0->fnorms[i_f0], m0->vpos, n0 );
            }
            glEnd();
            
            glDisable(GL_LIGHTING);
            glBegin(GL_LINES);
            glColor4f( 0.7f, 0.7f, 0.7f, 0.2f );
            for( int i = 0; i < l_mmps0a.size(); i++ ) {
                auto mmp = l_mmps0a[i];
                auto n0 = l_mmp0a_norms0[i] * glob_part_offset;
                for( int i_v0 : mmp->siv0_border ) {
                    glsVertex(m0->vpos[i_v0]);
                    glsVertex(m0->vpos[i_v0]+n0);
                }
            }
            
            for( int i = 0; i < l_mmps0b.size(); i++ ) {
                auto mmp = l_mmps0b[i];
                auto n0 = l_mmp0b_norms0[i] * glob_part_offset * 2.0f;
                for( int i_v0 : mmp->siv0_border ) {
                    glsVertex(m0->vpos[i_v0]);
                    glsVertex(m0->vpos[i_v0]+n0);
                }
            }
            glEnd();
            glEnable(GL_LIGHTING);
        }
    };
    
    auto pnl_3dviewa = new VP_CallBacks();
    pnl_3dviewa->name = "mesha";
    pnl_3dviewa->background = pbg_White;
    pnl_3dviewa->data = &l_mm;
    pnl_3dviewa->camera = camera;
    pnl_3dviewa->s_display = {
        to_string("File: %s",ma->filename.c_str()),
        to_string("Counts: %d verts, %d faces", (int)ma->n_v, (int)ma->n_f ),
        to_string("Changes: %d parts, +%d faces", (int)l_mmps0a.size(), num_add_faces0a),
    };
    pnl_3dviewa->lfn_draw = [&] {
        MeshMatch *mm = mm0a;
        draw_mesh( mm->m1, mm->m0, mm->vm10, mm->fm10, mat_unmatcha0, mat_unmatcha0_ );
        
        if( glob_part_offset > 0.0f ) {
            glBegin(GL_TRIANGLES);
            for( int i = 0; i < l_mmps0a.size(); i++ ) {
                auto mmp = l_mmps0a[i];
                auto n1 = l_mmp0a_norms1[i] * glob_part_offset;
                set_material(mat_unmatcha0,mat_unmatcha0_);
                for( int i_f1 : mmp->sif1 ) draw_face( ma->finds[i_f1], ma->fnorms[i_f1], ma->vpos, n1 );
            }
            glEnd();
            
            glDisable(GL_LIGHTING);
            glBegin(GL_LINES);
            glColor4f( 0.7f, 0.7f, 0.7f, 0.2f );
            for( int i = 0; i < l_mmps0a.size(); i++ ) {
                auto mmp = l_mmps0a[i];
                auto n1 = l_mmp0a_norms1[i] * glob_part_offset;
                for( int i_v1 : mmp->siv1_border ) {
                    glsVertex(ma->vpos[i_v1]);
                    glsVertex(ma->vpos[i_v1]+n1);
                }
            }
            glEnd();
            glEnable(GL_LIGHTING);
        }
    };
    
    auto pnl_3dviewb = new VP_CallBacks();
    pnl_3dviewb->name = "meshb";
    pnl_3dviewb->background = pbg_White;
    pnl_3dviewb->data = &l_mm;
    pnl_3dviewb->camera = camera;
    pnl_3dviewb->s_display = {
        to_string("File: %s",mb->filename.c_str()),
        to_string("Counts: %d verts, %d faces", (int)mb->n_v, (int)mb->n_f ),
        to_string("Changes: %d parts, +%d faces", (int)l_mmps0b.size(), num_add_faces0b),
    };
    pnl_3dviewb->lfn_draw = [&] {
        MeshMatch *mm = mm0b;
        draw_mesh( mm->m1, mm->m0, mm->vm10, mm->fm10, mat_unmatchb0, mat_unmatchb0_ );
        
        if( glob_part_offset > 0.0f ) {
            glBegin(GL_TRIANGLES);
            for( int i = 0; i < l_mmps0b.size(); i++ ) {
                auto mmp = l_mmps0b[i];
                auto n1 = l_mmp0b_norms1[i] * glob_part_offset;
                set_material(mat_unmatchb0,mat_unmatchb0_);
                for( int i_f1 : mmp->sif1 ) draw_face( mb->finds[i_f1], mb->fnorms[i_f1], mb->vpos, n1 );
            }
            glEnd();
            
            glDisable(GL_LIGHTING);
            glBegin(GL_LINES);
            glColor4f( 0.7f, 0.7f, 0.7f, 0.2f );
            for( int i = 0; i < l_mmps0b.size(); i++ ) {
                auto mmp = l_mmps0b[i];
                auto n1 = l_mmp0b_norms1[i] * glob_part_offset;
                for( int i_v1 : mmp->siv1_border ) {
                    glsVertex(mb->vpos[i_v1]);
                    glsVertex(mb->vpos[i_v1]+n1);
                }
            }
            glEnd();
            glEnable(GL_LIGHTING);
        }
    };
    
    vector<ViewPanel*> panels = { pnl_3dviewa, pnl_3dview0, pnl_3dviewb };
    pnl_main->panel = new VP_Divide_N("3dviews", panels, DivideType::LEFTRIGHT);
    
    
    
    pnl_properties->add( new Prop_Text("View Options",TEXT_CENTER));
    pnl_properties->add( new Prop_Bind_Float("Lens", &camera->d, 0.001f, 1.0f ));
    bool b_drawtext = true;
    auto prop_drawtext = new Prop_Bind_Bool("Text", &b_drawtext);
    prop_drawtext->lfn_change = [&](void*) {
        for( auto pnl : panels ) pnl->b_display = b_drawtext;
    };
    pnl_properties->add( prop_drawtext );
    pnl_properties->add( new Prop_Text("Mesh",TEXT_LEFT));
    pnl_properties->add( new Prop_Bind_Bool(" Edges", &glob_mesh_edges));
    pnl_properties->add( new Prop_Bind_Int("  Darkness", &glob_mesh_edges_darkness, 1, 5));
    pnl_properties->add( new Prop_Bind_Bool(" Faces", &glob_mesh_faces));
    pnl_properties->add( new Prop_Bind_Bool("  Same Only", &glob_mesh_faces_sameonly));
    pnl_properties->add( new Prop_Bind_Bool("  Changes Only", &glob_mesh_faces_chgsonly));
    pnl_properties->add( new Prop_Bind_Bool("  Two-Toned", &glob_mesh_faces_twotoned));
    pnl_properties->add( new Prop_Bind_Float(" Part Offset", &glob_part_offset, 0.0f, 10.0f));
    pnl_properties->add( new Prop_Divider(' ') );
    
    add_camera_properties( camera );
    
    
    
    
    viewer->run();
}

void diff_seq( Viewer *viewer, vector<Mesh*> &meshes ) {
    
    int n_meshes = meshes.size();
    int h = (int)sqrt(n_meshes);
    int w = ceilf( (float)n_meshes / (float)h );
    
    dlog.start("Mesh matchings");
    vector<MeshMatch*> l_mm;
    for( int i = 0; i < n_meshes-1; i++ ) {
        auto mm = new MeshMatch(meshes[i],meshes[i+1]);
        mm->algorithm();
        
        l_mm.push_back(mm);
    }
    dlog.end();
    
    VPCamera *camera = new VPCamera();
    camera->set_default_lights();
    camera->lookat( makevec3f(30,-50,30), makevec3f(0,0,0), makevec3f(0,0,1));
    camera->d = 0.2f;
    
    
    
    
    vector<ViewPanel*> l_pnls;
    
    for( int i = 0; i < n_meshes; i++ ) {
        auto p = new pair<MeshMatch*,MeshMatch*>( (i>0?l_mm[i-1]:nullptr), (i<n_meshes-1?l_mm[i]:nullptr) );
        
        auto pnl = new VP_CallBacks();
        pnl->name = to_string("mesh%d",i);
        pnl->background = pbg_White;
        pnl->data = p;
        pnl->camera = camera;
        pnl->s_display = {
            to_string("File: %s", meshes[i]->filename.c_str()),
            to_string("Counts: %d %d", (int)meshes[i]->n_v, (int)meshes[i]->n_f )
        };
        pnl->fndraw = [](VP_CallBacks*vp) {
            auto p = (pair<MeshMatch*,MeshMatch*>*)vp->data;
            MeshMatch *mm0 = p->first;
            MeshMatch *mm1 = p->second;
            
            if( !mm0 && !mm1 ) {                                                                    // only
            } else if( !mm0 ) {                                                                     // first
                draw_mesh( mm1->m0, mm1->vm01, mm1->fm01, mat_unmatch0, mat_unmatch0_ );
            } else if( !mm1 ) {                                                                     // last
                draw_mesh( mm0->m1, mm0->m0, mm0->vm10, mm0->fm10, mat_unmatch1, mat_unmatch1_ );
            } else {                                                                                // middle
                draw_mesh( mm1->m0, mm0->m0, mm0->vm10, mm0->fm10, mm1->vm01, mm1->fm01, false );
            }
        };
        
        l_pnls.push_back(pnl);
    }
    
    vector<ViewPanel*> grid;
    for( int y = 0, i = 0; y < h; y++ ) {
        vector<ViewPanel*> row;
        for( int x = 0; x < w && i < n_meshes; x++, i++ ) {
            if( i < n_meshes ) row.push_back(l_pnls[i]);
            else row.push_back(nullptr);
        }
        grid.push_back(new VP_Divide_N("row", row, DivideType::LEFTRIGHT));
    }
    reverse(grid.begin(),grid.end());
    
    //pnl_main->panel = new VP_Divide_N("3dviews", l_pnls, DivideType::LEFTRIGHT);
    pnl_main->panel = new VP_Divide_N("grid", grid, DivideType::TOPBOTTOM );
    
    
    
    pnl_properties->add( new Prop_Text("View Options",TEXT_CENTER));
    pnl_properties->add( new Prop_Bind_Float("Lens", &camera->d, 0.001f, 1.0f ));
    bool b_drawtext = true;
    auto prop_drawtext = new Prop_Bind_Bool("Text", &b_drawtext);
    prop_drawtext->lfn_change = [&](void*) {
        for( auto pnl : l_pnls ) pnl->b_display = b_drawtext;
    };
    pnl_properties->add( prop_drawtext );
    pnl_properties->add( new Prop_Text("Mesh",TEXT_LEFT));
    pnl_properties->add( new Prop_Bind_Bool(" Edges", &glob_mesh_edges));
    pnl_properties->add( new Prop_Bind_Int("  Darkness", &glob_mesh_edges_darkness, 1, 5));
    pnl_properties->add( new Prop_Bind_Bool(" Faces", &glob_mesh_faces));
    pnl_properties->add( new Prop_Bind_Bool("  Same Only", &glob_mesh_faces_sameonly));
    pnl_properties->add( new Prop_Bind_Bool("  Changes Only", &glob_mesh_faces_chgsonly));
    pnl_properties->add( new Prop_Bind_Bool("  Two-Toned", &glob_mesh_faces_twotoned));
    pnl_properties->add( new Prop_Bind_Float(" Part Offset", &glob_part_offset, 0.0f, 10.0f));
    pnl_properties->add( new Prop_Divider() );
    
    add_camera_properties( camera );
    
    
    
    viewer->run();
}

void diff_compare( Viewer *viewer, vector<Mesh*> &meshes, vector<string> &fn_maps ) {
    int n_meshes = 2+fn_maps.size();
    int h = (int)sqrt(n_meshes*2);
    int w = ceilf( (float)n_meshes / (float)h );
    bool b_drawtext = true;
    
    dlog.start("Creating/loading mesh matchings");
    vector<MeshMatch*> l_mms;
    MeshMatch *mm;
    
    mm = new MeshMatch(meshes[0],meshes[1]);
    mm->name = "MeshGit";
    mm->algorithm();
    l_mms.push_back(mm);
    
    mm = new MeshMatch(meshes[0],meshes[1]);
    mm->name = "Exact";
    mm->assign_exact( 1, 0.1f );
    mm->cleanup();
    l_mms.push_back(mm);
    
    for( string &fn_map : fn_maps ) {
        mm = new MeshMatch(meshes[0],meshes[1]);
        mm->name = fn_map;
        mm->load(fn_map.c_str());
        
        if(true) {
            mm->cleanup();
        }
        
        l_mms.push_back(mm);
    }
    
    dlog.end();
    
    VPCamera *camera = new VPCamera();
    camera->set_default_lights();
    camera->lookat( makevec3f(30,-50,30), makevec3f(0,0,0), makevec3f(0,0,1));
    camera->d = 0.2f;
    
    
    auto f_create_panel = [camera](MeshMatch*mm)->ViewPanel* {
        VP_CallBacks *pnl0, *pnl1;
        
        pnl0 = new VP_CallBacks();
        pnl0->name = "mesh0";
        //pnl0->s_display = { mm->name };
        pnl0->background = pbg_White;
        pnl0->camera = camera;
        pnl0->lfn_draw = [mm] {
            draw_mesh( mm->m0, mm->vm01, mm->fm01, mat_unmatch0, mat_unmatch0_ );
        };
        
        pnl1 = new VP_CallBacks();
        pnl1->name = "mesh1";
        //pnl1->s_display = { mm->name };
        pnl1->background = pbg_White;
        pnl1->camera = camera;
        pnl1->lfn_draw = [mm] {
            draw_mesh( mm->m1, mm->m0, mm->vm10, mm->fm10, mat_unmatch1, mat_unmatch1_ );
        };
        
        auto pnl_text = new ViewPanel();
        pnl_text->s_display = { mm->name };
        pnl_text->background = pbg_Gray;
        
        vector<ViewPanel*> l_pnls = {pnl0,pnl1};
        auto pnl_pair = new VP_Divide_N("pair", l_pnls, DivideType::LEFTRIGHT);
        
        auto pnl_pairset = new VP_Split("pairset", pnl_text, pnl_pair, 15, SplitTypes::SPLIT_TOP );
        pnl_pairset->resizable = false;
        pnl_pairset->border = 1;
        return pnl_pairset;
    };
    
    vector<ViewPanel*> l_pnls;
    for( MeshMatch *mm : l_mms ) {
        l_pnls.push_back( f_create_panel(mm) );
    }
    vector<ViewPanel*> grid;
    for( int y = 0, i = 0; y < h; y++ ) {
        vector<ViewPanel*> row;
        for( int x = 0; x < w; x++, i++ ) {
            if( i < n_meshes ) row.push_back(l_pnls[i]);
            else row.push_back(nullptr);
        }
        grid.push_back(new VP_Divide_N("row", row, DivideType::LEFTRIGHT));
    }
    reverse(grid.begin(),grid.end());
    pnl_main->panel = new VP_Divide_N("grid", grid, DivideType::TOPBOTTOM );
    
    
    pnl_properties->add( new Prop_Text("View Options",TEXT_CENTER));
    pnl_properties->add( new Prop_Bind_Float("Lens", &camera->d, 0.001f, 1.0f ));
    auto prop_drawtext = new Prop_Bind_Bool("Text", &b_drawtext);
    prop_drawtext->lfn_change = [&](void*) {
        for( auto pnl : l_pnls ) {
            auto pnl_ = (VP_Divide_N*)pnl;
            for( auto pnl__ : pnl_->panels ) pnl__->b_display = b_drawtext;
        }
    };
    pnl_properties->add( prop_drawtext );
    
    pnl_properties->add( new Prop_Text("Mesh",TEXT_LEFT));
    pnl_properties->add( new Prop_Bind_Bool(" Edges", &glob_mesh_edges));
    pnl_properties->add( new Prop_Bind_Int("  Darkness", &glob_mesh_edges_darkness, 1, 5));
    pnl_properties->add( new Prop_Bind_Bool(" Faces", &glob_mesh_faces));
    pnl_properties->add( new Prop_Bind_Bool("  Same Only", &glob_mesh_faces_sameonly));
    pnl_properties->add( new Prop_Bind_Bool("  Changes Only", &glob_mesh_faces_chgsonly));
    pnl_properties->add( new Prop_Bind_Bool("  Two-Toned", &glob_mesh_faces_twotoned));
    pnl_properties->add( new Prop_Divider(' ') );
    
    add_camera_properties( camera );
    
    
    
    
    viewer->run();
}

void merge( Viewer *viewer, vector<Mesh*> &meshes ) {
    Mesh *m0 = meshes[0];
    Mesh *ma = meshes[1];
    Mesh *mb = meshes[2];
    
    SimpleMesh *mm = nullptr;
    int subd_level = 0;
    
    dlog.start("Mesh matching");
    auto mm0a = new MeshMatch(m0,ma);
    auto mm0b = new MeshMatch(m0,mb);
    mm0a->algorithm();
    mm0b->algorithm();
    vector<MeshMatch*> l_mm = { mm0a, mm0b };
    dlog.end();
    
    dlog.start("Partitioning");
    int num_del_faces0a = 0, num_add_faces0a = 0;
    vector<MeshMatchPartition*> l_mmps0a = MeshMatchPartition::partition(mm0a);
    vector<vec3f> l_mmp0a_norms0, l_mmp0a_norms1;
    for( auto mmp : l_mmps0a ) {
        vec3f n0 = zero3f, n1 = zero3f;
        for( int i_f0 : mmp->sif0 ) n0 += m0->fnorms[i_f0];
        n0 = normalize(n0);
        if( n0%n0<0.1f ) n0 = { 1,0,0 };
        l_mmp0a_norms0.push_back(n0);
        num_del_faces0a += mmp->sif0.size();
        
        for( int i_f1 : mmp->sif1 ) n1 += ma->fnorms[i_f1];
        n1 = normalize(n1);
        if( n1%n1<0.1f ) n1 = { 1,0,0 };
        l_mmp0a_norms1.push_back(n1);
        num_add_faces0a += mmp->sif1.size();
    }
    
    int num_del_faces0b = 0, num_add_faces0b = 0;
    vector<MeshMatchPartition*> l_mmps0b = MeshMatchPartition::partition(mm0b);
    vector<vec3f> l_mmp0b_norms0, l_mmp0b_norms1;
    for( auto mmp : l_mmps0b ) {
        vec3f n0 = zero3f, n1 = zero3f;
        for( int i_f0 : mmp->sif0 ) n0 += m0->fnorms[i_f0];
        n0 = normalize(n0);
        if( n0%n0<0.1f ) n0 = { 1,0,0 };
        l_mmp0b_norms0.push_back(n0);
        num_del_faces0b += mmp->sif0.size();
        
        for( int i_f1 : mmp->sif1 ) n1 += mb->fnorms[i_f1];
        n1 = normalize(n1);
        if( n1%n1<0.1f ) n1 = { 1,0,0 };
        l_mmp0b_norms1.push_back(n1);
        num_add_faces0b += mmp->sif1.size();
    }
    
    dlog.print( "Partitions: %d %d", l_mmps0a.size(), l_mmps0b.size() );
    dlog.end();
    
    
    dlog.start_("Detecting face change conflicts");
    vector<vector<bool>> conflicts;
    for( auto mmp0a : l_mmps0a ) {
        vector<bool> row;
        for( auto mmp0b : l_mmps0b ) {
            bool c = false;
            for( int i_f : mmp0a->sif0 ) if( mmp0b->sif0.count(i_f) ) { c = true; break; }
            row.push_back(c);
        }
        conflicts.push_back(row);
    }
    
    bool *is_a = new bool(l_mmps0a.size());
    bool *is_b = new bool(l_mmps0a.size());
    for( int ia = 0; ia < l_mmps0a.size(); ia++ ) {
        bool c = false;
        for( int ib = 0; ib < l_mmps0b.size(); ib++ ) c |= conflicts[ia][ib];
        is_a[ia] = !c;
    }
    for( int ib = 0; ib < l_mmps0b.size(); ib++ ) {
        bool c = false;
        for( int ia = 0; ia < l_mmps0a.size(); ia++ ) c |= conflicts[ia][ib];
        is_b[ib] = !c;
    }
    dlog.end();
    

    
    vector<vec3f> vpos = m0->vpos;
    vector<vector<ivert>> finds = m0->finds;
    vector<vec3f> fnorms = m0->fnorms;
    
    vector<material> fmats( m0->finds.size(), mat_match );
    vector<material> fmats_( m0->finds.size(), mat_match_ );
    vector<bool> fcolor( m0->finds.size(), false );
    
    vector<bool> fshow( m0->finds.size(), true );
    
    vector<vector<iface>> fswitcha;
    for( auto mmp : l_mmps0a ) {
        vector<ivert> l_iv_add;
        unordered_map<ivert,ivert> map_iv_iv;
        vector<iface> sw;
        for( int i_v : mmp->siv1 ) {
            map_iv_iv[i_v] = vpos.size();
            vpos.push_back(ma->vpos[i_v]);
        }
        for( int i_v : mmp->siv1_border ) {
            map_iv_iv[i_v] = mm0a->vm10[i_v];
        }
        for( int i_f : mmp->sif1 ) {
            vector<ivert> vinds;
            for( int i_v : ma->finds[i_f] ) vinds.push_back( map_iv_iv[i_v] );
            sw.push_back( finds.size() );
            finds.push_back(vinds);
            fshow.push_back(false);
            fnorms.push_back(ma->fnorms[i_f]);
            fmats.push_back(mat_unmatcha0);
            fmats_.push_back(mat_unmatcha0_);
            fcolor.push_back(true);
        }
        fswitcha.push_back(sw);
    }
    
    vector<vector<iface>> fswitchb;
    for( auto mmp : l_mmps0b ) {
        vector<ivert> l_iv_add;
        unordered_map<ivert,ivert> map_iv_iv;
        vector<iface> sw;
        for( int i_v : mmp->siv1 ) {
            map_iv_iv[i_v] = vpos.size();
            vpos.push_back(mb->vpos[i_v]);
        }
        for( int i_v : mmp->siv1_border ) {
            map_iv_iv[i_v] = mm0b->vm10[i_v];
        }
        for( int i_f : mmp->sif1 ) {
            vector<ivert> vinds;
            for( int i_v : mb->finds[i_f] ) vinds.push_back( map_iv_iv[i_v] );
            sw.push_back( finds.size() );
            finds.push_back(vinds);
            fshow.push_back(false);
            fnorms.push_back(ma->fnorms[i_f]);
            fmats.push_back(mat_unmatchb0);
            fmats_.push_back(mat_unmatchb0_);
            fcolor.push_back(true);
        }
        fswitchb.push_back(sw);
    }
    
    dlog.start_("Detect vert change conflicts");
    unordered_set<int> s_iv_conflict;
    vector<bool> l_vmove_a(ma->n_v,false), l_vmove_b(mb->n_v,false);
    for( int i_v0 = 0; i_v0 < m0->n_v; i_v0++ ) {
        int i_va = mm0a->vm01[i_v0];
        int i_vb = mm0b->vm01[i_v0];
        vec3f &v = m0->vpos[i_v0];
        bool mov0a = ( i_va!=-1 ? (lengthSqr(v-ma->vpos[i_va])>0.001f) : false );
        bool mov0b = ( i_vb!=-1 ? (lengthSqr(v-mb->vpos[i_vb])>0.001f) : false );
        if( mov0a && mov0b ) s_iv_conflict.insert(i_v0);
        if( mov0a ) l_vmove_a[i_va] = true;
        if( mov0b ) l_vmove_b[i_vb] = true;
    }
    vec3f alpha_blend = {0,0,1};
    float alpha_blend0a = 1.0f;
    float alpha_blend0b = 1.0f;
    dlog.end();
    
    
    auto fn_update_mergemesh = [&]{
        // turn on all faces of m0
        for( int i_f = 0; i_f < finds.size(); i_f++ ) fshow[i_f] = (i_f < m0->n_f);
        
        // turn off faces of m0 and on faces of ma
        for( int i_a = 0; i_a < l_mmps0a.size(); i_a++ ) {
            if( !is_a[i_a] ) continue;
            auto mmp = l_mmps0a[i_a];
            for( int i_f : mmp->sif0 ) fshow[i_f] = false;
            for( int i_f : fswitcha[i_a] ) fshow[i_f] = true;
        }
        
        // turn off faces of m0 and on faces of mb
        for( int i_b = 0; i_b < l_mmps0b.size(); i_b++ ) {
            if( !is_b[i_b] ) continue;
            auto mmp = l_mmps0b[i_b];
            for( int i_f : mmp->sif0 ) fshow[i_f] = false;
            for( int i_f : fswitchb[i_b] ) fshow[i_f] = true;
        }
    };
    
    auto fn_update_pos = [&]{
        vector<ivert> &vm0a = mm0a->vm01;
        vector<ivert> &vm0b = mm0b->vm01;
        for( int i_v0 = 0; i_v0 < m0->n_v; i_v0++ ) {
            int i_va = vm0a[i_v0];
            int i_vb = vm0b[i_v0];
            vec3f &v0 = m0->vpos[i_v0];
            if( s_iv_conflict.count(i_v0) ) {
                vec3f &va = ma->vpos[i_va];
                vec3f &vb = mb->vpos[i_vb];
                vpos[i_v0] = v0 * alpha_blend.x + va * alpha_blend.y + vb * alpha_blend.z;
            } else {
                if( i_va != -1 && l_vmove_a[i_va] ) {
                    vec3f &va = ma->vpos[i_va];
                    vpos[i_v0] = v0 * (1.0f-alpha_blend0a) + va * alpha_blend0a;
                }
                if( i_vb != -1 && l_vmove_b[i_vb] ) {
                    vec3f &vb = mb->vpos[i_vb];
                    vpos[i_v0] = v0 * (1.0f-alpha_blend0b) + vb * alpha_blend0b;
                }
            }
        }
        
        // approximate face normals
        for( int i_f = 0; i_f < finds.size(); i_f++ ) {
            vector<ivert> &vinds = finds[i_f];
            vec3f &v0 = vpos[vinds[0]];
            vec3f &v1 = vpos[vinds[1]];
            vec3f &v2 = vpos[vinds[2]];
            fnorms[i_f] = triangle_normal(v0,v1,v2);
        }
    };
    
    fn_update_mergemesh();
    fn_update_pos();
    
    VPCamera *camera = new VPCamera();
    camera->set_default_lights();
    camera->lookat( makevec3f(30,-50,30), makevec3f(0,0,0), makevec3f(0,0,1));
    camera->d = 0.2f;
    
    
    
    
    auto pnl_3dview0 = new VP_CallBacks();
    pnl_3dview0->name = "mesh0";
    pnl_3dview0->background = pbg_White;// Maya;
    pnl_3dview0->data = &l_mm;
    pnl_3dview0->camera = camera;
    pnl_3dview0->s_display = {
        to_string("File: %s",m0->filename.c_str()),
        to_string("Counts: %d verts, %d faces", (int)m0->n_v, (int)m0->n_f ),
        to_string("Changes: %d,%d parts, -%d,-%d faces", (int)l_mmps0a.size(), (int)l_mmps0b.size(), num_del_faces0a, num_del_faces0b),
    };
    pnl_3dview0->fndraw = [](VP_CallBacks *vp) {
        vector<MeshMatch*> *l_mm = (vector<MeshMatch*> *)vp->data;
        MeshMatch *mm0a = (*l_mm)[0];
        MeshMatch *mm0b = (*l_mm)[1];
        draw_mesh( mm0a->m0, mm0a->vm01, mm0a->fm01, mm0b->vm01, mm0b->fm01, true );
    };
    pnl_3dview0->fnmousedown = [&](VP_CallBacks*) {
        if( pnl_3dview0->mouse_but != MOUSE_LBUTTON && pnl_3dview0->mouse_but != MOUSE_RBUTTON ) return;
        bool add = ( pnl_3dview0->mouse_but == MOUSE_LBUTTON );
        
        ray3f r = pnl_3dview0->get_ray();
        int i_f_hit = -1; float d_f_best = 0.0f;
        for( int i_f = 0; i_f < mm0a->m0->n_f; i_f++ ) {
            vector<int> &f = mm0a->m0->finds[i_f];
            int n_i = f.size();
            for( int i0 = 0, i1 = 1, i2 = 2; i2 < n_i; i1++,i2++ ) {
                vec3f &v0 = mm0a->m0->vpos[f[i0]];
                vec3f &v1 = mm0a->m0->vpos[f[i1]];
                vec3f &v2 = mm0a->m0->vpos[f[i2]];
                float t,ba,bb;
                if( !intersect_triangle(r,v0,v1,v2,t,ba,bb) ) continue;
                if( i_f_hit == -1 || t < d_f_best ) { i_f_hit = i_f; d_f_best = t; }
            }
        }
        if( i_f_hit == -1 ) return;
        
        // find partition with i_f_hit
        if( add ) {
            int n_a = l_mmps0a.size();
            for( int i_a = 0; i_a < n_a; i_a++ ) {
                auto mmp0a = l_mmps0a[i_a];
                if( !mmp0a->sif0.count(i_f_hit) ) continue;
                is_a[i_a] = false;
            }
            int n_b = l_mmps0b.size();
            for( int i_b = 0; i_b < n_b; i_b++ ) {
                auto mmp0b = l_mmps0b[i_b];
                if( !mmp0b->sif0.count(i_f_hit) ) continue;
                is_b[i_b] = false;
            }
        } else {
            int i_a_in = -1, i_b_in = -1;
            int n_a = l_mmps0a.size();
            for( int i_a = 0; i_a < n_a; i_a++ ) {
                auto mmp0a = l_mmps0a[i_a];
                if( !mmp0a->sif0.count(i_f_hit) ) continue;
                i_a_in = i_a;
            }
            int n_b = l_mmps0b.size();
            for( int i_b = 0; i_b < n_b; i_b++ ) {
                auto mmp0b = l_mmps0b[i_b];
                if( !mmp0b->sif0.count(i_f_hit) ) continue;
                i_b_in = i_b;
            }
            if( i_a_in != -1 && i_b_in != -1 ) return;
            if( i_a_in != -1 ) {
                int i_a = i_a_in;
                for( int i_b = 0; i_b < l_mmps0b.size(); i_b++ ) {
                    if( conflicts[i_a][i_b] ) is_b[i_b] = false;
                }
                is_a[i_a] = true;
            } else {
                int i_b = i_b_in;
                for( int i_a = 0; i_a < l_mmps0a.size(); i_a++ ) {
                    if( conflicts[i_a][i_b] ) is_a[i_a] = false;
                }
                is_b[i_b] = true;
            }
        }
        fn_update_mergemesh();
    };
    
    auto pnl_3dviewa = new VP_CallBacks();
    pnl_3dviewa->name = "mesha";
    pnl_3dviewa->background = pbg_White;
    pnl_3dviewa->data = &l_mm;
    pnl_3dviewa->camera = camera;
    pnl_3dviewa->s_display = {
        to_string("File: %s",ma->filename.c_str()),
        to_string("Counts: %d verts, %d faces", (int)ma->n_v, (int)ma->n_f ),
        to_string("Changes: %d parts, +%d faces", (int)l_mmps0a.size(), num_add_faces0a),
    };
    pnl_3dviewa->fndraw = [](VP_CallBacks *vp) {
        vector<MeshMatch*> *l_mm = (vector<MeshMatch*> *)vp->data;
        MeshMatch *mm = (*l_mm)[0];
        draw_mesh( mm->m1, mm->m0, mm->vm10, mm->fm10, mat_unmatcha0, mat_unmatcha0_ );
    };
    pnl_3dviewa->fnmousedown = [&](VP_CallBacks*) {
        if( pnl_3dviewa->mouse_but != MOUSE_LBUTTON && pnl_3dviewa->mouse_but != MOUSE_RBUTTON ) return;
        bool add = ( pnl_3dviewa->mouse_but == MOUSE_LBUTTON );
        
        ray3f r = pnl_3dviewa->get_ray();
        int i_f_hit = -1; float d_f_best = 0.0f;
        for( int i_f = 0; i_f < mm0a->m1->n_f; i_f++ ) {
            vector<int> &f = mm0a->m1->finds[i_f];
            int n_i = f.size();
            for( int i0 = 0, i1 = 1, i2 = 2; i2 < n_i; i1++,i2++ ) {
                vec3f &v0 = mm0a->m1->vpos[f[i0]];
                vec3f &v1 = mm0a->m1->vpos[f[i1]];
                vec3f &v2 = mm0a->m1->vpos[f[i2]];
                float t,ba,bb;
                if( !intersect_triangle(r,v0,v1,v2,t,ba,bb) ) continue;
                if( i_f_hit == -1 || t < d_f_best ) { i_f_hit = i_f; d_f_best = t; }
            }
        }
        if( i_f_hit == -1 ) return;
        
        // find partition with i_f_hit
        for( int i_a = 0; i_a < l_mmps0a.size(); i_a++ ) {
            auto mmp0a = l_mmps0a[i_a];
            if( !mmp0a->sif1.count(i_f_hit) ) continue;
            
            if( add ) {
                is_a[i_a] = true;
                for( int i_b = 0; i_b < l_mmps0b.size(); i_b++ ) {
                    if( conflicts[i_a][i_b] ) is_b[i_b] = false;
                }
            } else {
                is_a[i_a] = false;
            }
        }
        fn_update_mergemesh();
    };
    
    auto pnl_3dviewb = new VP_CallBacks();
    pnl_3dviewb->name = "meshb";
    pnl_3dviewb->background = pbg_White;
    pnl_3dviewb->data = &l_mm;
    pnl_3dviewb->camera = camera;
    pnl_3dviewb->s_display = {
        to_string("File: %s",mb->filename.c_str()),
        to_string("Counts: %d verts, %d faces", (int)mb->n_v, (int)mb->n_f ),
        to_string("Changes: %d parts, +%d faces", (int)l_mmps0b.size(), num_add_faces0b),
    };
    pnl_3dviewb->fndraw = [](VP_CallBacks *vp) {
        vector<MeshMatch*> *l_mm = (vector<MeshMatch*> *)vp->data;
        MeshMatch *mm = (*l_mm)[1];
        draw_mesh( mm->m1, mm->m0, mm->vm10, mm->fm10, mat_unmatchb0, mat_unmatchb0_ );
    };
    pnl_3dviewb->fnmousedown = [&](VP_CallBacks*) {
        if( pnl_3dviewb->mouse_but != MOUSE_LBUTTON && pnl_3dviewb->mouse_but != MOUSE_RBUTTON ) return;
        bool add = ( pnl_3dviewb->mouse_but == MOUSE_LBUTTON );
        
        ray3f r = pnl_3dviewb->get_ray();
        int i_f_hit = -1; float d_f_best = 0.0f;
        for( int i_f = 0; i_f < mm0b->m1->n_f; i_f++ ) {
            vector<int> &f = mm0b->m1->finds[i_f];
            int n_i = f.size();
            for( int i0 = 0, i1 = 1, i2 = 2; i2 < n_i; i1++,i2++ ) {
                vec3f &v0 = mm0b->m1->vpos[f[i0]];
                vec3f &v1 = mm0b->m1->vpos[f[i1]];
                vec3f &v2 = mm0b->m1->vpos[f[i2]];
                float t,ba,bb;
                if( !intersect_triangle(r,v0,v1,v2,t,ba,bb) ) continue;
                if( i_f_hit == -1 || t < d_f_best ) { i_f_hit = i_f; d_f_best = t; }
            }
        }
        if( i_f_hit == -1 ) return;
        
        // find partition with i_f_hit
        for( int i_b = 0; i_b < l_mmps0b.size(); i_b++ ) {
            auto mmp0b = l_mmps0b[i_b];
            if( !mmp0b->sif1.count(i_f_hit) ) continue;
            
            if( add ) {
                is_b[i_b] = true;
                for( int i_a = 0; i_a < l_mmps0a.size(); i_a++ ) {
                    if( conflicts[i_a][i_b] ) is_a[i_a] = false;
                }
            } else {
                is_b[i_b] = false;
            }
        }
        fn_update_mergemesh();
    };
    
    auto pnl_merge = new VP_CallBacks();
    pnl_merge->name = "merge";
    pnl_merge->background = pbg_White;
    pnl_merge->camera = camera;
    pnl_merge->s_display = { "Merged" };
    pnl_merge->lfn_draw = [&]{
        int n_f = finds.size();
        vec3f color0 = mat_match.diffuse, color1 = { mat_match.diffuse.x * 0.5f, mat_match.diffuse.y * 0.5f, 1.0f };
        vec3f color0_ = mat_match_.diffuse, color1_ = { mat_match_.diffuse.x * 0.5f, mat_match_.diffuse.y * 0.5f, 1.0f };
        
        glBegin(GL_TRIANGLES);
        for( int i_f = 0; i_f < n_f; i_f++ ) {
            if( !fshow[i_f] ) continue;
            set_material( fmats[i_f], fmats_[i_f] );
            //draw_face( finds[i_f], vpos );
            
            if( i_f < m0->n_f ) {
                draw_face( finds[i_f], fnorms[i_f], vpos, m0->vpos, color0, color0_, color1, color1_ );
            } else {
                draw_face( finds[i_f], vpos );
            }
        }
        glEnd();
        
        glDepthRange(0.0f,0.999999f);
        glLineWidth(1.0f);
        glEnable(GL_LINE_SMOOTH);
        glDisable(GL_LIGHTING);
        
        if( glob_mesh_edges ) {
            glEnable(GL_BLEND);
            glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
            
            switch( glob_mesh_edges_darkness ) {
                case 1: { glColor4f(0.2f,0.2f,0.2f,0.1f); glLineWidth(1.0f); } break;
                case 2: { glColor4f(0.2f,0.2f,0.2f,0.5f); glLineWidth(1.0f); } break;
                case 3: { glColor4f(0.2f,0.2f,0.2f,0.7f); glLineWidth(1.0f); } break;
                case 4: { glColor4f(0.2f,0.2f,0.2f,0.5f); glLineWidth(2.0f); } break;
                case 5: { glColor4f(0.2f,0.2f,0.2f,0.7f); glLineWidth(2.0f); } break;
            }
            
            glBegin(GL_LINES);
            for( int i_f = 0; i_f < n_f; i_f++ ) {
                if( !fshow[i_f] ) continue;
                vector<ivert> &_vinds = finds[i_f];
                int n = _vinds.size();
                for( int i0 = 0; i0 < n; i0++ ) {
                    int i1 = (i0+1)%n;
                    glsVertex(vpos[_vinds[i0]]);
                    glsVertex(vpos[_vinds[i1]]);
                }
            }
            glEnd();
        }
        
        
        glEnable(GL_LIGHTING);
        glDepthRange(0.0f,1.0f);
    };
    
    auto pnl_merge2 = new VP_CallBacks();
    pnl_merge2->name = "merge";
    pnl_merge2->background = pbg_White;
    pnl_merge2->camera = camera;
    pnl_merge2->s_display = { "","" };
    pnl_merge2->lfn_draw = [&]{
        if( mm == nullptr ) return;
        mm->draw( glob_mesh_faces, glob_mesh_edges, mat_match, mat_match_ );
    };
    
    auto f_update_mm = [&]{
        delete mm;
        
        mm = new SimpleMesh();
        mm->vpos = vpos;
        for( int i_v = 0; i_v < vpos.size(); i_v++ ) {
            float dist = lengthSqr( m0->vpos[i_v] - vpos[i_v] );
            float d = 0.1f / (0.1f + dist);
            mm->vinfo.push_back( { d, true } );
        }
        for( int i_f = 0; i_f < finds.size(); i_f++ ) {
            if( !fshow[i_f] ) continue;
            mm->finds.push_back(finds[i_f]);
            mm->fmats.push_back(fmats[i_f]);
            mm->fmats_.push_back(fmats_[i_f]);
            mm->fcolor.push_back(fcolor[i_f]);
        }
        mm->clean();
        mm->recompute_normals();
        
        for( int l = 0; l < subd_level; l++ ) {
            auto mm_ = mm->subd_catmullclark();
            delete mm;
            mm = mm_;
        }
        
        pnl_merge2->s_display[0] = to_string("Merged + Subdivision (%d)",subd_level);
        pnl_merge2->s_display[1] = to_string("Counts: %d verts, %d faces", (int)mm->vpos.size(), (int)mm->finds.size() );
    };
    auto f_update_mm_mesh0 = [&]{
        //draw_mesh( mm->m1, mm->m0, mm->vm10, mm->fm10, mat_unmatchb0, mat_unmatchb0_ );
        delete mm;
        
        mm = new SimpleMesh();
        mm->vpos = m0->vpos;
        for( int i_v = 0; i_v < m0->vpos.size(); i_v++ ) mm->vinfo.push_back( { 0, true } );
        for( int i_f = 0; i_f < m0->finds.size(); i_f++ ) {
            mm->finds.push_back(m0->finds[i_f]);
            
            if( mm0a->fm01[i_f] == -1 && mm0b->fm01[i_f] == -1 ) {
                mm->fmats.push_back( mat_unmatch0ab );
                mm->fmats_.push_back( mat_unmatch0ab_ );
            } else if( mm0a->fm01[i_f] == -1 ) {
                mm->fmats.push_back( mat_unmatch0a );
                mm->fmats_.push_back( mat_unmatch0a_ );
            } else if( mm0b->fm01[i_f] == -1 ) {
                mm->fmats.push_back( mat_unmatch0b );
                mm->fmats_.push_back( mat_unmatch0b_ );
            } else {
                mm->fmats.push_back( mat_match );
                mm->fmats_.push_back( mat_match_ );
            }
            mm->fcolor.push_back(true);
        }
        mm->clean();
        mm->recompute_normals();
        
        for( int l = 0; l < subd_level; l++ ) {
            auto mm_ = mm->subd_catmullclark();
            delete mm;
            mm = mm_;
        }
        
        pnl_merge2->s_display[0] = to_string("Subdivision (%d)",subd_level);
        pnl_merge2->s_display[1] = to_string("Counts: %d verts, %d faces", (int)mm->vpos.size(), (int)mm->finds.size() );
    };
    auto f_update_mm_mesh = [&](Mesh *m, vector<ivert> &vm, vector<iface> &fm, material &mat_unmatch, material &mat_unmatch_ ){
        //draw_mesh( mm->m1, mm->m0, mm->vm10, mm->fm10, mat_unmatchb0, mat_unmatchb0_ );
        delete mm;
        
        mm = new SimpleMesh();
        mm->vpos = m->vpos;
        for( int i_v = 0; i_v < m->vpos.size(); i_v++ ) {
            if( vm[i_v] == -1 ) {
                mm->vinfo.push_back( { 0, true } );
            } else {
                float dist = lengthSqr( m0->vpos[vm[i_v]] - m->vpos[i_v] );
                float d = 0.1f / (0.1f + dist);
                mm->vinfo.push_back( { d, true } );
            }
        }
        for( int i_f = 0; i_f < m->finds.size(); i_f++ ) {
            mm->finds.push_back(m->finds[i_f]);
            if( fm[i_f] != -1 ) {
                mm->fmats.push_back( mat_match );
                mm->fmats_.push_back( mat_match_ );
                mm->fcolor.push_back(false);
            } else {
                mm->fmats.push_back( mat_unmatch );
                mm->fmats_.push_back( mat_unmatch_ );
                mm->fcolor.push_back(true);
            }
        }
        mm->clean();
        mm->recompute_normals();
        
        for( int l = 0; l < subd_level; l++ ) {
            auto mm_ = mm->subd_catmullclark();
            delete mm;
            mm = mm_;
        }
        
        pnl_merge2->s_display[0] = to_string("Subdivision (%d)",subd_level);
        pnl_merge2->s_display[1] = to_string("Counts: %d verts, %d faces", (int)mm->vpos.size(), (int)mm->finds.size() );
    };
    
    
    vector<ViewPanel*> originals = { pnl_3dviewa, pnl_3dview0, pnl_3dviewb };
    auto pnl_originals = new VP_Divide_N("3dviews", originals, DivideType::LEFTRIGHT);
    
    vector<ViewPanel*> merged = { nullptr, pnl_merge, pnl_merge2 };
    auto pnl_merged = new VP_Divide_N("merged", merged, DivideType::LEFTRIGHT );
    
    vector<ViewPanel*> grid = { pnl_originals, pnl_merged };
    pnl_main->panel = new VP_Divide_N("grid", grid, DivideType::TOPBOTTOM );
    
    pnl_properties->add( new Prop_Text("View Options",TEXT_CENTER));
    pnl_properties->add( new Prop_Bind_Float("Lens", &camera->d, 0.001f, 1.0f ));
    bool b_drawtext = true;
    auto prop_drawtext = new Prop_Bind_Bool("Text", &b_drawtext);
    prop_drawtext->lfn_change = [&](void*) {
        pnl_3dview0->b_display = pnl_3dviewa->b_display = pnl_3dviewb->b_display = b_drawtext;
        pnl_merge->b_display = pnl_merge2->b_display = b_drawtext;
    };
    pnl_properties->add( prop_drawtext );
    pnl_properties->add( new Prop_Text("Mesh",TEXT_LEFT));
    pnl_properties->add( new Prop_Bind_Bool(" Edges", &glob_mesh_edges));
    pnl_properties->add( new Prop_Bind_Int("  Darkness", &glob_mesh_edges_darkness, 1, 5));
    pnl_properties->add( new Prop_Bind_Bool(" Faces", &glob_mesh_faces));
    pnl_properties->add( new Prop_Bind_Bool("  Same Only", &glob_mesh_faces_sameonly));
    pnl_properties->add( new Prop_Bind_Bool("  Changes Only", &glob_mesh_faces_chgsonly));
    pnl_properties->add( new Prop_Bind_Bool("  Two-Toned", &glob_mesh_faces_twotoned));
    pnl_properties->add( new Prop_Bind_Float(" Part Offset", &glob_part_offset, 0.0f, 10.0f));
    pnl_properties->add( new Prop_Text("Catmull-Clark Subd", TEXT_LEFT) );
    pnl_properties->add( new Prop_Bind_Int(" Level", &subd_level, 0, 5 ) );
    pnl_properties->add( new Prop_Button(" Mesh 0", nullptr, [&](void*){
        f_update_mm_mesh0();
    }));
    pnl_properties->add( new Prop_Button(" Mesh A", nullptr, [&](void*){
        f_update_mm_mesh( ma, mm0a->vm10, mm0a->fm10, mat_unmatcha0, mat_unmatcha0_ );
    }));
    pnl_properties->add( new Prop_Button(" Mesh B", nullptr, [&](void*){
        f_update_mm_mesh( mb, mm0b->vm10, mm0b->fm10, mat_unmatchb0, mat_unmatchb0_ );
    }));
    pnl_properties->add( new Prop_Button(" Merge", nullptr, [&](void*){
        f_update_mm();
    }));
    pnl_properties->add( new Prop_Divider(' ') );
    
    // JUST LOAD THE INPUT PLY FILES IN MODELING SOFTWARE!!
    //pnl_properties->add( new Prop_Button("Merge Manually", nullptr, [&](void*) {
    //    printf("\n\nNot implemented, yet\n\n");
    //}));
    //pnl_properties->add( new Prop_Divider(' ') );
    
    string fn_merged = "merged.ply";
    pnl_properties->add( new Prop_Text("Filename", TEXT_LEFT));
    pnl_properties->add( new Prop_Bind_String(": ",&fn_merged) );
    pnl_properties->add( new Prop_Button("Save",nullptr,[&](void*){
        auto m = new SimpleMesh();
        m->vpos = vpos;
        for( int i_f = 0; i_f < finds.size(); i_f++ ) {
            if( !fshow[i_f] ) continue;
            m->finds.push_back(finds[i_f]);
        }
        m->clean();
        
        m->xform( -glob_offset, 1.0f / glob_scale );
        m->save_ply(fn_merged.c_str(), to_string("%s %s %s",m0->filename.c_str(),ma->filename.c_str(),mb->filename.c_str()).c_str());
        
        delete m;
    }));
    pnl_properties->add( new Prop_Divider(' ') );
    
    pnl_properties->add( new Prop_Text("Merge Options", TEXT_CENTER) );
    pnl_properties->add( new Prop_Text("Blend Verts", TEXT_LEFT) );
    auto prop_ab0a = new Prop_Bind_Float(" 0->a", &alpha_blend0a, 0.0f, 1.0f );
    prop_ab0a->lfn_change = [&](void*) { fn_update_pos(); };
    pnl_properties->add( prop_ab0a );
    auto prop_ab0b = new Prop_Bind_Float(" 0->b", &alpha_blend0b, 0.0f, 1.0f );
    prop_ab0b->lfn_change = [&](void*) { fn_update_pos(); };
    pnl_properties->add( prop_ab0b );
    pnl_properties->add( new Prop_Text(" Conflicted", TEXT_LEFT) );
    auto prop_abx = new Prop_Bind_Float("  0",&alpha_blend.x,0.0f,1.0f);
    prop_abx->data = &alpha_blend;
    prop_abx->lfn_change = [&](void*data){
        vec3f *ab = (vec3f*)data;
        float s = ab->x + ab->y + ab->z;
        ab->y += (1.0f - s) / 2.0f;
        ab->z += (1.0f - s) / 2.0f;
        if( ab->y < 0.0f ) { ab->z += ab->y; ab->y = 0.0f; }
        if( ab->z < 0.0f ) { ab->y += ab->z; ab->z = 0.0f; }
        fn_update_pos();
    };
    pnl_properties->add( prop_abx );
    auto prop_aby = new Prop_Bind_Float("  a",&alpha_blend.y,0.0f,1.0f);
    prop_aby->data = &alpha_blend;
    prop_aby->lfn_change = [&](void*data){
        vec3f *ab = (vec3f*)data;
        float s = ab->x + ab->y + ab->z;
        ab->x += (1.0f - s) / 2.0f;
        ab->z += (1.0f - s) / 2.0f;
        if( ab->x < 0.0f ) { ab->z += ab->x; ab->x = 0.0f; }
        if( ab->z < 0.0f ) { ab->x += ab->z; ab->z = 0.0f; }
        fn_update_pos();
    };
    pnl_properties->add( prop_aby );
    auto prop_abz = new Prop_Bind_Float("  b",&alpha_blend.z,0.0f,1.0f);
    prop_abz->data = &alpha_blend;
    prop_abz->lfn_change = [&](void*data){
        vec3f *ab = (vec3f*)data;
        float s = ab->x + ab->y + ab->z;
        ab->x += (1.0f - s) / 2.0f;
        ab->y += (1.0f - s) / 2.0f;
        if( ab->x < 0.0f ) { ab->y += ab->x; ab->x = 0.0f; }
        if( ab->y < 0.0f ) { ab->x += ab->y; ab->y = 0.0f; }
        fn_update_pos();
    };
    pnl_properties->add( prop_abz );
    
    pnl_properties->add( new Prop_Text("Part:Conflicts", TEXT_LEFT) );
    for( int i_a = 0; i_a < l_mmps0a.size(); i_a++ ) {
        string pname = to_string(" a%02d",i_a);
        bool first = true;
        for( int i_b = 0; i_b < l_mmps0b.size(); i_b++ ) {
            if( !conflicts[i_a][i_b] ) continue;
            char sep = ( first ? ':' : ',' ); first = false;
            pname += to_string("%cb%02d",sep,i_b);
        }
        auto p = new Prop_Bind_Bool(pname, &is_a[i_a]);
        p->data = new int(i_a);
        p->lfn_change = [&](void*data){
            int i_a = *(int*)data;
            printf("i_a=%d\n",i_a);
            if(is_a[i_a]) {
                for( int i_b = 0; i_b < l_mmps0b.size(); i_b++ ) {
                    if( conflicts[i_a][i_b] ) is_b[i_b] = false;
                }
            }
            fn_update_mergemesh();
        };
        pnl_properties->add( p );
    }
    for( int i_b = 0; i_b < l_mmps0b.size(); i_b++ ) {
        string pname = to_string(" b%02d",i_b);
        bool first = true;
        for( int i_a = 0; i_a < l_mmps0a.size(); i_a++ ) {
            if( !conflicts[i_a][i_b] ) continue;
            char sep = ( first ? ':' : ',' ); first = false;
            pname += to_string("%ca%02d",sep,i_a);
        }
        auto p = new Prop_Bind_Bool(pname, &is_b[i_b]);
        p->data = new int(i_b);
        p->lfn_change = [&](void*data){
            int i_b = *(int*)data;
            printf("i_b=%d\n",i_b);
            if(is_b[i_b]) {
                for( int i_a = 0; i_a < l_mmps0a.size(); i_a++ ) {
                    if( conflicts[i_a][i_b] ) is_a[i_a] = false;
                }
            }
            fn_update_mergemesh();
        };
        pnl_properties->add( p );
    }
    
    pnl_properties->add( new Prop_Divider() );
    add_camera_properties( camera );
    
    
    viewer->run();
}


void draw_mesh( Mesh *m, vector<ivert> &vm, vector<iface> &fm, vec3f c_e_unm, Mesh *m_, vector<vector<ivert>> &vknn, vector<vector<ivert>> &fknn ) {
    static timer uptimer;
    
    vector<vec3f> &vpos = m->vpos;
    vector<vec3f> &fpos = m->fpos;
    vector<vector<ivert>> &finds = m->finds;
    vector<vec3f> &fnorms = m->fnorms;
    int n_v = m->n_v;
    int n_f = m->n_f;
    
    vector<vec3f> &vpos_ = m_->vpos;
    vector<vec3f> &fpos_ = m_->fpos;
    vector<vector<ivert>> &finds_ = m_->finds;
    vector<vec3f> &fnorms_ = m_->fnorms;
    int n_f_ = m_->n_f;
    
    vec3f c_n = { 0.7f, 0.7f, 0.7f }, c_n_ = c_n * 0.5f;
    vec3f c_unm = { 0.5f, 0.5f, 0.5f }, c_unm_ = c_unm * 0.5f;
    vec3f c_e_unm_ = c_e_unm * 0.5f, c_e_unm__ = c_e_unm_ * 0.5f;
    
    float morph_blend = uptimer.elapsed_sine(0.5f);
    float morph_blend_ = 1.0f - morph_blend;
    
    if( glob_mesh_faces ) {
        glBegin(GL_TRIANGLES);
        for( int i_f = 0; i_f < n_f; i_f++ ) {
            if( fm[i_f] == -1 ) {
                if( glob_mesh_faces_sameonly ) continue;
                set_material(GL_DIFFUSE,c_unm,c_unm_);
                set_material(GL_EMISSION,c_e_unm,c_e_unm_);
            } else {
                if( glob_mesh_faces_chgsonly ) continue;
                set_material(GL_DIFFUSE,c_n,c_n_);
                set_material(GL_EMISSION, zero3f, zero3f);
            }
            
            draw_face( finds[i_f], fnorms[i_f], vpos );
        }
        glEnd();
        set_material(GL_EMISSION, zero3f, zero3f);
    }
    
    if( glob_mesh_components ) {
        srand(0);
        vector<vec3f> comp_colors, comp_colors_;
        for( int i_c = 0; i_c < m->components.size(); i_c++ ) {
            vec3f c = { (float)((rand()%128)+128)/256.0f,(float)((rand()%128)+128)/256.0f,(float)((rand()%128)+128)/256.0f };
            comp_colors.push_back( c );
            comp_colors_.push_back( c * 0.5f );
        }
        
        glBegin(GL_TRIANGLES);
        for( int i_f = 0; i_f < n_f; i_f++ ) {
            set_material(GL_DIFFUSE,comp_colors[m->fnodes[i_f]->i_comp],comp_colors_[m->fnodes[i_f]->i_comp]);
            vector<ivert> &_vinds = finds[i_f];
            int n = _vinds.size();
            const vec3f &norm = fnorms[i_f];
            glsNormal(norm);
            int i0 = 0;
            for( int i = 1; i < n; i++ ) {
                int i1 = i%n, i2 = (i+1)%n;
                glsVertex(vpos[_vinds[i0]]);
                glsVertex(vpos[_vinds[i1]]);
                glsVertex(vpos[_vinds[i2]]);
            }
        }
        glEnd();
    }
    
    if( glob_morph_faces ) {
        glBegin(GL_TRIANGLES);
        set_material(GL_DIFFUSE,c_n,c_n_);
        for( int i_f = 0; i_f < n_f; i_f++ ) {
            int i_f_ = fm[i_f]; if(i_f_==-1) continue;
            
            vector<ivert> &_vinds = finds[i_f];
            int n = _vinds.size();
            const vec3f &norm = fnorms[i_f];
            const vec3f &norm_ = fnorms_[i_f_];
            
            glsNormal(normalize((norm*morph_blend)+(norm_*morph_blend_)));
            
            int i0 = 0;
            for( int i = 1; i < n; i++ ) {
                int i1 = i%n, i2 = (i+1)%n;
                int iv0 = _vinds[i0], iv1 = _vinds[i1], iv2 = _vinds[i2];
                int iv0_ = vm[iv0], iv1_ = vm[iv1], iv2_ = vm[iv2];
                if( iv0_ == -1 || iv1_ == -1 || iv2_ == -1 ) continue;
                glsVertex((vpos[iv0]*morph_blend) + (vpos_[iv0_]*morph_blend_));
                glsVertex((vpos[iv1]*morph_blend) + (vpos_[iv1_]*morph_blend_));
                glsVertex((vpos[iv2]*morph_blend) + (vpos_[iv2_]*morph_blend_));
            }
        }
        glEnd();
    }
    
    glDepthRange(0.0f,0.999999f);
    glLineWidth(1.0f);
    glEnable(GL_LINE_SMOOTH);
    glDisable(GL_LIGHTING);
    
    if( glob_knn_vert ) {
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        
        glColor4f(0.2f,0.2f,0.8f,0.1f);
        
        glBegin(GL_LINES);
        for( int i_v = 0; i_v < n_v; i_v++ ) {
            for( int i_v_ : vknn[i_v] ) {
                glsVertex(vpos[i_v]);
                glsVertex(vpos_[i_v_]);
            }
        }
        glEnd();
    }
    if( glob_knn_face ) {
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        
        glColor4f(0.2f,0.2f,0.8f,0.1f);
        
        glBegin(GL_LINES);
        for( int i_f = 0; i_f < n_f; i_f++ ) {
            for( int i_f_ : fknn[i_f] ) {
                glsVertex(fpos[i_f]);
                glsVertex(fpos_[i_f_]);
            }
        }
        glEnd();
    }
    
    if( glob_mesh_edges ) {
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        
        switch( glob_mesh_edges_darkness ) {
            case 1: { glColor4f(0.2f,0.2f,0.2f,0.1f); glLineWidth(1.0f); } break;
            case 2: { glColor4f(0.2f,0.2f,0.2f,0.5f); glLineWidth(1.0f); } break;
            case 3: { glColor4f(0.2f,0.2f,0.2f,0.7f); glLineWidth(1.0f); } break;
            case 4: { glColor4f(0.2f,0.2f,0.2f,0.5f); glLineWidth(2.0f); } break;
            case 5: { glColor4f(0.2f,0.2f,0.2f,0.7f); glLineWidth(2.0f); } break;
        }
        
        glBegin(GL_LINES);
        for( int i_f = 0; i_f < n_f; i_f++ ) {
            vector<ivert> &_vinds = finds[i_f];
            int n = _vinds.size();
            for( int i0 = 0; i0 < n; i0++ ) {
                int i1 = (i0+1)%n;
                glsVertex(vpos[_vinds[i0]]);
                glsVertex(vpos[_vinds[i1]]);
            }
        }
        glEnd();
    }
    
    if( glob_morph_verts ) {
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glPointSize(3.0f);
        glColor4f(0.2f,0.2f,0.2f,0.5f);
        glBegin(GL_POINTS);
        for( int i_v = 0; i_v < n_v; i_v++ ) {
            int i_v_ = vm[i_v];
            if( i_v_ == -1 ) continue;
            glsVertex( (vpos[i_v]*morph_blend) + (vpos_[i_v_]*morph_blend_) );
        }
        glEnd();
        glDisable(GL_BLEND);
    }
    
    if( glob_connect_matched_verts ) {
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glLineWidth(1.0f);
        glColor4f(0.5f,0.5f,0.8f,0.5f);
        glBegin(GL_LINES);
        for( int i_v = 0; i_v < n_v; i_v++ ) {
            int i_v_ = vm[i_v];
            if( i_v_ == -1 ) continue;
            glsVertex( vpos[i_v] ); glsVertex( vpos_[i_v_] );
        }
        glEnd();
        glDisable(GL_BLEND);
    }
    if( glob_connect_matched_faces ) {
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glLineWidth(1.0f);
        glColor4f(0.5f,0.5f,0.8f,0.5f);
        glBegin(GL_LINES);
        for( int i_f = 0; i_f < n_f; i_f++ ) {
            int i_f_ = fm[i_f]; if( i_f_ == -1 ) continue;
            glsVertex( fpos[i_f] ); glsVertex( fpos_[i_f_] );
        }
        glEnd();
        glDisable(GL_BLEND);
    }
    
    if( glob_overlay_edges ) {
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glLineWidth(1.0f);
        glBegin(GL_LINES);
        glColor4f(0.8f,0.8f,0.8f,0.25f);
        for( int i_f = 0; i_f < n_f_; i_f++ ) {
            vector<ivert> &_vinds = finds_[i_f];
            int n = _vinds.size();
            for( int i0 = 0; i0 < n; i0++ ) {
                int i1 = (i0+1)%n;
                glsVertex(vpos_[_vinds[i0]]);
                glsVertex(vpos_[_vinds[i1]]);
            }
        }
        glColor4f(0.6f,0.6f,0.8f,0.25f);
        for( int i_f = 0; i_f < n_f; i_f++ ) {
            vector<ivert> &_vinds = finds[i_f];
            int n = _vinds.size();
            for( int i0 = 0; i0 < n; i0++ ) {
                int i1 = (i0+1)%n;
                glsVertex(vpos[_vinds[i0]]);
                glsVertex(vpos[_vinds[i1]]);
            }
        }
        glEnd();
        glDisable(GL_BLEND);
    }
    
    glEnable(GL_LIGHTING);
    glDepthRange(0.0f,1.0f);
}



void draw_mesh( Mesh *m, vector<ivert> &vm, vector<iface> &fm, material &mat_unmatch, material &mat_unmatch_ ) {
    vector<vec3f> &vpos = m->vpos;
    vector<vector<ivert>> &finds = m->finds;
    vector<vec3f> &fnorms = m->fnorms;
    int n_f = m->n_f;
    
    if( glob_mesh_faces ) {
        glBegin(GL_TRIANGLES);
        for( int i_f = 0; i_f < n_f; i_f++ ) {
            if( fm[i_f] == -1 && fm[i_f] == -1 ) {
                if( glob_mesh_faces_sameonly ) continue;
                set_material( mat_unmatch, mat_unmatch_ );
            } else {
                if( glob_mesh_faces_chgsonly ) continue;
                set_material( mat_match, mat_match_ );
            }
            
            draw_face( finds[i_f], fnorms[i_f], vpos );
        }
        glEnd();
        set_material(mat_neutral,mat_neutral);
    }
    
    glDepthRange(0.0f,0.999999f);
    glLineWidth(1.0f);
    glEnable(GL_LINE_SMOOTH);
    glDisable(GL_LIGHTING);
    
    if( glob_mesh_edges ) {
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        
        switch( glob_mesh_edges_darkness ) {
            case 1: { glColor4f(0.2f,0.2f,0.2f,0.1f); glLineWidth(1.0f); } break;
            case 2: { glColor4f(0.2f,0.2f,0.2f,0.5f); glLineWidth(1.0f); } break;
            case 3: { glColor4f(0.2f,0.2f,0.2f,0.7f); glLineWidth(1.0f); } break;
            case 4: { glColor4f(0.2f,0.2f,0.2f,0.5f); glLineWidth(2.0f); } break;
            case 5: { glColor4f(0.2f,0.2f,0.2f,0.7f); glLineWidth(2.0f); } break;
        }
        
        glBegin(GL_LINES);
        for( int i_f = 0; i_f < n_f; i_f++ ) {
            vector<ivert> &_vinds = finds[i_f];
            int n = _vinds.size();
            for( int i0 = 0; i0 < n; i0++ ) {
                int i1 = (i0+1)%n;
                glsVertex(vpos[_vinds[i0]]);
                glsVertex(vpos[_vinds[i1]]);
            }
        }
        glEnd();
    }
    
    
    glEnable(GL_LIGHTING);
    glDepthRange(0.0f,1.0f);
}

void draw_mesh( Mesh *m, Mesh *m_prev, vector<ivert> &vm, vector<iface> &fm, material &mat_unmatch, material &mat_unmatch_ ) {
    vector<vec3f> &vpos = m->vpos;
    vector<vec3f> &vpos_prev = m_prev->vpos;
    vector<vector<ivert>> &finds = m->finds;
    vector<vec3f> &fnorms = m->fnorms;
    int n_f = m->n_f;
    
    if( glob_mesh_faces ) {
        glBegin(GL_TRIANGLES);
        for( int i_f = 0; i_f < n_f; i_f++ ) {
            if( fm[i_f] == -1 && fm[i_f] == -1 ) {
                if( glob_mesh_faces_sameonly ) continue;
                set_material( mat_unmatch, mat_unmatch_ );
                draw_face( finds[i_f], fnorms[i_f], vpos );
            } else {
                if( glob_mesh_faces_chgsonly ) continue;
                set_material( mat_match, mat_match_ );
                vec3f color0 = mat_match.diffuse, color1 = { mat_match.diffuse.x * 0.5f, mat_match.diffuse.y * 0.5f, 1.0f };
                vec3f color0_ = mat_match_.diffuse, color1_ = { mat_match_.diffuse.x * 0.5f, mat_match_.diffuse.y * 0.5f, 1.0f };
                draw_face(finds[i_f], fnorms[i_f], vpos, vm, vpos_prev, color0, color0_, color1, color1_ );
            }
        }
        glEnd();
        set_material( mat_neutral, mat_neutral );
    }
    
    glDepthRange(0.0f,0.999999f);
    glLineWidth(1.0f);
    glEnable(GL_LINE_SMOOTH);
    glDisable(GL_LIGHTING);
    
    if( glob_mesh_edges ) {
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        
        switch( glob_mesh_edges_darkness ) {
            case 1: { glColor4f(0.2f,0.2f,0.2f,0.1f); glLineWidth(1.0f); } break;
            case 2: { glColor4f(0.2f,0.2f,0.2f,0.5f); glLineWidth(1.0f); } break;
            case 3: { glColor4f(0.2f,0.2f,0.2f,0.7f); glLineWidth(1.0f); } break;
            case 4: { glColor4f(0.2f,0.2f,0.2f,0.5f); glLineWidth(2.0f); } break;
            case 5: { glColor4f(0.2f,0.2f,0.2f,0.7f); glLineWidth(2.0f); } break;
        }
        
        glBegin(GL_LINES);
        for( int i_f = 0; i_f < n_f; i_f++ ) {
            vector<ivert> &_vinds = finds[i_f];
            int n = _vinds.size();
            for( int i0 = 0; i0 < n; i0++ ) {
                int i1 = (i0+1)%n;
                glsVertex(vpos[_vinds[i0]]);
                glsVertex(vpos[_vinds[i1]]);
            }
        }
        glEnd();
    }
    
    
    glEnable(GL_LIGHTING);
    glDepthRange(0.0f,1.0f);
}


void draw_mesh( Mesh *m, vector<ivert> &vm0, vector<iface> &fm0, vector<ivert> &vm1, vector<iface> &fm1, bool ab ) {
    vector<vec3f> &vpos = m->vpos;
    vector<vector<ivert>> &finds = m->finds;
    vector<vec3f> &fnorms = m->fnorms;
    int n_f = m->n_f;
    
    if( glob_mesh_faces ) {
        glBegin(GL_TRIANGLES);
        for( int i_f = 0; i_f < n_f; i_f++ ) {
            if( fm0[i_f] == -1 && fm1[i_f] == -1 ) {
                if( glob_mesh_faces_sameonly ) continue;
                if( ab ) set_material( mat_unmatch0ab, mat_unmatch0ab_ );
                else set_material( mat_unmatch01, mat_unmatch01_ );
            } else if( fm0[i_f] == -1 ) {
                if( glob_mesh_faces_sameonly ) continue;
                if( ab ) set_material( mat_unmatch0a, mat_unmatch0a_ );
                else set_material( mat_unmatch1, mat_unmatch1_ );
            } else if( fm1[i_f] == -1 ) {
                if( glob_mesh_faces_sameonly ) continue;
                if( ab ) set_material( mat_unmatch0b, mat_unmatch0b_ );
                else set_material( mat_unmatch0, mat_unmatch0_ );
            } else {
                if( glob_mesh_faces_chgsonly ) continue;
                set_material( mat_match, mat_match_ );
            }
            
            draw_face( finds[i_f], fnorms[i_f], vpos );
        }
        glEnd();
        set_material( mat_neutral, mat_neutral );
        set_material(GL_EMISSION, zero3f, zero3f);
    }
    
    glDepthRange(0.0f,0.999999f);
    glLineWidth(1.0f);
    glEnable(GL_LINE_SMOOTH);
    glDisable(GL_LIGHTING);
    
    if( glob_mesh_edges ) {
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        
        switch( glob_mesh_edges_darkness ) {
            case 1: { glColor4f(0.2f,0.2f,0.2f,0.1f); glLineWidth(1.0f); } break;
            case 2: { glColor4f(0.2f,0.2f,0.2f,0.5f); glLineWidth(1.0f); } break;
            case 3: { glColor4f(0.2f,0.2f,0.2f,0.7f); glLineWidth(1.0f); } break;
            case 4: { glColor4f(0.2f,0.2f,0.2f,0.5f); glLineWidth(2.0f); } break;
            case 5: { glColor4f(0.2f,0.2f,0.2f,0.7f); glLineWidth(2.0f); } break;
        }
        
        glBegin(GL_LINES);
        for( int i_f = 0; i_f < n_f; i_f++ ) {
            vector<ivert> &_vinds = finds[i_f];
            int n = _vinds.size();
            for( int i0 = 0; i0 < n; i0++ ) {
                int i1 = (i0+1)%n;
                glsVertex(vpos[_vinds[i0]]);
                glsVertex(vpos[_vinds[i1]]);
            }
        }
        glEnd();
    }
    
    
    glEnable(GL_LIGHTING);
    glDepthRange(0.0f,1.0f);
}

void draw_mesh( Mesh *m, Mesh *m_prev, vector<ivert> &vm0, vector<iface> &fm0, vector<ivert> &vm1, vector<iface> &fm1, bool ab ) {
    vector<vec3f> &vpos = m->vpos;
    vector<vector<ivert>> &finds = m->finds;
    vector<vec3f> &fnorms = m->fnorms;
    vector<vec3f> &vpos_prev = m_prev->vpos;
    int n_f = m->n_f;
    
    if( glob_mesh_faces ) {
        glBegin(GL_TRIANGLES);
        for( int i_f = 0; i_f < n_f; i_f++ ) {
            if( fm0[i_f] == -1 || fm1[i_f] == -1 ) {
                if( glob_mesh_faces_sameonly ) continue;
                if( fm0[i_f] == -1 && fm1[i_f] == -1 ) {
                    if( ab ) set_material( mat_unmatch0ab, mat_unmatch0ab_ );
                    else set_material( mat_unmatch01, mat_unmatch01_ );
                } else if( fm0[i_f] == -1 ) {
                    if( ab ) set_material( mat_unmatch0a, mat_unmatch0a_ );
                    else set_material( mat_unmatch1, mat_unmatch1_ );
                } else if( fm1[i_f] == -1 ) {
                    if( ab ) set_material( mat_unmatch0b, mat_unmatch0b_ );
                    else set_material( mat_unmatch0, mat_unmatch0_ );
                }
                draw_face( finds[i_f], fnorms[i_f], vpos );
            } else {
                if( glob_mesh_faces_chgsonly ) continue;
                
                set_material( mat_match, mat_match_ );
                
                vec3f color0 = mat_match.diffuse, color1 = { mat_match.diffuse.x * 0.5f, mat_match.diffuse.y * 0.5f, 1.0f };
                vec3f color0_ = mat_match_.diffuse, color1_ = { mat_match_.diffuse.x * 0.5f, mat_match_.diffuse.y * 0.5f, 1.0f };
                
                draw_face(finds[i_f], fnorms[i_f], vpos, vm0, vpos_prev, color0, color0_, color1, color1_ );
            }
        }
        glEnd();
        set_material( mat_neutral, mat_neutral );
        set_material(GL_EMISSION, zero3f,zero3f);
    }
    
    glDepthRange(0.0f,0.999999f);
    glLineWidth(1.0f);
    glEnable(GL_LINE_SMOOTH);
    glDisable(GL_LIGHTING);
    
    if( glob_mesh_edges ) {
        glEnable(GL_BLEND);
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        
        switch( glob_mesh_edges_darkness ) {
            case 1: { glColor4f(0.2f,0.2f,0.2f,0.1f); glLineWidth(1.0f); } break;
            case 2: { glColor4f(0.2f,0.2f,0.2f,0.5f); glLineWidth(1.0f); } break;
            case 3: { glColor4f(0.2f,0.2f,0.2f,0.7f); glLineWidth(1.0f); } break;
            case 4: { glColor4f(0.2f,0.2f,0.2f,0.5f); glLineWidth(2.0f); } break;
            case 5: { glColor4f(0.2f,0.2f,0.2f,0.7f); glLineWidth(2.0f); } break;
        }
        
        glBegin(GL_LINES);
        for( int i_f = 0; i_f < n_f; i_f++ ) {
            vector<ivert> &_vinds = finds[i_f];
            int n = _vinds.size();
            for( int i0 = 0; i0 < n; i0++ ) {
                int i1 = (i0+1)%n;
                glsVertex(vpos[_vinds[i0]]);
                glsVertex(vpos[_vinds[i1]]);
            }
        }
        glEnd();
    }
    
    
    glEnable(GL_LIGHTING);
    glDepthRange(0.0f,1.0f);
}



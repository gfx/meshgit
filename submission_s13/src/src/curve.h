#ifndef _CURVE_H_
#define _CURVE_H_

#include "object.h"
#include "std.h"
#include "vec.h"
#include "gls.h"
#include "geom.h"

// TODO: remove most curve code


/*
template <typename R>
struct range1 {
	R min, max;
};
typedef range1<float> range1f;



template<typename T>
struct Curve : object {
    constexpr static const float _epsilon = 0.00000001f;
    
    virtual void init() { }
    
    virtual float minu() = 0;
    virtual float maxu() = 0;
    
    virtual range1f interval() = 0;
    
    virtual T p(float u) = 0;
    virtual T dp(float u) = 0;
    
    virtual void draw_control() { }
};

typedef Curve<float> Curve1f;
typedef Curve<vec2f> Curve2f;
typedef Curve<vec3f> Curve3f;

template<typename T>
struct Circle : Curve<T> {
    T       o;
    T       x;
    T       y;
    float   r;
    
    void init_afterload() { orthonormalize(x,y); }
    
    virtual float minu() { return 0; }
    virtual float maxu() { return 1; }
    
    virtual range1f interval() { return range1f(0,1); }
    
    virtual T p(float u) {
        u = clamp(u,minu(),maxu());
        float a = u * 2 * pif;
        return o + r * (x * cos(a) + y * sin(a));
    }
    virtual T dp(float u) {
        u = clamp(u,minu(),maxu());
        float a = u * 2 * pif;
        return - x * sin(a) + y * cos(a); 
    }
};

struct Circle2f : Circle<vec2f> {
    Circle2f() { o = zero2f; x = x2f; y = y2f; r = 1; }
};

struct Circle3f : Circle<vec3f> {
    Circle3f() { o = zero3f; x = x3f; y = y3f; r = 1; }    

    virtual void draw_control();
};

template<typename T>
struct BezierSpline : Curve<T> {
    vector<float>       ct;
    vector<T>           cp;
    int                 degree = 1;
    
    int segments() { return cp.size() / (degree+1); }
    
    virtual float minu() { if (ct.empty()) return 0; else return ct.front(); }
    virtual float maxu() { if (ct.empty()) return segments(); else return ct.back(); }
    
    virtual range1f interval() { return range1f( (ct.empty()) ? 0 : ct.front(), (ct.empty()) ? segments() : ct.back() ); }
    
    void _segment(float u, float& t, int& s) {
        u = clamp(u,minu(),maxu()-Curve<T>::_epsilon);
        if(ct.empty()) {
            s = int(floor(u));
            t = u - s;
        } else {
            for(s = 0; ct[s+1] < u; s ++);
            t = (u - ct[s]) / (ct[s+1] - ct[s]);
        }
    }
    
    virtual T p(float u) {
        error_if_not(degree > 0 and degree < 4, "unsupported degree");
        float t; int s;
        _segment(u,t,s);
        T p = T();
        for(int i = 0; i <= degree; i ++) p += cp[s*(degree+1)+i]*bernstein(t,i,degree);
        return p;
    }
    
    virtual T dp(float u) {        
        error_if_not(degree > 0 and degree < 4, "unsupported degree");
        float t; int s;
        _segment(u,t,s);
        T p = T();
        for(int i = 0; i <= degree; i ++) p += cp[s*(degree+1)+i]*bernstein_derivative(t,i,degree);
        return p;
    }        
};

struct BezierSpline1f : BezierSpline<float> {
};

struct BezierSpline2f : BezierSpline<vec2f> {
};

struct BezierSpline3f : BezierSpline<vec3f> {
    virtual void draw_control();
};
 */

#endif

#ifndef _DEPGRAPH_H_
#define _DEPGRAPH_H_

#include "debug.h"
#include "globals.h"
#include "std.h"
#include "std_utils.h"
#include "vec.h"
#include "ext/common/frame.h"
#include <unordered_map>

using namespace std;

namespace BrushName {
    enum Enum {
        Unspecified,
        Blob, Brush,
        Clay, ClayStrips, Crease,
        Draw,
        FillDeepen, FlattenContrast,
        Grab,
        InflateDeflate,
        Layer,
        Mask,
        Nudge,
        PinchMagnify,
        Polish,
        ScrapePeaks,
        SculptDraw,
        Smooth,
        SnakeHook,
        Thumb,
        Twist,
        Mixed
    };
    const vector<string> Names {
        "Unspecified", "Blob", "Brush", "Clay", "Clay Strips", "Crease",
        "Draw", "Fill/Deepen", "Flatten/Contrast", "Grab", "Inflate/Deflate",
        "Layer", "Mask", "Nudge", "Pinch/Magnify", "Polish", "Scrape/Peaks",
        "SculptDraw", "Smooth", "Snake Hook", "Thumb", "Twist",
        "Mixed"
    };
    extern map<string,Enum> Map_Name_Enum;
};

namespace BrushModifier {
    enum Enum {
        Normal,
        Invert,
        Smooth
    };
    const vector<string> Names {
        "Normal", "Invert", "Smooth"
    };
    extern map<string,Enum> Map_Name_Enum;
};

namespace BrushClassification {
    enum Enum {
        None, Vol_Add, Vol_Del, Smooth, Crease, Move, Feat_Add, Feat_Del
    };
    const vector<string> Names {
        "None", "Volume Add", "Volume Del", "Smooth", "Crease", "Move", "Feature Add", "Feature Del"
    };
};

struct StrokeReprData {
    int   i_f;      // index of face
    float ba, bb;   // barycentric coords of face
    float n;        // displace along normal of face
    bool  brk;      // discontinuous (broken) stroke
};

struct StrokeData {
    BrushName::Enum brush;
    BrushModifier::Enum mod;
    vector<vec3f> pos; vector<bool> brk;        // original stroke info
    int mirror;                                 // 1=x-axis, 2=y-axis, 4=z-axis
    vector<StrokeReprData> rpos;                // projected stroke info
    
    vector<BrushName::Enum> brushes;
    vector<BrushModifier::Enum> mods;
};

struct CameraData {
    bool ortho;
    float dist;
    frame3f frame;
};

struct Statistics {
    float vol_delta;
    float area_pre, area_post;
    vec3f avgnorm_pre, avgnorm_post;
    float stddevnorm_pre, stddevnorm_post;
    BrushClassification::Enum classification;
    
    float revg_visual_importance = 0.0f;
};

struct ReprMoveVert {
    int i_v, i_f0, i_f1;
    vec3f v0, v1;
};

struct AddDelData {
    vector<int> li_add;
    vector<int> li_del;
};

struct StartEndData {
    int i0, i1;
};



class OpNode {
public:
    int idx;
    vector<OpNode*> nodes;
    OpNode* node_up = nullptr;
    
    AddDelData edges;
    AddDelData faces;
    vector<ReprMoveVert> reprchanges;
    
    CameraData camera;
    StrokeData stroke;
    Statistics stats;
    
    vec3f pos0 = zero3f, pos1 = zero3f, color = one3f;
    bool bookmarked = false;
    
    unordered_set<OpNode*> parents;
    unordered_set<OpNode*> children;
    
    ~OpNode() {
        edges.li_add.clear(); edges.li_del.clear();
        faces.li_add.clear(); faces.li_del.clear();
        reprchanges.clear();
        parents.clear(); children.clear();
        nodes.clear();
    }
    
    // CAREFUL!  this will break the dependency graph
    void detach_all() {
        while( !parents.empty() ) detach_parent(*parents.begin());
        while( !children.empty() ) detach_child(*children.begin());
    }
    
    void detach_parent( OpNode *parent ) {
        if( !parent || !parents.count(parent) ) return;
        parents.erase(parent);
        parent->children.erase(this);
    }
    void detach_child( OpNode *child ) {
        if( !child || !children.count(child) ) return;
        children.erase(child);
        child->parents.erase(this);
    }
    
    bool is_root() { return parents.size()==0; }
    bool is_leaf() { return children.size()==0; }
    
    bool is_child( OpNode *other ) { return( children.count(other) ); }
    bool is_parent( OpNode *other ) { return( parents.count(other) ); }
    
    // recursive depth-first search with memoization
    inline bool is_dep_on( OpNode *other ) {
        unordered_set<OpNode*> _;
        return _is_dep_on( other, _ );
    }
    bool _is_dep_on( OpNode *other, unordered_set<OpNode*> &visited ) {
        if( visited.count(this) ) return false;
        visited.insert(this);
        if( other == this ) return true;
        for( auto parent : parents ) if( parent->_is_dep_on( other, visited ) ) return true;
        return false;
    }
    
    inline bool is_dep_on( unordered_set<OpNode*> &others ) {
        unordered_set<OpNode*> visited;
        return _is_dep_on( others, visited );
    }
    bool _is_dep_on( unordered_set<OpNode*> &others, unordered_set<OpNode*> &visited ) {
        if( visited.count(this) ) return false;
        if( others.count(this) ) return true;
        visited.insert(this);
        for( OpNode *p : parents ) if( p->_is_dep_on( others, visited ) ) return true;
        return false;
    }
    
    inline bool has_dep( unordered_set<OpNode*> &others ) {
        unordered_set<OpNode*> visited;
        return _has_dep( others, visited );
    }
    bool _has_dep( unordered_set<OpNode*> &others, unordered_set<OpNode*> &visited ) {
        if( visited.count(this) ) return false;
        if( others.count(this) ) return true;
        visited.insert(this);
        for( OpNode *c : children ) if( c->_has_dep( others, visited ) ) return true;
        return false;
    }
    
    int get_distance_to( OpNode *other, unordered_set<OpNode*> &visited, int d ) {
        if( visited.count(this) ) return -1;
        visited.insert(this);
        if( other == this ) return d;
        for( auto c : children ) {
            int d_ = c->get_distance_to( other, visited, d+1 );
            if( d_ != -1 ) return d_;
        }
        return -1;
    }
    int get_distance_to( OpNode *other ) {
        unordered_set<OpNode*> _; return get_distance_to(other,_,0);
    }
    
    void get_sub_indices( unordered_set<int> &inds ) const {
        if( nodes.empty() ) { inds.insert(idx); return; }
        for( OpNode *sub : nodes ) sub->get_sub_indices(inds);
    }
    void get_sub_nodes( unordered_set<OpNode*> &nodes_sub ) {
        if( nodes.empty() ) { nodes_sub.insert(this); return; }
        for( OpNode *sub : nodes ) sub->get_sub_nodes(nodes_sub);
    }
    
    
    inline void clear_excessive_parenting( bool recurse=true ) {
        if( recurse ) {
            unordered_set<OpNode*> visited; visited.reserve(2000);
            _clear_excessive_parenting(visited);
        } else {
            _clear_excessive_parenting();
        }
    }
    void _clear_excessive_parenting( unordered_set<OpNode*> &visited ) {
        if( visited.count(this) ) return;
        for( OpNode *p : parents ) if( !visited.count(p) ) return;
        visited.insert(this);
        _clear_excessive_parenting();
        for( OpNode *c : children ) c->_clear_excessive_parenting(visited);
    }
    void _clear_excessive_parenting() {
        unordered_set<OpNode*> pkill;
        for( OpNode *p0 : parents ) {
            for( OpNode *p1 : parents ) {
                if( p0 != p1 && p0->is_dep_on(p1) ) pkill.insert(p1);
            }
        }
        for( OpNode *p : pkill ) {
            parents.erase(p);
            p->children.erase(this);
        }
    }
    
    
    void update_brush_to_sub() {
        if( nodes.empty() ) {
            stroke.brush = BrushName::Unspecified;
        } else {
            stroke.brush = (*nodes.begin())->stroke.brush;
            for( OpNode *s : nodes ) {
                if( s->stroke.brush != stroke.brush ) {
                    stroke.brush = BrushName::Mixed;
                    return;
                }
            }
        }
    }
    
    void update_to_sub() { update_to_sub(true,true,true); }
    void update_to_sub(bool upd_strokes, bool upd_deladd, bool upd_repr) {
        if( upd_strokes ) update_brush_to_sub();
        
        if( upd_deladd ) {
            edges.li_add.clear(); edges.li_del.clear();
            faces.li_add.clear(); faces.li_del.clear();
        }
        
        if( nodes.empty() ) return;
        
        //unordered_set<OpNode*> s_nodes; get_sub_nodes(s_nodes);
        //vector<OpNode*> l_nodes(s_nodes.begin(), s_nodes.end());
        //sort(l_nodes.begin(),l_nodes.end(),[](OpNode*a,OpNode*b)->bool { return a->idx<b->idx; });
        vector<OpNode*> &l_nodes = nodes;
        
        unordered_map<int,int> m_iv_irmv;
        
        int n_nodes = l_nodes.size();
        for( int i = 0; i < n_nodes; i++ ) {
            OpNode *n0 = l_nodes[i];
            OpNode *n1 = l_nodes[n_nodes-1-i];
            
            if( upd_deladd ) {
                sorted_del_add( edges.li_add, n0->edges.li_del, n0->edges.li_add );
                sorted_del_add( edges.li_del, n1->edges.li_add, n1->edges.li_del );
                sorted_del_add( faces.li_add, n0->faces.li_del, n0->faces.li_add );
                sorted_del_add( faces.li_del, n1->faces.li_add, n1->faces.li_del );
            }
            
            if( upd_repr ) {
                for( ReprMoveVert &rmv : n0->reprchanges ) {
                    if( m_iv_irmv.count(rmv.i_v) ) {
                        int irmv = m_iv_irmv[rmv.i_v];
                        reprchanges[irmv].i_f1 = rmv.i_f1;
                        reprchanges[irmv].v1 = rmv.v1;
                    } else {
                        m_iv_irmv[rmv.i_v] = reprchanges.size();
                        reprchanges.push_back( rmv );
                    }
                }
            }
        }
    }
    
    void traverse_dfs_parents( function<void(OpNode*node)> fn ) {
        unordered_set<OpNode*> touched; _traverse_dfs_parents( fn, touched );
    }
    void _traverse_dfs_parents( function<void(OpNode*node)> fn, unordered_set<OpNode*> &touched ) {
        if( touched.count(this) ) return;
        for( OpNode *p : parents ) if( touched.count(p)==0 ) return;
        touched.insert(this);
        fn(this);
        for( auto c : children ) c->_traverse_dfs_parents(fn,touched);
    }
    
    void traverse_dfs( function<void(OpNode*node)> fn ) {
        unordered_set<OpNode*> touched; _traverse_dfs( fn, touched );
    }
    void _traverse_dfs( function<void(OpNode*node)> fn, unordered_set<OpNode*> &touched ) {
        if( touched.count(this) ) return;
        touched.insert(this);
        fn(this);
        for( auto c : children ) c->_traverse_dfs(fn,touched);
    }
    
    void traverse_dfs_stoppable( function<bool(OpNode*node)> fn ) {
        unordered_set<OpNode*> touched; _traverse_dfs_stoppable( fn, touched );
    }
    void _traverse_dfs_stoppable( function<bool(OpNode*node)> fn, unordered_set<OpNode*> &touched ) {
        if( !touched.count(this) ) {
            touched.insert(this);
            if( fn(this) ) {
                for( auto c : children ) c->_traverse_dfs_stoppable(fn,touched);
            }
        }
    }
    
    
    void update_position( function<vec3f(const vector<int>&l_if)> avg_face_pos ) {
        traverse_dfs_parents([&avg_face_pos](OpNode*node) {
            float x_max = 0.0f; for( OpNode *p : node->parents ) x_max = max( x_max, p->pos0.x );
            vec3f pavg = avg_face_pos( node->faces.li_add );
            node->pos0 = { x_max+1.0f, pavg.y, pavg.z };
            node->pos1 = { (float)node->idx, 0.0f, 0.0f };
        });
    }
    
    void update_color() {
        traverse_dfs_parents([](OpNode*node) {
            if( node->parents.size() == 1 && (*node->parents.begin())->children.size() == 1 ) {
                node->color = (*node->parents.begin())->color;
            } else {
                float rcr = (float)rand() / (float)RAND_MAX * 0.5f + 0.25f;
                float rcg = (float)rand() / (float)RAND_MAX * 0.5f + 0.25f;
                float rcb = (float)rand() / (float)RAND_MAX * 0.5f + 0.25f;
                node->color = { rcr, rcg, rcb };
            }
        });
    }
    
    void get_descendents( unordered_set<OpNode*> &family ) {
        for( auto child : children ) {
            if( family.count(child) ) continue;
            family.insert(child);
            child->get_descendents(family);
        }
    }
    void get_ancestors( unordered_set<OpNode*> &family ) {
        for( auto parent : parents ) {
            if( family.count(parent) ) continue;
            family.insert(parent);
            parent->get_ancestors(family);
        }
    }
    void get_descendents_to( OpNode *other, unordered_set<OpNode*> &family ) {
        family.clear();
        unordered_set<OpNode*> o_ancestors; other->get_ancestors(o_ancestors);
        //unordered_set<OpNode*> t_descendents; get_descendents(t_descendents);
        //for( auto d : t_descendents ) if( o_ancestors.count(d) ) family.insert(d);
        traverse_dfs_stoppable( [&](OpNode *node)->bool {
            if( !o_ancestors.count(node) ) return false;
            family.insert(node);
            return true;
        });
    }
    
    static OpNode *graph_layer(OpNode *root) {
        map<OpNode*,OpNode*> map_node_node;
        return graph_layer(root,map_node_node);
    }
    static OpNode *graph_layer(OpNode *root,map<OpNode*,OpNode*> &map_node_node) {
        if( map_node_node.count(root) ) return map_node_node[root];
        
        OpNode *root_ = new OpNode();
        root_->idx          = root->idx;
        root_->stats        = root->stats;
        root_->edges        = root->edges;
        root_->faces        = root->faces;
        root_->reprchanges  = root->reprchanges;
        root_->nodes.push_back(root);
        
        
        //root_->bookmarked   = root->bookmarked;
        //root_->camera       = root->camera;
        //root_->color        = root->color;
        //root_->edges        = root->edges;
        //root_->faces        = root->faces;
        //root_->pos0         = root->pos0;
        //root_->pos1         = root->pos1;
        //root_->reprchanges  = root->reprchanges;
        //root_->stats        = root->stats;
        //root_->stroke       = root->stroke;
        
        for( auto c : root->children ) root_->add_child( graph_layer(c, map_node_node ) );
        
        map_node_node[root] = root_;
        return root_;
    }
    
    static void graph_to_list(OpNode *root, vector<OpNode*> &l, bool reindex) {
        l.clear();
        
        if( reindex ) {
            if(root->nodes.empty()) {
                root->traverse_dfs_parents([&l](OpNode*n){ l.push_back(n); });
            } else {
                root->traverse_dfs_parents([&l](OpNode*n){
                    unordered_set<int> inds; n->get_sub_indices(inds);
                    n->idx = *min_element(inds.begin(), inds.end());
                    l.push_back(n);
                });
                sort(l.begin(),l.end(),[](OpNode*a,OpNode*b){ return a->idx<b->idx; });
            }
            for( int i=0,n=l.size();i<n;i++ ) l[i]->idx=i;
        } else {
            int m = 0; root->traverse_dfs([&m](OpNode*n){ m=max(m,n->idx+1); });
            l.resize(m,nullptr);
            root->traverse_dfs([&l](OpNode*n){ l[n->idx]=n; });
        }
        for( auto n : l ) assert(n);
    }
    
    void print() {
        dlog.start("%d",idx);
        for( OpNode *n : nodes ) n->print();
        dlog.end_quiet();
    }
    
    // WARNING!!! REWRITES DEPGRAPH AND DELETES NODES
    static OpNode *cluster_delete_set(unordered_set<OpNode*> &family, bool parent_cleanup=false) {
        
        assert(!family.empty());
        if( family.size() == 0 ) return nullptr;
        
        if( family.size() == 1 ) return *family.begin();
        
        OpNode *cluster = new OpNode();
        cluster->idx = -1;
        unordered_set<OpNode*> sub_nodes, parents, children;
        for( auto n : family ) {
            for( auto s : n->nodes ) sub_nodes.insert(s);
            for( auto p : n->parents ) if( !family.count(p) ) parents.insert(p);
            for( auto c : n->children ) if( !family.count(c) ) children.insert(c);
        }
        cluster->nodes.assign(sub_nodes.begin(),sub_nodes.end());
        sort( cluster->nodes.begin(), cluster->nodes.end(), [](const OpNode *l, const OpNode *r) { return l->idx < r->idx; } );
        
        for( auto p : parents ) cluster->add_parent(p,false);
        for( auto c : children ) cluster->add_child(c,false);
        
        for( auto n : family ) { n->detach_all(); delete n; }
        
        if( parent_cleanup ) cluster->_clear_excessive_parenting();
        
        cluster->update_brush_to_sub();
        
        return cluster;
    }
    
    // WARNING!!! REWRITES DEPGRAPH AND DELETES NODES
    static OpNode *cluster_delete_set(OpNode *n0, OpNode *n1, bool parent_cleanup=false) {
        
        assert(n0 && n1);
        OpNode *cluster = new OpNode();
        cluster->idx = -1;
        
        unordered_set<OpNode*> sub_nodes, parents, children;
        for( auto s : n0->nodes ) sub_nodes.insert(s);
        for( auto s : n1->nodes ) sub_nodes.insert(s);
        for( auto p : n0->parents ) if( n1 != p ) parents.insert(p);
        for( auto p : n1->parents ) if( n0 != p ) parents.insert(p);
        for( auto c : n0->children ) if( n1 != c ) children.insert(c);
        for( auto c : n1->children ) if( n0 != c ) children.insert(c);
        
        cluster->nodes.assign(sub_nodes.begin(),sub_nodes.end());
        sort( cluster->nodes.begin(), cluster->nodes.end(), [](const OpNode *l, const OpNode *r) { return l->idx < r->idx; } );
        
        for( auto p : parents ) cluster->add_parent(p,false);
        for( auto c : children ) cluster->add_child(c,false);
        
        n0->detach_all(); delete n0;
        n1->detach_all(); delete n1;
        
        if( parent_cleanup ) cluster->_clear_excessive_parenting();
        
        cluster->update_brush_to_sub();
        
        return cluster;
    }
    
    static OpNode *get_clustered(unordered_set<OpNode*> &family) {
        OpNode *cluster = new OpNode();
        
        cluster->idx = -1;
        if( family.empty() ) {
        } else {
            unordered_set<OpNode*> sub_nodes;
            for( auto n : family ) {
                cluster->idx = min(cluster->idx,n->idx);
                n->get_sub_nodes(sub_nodes);
            }
            cluster->nodes.assign(sub_nodes.begin(),sub_nodes.end());
            sort( cluster->nodes.begin(), cluster->nodes.end(), [](const OpNode *l, const OpNode *r) { return l->idx < r->idx; } );
            cluster->update_to_sub();
        }
        
        return cluster;
    }
    
    struct segment {
        int i0, i1;
        vector<int> near;
    };
    vector<vec3f> pos_orig;
    vector<vec3f> pos;
    vector<float> pos_alpha;
    vector<segment> strokes;
    bool is_init = false;
    
    void reset_repr_stroke();
    void compute_repr_stroke( int n_iters, float ke, float ksl, float kso, float time_step );
    void update_repr_stroke();
    
    static bool is_cluster_valid(OpNode* node, unordered_set<OpNode*> &cluster) {
        if( cluster.count(node) ) return true;                  // node already in cluster!
        for( OpNode *p : node->parents ) {
            if( cluster.count(p) ) continue;
            if( p->is_dep_on(cluster) ) return false;       // adding node to cluster will create a cycle unless parent is added, too
        }
        for( OpNode *c : node->children ) {
            if( cluster.count(c) ) continue;
            if( c->has_dep(cluster) ) return false;
        }
        return true;
    }
    
    
    void add_parent( OpNode *other, bool deepcheck=true ) {
        if( deepcheck ) {
            if( is_dep_on(other) ) return;
        } else {
            if( is_parent(other) ) return;
        }
        parents.insert(other);
        other->children.insert(this);
    }
    
    void add_child( OpNode *other, bool deepcheck=true ) {
        if( deepcheck ) {
            if( other->is_dep_on(this) ) return;
        } else {
            if( is_child(other) ) return;
        }
        children.insert(other);
        other->parents.insert(this);
    }
    
    inline vec3f get_pos( const float p ) const {
        return get_pos( 1.0f - sin(p*PIf/2.0f), cos(p*PIf/2.0f) );
    }
    inline vec3f get_pos( const float px0, const float pyz0 ) const {
        const float px1 = 1.0f - px0, pyz1 = 1.0f - pyz0;
        return {
            glob.depgraph_xmult * (pos0.x*px0 + pos1.x*px1),
            glob.depgraph_ymult * (pos0.y*pyz0 + pos1.z*pyz1),
            glob.depgraph_zmult * (pos0.z*pyz0 + pos1.z*pyz1)
        };
    }
};

class OpNodeSmallestIndex {
public:
    bool operator() (const OpNode *l, const OpNode *r) { return l->idx > r->idx; }
};


class DepGraph {
public:
    vector<OpNode*> nodes;
    
    DepGraph( int n_e, int n_f, vector<AddDelData> &eops, vector<AddDelData> &fops, vector<StartEndData> &etimes, vector<StartEndData> &ftimes ) {
    }
    
    DepGraph( char *fn_cache ) { cache_load( fn_cache ); }
    
    void cache_save( char *fn_cache );
    void cache_load( char *fn_cache );
    
    ~DepGraph() {
        for( auto node : nodes ) delete node;
    }
};

#endif



MeshGit: Diffing and Merging Meshes for Polygonal Modeling
ReadMe


-------
Summary
-------

MeshGit takes for input .ply files and outputs a visualization and/or a .ply file.
Below are instructions for compiling and running MeshGit.
Citation for meshes in supplemental and paper are included in meshes.bib.



---------
Compiling
---------

*** Pre-compiled binaries for OSX and Linux are provided in bin/ folder ***


NOTE:
The source code uses C++11.  You must use a supporting compiler.
There are no external libraries needed except for OpenGL and GLUT.
Licenses for used code is supplied in comments.


Compile on OSX (Xcode):
    $ ./cmake-xcode.sh  # build in xcode after automatically opens

Compile on Linux (GCC-4.7):
    $ cd src
    $ make

MeshGit has been tested to work on OSX 10.7 and Kubuntu 12.04 Linux



-------
Running
-------

*** Pre-compiled binaries for OSX and Linux are provided in bin/ folder ***


Copy meshgit and lib* binaries (or just symlink to meshgit) into the models/ folder for easier access.

In general, meshgit takes as input a command and a list of meshes in ASCII Stanford PLY format:
    meshgit <command> <mesh0.ply> <mesh1.ply> <...>

Command-line help:
    $ meshgit

Along with the meshes are BASH shell scripts for reproducing the figures from the paper.  Example:
    $ ./fig1a.sh            # reproduces 2-way diff of creature meshes
    $ ./fig9.sh             # reproduces merge of spaceship meshes
    
    NOTE: The comparison figure (Fig. 4) will load cached matches for all methods except Iterative Greedy and Exact Matching.
          They are not recomputed.  All necessary data for recomputing these matches is provided in supplemental.

MeshGit graphical user interface:
    F1      - toggles on/off help menu
    MMB     - rotate camera
     +shift - pan
     +ctrl  - dolly / zoom
    
    The UI is context sensitive.  Mouse cursor must be over the panel or property you wish to manipulate.
    
    The Properties Panel (right side) is a list of properties that can be toggled, cycled, or set.
        The settable properties are modified with LMB clicking or draging.
        Items in brackets (ex: [ Merge ] ) are buttons.
        Use MMB drag to scroll.

Merge UI:
    The lower three panels show derivative a, original, and derivative b.
    The top-middle panel is the merged mesh.
    The top-right panel can show a mesh after applying Catmull-Clark subdivision rules (see properties panel).
    To resolve conflicts, either LMB click on the edit you wish to apply.
    The conflicting edits will be automatically removed.
    Clicking on the original will remove both edits, restoring merged mesh toward original.







#!/bin/bash -e

./meshgit view creature?.ply
./meshgit diff creature?.ply
mv view0.lmesh ../test/lmeshes/creature0_view.lmesh
mv view1.lmesh ../test/lmeshes/creature1_view.lmesh
mv diff0.lmesh ../test/lmeshes/creature0_diff0.lmesh
mv diff1.lmesh ../test/lmeshes/creature1_diff1.lmesh

./meshgit view shuttle?.ply
./meshgit diff shuttle?.ply
mv view0.lmesh ../test/lmeshes/shuttle0_view.lmesh
mv view1.lmesh ../test/lmeshes/shuttle1_view.lmesh
mv diff0.lmesh ../test/lmeshes/shuttle0_diff0.lmesh
mv diff1.lmesh ../test/lmeshes/shuttle1_diff1.lmesh

./meshgit view woman_0.ply woman_1.ply
./meshgit diff woman_0.ply woman_1.ply
mv view0.lmesh ../test/lmeshes/woman0_view.lmesh
mv view1.lmesh ../test/lmeshes/woman1_view.lmesh
mv diff0.lmesh ../test/lmeshes/woman0_diff0.lmesh
mv diff1.lmesh ../test/lmeshes/woman1_diff1.lmesh


./meshgit view durano03.ply durano04_mod.ply
mv view0.lmesh ../test/lmeshes/durano03_view.lmesh
mv view1.lmesh ../test/lmeshes/durano04_view.lmesh

./meshgit diff-load durano03.ply durano04_mod.ply durano03_durano04.none
mv diff0.lmesh ../test/lmeshes/durano03_diff0_none.lmesh
mv diff1.lmesh ../test/lmeshes/durano04_diff1_none.lmesh

./meshgit diff-load durano03.ply durano04_mod.ply durano03_durano04.exact
mv diff0.lmesh ../test/lmeshes/durano03_diff0_exact.lmesh
mv diff1.lmesh ../test/lmeshes/durano04_diff1_exact.lmesh

./meshgit diff-load durano03.ply durano04_mod.ply durano03_durano04.blend
mv diff0.lmesh ../test/lmeshes/durano03_diff0_blend.lmesh
mv diff1.lmesh ../test/lmeshes/durano04_diff1_blend.lmesh

./meshgit diff-load durano03.ply durano04_mod.ply durano03_durano04.spectral
mv diff0.lmesh ../test/lmeshes/durano03_diff0_spectral.lmesh
mv diff1.lmesh ../test/lmeshes/durano04_diff1_spectral.lmesh

./meshgit diff-load durano03.ply durano04_mod.ply durano03_durano04.disney
mv diff0.lmesh ../test/lmeshes/durano03_diff0_disney.lmesh
mv diff1.lmesh ../test/lmeshes/durano04_diff1_disney.lmesh

./meshgit diff durano03.ply durano04_mod.ply
mv diff0.lmesh ../test/lmeshes/durano03_diff0.lmesh
mv diff1.lmesh ../test/lmeshes/durano04_diff1.lmesh

./meshgit diff-load durano03.ply durano04_mod.ply durano03_durano04.noadj
mv diff0.lmesh ../test/lmeshes/durano03_diff0_noadj.lmesh
mv diff1.lmesh ../test/lmeshes/durano04_diff1_noadj.lmesh

./meshgit diff-load durano03.ply durano04_mod.ply durano03_durano04.greedy
mv diff0.lmesh ../test/lmeshes/durano03_diff0_greedy.lmesh
mv diff1.lmesh ../test/lmeshes/durano04_diff1_greedy.lmesh

./meshgit diff-sequence durano??.ply
mv diff0.lmesh  ../test/lmeshes/durano00_seq.lmesh
mv diff1.lmesh  ../test/lmeshes/durano01_seq.lmesh
mv diff2.lmesh  ../test/lmeshes/durano02_seq.lmesh
mv diff3.lmesh  ../test/lmeshes/durano03_seq.lmesh
mv diff4.lmesh  ../test/lmeshes/durano04_seq.lmesh
mv diff5.lmesh  ../test/lmeshes/durano05_seq.lmesh
mv diff6.lmesh  ../test/lmeshes/durano06_seq.lmesh
mv diff7.lmesh  ../test/lmeshes/durano07_seq.lmesh
mv diff8.lmesh  ../test/lmeshes/durano08_seq.lmesh
mv diff9.lmesh  ../test/lmeshes/durano09_seq.lmesh
mv diff10.lmesh ../test/lmeshes/durano10_seq.lmesh
mv diff11.lmesh ../test/lmeshes/durano11_seq.lmesh



./meshgit view sintel?.ply
mv view0.lmesh ../test/lmeshes/sintel0_view.lmesh
mv view1.lmesh ../test/lmeshes/sintel1_view.lmesh

./meshgit diff-load sintel?.ply sintel0_sintel1.none
mv diff0.lmesh ../test/lmeshes/sintel0_diff0_none.lmesh
mv diff1.lmesh ../test/lmeshes/sintel1_diff1_none.lmesh

./meshgit diff-load sintel?.ply sintel0_sintel1.exact
mv diff0.lmesh ../test/lmeshes/sintel0_diff0_exact.lmesh
mv diff1.lmesh ../test/lmeshes/sintel1_diff1_exact.lmesh

./meshgit diff-load sintel?.ply sintel0_sintel1.greedy1000
mv diff0.lmesh ../test/lmeshes/sintel0_diff0_greedy1000.lmesh
mv diff1.lmesh ../test/lmeshes/sintel1_diff1_greedy1000.lmesh
./meshgit diff-load sintel?.ply sintel0_sintel1.greedy2000
mv diff0.lmesh ../test/lmeshes/sintel0_diff0_greedy2000.lmesh
mv diff1.lmesh ../test/lmeshes/sintel1_diff1_greedy2000.lmesh
./meshgit diff-load sintel?.ply sintel0_sintel1.greedy3000
mv diff0.lmesh ../test/lmeshes/sintel0_diff0_greedy3000.lmesh
mv diff1.lmesh ../test/lmeshes/sintel1_diff1_greedy3000.lmesh
./meshgit diff-load sintel?.ply sintel0_sintel1.greedy
mv diff0.lmesh ../test/lmeshes/sintel0_diff0_greedy.lmesh
mv diff1.lmesh ../test/lmeshes/sintel1_diff1_greedy.lmesh

./meshgit diff-load sintel?.ply sintel0_sintel1.backtrack
mv diff0.lmesh ../test/lmeshes/sintel0_diff0_greedybacktrack.lmesh
mv diff1.lmesh ../test/lmeshes/sintel1_diff1_greedybacktrack.lmesh

./meshgit diff-load sintel?.ply sintel0_sintel1.meshgit
mv diff0.lmesh ../test/lmeshes/sintel0_diff0.lmesh
mv diff1.lmesh ../test/lmeshes/sintel1_diff1.lmesh


./meshgit diff keys?.ply
mv diff0.lmesh ../test/lmeshes/keys0_diff0.lmesh
mv diff1.lmesh ../test/lmeshes/keys1_diff1.lmesh


read -p "merge, subd"


./meshgit diff hand?.ply
mv diff0.lmesh ../test/lmeshes/hand0_diff0.lmesh
mv diffa.lmesh ../test/lmeshes/handa_diffa.lmesh
mv diffb.lmesh ../test/lmeshes/handb_diffb.lmesh
./meshgit merge hand?.ply
mv merge.lmesh ../test/lmeshes/hand_merge.lmesh
./meshgit merge hand?.ply
mv merge.lmesh ../test/lmeshes/hand_merge_subd.lmesh
./meshgit merge hand?.ply
mv merge.lmesh ../test/lmeshes/hand_merge_subd3.lmesh

./meshgit diff shaolin?.ply
mv diff0.lmesh ../test/lmeshes/shaolin0_diff0.lmesh
mv diffa.lmesh ../test/lmeshes/shaolina_diffa.lmesh
mv diffb.lmesh ../test/lmeshes/shaolinb_diffb.lmesh
./meshgit merge shaolin?.ply
mv merge.lmesh ../test/lmeshes/shaolin_merge.lmesh
./meshgit merge shaolin?.ply
mv merge.lmesh ../test/lmeshes/shaolin_merge_subd.lmesh


read -p "merge 0, merge a, merge b"

./meshgit diff spaceship?.ply
mv diff0.lmesh ../test/lmeshes/spaceship0_diff0.lmesh
mv diffa.lmesh ../test/lmeshes/spaceshipa_diffa.lmesh
mv diffb.lmesh ../test/lmeshes/spaceshipb_diffb.lmesh
mv diff0_changes.lmesh ../test/lmeshes/spaceship0_diff0_changes.lmesh
mv diffa_changes.lmesh ../test/lmeshes/spaceshipa_diffa_changes.lmesh
mv diffb_changes.lmesh ../test/lmeshes/spaceshipb_diffb_changes.lmesh
./meshgit merge spaceship?.ply
mv merge.lmesh ../test/lmeshes/spaceship_merge0.lmesh
./meshgit merge spaceship?.ply
mv merge.lmesh ../test/lmeshes/spaceship_mergea.lmesh
./meshgit merge spaceship?.ply
mv merge.lmesh ../test/lmeshes/spaceship_mergeb.lmesh







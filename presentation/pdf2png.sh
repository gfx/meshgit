#!/bin/bash

# http://stackoverflow.com/questions/653380/converting-a-pdf-to-png

#convert -background transparent page.pdf page.png
#gs -sDEVICE=pngalpha -sOutputFile=%stdout -r10 page.pdf | convert -background transparent - page.png

#gm convert -transparent white -background transparent -density 300x300 -resize 24% page.pdf page.png
#gm convert -transparent white -background transparent -density 300x300 -resize 24% page.pdf +adjoin "page%d.png"

if [ $# -ne 1 ]; then
    echo "Usage: `basename $0` page.pdf"
    exit 1
fi

fnpdf=$1
fnbase="${fnpdf%.*}"
pgpath="${fnbase}_pages"

# create working folder
mkdir ${pgpath}
cd ${pgpath}
rm *
ln -s ../${fnpdf} .

# split pdf so each page is separate pdf
pdftk ${fnpdf} burst

# convert each (split) pdf into png
for fnpgpdf in `ls pg_*.pdf`; do
    fnpgpng="${fnpgpdf%.*}.png"
    
    # reducing density will decrease quality, but increase throughput
    gm convert -density 1200x1200 -resize 1920x1080 "${fnpgpdf}" "${fnpgpng}"
    
    #gm convert -density 345.6x345.6 "${fnpgpdf}" "${fnpgpng}"
    
    rm ${fnpgpdf}
done


#fnpngddd="${fnbase}%03d.png"
#fnpngqqq="${fnbase}???.png"
#rm ${fnpngqqq}
##gm convert -density 1200x1200 -resize 19.2% "${fnpdf}" +adjoin "${fnpngddd}"
#gm convert -density 600x600 -resize 38.4% "${fnpdf}" +adjoin "${fnpngddd}"

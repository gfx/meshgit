import bpy
import subprocess
import os

"""
this script will:
    export objects from layers 0,1 to [file]_0.ply,[file]_1.ply
    run: meshgit diff [file]_0.ply [file]_1.ply

[file] is the blend filename (with .blend extension)

NOTE: assumes the meshgit scripts are located in the same folder as the blend file


blender layers:
     0  1  2  3  4    5  6  7  8  9
    10 11 12 13 14   15 16 17 18 19

"""



path,prefix = os.path.split( bpy.data.filepath )
meshgit     = os.path.join( path, 'meshgit' )

fn0 = os.path.join( path, prefix + '_0.ply' )
fn1 = os.path.join( path, prefix + '_1.ply' )




#################################################################################################################
def set_object_mode():
    while bpy.context.mode != 'OBJECT':
        bpy.ops.object.mode_set(mode='OBJECT')
    return

def layers( li_layer ):
    return [ (i in li_layer) for i in range(20) ]

def viewlayers( li_layer ):
    bpy.context.scene.layers = layers( li_layer )

def selectobj(obj):
    set_object_mode()
    if type(obj) is str:
        obj = bpy.data.objects[obj]
    obj.select = True
    bpy.context.scene.objects.active = obj
    return obj

def selectall():
    bpy.ops.object.select_all(action='SELECT')

def deselectall():
    bpy.ops.object.select_all(action='DESELECT')

def deselectall_alllayers():
    viewlayers([ i for i in range(20) ])
    deselectall()

def apply_mods(obj):
    deselectall()
    selectobj(obj)
    # make sure Multires modifier has levels set properly before applying
    for mod in obj.modifiers:
        if mod.name == 'Multires':
            mod.levels = mod.sculpt_levels
    bpy.ops.object.convert(target='MESH', keep_original=False)

def get_camera():
    a_viewport = [a for a in bpy.data.window_managers[0].windows[0].screen.areas if a.type == 'VIEW_3D']
    if not a_viewport:
        # likely a snapshot while trying to save...  hack!
        try:
            bpy.ops.screen.back_to_previous()
        except Exception:
            return None
        a_viewport = [a for a in bpy.data.window_managers[0].windows[0].screen.areas if a.type == 'VIEW_3D']
    a_viewport = a_viewport[0]
    r3d_viewport = a_viewport.spaces[0].region_3d
    
    o = r3d_viewport.view_location
    x, y, z = Vector((1, 0, 0)), Vector((0, 1, 0)), Vector((0, 0, 1))
    x.rotate(r3d_viewport.view_rotation)
    y.rotate(r3d_viewport.view_rotation)
    z.rotate(r3d_viewport.view_rotation)
    
    return {
        'o': list(o),
        'x': list(x), 'y': list(y), 'z': list(z),
        'd': r3d_viewport.view_distance,
        'p': r3d_viewport.view_perspective,
    }

def triangulate(f_ngon):
    l = len(f_ngon)
    if l < 3:
        return
    elif l == 3:
        yield f_ngon
    else:
        i0 = f_ngon[0]
        for i1, i2 in zip(f_ngon[1:-1], f_ngon[2:]):
            yield (i0, i1, i2)

def exec_nowait( arglist ):
    return subprocess.Popen( arglist, stderr=subprocess.STDOUT, stdout=subprocess.PIPE )

def exec_wait( arglist ):
    return exec_nowait(arglist).communicate()

def writebin_f(fn, l):
    open(fn, 'wb').write(struct.pack('i%df' % len(l), len(l), *l))

def writebin_i(fn, l):
    open(fn, 'wb').write(struct.pack('i%di' % len(l), len(l), *l))

def flatten(ll):
    return [e for l in ll for e in l]
####################################################################################



# make sure we're in object mode
set_object_mode()


# make sure that layers 0,1,2 have only one object each
#for i in range(3):
#    assert len([ o for o in bpy.data.objects if o.layers[i] ]) == 1, 'layer%i must have only 1 object' % i


deselectall_alllayers()

# export objects in layers 0,1
for i_l,fn in enumerate([fn0,fn1]):
    viewlayers([i_l])
    selectall()
    bpy.context.scene.objects.active = bpy.context.selected_objects[0]
    bpy.ops.object.shade_smooth()
    bpy.ops.export_mesh.ply(filepath=fn,use_mesh_modifiers=True,use_normals=False,use_uv_coords=False,use_colors=False)
    bpy.ops.object.shade_flat()
    deselectall()


# perform diff
exec_nowait([meshgit, 'diff', fn0, fn1])



# delete any of the files created
#for fn in [ fn0,fn1 ]:
#    try: os.remove( fn )
#    except: pass


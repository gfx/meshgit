#!/bin/bash -e

if [ $# -eq 0 ]; then
    echo "Usage: ${0} <watch files>"
    exit 1
fi

if [ `uname` == "Darwin" ]; then
    HASHFN="md5"
    CMD_START="afplay -v .8 ~/sounds/smb_pipe.wav"
    CMD_SUCCESS="afplay -v .8 ~/sounds/smb_kick.wav"
    CMD_FAILED="afplay -v .8 ~/sounds/smb_breakblock.wav"
    CMD_SUCCESS2="afplay -v .8 ~/sounds/smb_jumpsmall.wav"
    CMD_FAILED2="afplay -v .8 ~/sounds/smb_breakblock.wav"
else
    HASHFN="md5sum"
    CMD_START="play -q ~/sounds/smb_pipe.wav"
    CMD_SUCCESS="play -q ~/sounds/smb_kick.wav"
    CMD_FAILED="play -q ~/sounds/smb_breakblock.wav"
    CMD_SUCCESS2="play -q ~/sounds/smb_jumpsmall.wav"
    CMD_FAILED2="play -q ~/sounds/smb_breakblock.wav"
fi

HASH=""
while [ 1 ]; do
    NHASH=`${HASHFN} $@`
    if [ "${HASH}" != "${NHASH}" ]; then
        echo "Rebuilding"
        eval "${CMD_START}" &
        #echo | pdflatex meshgit.tex && eval "${CMD_SUCCESS}" || eval "${CMD_FAILED}"
        #echo | pdflatex meshgit_notes.tex && eval "${CMD_SUCCESS}" || eval "${CMD_FAILED}"
        
        SUCCESS=0
        echo | pdflatex meshgit_notes.tex > /dev/null &
        echo | pdflatex meshgit_nofigs.tex > /dev/null &
        echo | pdflatex meshgit.tex && SUCCESS=1
        if [ ${SUCCESS} -eq 1 ]; then
            eval "${CMD_SUCCESS}"
            #echo | pdflatex meshgit_notes.tex && eval "${CMD_SUCCESS2}" || eval "${CMD_FAILED2}"
        else
            eval "${CMD_FAILED}"
        fi
        
        HASH="${NHASH}"
    fi
    sleep 1
done


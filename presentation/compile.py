#!/usr/bin/python

import time, subprocess, os, sys, signal
import shlex
import re


def find_syspath_to_file( fn ):
    for p in sys.path:
        pfn = os.path.join( p, fn )
        if os.path.exists( pfn ): return pfn
    #print 'WARNING: could not find file (%s) given paths: %s' % (fn,','.join(sys.path))
    return fn



if len(sys.argv) == 1:
    print 'Usage: %s <filename.tex>' % sys.argv[0]
    exit(1)
fn_tex = sys.argv[1]
fn_pdf = '%s.pdf' % os.path.splitext(fn_tex)[0]
if not os.path.exists(fn_tex):
    print 'ERROR: %s does not exist' % fn_tex
    exit(1)


cmd_trace           = find_syspath_to_file('bin/trace')
pattern_trace       = r'\Trace{([^}]*)}{([^}]*)}{([^}]*)}'
regex_trace         = re.compile(pattern_trace)

cmd_interactive     = find_syspath_to_file('bin/trace')
pattern_interactive = r'\Interactive{([^}]*)}{([^}]*)}{([^}]*)}{([^}]*)}{([^}]*)}{([^}]*)}'
regex_interactive   = re.compile(pattern_interactive)

opts_pdf2png    = True
opts_mult       = 1280.0 / 400.0

if not os.path.exists(cmd_trace):
    print 'ERROR: trace does not exist (%s)' % cmd_trace
    exit(1)



print 'executing inlined commands...'
# load entire file, but ignore commented code
tex = ''.join([ re.sub(r'%.*','',l) for l in open(fn_tex,'rt') ])
path_cur = os.path.abspath('.')  #realpath('.')

# find and execute trace commands
for match in regex_trace.finditer(tex):
    fn_scene = match.group(1)
    fn_image = match.group(2)
    
    path_scene,fn_scene = os.path.split(fn_scene)
    path_scene = os.path.abspath(path_scene)
    fn_image = os.path.abspath(fn_image)
    
    if not os.path.exists(fn_image):
        print 'trace: %s -> %s' % (fn_scene,fn_image)
        os.chdir(path_scene)
        cmd = [cmd_trace,fn_scene,fn_image]
        subprocess.Popen(cmd).communicate()
        os.chdir(path_cur)
    else:
        print 'trace: %s already exists' % (os.path.split(fn_image)[1],)
        pass

# find and execute interactive commands
for match in regex_interactive.finditer(tex):
    fn_scene = match.group(1)
    fn_image = match.group(2)
    x = int(float(match.group(3))*opts_mult)
    y = int(float(match.group(4))*opts_mult)
    w = int(float(match.group(5))*opts_mult)
    h = int(float(match.group(6))*opts_mult)
    
    path_scene,fn_scene = os.path.split(fn_scene)
    path_scene = os.path.abspath(path_scene)
    fn_image = os.path.abspath(fn_image)
    
    if not os.path.exists(fn_image):
        print 'interactive: %s -> %s' % (fn_scene,fn_image)
        os.chdir(path_scene)
        cmd = [cmd_interactive,fn_scene,fn_image]
        subprocess.Popen(cmd).communicate()
        os.chdir(path_cur)
    else:
        print 'interactive: %s already exists' % (os.path.split(fn_image)[1],)
        pass
    
    print 'interactive element at (%d,%d)x(%d,%d)' % (x,y,w,h)

# run twice so \LastPage is updated correctly
print 'compiling tex...'
cmd_pdflatex = [ find_syspath_to_file('pdflatex'), '-shell-escape', fn_tex ]
subprocess.Popen( cmd_pdflatex, stdout=subprocess.PIPE ).communicate()
subprocess.Popen( cmd_pdflatex, stdout=subprocess.PIPE ).communicate()


if opts_pdf2png:
    print 'converting to pngs...'
    subprocess.Popen( [find_syspath_to_file('pdf2png.sh'),fn_pdf], stdout=subprocess.PIPE ).communicate()


print 'done'

/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
/*
 *  histograms.cpp
 *  SpecMatch
 *
 *  Created by Diana Mateus and David Knossow on 10/7/07.
 *  Copyright 2007 INRIA. All rights reserved.
 *
 */
#include <iostream>
#include <fstream>

using namespace std;

#include "histograms.h"




Histogram::Histogram()
{

    template_filename = NULL;
    filename = new char[2048];	//NULL ;
    histogram_nomalization = CENTER_NORM ; //INDIVIDUAL_NORM;
    dim = 0;
}


Histogram::~Histogram()
{

    free(template_filename);
    delete[]filename;
}

void Histogram::setParameters(int p_dim, int p_nbins, char *p_templateF)
{
  if (p_templateF) {
    template_filename = strdup(p_templateF);
  }
  else {
    template_filename = strdup("tmp/");
  }
    dim = p_dim;
    num_of_bins = p_nbins;

}

int Histogram::getNumberOfBins()
{
    return num_of_bins;
}

int Histogram::getDim()
{
    return dim;
}

void Histogram::fillSingleHistogram(FL * p_X, int *p_maskX, FL * p_vecHist,
				    int p_count)
{

    FL l_Glob_Min = p_X[0];
    FL l_Glob_Max = p_X[0];

    setGlobalMinMaxFunc(p_X, p_maskX, p_count, &l_Glob_Min, &l_Glob_Max);

    FL *l_min_value = new FL[dim];
    FL *l_max_value = new FL[dim];

    bzero(l_min_value, dim * sizeof(FL));
    bzero(l_max_value, dim * sizeof(FL));
    setMinMaxFunc(p_X, p_maskX, p_count, l_min_value, l_max_value);
    fillHistogramMinMax(p_vecHist, p_X, p_maskX, p_count, l_min_value,
			l_max_value, l_Glob_Min, l_Glob_Max);


    if (l_min_value)
	delete[]l_min_value;
    if (l_max_value)
	delete[]l_max_value;

}

void Histogram::fillAllHistograms(FL * p_XX, int *p_maskXX, FL * p_YY,
				  int *p_maskYY, FL * p_vecHistX,
				  FL * p_vecHistY, FL * p_vecHistYF,
				  int p_countx, int p_county)
{
    FL l_Glob_MinX = p_XX[0];
    FL l_Glob_MaxX = p_XX[0];


    FL l_Glob_MinY = p_YY[0];
    FL l_Glob_MaxY = p_YY[0];

    FL l_Glob_MinFY = p_YY[0];
    FL l_Glob_MaxFY = p_YY[0];

    PRINT_(std::cout << "COMPUTING ALL HISTOGRAMS" << std::endl);

    FL *l_YYF = new FL[p_county * dim];
    bzero(l_YYF, p_county * dim * sizeof(FL));
    for (int i = 0; i < p_county * dim; ++i) {
	l_YYF[i] = -p_YY[i];
    }


#ifdef DEBUG_3
    std::cout << "Flipped embedding stored: " <<std::endl ;
    for (int i = 0; i < p_county; i++) {
	for (int j = 0; j < dim; j++) {
	    std::cout << l_YYF[i * dim + j] << " ";
	}
	std::cout << std::endl;
    }
    std::cout << std::endl;
#endif


    setGlobalMinMaxFunc(p_XX, p_maskXX, p_countx, &l_Glob_MinX,
			&l_Glob_MaxX);
    setGlobalMinMaxFunc(p_YY, p_maskYY, p_county, &l_Glob_MinY,
			&l_Glob_MaxY);
    setGlobalMinMaxFunc(l_YYF, p_maskYY, p_county, &l_Glob_MinFY,
			&l_Glob_MaxFY);

    FL *l_min_value_X = new FL[dim];
    FL *l_max_value_X = new FL[dim];
    FL *l_min_value_Y = new FL[dim];
    FL *l_max_value_Y = new FL[dim];
    FL *l_min_value_YF = new FL[dim];
    FL *l_max_value_YF = new FL[dim];
    bzero(l_min_value_X, dim * sizeof(FL));
    bzero(l_max_value_X, dim * sizeof(FL));
    bzero(l_min_value_Y, dim * sizeof(FL));
    bzero(l_max_value_Y, dim * sizeof(FL));
    bzero(l_min_value_YF, dim * sizeof(FL));
    bzero(l_max_value_YF, dim * sizeof(FL));


    setMinMaxFunc(p_XX, p_maskXX, p_countx, l_min_value_X, l_max_value_X);
    setMinMaxFunc(p_YY, p_maskYY, p_county, l_min_value_Y, l_max_value_Y);
    setMinMaxFunc(l_YYF, p_maskYY, p_county, l_min_value_YF,
		  l_max_value_YF);


#ifdef DEBUG_3
    std::cout << "Printing Max and Min for all dimensions " <<std::endl ;
    std::cout << "Min: "<<std::endl ;
    for (int i = 0 ; i < dim ; ++i) {
	std::cout << i <<
	    " " << l_min_value_X[i] << 
	    " " << l_min_value_Y[i] << 
	    " " << l_min_value_YF[i] << std::endl ;
    }
    std::cout << "Max: "<<std::endl ;
    for (int i = 0 ; i < dim ; ++i) {
	std::cout << i <<
	    " " << l_max_value_X[i] << 
	    " " << l_max_value_Y[i] << 
	    " " << l_max_value_YF[i] << std::endl ;
    }
#endif

    fillHistogramMinMax(p_vecHistX, p_XX, p_maskXX, p_countx,
			l_min_value_X, l_max_value_X, l_Glob_MinX,
			l_Glob_MaxX);
    fillHistogramMinMax(p_vecHistY, p_YY, p_maskYY, p_county,
			l_min_value_Y, l_max_value_Y, l_Glob_MinY,
			l_Glob_MaxY);
    fillHistogramMinMax(p_vecHistYF, l_YYF, p_maskYY, p_county,
			l_min_value_YF, l_max_value_YF, l_Glob_MinFY,
			l_Glob_MaxFY);

    if (p_county > 0)
	delete[]l_YYF;
    l_YYF = NULL;

    if (l_min_value_X)
	delete[]l_min_value_X;
    if (l_max_value_X)
	delete[]l_max_value_X;

    if (l_min_value_Y)
	delete[]l_min_value_Y;
    if (l_max_value_Y)
	delete[]l_max_value_Y;

    if (l_min_value_YF)
	delete[]l_min_value_YF;
    if (l_max_value_YF)
	delete[]l_max_value_YF;
}


void Histogram::setGlobalMinMaxFunc(FL * p_Vect, int *p_mask, int p_length,
				    FL * p_global_min_value,
				    FL * p_global_max_value)
{


    for (int i = 0; i < p_length; ++i) {
	if (p_mask && p_mask[i] == 0)
	    continue;
	for (int d = 0; d < dim; ++d) {
	    if (p_Vect[i * dim + d] < *p_global_min_value)
		*p_global_min_value = p_Vect[i * dim + d];
	    if (p_Vect[i * dim + d] > *p_global_max_value)
		*p_global_max_value = p_Vect[i * dim + d];
	}
    }

}



void Histogram::setMinMaxFunc(FL * p_Vect, int *p_mask, int p_length,
			      FL * p_min_value, FL * p_max_value)
{

    for (int i = 0; i < p_length; ++i) {
	if (p_mask && p_mask[i] == 0)
	    continue;
	for (int d = 0; d < dim; ++d) {
	    if (p_Vect[i * dim + d] < p_min_value[d])
		p_min_value[d] = p_Vect[i * dim + d];
	    if (p_Vect[i * dim + d] > p_max_value[d])
		p_max_value[d] = p_Vect[i * dim + d];
	}
    }
}







void Histogram::fillHistogramMinMax(FL * p_Hist, FL * p_Vect, int *p_mask,
				    int p_length, FL * p_min, FL * p_max,
				    FL p_Glob_min, FL p_Glob_max)
{
    double l_range = 0;
    int l_ind = 0;
    FL l_offset = 0;

    switch (histogram_nomalization) {
    case GLOBAL_NORM:{
	    PRINT_COLOR_(fprintf
			 (stderr,
			  "Global Min and Max are taken into account \n"),
			 GREEN);
	    break;
	}
    case INDIVIDUAL_NORM:{
	    PRINT_COLOR_(fprintf
			 (stderr,
			  "Individual Min and Max are taken into account \n"),
			 GREEN);
	    break;
	}
    case CENTER_NORM:{
	    PRINT_COLOR_(fprintf
			 (stderr,
			  "MAX( fabs(Min), fabs(Max)) is taken into account \n"),
			 GREEN);
	    break;
	}
    default:{
	    PRINT_COLOR_(fprintf
			 (stderr,
			  "No histogram normalization process selected\n"),
			 GREEN);
	}
    }
    for (int d = 0; d < dim; ++d) {
	switch (histogram_nomalization) {
	case GLOBAL_NORM:{
		l_range = (p_Glob_max - p_Glob_min) / (num_of_bins - 1);
		l_offset = p_Glob_min;
		DBG2_(std::
		      cout << "max-min " << d << ": " << p_Glob_max << ":"
		      << p_Glob_min << std::endl);
		break;
	    }
	case INDIVIDUAL_NORM:{
		l_range = (p_max[d] - p_min[d]) / (num_of_bins - 1);
		l_offset = p_min[d];
		DBG2_( 
		      std::cout << "Offset : " << l_offset << " range :" << l_range <<std::endl //;
		      );
		break;
	    }
	case CENTER_NORM:{
	    l_offset = MAX(fabs(p_max[d]), fabs(p_min[d]));
	   
	    l_range = 2 * l_offset / (num_of_bins - 1);
	    l_offset = -l_offset;

	    DBG3_(
		  std::cout << "Offset : " << l_offset << " range :" << l_range <<std::endl //;
		  ) ;


		break;
	    }
	default:{
		break;
	    }
	}

	DBG2_(std::cout << "Range " << d << ": " << l_range << std::endl);
	if (l_range == 0) {
	    WARNING_(fprintf
		     (stderr, "Can not fill histogram %d (range = 0) \n",
		      d));
	    continue;
	}
	for (int i = 0; i < p_length; i++) {
	    if (p_mask && p_mask[i] == 0)
		continue;
	    l_ind =
		(int) round((p_Vect[i * dim + d] - l_offset) / l_range);
#ifdef DEBUG_3
	    std::cout << p_Vect[i * dim + d] << " " <<  l_offset << " " << p_Vect[i * dim + d] - l_offset << " " << (p_Vect[i * dim + d] - l_offset) / l_range << " " << l_ind << std::endl ;
#endif
	    if ((l_ind < 0) || (l_ind >= num_of_bins)) {
		WARNING_(fprintf
			 (stderr, "OUT OF RANGE %d %d %d %f %f %f %d \n",i, dim, d,
			  p_Vect[i * dim + d],
			  p_Vect[i * dim + d] - p_min[d], l_range, l_ind));
	    } 
	    else {
		p_Hist[d * num_of_bins + l_ind] += 1;
	    }
	}


    }

#ifdef DEBUG_3
    for (int i = 0 ; i < num_of_bins ; ++i) {
	for (int d = 0; d < dim; ++d) {
	    std::cout << p_Hist[d * num_of_bins + i] << " " ;
	}
	std::cout << std::endl ;
    }
#endif



}

//----------------------------------------------------------------------------
// DISTANCE BETWEEN HISTOGRAMS 
//----------------------------------------------------------------------------


void Histogram::distance(HISTOGRAM_DISTANCE p_HistogramDistance,
			 FL * p_vecHistX, FL * p_vecHistY,
			 FL * p_vecHistFlippedY, FL * p_error_hist,
			 FL * p_error_hist_Flipped)
{


    switch (p_HistogramDistance) {

    case BINTOBIN:
	computeBintoBinDifference(p_vecHistX, p_vecHistY,
				  p_vecHistFlippedY, p_error_hist,
				  p_error_hist_Flipped);
	print_flmatrix(p_error_hist, "tmp/error_hist_B2BDist", dim, dim);
	print_flmatrix(p_error_hist_Flipped,
		       "tmp/error_hist_B2BDist_Flips", dim, dim);
	break;
#ifdef USE_DIFFUSION_DISTANCE
    case DIFFUSION:
	computeDiffusionDist(p_vecHistX, p_vecHistY, p_vecHistFlippedY,
			     p_error_hist, p_error_hist_Flipped);
	print_flmatrix(p_error_hist, "tmp/error_hist_DiffDist", dim, dim);
	print_flmatrix(p_error_hist_Flipped,
		       "tmp/error_hist_DiffDist_Flips", dim, dim);
	break;
#endif
#ifdef USE_EMDL1_DISTANCE
    case EMDL1:
	computeEMDL1Dist(p_vecHistX, p_vecHistY, p_vecHistFlippedY,
			 p_error_hist, p_error_hist_Flipped);
	print_flmatrix(p_error_hist, "tmp/error_hist_EMDL1", dim, dim);
	print_flmatrix(p_error_hist_Flipped, "tmp/error_hist_EMDL1_Flips",
		       dim, dim);
	break;
#endif
    case H_TEST_ALL:
	computeBintoBinDifference(p_vecHistX, p_vecHistY,
				  p_vecHistFlippedY, p_error_hist,
				  p_error_hist_Flipped);
	print_flmatrix(p_error_hist, "tmp/error_hist_B2BDist", dim, dim);
	print_flmatrix(p_error_hist_Flipped,
		       "tmp/error_hist_B2BDist_Flips", dim, dim);
#ifdef USE_EMDL1_DISTANCE
	memset(p_error_hist, 0, dim * dim * sizeof(FL));
	memset(p_error_hist_Flipped, 0, dim * dim * sizeof(bool));
	computeEMDL1Dist(p_vecHistX, p_vecHistY, p_vecHistFlippedY,
			 p_error_hist, p_error_hist_Flipped);
	print_flmatrix(p_error_hist, "tmp/error_hist_EMDL1", dim, dim);
	print_flmatrix(p_error_hist_Flipped, "tmp/error_hist_EMDL1_Flips",
		       dim, dim);
	memset(p_error_hist, 0, dim * dim * sizeof(FL));
	memset(p_error_hist_Flipped, 0, dim * dim * sizeof(bool));
#endif
#ifdef USE_DIFFUSION_DISTANCE
	computeDiffusionDist(p_vecHistX, p_vecHistY, p_vecHistFlippedY,
			     p_error_hist, p_error_hist_Flipped);
	print_flmatrix(p_error_hist, "tmp/error_hist_DiffDist", dim, dim);
	print_flmatrix(p_error_hist_Flipped,
		       "tmp/error_hist_DiffDist_Flips", dim, dim);
#endif
	break;
    default:
	DBG_(std::cout << "Unknown histogram distance" << std::endl);
	exit(0);
	break;
    }

}


void Histogram::computeBintoBinDifference(FL * p_vecHistX, FL * p_vecHistY,
					  FL * p_vecHistFlippedY,
					  FL * p_error_hist,
					  FL * p_error_hist_Flipped)
{

    int l_opt_t = 0;
    double l_opt_h = 0;
    bool l_opt_t_flipped = false;
    bool l_opt_h_flipped = false;
    bool l_opt_s_flipped = false;
    double l_score1 = 0;
    double l_score2 = 0;
    double l_score = 0;
    PRINT_COLOR_(std::cout << "Bin to Bin  distance" << std::endl, RED);

    sprintf(filename, "%s_histolog", template_filename);
    std::ofstream log(filename);
    if (log.is_open() == 0) {
	DBGout <<
	    "Can not write log printing on standard output (log file not opened)"
	    << std::endl;
    }
    else {
	DBGout << "Results of computeBintoBinDifference printed to log: "
	    << filename << std::endl;
    }

#ifdef DEBUG_3
    for (int d = 0; d < dim; d++) {
	for (int i = 0; i < num_of_bins; i++) {
	    std::cout << "X,Y,FY " << p_vecHistX[d * num_of_bins +
						 i] << " " << p_vecHistY[d
									 *
									 num_of_bins
									 +
									 i]
		<< " " << p_vecHistFlippedY[d * num_of_bins +
					    i] << std::endl;
	}
    }
#endif

    if (log.is_open() == 0) {
	DBGout << "Bin-to-bin distance printed on screen" << std::endl;
    }
    else {
	log << "Bin-to-bin distance: " << filename << std::endl;
    }


    for (int i = 0; i < dim; i++) {
	for (int j = 0; j < dim; j++) {

	    double l_min_s_score = -1;
	    double l_opt_s = 0;
	    double l_min_t_score = -1;
	    double l_min_h_score = -1;
	    l_opt_t = 0;
	    l_opt_h = 0;
	    double s_min = 0 ;
	    HASH_MAP_PARAM_(double, "hor_scale_min", Double, s_min ) ;
	    double s_max = 0 ;
	    HASH_MAP_PARAM_(double, "hor_scale_max", Double, s_max ) ;
	    double s_step = 0 ;
	    HASH_MAP_PARAM_(double, "hor_scale_step", Double, s_step ) ;
	    int l_trans_bins = 0 ;
	    HASH_MAP_PARAM_(int, "trans_bins", Int, l_trans_bins) ;



	    double sh_min = 0 ;
	    HASH_MAP_PARAM_(double, "ver_scale_min", Double, sh_min ) ;
	    double sh_max = 0 ;
	    HASH_MAP_PARAM_(double, "ver_scale_max", Double, sh_max ) ;
	    double sh_step = 0 ;
	    HASH_MAP_PARAM_(double, "ver_scale_step", Double, sh_step ) ;

	    for (double s = s_min ; s < s_max ; s += s_step ) {	// scaling horizontal 
	      for (int t = -l_trans_bins; t < l_trans_bins; ++t) {	// translation
		for (double s_h = sh_min; s_h < sh_max ; s_h += sh_step) {	//scaling vertical


			l_score1 = difference(p_vecHistX + i * num_of_bins, p_vecHistY + j * num_of_bins, num_of_bins, s, s_h, t);	//, sup_X[2*i], sup_Y[2*i],  sup_X[2*i+1], sup_Y[2*i+1]) ;
			l_score2 = difference(p_vecHistX + i * num_of_bins, p_vecHistFlippedY + j * num_of_bins, num_of_bins, s, s_h, t);	//, sup_X[2*i], sup_YF[2*i],  sup_X[2*i+1], sup_YF[2*i+1]) ;
			l_score = l_score1;
			if (log.is_open() == 0) {
			    DBGout << i << " " << j << " " << s << " " << t
				<< " " << s_h << " " << l_score1 << " " <<
				l_score2 << std::endl;
			}
			else {
			    log << i << " " << j << " " << s << " " << t <<
				" " << s_h << " Not Flipped : " << l_score1
				<< " Flipped :" << l_score2 << std::endl;
			}

			if (l_score > l_score2) {
			    l_score = l_score2;
			}

			if (l_min_h_score == -1 || l_min_h_score > l_score) {
			  if (l_score == l_score2 && l_score2 != l_score1 ) {
				l_opt_h_flipped = true;
			  }
			  else {
				l_opt_h_flipped = false;
			  }
			    l_opt_h = s_h;
			    l_min_h_score = l_score;
			}
		    }

		    if (l_min_t_score == -1
			|| l_min_t_score > l_min_h_score) {
			l_opt_t = t;
			l_min_t_score = l_min_h_score;
			l_opt_t_flipped = l_opt_h_flipped;
		    }

		}
		if (l_min_s_score == -1 || l_min_s_score > l_min_t_score) {
		    l_opt_s = s;
		    l_min_s_score = l_min_t_score;
		    l_opt_s_flipped = l_opt_t_flipped;
		}
	    }

	    if (!l_opt_s_flipped) {
	      if (log.is_open() == 0) {
		    PRINTout << "Optimal For d1=" << i <<
			" compared to d2 =" << j << " at scale " << l_opt_s
			<< " and trans " << l_opt_t << " and height scale "
			<< l_opt_h << " l_score is " << l_min_s_score <<
			std::endl;
	      }
	      else {
		    log << "Optimal For d1=" << i << " compared to d2 =" <<
			j << " at scale " << l_opt_s << " and trans " <<
			l_opt_t << " and height scale " << l_opt_h <<
			" l_score is " << l_min_s_score << std::endl;
	      }
	    } else {
		p_error_hist_Flipped[i * dim + j] = l_opt_s_flipped;
		if (log.is_open() == 0) {
		    PRINTout << "Optimal For d1=" << i <<
			" compared to flipped d2 =" << j << " at scale " <<
			l_opt_s << " and trans " << l_opt_t <<
			" and height scale " << l_opt_h << " l_score is "
			<< l_min_s_score << std::endl;
		}
		else {
		    log << "Optimal For d1=" << i <<
			" compared to flipped d2 =" << j << " at scale " <<
			l_opt_s << " and trans " << l_opt_t <<
			" and height scale " << l_opt_h << " l_score is "
			<< l_min_s_score << std::endl;
		}
	    }

	    p_error_hist[i * dim + j] = l_min_s_score;

	}
    }
    if (log.is_open() != 0)
	log.close();
}






#ifdef USE_EMDL1_DISTANCE
void Histogram::computeEMDL1Dist(FL * p_h1, FL * p_h2, FL * p_h2F,
				 double *p_distmat,
				 double *p_distmat_flipped)
{

    PRINT_COLOR_(std::cout << "EMD L1  distance" << std::endl, RED);


    sprintf(filename, "%s_histolog", template_filename);
    std::ofstream log(filename);
    if (log.is_open() == 0) {
	DBGout << "Can not write log printing on standard output" << std::
	    endl;
    }
    else {
	DBGout << "Results of computeEMD_L1_Dist  printed to log: " <<
	    filename << std::endl;
    }
    if (log.is_open() == 0) {
	DBGout << "EMD L1  distance printed on screen" << std::endl;
    }
    else {
	log << "EMD L1 distance: " << filename << std::endl;
    }

    double *l_temp_distmat = new double[dim * dim];
    double *l_temp_distmatflipped = new double[dim * dim];
    EmdL1 *C_Emd = new EmdL1();

    for (int i = 0; i < dim; i++) {
	for (int j = 0; j < dim; j++) {
	    double l_dist = C_Emd->EmdDist(p_h1 + i * num_of_bins,
					   p_h2 + j * num_of_bins,
					   num_of_bins, 1);
	    if (log.is_open() == 0) {
		PRINTout << "Distance for " << i << " " << j << " : " <<
		    l_dist << std::endl;
	    }
	    else {
		log << "Distance for " << i << " " << j << " : " << l_dist
		    << std::endl;
	    }
	    l_temp_distmat[i * dim + j] = l_dist;
	}
    }


    print_flmatrix(l_temp_distmat, "tmp/error_hist_emd_pos", dim, dim);

    for (int i = 0; i < dim; i++) {
	for (int j = 0; j < dim; j++) {
	    double l_dist = C_Emd->EmdDist(p_h1 + i * num_of_bins,
					   p_h2F + j * num_of_bins,
					   num_of_bins, 1);
	    if (log.is_open() == 0) {
		std::
		    cout << "Distance flipped for " << i << " " << j <<
		    " : " << l_dist << std::endl;
	    }
	    else {
		log << "Distance flipped for " << i << " " << j << " : " <<
		    l_dist << std::endl;
	    }
	    l_temp_distmatflipped[i * dim + j] = l_dist;
	}
    }
    print_flmatrix(l_temp_distmatflipped, "tmp/error_hist_emd_neg", dim,
		   dim);

    if (p_distmat)
	bzero(p_distmat, dim * dim * sizeof(FL));
    else {
	p_distmat = new double[dim * dim];
	bzero(p_distmat, dim * dim * sizeof(FL));
    }

    double *l_tmp_out = p_distmat;
    double *l_tmp_out_F = p_distmat_flipped;
    double *l_tmp_d = l_temp_distmat;
    double *l_tmp_df = l_temp_distmatflipped;
    double *l_tmp_out_end = p_distmat + dim * dim;

    while (l_tmp_out < l_tmp_out_end) {
	*l_tmp_out = std::min(*l_tmp_d, *l_tmp_df);
	if (*l_tmp_out == *l_tmp_df && *l_tmp_d != *l_tmp_df )
	    *l_tmp_out_F = 1;

	++l_tmp_d;
	++l_tmp_df;
	++l_tmp_out;
	++l_tmp_out_F;
    }

    delete C_Emd;

    if (log.is_open() == 0)
	log.close();
}
#endif
#ifdef USE_DIFFUSION_DISTANCE
void Histogram::computeDiffusionDist(FL * p_h1, FL * p_h2, FL * p_h2F,
				     double *p_distmat,
				     double *p_distmat_flipped)
{

    PRINT_COLOR_(std::cout << "Histogram Diffusion  distance" << std::endl,
		 RED);

    sprintf(filename, "%s_histolog", template_filename);
    std::ofstream log(filename);
    if (log.is_open() == 0) {
	PRINTout << "Can not write log printing on standard output" <<
	    std::endl;
    }
    else {
	PRINTout << "Results of computeDiffusion_Dist  printed to log: " <<
	    filename << std::endl;
    }

    if (log.is_open() == 0) {
	DBGout << "Diffusion  distance printed on screen" << std::endl;
    }
    else {
	log << "Diffusion distance: " << filename << std::endl;
    }

    double *l_temp_distmat = new double[dim * dim];
    double *l_temp_distmatflipped = new double[dim * dim];
    dd_dist *C_Diffdist = new dd_dist();





    for (int i = 0; i < dim; i++) {
	for (int j = 0; j < dim; j++) {
	    double l_dist = C_Diffdist->dd2D(p_h1 + i * num_of_bins,
					     p_h2 + j * num_of_bins,
					     num_of_bins, 1);
	    if (log.is_open() == 0) {
		PRINTout << "Distance for " << i << " " << j << " : " <<
		    l_dist << std::endl;
	    }
	    else {
		log << "Distance for " << i << " " << j << " : " << l_dist
		    << std::endl;
	    }
	    l_temp_distmat[i * dim + j] = l_dist;
	}
    }


    for (int i = 0; i < dim; i++) {
	for (int j = 0; j < dim; j++) {
	    double l_dist = C_Diffdist->dd2D(p_h1 + i * num_of_bins,
					     p_h2F + j * num_of_bins,
					     num_of_bins, 1);
	    if (log.is_open() == 0) {
		PRINTout << "Distance flipped for " << i << " " << j <<
		    " : " << l_dist << std::endl;
	    }
	    else {
		log << "Distance flipped for " << i << " " << j << " : " <<
		    l_dist << std::endl;
	    }
	    l_temp_distmatflipped[i * dim + j] = l_dist;
	}
    }

    if (p_distmat)
	bzero(p_distmat, dim * dim * sizeof(FL));
    else {
	p_distmat = new double[dim * dim];
	bzero(p_distmat, dim * dim * sizeof(FL));
    }

    double *l_tmp_out = p_distmat;
    double *l_tmp_out_F = p_distmat_flipped;
    double *l_tmp_d = l_temp_distmat;
    double *l_tmp_df = l_temp_distmatflipped;
    double *l_tmp_out_end = p_distmat + dim * dim;

    while (l_tmp_out < l_tmp_out_end) {
	if (*l_tmp_d == *l_tmp_df) {
	    *l_tmp_out = 1;
	} else {
	    *l_tmp_out = std::min(*l_tmp_d, *l_tmp_df);
	    if (*l_tmp_out == *l_tmp_df && *l_tmp_d != *l_tmp_df)
		*l_tmp_out_F = 1;
	}

	++l_tmp_d;
	++l_tmp_df;
	++l_tmp_out;
	++l_tmp_out_F;
    }

    if (log.is_open() == 0)
	log.close();

    delete C_Diffdist;
    delete[]l_temp_distmat;
    delete[]l_temp_distmatflipped;

}
#endif

//----------------------------------------------------------------------------
// ASSIGNMENT  
//----------------------------------------------------------------------------
void Histogram::assign(ASSIGNMENT_METHOD p_AssignmentMethod,
		       FL * p_assignment_hist, FL * p_cost,
		       FL * p_error_hist, FL * p_error_hist_Flipped,
		       FL * p_T)
{

    memset(p_assignment_hist, 0, dim * sizeof(FL));

    switch (p_AssignmentMethod) {

    case FABIO:
	computeFabioAssignment(p_assignment_hist, p_error_hist,
			       p_error_hist_Flipped, p_T);
	break;
    case OPTIMAL:
	computeOptimalAssignment(p_assignment_hist, p_cost, p_error_hist,
				 p_error_hist_Flipped, p_T);
	break;

    case SUBOPTIMAL1:
	computeSubOptimalAssignment(p_assignment_hist, p_cost,
				    p_error_hist, p_error_hist_Flipped,
				    p_T);
	break;

    case SUBOPTIMAL2:
	DBG_(std::cout << "Not yet ported" << std::endl);
	break;

    case TEST_ALL:
	computeOptimalAssignment(p_assignment_hist, p_cost, p_error_hist,
				 p_error_hist_Flipped, p_T);
	computeSubOptimalAssignment(p_assignment_hist, p_cost,
				    p_error_hist, p_error_hist_Flipped,
				    p_T);
	break;

    default:
	DBG_(std::
	     cout << "Unknown assignment method " << p_AssignmentMethod <<
	     std::endl);
	break;
    }


}

void Histogram::computeFabioAssignment(FL * p_assignment_hist,
				       FL * p_error_hist,
				       FL * p_error_hist_Flipped, FL * p_T)
{

    PRINT_COLOR_(std::cout << "compute Fabio's idea Assignment" << std::
		 endl, GREEN);

    assignmentFabio(p_error_hist, dim, dim);

    if (template_filename) {
	strcpy(filename, template_filename);
	strcat(filename, ".initFabioAss");
	print_flmatrix(p_assignment_hist, filename, 1, dim);
    } else {
	print_flmatrix(p_assignment_hist, "tmp/initFabioAss", dim + 1,
		       dim + 1);
    }


    memset(p_T, 0, (dim + 1) * (dim + 1) * sizeof(FL));
    for (int i = 0; i < dim; ++i) {
	if (p_error_hist_Flipped[i * dim + (int) p_assignment_hist[i]]) {
	    p_T[i * (dim + 1) + (int) p_assignment_hist[i]] = -1;
	} else {
	    p_T[i * (dim + 1) + (int) p_assignment_hist[i]] = 1;
	}
    }
    p_T[(dim + 1) * (dim + 1) - 1] = 1.0;



    if (template_filename) {
	strcpy(filename, template_filename);
	strcat(filename, ".initFabioT");
	print_flmatrix(p_T, filename, dim + 1, dim + 1);
    } else {
	print_flmatrix(p_T, "tmp/initFabioT", dim + 1, dim + 1);
    }


    for (int i = 0; i < dim; i++) {
	for (int j = 0; j < dim; j++) {
	    std::cout << p_T[i * (dim + 1) + j] << " ";
	}
	std::cout << std::endl;
    }

}









void Histogram::computeOptimalAssignment(FL * p_assignment_hist,
					 FL * p_cost, FL * p_error_hist,
					 FL * p_error_hist_Flipped,
					 FL * p_T)
{
    PRINT_COLOR_(std::cout << "compute Optimal Assignment" << std::endl,
		 GREEN);

    FL *l_error_hist_transposed = new FL[dim * dim];
    memset(l_error_hist_transposed, 0, dim * dim * sizeof(FL));


    for (int i = 0; i < dim; i++) {
	for (int j = 0; j < dim; j++) {
	    l_error_hist_transposed[j * dim + i] =
		p_error_hist[i * dim + j];
	}
    }
    print_flmatrix(l_error_hist_transposed, "tmp/error_hist_transposed",
		   dim, dim);

    assignmentOptimal(p_assignment_hist, p_cost, l_error_hist_transposed,
		      dim, dim);



    if (template_filename) {
	strcpy(filename, template_filename);
	strcat(filename, ".initOptimalAss");
	print_flmatrix(p_assignment_hist, filename, 1, dim);
    } else {
	print_flmatrix(p_assignment_hist, "tmp/initOptimalAss", 1, dim);
    }


    memset(p_T, 0, (dim + 1) * (dim + 1) * sizeof(FL));
    for (int i = 0; i < dim; ++i) {
	if (p_error_hist_Flipped[i * dim + (int) p_assignment_hist[i]]) {
	    p_T[i * (dim + 1) + (int) p_assignment_hist[i]] = -1;
	} else {
	    p_T[i * (dim + 1) + (int) p_assignment_hist[i]] = 1;
	}
    }
    p_T[(dim + 1) * (dim + 1) - 1] = 1.0;



    if (template_filename) {
	strcpy(filename, template_filename);
	strcat(filename, ".initOptT");
	print_flmatrix(p_T, filename, dim + 1, dim + 1);
    } else {
	print_flmatrix(p_T, "tmp/initOptT", dim + 1, dim + 1);
    }

    for (int i = 0; i < dim; i++) {
	for (int j = 0; j < dim; j++) {
	    std::cout << p_T[i * (dim + 1) + j] << " ";
	}
	std::cout << std::endl;
    }
    std::cout << std::endl;

    delete[]l_error_hist_transposed;

}




void Histogram::computeSubOptimalAssignment(FL * p_assignment_hist,
					    FL * p_cost, FL * p_error_hist,
					    FL * p_error_hist_Flipped,
					    FL * p_T)
{

    PRINT_COLOR_(std::cout << "Compute Sub Optimal Assignment" << std::
		 endl, GREEN);
    FL *l_error_hist_transposed = new FL[dim * dim];
    memset(l_error_hist_transposed, 0, dim * dim * sizeof(FL));


    for (int i = 0; i < dim; i++) {
	for (int j = 0; j < dim; j++) {
	    l_error_hist_transposed[j * dim + i] =
		p_error_hist[i * dim + j];
	}
    }

    assignmentSuboptimal2(p_assignment_hist, p_cost,
			  l_error_hist_transposed, dim, dim);

    if (template_filename) {
	strcpy(filename, template_filename);
	strcat(filename, ".initSubOptimalAss");
	print_flmatrix(p_assignment_hist, filename, 1, dim);
    } else {
	print_flmatrix(p_assignment_hist, "tmp/initSubOptimalAss", dim + 1,
		       dim + 1);
    }


    memset(p_T, 0, (dim + 1) * (dim + 1) * sizeof(FL));
    for (int i = 0; i < dim; ++i) {
	if (p_error_hist_Flipped[i * dim + (int) p_assignment_hist[i]]) {
	    p_T[i * (dim + 1) + (int) p_assignment_hist[i]] = -1;
	} else {
	    p_T[i * (dim + 1) + (int) p_assignment_hist[i]] = 1;
	}
    }
    p_T[(dim + 1) * (dim + 1) - 1] = 1.0;



    if (template_filename) {
	strcpy(filename, template_filename);
	strcat(filename, ".initSubOptT");
	print_flmatrix(p_T, filename, dim + 1, dim + 1);
    } else {
	print_flmatrix(p_T, "tmp/initSubOptT", dim + 1, dim + 1);
    }


    for (int i = 0; i < dim; i++) {
	for (int j = 0; j < dim; j++) {
	    std::cout << p_T[i * (dim + 1) + j] << " ";
	}
	std::cout << std::endl;
    }

    delete[]l_error_hist_transposed;

}


void Histogram::
setHistogramNormalization(HISTOGRAM_NORMALIZATION p_hist_norm)
{

    histogram_nomalization = p_hist_norm;

}


//----------------------------------------------------------------------
// SUB FUNCTIONS FOR DISTANCE COMPUTATION
//---------------------------------------------------------------------



double Histogram::difference(FL * hist1, FL * hist2, int num_bin,
			     double scale, double scale_h, int trans)
{

    FL *lhist1 = hist1;
    FL *lhist2 = hist2;


    int ind = 0;
    int i = 0;

    double dist = 0;
    double d = 0;
    double disp = 0;
    double t = 0;
    double h2 = 0;
    double h1 = 0;

    while (ind < num_bin - 1 && i < num_bin) {
	if (i + trans > -1 && i + trans < num_bin) {
	    h2 = t * (lhist1[ind + 1] - lhist1[ind]) + lhist1[ind];
	    h1 = scale_h * lhist2[i + trans];
	    d = h2 - h1;
	    if (h1 != 0 || h2 != 0)
		d = d / (h1 + h2);
	} else {
	    d = 1;

	}
	dist += d * d;
	disp = scale * (i + 1);
	ind = (int) floor(disp);
	t = disp - (double) ind;
	i++;
    }
    return dist;

}







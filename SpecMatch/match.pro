#################################################################################
# This file is part of SpecMatch.						#
# SpecMatch : Spectral Matching							#
# SpecMatch is free software: you can redistribute it and/or modifyit		#
# under the terms of the GNU General Public License as published by the		#
# Free Software Foundation, either version 3 of the License, or (at your	#
# option) any later version.							#
#										#
# SpecMatch is distributed in the hope that it will be useful, but WITHOUT	#
# ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or		#
# FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License		#
# for more details.								#
#										#
# You should have received a copy of the GNU General Public License		#
# along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		#
#										#
#   Copyright (C) 2007, 2008 INRIA						#
#   Authors : Diana Mateus, David Knossow, Radu Horaud				#
#   Contact :	mateus@in.tum.de						#
#										#
# It has been deposited to the "Agence pour la Protection des Programmes"       #
# It has an Inter Deposit Digital Number:					#
# IDDN.FR.001.100003.000.S.P.2009.000.10800					#
#										#
#################################################################################
include(commons.prf)


TEMPLATE = app
TARGET   = match



HEADERS  += \
	legal.h Constants.h utilities.h types.h drawprim.h \
	QuatTypes.h \
	EulerAngles.h \	
	histograms.h  \
	ShapeData.h mesh.h voxel.h embedding.h \
	neighbors.h distances.h \
	eigs.h generalized_eigs.h \
	embed.h \
	shape.h  visual_shape.h \
		match_em.h visual_em.h thread_em.h \
	constantReader.h variant.h
#In case of using emdL1 or Diffusion distance:
#after downloading the libraries as explained in
#INSTALL.txt, add
# path_to_header/dd_head.h path_to_header/emdL1.h



SOURCES  += legal.cpp utilities.cpp drawprim.cpp \
	EulerAngles.cpp \
	ShapeData.cpp mesh.cpp voxel.cpp embedding.cpp \
	neighbors.cpp distances.cpp \
	eigs.cpp generalized_eigs.cpp \
	embed.cpp \
	shape.cpp visual_shape.cpp \
	histograms.cpp hungarian.cpp \
	match_em.cpp thread_em.cpp match_em_line.cpp visual_em.cpp \
		constantReader.cpp main-match.cpp 
#In case of using emdL1 or Diffusion distance:
#after downloading the libraries as explained in
#INSTALL.txt, add
# path_to_header/dd_dist.cpp path_to_header/emdL1.cpp



/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
#ifndef EMBEDDING_H
#define EMBEDDING_H
/*
 *  embedding.h
 *  embed
 *
 *  Created by Diana Mateus and David Knossow on 9/14/07.
 *  Copyright 2007 INRIA. All rights reserved.
 *
 * This class provides a container for the embeddings computed from the meshes or voxels.
 * 
 */

#include <QGLViewer/qglviewer.h>
#include <stdio.h>
#include <fstream>

#include "types.h"
#include "utilities.h"


class Embedding {
  protected:
    // Are the embeddings loaded (from a file a file or computed)
    bool loaded;

    // number of eigenvalues, eigenvectors loaded.
    int dim;

    bool center_embedding;

    // Number of coordinates of the eigenvectors.
    int npoints;

    //Display
    // Contains the color of each point in the eigenspace.
    FL *color_vec;

    // Stores the color depth provided in load_color function.
    int color_depth;

    // stores the default color to display points
    FL color[3];

    // Size of points to be displayed
    float point_size;

    // Scale of the embedding. Allows to display the embeddings in the unit sphere.
    FL scale;

    // Number of triplets of embeddings created for display purposes.
    int nlist;

    // Current selected list to be displayed.
    int current_list;

    // Vector of OpenGL lists of triplet of embeddings to be displayed
    GLuint *trio_list;



  public:

     Embedding();
    ~Embedding();


    // Store the embeddings (eigenvectors from the SVD of the Graph created out of the voxels or mesh).
    FL *coords;

    // Coordinates of the embeddings after transformation (estimated for the matching process) is applied
    FL *coords_transfo;

    // Store the eigenvalue from the SVD of the Graph created out of the voxels or mesh.
    FL *eigenvalues;

    
    // Obvious
    bool isLoaded();

    // Reads the embeddings (eigenvector) from a file.
    // Parameters are:
    // - filename
    // - in_dim: number of eigenvectors to be read.
    // - remove_first: in some cases, the eigenvector corrsponding to eigenvalue 0 is stored in the file. This first eigenvector should not be kept.
    // - sample rate: decimate the number of coordinates read.
    int readEmbedding(const char *filename, int in_dim =
		      0, bool remove_first = false, int sample_rate = 1);

    // Read the eigenvalues from a file.
    int readEigenvalues(const char *filename, int in_dim = 0);

    // Normalize the embedding, so that it fits into a unit sphere. It is mandatory to scale the embedding when performing the matching.
    int normalizeEmbedding(NORMALIZATION_EMBEDDING type, int offset = 0);


    void removeNonRelevantPoints ( FL thres_embed_small_norms = -1) ;


    // Copy the embedding from allready loaded embedding. It allows to select some eigenvectors out of all. 
    // Parameters are:
    // - in_Embedding: array of eigenvectors (of dimension in_n * vec_dim)
    // - in_n: number of coordinates for the eigenvectors.
    // - in_dim: number of eigenvectors to retain from in_Embedding
    // - vec_dim: number of eigenvectors in in_Embedding
    // - remove_first: in some cases, the eigenvector corrsponding to eigenvalue 0 is stored in the file. This first eigenvector should not be kept.
    int loadEmbedding(FL * in_Embedding, int in_n, int in_dim, int vec_dim,
		      bool remove_first = false);

    // Copy the eigenvalues.
    int loadEigenvalues(FL * eigenvals, int in_n);

    // Copy colors. 
    int loadColor(float *color_in, int n, int depth = -1);

    //Access to values
    // Returns a pointer on coords.
    FL *getCoords();

    // Returns a pointer on eigenvalues.
    FL *getEigenvalues();

    // Returns the number of coordinates of the eigenvectores.
    int getNumOfPoints();

    // Returns the number of eigenvectors.
    int getDim();

    // Returns the number of lists created for display purposes
    int getNumOfList();

    // Returns the index of the current selected list
    int getCurrentList();

    // Set the index of the current selected list
    int setCurrentList(int list_id);

    // Return the scale of the Embedding (for display purposes)
    FL getScale();

    //Perform a rigid transformation on the coordinates
    void computeEmbedTransfo(FL * initT, FL * T);

    //Use the eigenvectors and eigenvalues to project a function
    //project a multidimensional function in to the basis formed by a selection (maxdim) of eigenvalues and eigenfunctions 
    //vec = eigenvectors , weights = eigenvalues
    //funcdim is the dimension of function
    //maxdim is the number of eigenvectors to consider
    //returns the aproximation of function
    //result should have the same size as function
    //sampling rate to just overload some and not all the points in the function (default all)
    int project(FL * projection, FL * function, int funcdim, int maxdim,
		int sample);


    //Display
    // Create the OpenGL lists of triplets of eigenvectors.
    int buildCoordsLists();

    // Create the OpenGL lists of triplets of eigenvectors being transformed (coords_transfo).
    int buildTranfoCoordsLists();

    // draws current list
    int drawCoordsList();

    // draws list given by listid
    int drawCoordsList(int listid);

};
#endif

/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
/*
 *  match_em.h
 *  
 *
 *  Created by Diana Mateus and David Knossow on 9/23/07.
 *  Copyright 2007 INRIA. All rights reserved.
 *
 * The Match class provides all the processings to perform the point 
 * registration between to set of points.
 * 
 * It provides necessary functions to initialize the point registration algorithm. 
 * Different options are possible (refer to types.h, INIT_MODE)
 *
 * It also provides different approaches to perform the EM algorithm (refer to types.h, EM_MODE).
 *
 * Evantually all results are stored in files.
 */

#ifndef MATCHEM_H
#define MATCHEM_H


#include <cv.h>

#include "Constants.h"
#include "types.h"
#include "utilities.h"
#include "histograms.h"

extern "C" {
#include "glib.h"
}
// Call to Lapack functions. 
extern "C" { 
    void dsymv_(const char *up, const int *N, const double *alpha, const double *A, const int *lda, const double *X, const int *incX, const double *beta, double *Y, const int *incY);
    
    double ddot_(const int *n, const double *x, const int *incx, const double *y, const int *incy);
} 


class Match {

 protected:
    // we switch to a less memory greedy algorithm. 
    // The counterpart: it is slower.
    // The idea is that the program allocate a matrix of data_size * data_size * sizeof(double).
    // For huge data_size, it is not possible. So instead of storing the computations in memory, we 
    // recompute everytime it is required.
    bool use_alpha_line ;


 private:

    //Input data

    //filenames to store results
    char *template_filename ; 
    char *filename ; 


    // Dimension of the datasets to be matched. For Voxels, it is generaly 3,
    // for eigenvectors, it is the dimension of the selected eigenspace.
    int data_dim;

    // Number of points/coordinates for each dataset.
    int nX;
    int nY;

    int nClass ;

    // Pointers to the data sets.
    FL *X;
    FL *Y;

    //Baricentric cluster contributions 
    FL *W;

    // Yt = T*Yt where T is the estimated transformation
    FL *Yt;

    //Masks disconnected components of X  ( Y) (all coords == 0 for a voxel)
    int *data_mask_X;
    int *data_mask_Y;

    // When performing the initialization, we have methods to select only relevant 
    // eigenvectors (for voxels, it is not meaningfull) to perform the registration 
    // with.
    int *dim_mask;

    // outliers : partition values to small
    int *mask_X;

    // unmatched points : not chosen as argmax from alpha
    int *mask_Y;


    //Filename of connectivity files
    char *connect1;
    char *connect2;

    
    // Do we remove first eigenvector when performing the matching.
    bool remove_first;




    //EM variables
    //posterior ->matching. Probability of an observation to belong to a class.
    FL **mat_alpha ;
    FL *vec_alpha ;

    //partition values to normalize alpha  (part[i] = Sum_j (alpha_(ij)).
    FL *part;

    //weight. beta(j) = Sum_i (alpha(ij)). if beta(j) < THRES, point j is not matched to X.
    FL *beta;

    // total sum of weights
    FL sum_beta;

    // Current iteration number in the EM process.
    int iter;

    // sigma of inliers. 
    FL sigma;
    FL sigma2;			//sigma^2


    // sigma of outliers : kept constant to compare at each annealing step. (OBSOLET)
    FL sigma_out;

    // Covariance of the matching.
    FL *covmat;
    CvMat covmat_;

    //Inverse mat of the covariance.
    FL *inv_covmat;
    CvMat inv_covmat_;

    //transformation estimated in the EM process
    FL *T;

    // Permutation and sign matrix
    int *initT;

    // copy of T.
    FL *lastT;

    // Dimension of T after histogram matching and without dim selection
    int init_dim;

    //matching results
    // Matching results had been computed.
    bool match_available;
    // arg max match (2 column variable with the matching indexes of x and y
    // respectively)
    int *ag_match;
    // same as before but considers the n most probable matches. 1 column for 
    // x and n for the y matching values.
    int *nag_match;
    // In previous matching, there can be multiple assignments. We force here to have a one to one assignment.
    int *best_one_to_one_match;


    //number of matches per class
    int n_max_match;



    //parameters
    // Ratio for outliers
    FL k_out;
    // rate of annealing
    FL kapa;


    // Refer to types.h
    EM_MODE em_mode;
    EM_STEP step;
    INIT_MODE init_mode;
    TRANSFO_MODE transfo_mode;


    int no_of_fiedler_corresp ;
    int *vec_fiedler_corresp ;
    int **fiedler_corresp ;

    //transformation parameters 
    //set with set_transfo_mode()

    //Use beta as weights 
    int weighted;

    //If true do not estimate translation
    int centered;

    //Forces the result to be a rotation
    int rot_only;


    //Termination values
    int max_iter;
    // Set to MIN_SIGMA but may be updated by update_min_sigma(). 
    // It allows for termination criteria of the EM process
    FL min_sigma;

    // Set to MIN_E. The error is Sum_ij (W_ij-Yt_ij).
    // It allows for termination criteria of the EM process.
    FL min_error;
    // Set to  MIN_T. changeT= Sum_k (lastT_k - T_k)
    FL min_changeT;

    //measures of convergence and performance (arrays of max_iter elements)
    // Evolution of the number of outlier along the iterations
    int *n_outliers;
    // Evolution of the number of non-matched points along the iterations
    int *n_unmatched;
    // Evolution of the error along the iterations
    FL *error;
    // Evolution of T along the iterations
    FL *changeT;

    bool Yt_modified ;

    int *closest_near_nei_YtX ;


    //sigma and covariance initialization
    //needed with ISO_ANNEAL verify before E step that it is initialized
    bool sigma_initialized;

    int str_length ;
    int gt_level ;

    double beta_th ;
    double zero_th ;
    double out_th ;

    //auxiliary statistics functions. Names are obvious.
    int factorial(int num);
    int mean(int n, int vdim, const FL * vector, FL * meanv, int *mask_in =
	     NULL);
    int std(int n, int vdim, const FL * vector, FL * stdv, int *mask_in =
	    NULL, FL * meanv_in = NULL);
    int covariance(int n, int vdim, const FL * vector, int *mask_in =
		   NULL, FL * meanv_in = NULL);


    // When sigma is not set manually, it is computed here. 
    // Calculates the sigma for the affinity matrix.
    int initSigma( ) ;

    int initSigma( SIGMA_INIT_TYPE p_type ) ;



    int computeSigmaInit(FL * A, int na, FL * B, int nb);

    // Call init_sigma with different entries.
    int computeSigmaFromXY();	//can be removed by just applying an I transfo which copies the result to Yt
    int computeSigmaFromWYt();
    int computeSigmaFromXYt();

    // Compute sigma from the set of nearest neighbors.
    int computeSigmaFromNN(int nn);

    // Find in vec2 the nearest neighbors of points in Vec1
    int findNN(int *bestnn, FL * vec1, int n1, FL * vec2, int n2, int dim,
	       int nn);

    // Computes Sum_ij (W_ij-Yt_ij)
    FL estimateError();

    // initializes with a diagonal filled with previously initialized sigma
    int initCov();
    int initCovFromNN(int nn);

    FL mean_cluster_dist ;
    FL max_closest_dist ;
    void computeClusterMeanAndMaxDistance () ;
    
    // Initialize Min sigma to the max distance between the closest points 
    // in the set of clusters.
    int initMinSigma () ;
    //update sigma and covariance
    int updateMinSigma();
    //update the sigma using only matched points 
    FL computeSigmaFromMatches() ;
    //Update Sigma given formula introduced in related pa
    FL updateSigma() ;
    FL lineUpdateSigma() ;
    
    int updateCov();

    // Computes Alpha matrix and the normalization factors
    void computeAlphaAndPart( );




    //to be consistent force |Y| > |X|
    //only taken into account when printing
    //X and Y are actually changed roles
    bool xy_inversed;


    //INITIALIZATION
    //matching eigenfunctions with histograms;

    // Number of bins per histograms
    int numOfBins;

    // Usefull in searchNBestAssignment.    
    FL discriminant_ratio;

    //Refer to types.h
    HISTOGRAM_DISTANCE histogram_distance;
    ASSIGNMENT_METHOD assignment_method;

    //deletes the masked dimensions and copies the result to vectors of coherent dimension
    //changes dim and reallocates all the dim dependent variables
    int dimensionSelection(int new_dim, int *maskdim);


    // Initialize the EM algorithm by finding the Permutations and signs flips of eigenvectors.
    // Fills histograms, compute the distance between all the histograms, and make an assignment
    int matchEigenFunc();

    //Histograms
    FL *vec_hist_X, *vec_hist_Y, *vec_hist_FY;

    //Evaluate the smoothness of the matching. 
    //Allows to evaluate the performance of the matching.
    bool eval_smoothness ;



    
 public: 
    Match();
    ~Match() ;

    void init () ;

    void setAlphaLineMode (bool on = false) ;
    bool getAlphaLineMode( ) ;

    void checkEmMode () ;

    // Assignment estimation
    int E();
    // Transformation estimation
    int M();

    // Compute the covariance of the alpha matrix.
    int computeMCovariance();

    // Eval all criteria to decide if stopping the EM.
    int evalTermination();


    FL norm_fact;		// normalization of the gaussian distributions. 1.0 if cancels out when using an outlier term 

    FL outlier_term;		// set to K_OUT if constant 


    //Line EM
    //version which does not fill alpha but estimates at each step the line or column of interest
    //use it for large data set. Slower!!!
    // When selecting the ALPHA_LINE mode, we can sample the lines to perform computations on.
    int line_sample;
    int setLineSample(int val);

    int lineEM(INIT_MODE init_m, EM_MODE em_m, int *int_arg, FL * fl_arg);


    // Compute alpha for a given line
    int computeAlphaij(FL & alphaij, FL * x_idx, FL * y_idx, FL * &distv,
		       CvMat & distv_, CvMat * &dummy);
    FL computeAlphaijFromEuclideanDistance( FL * p_x_idx, FL * p_y_idx, FL * &distv ) ;
    FL computeAlphaijFromCovariance ( FL * p_x_idx, FL * p_y_idx,
					    FL * &p_distv ) ;
    //E line
    int lineE();
    int initIsoAnneal( ) ;
    int initGlobalCov(FL * &distv );

    //M line
    int lineM();

    int evalBestMatch(int i, FL val, int *&best_id, FL * &best_val,
		      int &worst_id, FL & worst_val);
    int fillNAgMatch(int j, int *best_id, FL * best_val);

    // Only available for ALPHA_LINE mode
    int fillOneToOneMatchHash(int class_idx, int *bestAssObsIdx,
			      FL * bestAssObsProb,
			      GHashTable ** obs_to_class);
    int getBestOnetoOneAssignment(GHashTable ** obs_to_class);



    

    //Transformation estimation;
    int transform();		//apply the transformation
    int huang(int weighted, int centered, int rotation_only);

    
    // Histograms
    Histogram C_Histo;		//holds parameters
    int sort(FL * in, int n, int vdim, int *orderout = NULL);	// IN PLACE FUNCTION
    FL *getVecHistX();
    FL *getVecHistY();
    FL *getVecHistFY();

    //print outputs
    int printOutputs();		// error, outliers, unmatched, changeT
    int printMatches();		// argmax nargmax best
    int printT();

    //interface
    int loadData(int nx, int ny, int dim, FL * inX, FL * inY,
		 char *filename = NULL);

    // Core of the matching.
    int EM(INIT_MODE init_m, EM_MODE em_m, int *int_arg =
	   NULL, FL * fl_arg = NULL);

    // Set histogram parameters to perform the intialization
    int setHistoParams(int num_of_bins, HISTOGRAM_DISTANCE histDist,
		       ASSIGNMENT_METHOD assignMethod, FL disc_ratio);

    // Initialize all EM parameters
    int initEM(INIT_MODE init_m, int *int_arg = NULL, FL * fl_arg = NULL);

    // Manually set Sigma.
    int setSigma(FL val) ;

    // Set criteria to stop the EM algo.
    int loadTerminationCriteria(int maxiter, FL minsigma, FL minchangeT,
				FL minerror);

    // Set the outlier term for estimating the assignment.
    int setOutlierConstant(FL kout_in);

    //Match estimations
    bool isMatchAvailable();

    // For all clusters retain the n bests observations associated
    int computeNArgMaxMatch() ;

    // For each observation retain the best associated cluster.
    int computeArgMaxMatch();

    int printArgMaxMatch(const char *filename);
    int printNArgMaxMatch(const char *filename);

    // only available in ALPHA_LINE mode
    int printBestMatch(const char *filename);



    //access to class variables
    int getDataDim();
    int getDimOfX();
    int getDimOfY();
    FL *getX();
    FL *getY();
    FL *getYt();
    FL *getW();
    int *getMaskX();
    int *getMaskY();
    int *getInitT();
    int getInitDim();
    int getNumOfBins();

    int getNMaxMatch();
    int *getNAgMatch();
    int *getAgMatch();
    FL getSigma();
    CvMat *getCovMatCV();

    EM_STEP getStep();
    EM_MODE getMode();


    //mode setting
    void setMode(EM_MODE mode_in);
    void setStep(EM_STEP step_in);
    int setTransfoMode(TRANSFO_MODE mode_in );

    void setRemoveFirst(bool remove);
    // Enable or Disable performance evaluation of the matching
    void enableEvalSmoothness (bool on) ;

    TRANSFO_MODE getTransfoMode();
    int setOutlierConstant(int kout_in);


    

    


    typedef FL (Match::*computeAlphaProto)(FL * x_idx, FL * y_idx, FL * &distv) ;

    computeAlphaProto computeAlpha ;


};

#endif

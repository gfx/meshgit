/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
/*
 *  sequence.h
 *  
 *
 *  Created by Diana Mateus and David Knossow on 11/6/07.
 *  Copyright 2007 INRIA. All rights reserved.
 *
 * This class is a high level one. It provides all access 
 * functions required to read the input files and perform 
 * all the processings. It manages the data, their copies 
 * and their release.
 */
#ifndef SEQUENCE_H
#define SEQUENCE_H


#include "voxel.h"
#include "mesh.h"
#include "embed.h"

#include "embedding.h"

#define STRLEN 1024

class Sequence {

  protected:
    //voxel formats 
    VOXEL_TYPE voxel_format;

    METHOD embed_method;

    // filename stores file names built for convenience (temporary variable).
    char *filename;

    // store the voxel filename
    char *voxel_basename;

    // store the mesh filename
    char *mesh_basename;


    NORMALIZATION_EMBEDDING normalize_embed ;
    
    // store the connectivity filename
    char *nei_basename;

    // store the label filename
    char *lbl_basename;

    // store the embedding filename
    char *embed_basename;

    // store the string pattern for file as nei_filename. Allows to built the filename from a string and for instance an image number : %s_%d
    char *pattern;

    //active when reading both voxels and function (e.g embedding)
    bool function_loaded;

    //current frame;
    int current_f;

    // Number of frames the soft will process.
    int n_frames;

    // Start ID of the image sequence. 
    int start;

    // See distance.h
    NEIGHBORHOOD dist_mode;

    // Sets the mode to compute the laplacian. See types.h
    LAPLACIAN lap_mode;

    // Depending on the dist_mode. If SPARSE is set, it represents the max number of neighbors selected.
    int K;

    // Size of the ball in which to compute the neighbors.
    FL epsilon;

    // Set the r-ring neighborhood. 
    int depth;

    // Set the r-ring neighborhood. 
    int seq_depth;

    // Max number of eigenvalues (eigenvectors) to be computed when performing the embedding.
    int n_eigenfunc;

    // Normalization factor when computing the edges of the graph. Can be set by the user using the -t option, or it is automaticaly computed while performing the embedding.
    FL sigma;

    // see EDGES in embed.h
    EDGES edge_type;

    // Set the normalization of the embeddings by the eigenvalues.
    NORMALIZATION_FORMAT normalization_format;

    // In the case of diffusion distance, depth is set to 1, and the diffusion is taken into account by 
    // elevating the eigenvalues at the power of depth. 
    int normalization_factor_power;

    // Set the distance computation mode. See comments on EDGES.    
    bool use_depth_as_dist;

    // Set the type of distance to compute between nodes of the graph.
    GRAPH_DISTANCE_TYPE graph_distance_type ;

    
    // Remove or not the first eigenvector associated with 0 eigenvalue when performing the embeddings.
    bool remove_first;


    //Enable/Disable the cut of disconnected componnents. In case of testing purposes, we want to keep disconnected comps.
    bool cut_disc_comps ;


    // For testing purposes. Remove points in embeded space, which norm is close to 0.
    FL thres_embed_small_norms ;

    // Load voxels from a file. 
    // Depending on VOXEL_TYPE, it reads voxels as 
    // - points (X Y Z), 
    // - voxels and function value (function can be a label or anything), 
    // - a matlab voxel grid.
    int loadVoxelset();

    // Load meshes from an off file.
    int loadMeshset();
    


    // Load labels from a file.
    int loadLabels();

    // Load eignevalues from a .eval file. The number of eigenvectors to be read is provided by n_eigenfunc.
    int loadEmbedding();







    // Main loop function. Provides the voxelset embedding.
    int callEmbed() ;
    // Main loop function. Provides the meshset embedding.
    int callMeshEmbed() ;
        
    

    void setEmbedParameters () ;


    // Enable the saving of the complete distance matrix in a file.
    bool enable_save_distmat;
    // Enable the saving of the eigenvalues in a file.
    bool enable_save_eigvalues;
    // Enable the saving of the parameters used to perform all the processings in a file.
    bool enable_save_parameters;

    

    


  public:
    // Class Constructor
     Sequence();
    // Class destructor
    ~Sequence();

    // Release all allocated data.
    void release();

    // Pointer on loaded voxel sets.
    Voxel **C_Voxel;

    // Pointer on loaded meshes.
    Mesh **C_Mesh;
    
      
        

    // Pointer on loaded or computed embeddings.
    Embedding **C_Embedding;

    // Embedding processor. Allows to compute the embeddings.
    Embed C_Embed;

    // Initialize all variables and classes to load voxelset from files.
    // - basename: voxel file name (absolute path to the file).
    // - pattern: if a sequence of files should be loaded, it sets the pattern of the sequence file name.
    // - n_frames: number of frames to be read in a sequence (must be 1 at leat).
    // - start: first frame ID. When not a sequence, it is set to 0.
    // - nei_basename: connectivity file name (absolute path to the file).
    // - save_basename: absolute path to the directory, and basename of file to be saved.
    // - lbl_basename: label file name (absolute path to the file).
    // - voxel_format: set the type of voxels being computed.
    int loadVoxelParams(char *in_basename, char *in_pattern,
			int in_n_frames = 1, int in_start =
			0, char *in_nei_basename =
			NULL, char *in_save_basename =
			NULL, char *in_lbl_basename =
			NULL, VOXEL_TYPE in_voxel_format = VOXEL);

    // Initialize all variables and classes to load meshes from files.
    // - basename: mesh file name (absolute path to the file).
    // - pattern: if a sequence of files should be loaded, it sets the pattern of the sequence file name.
    // - n_frames: number of frames to be read in a sequence (must be 1 at leat).
    // - start: first frame ID. When not a sequence, it is set to 0.
   // - save_basename: absolute path to the directory, and basename of file to be saved.
    int loadMeshParams(char *in_basename, char *in_pattern,
		       int in_n_frames, int in_start =
		       0, char *in_save_basename =
		       NULL );

    
    	

    // Itinialize all parameters to compute the embedings either from voxels or meshes.
	// - in_method: choose between LAP, ISOMAP, LLE
    // - in_lap_mode: refer to LAPLACIAN,
    // - in_dist_mode: refer to NEIGHBORHOOD in distance.h,
    // - in_dim: number of eigenvalues to be computed,
    // - in_k: depending on the dist_mode. if LOCAL GEODESIC distance is set, it is not used. If SPARSE is set, it represents the max number of neighbors selected. If mode is LOCAL_GEO, it is the r-ring depth,
    // - in_eps: Max dist between the center and the neighbor (if SPARSE distmode is set),
    // - sigma: if set, it sets the normalization factor when computing the weight matrix,
    // - edge_type: see EDGES in embed.h,
    // - use_depth_as_dist: use the r-ring depth as the distance and not the real distance.
    // - remove_first: allow to remove first eigen vector of the embeddings.
    int loadEmbeddingParams(int in_method, int in_lap_mode, int in_dist_mode, int in_dim,
			    int in_k, FL in_eps, FL in_sigma =
			    0.0, int in_edge_type =
			    0, bool in_use_depth_as_dist =
			    false, GRAPH_DISTANCE_TYPE p_graph_distance = EUCLIDIAN, bool in_remove_first =
			    true,
			    NORMALIZATION_FORMAT in_normalization_format =
			    NO_NORM, bool cut_disc_comps = false, NORMALIZATION_EMBEDDING p_normalize_embed = NO_NORM_EMBED, FL p_thres_embed_small_norms = -1);

    //Print all outputs of the embedding process.
    void printOutputs () ;

    // Main processing loop to compute the embeddings.
    int go();
};
#endif

/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
/*
 *  utilities.cpp
 *  SpecMatch
 *
 *  Created by David Knossow on 10/10/07.
 *  Copyright 2007 INRIA. All rights reserved.
 *
 */
#include <math.h>
#include "utilities.h"
#include "EulerAngles.h"

FL **allocateDMat(FL * p_vec, int p_size_h, int p_size_w)
{

    FL **l_mat = NULL;

    l_mat = new FL *[p_size_h];

    for (int i = 0; i < p_size_h; ++i) {
	l_mat[i] = (p_vec + i * p_size_w);
    }

    return l_mat;
}

float **allocateFMat(float *p_vec, int p_size_h, int p_size_w)
{

    float **l_mat = NULL;

    l_mat = new float *[p_size_h];

    for (int i = 0; i < p_size_h; ++i) {
	l_mat[i] = (p_vec + i * p_size_w);
    }

    return l_mat;
}

int **allocateIMat(int *p_vec, int p_size_h, int p_size_w)
{
    int **l_mat = NULL;
    l_mat = new int *[p_size_h];
    for (int i = 0; i < p_size_h; ++i) {
	l_mat[i] = (p_vec + i * p_size_w);
    }
    return l_mat;
}

FULL_MAT_TYPE **allocateMat(FULL_MAT_TYPE *p_vec, int p_size_h, int p_size_w)
{

    FULL_MAT_TYPE **l_mat = NULL;

    l_mat = new FULL_MAT_TYPE *[p_size_h];

    for (int i = 0; i < p_size_h; ++i) {
	l_mat[i] = (p_vec + i * p_size_w);
    }

    return l_mat;
}


int print_flmatrix(FL * p_matrix, const char *p_filename, int p_rows,
		   int p_cols)
{

    if (!p_matrix) {
	ERROR_(fprintf(stderr, "Trying to print NULL matrix"),
	       ERROR_EMPTY_DATA);
    }

    WARNING_(fprintf(stderr, "Printing %s\n", p_filename));

    FILE *f = NULL;
    f = fopen(p_filename, "w");

    if (!f) {
	ERROR_(fprintf(stderr, "could not open file %s \n", p_filename),
	       ERROR_OPEN_FILE);
    }

    for (int r = 0; r < p_rows; r++) {
	for (int c = 0; c < p_cols; c++) {
	    fprintf(f, "%g   ", p_matrix[r * p_cols + c]);
	}
	fprintf(f, "\n");
    }
    fclose(f);

    DBG_(fprintf(stderr, " %s printed\n", p_filename));

    return 0;
}



int print_full_matrix(FULL_MAT_TYPE * p_matrix, const char *p_filename, int p_rows,
		   int p_cols)
{

    if (!p_matrix) {
	ERROR_(fprintf(stderr, "Trying to print NULL matrix"),
	       ERROR_EMPTY_DATA);
    }

    WARNING_(fprintf(stderr, "Printing %s\n", p_filename));

    FILE *f = NULL;
    f = fopen(p_filename, "w");

    if (!f) {
	ERROR_(fprintf(stderr, "could not open file %s \n", p_filename),
	       ERROR_OPEN_FILE);
    }

    for (int r = 0; r < p_rows; r++) {
	for (int c = 0; c < p_cols; c++) {
	    fprintf(f, "%g   ", p_matrix[r * p_cols + c]);
	}
	fprintf(f, "\n");
    }
    fclose(f);

    DBG_(fprintf(stderr, " %s printed\n", p_filename));

    return 0;
}


int print_floatmatrix(float *p_matrix, const char *p_filename, int p_rows,
		      int p_cols)
{

    if (!p_matrix) {
	ERROR_(fprintf(stderr, "Trying to print NULL matrix"),
	       ERROR_EMPTY_DATA);
    }

    WARNING_(fprintf(stderr, "Printing %s\n", p_filename));
    FILE *f = NULL;
    f = fopen(p_filename, "w");
    if (!f) {
	ERROR_(fprintf(stderr, "could not open file %s \n", p_filename),
	       ERROR_OPEN_FILE);
    }
    for (int r = 0; r < p_rows; r++) {
	for (int c = 0; c < p_cols; c++) {
	    fprintf(f, "%g\t", p_matrix[r * p_cols + c]);
	}
	fprintf(f, "\n");
    }
    fclose(f);

    DBG_(fprintf(stderr, " %s printed\n", p_filename));

    return 0;
}


int print_intmatrix(int *p_matrix, const char *p_filename, int p_rows,
		    int p_cols)
{
    if (!p_matrix) {
	ERROR_(fprintf(stderr, "Trying to print NULL matrix"),
	       ERROR_EMPTY_DATA);
    }

    WARNING_(fprintf(stderr, "Printing %s\n", p_filename));
    FILE *f = NULL;
    f = fopen(p_filename, "w");
    if (!f) {
	ERROR_(fprintf(stderr, "could not open file %s \n", p_filename),
	       ERROR_OPEN_FILE);
    }
    for (int r = 0; r < p_rows; r++) {
	for (int c = 0; c < p_cols; c++) {
	    fprintf(f, "%d\t", p_matrix[r * p_cols + c]);
	}
	fprintf(f, "\n");
    }
    fclose(f);
    DBG_(fprintf(stderr, " %s printed\n", p_filename));
    return 0;
}



int compareFuncOnSparseStorage(const void *d1, const void *d2)
{
    int status = 1;
    SparseStorage *a = (SparseStorage *) d1;
    SparseStorage *b = (SparseStorage *) d2;
    if ((a)->value > (b)->value)
	status = -1;

    return status;
}


int compareFunc(const void *d1, const void *d2)
{
    int status = 1;

    if (*((FL *) (d1) + 1) < *((FL *) (d2) + 1))
	status = -1;

    return status;
}


int compareFuncSortVertexY(const void *d1, const void *d2)
{
    int status = 1;

    if (*((FL *) (d1) + 1) < *((FL *) (d2) + 1))
	status = -1;

    return status;
}

int compareValueFuncDec(const void *d1, const void *d2)
{
    int status = 1;

    if (*((FL *) (d1) + 2) > *((FL *) (d2) + 2))
	status = -1;

    return status;
}




// C-Function provided to g_hash_table_new_full in build_dist_mat.
// It allows to free the memory allocated for the hash table.
void DeleteHashTable_value(gpointer data)
{
    if (data) {
	delete(HashTable_value *) data;
    }
    data = NULL;

}










void ConvertXYZ2Mat ( float * in, HMatrix M) {


  EulerAngles ea ;
  ea.x = in[0] * M_PI / 180 ;
  ea.y = in[1] * M_PI / 180  ;
  ea.z = in[2] * M_PI / 180  ;
  ea.w =  EulOrdXYZr ;
  Eul_ToHMatrix(ea, M) ;
}


float * RotatePoint(double **temp, float * Point) {

  float * res ;
  res = (float*)malloc(3 * sizeof(float)) ;

  res[0] = temp[0][0] * Point[0] + temp[0][1] * Point[1] + temp[0][2] * Point[2] ;
  res[1] = temp[1][0] * Point[0] + temp[1][1] * Point[1] + temp[1][2] * Point[2] ;
  res[2] = temp[2][0] * Point[0] + temp[2][1] * Point[1] + temp[2][2] * Point[2] ;

  return res ;


}




double gauss_rand (double mean, double dev)
{
  static int iset = 0;
  static double gset;
  double fac, r, v1, v2;

  /* Generate a new random number pair */
  /* Start by getting a random point in the unit circle */
  do {
      /* pick two unifor
	 m numbers in the square [-1, +1] in each direction */
      v1 = 2*(random()/(double) RAND_MAX) -1.0;
      v2 = 2*(random()/(double) RAND_MAX) -1.0;
      r = v1*v1 + v2*v2;     /* See if they fall in the unit circle */
  } while (r >= 1.0);
  
  /* Now do the Box-Muller transformation to get two normal deviates */
  /* return one and save the other */
  fac = sqrt(-2.0*log(r)/r);
  gset = v1*fac;
  iset = 1;
  return (v2*fac*dev + mean);  
  
}

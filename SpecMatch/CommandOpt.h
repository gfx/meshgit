/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
/*
 *  CommandOpt.h
 *  SpecMatch
 *
 *  Created by David Knossow on 06/12/08.
 *  Copyright 2007 INRIA. All rights reserved.
 *
 * This file contains enum of command line options.
 */



enum OPT {

    EMBED1 = 260,		// file name for first embedding
    EMBED2,			// file name for second embedding
    VOXEL1,			// file name for first voxel set
    VOXEL2,			// file name for second mesh set
    MESH1,			// file name for first mesh
    MESH2,			// file name for second mesh
    MESH3,			// file name for third mesh
    MESH4,			// file name for fourth mesh
    SILHOUETTE_IMAGE1,          //file name for first image set
    SILHOUETTE_IMAGE2,          //file name for second image set
    VOXEMBED1,			// file name for first voxel-embedding set
    VOXEMBED2,			// file name for second voxel-embedding set
    GRAPH1,			// file name for first graph
    GRAPH2,			// file name for second graph
    REMOVE_FIRST,		// Remove first dimansion when reading/loading the embeddings (might correpond to constant vector)
    NORMALIZE_EMBED,		// Normalize mode for the embeddings
    CONNECT1,			// File name for first connectivity
    CONNECT2,			// File name for second connectivity
    DIM,			// Dimension to select
    EDGE_TYPE,                  // Use Topologic distance, Kernelize the distance or let it raw.
    USE_DEPTH_AS_DIST,          // Instead of using a distance, use the ring sizze as distance.
    XMIN,			// Bounding box for displaying matches
    XMAX,			// "
    YMIN,			// "
    YMAX,			// "
    ZMIN,			// " 
    ZMAX,			// "
    EM,				// EM mode (ISO_ANNEAL/ GLOBA_COV)
    INIT,			// Init mode (initialize EM from ....)
    SIGMA,			// Force sigma to at given value
    OUTLIER_K,			// Outlier rejection rate
    T_MODE,			// Type of RIGID transformation to estimate
    HIST_DIST,			// Distance between histograms
    ASSIGN,			// Assignment  method for intial matching of eigenvectors
    BINS,			// Number of bins for histograms
    RATIO,			// Discriminant ratio to perform the selection of dscriminative eigenvectors.
    LIVE,			// 
    LINE_SAMPLE,		// Sampling rate to estimated the matching
    MATCH_SEQ,			// Enable reading of temporal evolution of matches
    FRAME_SEQ,			// Enable reading of multiple matches.
    WIN_TITLE,			// Set the window title.
    EVALUATE,                   // Enable the evaluation mode for the matching
    RX,                         // Rotate around X (in deg), for display purpose.
    RY,                         // Rotate around Y (in deg), for display purpose.
    RZ,                         // Rotate around Z (in deg), for display purpose.
    VIZU_SAMPLING,              // Sampling rate of line matches to display in vizushape  
    EVALSCORE,                  // Provide the filename to store the matching scores.  
    SORTMESH,                   // Enable/Disable sort of points on the meshes for display purposes 
    GRAPH_DISTANCE,             // Type of distance to compute between nodes of the graph built in embed.
    CUT_DISC_COMP,              // Allows to enable or disable the cut of disconnected componnents. 
    CUT_SMALL_NORM,             // Remove embedded points that are to colse to origin (norm < thres) 
    ADJENCY_MATRIX_TYPE,        // Set the type of adjency matrix to compute.
    EMBED_METHOD,               // Set the type of embedding to perform.
    LAPLACIAN_TYPE,             // Laplacian matrix shape.
    
    };






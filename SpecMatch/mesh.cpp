/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
/*
 *  mesh.cpp
 *  SpecMatch
 *
 *  Created by Diana Mateus and David Knossow on 8/16/07.
 *  The NEI manager had been re-written on March 2009.
 *  Copyright 2007 INRIA. All rights reserved.
 *
 */

#include <stdio.h>

#include "mesh.h"



using namespace std;


Mesh::Mesh()
{


  center_mesh = true;

  NEI_colormap = NULL;

  vertex = NULL;
  vertex_normal = NULL;
  vertex_color = NULL;
  nvert_facet = NULL;

  facets = NULL;
  facet_normal = NULL;

  MESHNEISET = -1;

  nvertex = 0;
  nfacets = 0;
  nedges = 0;

  max_num_vert_per_facets = -1 ;

  total_num_nei = NULL;

  facets = NULL ;

  weights = NULL;
  mat_weights = NULL;


  vec_depth = NULL;
  mat_depth = NULL;

  vertex_to_facets = NULL;
  nfacets_per_vertex = NULL;

  approx_voronoi_area = NULL;

  graph_distance_type = EUCLIDIAN ;

  use_depth_as_dist = false;
  scale = 1;


  revert_index_sort_mesh = NULL ;

  enable_sort_mesh = false ;


  hash_map_vector.clear () ;


}



Mesh::~Mesh()
{
  //Delete array of vertices 
  releaseData();
  // vertex points on a pointer called points that is release in Release_Data.
  vertex = NULL;


  if (revert_index_sort_mesh)
    delete [] revert_index_sort_mesh ;

  if (NEI_colormap)
    delete[]NEI_colormap;

  if (vertex_normal) {
    delete[]vertex_normal;
  }
  if (facet_normal) {
    delete[]facet_normal;
    facet_normal = NULL;
  }
  //Delete array of faces
  if (facets) {
    delete[]facets;
    facets = NULL;
  }
  if (nvert_facet) {
    delete[]nvert_facet;
    nvert_facet = NULL;
  }
  if (vertex_to_facets) {
    delete[]vertex_to_facets;
    vertex_to_facets = NULL;
  }
  if (nfacets_per_vertex) {
    delete[]nfacets_per_vertex;
    nfacets_per_vertex = NULL;
  }

  hash_map_vector.clear () ;


  if (approx_voronoi_area) {
    delete[]approx_voronoi_area;
    approx_voronoi_area = NULL;
  }


  if (weights) {
    delete [] weights;
    weights = NULL;
  }

  if (mat_weights) {
    delete[]mat_weights;
    mat_weights = NULL;
  }

  if (vec_depth) {
    delete[]vec_depth;
    vec_depth = NULL;
  }

  if (mat_depth) {
    delete[]mat_depth;
    mat_depth = NULL;
  }


  if (total_num_nei) {
    delete[]total_num_nei;
    total_num_nei = NULL;
  }





}


void Mesh::setDepthAsDist(bool p_status)
{

  use_depth_as_dist = p_status;

}


void Mesh::setGraphDistanceType(GRAPH_DISTANCE_TYPE p_graph_dist)
{

  graph_distance_type = p_graph_dist ;

}

int Mesh::readOffFile(char *p_offFileName)
{

  char l_next_string[256];


  init () ;


  in.open(p_offFileName);

  setColordepth(color_depth);

  if (! in.is_open() ) {
    FATAL_(fprintf(stderr, "Could not open %s \n", p_offFileName),
	   FATAL_OPEN_FILE) ;
  }

  while (in.peek() == ' ' || in.peek() == '\n')
    in.get();

  while (in.peek() == '#') {
    in.getline(l_next_string, 256);	// skip comment
  }

  in >> l_next_string;
  if (!strcmp(l_next_string, "OFF")) {
    PRINT_(std::cout << "Loading OFF file " << p_offFileName << std::
	   endl);
    readOff();
  } else if (!strcmp(l_next_string, "NOFF")) {
    PRINT_(std::
	   cout << "Loading NOFF file (with normals) " << p_offFileName
	   << std::endl);
    readNOff();
  } else if (!strcmp(l_next_string, "COFF")) {
    PRINT_(std::
	   cout << "Loading COFF file (with colors) " << p_offFileName
	   << std::endl);
    readCOff();
  } else {
    FATAL_(fprintf
	   (stderr, "%s : Unknown OFF file format \n", l_next_string),
	   FATAL_UNKNOWN_FORMAT);
  }

  readFacets();

  if (enable_sort_mesh) {
    PRINT_COLOR_(std::cout << "Sort the mesh..." <<std::endl, RED) ;
    sortMeshY () ;
  }

  findMeanAndScale(center_mesh);

  fillFacetNormals();

  in.close();

  PRINT_(std::cout << "Compute Neighborhoods and Distances" << std::
	 endl) ;


  computeNeis();


  if (!use_depth_as_dist) {
    switch (graph_distance_type) {
    case EUCLIDIAN:
      PRINT_COLOR_(std::cout << "Computing the euclidian distance between the vertices of the mesh" <<std::endl , GREEN) ;
      computeNormalizedEuclidianWeight(false) ;
      break ;
    case NORMALIZED_EUCLIDIAN:
      PRINT_COLOR_(std::cout << "Computing the NORMALIZED euclidian distance between the vertices of the mesh" <<std::endl , GREEN) ;
      computeNormalizationWeight() ;
	    
      computeNormalizedEuclidianWeight(false) ;

      break ;
    case COTAN_WEIGHT:
      PRINT_COLOR_(std::cout << "Computing the cotan-weight between the vertices of the mesh" <<std::endl , GREEN) ;
      computeCotanWeight();
	    
      break ;

    case NORMALIZED_COTAN_WEIGHT:
      PRINT_COLOR_(std::cout << "Computing the NORMALIZED cotan-weight between the vertices of the mesh" <<std::endl , GREEN) ;
      computeNormalizationWeight() ;

      computeCotanWeight() ;

      break ;
    }
  }

  loaded = true;


  DBG_(std::cout << "OFF File has been read" << std::endl);

  return 0;
}


void Mesh::readOff()
{

  in >> npoints >> nfacets >> nedges;
  nvertex = npoints;

  PRINT_(std::
	 cout << "Loading : " << nvertex << " vertices with " << nfacets
	 << " facets and " << nedges << " edges" << std::endl);

  if (vertex)
    delete[]vertex;

  points = new double[dim * nvertex];
  vertex = points;
  memset(vertex, 0, dim * nvertex * sizeof(double));

  if (vertex_color)
    delete[]vertex_color;

  points_color = new float[color_depth * nvertex];
  vertex_color = points_color;
  memset(vertex_color, 0, color_depth * nvertex * sizeof(float));

  for (int i = 0; i < nvertex; i++) {
    in >> vertex[dim * i + 0] >> vertex[dim * i +
					1] >> vertex[dim * i + 2];
    vertex_color[color_depth * i + 0] = 0.5;
    vertex_color[color_depth * i + 1] = 0.5;
    vertex_color[color_depth * i + 2] = 0.5;
    vertex_color[color_depth * i + 3] = 1;

    while (in.peek() != '\n')
      in.get();
  }

  // we do not close the file, it will be used in read facets.

}

void Mesh::readNOff()
{

  in >> npoints >> nfacets >> nedges;

  nvertex = npoints;

  PRINT_(std::
	 cout << "Loading : " << nvertex << " vertices with " << nfacets
	 << " facets and " << nedges << " edges" << std::endl);

  if (vertex)
    delete[]vertex;

  points = new double[dim * nvertex];
  vertex = points;
  memset(vertex, 0, dim * nvertex * sizeof(double));

  if (vertex_color)
    delete[]vertex_color;

  points_color = new float[color_depth * nvertex];
  vertex_color = points_color;
  memset(vertex_color, 0, color_depth * nvertex * sizeof(float));

  if (vertex_normal)
    delete[]vertex_normal;

  vertex_normal = new double[dim * nvertex];
  memset(vertex_normal, 0, dim * nvertex * sizeof(double));


  for (int i = 0; i < nvertex; i++) {
    in >> vertex[dim * i + 0] >> vertex[dim * i +
					1] >> vertex[dim * i + 2];
    in >> vertex_normal[dim * i + 0] >> vertex_normal[dim * i +
						      1] >>
      vertex_normal[dim * i + 2];
    vertex_color[color_depth * i + 0] = 0.5;
    vertex_color[color_depth * i + 1] = 0.5;
    vertex_color[color_depth * i + 2] = 0.5;
    vertex_color[color_depth * i + 3] = 1;

    while (in.peek() != '\n')
      in.get();
  }

  // we do not close the file, it will be used in read facets.

}

void Mesh::readCOff()
{

  in >> npoints >> nfacets >> nedges;

  nvertex = npoints;

  PRINT_(std::
	 cout << "Loading : " << nvertex << " vertices with " << nfacets
	 << " facets and " << nedges << " edges" << std::endl);

  if (vertex)
    delete[]vertex;

  points = new double[dim * nvertex];
  vertex = points;
  memset(vertex, 0, dim * nvertex * sizeof(double));

  if (vertex_color)
    delete[]vertex_color;

  points_color = new float[color_depth * nvertex];
  vertex_color = points_color;
  memset(vertex_color, 0, color_depth * nvertex * sizeof(float));

  for (int i = 0; i < nvertex; i++) {
    in >> vertex[dim * i + 0] >> vertex[dim * i +
					1] >> vertex[dim * i + 2];
    in >> vertex_color[color_depth * i +
		       0] >> vertex_color[color_depth * i +
					  1] >>
      vertex_color[color_depth * i +
		   2] >> vertex_color[color_depth * i + 3];
    while (in.peek() != '\n')
      in.get();
  }
}



void Mesh::readFacets()
{

  if (nvert_facet) {
    delete[]nvert_facet;
    nvert_facet = NULL;
  }
  if (facets) {
    delete[]facets;
    facets = NULL;
  }

  if (vertex_to_facets) {
    delete[]vertex_to_facets;
    vertex_to_facets = NULL;
  }
  if (nfacets_per_vertex) {
    delete[]nfacets_per_vertex;
    nfacets_per_vertex = NULL;
  }
  nvert_facet = new int[nfacets];
  
  HASH_MAP_PARAM_(int, "max_num_vert_per_facets", Int, max_num_vert_per_facets) ;
  facets = new int[max_num_vert_per_facets * nfacets] ;
  int *t_facets = facets;
  HASH_MAP_PARAM_(int, "max_num_facets_per_vertex", Int, max_num_facets_per_vertex ) ;

  vertex_to_facets = new int[nvertex * max_num_facets_per_vertex];
  nfacets_per_vertex = new int[nvertex];
  memset(nfacets_per_vertex, 0, nvertex * sizeof(int));
  memset(vertex_to_facets, 0,
	 nvertex * max_num_facets_per_vertex * sizeof(int));
  memset(facets, 0, max_num_vert_per_facets * nfacets * sizeof(int));

  for (int i = 0; i < nfacets; ++i) {
    in >> nvert_facet[i];
    if (nvert_facet[i] > max_num_vert_per_facets) {
      FATAL_(fprintf
	     (stderr,
	      "You must set MAX_NUM_VERT_PER_FACETS (in Constants.h) to at least %d \n"
	      "The facet normal computation does not support more the 3 vertices per facets \n",
	      nvert_facet[i]), FATAL_MEMORY_ALLOCATION);
    }
    for (int j = 0; j < nvert_facet[i]; ++j) {
      in >> *(t_facets + j);
      if (nfacets_per_vertex[*(t_facets + j)] >
	  max_num_facets_per_vertex) {
	FATAL_(fprintf
	       (stderr,
		"You must set  MAX_NUM_FACETS_PER_VERTEX (in Constants.h) to at least %d \n",
		nfacets_per_vertex[*(t_facets + j)]),
	       FATAL_MEMORY_ALLOCATION);
      }
      vertex_to_facets[*(t_facets + j) * max_num_facets_per_vertex +
		       nfacets_per_vertex[*(t_facets + j)]] = i;
      nfacets_per_vertex[*(t_facets + j)]++;
    }

    while (in.peek() != '\n')
      in.get();

    t_facets += max_num_vert_per_facets;
  }
	
  // the file is closed outside of this function.

}


void Mesh::findMeanAndScale(bool p_center)
{

  FL *l_mean = NULL;
  l_mean = new FL[dim];

  FL *l_minval = NULL;
  l_minval = new FL[dim];

  FL *l_maxval = NULL;
  l_maxval = new FL[dim];

  for (int i = 0; i < dim; i++) {
    l_mean[i] = 0.0;
    l_minval[i] = 0.0;
    l_maxval[i] = 0.0;
  }

  int idim = 0;

  // Find the center of mass of the mesh (each vertex hax mass=1).
  for (int i = 0; i < nvertex; i++) {
    for (int d = 0; d < dim; d++) {
      l_mean[d] += vertex[idim + d];
      if ((vertex[idim + d] < l_minval[d]) || (i == 0))
	l_minval[d] = vertex[idim + d];
      if ((vertex[idim + d] > l_maxval[d]) || (i == 0))
	l_maxval[d] = vertex[idim + d];
    }
    idim += 3;
  }

  idim = 0;

  for (int d = 0; d < dim; d++)
    l_mean[d] = l_mean[d] / ((FL) nvertex);

  // Center the mesh if required.
  if (p_center) {
    for (int i = 0; i < nvertex; i++) {
      for (int d = 0; d < dim; d++) {
	vertex[idim + d] -= l_mean[d];
      }
      idim += 3;
    }
  }
  // Find the scale factor of the mesh
  FL l_deltamax = fabs(l_maxval[0] - l_minval[0]);
  for (int j = 1; j < dim; j++) {
    if (l_maxval[j] - l_minval[j] > l_deltamax)
      l_deltamax = fabs(l_maxval[j] - l_minval[j]);
  }

  scale = l_deltamax;
  delete[]l_maxval;
  delete[]l_minval;
  delete[]l_mean;
}




void Mesh::computeNeis()
{



  hash_map_vector.clear () ;

  hash_map_vector.resize( nvertex ) ;

  int *l_t_facets = facets;
  int ind_j;
  int ind_k_left;
  int ind_k_right;
  
  MapMesh map_mesh ;

  for (int i = 0; i < nfacets; ++i) {
    for (int j = 0; j < nvert_facet[i]; ++j) {
      
      
      ind_j = *(l_t_facets + j) ;
      
      int l_k_left = j+1 ;
      int l_k_right = j-1 ;
      if ( l_k_left >=  nvert_facet[i])
	l_k_left = 0 ;
      if (l_k_right < 0)
	l_k_right =  nvert_facet[i] - 1 ;

      ind_k_left = *(l_t_facets + l_k_left) ;
      ind_k_right = *(l_t_facets + l_k_right) ;
      
      map_mesh.exists = true ;
      map_mesh.dist = 0 ;
      map_mesh.prev_dist = 0 ;
      map_mesh.depth = 1 ;

      hash_map_vector[ind_j].insert(make_pair(ind_k_right,map_mesh)) ;
      hash_map_vector[ind_j].insert(make_pair(ind_k_left,map_mesh)) ;

    }
    l_t_facets += max_num_vert_per_facets;

  }
  
  //  #ifdef DEBUG_3
  for (int i = 0; i < nvertex ; ++i) {
    for (hash_map< int, MapMesh >::iterator it = hash_map_vector[i].begin() ; it != hash_map_vector[i].end() ; ++it ) {
      if (  hash_map_vector[it->first].find(i) ==  hash_map_vector[i].end() ) {
	FATAL_(std::cout << i << " " << it->first << "is not symetric" <<std::endl, FATAL_VARIABLE_INIT) ;
      }
    }

  }
  //#endif


}


void Mesh::computeCotanWeight()
{


  int *l_cotanpoint = new int[2];
  int l_count = 0;
  int l_morecount = 0;
  int l_lesscount = 0;

#ifdef DEBUG_3
  std::cout << "Values for the Cotangent weights" <<std::endl ;
#endif
  int pos_i_at_pi = -1 ;
  for (int i = 0; i < nvertex; ++i) {
    pos_i_at_pi = -1 ;


    for (hash_map< int, MapMesh >::iterator it = hash_map_vector[i].begin() ; it != hash_map_vector[i].end() ; ++it ) {
      // hash_map is made of pairs.
      // it.first returns the key, 
      // it->second return the data
      int pi = it->first ;

      if (it->second.dist == 0) {
	l_cotanpoint[0] = 0;
	l_cotanpoint[1] = 0;
	    
	l_count = 0;


	for (hash_map< int, MapMesh >::iterator k = hash_map_vector[i].begin() ; k != hash_map_vector[i].end() ; ++k ) {
	  for (hash_map< int, MapMesh >::iterator l = hash_map_vector[pi].begin() ; l != hash_map_vector[pi].end() ; ++l ) {
	    if ( k->first == l->first) {
	      if (l_count < 2) {
		    
		pos_i_at_pi = l->first ;
		    
		l_cotanpoint[l_count] = k->first ;
		    
	      } 
	      else {
		// the mesh is not correct for the cotan weight computations
		WARNING_(cout << i << " and " << pi <<
			 " There is more than 2 point (" << l_count
			 << ") in common...( " << vertex[dim * i +
							 0] << " "
			 << vertex[dim * i +
				   1] << " " << vertex[dim * i +
						       2] << ") ("
			 << vertex[dim * pi +
				   0] << " " << vertex[dim * pi +
						       1] << " " <<
			 vertex[dim * pi + 2] << ")" << endl);
		l_morecount++;
	      }
		  
	      l_count++;
		  
	    }	// end of if
	  }		// end of for l
	}		// end of for k
	    
	if (l_count < 2) {
	  //  the mesh is not correct for the cotan weight computations
	  WARNING_(cout << i << " and " << pi <<
		   " There is less than 2 point in common..." << std::
		   endl);
	  l_lesscount++;
	}
	else {
	  // Core computation.
	  FL l_dist = 1;
	  {
	    FL l_va1[3];
	    FL l_va2[3];
	    l_va1[0] =
	      vertex[dim * i + 0] - vertex[dim * l_cotanpoint[0] +
					   0];
	    l_va1[1] =
	      vertex[dim * i + 1] - vertex[dim * l_cotanpoint[0] +
					   1];
	    l_va1[2] =
	      vertex[dim * i + 2] - vertex[dim * l_cotanpoint[0] +
					   2];
		
	    l_va2[0] =
	      vertex[dim * pi + 0] - vertex[dim * l_cotanpoint[0] +
					    0];
	    l_va2[1] =
	      vertex[dim * pi + 1] - vertex[dim * l_cotanpoint[0] +
					    1];
	    l_va2[2] =
	      vertex[dim * pi + 2] - vertex[dim * l_cotanpoint[0] +
					    2];
		    
	    FL l_nva1 =
	      sqrt(l_va1[0] * l_va1[0] + l_va1[1] * l_va1[1] +
		   l_va1[2] * l_va1[2]);
	    FL l_nva2 =
	      sqrt(l_va2[0] * l_va2[0] + l_va2[1] * l_va2[1] +
		   l_va2[2] * l_va2[2]);
		    
	    FL l_cosA =
	      l_va1[0] * l_va2[0] + l_va1[1] * l_va2[1] +
	      l_va1[2] * l_va2[2];
	    l_cosA = l_cosA / (l_nva1 * l_nva2);
		    
	    FL l_sinA = sqrt(1 - l_cosA * l_cosA);
	    FL l_cotanA = l_cosA / l_sinA;
		    
	    FL l_vb1[3];
	    FL l_vb2[3];
	    l_vb1[0] =
	      vertex[dim * i + 0] - vertex[dim * l_cotanpoint[1] +
					   0];
	    l_vb1[1] =
	      vertex[dim * i + 1] - vertex[dim * l_cotanpoint[1] +
					   1];
	    l_vb1[2] =
	      vertex[dim * i + 2] - vertex[dim * l_cotanpoint[1] +
					   2];
		    
	    l_vb2[0] =
	      vertex[dim * pi + 0] - vertex[dim * l_cotanpoint[1] +
					    0];
	    l_vb2[1] =
	      vertex[dim * pi + 1] - vertex[dim * l_cotanpoint[1] +
					    1];
	    l_vb2[2] =
	      vertex[dim * pi + 2] - vertex[dim * l_cotanpoint[1] +
					    2];
		    
	    FL l_nvb1 =
	      sqrt(l_vb1[0] * l_vb1[0] + l_vb1[1] * l_vb1[1] +
		   l_vb1[2] * l_vb1[2]);
	    FL l_nvb2 =
	      sqrt(l_vb2[0] * l_vb2[0] + l_vb2[1] * l_vb2[1] +
		   l_vb2[2] * l_vb2[2]);
		    
		    
	    FL l_cosB =
	      l_vb1[0] * l_vb2[0] + l_vb1[1] * l_vb2[1] +
	      l_vb1[2] * l_vb2[2];
	    l_cosB = l_cosB / (l_nvb1 * l_nvb2);
		    
	    FL l_sinB = sqrt(1 - l_cosB * l_cosB);
		    
	    FL l_cotanB = l_cosB / l_sinB;
		    
		    
		    
	    l_dist = 0.5 * (l_cotanA + l_cotanB) ;
		    
	  }
		
	  FL normalize = 1 ;
	  if (approx_voronoi_area)
	    normalize =  2.0 / (approx_voronoi_area[i] + approx_voronoi_area[pi]) ;
		


	  it->second.dist = l_dist * normalize ;
	  it->second.depth = 1 ;
	  hash_map< int, MapMesh >::iterator it = hash_map_vector[pi].find(i) ;
	    if (it != hash_map_vector[pi].end()) {
	      hash_map_vector[pi][i].dist = l_dist * normalize ;
	      hash_map_vector[pi][i].prev_dist = l_dist * normalize ;
	      hash_map_vector[pi][i].depth = 1 ;
	    }

	}
      }
    }			// for j (mesh_nei_count)
#ifdef DEBUG_3
    std::cout << std::endl ;
#endif
  }				// for i (nvertex)

  DBG_(cout << "Total number of point more then 2 comon nei : " <<
       l_morecount << " and less : " << l_lesscount << std::endl);
}


void Mesh::computeNormalizationWeight()
{

  if (approx_voronoi_area) {
    delete[]approx_voronoi_area;
    approx_voronoi_area = NULL;
  }

  approx_voronoi_area = new FL[nvertex];
  bzero(approx_voronoi_area, nvertex * sizeof(FL));
  int l_max_num_facets_per_vertex = 0 ;
  HASH_MAP_PARAM_(int, "max_num_facets_per_vertex", Int, l_max_num_facets_per_vertex ) ;
    
    double l_area = 0;


  FL l_a1x = 0, l_a1y = 0, l_a1z = 0;
  FL l_a2x = 0, l_a2y = 0, l_a2z = 0;
  FL l_ax = 0, l_ay = 0, l_az = 0;

  int l = 0;
  for (int i_vertex = 0; i_vertex < nvertex; ++i_vertex) {
    l_area = 0;
    for (int i = 0; i < nfacets_per_vertex[i_vertex]; ++i) {
      int ind_facet =
	vertex_to_facets[i_vertex * l_max_num_facets_per_vertex + i];
      l = 0;

      for (int j = 0; j < nvert_facet[ind_facet]; ++j) {

	int ind_vert =
	  facets[ind_facet * max_num_vert_per_facets + j];


	if (ind_vert != i_vertex) {
	  if (l) {
	    l_a1x =
	      vertex[dim * ind_vert + 0] -
	      vertex[dim * i_vertex + 0];
	    l_a1y =
	      vertex[dim * ind_vert + 1] -
	      vertex[dim * i_vertex + 1];
	    l_a1z =
	      vertex[dim * ind_vert + 2] -
	      vertex[dim * i_vertex + 2];
	  } else {
	    l_a2x =
	      vertex[dim * ind_vert + 0] -
	      vertex[dim * i_vertex + 0];
	    l_a2y =
	      vertex[dim * ind_vert + 1] -
	      vertex[dim * i_vertex + 1];
	    l_a2z =
	      vertex[dim * ind_vert + 2] -
	      vertex[dim * i_vertex + 2];
	  }
	  ++l;
	}
      }

      l_ax = l_a1y * l_a2z - l_a1z * l_a2y;
      l_ay = l_a1z * l_a2x - l_a1x * l_a2z;
      l_az = l_a1x * l_a2y - l_a1y * l_a2x;

      l_area += sqrt(l_ax * l_ax + l_ay * l_ay + l_az * l_az) / 2;


    }
    if (approx_voronoi_area)
      approx_voronoi_area[i_vertex] = l_area ;
  }

}


FL *Mesh::getVoronoiAreas()
{

  return approx_voronoi_area;

}


void Mesh::computeNormalizedEuclidianWeight(bool p_normalize)
{


  FL *l_t_approx_voronoi_area = approx_voronoi_area;
#ifdef DEBUG_3
  std::cout << "Distance Computation: " <<std::endl ;
#endif
  for (int i = 0; i < nvertex; ++i) {
      
    for (hash_map< int, MapMesh >::iterator it = hash_map_vector[i].begin() ; it != hash_map_vector[i].end() ; ++it ) {
      // hash_map is made of pairs.
      // it->first returns the key, 
      // it->second return the data
	
      int pi = it->first ;
	
	
      FL va1[3];
      FL va2[3];
      va1[0] = vertex[dim * i + 0];
      va1[1] = vertex[dim * i + 1];
      va1[2] = vertex[dim * i + 2];
	
      va2[0] = vertex[dim * pi + 0];
      va2[1] = vertex[dim * pi + 1];
      va2[2] = vertex[dim * pi + 2];
      FL a = va1[0] - va2[0];
      FL b = va1[1] - va2[1];
      FL c = va1[2] - va2[2];
	
	
      FL dist = sqrt(a * a + b * b + c * c);
#ifdef DEBUG_3
      std::cout<< i << " "<< pi << " (" << va1[0] << " " << va1[1] << " " << va1[2] << ") " << " (" << va2[0] << " " << va2[1] << " " << va2[2] << ") dist=" << dist <<std::endl ;
#endif
      if (p_normalize && approx_voronoi_area)
	dist = dist * 2.0 / (approx_voronoi_area [i] + approx_voronoi_area [pi]);
	
	
      it->second.dist = dist ;
      it->second.prev_dist = dist ;
      it->second.depth = 1 ;
		  
    }
      
    if (p_normalize && approx_voronoi_area)
      ++l_t_approx_voronoi_area;
      
  }
    
}





int Mesh::fillFacetNormals()
{
  /*TODO: overwriting normal for repeating nodes. Average is needed, except for sharp changing
    surfaces as a cube where this method is correct, probably leave both just in case
    calculate n1+n2+n3+n4 and then normalize it. (You can get a better
    average if you weight the normals by the size of the angles at the shared intersection.)
  */

  if (facet_normal) {
    delete[]facet_normal;
    facet_normal = NULL;
  }
  facet_normal = new double[dim * nfacets];
  memset(facet_normal, 0, dim * nfacets * sizeof(double));


  for (int i = 0; i < nfacets; i++) {
    if (nvert_facet[i] < 2) {
      PRINT_(fprintf
	     (stderr, "Face %d has less than 3 vertices\n", i));
      continue;
    }

    computeOneNormal(facets[i * max_num_vert_per_facets + 0],
		     facets[i * max_num_vert_per_facets + 1],
		     facets[i * max_num_vert_per_facets + 2],
		     (facet_normal + i * dim));

  }

  return 0;
}


int Mesh::computeOneNormal(int p_v0, int p_v1, int p_v2, double *p_normal)
{
  double a[3];
  double b[3];

  // calculate the vectors A and B
  // note that v[3] is defined with counterclockwise winding in mind
  for (int i = 0; i < 3; i++) {
    a[i] =
      vertex[p_v0 * dim + i] -
      vertex[p_v1 * dim + i];
    b[i] =
      vertex[p_v1 * dim + i] -
      vertex[p_v2 * dim + i];
  }

  // calculate the cross product and place the resulting vector
  // into the address specified in parameters
  p_normal[0] = (double) ((a[1] * b[2]) - (a[2] * b[1]));
  p_normal[1] = (double) ((a[2] * b[0]) - (a[0] * b[2]));
  p_normal[2] = (double) ((a[0] * b[1]) - (a[1] * b[0]));


  // normalize
  float l_len =
    (double) sqrt((p_normal[0] * p_normal[0]) +
		  (p_normal[1] * p_normal[1]) +
		  (p_normal[2] * p_normal[2]));
  if (l_len == 0.0) {
    l_len = 1.0f;
  }

  for (int i = 0; i < 3; i++)
    p_normal[i] /= l_len;

  return 0;
}







void Mesh::buildDistMat(int p_max_depth)
{

  PRINT_(std::cout << "Start DISTMAT" << std::endl);

  if (total_num_nei) {
    delete[]total_num_nei;
    total_num_nei = NULL;
  }

  total_num_nei = new int[nvertex];

  int l_depth = 1;

  double l_length = 0 ;

  for (int i = 0 ; i < p_max_depth ; ++i ) {
    l_length += 6 * pow(2,i) ;
  }

  int l_ilength =  0 ;
  if (p_max_depth == 0 )
    HASH_MAP_PARAM_(int, "max_num_of_nei", Int, l_ilength ) ;
  else 
    l_ilength = (int) l_length ;
  if (l_ilength > 1900000000) {
    FATAL_(fprintf
	   (stderr,
	    "MaxDepth is too high to build the distance matrix.\n"
	    "The maximum number of element to be stored is too high with max_depth = %d \n"
	    "Please consider reducing max_depth, or make sure you have less than %d neighbors per vertex \n"
	    "In the latter case, set MAX_NUM_OF_NEI to the correct value in Constant.h \n",
	    p_max_depth, l_ilength), FATAL_MEMORY_ALLOCATION);
  }

  bool *l_nei_done = new bool[nvertex];
  bzero(l_nei_done, nvertex * sizeof(bool));


  int curr_ind = 0;



#ifdef DEBUG_SPREAD
  std::cout << "SPREAD STEP " <<std::endl ;
#endif
  // SPREAD FUNCTION
  //Create a queue in which we store the current map. 
  
  std::vector< hash_map< int, MapMesh > > l_hash_map_vector ;
  l_hash_map_vector = hash_map_vector ;
  
  deque <std::pair<int, MapMesh> > l_Q ;
  
  for (int i = 0; i < nvertex; ++i) {
    
    l_depth = 1;
    
    memset(l_nei_done, 0, nvertex * sizeof(bool) ) ;
    
    while (l_depth < p_max_depth) {
      l_depth++;

      l_Q.clear() ;
      
      // Copy the content of the map.
      for (hash_map< int, MapMesh >::iterator it = hash_map_vector[i].begin() ; it != hash_map_vector[i].end() ; ++it ) {
	l_Q.push_front(std::make_pair(it->first, it->second)) ;
      }
      
      

#ifdef DEBUG_SPREAD
      std::cout <<  "STARTING RING NUMBER "<< l_depth << " for vertex " << i << std::endl ;
      if ( i == 1167) {
	
	fflush(stdout) ;
	fflush(stdin) ;
	getchar () ;
      }

#endif
      std::pair<int, MapMesh> p ;
      //      for (int j = 0; j < total_num_nei[i]; ++j) {
      deque< std::pair<int, MapMesh> >::iterator it = l_Q.begin() ; 
      while ( it != l_Q.end() ) {
	
	//	curr_ind = l_nei_k[j];	// current neighbor of j
	curr_ind = it->first ;
	if (!l_nei_done[curr_ind]) {
	  for (hash_map< int, MapMesh >::iterator it = l_hash_map_vector[curr_ind].begin() ; it != l_hash_map_vector[i].end() ; ++it ) {
	
	    if (it->first != i && hash_map_vector[i].find(it->first) == hash_map_vector[i].end() ) {
	      p.first = it->first ;
	      
	      if (!use_depth_as_dist) {
		p.second.prev_dist = p.second.dist ;
		p.second.dist += it->second.dist ;
		hash_map_vector[i].insert(p) ;
	      }
	      else {
		p.second.depth = l_depth ;
		hash_map_vector[i].insert(p) ;
	      }
	    } // if (it->first != i && ....
	    else if ( !use_depth_as_dist && it->first != i ) {

	      FL l_dist = hash_map_vector[i][it->first].prev_dist + it->second.dist ;
	      if (l_dist <  hash_map_vector[i][it->first].dist ) {
		
		hash_map_vector[i][it->first].dist = l_dist ;
		hash_map_vector[i][it->first].depth = l_depth ;
	      } // if (l_dist <  hash_map_vector[i][it->first].dist
	      
	    } // else if ( !use_depth_as_dist && it->first != i )
	    
	  } // for (hash_map< int, MapMesh >::iterator it = l_hash_map_vector[curr_ind] ....
	  
	  l_nei_done[curr_ind] = true;

	} // if (!l_nei_done[curr_ind])  ...
	
	++it ;
      } // while ( !l_Q.empty() )
      

      // Update the previous computed distance to the current one for the next iteration step in depth and distance computation.
      if (!use_depth_as_dist) {
	it = l_Q.begin() ; 
	while ( it != l_Q.end() ) {
	  curr_ind = it->first ;	// current neighbor of i
	  for (hash_map< int, MapMesh >::iterator it_1 = l_hash_map_vector[curr_ind].begin() ; it_1 != l_hash_map_vector[i].end() ; ++it_1 ) {

	    if ( hash_map_vector[i].find(it_1->first) !=  hash_map_vector[i].end() ) {
	      hash_map_vector[i][it_1->first].prev_dist = hash_map_vector[i][it_1->first].dist ;
	    }
	  }// for (hash_map< int, MapMesh >::iterator it_1
	} // while ( it != l_Q.end() )
      } // if (!use_depth_as_dist)



    } // end of while on depth.
  } // end of for on nvertex

  for (int i = 0; i < nvertex; ++i) {
    nzelems += hash_map_vector[i].size () ;
  }
  
  PRINT_(std::cout << "Total number of NEI : " << nzelems << std::
	 endl);

  l_hash_map_vector.clear () ;
  l_Q.clear () ;

  delete [] l_nei_done;


  fillSparseWeight();



  PRINT_(std::cout << "End DISTMAT" << std::endl);

}



int Mesh::fillSparseWeight()
{

  if (weights) {
    delete[]weights;
    weights = NULL;
  }
  if (mat_weights) {
    delete[]mat_weights;
    mat_weights = NULL;
  }

  weights = new FL[3 * nzelems];

  memset(weights, 0, 3 * nzelems * sizeof(FL));

  mat_weights = new FL *[nvertex];
  if (vec_depth) {
    delete [] vec_depth;
    vec_depth = NULL;
  }

  if (mat_depth) {
    delete[]mat_depth;
    mat_depth = NULL;
  }

  vec_depth = new int[3 * nzelems];

  memset(vec_depth, 0, 3 * nzelems * sizeof(int));

  mat_depth = new int *[nvertex]; 

  int l_total_count = 0;
  int l_count = 0;

  // Filling Matweights.
  mat_weights[0] = weights ;
  mat_depth[0] = vec_depth ;
  int l_cumu_count = 0;
#ifdef DEBUG_FULL
  std::cout << "Weight mat:" <<std::endl ;
#endif
  for (int i = 0; i < nvertex; ++i) {
    l_count = 0;
    hash_map< int, MapMesh >::iterator it = hash_map_vector[i].begin() ;

    while ( it != hash_map_vector[i].end() ) {
      weights[l_total_count * 3] = (FL) i ;
      weights[l_total_count * 3 + 1] = (FL) it->first ;
      vec_depth[l_total_count * 3] = i;
      vec_depth[l_total_count * 3 + 1] =  it->first ;


      if (!use_depth_as_dist) {
	weights[l_total_count * 3 + 2] = it->second.dist ;
      }
      else {
	weights[l_total_count * 3 + 2] = it->second.depth ;
      }    
      vec_depth[l_total_count * 3 + 2] = it->second.depth ;


#ifdef DEBUG_FULL
      std::cout << weights[l_total_count * 3] << " " << weights[l_total_count * 3 + 1] << " " << weights[l_total_count * 3 + 2] <<std::endl ;
#endif
      ++it ;
      l_total_count++;
      l_count++;
    }
    l_cumu_count += 3 * l_count;

    if (i < nvertex - 1) {
      mat_weights[i + 1] = weights + l_cumu_count;
      mat_depth[i + 1] = vec_depth + l_cumu_count;
    }


  }

  if (l_total_count != nzelems) {
    FATAL_(fprintf
	   (stderr,
	    "There is an error in Fill Sparse Mesh Dist Matrix: %d != %d \n",
	    l_total_count, nzelems), FATAL_ERROR);
  }
  // Releasing memory




  sortMatWeight();




  //  if (!use_depth_as_dist) {
#if 0
    symmeterizeMatWeight();
#endif 
    checkSymmeterizeMatWeight();
#ifdef DEBUG_FULL
    fflush(stdout) ;
    fflush(stdin) ;
    getchar () ;
#endif
    //  }


  hash_map_vector.clear () ;


  return 0;

}




void Mesh::sortMatWeight()
{
  FL *l_sorted = NULL;



  for (int i = 0; i < nvertex; ++i) {

    if (l_sorted)
      delete[]l_sorted;
    l_sorted = new FL[3 * hash_map_vector[i].size()];

    memcpy(l_sorted, mat_weights[i],
	   3 * hash_map_vector[i].size() * sizeof(FL));
    // CompareFunc() is provided in utilities.h. 
    qsort(l_sorted, hash_map_vector[i].size(), 3 * sizeof(FL), compareFunc);

    memcpy(mat_weights[i], l_sorted,
	   3 * hash_map_vector[i].size() * sizeof(FL));

  }

  if (l_sorted)
    delete[]l_sorted;

}


void Mesh::symmeterizeMatWeight()
{

  PRINT_(fprintf(stderr, "Symeterize the Weight matrix\n"));



  int j = 0 ;
  int k = 0 ;

  for (int i = 0; i < nvertex; ++i) {
    hash_map< int, MapMesh >::iterator it = hash_map_vector[i].begin() ;    
    j = 0 ;
    while ( it != hash_map_vector[i].end() ) {
      k = 0 ;
      hash_map< int, MapMesh >::iterator it2 = hash_map_vector[it->first].begin() ;
      while ( it2 != hash_map_vector[it->first].end() ) {

	if (i ==
	    mat_weights[(int) mat_weights[i][3 * j + 1]][3 * k +
							 1]) {
	  mat_weights[i][3 * j + 2] +=
	    mat_weights[(int) mat_weights[i][3 * j + 1]][3 *
							 k +
							 2];
	  mat_weights[i][3 * j + 2] /= 2;
	  mat_weights[(int) mat_weights[i][3 * j + 1]][3 * k +
						       2] =
	    mat_weights[i][3 * j + 2];

	  break;

	}
	++k;
	++it2 ;
      }
      ++j;
      ++it ;
    }
  }
}




void Mesh::checkSymmeterizeMatWeight()
{
  FL l_error = 0;

  int k = 0 ;
  int l = 0 ;

  for (int i = 0; i < nvertex; ++i) {
    hash_map< int, MapMesh >::iterator it = hash_map_vector[i].begin() ;    
    k = 0 ;
    while ( it != hash_map_vector[i].end() ) {
      l = 0 ;
      hash_map< int, MapMesh >::iterator it2 = hash_map_vector[it->first].begin() ;
      while ( it2 != hash_map_vector[it->first].end() ) {
	
	if (i ==
	    mat_weights[(int) mat_weights[i][3 * k + 1]][3 * l +
								 1]) {
	  
	  FL a =
	    (mat_weights[i][3 * k + 2] -
	     mat_weights[(int) mat_weights[i][3 * k + 1]][3 *
								  l +
							  2]);
	  if (a !=0)
	    std::cout << "Not symetric on : " << i << " " << (int) mat_weights[i][3 * k + 1] << " "<< mat_weights[i][3 * k + 2]<< " "<<  mat_weights[(int) mat_weights[i][3 * k + 1]][3 * l + 2] <<std::endl ;
	  l_error += a * a;

	  break;
	}
	++l;
	++it2 ;
      }
      ++k;
      ++it ;
    }
  }


  for (int i = 0; i < nvertex; ++i) {
    hash_map< int, MapMesh >::iterator it = hash_map_vector[i].begin() ;    
    while ( it != hash_map_vector[i].end() ) {
      hash_map< int, MapMesh >::iterator it2 = hash_map_vector[it->first].find(i) ;
      if (it2 == hash_map_vector[it->first].end()) {
	FATAL_(std::cout << "Not symmetric on " << i << " " << it->first <<std::endl, FATAL_VARIABLE_INIT) ;
      }
      else if (it2->second.depth != it->second.depth) {
	FATAL_(std::cout << i << " " << it->first << " d not have the same depth" <<std::endl, FATAL_VARIABLE_INIT) ;
      }
      ++it ;
    }
  }
  PRINT_COLOR_(std::cout << "Error for symmeterize is : " << l_error << std::
	       endl, BLUE);
}



FL *Mesh::getFLSparseWeight()
{

  return weights;
}

FL **Mesh::getFLSparseMatWeight()
{

  return mat_weights;
}

int **Mesh::getSparseMatDepth()
{
  return mat_depth;
}

int Mesh::buildMeshsetPointList()
{
  int res = buildPointsetList();
  return res;
}

int Mesh::buildMeshsetList()
{
  int res = buildPointsetList();
  return res;
}

int Mesh::drawMeshsetPoint()
{
  int res = drawPointsetList();
  return res;
}

int Mesh::drawMeshsetList()
{
  int res = drawPointsetList();
  return res;
}

int Mesh::drawMeshsetFacets(bool drawEdges)
{

  if (loaded) {
    int ind = 0;
    for (int i = 0; i < nfacets; i++) {
      glBegin(GL_POLYGON);
      glNormal3f(facet_normal[i * dim], facet_normal[i * dim + 1],
		 facet_normal[i * dim + 2]);
      for (int j = 0; j < nvert_facet[i]; j++) {
	ind = facets[i * max_num_vert_per_facets + j];
	glColor4f(points_color[ind * color_depth],
		  points_color[ind * color_depth + 1],
		  points_color[ind * color_depth + 2], 0.5);
	glVertex3f((float) (vertex[ind * dim]) / scale,
		   (float) (vertex[ind * dim + 1]) / scale,
		   (float) (vertex[ind * dim + 2]) / scale);
      }
      glEnd();

	   

      if (drawEdges) {
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glLineWidth(3);
	glBegin(GL_POLYGON);
	glNormal3f(facet_normal[i * dim], facet_normal[i * dim + 1],
		   facet_normal[i * dim + 2]);
	for (int j = 0; j < nvert_facet[i]; j++) {
	  ind = facets[i * max_num_vert_per_facets + j];
	  glColor4f(0.0f, 0.0f, 0.0f, 0);
	  glVertex3f((float) (vertex[ind * dim]) / scale,
		     (float) (vertex[ind * dim + 1]) / scale,
		     (float) (vertex[ind * dim + 2]) / scale);
	}
	glEnd();
      }
      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }
  }
  return (0);

}


void Mesh::buildNeiColormap(int p_depth)
{

  if (NEI_colormap) {
    delete[]NEI_colormap;
    NEI_colormap = NULL;
  }

  NEI_colormap = new float[color_depth * p_depth];

  DBG2_(fprintf
	(stderr, "Building NEI_colormap with %d colors\n", p_depth));

  float l_r, l_g, l_b, l_deltar, l_deltag, l_deltab, l_minr, l_ming,
    l_minb;
  l_deltar = 0.8f;
  l_deltag = 0.8f;
  l_deltab = 0.8f;
  l_minr = 0.1f;
  l_ming = 0.1f;
  l_minb = 0.1f;

  int ind;
  float l_colval = 0.0f;

  int l_ncolor = p_depth + 1;

  for (int i = 0; i < p_depth; i++) {

    ind = i;
    if (ind < 0) {		//outlier 
      l_r = l_g = l_b = 0.5;
    } else if (ind < (int) round((float) (l_ncolor) / 3.0f)) {
      l_colval = 3.0f * (float) ind / (float) (l_ncolor);
      l_r = l_minr;
      l_g = l_deltag * l_colval + l_ming;
      l_b = l_deltab + l_minb;
    } else if (ind < (int) round(2.0f * (float) (l_ncolor) / 3.0f)) {
      ind = ind - (int) round((float) (l_ncolor) / 3.0f);
      l_colval = 3.0f * (float) ind / (float) (l_ncolor);
      l_r = l_deltar * l_colval + l_minr;
      l_g = l_deltag + l_ming;
      l_b = l_deltab * (1.0f - l_colval) + l_minb;
    } else {
      ind = ind - (int) round(2.0f * (float) (l_ncolor) / 3.0f);
      l_colval = 3.0f * (float) ind / (float) (l_ncolor);
      l_r = l_deltar + l_minr;
      l_g = l_deltag * (1.0f - l_colval) + l_ming;
      l_b = l_minb;


    }

    NEI_colormap[i * color_depth] = l_r;
    NEI_colormap[i * color_depth + 1] = l_g;
    NEI_colormap[i * color_depth + 2] = l_b;
    if (color_depth == 4)
      NEI_colormap[i * color_depth + 3] = 1;
    DBG3_(fprintf
	  (stderr, "color %d : %g %g %g\n", ind,
	   NEI_colormap[i * color_depth],
	   NEI_colormap[i * color_depth + 1],
	   NEI_colormap[i * color_depth + 2]));

  }


}




int Mesh::buildMeshsetNeiList()
{
  //    double scale = 1 ;

  float l_x, l_y, l_z;
  int l_nei_draw_sample_rate ;
  HASH_MAP_PARAM_(int, "nei_draw_sample_rate", Int, l_nei_draw_sample_rate) ;
  if (mat_weights == NULL)
    return 0;
  if (loaded) {
    if (MESHNEISET != (uint) (-1)) {
      PRINT_(fprintf
	     (stderr,
	      "Destroying previously created list. Be sure this is not done inside a glBegin-glEnd cycle\n"));
      glDeleteLists(MESHNEISET, 1);
      MESHNEISET = (uint) (-1);
    }
    MESHNEISET = glGenLists(1);

    if (glIsList(MESHNEISET)) {
      glNewList(MESHNEISET, GL_COMPILE);
      for (int i = 0; i < nvertex; i +=  l_nei_draw_sample_rate) {

	l_x = (float) (vertex[i * dim] / scale);
	l_y = (float) (vertex[i * dim + 1] / scale);
	l_z = (float) (vertex[i * dim + 2] / scale);
	// Drawing the current point
	GLUquadric *quad = gluNewQuadric();
	glPushMatrix();
	glTranslated(l_x, l_y, l_z);
	glColor3f(1, 0, 0);
	gluSphere(quad, 0.002, 10, 10);
	glPopMatrix();
	// Drawing the neighbors of the current point
	for (int j = 0; j < total_num_nei[i]; ++j) {
	  int ind = int (mat_depth[i][3 * j + 1]);
	  int color_index = int (mat_depth[i][3 * j + 2]);
	  if (NULL || NEI_colormap) {
	    glColor3f(NEI_colormap[color_index],
		      NEI_colormap[color_index + 1],
		      NEI_colormap[color_index + 2]);
	  } else {

	    PRINT_(fprintf
		   (stderr,
		    "The NEI colormap for the mesh is not set. Using hardcoded colors. \n"));
	    if (mat_weights[i][3 * j + 2] == 1)
	      glColor3f(1, 0, 0);

	    if (mat_weights[i][3 * j + 2] == 2)
	      glColor3f(0, 1, 0);

	    if (mat_weights[i][3 * j + 2] == 3)
	      glColor3f(0, 0, 1);

	    if (mat_weights[i][3 * j + 2] == 4)
	      glColor3f(1, 1, 0);

	    if (mat_weights[i][3 * j + 2] == 5)
	      glColor3f(1, 0, 1);
	  }

	  l_x = (float) (vertex[ind * dim] / scale);
	  l_y = (float) (vertex[ind * dim + 1] / scale);
	  l_z = (float) (vertex[ind * dim + 2] / scale);

	  GLUquadric *quad = gluNewQuadric();
	  glPushMatrix();
	  glTranslated(l_x, l_y, l_z);
	  gluSphere(quad, 0.002, 10, 10);
	  glPopMatrix();

	}

      }

      glEndList();
    }
  }

  return 0;

}


int Mesh::drawMeshsetNei()
{

  if (!loaded)
    return 1;
  if (MESHNEISET == (uint) (-1))
    buildMeshsetNeiList();
  glCallList(MESHNEISET);


  return (0);

}


FL *Mesh::getVertices()
{
  return getPoints();
}


int Mesh::getNumOfVertex()
{
  return getNumOfPoints();
}

int *Mesh::getNumOfNeik()
{
  return total_num_nei;
}





void Mesh::sortMeshY () {

  std::cout << "SORTING MESHES IS CALLED" <<std::endl ;
  FL *l_sort = new FL[ (dim+1) * nvertex ] ;

  revert_index_sort_mesh = new int[nvertex] ;

  // Sorting points
  FL *l_t_sort = l_sort ;
  FL *l_t_vert = vertex ;
  for (int  i = 0 ; i < nvertex ; ++i) {
    *l_t_sort = (FL)i ;
    memcpy((l_t_sort + 1), l_t_vert, dim * sizeof(FL)) ;
    l_t_sort += (dim +1) ;
    l_t_vert += dim ;
  }

  qsort (l_sort, nvertex, (dim +1) * sizeof(FL), compareFuncSortVertexY) ;

    


  l_t_sort = l_sort ;
  l_t_vert = vertex ;
  for (int  i = 0 ; i < nvertex ; ++i) {
    memcpy( l_t_vert, (l_t_sort + 1), dim * sizeof(FL)) ;
    revert_index_sort_mesh[(int)(*l_t_sort )] = i ;
    l_t_sort += (dim +1) ;
    l_t_vert += dim ;
  }

  // According to the sort, reoganize facets
  for ( int i = 0 ; i < nfacets ; ++i ) {
    for (int k = 0 ; k < max_num_vert_per_facets ; ++k) {
      facets[i * max_num_vert_per_facets + k] = revert_index_sort_mesh [ facets [i * max_num_vert_per_facets + k] ] ;
    }
  }
  int l_max_num_facets_per_vertex = 0 ;
  HASH_MAP_PARAM_(int, "max_num_vert_per_facets", Int, l_max_num_facets_per_vertex ) ;
    int *l_f_sort = new int [ l_max_num_facets_per_vertex * nvertex] ;
  for (int  i = 0 ; i < nvertex ; ++i) {
    memcpy((l_f_sort + i * l_max_num_facets_per_vertex ), 
	   (vertex_to_facets + (int)revert_index_sort_mesh [ i ] * l_max_num_facets_per_vertex) , 
	   l_max_num_facets_per_vertex * sizeof(int)) ;

  }
  delete [] vertex_to_facets ;

  vertex_to_facets = l_f_sort ;
   
  delete [] l_sort ;
}


int *Mesh::getIndexSorting ( ) {
  return revert_index_sort_mesh ;
    
}



void Mesh::enableSortMesh(bool on) {
  enable_sort_mesh = on ;
}

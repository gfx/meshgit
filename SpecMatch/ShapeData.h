/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
/*
 *  ShapeData.h
 *  
 *
 *  Created by Diana Mateus and David Knossow on 10/5/08.
 *  Copyright 2007 INRIA. All rights reserved.
 *
 * This class provides structures to store either voxels or 
 * vertices from meshes. It allows for a common structure for
 * both meshes and voxels.
 *
 */



#ifndef SHAPEDATA_H
#define SHAPEDATA_H
#include <QGLViewer/qglviewer.h>
#ifndef MACOSX
#include <GL/glut.h>
#endif

#include "Constants.h"
#include "utilities.h"
#include "types.h"
#include "drawprim.h"


class ShapeData {

  protected:
    // Store temporary filenames to be read or saved.
    char *filename;

    // Space dimension in which points are described (3 for voxels or mesh vertices).
    int dim;

    // Store 3D coordinates either of voxels or of mesh vertices.
    FL *points;

    // Number of voxels or mesh vertices 
    int npoints;


    //Used to display inside the unit ball
    FL scale;

    // States if data had been loaded or not.
    bool loaded;

    // Used for coinvenience. Store COLOR_DEPTH.
    int color_depth;

    //OpenGL list. Coordinates of points to be displayed.
    GLuint POINTSET;

    // Used to display functions as color using colormap
    FL *function_value;		//matrix of (nvoxel x n_function)

    // Number of functions.
    int n_functions;

    // Stores the min of each eigenfunction
    FL *func_min;

    // Stores the max of each eigenfunction
    FL *func_max;


    // Vector field
    FL *vector_field;
    GLuint VECTORFIELD;

    //colormap
    //Number of colors in the colormap
    int ncolor;

    // Contains the colors (r,g,b). Size is 3 * ncolors
    float *colormap;

    // Color assigned for to each voxel
    float *points_color;

    // Store the CUBE_SIZE. Allows to rebuilt the point sets if changes have happened in the cube size.
    float g_cube_size;

    //graph    
    int *connect;		//list of edges (nzelemsx2)

    //colorbar
    int min_color;		//color index of min of current function 
    int max_color;		//color index of min of current function 
    int zero_color;		//color index of min of current function 


    //mask
    int *mask;			//used for outliers, automatically detected 

    //when all the dimensions of a multidimensional 
    //function are zero for a given voxel       
    void createMask();		//fill mask

    int initFunctionValue(int in_nfunc);


  public:
     ShapeData();
    ~ShapeData();

    void init () ;

    // Number of edges
    int nzelems;

    // Release all created arrays
    int releaseData();

    // Obvious.
    bool isLoaded();

    // Store the functions values from a pre-allocated array (either FL or int format).
    int loadFunctionValue(FL * values, int nfunc = 1);
    int loadFunctionValue(int *values, int in_nfunc = 1);

    // Store the mapping between to 3D shapes from the distance matrix (it fills connect). 
    int loadGraph(int in_nzelems, FL * distmat, int offset = 0);
    int loadGraph(int in_nzelems, int *distmat, int offset = 0);

    
    // Returns the outlier mask.
    int *getMask();

    // Retrun the coordinates of voxels or mesh vertices.
    FL *getPoints();

    // Get the number of voxels or meshvertices.
    int getNumOfPoints();

    // Get the dimension in which the points are described (default is 3).
    void setDim (int p_dim = 3) ;
    
    // Get the dimension in which the points are described (generaly 3).
    int getDim();

    // Get the number of edges in the graph (non zero elements in the distance matrix).
    int getNzelems();

    // Get the scale of the shape. Use for display purposes.
    FL getScale();

    // Manually set the scale.
    int setScale(FL s);

    // Returns the number of functions loaded,
    int getNumOfFunctions();

    // Returns the Number of colors contained in the colormap.
    int getNumOfColor();

    // Set points_color to a blue value.
    int resetPointColor();

    // Set all points at the same color.
    int setAllPointsColor(float r, float g, float b);

    // Set point given by ind at color (r,g,b).
    int setPointColor(int ind, float *r, float *g, float *b);

    // Return point color given by ind.
    int getPointColor(int ind, float *r, float *g, float *b);

    // returns a pointer on points_color.
    float *getColorVector();

    // returns a pointer on colormap.
    float *getColormap();

    // returns a pointer on function_value.
    FL *getFunctionValue();

    // returns a pointer on vectorfield.
    FL *getVectorfield();

    // Create the colormap that best suits the given number of colors.
    int buildColormap(int ncolor, int *colorind = NULL);

    // Create the gray level colormap that best suits the given number of colors.
    int buildGrayColormap(int in_ncolor, int *colorind = NULL);

    // Fills points_color with a colormap built using ncolor = npoints
    int fillColorWithColormap();

    // Fills points_color with a colormap built using the function values.
    int fillColorWithFunctionValue(int f = 0);

    // Set the color vector from a given array
    void setColorVector(float *v);

    //vector field list
    int buildVectorfieldList();
    int drawVectorfieldList();

    //for colorbar
    int getMinCol();
    int getMaxCol();
    int getZeroCol();
    FL getFuncMin(int f);
    FL getFuncMax(int f);


    // Draw the POINTSET list
    int drawPointsetList();

    // Creates an OpenGL list of points (or cubes) to be displayed.
    int buildPointsetList(float cube_size = -1);

    // set the color_depth.
    int setColordepth(int depth = 3);

    // returns the color_depth 
    int getColorDepth();

    int str_length ;

};
#endif

#ifndef CIRCULATOR_H_DEFINED
#define CIRCULATOR_H_DEFINED

#include <cassert>

template <class Iter>
class Circulator
{
	protected :
	typedef Circulator<Iter>	Circ;
	Iter mHere;
	Iter mBegin;
	Iter mEnd;


	public :
	typedef typename Iter::value_type	value_type;
	typedef typename Iter::reference	reference;

	inline Circulator(Iter begin, Iter end):mHere(begin), mBegin(begin), mEnd(end){
		assert(begin != end);
	}

	inline Circulator(const Circ& B):mHere(B.mHere), mBegin(B.mBegin), mEnd(B.mEnd){}

	inline Circ& operator = (const Iter& a){
		mHere = a; 
		return *this;
	}

	inline bool operator != (const Iter& a) const {
		return mHere != a;
	}

	inline bool operator == (const Circ& B) const {
		return mHere == B.mHere;
	}

	inline bool operator != (const Circ& B) const {
		return mHere != B.mHere;
	}

	inline reference	operator *(){
		return *mHere;
	}

	inline Circ& operator ++() {
		++mHere; 
		if(mHere == mEnd) mHere = mBegin; 
		return *this;
	}
	inline Circ& operator --() {
		if(mHere == mBegin) mHere = mEnd; 
		--mHere;
		return *this;
	}
	inline const Circ operator ++(int) {
		Circ copy(*this);
		++mHere; 
		if(mHere == mEnd) mHere = mBegin; 
		return copy;
	}
	inline const Circ operator --(int) {
		Circ copy(*this);
		if(mHere == mBegin) mHere = mEnd; 
		--mHere;
		return copy;
	}

};


#endif

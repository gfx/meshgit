/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
/*
 *  shape.cpp
 *  match
 *
 *  Created by Diana Mateus and David Knossow on 7/30/07.
 *  Copyright 2007 INRIA. All rights reserved.
 *
 */

#include "shape.h"


using namespace std;


Shape::Shape()
{

    //Data
    C_Voxel1 = NULL;
    C_Voxel2 = NULL;

    C_Mesh1 = NULL;
    C_Mesh2 = NULL;

    C_Mesh3 = NULL;
    C_Mesh4 = NULL;

    C_Embed1 = NULL;
    C_Embed2 = NULL;

    
    //Match
    binmatch_filename = NULL;
    match_id = new int[2];
    match_id[0] = 0;
    match_id[1] = 1;
    match = NULL;
    proba_matrix = NULL;
    proba_vec = NULL;

    //match sequence
    match_pattern = NULL;
    voxel_pattern = NULL;
    mesh_pattern = NULL;
    embed_pattern = NULL;
        match_it = -1;
    match_first_it = -1;
    match_last_it = -1;
    match_delta = 1;

    match_saved = false;

    //Transfo
    transfo_filename = NULL;
    init_transfo_filename = NULL;
    transfo = NULL;
    init_transfo = NULL;
    dimx = -1;
    dimy = 0;



    coords_transfo_1 = NULL;
    coords_transfo_2 = NULL;
    coords_transfo_2_t_dim_sel = NULL;
    mat_init_transfo_dim = 0;
    mat_transfo_dim = 0;


    enable_reshape_binary_match = false ;

    allocate3DShape();
    allocateEmbedding();




    apply_init_transfo_once = true;
}





Shape::~Shape()
{

    releaseAll();

}





void Shape::releaseAll()
{
    if (transfo_filename) {
	delete[]transfo_filename;
	transfo_filename = NULL;
    }

    if (init_transfo_filename) {
	delete[]init_transfo_filename;
	init_transfo_filename = NULL;
    }

    if (C_Voxel1) {
	delete C_Voxel1;
	C_Voxel1 = NULL;
    }
    if (C_Voxel2) {
	delete C_Voxel2;
	C_Voxel2 = NULL;
    }
    if (C_Mesh1) {
	delete C_Mesh1;
	C_Mesh1 = NULL;
    }
    if (C_Mesh2) {
	delete C_Mesh2;
	C_Mesh2 = NULL;
    }
    if (C_Mesh3) {
	delete C_Mesh3;
	C_Mesh3 = NULL;
    }
    if (C_Mesh4) {
	delete C_Mesh4;
	C_Mesh4 = NULL;
    }
    if (C_Embed1) {
	delete C_Embed1;
	C_Embed1 = NULL;
    }
    if (C_Embed2) {
	delete C_Embed2;
	C_Embed2 = NULL;
    }
    
    if (match) {
	delete[]match;
	match = NULL;
    }
    if (match_id) {
	delete[]match_id;
	match_id = NULL;
    }
    if (proba_vec) {
	delete[]proba_vec;
	proba_vec = NULL;
    }
    if (proba_matrix) {
	delete[]proba_matrix;
	proba_matrix = NULL;
    }
    if (transfo) {
	delete[]transfo;
	transfo = NULL;
    }
    if (init_transfo) {
	delete[]init_transfo;
	init_transfo = NULL;
    }
    if (match_pattern) {
	delete[]match_pattern;
	match_pattern = NULL;
    }
    if (voxel_pattern) {
	free(voxel_pattern);
	voxel_pattern = NULL;
    }
    
    if (mesh_pattern) {
	free(mesh_pattern);
	mesh_pattern = NULL;
    }
    if (embed_pattern) {
	free(embed_pattern);
	embed_pattern = NULL;
    }
    if (coords_transfo_2)
	delete[]coords_transfo_2;
    if (coords_transfo_2_t_dim_sel)
	delete[]coords_transfo_2_t_dim_sel;
    if (coords_transfo_1)
	delete[]coords_transfo_1;

}


int Shape::readTransfo(const char *p_filename, int p_dimx, int p_dimy,
		       bool p_remove_first)
{


    if (p_remove_first) {
	WARNING_(std::
		 cout <<
		 "Check carefully, if the first element of the matrix really corrsponds to the constant eigenvector. In general this should be 1 on the element of the matrix."
		 << std::endl);
    }

    if (p_filename && transfo_filename) {
	delete[]transfo_filename;
	if (apply_init_transfo_once == false) {
	    delete[]init_transfo_filename;
	    init_transfo_filename = NULL;
	    delete[]init_transfo;
	    init_transfo = NULL;
	}
    }
    if (p_filename) {
	transfo_filename = new char[1024];

	strcpy(transfo_filename, p_filename);
	strcat(transfo_filename, ".T");
	if (init_transfo_filename == NULL) {
	    init_transfo_filename = new char[1024];
	    strcpy(init_transfo_filename, p_filename);
	    strcat(init_transfo_filename, ".initT");
	}
	dimx = p_dimx;
	dimy = p_dimy;
    }

    if (dimx != dimy) {
	ERROR_(fprintf
	       (stderr,
		"Transformation file should store a squared matrix\n"),
	       ERROR_VARIABLE_INIT);
    }


    PRINT_(fprintf
	   (stderr, "-->Reading init transformation file : %s\n",
	    init_transfo_filename));

    DBG_(fprintf(stderr, "with dimx %d dim %d\n", dimx, dimy));
    //Allocate memory for transfo       
    //Read transfo
    std::ifstream infile;
    int l_dim = 0;
    if (init_transfo == NULL) {

	if (init_transfo) {
	    delete[]init_transfo;
	    init_transfo = NULL;
	}
	infile.open(init_transfo_filename);
	if (infile.is_open() == 0) {
	    ERROR_(std::
		   cout << "File not found " << init_transfo_filename <<
		   std::endl, ERROR_OPEN_FILE);
	}


	istream_iterator < string > l_DataBegin(infile);
	istream_iterator < string > l_DataEnd;
	list < string > l_DataList(l_DataBegin, l_DataEnd);

	l_dim = (int) floor(sqrt((double) l_DataList.size()));
	double l_t_dim = sqrt((double) l_DataList.size());

	DBG_(std::cout << "Mat dim " << l_dim << std::endl);
	if (l_t_dim != l_dim) {
	    ERROR_(cout <<
		   "The init transfo file should be a squarred matrix" <<
		   std::endl, ERROR_VARIABLE_INIT);
	}

	init_transfo = new FL[(l_dim) * (l_dim)];

	list < string >::const_iterator l_it = l_DataList.begin();
	int l_count = 0;
	for (; l_it != l_DataList.end(); ++l_it) {
	    init_transfo[l_count] = atof(l_it->c_str());
	    ++l_count;
	}

#ifdef DEBUG
	std::cout << "Initial Transfo" << std::endl;
	for (int i = 0; i < l_dim; i++) {
	    for (int j = 0; j < l_dim; j++) {
		std::cout << init_transfo[i * l_dim + j] << " ";
	    }
	    std::cout << std::endl;
	}
	std::cout << std::endl;
#endif

	infile.close();

	if (init_transfo[l_dim * l_dim - 1] != 1) {
	    FATAL_(fprintf
		   (stderr,
		    "Last row and last line should be 0 except for last element that should be 1 \n"),
		   FATAL_OPEN_FILE);
	}

	if (((dimx + 1) != l_dim) && ((dimx + 2) != l_dim)) {
	    if (!p_remove_first && (l_dim) - dimx > 1) {
		FATAL_(fprintf
		       (stderr,
			"Number of selected eigenfunctions and dimension of initT file are not coherent \n"),
		       FATAL_VARIABLE_INIT);
	    } else if (p_remove_first && (l_dim) - dimx > 2) {
		FATAL_(fprintf
		       (stderr,
			"Number of selected eigenfunctions and dimension of initT file are not coherent \n"),
		       FATAL_BAD_OPTION);
	    } else if ((l_dim) - dimx <= 0) {
		FATAL_(fprintf
		       (stderr,
			"Number of selected eigenfunctions and dimension of initT file are not coherent \n"),
		       FATAL_VARIABLE_INIT);
	    }

	} else {
	    dimx = l_dim - 1;
	    dimy = l_dim - 1;
	}

	mat_init_transfo_dim = dimx;

    }


    if (transfo) {
	delete[]transfo;
	transfo = NULL;
    }

    infile.open(transfo_filename);
    if (infile.is_open() == 0) {

	WARNING_(std::
		 cout << "File " << transfo_filename << " not found" <<
		 std::endl);
	PRINT_(std::cout << "Copying init_T in T" << std::endl);
	transfo = new FL[(l_dim) * (l_dim)];
	memcpy(transfo, init_transfo, (l_dim) * (l_dim) * sizeof(FL));


#ifdef DEBUG
	std::cout << "Transfo" << std::endl;
	for (int i = 0; i < l_dim; i++) {
	    for (int j = 0; j < l_dim; j++) {
		std::cout << transfo[i * l_dim + j] << " ";
	    }
	    std::cout << std::endl;
	}
	std::cout << std::endl;
#endif
	return 0;
    } else {
	istream_iterator < string > l_DataBegin(infile);
	istream_iterator < string > l_DataEnd;
	list < string > l_DataList(l_DataBegin, l_DataEnd);

	int l_dim1 = (int) floor(sqrt((double) l_DataList.size()));
	double l_t_dim1 = sqrt((double) l_DataList.size());

	if (l_t_dim1 != l_dim1) {
	    ERROR_(cout << "The transfo file should be a squarred matrix"
		   << std::endl, ERROR_VARIABLE_INIT);
	}

	mat_transfo_dim = l_dim1 - 1;

	transfo = new FL[(l_dim1) * (l_dim1)];

	list < string >::const_iterator l_it = l_DataList.begin();

	int l_count = 0;

	for (; l_it != l_DataList.end(); ++l_it) {
	    transfo[l_count] = atof(l_it->c_str());
	    ++l_count;
	}

	infile.close();

	//#ifdef DEBUG
	std::cout << "Transfo" << std::endl;
	for (int i = 0; i < l_dim1; i++) {
	    for (int j = 0; j < l_dim1; j++) {
		std::cout << transfo[i * l_dim1 + j] << " ";
	    }
	    std::cout << std::endl;
	}
	std::cout << std::endl;
	//#endif
    }
    return 0;
}


int Shape::readBinaryMatch(const char *p_filename)
{

    if (p_filename && binmatch_filename) {
	free(binmatch_filename);
    }
    if (p_filename)
	binmatch_filename = strdup(p_filename);

    std::ifstream infile;

    infile.open(binmatch_filename);

    if (!infile.is_open()) {
	ERROR_(std::
	       cout << "File " << binmatch_filename << " not found" <<
	       std::endl, ERROR_OPEN_FILE);
    }

    DBG_(fprintf
	 (stderr, "-->Reading binary match file : %s\n",
	  binmatch_filename));

    int l_dummy_t;
    infile >> nmatch >> l_dummy_t;

    DBG_(fprintf
	 (stderr, " Allocating memory for %d matching values %d\n", nmatch,
	  l_dummy_t));

    //Allocate memory for match value
    match = new int[2 * nmatch];

    //Read matches
    int l_max1, l_max2;
    l_max1 = 0;
    l_max2 = 0;
    int *l_t_match = match;

    for (int i = 0; i < nmatch; i++) {
	infile >> *l_t_match >> *(l_t_match + 1);
	if (*l_t_match > l_max1)
	    l_max1 = *l_t_match;
	if (*(l_t_match + 1) > l_max2)
	    l_max2 = *(l_t_match + 1);

	l_t_match += 2;
    }

    DBG_(fprintf
	 (stderr, "Max index in file's  columns: %d %d\n", l_max1,
	  l_max2));

    infile.close();



    if (C_Voxel1->isLoaded() & C_Voxel2->isLoaded()) {
	if ((l_max1 >= C_Voxel1->getNumOfVoxels())
	    || (l_max2 >= C_Voxel2->getNumOfVoxels())) {
	    if ((l_max1 <= C_Voxel2->getNumOfVoxels())
		&& (l_max2 <= C_Voxel1->getNumOfVoxels())) {
		DBG_(fprintf
		     (stderr,
		      " Changing the order of the matched sequence to match sizes\n"));
		DBG_(fprintf
		     (stderr, " max1:%d , max2: %d \n", l_max1, l_max2));
		match_id[0] = 1;
		match_id[1] = 0;
	    } else {
		DBG_(fprintf
		     (stderr,
		      " Size of match file do not agree with loaded voxelsets\n"));
		DBG_(fprintf
		     (stderr, " Sizes %d %d %d %d\n",
		      C_Voxel1->getNumOfVoxels(),
		      C_Voxel2->getNumOfVoxels(), l_max1, l_max2));
		return 1;
	    }
	}
    }

    if (C_Mesh1->isLoaded() & C_Mesh2->isLoaded()) {
	if ((l_max1 >= C_Mesh1->getNumOfVertex())
	    || (l_max2 >= C_Mesh2->getNumOfVertex())) {
	    if ((l_max1 <= C_Mesh2->getNumOfVertex())
		&& (l_max2 <= C_Mesh1->getNumOfVertex())) {
		DBG_(fprintf
		     (stderr,
		      " Changing the order of the matched sequence to match sizes\n"));
		DBG_(fprintf
		     (stderr, " max1:%d , max2: %d \n", l_max1, l_max2));
		match_id[0] = 1;
		match_id[1] = 0;
	    } else {
		DBG_(fprintf
		     (stderr,
		      " Size of match file do not agree with loaded voxelsets\n"));
		DBG_(fprintf
		     (stderr, " Sizes %d %d %d %d\n",
		      C_Mesh1->getNumOfVertex(), C_Mesh2->getNumOfVertex(),
		      l_max1, l_max2));
		return 1;
	    }

	}
    }
    
    if (C_Embed1->isLoaded() & C_Embed2->isLoaded()) {
	if ((l_max1 >= C_Embed1->getNumOfPoints())
	    || (l_max2 >= C_Embed2->getNumOfPoints())) {
	    if ((l_max1 <= C_Embed2->getNumOfPoints())
		&& (l_max2 <= C_Embed1->getNumOfPoints())) {
		DBG_(fprintf
		     (stderr,
		      "Changing the order of the matched sequence to match sizes\n"));
		match_id[0] = 1;
		match_id[1] = 0;
	    } else {
		DBG_(fprintf
		     (stderr,
		      "Size of match file do not agree with loaded embeddings\n"));
		return 1;
	    }
	}
    }


    save_filename = p_filename;
    
    int l_ext = -1;
    if (l_ext > -1) {
	save_filename.remove(l_ext, save_filename.length() - l_ext);
    }
    save_filename.append("_snapshot");

    
    return 0;
}





int Shape::readProbabilisticMatch(const char *p_filename)
{

    std::ifstream infile;
    infile.open(p_filename);
    if (infile.is_open() == 0) {
	ERROR_(std::cout << "File " << p_filename << " not found" << std::
	       endl, ERROR_OPEN_FILE);
    }

    DBG_(fprintf
	 (stderr, " Reading PROBABILITY MATRIX file %s\n", p_filename));

    infile >> pm_dim[0] >> pm_dim[1];


    proba_vec = new double[pm_dim[0] * pm_dim[1]];
    bzero(proba_vec, pm_dim[0] * pm_dim[1] * sizeof(double));
    proba_matrix = new double *[pm_dim[0]];
    for (int i = 0; i < pm_dim[0]; i++) {
	proba_matrix[i] = proba_vec + i * pm_dim[1];
    }



    DBG_(fprintf(stderr, " Reading match values\n"));
    double *l_t_proba_vec = proba_vec;

    //Read matches
    for (int i = 0; i < pm_dim[0]; i++) {
	for (int j = 0; j < pm_dim[1]; j++) {
	    infile >> *l_t_proba_vec;
	    ++l_t_proba_vec;
	}
    }
    infile.close();


    DBG_(fprintf(stderr, " Finished reading probability matrix\n"));

    return 0;




}


int Shape::getNumOfMatch()
{
    return nmatch;
}

int *Shape::getMatch()
{
    return match;
}

int Shape::getMatch(int **p_out_match)
{
    if (!(*p_out_match)) {
	ERROR_(fprintf
	       (stderr, "Can not copy match to a non-allocated vector\n"),
	       ERROR_EMPTY_DATA);
    }
    for (int i = 0; i < nmatch * 2; i++) {
	(*p_out_match)[i] = match[i];
    }
    return 0;
}


int *Shape::getMatchId()
{
    return match_id;
}

int Shape::getTransfo(FL ** p_out_transfo, bool p_remove_first,
		      int p_out_dim)
{

    DBG_(std::
	 cout << "Getting T out of shape " << dimx << " " << p_out_dim <<
	 std::endl);

    if (!(*p_out_transfo)) {
	ERROR_(fprintf
	       (stderr,
		"Can not copy transfo to a non-allocated vector\n"),
	       ERROR_EMPTY_DATA);
    }


    int l_offset = 0;
    if (p_remove_first)
	l_offset = 1;

    for (int i = 0; i < p_out_dim; i++) {
	for (int j = 0; j < p_out_dim; j++) {
	    DBG2_(std::cout << transfo[i * (dimy + 1) + j] << " ");
	    (*p_out_transfo)[i * p_out_dim + j] =
		transfo[(i + l_offset) * (dimx + 1) + (j + l_offset)];
	}
	DBG2_(std::cout << std::endl);
    }
    DBG2_(std::cout << std::endl);

    return 0;
}


double Shape::computePerformance(int p_BodyID)
{


    FL *l_lbl1 = C_Voxel1->getLabels();
    FL *l_lbl2 = C_Voxel2->getLabels();
    double l_perf = 0;

    if (p_BodyID != -1) {
	int l_posMatch = 0;
	int l_negMatch = 0;
	for (int i = 0; i < nmatch; ++i) {
	    if (match[i * 2] > -1 && match[i * 2] > -1)
		if ((int) l_lbl1[match[i * 2 + match_id[0]]] == p_BodyID
		    && (int) l_lbl2[match[i * 2 + match_id[1]]] ==
		    p_BodyID) {
		    l_posMatch++;
		}
		else {
		    l_negMatch++;
		}
	}

	PRINT_COLOR_(std::
		     cout << "Positive Match = " << l_posMatch <<
		     " and Neg : " << l_negMatch << std::endl, 1);
	l_perf = (double) l_posMatch / (double) (l_posMatch + l_negMatch);
    }

    return l_perf;

}

void Shape::allocate3DShape()
{
    if (!C_Voxel1)
	C_Voxel1 = new Voxel();
    if (!C_Voxel2)
	C_Voxel2 = new Voxel();
        if (!C_Mesh1)
	C_Mesh1 = new Mesh();
    if (!C_Mesh2)
	C_Mesh2 = new Mesh();

    if (!C_Mesh3)
	C_Mesh3 = new Mesh();
    if (!C_Mesh4)
	C_Mesh4 = new Mesh();

}


void Shape::allocateEmbedding()
{
    if (!C_Embed1)
	C_Embed1 = new Embedding();
    if (!C_Embed2)
	C_Embed2 = new Embedding();
}



int Shape::transformEmbedCoord(bool p_solelyApplyInitT)
{


    int l_dim1 = C_Embed1->getDim();
    int l_dim2 = C_Embed2->getDim();
    FL *l_coords_2 = C_Embed2->getCoords();
    FL *l_coords_1 = C_Embed1->getCoords();
    int l_n2 = C_Embed2->getNumOfPoints();
    int l_n1 = C_Embed1->getNumOfPoints();



    if (l_dim1 != l_dim2) {
	FATAL_(std::
	       cout << "The two set of points do not have the same size" <<
	       std::endl, FATAL_VARIABLE_INIT);
    }



    if (coords_transfo_1) {
	delete[]coords_transfo_1;
	coords_transfo_1 = NULL;
    }

    if (coords_transfo_2) {
	delete[]coords_transfo_2;
	coords_transfo_2 = NULL;
    }
    if (!init_transfo) {

	PRINT_(std::
	       cout << "The initT transformation hasn't been loaded" <<
	       std::endl);

	coords_transfo_2 = new FL[l_n2 * l_dim2];
	bzero(coords_transfo_2, l_n2 * l_dim2 * sizeof(FL));
	for (int j = 0; j < l_n2; j++) {
	    for (int d = 0; d < l_dim2; d++) {
		coords_transfo_2[j * l_dim2 + d] =
		    l_coords_2[j * l_dim2 + d];
	    }
	}
	coords_transfo_1 = new FL[l_n1 * l_dim1];
	bzero(coords_transfo_1, l_n1 * l_dim1 * sizeof(FL));
	for (int j = 0; j < l_n1; j++) {
	    for (int d = 0; d < l_dim1; d++) {
		coords_transfo_1[j * l_dim1 + d] =
		    l_coords_1[j * l_dim1 + d];
	    }
	}


	return 1;
    }


    else {


	PRINT_(fprintf
	       (stderr,
		"-->Applying estimated transformation for dim = %d to embedding of dimension %d \n",
		mat_transfo_dim, l_dim2));
	if (apply_init_transfo_once == false) {
	    delete[]coords_transfo_2_t_dim_sel;
	    coords_transfo_2_t_dim_sel = NULL;
	}



	if (coords_transfo_2_t_dim_sel == NULL) {
	    FL *l_coords_transfo_2_t = new FL[l_n2 * mat_init_transfo_dim];
	    bzero(l_coords_transfo_2_t,
		  l_n2 * mat_init_transfo_dim * sizeof(FL));

	    // Perform coords_transfo_2 = T * initT * coords ;    
	    for (int j = 0; j < l_n2; j++) {
		for (int d = 0; d < mat_init_transfo_dim; d++) {
		    l_coords_transfo_2_t[j * mat_init_transfo_dim + d] = init_transfo[d * (mat_init_transfo_dim + 1) + mat_init_transfo_dim];	//load translation

		    //add Rotation
		    for (int k = 0; k < mat_init_transfo_dim; k++) {	//Inner product
			l_coords_transfo_2_t[j * mat_init_transfo_dim +
					     d] +=
			    init_transfo[d * (mat_init_transfo_dim + 1) +
					 k] * l_coords_2[j * l_dim2 + k];
		    }
		}
	    }

	    // Copy all embeddings after the dimension selection
	    int l_new_dim =
		performDimensionSelection(l_coords_transfo_2_t);

	    if (l_new_dim != mat_transfo_dim) {
		FATAL_(std::
		       cout <<
		       "The number of unmasked dimensions do not match the Transfo mat size (" << l_new_dim << " != " <<  mat_transfo_dim << ")"
		       << std::endl, FATAL_ERROR);
	    }


	}

	if (p_solelyApplyInitT || transfo == NULL) {
	    coords_transfo_2 = new FL[l_n2 * mat_transfo_dim];
	    memcpy(coords_transfo_2, coords_transfo_2_t_dim_sel,
		   l_n2 * mat_transfo_dim * sizeof(FL));
	}

	else {
	  coords_transfo_2 = new FL[l_n2 * mat_transfo_dim];
	  bzero(coords_transfo_2, l_n2 * mat_transfo_dim * sizeof(FL));
	  
	  for (int j = 0; j < l_n2; j++) {	    
	    for (int d = 0; d < mat_transfo_dim; d++) {
	      //load translation
	      coords_transfo_2[j * mat_transfo_dim + d] =
		transfo[d * (mat_transfo_dim + 1) + mat_transfo_dim] +
		coords_transfo_2_t_dim_sel[j * mat_transfo_dim + d];
	      //add Rotation
	      
	      for (int k = 0; k < mat_transfo_dim; k++) {
		coords_transfo_2[j * mat_transfo_dim + d] +=
		  transfo[d * (mat_transfo_dim + 1) + k] 
		  * coords_transfo_2_t_dim_sel[j * mat_transfo_dim + k] ;
		
	      }
	    }
	  }
	  
	  
	}
	
    }
    
    for (int j = 0; j < l_n2; j++) {	    
      FL scale = 0;
      for (int d = 0; d < mat_transfo_dim; d++) {
	scale += coords_transfo_2[j * mat_transfo_dim + d] * coords_transfo_2[j * mat_transfo_dim + d];
      }

      for (int d = 0; d < mat_transfo_dim; d++) {
	coords_transfo_2[j * mat_transfo_dim + d] /= sqrt(scale) ;
      }
    }

    for (int d = 0; d < mat_transfo_dim; d++) {
      for (int k = 0; k < mat_transfo_dim; k++) {
	std::cout << transfo[d * (mat_transfo_dim + 1) + k] << " " ;
      }
      std::cout <<std::endl ;
    }

	return 0;
}




int Shape::performDimensionSelection(FL * p_Y)
{

    PRINT_COLOR_(fprintf(stderr, "Performing dimension selection \n"),
		 RED);

    if (coords_transfo_2_t_dim_sel)
	delete[]coords_transfo_2_t_dim_sel;
    if (coords_transfo_1)
	delete[]coords_transfo_1;

    FL *l_out_1 = NULL;
    FL *l_out_2 = NULL;

    int l_dim = mat_init_transfo_dim;

    int *l_dimmask = new int[l_dim];
    bzero(l_dimmask, l_dim * sizeof(int));
    int l_countdim = 0;
    for (int i = 0; i < l_dim; i++) {
	for (int j = 0; j < l_dim; j++) {
	    if ((i < l_dim) && (init_transfo[i * (l_dim + 1) + j] != 0.0)) {
		l_dimmask[i] = 1;
	    }
	}
	if ((i < l_dim) && (l_dimmask[i] == 1)) {
	    l_countdim++;
	}
    }

    DBG_(fprintf
	 (stderr, "Found %d unmasked dimensions in the initialization T\n",
	  l_countdim));

    int l_new_dim = l_countdim;

    int l_n2 = C_Embed2->getNumOfPoints();
    int l_dim2 = C_Embed2->getDim();

    int l_n1 = C_Embed1->getNumOfPoints();
    int l_dim1 = C_Embed1->getDim();

    if (l_dim1 != l_dim2) {
	FATAL_(fprintf
	       (stderr, "The two embeddings have different dimensions\n"),
	       FATAL_VARIABLE_INIT);
    }

    l_out_2 = new FL[l_countdim * l_n2];
    l_out_1 = new FL[l_countdim * l_n1];

    bzero(l_out_1, l_countdim * l_n1 * sizeof(FL));
    bzero(l_out_2, l_countdim * l_n2 * sizeof(FL));

    coords_transfo_1 = l_out_1;
    coords_transfo_2_t_dim_sel = l_out_2;

    FL *l_X = C_Embed1->getCoords();

    int l_countd = 0;
    for (int d = 0; d < l_dim1; d++) {
	if (l_dimmask[d] == 1) {
	    //copy X to the new X
	    for (int i = 0; i < l_n1; i++) {
		l_out_1[i * l_new_dim + l_countd] = l_X[i * l_dim + d];
	    }
	    //copy Yt to the new Y
	    for (int j = 0; j < l_n2; j++) {
		l_out_2[j * l_new_dim + l_countd] = p_Y[j * l_dim + d];
	    }
	    l_countd++;
	}
    }

//select the dimensions
    delete[]l_dimmask;

    // DO NOT DELETE out_1 and out_2, coords_transfo points on them

    return l_new_dim;

}

// Loading Sequence parameters
void Shape::setApplyInitTOnce(bool p_status)
{

    apply_init_transfo_once = p_status;

}


int Shape::setSequenceLimits(int p_first, int p_last, int p_delta)
{
    match_it = p_first;
    match_first_it = p_first;
    match_last_it = p_last;
    match_delta = p_delta;
    match_saved = false;
    return 0;
}
int Shape::setPattern(char *&p_internal_pattern, char *p_pattern)
{
    p_internal_pattern = strdup(p_pattern);
    return 0;
}

int Shape::setMatchfilePattern(char *p_pattern)
{
    if (match_pattern)
	free(match_pattern);
    match_pattern = NULL;
    setPattern(match_pattern, p_pattern);

    return 1;
}

int Shape::setVoxelPattern(char *p_pattern)
{
    if (voxel_pattern)
	free(voxel_pattern);
    voxel_pattern = NULL;
    setPattern(voxel_pattern, p_pattern);

    return 0;
}

int Shape::setMeshPattern(char *p_pattern)
{
    if (mesh_pattern)
	free(mesh_pattern);
    mesh_pattern = NULL;
    setPattern(mesh_pattern, p_pattern);


    return 0;
}

int Shape::setEmbedPattern(char *p_pattern)
{
    if (embed_pattern)
	free(embed_pattern);
    embed_pattern = NULL;

    setPattern(embed_pattern, p_pattern);
    return 0;
}

void Shape::reshapeBinaryMatch (int *sort1, int* sort2) {


    for (int i = 0 ; i < nmatch ; ++i ) {
	if ( match[i * 2 + match_id[0]] != -1 &&  match[i * 2 + match_id[1]] != -1) {
	    match[i * 2 + match_id[0]] = sort1[match[i * 2 + match_id[0]]] ;
	    match[i * 2 + match_id[1]] = sort2[match[i * 2 + match_id[1]]] ;
	}
    }

}


void Shape::enableReshapeBinaryMatch(bool on) {

    enable_reshape_binary_match = on ;

}

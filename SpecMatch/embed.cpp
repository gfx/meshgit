/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
/*
 *  embed.cpp
 *  match
 *
 *  Created by Diana Mateus and David Knossow on 10/7/07.
 *  Copyright 2007 INRIA. All rights reserved.
 *
 */


#ifdef GNU_SVD
#include <gsl/gsl_linalg.h>
#include <gsl/gsl_math.h>
#include <gsl/gsl_eigen.h>
#else
#include <cv.h>
#endif




#include <math.h>

#include <iostream>
#include <stdio.h>
#include "embed.h"



Embed::Embed()
{
	
  params.method = LAP;
  params.edges = GAUSSIAN;
  params.laplacian = EIGENMAPS;
	
  params.sigma = 0.0;
  params.n_eigenfunc = 10;
	
  params.set_D_as_Voronoi = false;

	
  params.eigenvalue_power = 1;
	
  params.tolerance = 0.0;	// used to remove exponential values bellow tol (if zero and gaussian weights is then automatically set to a multiple of sigma)

  params.regular = 1.0 ;
	
  mask = NULL;
	
  nzelems_W = 0;
	
  Voronoi = NULL;
	
  sparse_W = NULL;
  mat_sparse_W = NULL;	//pointer to distmat do not allocate or deallocate inside
	
	
    full_W = NULL;
  mat_full_W = NULL;
	
  full_M = NULL;
  mat_full_M = NULL;
  	
  sparse_M = NULL;
  mat_sparse_M = NULL;
  nzelems_M = 0;
	
  E = NULL;
	
	
  D = NULL;
  loaded = false;
	
  eigen_values = NULL;
  eigen_vecs = NULL;
	
  npoints = 0;
	
  //eigendecomposition parameters
  invert_order = false;
  sprintf(which, "%s", "SM");
  sprintf(right_matrix, "%s", "G");
	
	
  
  

    Points = NULL;
  
  modify_M = true ;
  modify_W = true ;
  enable_cut_disconnected_componnents = true ;

}

Embed::~Embed()
{
	
  if (mat_sparse_W)
    delete[]mat_sparse_W;
  mat_sparse_W = NULL;
	
    if (mat_full_W)
    delete[]mat_full_W;
  mat_full_W = NULL;
  
  if ( full_M && modify_M ) {   
    delete [] full_M;
  }

  if (mat_full_M)
    delete[]mat_full_M;
  mat_full_M = NULL;
  	
  if (sparse_M)
    delete[]sparse_M;
	
	
  if (mat_sparse_M)
    delete[]mat_sparse_M;
	
	
  if (E)
    delete[]E;
	
  if (D)
    delete[]D;
	
  if (eigen_values)
    delete[]eigen_values;
	
  if (eigen_vecs)
    delete[]eigen_vecs;
	
	
  
  
	
	
  if (mask)
    delete[]mask;
	
}

void Embed::release()
{
	
  PRINT_(std::cout << "Embed::release() " << std::endl);
	
  if (mat_sparse_W)
    delete[]mat_sparse_W;
  mat_sparse_W = NULL;
	
    if (mat_full_W)
    delete[]mat_full_W;
  mat_full_W = NULL;
	
  if (full_M)
    delete[]full_M;
  full_M = NULL;
	
  if (mat_full_M)
    delete[]mat_full_M;
  mat_full_M = NULL;
  	
  if (sparse_M)
    delete[]sparse_M;
  sparse_M = NULL;
	
  if (mat_sparse_M)
    delete[]mat_sparse_M;
  mat_sparse_M = NULL;
	
	
  if (E)
    delete[]E;
  E = NULL;
	
  if (D)
    delete[]D;
  D = NULL;
	
  if (eigen_values)
    delete[]eigen_values;
  eigen_values = NULL;
	
  if (eigen_vecs)
    delete[]eigen_vecs;
  eigen_vecs = NULL;
	
  if (mask)
    delete[]mask;
  mask = NULL;
	
  	
	
  PRINT_(std::cout << "End of Embed::release()" << std::endl);
	
}

void Embed::init () {

  HASH_MAP_PARAM_ (bool, "sqeignorm" , Bool, params.sq_eig_norm) ; 

  
  HASH_MAP_PARAM_( bool, "def_remove_first", Bool, params.remove_first) ;
	
  HASH_MAP_PARAM_( NORMALIZATION_FORMAT, "norm_factor", Int, params.normalization_format) ;


}
int Embed::sparsePairwiseMatrix(FL * p_data, int p_n, int p_nzelems)
{

  // (nzelems * 3) functions to set the pointer to raw data

  if (!p_data) {
    std::
      cout << "Trying to load an empty sparse pairwise matrix" <<std::endl;
    return 1;
  }
	
  npoints = p_n;
  nzelems_W = p_nzelems;
  nzelems_M = 0;

  sparse_W = p_data;
	
  if (mat_sparse_W)
    delete [] mat_sparse_W;
  mat_sparse_W = allocateDMat(sparse_W, nzelems_W, 3);
	
  loaded = true;
	
  return 0;
}

int Embed::fullPairwiseMatrix( FULL_MAT_TYPE* p_data, int p_n)
{
  // (nxn) functions to set the pointer to raw data
  if (!p_data) {
    ERROR_(fprintf
	   (stderr, "Trying to load an empty full pairwise matrix \n"),
	   ERROR_EMPTY_DATA);
  }
	
  npoints = p_n;
	
  nzelems_W = npoints * npoints;
	
  nzelems_M = npoints * npoints;
  


  modify_W = false ;
  full_W = p_data;
	
  if (mat_full_W)
    delete[]mat_full_W;

  mat_full_W = allocateMat(full_W, nzelems_W, npoints);
  loaded = true;
	
  return 0;
}



int Embed::fillEmbeddingMatrices()
{
	
  int l_retval = 0;
	
  PRINT_(std::cout << "-->Filling Pairwise matrix" << std::endl);
	
  PRINT_(std::cout << "Compute the distance matrix" << std::endl);
  // Create the distance matrix. See distance.h.
	
  l_retval = C_Mat_dist.fillNeighborhoodMatrix();	//call from inside

  if (l_retval) {
    return l_retval;
  }
  PRINT_(std::cout << "-->Loading Pairwise matrix" << std::endl);
	
  // Fill Wf or mat_sparse_W.
  if (C_Mat_dist.isSparse())
    l_retval =
      sparsePairwiseMatrix(C_Mat_dist.getSparseDistVector(),
			   C_Mat_dist.getNumOfNodes(),
			   C_Mat_dist.getNzelems());
    else
    l_retval =
      fullPairwiseMatrix(C_Mat_dist.getFullDistVector(),
			 C_Mat_dist.getNumOfNodes());
    
    
      
  if (!loaded || l_retval) {
    ERROR_(fprintf
	   (stderr,
	    " Data has not been loaded correctly in fill_embedding_matrices \n"),
	   ERROR_EMPTY_DATA);
  }
  //transform the matrix and decompose
  PRINT_(std::
	 cout << "-->Transform the matrix before eigendecompose (Method: "<<(int)params.method<<")" << std::
	 endl);
	
  switch (params.method) {
      case LLE:
    lle();
    break;
          case ISOMAP:
    isomap();
    break;
      case LAP:
    laplacian();
    break;
  default:
    WARNING_(fprintf(stderr, "Inexistent embedding method \n"));
    break;
  }
	
  return l_retval;
}

int Embed::findEmbeddingCoords()
{
  int l_retval = 0;
	
  l_retval = fillEmbeddingMatrices();
	
  if (l_retval)
    return l_retval;
	
  PRINT_(std::cout << "-->Find best embedding coordinates" << std::endl);
  l_retval = eigenDecomposition();
  PRINT_(std::cout << "-->Embedding Coordinates found" << std::endl);
	
  return l_retval;
}


int Embed::findEmbeddingCoords(FL * p_sparse_w,
			       int p_n_elem /*num of vertex */ ,
			       int p_nz_elems)
{
	
	
  int l_retval = 0;
	
  switch ( C_Mat_dist.getNeiMode()) {			
  case LOCAL_GEO:
	
    PRINT_(std::cout << "-->Loading SPARSE Pairwise matrix" << std::endl);
    DBG_(std::cout <<  p_n_elem << " " <<  p_nz_elems <<std::endl );
    // Fill Ws and sparseW
    l_retval = sparsePairwiseMatrix(p_sparse_w, p_n_elem, p_nz_elems);

    l_retval = C_Mat_dist.loadSparseDistance(p_sparse_w, p_n_elem, p_nz_elems) ;

    if (!loaded || l_retval) {
      ERROR_(fprintf
	     (stderr,
	      "Data has not been loaded correctly in find_embedding_coords \n"),
	     ERROR_EMPTY_DATA);
    }

    /*			   
    // If voronoi_areas is set, use it as the normalization factor.
    if (p_voronoi_areas) {
    setDVecAsVoronoi(p_voronoi_areas);
    }
    */		
    break;			
      case FULL_GEO:

    l_retval = C_Mat_dist.computeFullGeodesicDistances(p_sparse_w,p_n_elem,p_nz_elems);
    l_retval+= fullPairwiseMatrix(C_Mat_dist.getFullDistVector(),
				  C_Mat_dist.getNumOfNodes());
		    
		    
    break;
    		    
  default:
    WARNING_(fprintf
	     (stderr, "In construction: neighborhood estimation method\n"));
    break;
  }
  PRINT_(fprintf(stderr, "Neighborhood Filled \n"));

	
    
  //transform the matrix and decompose
  PRINT_(std::
	 cout << "-->Transform the matrix before eigendecompose" << std::
	 endl);
	
  switch (params.method) {
      case LLE:
    lle();
    break;
          case ISOMAP:
    l_retval = isomap();
    break;
      case LAP:
    
    l_retval = laplacian();
    break;
  default:
    WARNING_(fprintf(stderr, "Inexistent embedding method \n"));
    break;
  }
	
	
  if (l_retval)
    return l_retval;
	
  PRINT_(fprintf(stderr, "-->Find best embedding coordinates \n"));
	
  l_retval = eigenDecomposition();
	
  PRINT_(fprintf(stderr, "-->Embedding Coordinates found \n"));
	
	
  return l_retval;
}


void Embed::setDVecAsVoronoi(FL * p_voronoi)
{
  DBG_(std::cout << " ---> Set Approximation of Voronoi Area" << std::
       endl);
	
  params.set_D_as_Voronoi = true;
	
  Voronoi = p_voronoi;
}

int Embed::laplacian()
{
  PRINT_(fprintf
	 (stderr, "-->LAPLACIAN running on %d points \n", npoints));
	
  int l_retval = 0;
	
  //prepare diagonal degree matrix 
  if (D)
    delete[]D;
  D = new FL[npoints];
  memset(D, 0, npoints * sizeof(FL));

#if 0
  //SAVING SPARSE_W : the adjency matrix without gaussian kernelling
  saveDistMat( "tmp/AdjencyMatrix.adj.dist" ) ;
#endif
	
  // Eigenmaps
  if (sparse_W) {
    l_retval = sparseLaplacian();
  }

    else if (mat_full_W) {
    l_retval = fullLaplacian();
  }
  
  else {
    ERROR_(fprintf
	   (stderr, "Neither Ws or Wf exist. Laplacian aborted \n"),
	   ERROR_EMPTY_DATA);
  }
	
  return l_retval;
}


int Embed::sparseLaplacian()
{
	
  int l_retval = 0;
	
  
  
  
  
  PRINT_COLOR_(fprintf(stderr, "-->Sparse Laplacian \n"), RED);
  
  
  hash_map < std::pair<int,int>, FL, hash_func_pair_int_int > hash_map_Laplacian ;
  hash_map< std::pair<int,int> , FL, hash_func_pair_int_int >::iterator it =  hash_map_Laplacian.begin() ;
  
  if (sparse_M) {
    delete[]sparse_M;
    sparse_M = NULL;
  }
  if (mat_sparse_M) {
    delete[]mat_sparse_M;
    mat_sparse_M = NULL;
  }
  // Exponential, topological or just copy (0, 1, 2) weigths
  // Fill Diagonal matrix D
	
  int l_cut = 0;
	
  switch (params.edges) {
  case GAUSSIAN:
    {
      PRINT_COLOR_(fprintf(stderr, "-->Exponenial weights \n"),
		   GREEN);
      //Find scale, if not given (==0.0)
      //estimate from the distance matrix W
      if (params.sigma == 0.0) {
	PRINT_(fprintf(stderr, "-->Estimate scale \n"));
	params.sigma = sparseScale(sparse_W, nzelems_W);
	if (params.tolerance == 0.0)
	  params.tolerance = exp(-12.5);	//exp(-(5*sigma)^2/(2*sigma^2))
				
      }
      PRINT_(fprintf(stderr, "-->Scale: %f \n", params.sigma));
      PRINT_(fprintf(stderr, "-->tol: %f \n", params.tolerance));
			
      // w_ij  = exp(-(w_ij)²/(2 * sig²))
      FL l_factor = -0.5 / ((params.sigma) * (params.sigma));
      FL *l_sparse_W_ = sparse_W ;
      for (int i = 0; i < nzelems_W; i++, l_sparse_W_ += 3) {
	FL l_val = l_sparse_W_[2];
				
	l_val = exp(l_factor * (l_val) * (l_val));
	// Check for disconnected componnents
	if (l_val > params.tolerance) {
	  hash_map_Laplacian.insert ( std::make_pair(std::make_pair((int)l_sparse_W_[0], (int)l_sparse_W_[1]), l_val)) ;
	  D[(int) l_sparse_W_[0]] += l_val;
	} else {
	  l_cut++;
	  hash_map_Laplacian.insert ( std::make_pair(std::make_pair((int)l_sparse_W_[0], (int)l_sparse_W_[1]), 0.0)) ;
	}
      }
			
      PRINT_(fprintf
	     (stderr, "%d CUT (disconnected) values \n", l_cut));
			
      if (l_cut == nzelems_W) {
	ERROR_(fprintf
	       (stderr,
		"EXIT embed:: Error in GAUSSIAN scale, ALL the elements have been cut \n"),
	       ERROR_ILL_COND);
      }
	




		
      break;
    }
  case TOPOLOGICAL:		//replace by zero one values
    {
      PRINT_COLOR_(fprintf(stderr, "-->Topological weights \n"),
		   GREEN);
      FL *l_sparse_W_ = sparse_W;
      for (int i = 0; i < nzelems_W; i++, l_sparse_W_ += 3) {
	hash_map_Laplacian.insert ( std::make_pair(std::make_pair((int)l_sparse_W_[0], (int)l_sparse_W_[1]), 1)) ;
	D[(int) l_sparse_W_[0]] += 1.0;
      }
			
      break;
    }
  case NONE:
    {
      PRINT_COLOR_(fprintf
		   (stderr,
		    "--------- Laplacian = Distance Matrix  ------- \n"),
		   GREEN);
      FL *l_sparse_W_ = sparse_W;
      for (int i = 0; i < nzelems_W; i++, l_sparse_W_ += 3) {
	hash_map_Laplacian.insert ( std::make_pair(std::make_pair((int)l_sparse_W_[0], (int)l_sparse_W_[1]), l_sparse_W_[2])) ;
	D[(int) l_sparse_W_[0]] += l_sparse_W_[2];
      }


      break;
    }
  }
	
  if (params.set_D_as_Voronoi) {
    PRINT_COLOR_(fprintf
		 (stderr, "--------- Setting D as Voronoi area ------- \n"), GREEN);
    for (int k = 0; k < npoints; ++k) {
      D[k] = Voronoi[k];
    }
  }
  
#ifdef DEBUG_3
  for (int k = 0; k < npoints; ++k) {
    std::cout << D[k] << " " ;
  }
  std::cout <<std::endl ;
#endif

  PRINT_(fprintf(stderr, "-->Fill matrices for eigendecomposition \n"));
	
  //Build the M matrix to decompose
  switch (params.laplacian) {
  case ADJACENCY:{		//Wv = lv : M = W Take highest eigenvalues
			
    PRINT_COLOR_(fprintf(stderr, "-->ADJACENCY \n"), RED);
			
    nzelems_M = nzelems_W;

    sparse_M = new FL[nzelems_M * 3];
    bzero(sparse_M, nzelems_M * 3 * sizeof(FL));
    //Will be filled at the end of this function
    mat_sparse_M = allocateDMat(sparse_M, nzelems_W, 3);

    sprintf(right_matrix, "%s", "I");
    strcpy(which, "LA");
			
    break;
  }
  case SHIMEILA:{		// Wv = lDv : M=W Take highest eigenvalues
    PRINT_COLOR_(fprintf(stderr, "-->SHI MEILA \n"), RED);
			
    nzelems_M = nzelems_W ;
    sparse_M = new FL[nzelems_M * 3];
    bzero(sparse_M, nzelems_M * 3 * sizeof(FL));
    //Will be filled at the end of this function
    mat_sparse_M = allocateDMat(sparse_M, nzelems_W, 3);
			
    sprintf(right_matrix, "%s", "G");
    strcpy(which, "LA");
			
    break;
  }
  case DIFFUSION_DISTANCE:{	//D^(-1/2)(W)D^(-1/2)v = lv : M = D^(-1/2)(W)D^(-1/2)
    // In fact D^(-1)Wu = lu should be decomposed. But D^(-1)W is not symmetric.
    // Decomposing  D^(-1/2)(W)D^(-1/2) gives same eigenvalues. Eigenvectors are 
    // then obtain with u = D^(-1/2)v.    Take highest eigenvalues
			
    PRINT_COLOR_(fprintf(stderr, "-->DIFFUSION DISTANCE \n"), RED);
    nzelems_M = nzelems_W;
    sparse_M = new FL[nzelems_M * 3];
    bzero(sparse_M, nzelems_M * 3 * sizeof(FL));
			
    mat_sparse_M = allocateDMat(sparse_M, nzelems_W, 3);
			
    FL *l_invD = new FL[npoints];
			
    DBG_(std::cout << " SQRT Invert Diagonal Matrix" << std::endl);
    int l_count = 0;
    double l_z = 0 ;
    HASH_MAP_PARAM_( double, "zero_th", Double, l_z ) ;
    for (int i = 0; i < npoints; i++) {
      if ( sqrt(D[i]) < l_z ) {
	l_count++;
	WARNING_(fprintf(stderr, "sqrt(D) < ZERO_TH. %d : %g \n", i, D[i]));
	l_invD[i] = 0;
      } else {
	l_invD[i] = 1 / sqrt(D[i]);
      }
    }
			
    DBG_(std::
	 cout << l_count <<
	 " values of D[i] close to zero. Be sure they are removed/masked"
	 << std::endl);

    it =  hash_map_Laplacian.begin() ;
    int l_ni = 0 ;
    int l_nj = 0 ;
    while ( it != hash_map_Laplacian.end()) {
      l_ni = it->first.first ;
      l_nj = it->first.second ;
      it->second *= l_invD[l_ni];
      it->second *= l_invD[l_nj];
      ++it ;
    }		
    delete[]l_invD;
    l_invD = NULL;

        
    sprintf(right_matrix, "%s", "I");
    strcpy(which, "LA");
			
    break;
  }
  case EIGENMAPS:		//(D-W)v = lDv : M = D-W Take smallest eigenvalues
    {
      PRINT_COLOR_(fprintf(stderr, "-->LAPLACIAN EIGENMAPS \n"),
		   RED);
			
      // allocate additional space (n elements) in case of using (D-W)
      nzelems_M = nzelems_W + npoints;
      sparse_M = new FL[nzelems_M * 3];
      mat_sparse_M = allocateDMat(sparse_M, nzelems_M, 3);
			
      //Build the left side matrix (D-W)
      //copy -W

      it =  hash_map_Laplacian.begin() ;
      while ( it != hash_map_Laplacian.end()) {
	it->second = - it->second ;
	++it ;
      }
      for (int j = 0; j < npoints; j++) {
	hash_map_Laplacian.insert(std::make_pair(std::make_pair(j,j), D[j])) ;
      }

      sprintf(right_matrix, "%s", "G");
      strcpy(which, "SA");

      break;
    }
  case SIMPLE_EMAP:{		// (D-W)v = lv : M=D-W   Take smallest eigenvalues
    PRINT_COLOR_(fprintf(stderr, "-->SIMPLE EMAP \n"), RED);
			
    // allocate additional space (n elements) in case of using (D-W)
    nzelems_M = nzelems_W + npoints;
    sparse_M = new FL[nzelems_M * 3];
    mat_sparse_M = allocateDMat(sparse_M, nzelems_M, 3);
			
    //Build the left side matrix (D-W)
    //copy -W
    it =  hash_map_Laplacian.begin() ;
    while ( it != hash_map_Laplacian.end()) {
      it->second = -it->second ;
      ++it ;
    }
    //Add D on diagonal 
    for (int j = 0; j < npoints; j++) {
      hash_map_Laplacian.insert(std::make_pair(std::make_pair(j,j), D[j])) ;
    }

    for (int i = 0; i < npoints; i++) {
      D[i] = 1.0;
    }
			
    sprintf(right_matrix, "%s", "I");
    strcpy(which, "SA");
			
    break;
  }
  case NJW:{			//D^(-1/2)(D-W)D^(-1/2)v = lv : M = D^(-1/2)(D-W)D^(-1/2)
    // Take smallest eigenvalues
    PRINT_COLOR_(fprintf
		 (stderr, "-->NJW (Spectral Clustering) \n"), RED);
			
    // allocate additional space (n elements) in case of using (D-W)
    nzelems_M = nzelems_W + npoints;
    sparse_M = new FL[nzelems_M * 3];
    mat_sparse_M = allocateDMat(sparse_M, nzelems_M, 3);
			
    //Build the left side matrix (D-W)
    //copy -W
    hash_map< std::pair<int,int> , FL, hash_func_pair_int_int >::iterator it =  hash_map_Laplacian.begin() ;
    while ( it != hash_map_Laplacian.end()) {
      it->second = - it->second ;
      ++it ;
    }
    //Add D on diagonal
    for (int j = 0; j < npoints; j++) {
      hash_map_Laplacian.insert(std::make_pair(std::make_pair(j,j), D[j])) ;
    }
    
    //	saveSparseMat("Sparse_M_raw.dat", mat_sparse_M, nzelems_M) ;			

    FL *l_invD = new FL[npoints];

    DBG_(std::cout << " Invert Diagonal Matrix" << std::endl);
    int l_count = 0;
    double l_z ;
    HASH_MAP_PARAM_( double, "zero_th", Double, l_z ) ;
    for (int i = 0; i < npoints; i++) {
      if ( sqrt(D[i]) < l_z ) {
	l_count++;
	WARNING_(fprintf(stderr, "%d : %g \n", i, D[i]));
	l_invD[i] = 0;
      } else {
	l_invD[i] = 1 / sqrt(D[i]);
      }
    }
			
    DBG_(std::
	 cout << l_count <<
	 " values of D[i] close to zero. Be sure they are removed/masked"
	 << std::endl);

    it =  hash_map_Laplacian.begin() ;
    int l_ni = 0 ;
    int l_nj = 0 ;
    while ( it != hash_map_Laplacian.end()) {
      l_ni = it->first.first ;
      l_nj = it->first.second ;
      it->second *= l_invD[l_ni];
      it->second *= l_invD[l_nj];
      ++it ;
    }
			
    delete[]l_invD;
    l_invD = NULL;
    sprintf(right_matrix, "%s", "I");
    strcpy(which, "SA");

    it =  hash_map_Laplacian.begin() ;
    hash_map< std::pair<int,int> , FL, hash_func_pair_int_int >::iterator sym ;
    while ( it != hash_map_Laplacian.end()) {
      
      sym = hash_map_Laplacian.find(std::make_pair(it->first.second, it->first.first)) ;
      
      
      
#ifdef DEBUG
      if (sym == hash_map_Laplacian.end()) {
	FATAL_(std::cout << "SPARSE_W is not symetric at all..." << it->first.second<< " " << it->first.first << std::endl, FATAL_ERROR) ;
      }

      if ( ABS(sym->second - it->second) > 1e-13) {
	FATAL_(std::cout << "SPARSE_W is not symetric at all..." << it->first.first<< " " << it->first.second << " " << it->second << " " << sym->second << std::endl, FATAL_ERROR) ;
      }
#endif


      sym->second = (sym->second + it->second) / 2 ;
      it->second = sym->second;

      ++it ;
    }

    //	saveSparseMat("Sparse_M.dat", mat_sparse_M, nzelems_M) ;

    break;
			
  }
  }

  // From the hash_map, create a map (less efficient when searching), to 
  // order elements of the matrix.
  // Then copy the map in the array.
  it =  hash_map_Laplacian.begin() ;
  std::map< std::pair<int,int>, FL > l_map ;
  while ( it != hash_map_Laplacian.end()) {
    l_map.insert (std::make_pair(it->first, it->second)) ;
    ++it ;
  } 
  if (l_map.size() != nzelems_M ) {
    FATAL_(std::cout << "SPARSE_W is not symetric at all..." << l_map.size() << " " <<  nzelems_M <<std::endl, FATAL_ERROR) ;
  }
  
  std::map< std::pair<int,int>, FL >::iterator it_map = l_map.begin() ;
  
  int i = 0 ;
  
  FL *l_sparse_M = sparse_M ;
  while (it_map != l_map.end() ) {
    *(l_sparse_M) = (FL)it_map->first.first ;
    *(l_sparse_M + 1) = (FL)it_map->first.second ;
    *(l_sparse_M + 2) = (FL)it_map->second ;
    l_sparse_M += 3 ;
    ++it_map ;
  }
  

  l_map.clear () ;
  hash_map_Laplacian.clear () ;
  

#if 0
    {
      DBG_(std::cout << "Checking symmetry of Sparse_M in sparseLaplacian()" <<std::endl );

      FL **l_temp_mat ;
      int l_msize = nzelems_M * 3;

      l_temp_mat = new FL *[npoints] ;
      for (int i = 0 ; i < npoints ; ++i) {
	l_temp_mat[i] = new FL [npoints] ; 
	memset(l_temp_mat[i], 0, npoints * sizeof(FL)) ;
      }
      
      for (int i = 0; i < l_msize; i+=3) {
	l_temp_mat[ (int) sparse_M[i]][(int)sparse_M[i+1]] = sparse_M[i + 2] ;
      }

      for (int i = 0; i < npoints; i++) {
	for (int j = 0; j < npoints; j++) {
	  if ( l_temp_mat[i][j] != l_temp_mat[j][i ] ) {
	    WARNING_(std::cout << std::setprecision (20) << "Mat is not sym on " << i << " " << j << " (" <<l_temp_mat[i][j] << " , " <<l_temp_mat[j][i] << ") " <<   std::endl) ;
	  }
	}
      }
      DBG_(std::cout << "End of Sym check" <<std::endl) ;
      fflush(stdout) ;
      fflush(stdin) ;
      getchar() ;
      for (int i = 0 ; i < npoints ; ++i) {
	delete [] l_temp_mat[i] ;
      }
      delete [] l_temp_mat ;
    }
#endif

  return l_retval;
}


int Embed::fullLaplacian()
{
	
  PRINT_COLOR_(fprintf(stderr, "-->Full Laplacian \n"), RED);
  if (full_M) {
    delete[]full_M;
    full_M = NULL;
  }
  if (mat_full_M) {
    delete[]mat_full_M;
    mat_full_M = NULL;
  }
	
  full_M = new FULL_MAT_TYPE[npoints * npoints];
  mat_full_M = allocateMat(full_M, npoints, npoints);
	
  if (params.edges == GAUSSIAN) {
    //Find scale, if not given (==0.0)
    //estimate from the distance matrix W
    if (params.sigma == 0.0) {

      params.sigma = 0.1 * fullScale(full_W, npoints * npoints);
      if (params.tolerance == 0)
	params.tolerance = exp(-4.5);	//exp(-(3sigma)^2/(2*sigma^2))
    }
    float l_factor = -0.5 / ((params.sigma) * (params.sigma));

    int idx = 0 ;
    FULL_MAT_TYPE *l_t_full_W = full_W ;
    FULL_MAT_TYPE *l_t_full_M = full_M ;
    for (int i = 0; i < npoints; i++) {
      for (int j = 0; j < npoints; j++) {

	FL l_val = exp(l_factor * *(l_t_full_W));

	if (l_val > params.tolerance) {
	  *(l_t_full_M) = (FULL_MAT_TYPE)l_val;
	  D[i] += (FULL_MAT_TYPE)l_val;
	  
	} 
	else {
	  *(l_t_full_M) = 0.0;
	}
	++l_t_full_M ;
	++l_t_full_W ;
	++idx ;
      }


    }
  } else {
    for (int i = 0; i < npoints * npoints; i++) {
      full_M[i] = 1.0;
      int ii = i % npoints;
      D[ii] += full_M[i];
    }
  }
  PRINT_(std::cout << "Full decomposition not implemented" << std::endl);
	
  //TODO : D is not nxn anymore
  /* CvMat* D = cvCreateMat(n,n,CV_FL);
	 
  for(int i = 0; i < n; i++){
  cvSet1D();
  }
  // W = D-W
  cvSub(&D_,full_M,full_M);
	 
  // W = DD*(D-W)*DD
  if (params.normalized_lap){
  FL* DD = new FL[n*n];
  CvMat DD_ = cvMat(n,n,CV_FL,DD);
  for (int i = 0; i < n; i++)
  DD[i*n+i] = 1.0/sqrt(D[i*n+i]);
  cvMatMul(DD,full_M,full_M);
  cvMatMul(full_M,DD,full_M);
  delete [] DD;
  }                
  CvMat full_M_ = cvMat(n,n,CV_FL,full_M);
  */
  return 0;
}


int Embed::eigenDecomposition()
{
	
  PRINT_(std::cout << "-->Eigendecomposition" << std::endl);
	
  int l_retval = 0;
	
  if (!sparse_M		
            && !full_M
            ) {
    FATAL_(fprintf
	   (stderr,
	    "Eigendecomposition aborted. M hasn't been filled \n"),
	   FATAL_MEMORY_ALLOCATION);
  }
	
  if (eigen_values) {
    delete[]eigen_values;
    eigen_values = NULL;
  }
  if (eigen_vecs) {
    delete[]eigen_vecs;
    eigen_vecs = NULL;
  }
	
  embed_dim = params.n_eigenfunc + 1;	// in case removing one
  eigen_values = new FL[npoints];
  eigen_vecs = new FL[npoints * embed_dim];    
	
  //SPARSE
  if (sparse_M) {
    l_retval = sparseEigenDecomposition();
  }
    else if (full_M) {
    l_retval = fullEigenDecomposition();
  }
  	
  if (l_retval) {
    FATAL_(fprintf(stderr, "Error in eigenDecomposition \n"),
	   l_retval);
  }
  // Perform different normalizations according to parameters
  //invert the ordering of eigen_values and eigenvectors if required  
  int l_start, l_inc;
  int l_last = embed_dim;
	
  if (!invert_order) {
    l_start = 0;
		
    l_inc = 1;
  } else {
    PRINT_(fprintf
	   (stderr,
	    "-->Inverting order of eigen_values / eigenvectors\n"));
    l_start = l_last - 1;
    l_inc = -1;
  }
  if (params.remove_first) {
    PRINT_(fprintf
	   (stderr, "-->Removing first eigenvalue/eigenvector \n"));
    l_start += l_inc;
  }
  //save obtained eigen_values
  FL *l_eigen_values_copy = new FL[embed_dim];
	
  for (int i = 0 ; i < embed_dim ; i++) {
    l_eigen_values_copy[i] = eigen_values[i];
  }
  embed_dim--;		//reset to original value
    
  //create and copy to E and eigen_values
  if (E) {
    delete[]E;
    E = NULL;
  }
  if (eigen_values) {
    delete[]eigen_values;
    eigen_values = NULL;
  }
  PRINT_(std::
	 cout << "-->Fill embedding E with  (" << npoints << "," <<
	 embed_dim << ")" << std::endl);
  E = new FL[npoints * embed_dim];
  eigen_values = new FL[embed_dim];
	
	
	
	
	
  int dd = l_start;
  for (int d = 0; d < embed_dim; d++) {
    eigen_values[d] = l_eigen_values_copy[dd];
    for (int i = 0; i < npoints; i++) {
      E[i * embed_dim + d] = eigen_vecs[i * (embed_dim + 1) + dd];	//usually estimating the selected value + 1 in case of removing first
    }
    dd += l_inc;
		
  }
	
#ifdef DEBUG_3
  std::cout << "EigenValues: " <<std::endl ;
  for (int d = 0; d < embed_dim; d++) {
    std::cout << eigen_values[d] << " " ;
  }
  std::cout << std::endl ;
  std::cout << "EigenVectors: " <<std::endl ;
  for (int i = 0; i < npoints; i++) {	
    for (int d = 0; d < embed_dim; d++) {
      std::cout << E[i * embed_dim + d]  << " " ;
    }
    std::cout << std::endl ;
  }
  std::cout << std::endl ;
#endif
	
  switch (params.laplacian) {
  case DIFFUSION_DISTANCE:{
    for (int i = 0; i < npoints; i++) {
      if (sqrt(D[i])) {
	for (int d = 0; d < embed_dim; d++) {
	  E[i * embed_dim + d] /= sqrt(D[i]);
	}
      }
    }
    int l_count = 0;
			
    for (int d = 0; d < embed_dim; d++) {
      l_count = 1;
      FL l_temp = eigen_values[d];
      while (l_count < params.eigenvalue_power) {
	eigen_values[d] *= l_temp;
	l_count++;
      }
    }
    WARNING_(fprintf
	     (stderr,
	      "In case of diffusion distance, we force the normalization of eigenvectors \n to be LAMBDA^{depth} \n"));
    params.sq_eig_norm = LAMBDA;
    break;
  }
  default:
    PRINT_(fprintf(stderr, "No normalization for Eigen vectors\n"));
  }
	
	
  //estimate_projection_error(E,eigen_values);
	
  //sqeigenorm if required
  if (params.sq_eig_norm) {
    switch (params.normalization_format) {
    case SQRT_LAMBDA:{
      PRINT_COLOR_(fprintf
		   (stderr,
		    "-->Normalizing eigenvectors with squared eigen_values \n"),
		   GREEN);
      for (int d = 0; d < embed_dim; d++) {
	FL l_norm = sqrt(eigen_values[d]);
	DBG2_(std::
	      cout << " norm " << d << ":" << l_norm << std::
	      endl);
	for (int i = 0; i < npoints; i++) {
	  E[i * embed_dim + d] *= l_norm;
	}
      }
      break;
    }
    case INV_SQRT_LAMBDA:{
      PRINT_COLOR_(fprintf
		   (stderr,
		    "-->Normalizing eigenvectors with inverse of squared eigen_values \n"),
		   GREEN);
      double l_z = 0 ;
      HASH_MAP_PARAM_( double, "zero_th", Double, l_z ) ;
      for (int d = 0; d < embed_dim; d++) {
	if (sqrt(eigen_values[d]) < l_z ) {
	  WARNING_(fprintf
		   (stderr,
		    " Could not normalize eigenvalue is too small %f \n",
		    eigen_values[d]));
	} else {
	  FL l_norm = 1.0 / sqrt(eigen_values[d]);
	  DBG2_(std::
		cout << " norm " << d << ":" << l_norm <<
		std::endl);
	  for (int i = 0; i < npoints; i++) {
	    E[i * embed_dim + d] *= l_norm;
	  }
	}
      }
      break;
    }
    case LAMBDA:{
      PRINT_COLOR_(fprintf
		   (stderr,
		    "-->Normalizing eigenvectors with eigen_values %d \n",
		    embed_dim), GREEN);
      for (int d = 0; d < embed_dim; d++) {
	FL l_norm = eigen_values[d];
	DBG2_(std::
	      cout << " norm " << d << ":" << l_norm << std::
	      endl);
	for (int i = 0; i < npoints; i++) {
	  E[i * embed_dim + d] *= l_norm;
	}
      }
      break;
    }
    default:
      break;
    }
  }
    delete[]l_eigen_values_copy;
	
  return 0;
}

int Embed::maskDisconnectedComponents()
{
	
  PRINT_(std::cout << "Mask disconnected components" << std::endl);
  int l_retval = 0;
	
  if (mask) {
    delete[]mask;
    mask = NULL;
  }
  mask = new int[npoints];
  n_masked = 0;
	
  //stores the labels of the components for each element in the matrix
  int *l_comp = NULL;
  l_comp = new int[npoints + 1];

  for (int  i = 0 ; i < (npoints + 1) ; ++i)
    l_comp[i] = -1 ;



    
  //stores the number of elements per component
  int *l_comp_count = NULL;
  l_comp_count = new int[npoints + 1];
    
  memset(l_comp_count, 0, (npoints + 1) * sizeof(int));
    
  int l_ncomp = 1;
  FL *l_spM_ = sparse_M;
    FULL_MAT_TYPE *l_fM_ = full_M;
  if (l_spM_)
    DBG_(std::cout<<"Verifying sparse matrix"<<std::endl);
  else if (l_fM_)
    DBG_(std::cout<<"Verifying full matrix"<<std::endl);
  
  int i = 0 ;
  int  j = 0 ;
  FL w=0;
  for (int e = 0 ; e < nzelems_M; e++, ++j) {
    w = 0 ;
    if (l_spM_){
      i = (int) l_spM_[0];
      j = (int) l_spM_[1];
      w = (FL) l_spM_[2];
      l_spM_ +=3;
    }
        else if(l_fM_){

      if(j>=npoints){
	i++;
	j=0;
	DBG3_(std::cout<<"Disconnected components verifying row:"<<i<<std::endl);
      }
      w = (FL) (*l_fM_);
      ++l_fM_;
    }
    
    if (w == 0 || i == j) {
      continue;
    }

    int ci = l_comp[i];
    int cj = l_comp[j];



    //if none of the nodes has been visited create a new component
    if (ci == -1 && cj == -1) {
			
      l_comp[i] = l_ncomp;
      l_comp[j] = l_ncomp;
      l_comp_count[l_ncomp] += 2;
      l_ncomp++;
      if (l_ncomp >= npoints + 1) {	//more components than voxels
	
	WARNING_(std::
	  cout <<
	  " Error in mask_disconnected_components (more components than voxels)"
		 << std::endl) ;
	l_retval = 1;
	break;
      }
    }
    //if one of the two has been visited propagate the label of the component
    else if (ci != -1 && cj == -1) {
      l_comp[j] = ci;
      l_comp_count[ci]++;
    } 
    else if (ci == -1 && cj != -1) {
      l_comp[i] = cj;
      l_comp_count[cj]++;
    }
    
    //if both have been visited but have different labels unify the label
    else if (ci != cj) {
      for (int k = 0; k < npoints; k++) {
	if (l_comp[k] == cj) {
	  l_comp[k] = ci;
	}
      }


      PRINT_( std::cout << "Unifying components " << ci << " (" <<  l_comp_count[ci] << ") and " << cj << " (" <<  l_comp_count[cj] << ")" << std::endl );
           l_comp_count[ci] += l_comp_count[cj];
      l_comp_count[cj] = 0;

    }
    
  
    
  }
  
  //choose the label with the biggest number of elements
  int l_max_label = 0;
  int l_max_count = 0;
  PRINT_(std::
	 cout << "Total number of visited components " << l_ncomp <<
	 std::endl);
  for (int c = 0; c < l_ncomp; c++) {
    if (l_comp_count[c] > 0)
      PRINT_(std::
	   cout << "Component " << c << " with  " <<
	   l_comp_count[c] << " elements" << std::endl);
    if (l_comp_count[c] > l_max_count) {
      l_max_count = l_comp_count[c];
      l_max_label = c;
    }
  }
  if (l_max_label == 0) {
    WARNING_(std::
      cout <<
      " Error in mask_disconnected_components: All components have 0 elements "
	     << std::endl);
    l_retval = 1;
  }
	

  n_masked = 0;
  if (enable_cut_disconnected_componnents) {
    for (int i = 0; i < npoints; i++) {
      if (l_comp[i] == l_max_label)
	mask[i] = 1;
      else {
	mask[i] = 0;
	n_masked++;
      }
    }

  }
  else {
    for (int i = 0; i < npoints; i++) {
      mask[i] = 1;
    }
  }
  PRINT_(std::
	 cout << "Found " << n_masked <<
	 " points in disconnected components" << std::endl);
    
  if (l_comp)
    delete[]l_comp;
	
  if (l_comp_count)
    delete[]l_comp_count;
	
  return l_retval;
}

int Embed::sparseEigenDecomposition()
{
	
  PRINT_COLOR_(fprintf(stderr, "-->Sparse LAPACK \n"), RED);
  //mask disconnected components 
  maskDisconnectedComponents();
	
  FL *l_eigvec = NULL;
  FL **l_eigmat = NULL;
  FL *l_eigD = NULL;
  int l_eigmat_nzelems = nzelems_M;
  int l_eigmat_n = npoints;
	
  if (n_masked == 0) {	// No disconnected components detected directly operate on Ms
    DBG_(std::cout << "No disconnected components detected " << std::
	 endl);
    l_eigmat = mat_sparse_M;
    l_eigD = D;
  } else {
		
    int l_nz_count = 0;
    FL *l_sparse_M_ = sparse_M;
    for (int i = 0; i < nzelems_M; i++, l_sparse_M_ += 3) {
      int indi = (int) (l_sparse_M_[0]);
      int indj = (int) (l_sparse_M_[1]);
      if (!mask[indi] || !mask[indj]) {
	l_nz_count++;
      }
    }
    DBG_(std::
	 cout << l_nz_count << " non zero entries removed" << std::
	 endl);
		
    l_eigmat_n -= n_masked;
    l_eigmat_nzelems -= l_nz_count;
    l_eigvec = new FL[l_eigmat_nzelems * 3];
    l_eigmat = allocateDMat(l_eigvec, l_eigmat_nzelems, 3);
    l_eigD = new FL[l_eigmat_n];
		
    // Create a matrix only pointing to valid points                
    l_sparse_M_ = sparse_M;
    FL *l_eigvec_ = l_eigvec;
		
    l_nz_count = 0;
    for (int i = 0; i < nzelems_M; i++, l_sparse_M_ += 3) {
      int indi = (int) (l_sparse_M_[0]);
      int indj = (int) (l_sparse_M_[1]);
			
      if (mask[indi] > 0 && mask[indj] > 0) {
	l_eigvec_[0] = l_sparse_M_[0];
	l_eigvec_[1] = l_sparse_M_[1];
	l_eigvec_[2] = l_sparse_M_[2];
	l_eigvec_ += 3;
	++l_nz_count;
      }
    }
		
    // Copy correspond elements from D
		
    for (int i = 0, count = 0; i < npoints; i++) {
      if (mask[i] > 0) {
	l_eigD[count] = D[i];
	++count;
      }
    }
		
    if (l_nz_count != l_eigmat_nzelems) {
      FATAL_(std::cout << "Incoherent number of voxels after disconnected components removal "
	     << l_nz_count << " " << l_eigmat_nzelems << std::endl, FATAL_VARIABLE_INIT);
    }
		
    //Update the indexes in the new matrix.
    //TODO: find a more efficient way to do this
    DBG_(std::cout << "Updating indices in matrices eigmat " << std::
	 endl);
    for (int i = 0, count = 0; i < npoints; i++) {
      if (mask[i] == 0) {
	int l_c_count = i - count;
	for (int j = 0; j < l_eigmat_nzelems; j++) {
	  if (l_eigmat[j][0] > l_c_count)
	    --l_eigmat[j][0];
	  if (l_eigmat[j][1] > l_c_count)
	    --l_eigmat[j][1];
					
	}
	count++;
      }
    }
		
    DBG_(std::
	 cout << "End of Updating indices in matrices eigmat " << std::
	 endl);
  }
	
    
#ifdef DEBUG_3
  std::cout << "Diagonal elements" <<std::endl ;
  for (int i = 0; i < l_eigmat_n; i++) {
    std::cout << l_eigD[i] << " " ;
  }
  std::cout << std::endl ;
#endif
    
    
  //Prepare Eigendecomposition
  GEigs *C_GEigs = NULL;
  Eigs *C_Eigs = NULL;
  FL *l_eval_ = NULL;
  FL *l_evec_ = NULL;
  PRINT_(std::
	 cout << "Sparse eigendecomposition with " << embed_dim << " " <<
	 which << " eigen_values" << std::endl);
	
  if (!strcmp(right_matrix, "G")) {
    PRINT_COLOR_(fprintf
		 (stderr,
		  "Using the Generalized eigen decomposition \n"),
		 GREEN);
    C_GEigs = new GEigs();
    C_GEigs->loadSparseMatrix(l_eigmat_n, l_eigmat_nzelems, l_eigmat);
    C_GEigs->loadDiagonalMatrix(l_eigmat_n, l_eigD);
    C_GEigs->setParams(embed_dim, which, right_matrix, 1);
    C_GEigs->dsaupd();
    l_eval_ = C_GEigs->getEigVals();
    l_evec_ = C_GEigs->getEigVecs();
  } else {
    PRINT_COLOR_(fprintf
		 (stderr, "Using the standard eigen decomposition \n"),
		 GREEN);
    C_Eigs = new Eigs();
    C_Eigs->loadSparseMatrix(l_eigmat_n, l_eigmat_nzelems, l_eigmat);
    C_Eigs->setParams(embed_dim, which, right_matrix, 1);
    C_Eigs->dsaupd();
    l_eval_ = C_Eigs->getEigVals();
    l_evec_ = C_Eigs->getEigVecs();
  }
	
  if (!strcmp(which, "LA"))
    invert_order = true;
	
  int l_n_count = 0;

  for (int j = 0; j < embed_dim; j++) {
    eigen_values[j] = l_eval_[j];


    l_n_count = 0;
    for (int i = 0; i < npoints; i++) {
      if (mask[i] > 0) {
	eigen_vecs[i * embed_dim + j] = l_evec_[l_n_count * embed_dim + j];
			
	l_n_count++;
      } else		//disconnected component
	eigen_vecs[i * embed_dim + j] = 0.0;
    }
  }
	
  if (C_Eigs) {
    delete C_Eigs;
    C_Eigs = NULL;
  }
  if (C_GEigs)
    delete C_GEigs;
  C_GEigs = NULL;
	
  if (l_eigvec) {
    delete[]l_eigvec;
    l_eigvec = NULL;
  }
  if ((l_eigmat_n != npoints) && l_eigmat) {
    delete[]l_eigmat;
    l_eigmat = NULL;
  }
  if ((l_eigmat_n != npoints) && l_eigD) {
    delete[]l_eigD;
    l_eigD = NULL;
  }
	
  if (l_n_count != npoints - n_masked) {
    ERROR_(std::
	   cout << "The total number of voxels " << npoints <<
	   " should match the sum of masked " << n_masked <<
	   " and unmasked " << l_n_count << " elements" << std::endl,
	   ERROR_VARIABLE_INIT);
  }
	
  return 0;
}


int Embed::fullEigenDecomposition()
{
  PRINT_COLOR_(fprintf(stderr, "Full eigendecomposition of %d x %d matrix\n",npoints,npoints), RED);
		
  //Verify that there are no disconnected components
  maskDisconnectedComponents();
	
  int l_npoints;
  if (n_masked == 0) {	
    // No disconnected components detected directly operate on Ms
    DBG_(std::cout << "No disconnected components detected " << std::endl);
    l_npoints = npoints;
  } else {
    DBG_(std::cout << "Removing disconnected components before SVD" << std::endl);
    // otherwise keep adjust full_M by removing the disconnected components
    FULL_MAT_TYPE* l_full_M_ = full_M; 
    int l_count_removed = 0;
    for (int i = 0; i< npoints; i++){
      if(mask[i] == 0){
	l_count_removed++;
      }
      else{
	int ind = i*npoints;
	//stock unmasked lines
	for(int j=0; j< npoints; j++,l_full_M_++)
	  l_full_M_[0] = full_M[ind+j];
      }
    }
    if (l_count_removed != n_masked)
      DBG_(std::cout<< "Removed lines should be equal to masked nodes"<<std::endl);
    l_npoints = npoints-l_count_removed;
  }
#ifdef GNU_SVD	
  //gsl_matrix_view gsl_fullM = gsl_matrix_view_array(full_M, l_npoints, l_npoints);
  gsl_matrix *gsl_full_M = gsl_matrix_alloc(l_npoints, l_npoints);
  for (int i = 0; i<l_npoints*l_npoints;i++){
    gsl_full_M->data[i] = (double)full_M[i];
  }
  gsl_vector *l_eigen_values = gsl_vector_alloc(l_npoints);
  gsl_matrix *l_eigen_vecs = gsl_matrix_alloc(l_npoints, l_npoints);
  gsl_vector *work = gsl_vector_alloc(l_npoints);
  PRINT_(fprintf(stderr, "-->gsl_SVD init\n"));
  gsl_linalg_SV_decomp(gsl_full_M, l_eigen_vecs, l_eigen_values, work);
  PRINT_(fprintf(stderr, "-->gsl_SVD end\n"));	
#else
	
  CvMat* l_eigen_values = cvCreateMat(l_npoints, 1, CV_32FC1);
  CvMat* l_eigen_vecs = cvCreateMat(l_npoints, l_npoints, CV_32FC1);
  CvMat l_mat_ = cvMat(l_npoints,l_npoints, CV_32FC1, full_M);
					 		 
  PRINT_(fprintf(stderr, "-->cvSVD init\n"));
  //# CV_SVD_MODIFY_A enables modification of matrix src1 during the operation. It speeds up the processing.
  //# CV_SVD_U_T means that the tranposed matrix U is returned. Specifying the flag speeds up the processing.
  //# CV_SVD_V_T me
  cvSVD(&l_mat_, l_eigen_values, l_eigen_vecs, 0, CV_SVD_MODIFY_A|CV_SVD_U_T| CV_SVD_V_T);	//descending order
  PRINT_(fprintf(stderr, "-->cvSVD end\n"));

#endif
  //copy eigenvalues and eigenvectors
  DBG_(std::cout << "Copying results of SVD to class variables" << std::endl);
#ifdef GNU_SVD
  int row_count = 0;;
  for(int i = 0; i < npoints; i++){
    for(int d = 0, ind = i*embed_dim; d < embed_dim; d++,ind++){
      if (i==0){
	eigen_values[d] = (FL)l_eigen_values->data[d];
      }
      if (mask[i] == 0)  
	eigen_vecs[ind] = 0.0; 
      else{
	eigen_vecs[ind] = (FL)l_eigen_vecs->data[row_count*l_npoints+d];
	eigen_vecs[ind]*= sqrt(eigen_values[d]);
      }
    }
    if (mask[i]!=0)
      row_count++;
  }
#else
  int row_count = 0;
  for(int i = 0; i < npoints; i++){
    for(int d = 0, ind = i*embed_dim; d < embed_dim; d++,ind++){
      if (i==0){
	eigen_values[d] = (FL)l_eigen_values->data.fl[d];
	DBG_(fprintf(stderr,"Eigenvalue %d %g\n", d,eigen_values[d]));
      }
      if (mask[i] == 0)  
	eigen_vecs[ind] = 0.0; 
      else
	eigen_vecs[ind] = (FL)l_eigen_vecs->data.fl[d*l_npoints+row_count];
      eigen_vecs[ind]*= sqrt(eigen_values[d]);
    }
    if (mask[i]!=0)
      row_count++;
  }
#endif
#ifdef GNU_SVD
  gsl_matrix_free(gsl_full_M);
  gsl_vector_free(l_eigen_values);
  gsl_matrix_free(l_eigen_vecs);
  gsl_vector_free(work);
#else
  cvReleaseMat(&l_eigen_values);
  cvReleaseMat(&l_eigen_vecs);
  //the matrix being modified start all over again
  if(full_M){
    delete[]full_M;
    full_M=NULL;
  }
		
#endif
  return 0;
}


FULL_MAT_TYPE Embed::fullMedian(FULL_MAT_TYPE * p_array, int p_nelems)
{
  FULL_MAT_TYPE l_mean = 0.0;
  FULL_MAT_TYPE l_max = 0.0;
  FULL_MAT_TYPE l_min = 0.0;
	
  bool l_first = true;
  for (int i = 0; i < p_nelems; i++) {
    FL l_val = (FL)p_array[i];
    if (l_val != 0.0) {
      l_mean += l_val;
      if (l_first) {
	l_max = l_val;
	l_min = l_val;
	l_first = false;
      }
      if (l_val > l_max)
	l_max = l_val;
      if (l_val < l_min)
	l_min = l_val;
    }
  }
	
  l_mean /= (FL) p_nelems;
	
  DBG_(fprintf
       (stderr, "mean: %g min %g max %g \n", l_mean, l_min, l_max));
	
  int max_median_trial ;
  HASH_MAP_PARAM_( int, "median_max_num_trials", Int, max_median_trial ) ;

  //median
  FULL_MAT_TYPE l_median = l_mean;
  int l_count_below, l_count_above;
  int l_old_below = -1;
  int l_old_above = -1;
  int l_same = 0;
  while (true) {
    l_count_below = 0;
    l_count_above = 0;
    for (int i = 0; i < p_nelems; i++) {
      if (p_array[i] < l_median)
	l_count_below++;
      if (p_array[i] > l_median)
	l_count_above++;
    }
    if ((l_count_below == l_count_above)
	|| (abs(l_count_above - l_count_below) == 1))
      break;
    if ((l_count_below == l_old_below)
	&& (l_count_above == l_old_above)) {
      l_same++;
      if (l_same == max_median_trial)
	break;
    } else
      l_same = 0;
    if (l_count_below < l_count_above) {
      l_min = l_median;
      l_median += (l_max - l_median) / 2.0;
    } else if (l_count_below > l_count_above) {
      l_max = l_median;
      l_median -= (l_median - l_min) / 2.0;
    }
    //fprintf(stderr,"median: %g count_below %d count_above %d\n",median,count_below,count_above);
    l_old_below = l_count_below;
    l_old_above = l_count_above;
  }
  DBG_(fprintf
       (stderr, "Full median: %g count_below %d count_above %d\n",
	l_median, l_count_below, l_count_above));
	
  return l_median;
}



FL Embed::sparseMedian(FL * p_sparse_mat, int p_size)
{
  //fprintf(stderr,"sparse_median \n");
  if (!p_sparse_mat) {
    std::cout << "Ws = NULL, cannot estimate sparse median " << std::
      endl;
    return -1.0;
  }
	
  FL l_mean = 0.0;
  FL l_min = 0.0;
  FL l_max = 0.0;
  int l_count = 0;
	
      
  bool l_first = true;
  FL *l_spm_ = &(p_sparse_mat[2]);
  FL l_val;


  for (int i = 0; i < p_size ; i++, l_spm_ += 3) {
    l_val = *l_spm_ ;

        if (l_val != 0.0) {
      l_mean += l_val;
      if (l_first) {
	l_min = l_val;
	l_max = l_val;
	l_first = false;
      }
      if (l_val < l_min)
	l_min = l_val;
      if (l_val > l_max)
	l_max = l_val;
    } else
      l_count++;
  }
  
  l_mean /= (FL) p_size;
  DBG_(fprintf
       (stderr, "mean: %g min %g max %g \n", l_mean, l_min, l_max));
	
  int max_median_trial ;
  HASH_MAP_PARAM_( int, "median_max_num_trials", Int, max_median_trial ) ;	

  //median
  FL l_median = l_mean;
  int l_count_below, l_count_above;
  int l_old_below = -1;
  int l_old_above = -1;
  int l_same = 0;
  while (true) {
    l_count_above = 0;
    l_count_below = 0;
    l_spm_ = &(p_sparse_mat[2]);
    for (int i = 0; i < p_size; i++, l_spm_ += 3) {
      if (*l_spm_ < l_median)
	l_count_below++;
      if (*l_spm_ > l_median)
	l_count_above++;
      //fprintf(stderr,"count_below: %d count_above: %d\n", count_below, count_above);
    }
    //fprintf(stderr,"median: %g count_below %d count_above %d\n",median,count_below,count_above);
    if ((l_count_below == l_count_above)
	|| (abs(l_count_above - l_count_below) == 1))
      break;
		
    if ((l_count_below == l_old_below)
	&& (l_count_above == l_old_above)) {
      //fprintf(stderr,"median:count==old\n");
      l_same++;
      if (l_same == max_median_trial )
	break;
    } else
      l_same = 0;
    if (l_count_below < l_count_above) {
      l_min = l_median;
      l_median += (l_max - l_median) / 2.0;
    } else if (l_count_below > l_count_above) {
      l_max = l_median;
      l_median -= (l_median - l_min) / 2.0;
    }
		
    l_old_below = l_count_below;
    l_old_above = l_count_above;
  }
  DBG_(fprintf
       (stderr,
	"Sparse median: %g count_below %d count_above %d (total %d)\n",
	l_median, l_count_below, l_count_above, p_size));
	
  return l_median;
}


FULL_MAT_TYPE Embed::fullScale(FULL_MAT_TYPE * p_array, int p_nelems)
{
	
  FULL_MAT_TYPE l_median = fullMedian(p_array, p_nelems);
	
  FULL_MAT_TYPE *l_abmat = new FULL_MAT_TYPE[p_nelems];
	
  for (int i = 0; i < p_nelems; i++)
    l_abmat[i] = fabs(p_array[i] - l_median);
	
  FULL_MAT_TYPE l_scale = fullMedian(l_abmat, p_nelems);
	
  delete[]l_abmat;
  return l_scale;
}




FL Embed::sparseScale(FL * p_sparse_mat, int p_size)
{
  //   This is ok only for zero mean centered gaussians
  //     FL median = sparse_median(sparse_mat,size);
  //     FL* abmat = new FL[size];
  //     memset (abmat, 0, size*sizeof(FL));
	
  //     FL* spm_ = &(sparse_mat[2]);
  //     for (int i=0; i < size; i++, spm_+=3){
  //          abmat[i] = fabs(*spm_-median) ;
  //          //fprintf(stderr,"abmat[%d,%d] %g\n",i,nj,abmat[i*n+nj]);
  //     }
  //     FL scale = full_median(abmat,size);
  //     delete [] abmat;
  FL l_scale = sparseMedian(p_sparse_mat, p_size);
	
  //Gaussian Huber Robust Scale Estimator [1981]
  double l_gauss_huber ;
  HASH_MAP_PARAM_( double, "gaussian_huber_robust_scale_estimator", Double, l_gauss_huber ) ;
  l_scale *= l_gauss_huber ;
	
  return l_scale;
}


//Acces to Laplacian and Weight matrices

int Embed::getNzelemsM()
{
  return nzelems_M;
}

int Embed::getNzelemsW()
{
  return nzelems_W;
}

FL *Embed::getD()
{
  return D;
}

FL *Embed::getLaplacian()
{
  return sparse_M;
}

FL *Embed::getWeights()
{
  return sparse_W;
}


///////////////////////////////////////////////////////////////////////////////////////////
// QUALITY & DISTORITON MEASURES 
//////////////////////////////////////////////////////////////////////////////////////////


int Embed::setMethod(METHOD method, LAPLACIAN laplacian, EDGES edges)
{
  params.method = method;
  params.laplacian = laplacian;
  params.edges = edges;
  return 0;
}

int Embed::setSigma(FL sigma)
{
  params.sigma = sigma;
  return 0;
}

int Embed::setNEigenFunc(int n_eigenfunc)
{
  params.n_eigenfunc = n_eigenfunc;
  return 0;
}

int Embed::setRemoveFirst(bool remove)
{
  params.remove_first = remove;
  return 0;
}

int Embed::setSqEigNorm(bool eignorm)
{
  params.sq_eig_norm = eignorm;
  return 0;
}

int Embed::setNormalizationFormat(NORMALIZATION_FORMAT format)
{
  params.sq_eig_norm = true;
	
  params.normalization_format = format;
  return 0;
}



NORMALIZATION_FORMAT Embed::getNormalizationFormat()
{
  return params.normalization_format;
}


int Embed::setNormalizationPower(int power)
{
  params.eigenvalue_power = power;
  return 0;
}


int Embed::setRegular(FL regular)
{
  params.regular = regular;
  return 0;
}

bool Embed::isLoaded()
{
  return loaded;
}

int Embed::getNPoints()
{
  return npoints;
}

int Embed::getNEigenFunc()
{
  return params.n_eigenfunc;
}

bool Embed::getRemoveFirst()
{
  return params.remove_first;
}

bool Embed::getSqEigNorm()
{
  return params.sq_eig_norm;
}


FL Embed::getRegular()
{
  return params.regular;
}

FL *Embed::getE()
{
  return E;
}

FL *Embed::getEigenValues()
{
  return eigen_values;
}

int Embed::getEmbeddedDim()
{
  return embed_dim;
}


void Embed::enableCutDisconnectedComps (bool on) {
  enable_cut_disconnected_componnents = on ;
}


//assumes nzelems
int Embed::saveSparseMat(char *name, FL ** mat, int nz)
{
  FILE *f = fopen(name, "w+");
  if (!f) {
    ERROR_(fprintf(stderr, "Could not open file %s\n", name),
	   ERROR_OPEN_FILE);
  }
  PRINT_(fprintf(stderr, "saving %s\n", name));
  fprintf(f, "%d\n", nz);
  for (int i = 0; i < nz; i++) {
    fprintf(f, "%d %d %g\n", (int) mat[i][0], (int) mat[i][1],
	    mat[i][2]);
  }
	
  fclose(f);
  return 0;
}

int Embed::saveDoubleMat(char *name, FL * mat, int n1, int n2)
{
  FILE *f = fopen(name, "w+");
  if (!f) {
    ERROR_(fprintf(stderr, "Could not open file %s\n", name),
	   ERROR_OPEN_FILE);
  }
  PRINT_(fprintf(stderr, "saving %s\n", name));
	
  FL val;
  fprintf(f, "%d %d\n", n1, n2);
  for (int i = 0; i < n1; i++) {
    for (int j = 0; j < n2; j++) {
      val = (FL) mat[i * n2 + j];
      fprintf(f, "%g ", val);
    }
    fprintf(f, "\n");
  }
  fclose(f);
  return 0;
	
}


int Embed::saveFullMat(char *name, FULL_MAT_TYPE * mat, int n1, int n2)
{
  FILE *f = fopen(name, "w+");
  if (!f) {
    ERROR_(fprintf(stderr, "Could not open file %s\n", name),
	   ERROR_OPEN_FILE);
  }
  PRINT_(fprintf(stderr, "saving %s\n", name));
	
  FL val;
  fprintf(f, "%d %d\n", n1, n2);
  for (int i = 0; i < n1; i++) {
    for (int j = 0; j < n2; j++) {
      val = (FL) mat[i * n2 + j];
      fprintf(f, "%g ", val);
    }
    fprintf(f, "\n");
  }
  fclose(f);
  return 0;
	
}

int Embed::saveFloatMat(char *name, FULL_MAT_TYPE * mat, int n1, int n2)
{
  FILE *f = fopen(name, "w+");
  if (!f) {
    ERROR_(fprintf(stderr, "Could not open file %s\n", name),
	   ERROR_OPEN_FILE);
  }
  PRINT_(fprintf(stderr, "saving %s\n", name));
	
  FL val;
  fprintf(f, "%d %d\n", n1, n2);
  for (int i = 0; i < n1; i++) {
    for (int j = 0; j < n2; j++) {
      val = (FL) mat[i * n2 + j];
      fprintf(f, "%g ", val);
    }
    fprintf(f, "\n");
  }
  fclose(f);
  return 0;
	
}



int Embed::saveEmbedding(char *name)
{
  if (!E) {
    ERROR_(fprintf
	   (stderr,
	    "Embedding hasn't been calculated yet. Saving cancelled\n"),
	   ERROR_OPEN_FILE);
  }
	
  PRINT_(std::cout << "Saving embedding in " << name << std::endl);
	
  FILE *f = fopen(name, "w+");
  if (!f) {
    ERROR_(fprintf(stderr, "Could not open file %s\n", name),
	   ERROR_OPEN_FILE);
  }
  fprintf(f, "%d %d\n", npoints, embed_dim);
  for (int i = 0; i < npoints; i++) {
    for (int j = 0; j < embed_dim; j++) {
      fprintf(f, "%g\t", E[i * embed_dim + j]);
    }
    fprintf(f, "\n");
  }
	
  fclose(f);
  return 0;
}

int Embed::saveEigenValues(char *name)
{
  if (!eigen_values) {
    ERROR_(fprintf
	   (stderr,
	    "Eigen_Values have not been calculated yet. Saving cancelled\n"),
	   ERROR_EMPTY_DATA);
  }
  FILE *f = fopen(name, "w+");
  if (!f) {
    ERROR_(fprintf(stderr, "Could not open file %s\n", name),ERROR_OPEN_FILE) ;
  }
	
  PRINT_(std::cout << "Saving eigen_values in " << name << std::endl);
	
  for (int i = 0; i < embed_dim; i++)
    fprintf(f, "%g\t", 1 - eigen_values[i]);
	
  fclose(f);
  return 0;
}
int Embed::saveDistMat(char *name)
{
  if (sparse_W) {
    saveSparseMat(name, mat_sparse_W, nzelems_W);
  }
    else if (full_W) {
    saveFullMat(name, full_W, npoints, npoints);
  }
  
  return 0;
}


int Embed::saveParams(char *name)
{
  FILE *f = fopen(name, "w+");
	
  if (!f) {
    ERROR_(fprintf
	   (stderr, "Could not open file %s for writing parameters\n",
	    name), ERROR_OPEN_FILE);
  }
	
  PRINT_(std::cout << "Saving embedding parameters " << name << std::
	 endl);
	
  fprintf(f, "method:\t");
  switch (params.method) {
      case LLE:
    fprintf(f, "LLE (0)\n");
    break;
          case ISOMAP:
    fprintf(f, "ISOMAP (2)\n");
    break;
      case LAP:
    fprintf(f, "LAP (1)\n");
    switch (params.laplacian) {
    case NJW:
      fprintf(f,
	      "Laplacian:\t Ng Jordan Weiss, Spectral Clustering\n");
      break;
    case SHIMEILA:
      fprintf(f, "Laplacian:\t Ncuts, Shi-Meila\n");
      break;
    case EIGENMAPS:
      fprintf(f, "Laplacian:\t Belkin, Eigenmaps\n");
      break;
    case ADJACENCY:
      fprintf(f, "Laplacian:\t Adjacency Matrix\n");
      break;
    case DIFFUSION_DISTANCE:
      fprintf(f, "Laplacian:\t Diffusion Distance\n");
      break;
					
    case SIMPLE_EMAP:
      fprintf(f,
	      "Laplacian:\t Belkin, Eigenmaps as in matlab code\n");
      break;
    }
    break;
  };
	
  fprintf(f, "edges:\t");
  switch (params.edges) {
  case TOPOLOGICAL:
    fprintf(f, "TOPOLOGICAL (0)\n");
    break;
  case GAUSSIAN:
    fprintf(f, "GAUSSIAN (1)\n");
    break;
  case NONE:
    fprintf(f, "NONE (2)\n");
  }
	
  fprintf(f, "neighborhood:\t");
  switch (C_Mat_dist.getNeiMode()) {
          case LOCAL_GEO:
    fprintf(f, "LOCAL_GEO (2)\n");
    break;
          case FULL_GEO:
    fprintf(f, "FULL_GEO (4)\n");
    break;
    
  }
  fprintf(f, "sigma:\t%f\n", params.sigma);
  fprintf(f, "n_eigenfunc\t%d\n", params.n_eigenfunc);
  fprintf(f, "remove_first\t%d\n", (int) params.remove_first);
  fprintf(f, "sqeignorm\t%d\n", (int) params.sq_eig_norm);
    fprintf(f, "tol\t%f\n", params.tolerance);
  fprintf(f, "which:\t%s\n", which);
  fprintf(f, "right_matrix:\t%s\n", right_matrix);
  fprintf(f, "lle_regularization:\t%g\n", params.regular);
  fclose(f);
  return 0;
	
};




int Embed::lle()
{
  //verify that sparse_W has been loaded
  if(!sparse_W){
    ERROR_(fprintf
	   (stderr, "Empty sparse distance matrix.Can not perform an LLE embedding (aborted)\n"),
	   ERROR_EMPTY_DATA);
  }




  //load the centered copy of the data (avoids recomputing the mean)
  FL* l_data = NULL;
  int l_dim = 0;
  if (!Points){
    l_data = C_Mat_dist.getData();
    l_dim = C_Mat_dist.getDimData();

  }
  else{ // for meshes point should have been loaded outside
    l_data = Points;
    l_dim = 3;
  }
	
  if(!l_data){
    ERROR_(fprintf
	   (stderr, "Empty data. Can not perform an LLE embedding (aborted)\n"),
	   ERROR_EMPTY_DATA);
  }
	
	
  PRINT_(fprintf(stderr,"-->LLE embedding on %d points \n",npoints));
	
  if (sparse_M) {
    delete[]sparse_M;
    sparse_M = NULL;
  }
  if (mat_sparse_M) {
    delete[]mat_sparse_M;
    mat_sparse_M = NULL;
  }
	
  if (D)
    delete[]D;
  D = new FL[npoints];
  memset(D, 0, npoints * sizeof(FL));
	
  nzelems_M = nzelems_W;
  sparse_M = new FL[nzelems_M*3];
  mat_sparse_M = new FL*[npoints]; //allocateDMat(sparse_M, nzelems_M, 3);
	
  for (int i = 0; i < nzelems_M*3; i++)
    sparse_M[i]=0.0;
	
  //verify mat_sparse_W exists, if not create it to speed up multiplication access
  if (!mat_sparse_W){
    mat_sparse_W = new FL*[npoints]; //allocateDMat(sparse_W, nzelems_W, 3);
    /*
      for (int i = 0; i < nzelems_W * 3; i++)
      mat_sparse_W[i] = 0 ;
    */
    for (int i = 0; i < npoints; i++)
      mat_sparse_W[i] = NULL ;
  }
  else {
    WARNING_(fprintf(stderr,"-->Overwriting mat_sparse_W \n"));
  }

  int* l_num_nei = new int[npoints]; //keep for later use
  for (int i = 0; i < npoints; i++)
    l_num_nei[i]=0;
	
  //fill mat_sparse_W and count the number fo neighbors
  //copy the connectivity pattern in mat_sparse_M
	
  mat_sparse_W[0] = &(sparse_W[0]);
  mat_sparse_M[0] = &(sparse_M[0]);
	
  for (int i = 1,wind=0; i < npoints ; i++){
    int l_count=0;
    while((i-1)==(int)(sparse_W[wind])){
      wind+=3;
      l_count++;
    }
    l_num_nei[i-1]=l_count;
    if(i==(int)sparse_W[wind]){
      mat_sparse_W[i]= &(sparse_W[wind]);
      mat_sparse_M[i]= &(sparse_M[wind]);
    }
  }
	
#ifdef DEBUG_3
  for (int i = 0,wind=0; i < npoints ; i++){
    int l_count=0;
    while(i==(int)(sparse_W[wind])){
      fprintf(stderr,"row %d, nei %d/%d (%d,%d,%g)\n", i, l_count,l_num_nei[i],(int)sparse_W[wind],(int)sparse_W[wind+1],sparse_W[wind+2]);
      wind+=3;
      l_count++;
    }
  }
#endif 
	
  //if(K>D)
  FL tol=params.regular;//e-3; // regularlizer in case constrained fits are ill conditioned
  DBG_(fprintf(stderr,"-->regularization will be used with tol %g\n",tol)); 
	
	
  //Solve Weights Cw = 1 
	
  //D = inv(C+2*eye(sizeK));
	
  /* There is a closed form solution
     % compute lagrange coefficients lambda
	 
     alfa = 1;
     for j=1:K,
     for k=1:K,
     alfa = alfa - D(j,k)*sum(X(:,i).*eta(:,k));
     end
     end     
     %alfa
     beta = sum(sum(D));
	 
     lambda = alfa/beta;
	 
     % compute the weights associated with its neighbors
	 
     for j = 1:K,
     w(i,j) = 0;
     for k = 1:K,
     w(i,j) = w(i,j) + D(j,k) * (sum(X(:,i).*eta(:,k)) + lambda);
     end
     % w goes in the column of W corresponding to the position of the j-th
     % neighbor of X(:,i)
     W(i,ind(j)) = w(i,j);
     end
  */
	
  FL* l_veci = new FL[l_dim];
  FL* l_vecj = new FL[l_dim];
  FL* l_veck = new FL[l_dim];
  for (int indi = 0 ; indi < npoints; indi++){
		
    int l_nn = l_num_nei[indi];

    //Solving reconstruction weights for node indi  with l_nn neighbors		
    if (l_nn == 0){
      PRINT_(fprintf(stderr,"Lonely node %d\n",indi));
      continue;
    }

    //load data[i] 
    FL* datai = &(l_data[indi*l_dim]);
    for (int i=0; i< l_dim; i++)
      l_veci[i] = datai[i];
				
    DBG2_(fprintf(stderr,"Compute covariance\n"));
		
     FL* l_cov = new FL[l_nn*l_nn];
    for (int i = 0; i < l_nn*l_nn; i++)
      l_cov[i]=0.0;
		
    //compute covariance nei*nei'
    //cross multiply all neis of i
		
    //assume sparse_W is ordered		
    //point sparse_M to first element of current row
    FL* spW_ = mat_sparse_W[indi];
		
    //for every neighbor of i
    //point to current row neighbor with windj
    for (int i = 0, windj=0; i < l_nn; i++, windj+=3){
			
      int indj = (int)spW_[windj+1];
			
      //load vector indj from data[j]
      //center in point indi
      FL* dataj = &(l_data[indj*l_dim]);
      for (int j=0; j< l_dim; j++)
	l_vecj[j] = dataj[j] - l_veci[j];
			
#ifdef DEBUG_2
      for (int j = 0; j < l_dim; j++){
	//fprintf(stderr,"%g %g %g\t",l_veci[j],dataj[j],l_vecj[j]);
	fprintf(stderr,"%g\t",l_vecj[j]);
      }
      fprintf(stderr,"\n");
#endif			
      //multiply with other neighbors
      //point to current column neighbor with windk
      for (int j = 0, windk=0; j<l_nn; j++, windk+=3){
	int indk = (int)spW_[windk+1];
				
	//load vector indk from data[k]
	//center in point indi
	FL* datak = &(l_data[indk*l_dim]);
	for (int k=0; k< l_dim; k++)
	  l_veck[k] = datak[k] - l_veci[k];
				
	//multiply data[indj]-data[indi] with data[indk]-data[indi]
	//add resut to l_cov
	int ind = i*l_nn+j;
	for (int k = 0; k < l_dim; k++)
	  l_cov[ind]+=(l_vecj[k])*(l_veck[k]);
      }
    }
    //normalize covariance
    //for (int i = 0; i < l_nn*l_nn; i++)
    //	l_cov[i]/=l_nn;
		
#ifdef DEBUG_3
    fprintf(stderr,"Correlation Matrix\n");
    for (int i = 0; i < l_nn; i++){
      for (int j = 0; j < l_nn; j++){
	fprintf(stderr,"%g\t",l_cov[i*l_nn+j]);
      }
      fprintf(stderr,"\n");
    }
#endif
		
    DBG3_(fprintf(stderr,"Add regularization\n"));						
		
    //add regularization
    //	 C = C + eye(K,K)*tol*trace(C);
    //   C = C + tol*diag(diag(C));                       % regularlization
    //   C = C + eye(K,K)*tol*trace(C)*K;                 % regularlization
    FL trace =0.0;
    for (int i = 0; i<l_nn; i++)
      trace+= l_cov[i*l_nn+i];
		
    for (int i = 0; i<l_nn; i++)
      l_cov[i*l_nn+i]+= tol*trace;
		
#ifdef DEBUG_3		
    fprintf(stderr,"Correlation Matrix+regularization with trace %g\n",trace);
    for (int i = 0; i < l_nn; i++){
      for (int j = 0; j < l_nn; j++){
	fprintf(stderr,"%g\t",l_cov[i*l_nn+j]);
      }
      fprintf(stderr,"\n");
    }
#endif
		
		
    //prepare memory to solve
    CvMat l_cov_ = cvMat(l_nn,l_nn,CV_FL,l_cov);
    FL* l_weights = new FL[l_nn];
    CvMat* l_cov_inv = cvCreateMat(l_nn,l_nn,CV_FL);
#if 1

    double ret = cvInvert(&l_cov_,l_cov_inv,CV_SVD);

#ifdef DEBUG_2
    fprintf(stderr,"Conditioning number %g\n", ret);
#endif
    
#elif
    double ret = cvInvert(&l_cov_,l_cov_inv,CV_LU);
    if (ret==0){ //to use with CV_LU mode
      PRINT_(fprintf(stderr,"Correlation matrix could not be inverted\n"));
    }
    DBG_(fprintf(stderr,"Matrix determinant %g\n", ret));
		
#endif
		
#ifdef DEBUG_3
    fprintf(stderr," (C matrix inverted) \n");
    for (int i = 0; i < l_nn; i++){
      for (int j = 0; j < l_nn; j++){
	fprintf(stderr,"%g\t",l_cov_inv->data.db[i*l_nn+j]);
      }
      fprintf(stderr,"\n");
    }
#endif		
    FL* alpha = new FL[l_nn];	
    FL beta = 0.0;
    spW_ = mat_sparse_W[indi];
    for (int j = 0; j < l_nn; j++){
      alpha[j]=0.0;
      for (int k = 0; k < l_nn; k++){
	alpha[j]+=l_cov_inv->data.db[j*l_nn+k];
      }
      DBG3_(fprintf(stderr,"alpha[%d]:%g\n",j,alpha[j]));
      beta+=alpha[j];
    }
    DBG3_(fprintf(stderr,"beta:%g\n",beta));
		
    assert(finite(beta));
    //FATAL_(fprintf(stderr,"Numerical problem when calculating use a larger propagation(k)\ns"),FATAL_ILL_COND);
		
		
    for (int j = 0; j < l_nn; j++){
      l_weights[j]=alpha[j]/beta;
    }
    if(alpha){
      delete[]alpha;
      alpha=NULL;
    }
    cvReleaseMat(&l_cov_inv);
		
    //save results in sparseM
    DBG3_(fprintf(stderr,"Saving best weights to reconstruct %d in sparse_M\n",indi));								
		
    FL* spM_ = mat_sparse_M[indi];
    spW_ = mat_sparse_W[indi];
		
    for (int i=0, mind=0; i<l_nn; i++,mind+=3){
      DBG3_(fprintf(stderr,"writing result of nei (%d,%d)\n",indi,(int)spW_[mind+1]));								
      if (spW_[mind]!=indi){
	WARNING_(fprintf(stderr,"Error in indexing\n"));
      }
      spM_[mind] = spW_[mind];
      spM_[mind+1] = spW_[mind+1];
      spM_[mind+2] = l_weights[i];//->data.db
      DBG3_(fprintf(stderr,"Covariance result replaced (%d,%d,%g->%g)\n",(int)spM_[mind],(int)spM_[mind+1],spW_[mind+2],spM_[mind+2]));								
    }
		
    if(l_weights){
      delete[]l_weights;
      l_weights=NULL;
    }
    if(l_cov){
      delete[]l_cov;
      l_cov=NULL;
    }
		
#ifdef DEBUG_3
		
    //ERROR in reconstruction
    spW_ = mat_sparse_W[indi];	
    FL* error = new FL[l_dim];
    FL total = 0.0;	
    for(int i = 0; i < l_dim; i++)
      error[i]=0.0;
    for (int i = 0, mind=0; i < l_nn; i++, mind+=3){
      int indj = (int)spM_[mind+1];
      FL weight = spM_[mind+2];
      total = 0.0;
      //contribution of nei[j]
      for (int j=0; j< l_dim; j++){				
	FL val=l_data[indj*l_dim+j];//-l_veci[i*l_dim+j];
	error[j] += weight*val; 
      }
    }
		
    fprintf(stderr,"Reconstruction result of %i (%g %g %g) is (%g %g %g) from %d neighbors\n",
	    indi,l_veci[0],l_veci[1],l_veci[2],
	    error[0],error[1],error[2],l_nn);
#endif
		
#ifdef DEBUG_3
		
    for (int j=0; j< l_dim; j++){
      error[j] -= l_veci[j]; 
      total+= (error[j])*(error[j]);
    }
    total = sqrt(total);
    fprintf(stderr,"\nError reconstructing point %i is (%g, %g, %g)=%g\n",
	    indi,error[0],error[1],error[2],total);
    delete[]error;
    error=NULL;
		
#endif 
  }//end loop for solving weights for each indi
	
	
  if(l_veci){
    delete[]l_veci;
    l_veci=NULL;
  }
  if(l_vecj){
    delete[]l_vecj;
    l_vecj=NULL;
  }
  if(l_veck){
    delete[]l_veck;
    l_veck=NULL;
  }
	
	
	
  DBG_(fprintf(stderr,"(I-W)'(I-W)\n"));
  int l_nzM=0;
  FL* l_temp = new FL[npoints]; // to store temporary line result
  FL** l_results = new FL*[npoints];//temporal storage of result matrix
  int* l_res_count = new int[npoints];
	
  for(int i = 0; i < npoints; i++){				
    FL* rowM = mat_sparse_M[i];
		
    for (int j = 0; j< npoints; j++){
      l_temp[j]= 0.0; //reset value
			
      //just square the values and add the diagonal element 
      if (i==j){
	l_temp[j] += 1.0;
	for (int k = 0, rowk = 0; k < l_num_nei[i]; k++, rowk+=3){
	  l_temp[j] += (rowM[rowk+2])*(rowM[rowk+2]);
	}
      }
      else{
	//go through the elements of line i
	FL* colM = mat_sparse_M[j];
	int colk = 0;
	int max_colk = l_num_nei[j] * 3;
	for (int k = 0, rowk = 0; k < l_num_nei[i]; k++, rowk+=3){
					
	  int row_indk = (int)rowM[rowk+1];
					
	  //consider the one in the diagonal for the column
	  if (row_indk == j){
	    l_temp[j]-= rowM[rowk+2]; // I-W -> multiplying 1 times the -w
	    continue;// the diagonal element (j,j) wasn't explicitely included in mat_sparse_M so don't look for it
	  }
					
	  //find row_indk in column j
	  //asume elements are ordered
	  for(; colk < max_colk; colk+=3){
	    //consider the one in the diagonal for the row
	    int col_indk = (int)colM[colk+1]; 
	    if (col_indk > row_indk) 
	      break;				
						
	    else if (col_indk == i)
	      l_temp[j]-= colM[colk+2];// I-W multiplying 1 times the -w  
						
	    else if (col_indk==row_indk)
	      l_temp[j]+= (rowM[rowk+2])*(colM[colk+2]); //since both are negative dont put the sign here
						
	  }
	}
				
	//if wasn't yet found consider the one in the diagonal for the row
	for ( ; colk < max_colk; colk+=3){
	  int col_indk = (int)colM[colk+1]; 
	  if (col_indk > i) //found already
	    break;				
	  if(i == col_indk){
	    l_temp[j]-= colM[colk+2]; // I-W multiplying 1 times the -w  
	  }
	}
      }
    }
		
    //count number of nonzero elemets in the line
    int l_count=0;
    for(int j =0; j < npoints; j++){
      if (l_temp[j]!= 0.0){
	l_count++;
	l_nzM++;
      }
    }
    DBG2_(fprintf(stderr,"%d non zero elements in result line %d\n",l_count,i));
		
    //allocate appropiate amount of memory per line
		
    l_results[i] =  new FL[l_count*3];
    if (!l_results[i]){
      ERROR_(fprintf(stderr,"could not allocate memory\n"),ERROR_MEMORY_ALLOCATION);
    }
		
    l_res_count[i] = l_count;
		
    DBG3_(fprintf(stderr,"Saving to l_res matrix\n"));
		
    //save temp result in temporal sparse matrix
    FL* l_res_ = l_results[i];
    for (int j = 0, ind=0; j < npoints; j++){
      if (l_temp[j]!=0.0){
	l_res_[ind]=i;
	l_res_[ind+1]=j;
	l_res_[ind+2]=l_temp[j];	
	DBG3_(fprintf(stderr,"result(%d,%d,%g)",(int)l_res_[ind+0],(int)l_res_[ind+1],l_res_[ind+2]));
	//if ((((int)l_res_[ind])>=npoints) || (((int)l_res_[ind+1])>=npoints) || (((int)l_res_[ind])<0) || (((int)l_res_[ind+1])<0)){
	//	PRINT_(fprintf(stderr,"ERROR:(%d,%d,%g)",(int)l_res_[ind+0],(int)l_res_[ind+1],l_res_[ind+2]));
	//	getchar();
	//}
	ind+=3;
      }
    }	
    DBG2_(fprintf(stderr,"Finish calculating line %d of the (M-I)'(W-I) matrix\n",i));
  }
	
#ifdef DEBUG_3
  for (int i = 0; i < npoints; i++){		
    FL* l_res_ = l_results[i];
    DBG_(fprintf(stderr,"result %d\n",i));
    for (int j = 0, ind=0; j < l_res_[i]; j++,ind+=3){
      DBG_(fprintf(stderr,"(%d,%d,%g)",(int)l_res_[ind+0],(int)l_res_[ind+1],l_res_[ind+2]));
      if ((((int)l_res_[ind])>=npoints) || (((int)l_res_[ind+1])>=npoints) || (((int)l_res_[ind])<0) || (((int)l_res_[ind+1])<0)){
	DBG_(fprintf(stderr,"\n\nERROR:(%d,%d,%g)",(int)l_res_[ind+0],(int)l_res_[ind+1],l_res_[ind+2]));
      }
    }
    getchar();
  }
  getchar();
#endif 
	
  DBG_(fprintf(stderr,"Saving to sparse_M matrix (allocation of new sparse_M with %d nonzero values)\n",l_nzM));
	
  nzelems_M = l_nzM;
	
  if (sparse_M) {
    delete[]sparse_M;
    sparse_M = NULL;
  }
  sparse_M = new FL[nzelems_M*3];
	
  if (mat_sparse_M) {
    delete[]mat_sparse_M;
    mat_sparse_M = NULL;
  }
	
  mat_sparse_M = new FL*[nzelems_M];
	
  for (int i = 0; i < nzelems_M*3;i++)
    sparse_M[i] = 0;
	
  //copy result to sparse_M
  //fill D also for eigs verification of disconnected components
  for (int i = 0, mind=0, k=0; i < npoints; i++){
    FL* l_res_ = l_results[i];
    D[i]=0.0;
    for (int j = 0, rind=0; j < l_res_count[i]; j++, rind+=3,mind+=3,k++){
      sparse_M[mind] = l_res_[rind];
      sparse_M[mind+1] = l_res_[rind+1]; 
      sparse_M[mind+2] = l_res_[rind+2];				
      D[i]+=1.0;
      mat_sparse_M[k] = &sparse_M[mind];
      //if ((((int)l_res_[rind])>=npoints) || (((int)l_res_[rind+1])>=npoints) || (((int)l_res_[rind])<0) || (((int)l_res_[rind+1])<0)){
      //	PRINT_(fprintf(stderr,"ERROR(%d,%d)(%d,%d,%g)",i,j,(int)sparse_M[mind+0],(int)sparse_M[mind+1],sparse_M[mind+2]));
      //}
    }
  }
	
#ifdef DEBUG_3
  //verify symmetry
  DBG_(fprintf(stderr,"Verifying symmetry\n"));
  for (int i = 0, mind1=0; i < nzelems_M; i++, mind1+=3){
    fprintf(stderr,"%d\r",i);
    int i1 = (int) sparse_M[mind1];
    int j1 = (int) sparse_M[mind1+1];	
    if (i1==j1)
      continue;
    FL w1 = sparse_M[mind1+2];				
    bool found = false;
    for (int j = 0, mind2 = 0;  j< nzelems_M; j++, mind2+=3){
      int i2 = (int) sparse_M[mind2];
      int j2 = (int) sparse_M[mind2+1];	
      if ((i1==j2)&&(j1==i2)){
	if (fabs(w1-sparse_M[mind2+2])>1e-12){							
	  PRINT_(fprintf(stderr,"Matrix is not symmetric (%d,%d,%g)(%d,%d,%g)",i1,j1,w1,i2,j2,sparse_M[mind2+2]));
	  getchar();
	}
	found= true;
	break;
      }
    }
    if (!found){
      PRINT_(fprintf(stderr,"Matrix is not symmetric, could not find pair for (%d,%d,%g)",i1,j1,w1));
      getchar();
    }
  }
  DBG_(fprintf(stderr,"Symmetry verified\n"));
	
#endif
	
  DBG_(fprintf(stderr,"Free memory\n"));
	
  if (l_temp){
    delete[]l_temp;
    l_temp=NULL;
  }
  if (l_results){
    for (int i = 0; i < npoints; i++)
      delete l_results[i];
    delete[]l_results;
    l_results=NULL;
  }
	
	
  PRINT_COLOR_(fprintf(stderr, "-->LLEMAPS \n"), RED);
  sprintf(right_matrix, "%s", "I");
  strcpy(which, "SA");
	
  if(l_num_nei){
    delete[]l_num_nei;
    l_num_nei=NULL;
  }
	
  PRINT_(fprintf(stderr,"LLE transformation end\n"));
	




  return 0;
	
}

#if 0
int Embed::lle_2 () {

#if 0
  /*
    Input X: D by N matrix consisting of N data items in D dimensions.
    Output Y: d by N matrix consisting of d < D dimensional embedding coordinates for the input points.
  */



  /*    
    1. Find neighbours in X space [b,c].

    for i=1:N
     compute the distance from Xi to every other point Xj
     find the K smallest distances 
     assign the corresponding points to be neighbours of Xi
    end
  */
  // Sparse_W store these neighbors.

  /*
    2. Solve for reconstruction weights W.

      for i=1:N
        create matrix Z consisting of all neighbours of Xi [d]
        subtract Xi from every column of Z
        compute the local covariance C=Z'*Z [e]
        solve linear system C*w = 1 for w [f]
        set Wij=0 if j is not a neighbor of i
        set the remaining elements in the ith row of W equal to w/sum(w);
      end
  */

  /*
    3. Compute embedding coordinates Y using weights W.

      create sparse matrix M = (I-W)'*(I-W)
      find bottom d+1 eigenvectors of M
        (corresponding to the d+1 smallest eigenvalues) 
      set the qth ROW of Y to be the q+1 smallest eigenvector
        (discard the bottom eigenvector [1,1,1,1...] with eigenvalue zero)
  */
#endif
  if(!sparse_W){
    ERROR_(fprintf
	   (stderr, "Empty sparse distance matrix.Can not perform an LLE embedding (aborted)\n"),
	   ERROR_EMPTY_DATA);
  }

  // Load the adjency matrix.
  FL* l_3D_data = NULL;
  int l_dim = 0;
  if (!Points){
    l_3D_data = C_Mat_dist.getData();
    l_dim = C_Mat_dist.getDimData();

  }
  else{ // for meshes point should have been loaded outside
    l_3D_data = Points;
    l_dim = 3;
  }
	
  if(!l_data){
    FATAL_(fprintf
	   (stderr, "Empty data. Can not perform an LLE embedding (aborted)\n"),
	   FATAL_EMPTY_DATA);
  }

  PRINT_(fprintf(stderr,"-->LLE embedding on %d points \n",npoints));
	
  if (sparse_M) {
    delete[]sparse_M;
    sparse_M = NULL;
  }
  if (mat_sparse_M) {
    delete[]mat_sparse_M;
    mat_sparse_M = NULL;
  }
	
  if (D)
    delete[]D;
  D = new FL[npoints];
  memset(D, 0, npoints * sizeof(FL));
	
  nzelems_M = nzelems_W;
  sparse_M = new FL[nzelems_M*3];
  mat_sparse_M = new FL*[npoints];



  FL *l_sparse_W_ = sparse_W;
  hash_map < std::pair<int,int>, FL, hash_func_pair_int_int > hash_map_LLE ;
  for (int i = 0; i < nzelems_W; i++, l_sparse_W_ += 3) {
    hash_map_LLE.insert ( std::make_pair(std::make_pair((int)l_sparse_W_[0], 
							(int)l_sparse_W_[1]), 
					 l_sparse_W_[2])) ;
  }
  FL tol=params.regular;//e-3; // regularlizer in case constrained fits are ill conditioned
  DBG_(fprintf(stderr,"-->regularization will be used with tol %g\n",tol));



}
#endif

int Embed::isomap()
{
  PRINT_(fprintf(stderr, "-->ISOMAP running on %d points \n", npoints));
	
  int l_retval = 0;
	
  //prepare diagonal degree matrix 
  if (D)
    delete[]D;
  D = new FL[npoints];
  memset(D, 0, npoints * sizeof(FL));
	
  // Isomap on local geodesics or on full geodesics
  if (sparse_W) {
    l_retval = sparseIsomap();
  }
  else if (full_W) {
    l_retval = fullIsomap();
  }
  else {
    ERROR_(fprintf
	   (stderr, "Neither Ws or Wf exist. Isomap aborted \n"),
	   ERROR_EMPTY_DATA);
  }
	
  return l_retval;
}

int Embed::sparseIsomap()
{
  PRINT_(fprintf(stderr, "-->sparse ISOMAP under construction \n"));
  return 0;
}

int Embed::fullIsomap()
{
	
  PRINT_(fprintf(stderr, "-->full ISOMAP  \n"));
	
  if (full_M) {
    delete[]full_M;
    full_M = NULL;
  }
  if (mat_full_M) {
    delete[]mat_full_M;
    mat_full_M = NULL;
  }
    
  PRINT_(fprintf(stderr, "-->Fill matrices for eigendecomposition \n"));
	
  //Prepare M
  nzelems_M = npoints*npoints;
  DBG_(fprintf(stderr,"nzelems_M %d\n",nzelems_M));

  //Since matrices can be very big use W as M
  //Allocating this amount of memory again may lead to an allocation error
  //full_M = new FL[nzelems_M];
  full_M = full_W;
  full_W = NULL;

  modify_M = false ;

	
  if (!full_M ){
    WARNING_(fprintf(stderr, "Embed::fullIsomap()::ERROR\n"));
    WARNING_(fprintf(stderr, "Not enough memory to allocate full_M\n"));
    FATAL_(fprintf(stderr, "Can not allocate memory for full matrix in isomap"),FATAL_MEMORY_ALLOCATION);
  }	

  //bzero(full_M,nzelems_M*sizeof(FL));
  //mat_full_M = allocateDMat(full_M,npoints,npoints);
  mat_full_M = mat_full_W;
  mat_full_W = NULL;
	
  DBG_(fprintf(stderr,"Squaring values and calculating means \n"));
  //square the values of full W while copying to sparse_M
  //calculate mean, rowmean, colmean values
  FULL_MAT_TYPE l_mean = 0.0 ;
  FULL_MAT_TYPE* l_rowmean = new FULL_MAT_TYPE[npoints];
  FULL_MAT_TYPE* l_colmean = new FULL_MAT_TYPE[npoints];

  for (int i = 0; i< npoints; i++){
    l_rowmean[i]=0.0;
    l_colmean[i]=0.0;
  }
  
  float symm_error = 0.0;

  for (int i = 0; i< npoints; i++){
    int ind = i*npoints;
    for (int j = 0; j < npoints; j++, ind++){
      float val = (full_M[ind])*(full_M[ind]);
      full_M[ind] = val;
      l_mean += val;
      l_rowmean[i] += val;
      l_colmean[j] += val;
      
      
#ifdef DEBUG
      if (i>j){
	symm_error+=fabs(full_M[ind]-full_M[j*npoints+i]);
	if (fabs(full_M[ind]-full_M[j*npoints+i])>0.0){
	  fprintf(stderr,"full_M[%d,%d]=%g - full_M[%d,%d]=%g\n",i,j,full_M[ind],j,i,full_M[j*npoints+i]);
	  getchar();
	}
      }
#endif       
    }
  }
  DBG_(fprintf(stderr,"Error in symmetry: %g\n",symm_error));
  DBG_(fprintf(stderr,"Normalizing means \n"));
  
  //normalize means
  //in standard conditions l_rowmean should be equal to l_colmean
  l_mean = l_mean/(npoints*npoints);
  FULL_MAT_TYPE sum=0;
  for (int i = 0; i < npoints; i++){
    l_rowmean[i]=l_rowmean[i]/npoints;
    l_colmean[i]=l_colmean[i]/npoints;
    sum+=(l_rowmean[i]-l_colmean[i]);
  }
  DBG_(fprintf(stderr,"Error in mean calculation %g\n",sum));

  DBG_(fprintf(stderr,"Making matrix related to a dot prodcut\n"));
	
  //The matrix to eigendecompose is equal to
  //(-1/2)( D - rowmean*ones(1,npoints) - ones(npoints,1)*colmean + mean)
  //Considering D is the squared matrix 
  //assuming rowmean is a column vector
  //and colmean is a row vector
  //rowmean*ones(1,npoints) repeats the values of rowmean for each col of D
  //ones(npoints,1)*colmean repeats the values of rowcol for each row of D

  symm_error = 0.0;
  for (int i = 0; i< npoints; i++){
    int ind = i*npoints;
    for (int j = 0; j < npoints; j++, ind++){
      full_M[ind] = full_M[ind] - l_rowmean[i] - l_colmean[j] + l_mean;
      full_M[ind] = (-0.5)*(full_M[ind]);
      assert(finite(full_M[ind]));
      //DBG_(fprintf(stderr,"(%d,%d:%g)",i,j,full_M[ind]));
#ifdef DEBUG
      if (i>j){
	symm_error+=fabs(full_M[ind]-full_M[j*npoints+i]);
	if (fabs(full_M[ind]-full_M[j*npoints+i])>1e-3){
	  DBG3_(fprintf(stderr,"full_M[%d,%d]=%g - full_M[%d,%d]=%g\n",i,j,full_M[ind],j,i,full_M[j*npoints+i]));
	}
      }
#endif
    }
  }
  DBG_(fprintf(stderr,"Error in symmetry: %g\n",symm_error));

	
  PRINT_(fprintf(stderr, "-->Isomap matrix ready for eigendecomposition \n"));
  if(l_rowmean)
    delete[]l_rowmean;
  if(l_colmean)
    delete[]l_colmean;
  return 0;
}




int Embed::loadPoints(FL* p_points){
  Points = p_points; 
  DBG_(std::cout<<"Loading points from" << p_points <<std::endl);
  return 0;
}

/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
/*
 *  mesh.h
 *  SpecMatch
 *
 *  Created by Diana Mateus and David Knossow on 8/16/07.
 *  Copyright 2007 INRIA. All rights reserved.
 *
 * This class provides a container for meshes. It contains the 3D coordinates of vertices, the facets and their associated data. 
 * It provides functions to read from file, access data and create OpenGL drawlists.
 * This class derives from ShapeData that is a low level class. This latter class is common for voxels and meshes.
 *
 */

#ifndef MESH_H
#define MESH_H

#include <iostream>
#include <fstream>

#include <ext/hash_map>
#include <deque>
#include "types.h"
#include "ShapeData.h"


// Structure to store the data to build the neis of a mesh
typedef struct {
  bool exists ;  
  double dist ;
  double prev_dist ;
  int depth ;
} MapMesh ;



class Mesh:public ShapeData {
  private:
    // File content container. Allows to parse files.
    std::ifstream in;


    // Normalize or not the mesh. 
    bool center_mesh;


    // Number of vertices in the mesh.
    int nvertex;

    // Number of facets in the mesh.
    int nfacets;

    // Number of edges in the mesh (comes from the OFF file format.
    int nedges;

    // Array of number of vertex for each facet.
    int *nvert_facet;

    // 3D coordinates of all the vertices of the mesh.
    double *vertex;

    // 3D coordinates of all the normals at each vertex of the mesh.
    double *vertex_normal;

    // Contains the indices of points belonging to the same facet. 
    // Size is: MAX_NUM_VERT_PER_FACETS * nfaces.
    // To access a facet: facet_index * MAX_NUM_VERT_PER_FACETS.
    int max_num_vert_per_facets ;
    int max_num_facets_per_vertex ;
    int *facets;

    // 3D coordinates of the normal of each facet.
    double *facet_normal;

    // Stores for each vertex, the facet indices it belongs to. 
    // Size is: MAX_NUM_FACETS_PER_VERTEX * nvertex.
    int *vertex_to_facets;

    // Stores for each vertex, the number of facets it belongs to. 
    int *nfacets_per_vertex;

    // if a sort of the mesh is required, it sort the re-ordering of indexes.
    int *revert_index_sort_mesh ;

    /**********************************************/
    /* Display                                    */
    /**********************************************/
    // Array of color to display the r-rings. It shows the connectivity.
    float *NEI_colormap;

    // OpenGL list of points to display the r-rings (the connectivity).
    GLuint MESHNEISET;

    // Stores the color of each vertex.     
    float *vertex_color;

    //Enable/Disable the sort of meshes
    bool enable_sort_mesh ;

    /**********************************************/
    /* Distance                                   */
    /**********************************************/
    // Enable using the "r"-ring as the distance. If false, the distance can be either the Normalized Euclidian distance or the Cotan weight distance.
    bool use_depth_as_dist;

    // Type of distance to compute between the nodes of the graph, in case of not using depth as distance.
    GRAPH_DISTANCE_TYPE graph_distance_type ;

    // Store an approximation of the voronoi area of each vertex of the mesh. ADD A REFERENCE HERE.
    FL *approx_voronoi_area;
    

    // Map to "efficiently" store the connectivity on the mesh.
    // map to "efficiently" store the local geodesic distance on the mesh.
    // map to "efficiently" store the length (in terms of number of vertices) on the mesh.
    std::vector< hash_map < int, MapMesh > > hash_map_vector ;

    // Stores for each vertex the total number of neighbors (all connected neighbors at a given depth or r-ring).
    int *total_num_nei;



    // Stores the local geodesic distance on the mesh between two points that ar connected as an array. 
    FL *weights;

    // Stores the local geodesic distance as a Matrix using the weight array.
    FL **mat_weights;

    // Store the depths of vertice. Arranged in lines. (i,j,depth) depth is between i and j. 
    int *vec_depth;

    // Store the depths of vertice. As a matrix using vec_depth. 
    int **mat_depth;


    /**********************************************/
    /* load functions. Names are obvious here.    */
    /**********************************************/

    // Reads off file format (only the 3D coordinates and the facets).
    void readOff();

    // Reads off file format (the 3D coordinates + facet normal coordinates and the facets).
    void readNOff();

    // Reads off file format (the 3D coordinates + facet colors and the facets).
    void readCOff();

    // Reads the facets in the off, noff, and coff file formats.
    void readFacets();

    // Compute the facet normal for all facets. Call one_normal function.
    int fillFacetNormals();

    // Compute the facet normal for one facet
    // Parameters are:
    // - v0, v1, v2 : indices of the three points from which to compute the normal
    // - normal: the coordinates of the computed normal, normalized (norm(normal)= 1).
    int computeOneNormal(int v0, int v1, int v2, double *normal);


    // Compute the closest neighbors for each vertex (1-ring, or depth =1) as a HashTable
    void computeNeis();

    // Find the center of mass (each vertex has a mass of 1) and the scale of the mesh.
    void findMeanAndScale(bool center = false);

    // From the Hash Table created in ComputeNeis() create an array (mesh_nei).
    int buildMeshNei();

    // Sort the weight matrix. The indices on each row are ordered.
    void sortMatWeight();

    // The eigenfunction decomposition works with symmetric matrices. We symmeterize the matrix. This one may not be really symmetric due to numerical errors when adding distances in the iterative process.
    void symmeterizeMatWeight();

  public: Mesh();
    ~Mesh();

    // Reads OFF, NOFF, COFF files, Compute normals and 1-ring connectivity.
    int readOffFile(char *offFileName);

    // Returns the numbers of vertices in the mesh.
    int getNumOfVertex();

    // Returns the numbers of facets in the mesh.
    int getNumOfFacets();

    // Build a colormap to display the different connectivity rings in the mesh.
    void buildNeiColormap(int depth);

    // Enable the use of "r"-ring as the distance.
    void setDepthAsDist(bool status);

    // Calls build_pointset_list from ShapeData.h
    int buildMeshsetList();

    // Reorganize the mesh so that the points are sorted along the Y coordinates. 
    // It allows for a continuous coloring in case of disorganized meshes. 
    void sortMeshY ()  ;
    
    // Enable or disable the sort of points on the mesh.
    void enableSortMesh (bool status) ;
    
    // Returns the index sorted.
    int * getIndexSorting ( ) ;

    // Calls build_pointset_list from ShapeData.h
    int buildMeshsetPointList();

    // Build the OpenGL list of connectivity to be displayed.
    int buildMeshsetNeiList();

    // Draws the mesh vertices
    int drawMeshsetPoint();

    // Draws the mesh vertices
    int drawMeshsetList();

    // Draws the mesh facets
    int drawMeshsetFacets(bool drawEdges = false);

    // Draws the mesh connectivity.
    int drawMeshsetNei();

    // Return a pointer on total_num_nei.
    int *getNumOfNeik();

    // Build recursively the distance matrix from mesh_nei for a given "max_depth"-ring(build in build_mesh_nei). It fills up a HashTable
    void buildDistMat(int max_depth);

    // From the hashtable containing the distances, it creates a matrix.
    int fillSparseWeight();

    // From the hashtable containing the depths, it creates a matrix.
    int fillSparseDepth();


    // Returns a pointer on the 3D coordinates of the vertices of the mesh.
    FL *getVertices();

    // Returns a pointer on weights
    FL *getFLSparseWeight();

    // Returns a pointer on matweights
    FL **getFLSparseMatWeight();

    // Returns a pointer on matdepth
    int **getSparseMatDepth() ;

    // Returns a pointer on approx_voronoi_area
    FL *getVoronoiAreas();

    // compute the euclidian distance between two vertices and normalize this distance with the approximation of the voronoi area.
    void computeNormalizedEuclidianWeight(bool on);

    // Compute an approximation of the voronoi area for each vertex of the mesh.
    void computeNormalizationWeight();

    // Compute the Cotan weight for each vertex. NOT THOOROUGHLY TESTED YET.
    void computeCotanWeight();

    // Provided for convenience. Allows to check if SymmeterizeMatWeight () performed correctly.
    void checkSymmeterizeMatWeight();

    // Set the type of distance to compute between the nodes of the graph, in case of not using depth as distance.
    void setGraphDistanceType(GRAPH_DISTANCE_TYPE p_graph_dist) ;




};

#endif

/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
/*
 *  main-vizu.cpp
 *  
 *
 *  Created by Diana Mateus and David Knossow on 11/5/07.
 *  Copyright 2007 INRIA. All rights reserved.
 *
 */
#include <getopt.h>
#include <stdio.h>
#include <iostream>
#include <qapplication.h>


#ifdef MACOSX
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#ifndef QT_VERSION
#include <qglobal.h>
#endif

#include "legal.h"
#include "CommandOpt.h"
#include "mesh.h"
#include "voxel.h"
#include "visual_shape.h"
#include "embedding.h"		//class used for reading and accessing to embeddings
#include "match_em.h"
#include "visual_em.h"
#include "constantReader.h"

using namespace std;


#define EM_M ISO_ANNEAL
#define INIT_M UNIFORM

hash_map < std::string , Variant, hash_func > hash_map_global_params ;


void printHelp()
{

    fprintf(stderr, "Need some arguments"
	    "\n Usage : match \n"
	    "\t [-v1] voxelset1 \n"
	    "\t [-v2] voxelset2 \n"
	    "\t [-m1] meshset1 \n"
	    "\t [-m2] meshset2 \n"
	    "\t [-g1] meshset1 \n"
	    "\t [-g2] meshset2 \n"
	    "\t [-e1] embedfile1 \n"
	    "\t [-e2] embedfile2 \n"
	    "\t [-remove-first] remove first dimension of embeddings (0 or 1) \n"
	    "\t [-f1] vox_and_embed file1 \n"
	    "\t [-f2] vox_and_embed file2 \n"
	    "\t [-dim] embedding dim \n"
	    "\t [-b] binary match file \n"
	    "\t [-p] proba match file\n"
	    "\t [-o] template file name for all temporary outputs\n"
	    "\t [-d] display intermediate steps\n"
	    "\t [-l] labels_for synthetic data (two files)\n"
	    "\t [-t] transfo file applied to embeddings\n"
	    "\t [-match-seq] use names as patterns e.g match%%s_%%d to show match evolution \n"
	    "\t [-frame-seq] use names as patterns e.g match%%s_%%d to show a sequence of matches\n"
	    "\t [-s] start frame/match (default 0)\n"
	    "\t [-d] delta between each frame/match (default 1)\n"
	    "\t [-n] max number of consecutive matches\n"
	    "\t [-xmin] [-xmax] [-ymin] [-ymax] [-zmin] [-zmax] bounding box to display the matches\n"
	    "\t [-x] [-y] [-z] set the translation of a shape, for display purpose\n"
	    "\t [--rx] [--ry] [--rz] set the rotation of a shape, for display purpose\n"
	    "\t [--sampling] set the sampling rate to visualize the line matches. \n"
	    "\t [--sort] sort the mesh values for coloring purposes (when dislpaying)\n");
    exit(-1);

}

int main(int argc, char **argv)
{
    // Read command lines arguments.
    QApplication application(argc, argv);

    PRINT_COLOR(printIntroLegal(), BLACK) ;
    readConstantFileSettings ("CONSTANT.xml") ;


#ifndef MACOSX
    glutInit(&argc, argv);
#endif

    // Instantiate the viewer.
    bool live = false;
    bool meshset_loaded = false;
    bool voxelset_loaded = false;
    bool embedset_loaded = false;
        bool sort_meshes = false ;
    int emb_dim = -1 ;
    int count = argc;
    char *embed1 = NULL;
    char *embed2 = NULL;
    char *voxel1 = NULL;
    char *voxel2 = NULL;
        char *mesh1 = NULL;
    char *mesh2 = NULL;
    char *mesh3 = NULL;
    char *mesh4 = NULL;
    char *voxembed1 = NULL;
    char *voxembed2 = NULL;

    char *bin_match = NULL;
    char *proba_match = NULL;
    char *transfo_file = NULL;

    
    char *labels1 = NULL;
    char *labels2 = NULL;

    double xmin = -1, xmax = 1, ymin = -1, ymax = 1, zmin = -1, zmax = 1;

    bool remove_first = false;	//embeddings

    int num_match = 1;
    int start_match = 0;
    int delta_match = 1;
    bool match_seq = false;
    bool frame_seq = false;
    float rotate_x = 0 ;
    float rotate_y = 0 ;
    float rotate_z = 0 ;
    float trans_x = 0.5 ;
    float trans_y = 0.5 ;
    float trans_z = 0.5 ;

    NORMALIZATION_EMBEDDING normalize_embed = NO_NORM_EMBED ;
    int sampling_rate = 0 ;
    HASH_MAP_PARAM_(int, "vizu_sample", Int, sampling_rate) ;

    static struct option long_options[] = {
	/* Option long name, Has an argument or not , returns a value or not,
	   if not returns the id (here 260, must be different for each long name
	   and different than ASCII code corresponding to single characters). */
	{"e1", 1, 0, EMBED1},
	{"e2", 1, 0, EMBED2},
	{"remove-first", 1, 0, REMOVE_FIRST},
	{"normalize-embed", 1, 0, NORMALIZE_EMBED},
	{"m1", 1, 0, MESH1},
	{"m2", 1, 0, MESH2},
	{"m3", 1, 0, MESH3},
	{"m4", 1, 0, MESH4},
	{"v1", 1, 0, VOXEL1},
	{"v2", 1, 0, VOXEL2},
		{"f1", 1, 0, VOXEMBED1},
	{"f2", 1, 0, VOXEMBED2},
	{"dim", 1, 0, DIM},
	{"xmin", 1, 0, XMIN},
	{"xmax", 1, 0, XMAX},
	{"ymin", 1, 0, YMIN},
	{"ymax", 1, 0, YMAX},
	{"zmin", 1, 0, ZMIN},
	{"zmax", 1, 0, ZMAX},
	{"rx", 1, 0, RX},
	{"ry", 1, 0, RY},
	{"rz", 1, 0, RZ},
	{"live", 0, 0, LIVE},
	{"match-seq", 0, 0, MATCH_SEQ},
	{"frame-seq", 0, 0, FRAME_SEQ},
	{"sampling", 1, 0, VIZU_SAMPLING},
	{"sort", 0, 0, SORTMESH},
		{0, 0, 0, 0}
    };

    if (count == 1) {
	printHelp();
    }


    int opt = 0;
    int nbOpt = 0;

    int option_index = 0;
    do {
	opt = getopt_long(argc, argv, "b:d:l:n:p:s:t:x:y:z:",
			  long_options, &option_index);
	if (opt != -1) {
	    nbOpt++;
	    switch (opt) {
	    case EMBED1:
		embed1 = strdup(optarg);
		embedset_loaded = true;
		break;		// e1
	    case EMBED2:
		embed2 = strdup(optarg);
		break;		// e2                  
	    case MESH1:
		mesh1 = strdup(optarg);
		meshset_loaded = true;
		break;		// m1
	    case MESH2:
		mesh2 = strdup(optarg);
		break;		// m2                  
	    case MESH3:
		mesh3 = strdup(optarg);
		meshset_loaded = true;
		break;		// m1
	    case MESH4:
		mesh4 = strdup(optarg);
		break;		// m2                  	
	    case VOXEL1:
		voxel1 = strdup(optarg);
		voxelset_loaded = true;
		break;		// v1
	    case VOXEL2:
		voxel2 = strdup(optarg);
		break;		// v2
			    case VOXEMBED1:
		voxembed1 = strdup(optarg);
		break;		//v1 and e1
	    case VOXEMBED2:
		voxembed2 = strdup(optarg);
		break;		//v2 and e2
	    case REMOVE_FIRST:
		if (atoi(optarg)) 
		    remove_first = true;
		else 
		    remove_first = false;
		break;
		break ;
	    case DIM:
		emb_dim = atoi(optarg);
		break;		// dim

		
	    case XMIN:
		xmin = atoi(optarg);
		break;		//xmin
	    case XMAX:
		xmax = atoi(optarg);
		break;		//xmax
	    case YMIN:
		ymin = atoi(optarg);
		break;		//ymin
	    case YMAX:
		ymax = atoi(optarg);
		break;		//ymax
	    case ZMIN:
		zmin = atoi(optarg);
		break;		//zmin
	    case ZMAX:
		zmax = atoi(optarg);
		break;		//zmax
	    case LIVE:
		live = true;
		break;
	    case MATCH_SEQ:
		match_seq = true;
		break;
	    case FRAME_SEQ:
		frame_seq = true;
		break;
	    case RX:
		rotate_x = atof(optarg);
		break ;
	    case RY:
		rotate_y = atof(optarg);
		break ;
	    case RZ:
		rotate_z = atof(optarg);
		break ;
	    case VIZU_SAMPLING:
		sampling_rate = atoi(optarg) ;
		break ;
	    case SORTMESH:
		sort_meshes = true ;
		break ;
	    case 'b':
		bin_match = strdup(optarg);
		break;
	    case 'p':
		proba_match = strdup(optarg);
		break;
	    case 'l':
		labels1 = strdup(optarg);
		break;
	    case 't':
		transfo_file = strdup(optarg);
		break;
	    case 'n':
		num_match = atoi(optarg);
		break;
	    case 's':
		start_match = atoi(optarg);
		break;
	    case 'd':
		delta_match = atoi(optarg);
		break;
	    case 'x':
		trans_x = atof(optarg);
		break ;
	    case 'y':
		trans_y = atof(optarg);
		break ;
	    case 'z':
		trans_z = atof(optarg);
		break ;
	    case NORMALIZE_EMBED:
		normalize_embed = (NORMALIZATION_EMBEDDING) atoi (optarg);
		break;

	    default:
		printHelp();
		break;
	    }
	}
    } while (opt != -1);


    PRINT_COLOR_(cout << "Type H to display help" << endl, GREEN);
    

    VisualShape shape;

    // READ 
    int load_result = 0;

    if ( sort_meshes ) {
	if ( shape.C_Mesh1 ) 
	    shape.C_Mesh1->enableSortMesh (true) ;
	if ( shape.C_Mesh2 ) 
	    shape.C_Mesh2->enableSortMesh (true) ;
    }



    if (!frame_seq) {

	//Read data
	if (voxel1)
	    load_result += shape.C_Voxel1->readPoints(voxel1);
	if (voxel2)
	    load_result += shape.C_Voxel2->readPoints(voxel2);
	if (mesh1) {
	    load_result += shape.C_Mesh1->readOffFile(mesh1);
	}
	if (mesh2) {
	    load_result += shape.C_Mesh2->readOffFile(mesh2);
	}
	if (mesh3) {
	    shape.save_on = true ;
	    load_result += shape.C_Mesh3->readOffFile(mesh3);
	}
	if (mesh4) {
	    load_result += shape.C_Mesh4->readOffFile(mesh4);
	}
	if (embed1 && remove_first && emb_dim <= 3) {
	    WARNING_(fprintf(stderr, "Remove first is enabled, the dimension should be set to at least 4 using option [--dim]. Default is 4.\n")) ;
	    emb_dim = 4 ;
	}
	if (embed1 && emb_dim == -1) {
	    WARNING_(fprintf(stderr, "You can set the emdedding dimension with [--dim]. Default is 3.\n")) ;	   
	    emb_dim = 3 ;
	    if (remove_first)
		emb_dim = 4 ;
	}

	if (embed1) {
	    load_result +=
		shape.C_Embed1->readEmbedding(embed1, emb_dim,
					      remove_first);
	    shape.C_Embed1->normalizeEmbedding(normalize_embed);
	}
	if (embed2) {
	    load_result +=
		shape.C_Embed2->readEmbedding(embed2, emb_dim,
					      remove_first);
	    shape.C_Embed2->normalizeEmbedding(normalize_embed);
	}
	
	if (voxembed1) {
	    load_result +=
		shape.C_Voxel1->readVoxelsAndFunctionValue(voxembed1);
	    load_result +=
		shape.C_Embed1->loadEmbedding(shape.C_Voxel1->
					      getFunctionValue(),
					      shape.C_Voxel1->
					      getNumOfVoxels(), emb_dim,
					      shape.C_Voxel1->
					      getNumOfFunctions());

	}


	if (voxembed2) {
	    load_result +=
		shape.C_Voxel2->readVoxelsAndFunctionValue(voxembed2);
	    load_result +=
		shape.C_Embed2->loadEmbedding(shape.C_Voxel2->
					      getFunctionValue(),
					      shape.C_Voxel2->
					      getNumOfVoxels(), emb_dim,
					      shape.C_Voxel1->
					      getNumOfFunctions());


	}
	//Read match
	if (!match_seq) {
	    if (bin_match)
		load_result += shape.readBinaryMatch(bin_match);
	    if (proba_match)
		load_result += shape.readProbabilisticMatch(proba_match);
	}
    }
    if (bin_match && sort_meshes) {
	shape.enableReshapeBinaryMatch(true) ;
	shape.reshapeBinaryMatch ( shape.C_Mesh1->getIndexSorting (), shape.C_Mesh2->getIndexSorting () ) ;
    }
    //Read labels
    shape.setModelSep (trans_x, trans_y, trans_z) ;
    shape.setModelRot (rotate_x, rotate_y, rotate_z) ;



    shape.setVizuSampleRate (sampling_rate) ;


    
    if ((labels1) && (labels2)) {
	load_result += shape.C_Voxel1->readLabelValue(labels1);
	load_result += shape.C_Voxel1->readLabelValue(labels2);
    }
    //Read transfo
    int dim = 0;
    if (shape.C_Voxel1->isLoaded() && shape.C_Voxel2->isLoaded())
	dim = shape.C_Voxel1->getDim();
    if (shape.C_Mesh1->isLoaded() && shape.C_Mesh2->isLoaded())
	dim = shape.C_Mesh1->getDim();
    
    if (shape.C_Embed1->isLoaded() && shape.C_Embed2->isLoaded())
	dim = shape.C_Embed1->getDim();

    if (transfo_file)
	load_result += shape.readTransfo(transfo_file, dim, dim);

    if (load_result) {
	ERROR_(fprintf(stderr, "Problem while loading files \n"),
	       ERROR_VARIABLE_INIT);
    }

    int l_sample = 0 ;
    HASH_MAP_PARAM_(int, "sample", Int, l_sample) ;

    if (!frame_seq && !match_seq && (!bin_match || proba_match)) {
	if (voxelset_loaded)
	    shape.buildBinaryVoxelMatchList(l_sample);
	if (meshset_loaded)
	    shape.buildBinaryMeshMatchList(l_sample);
	if (embedset_loaded)
	    shape.buildBinaryEmbedMatchList(l_sample);
	
    }

    shape.setMinMaxValuesForDisplay(xmin, xmax, ymin, ymax, zmin, zmax);
    shape.initColorsAndShapes();



    // FRAME SEQUENCE

    if (frame_seq) {
	PRINT_COLOR_(std::cout << "Sequence of Frames " << std::endl, RED);
	if (bin_match)
	    shape.setMatchfilePattern(bin_match);
	if (voxel1)
	    shape.setVoxelPattern(voxel1);
	if (mesh1)
	    shape.setMeshPattern(mesh1);
	if (embed1)
	    shape.setEmbedPattern(embed1);
		shape.setSequenceLimits(start_match - 1, num_match, delta_match);
    }
    // MATCH SEQUENCE
    if (match_seq) {
	shape.setMatchfilePattern(bin_match);
	shape.setSequenceLimits(start_match, num_match, delta_match);
	shape.updateMatchFiles(1) ;

    }
    // DISPLAY

    DBG_(fprintf(stderr, "-->showing match\n"));
#if QT_VERSION < 0x040000
    application.setMainWidget(&shape);
#else
    shape.setWindowTitle("shapeViewer");
#endif
    shape.show();

    // Run main loop.   

    application.exec();

    if (bin_match)
	free(bin_match);
    if (proba_match)
	free(proba_match);
    if (labels1)
	free(labels1);
    if (transfo_file)
	free(transfo_file);

    if (embed1)
	free(embed1);
    if (embed2)
	free(embed2);
        if (mesh1)
	free(mesh1);
    if (mesh2)
	free(mesh2);
    if (mesh3)
	free(mesh3);
    if (mesh4)
	free(mesh4);
    if (voxembed2)
	free(voxembed2);
    if (voxembed1)
	free(voxembed1);
    if (voxel2)
	free(voxel2);
    if (voxel1)
	free(voxel1);


    exit(0);

}

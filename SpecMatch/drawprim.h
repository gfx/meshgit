/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
/*
 *  drawprim.h
 *  SpecMatch
 *
 *  Created by Diana Mateus on 9/30/07.
 *  Copyright 2007 INRIA. All rights reserved.
 *
 * This file contains some basic functions to draw basic primitives.
 *
 */

#ifndef DRAWPRIM_H
#define DRAWPRIM_H


#ifdef MACOSX
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

int drawCube(float x, float y, float z, float cube_size);
int drawSphere(float p_x, float p_y, float p_z, float p_sphere_size) ;
#endif

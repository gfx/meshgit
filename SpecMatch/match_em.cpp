/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
/*
 *  match_em.cpp
 *  
 *
 *  Created by Diana Mateus and David Knossow on 9/23/07.
 *  Copyright 2007 INRIA. All rights reserved.
 *
 */

#include <cmath>
#include <stdio.h>
#include <math.h>
#include <cv.h>
#include <highgui.h>



#include <iostream>

#ifdef ENABLE_EM_PARALLELS
#include <omp.h>
#endif

#include "match_em.h"

#include "timer.h"

using namespace std;


//
// d1,d2 (input 3d data points) for visualization purposes only
// p1 and p2 (input in embedded space) They correspond to (Y,X) respectively
// Transform should be applied to Y(p1);
// matching should be used p2->p1(matching),
// alpha = ni x nj
// nj = size(Y,1);
// ni = size(X,1);// 
//             p1: Y (nj)
//        -------------------
//       |                   |
//       |                   |
//       |                   |
// p2:X  |                   |
// (ni)  |      alpha        | -->part(i)
//       |                   |
//       |                   |
//       |         |         |
//       |         v         |
//        -------------------
//               beta(j)
//
// X are treated as observations and Y as class centers
// Several points of X are assembled into a baricentric value to estimate their
// contribution to a given Y, then |X|>|Y|


//void cvEigenVV( CvArr* mat, CvArr* evects, CvArr* evals, FL eps=0 );
//void cvCalcCovarMatrix( const CvArr** vects, int count, CvArr* cov_mat, CvArr* avg, int flags );




Match::Match()
{

    use_alpha_line = false ;

    data_dim = 0;
    nX = 0;
    nY = 0;
    X = NULL;
    Y = NULL;
    data_mask_X = NULL;
    data_mask_Y = NULL;

    init_dim = 0;

    str_length = -1 ;
    gt_level = -1 ;

    //Pointers
    covmat = NULL;
    inv_covmat = NULL;
    T = NULL;
    lastT = NULL;
    initT = NULL;
    mat_alpha = NULL ;
    vec_alpha = NULL ;
    part = NULL;
    beta = NULL;

    W = NULL;
    Yt = NULL;
    mask_X = NULL;
    mask_Y = NULL;
    ag_match = NULL;

    n_outliers = NULL;
    n_unmatched = NULL;
    nag_match = NULL;
    error = NULL;
    changeT = NULL;

    //Init parameters

    max_iter = 0;
    min_sigma = 0;
    min_error = 0.0;
    min_changeT = 0.0;
    xy_inversed = false;
    n_max_match = 0;
    match_available = false;

    mean_cluster_dist = 0 ;
	
    //histograms
    discriminant_ratio = -1;
    histogram_distance = BINTOBIN;
    assignment_method = SUBOPTIMAL1;
    vec_hist_X = NULL;
    vec_hist_Y = NULL;
    vec_hist_FY = NULL;

    //nei filenames
    connect1 = NULL;
    connect2 = NULL;

    // Given the selection of best associated observations to the classes 
    // (multiple obs per class), select the best class for each observation 
    // (leads to a one to one matching).
    best_one_to_one_match = NULL;


    Yt_modified = true;
    closest_near_nei_YtX = NULL ;



    remove_first = false;

    eval_smoothness = false ;

    vec_fiedler_corresp = NULL;
    fiedler_corresp = NULL;

    no_of_fiedler_corresp = -1 ;


    }

void Match::init() {
    HASH_MAP_PARAM_(double, "k_out", Double, k_out ) ;
    HASH_MAP_PARAM_(double, "kapa", Double, kapa ) ;      
    HASH_MAP_PARAM_(int, "str_length", Int, str_length) ;
    HASH_MAP_PARAM_(int, "gt_level", Int, gt_level) ;

    HASH_MAP_PARAM_(double, "out_th", Double, out_th ) ;
    HASH_MAP_PARAM_(double, "beta_th", Double, beta_th ) ;
    HASH_MAP_PARAM_(double, "zero_th", Double, zero_th ) ;

    template_filename = new char [str_length] ;
    filename = new char [str_length] ;
    
}    


Match::~Match()
{
    if (template_filename)
    delete [] template_filename ;
  if (filename)
    delete [] filename ;
  
  if (initT)
	delete[]initT;

    if (vec_hist_X)
	delete[]vec_hist_X;

    if (vec_hist_Y)
	delete[]vec_hist_Y;

    if (vec_hist_FY)
	delete[]vec_hist_FY;

    if (X)
	delete[]X;

    if (Y)
	delete[]Y;

    if (covmat)
	delete[]covmat;

    if (inv_covmat)
	delete[]inv_covmat;

    if (T)
	delete[]T;

    if (lastT)
	delete[]lastT;

    if (vec_alpha)
	delete [] vec_alpha ;
    if (mat_alpha) 
	delete [] mat_alpha ;


    if (part)
	delete[]part;

    if (beta)
	delete[]beta;

    if (W)
	delete[]W;

    if (Yt)
	delete[]Yt;

    if (n_outliers)
	delete[]n_outliers;

    if (n_unmatched)
	delete[]n_unmatched;

    if (mask_X)
	delete[]mask_X;

    if (mask_Y)
	delete[]mask_Y;

    if (data_mask_X)
	delete[]data_mask_X;

    if (data_mask_Y)
	delete[]data_mask_Y;

    if (ag_match)
	delete[]ag_match;

    if (nag_match)
	delete[]nag_match;

    if (error)
	delete[]error;

    if (changeT)
	delete[]changeT;

    if (connect1)
	delete[]connect1;

    if (connect2)
	delete[]connect2;

    if (closest_near_nei_YtX) 
	delete [] closest_near_nei_YtX ;

    if (best_one_to_one_match)
	delete[]best_one_to_one_match;

    }


void Match::setAlphaLineMode(bool on) {

    use_alpha_line = on ;
}

bool Match::getAlphaLineMode( ) {

    return use_alpha_line ;
}


/*****************************************************************************/
/* 
   When the matching is performed, we match a set of observations to 
   a set of model-classes. 
   In order to correctly perform the data-to-model association, we need to take
   as model points the smallest set of points in terms of cardinality.
   Thus, the data set is the biggest (in terms of cardinality).

   In loadData, we check for this constraint, and inverse the sets if necessary.

   At the end, X contains the data and Y contains the model and n_X >= N_Y.
*/
/*****************************************************************************/
int Match::loadData(int p_nx, int p_ny, int p_dim, FL * p_X, FL * p_Y,
		    char *p_filename)
{

    PRINT_(fprintf(stderr, "Loading data X(%d) and Y (%d)\n", p_nx, p_ny));
    data_dim = p_dim;


    	
	
	
	
    FL *l_x, *l_y;
    if (p_nx >= p_ny) {
	nX = p_nx;
	nY = p_ny;
	l_x = p_X;
	l_y = p_Y;
	xy_inversed = false ;
    } else {
	PRINT_COLOR_(fprintf
		     (stderr, "Inverting order of inputs X->Y and Y->X\n"), RED);
	nX = p_ny;
	nY = p_nx;
	l_x = p_Y;
	l_y = p_X;
	xy_inversed = true ;
    }

    DBG_(fprintf
	 (stderr,
	  "Memory deallocation and allocation in load_data (match_em.cpp)\n"));

    if (X) {
	delete[]X;
	X = NULL;
    }
    if (Y) {
	delete[]Y;
	Y = NULL;
    }
    if (data_mask_X) {
	delete[]data_mask_X;
	data_mask_X = NULL;
    }
    if (data_mask_Y) {
	delete[]data_mask_Y;
	data_mask_Y = NULL;
    }
    if (covmat) {
	delete[]covmat;
	covmat = NULL;
    }
    if (inv_covmat) {
	delete[]inv_covmat;
	inv_covmat = NULL;
    }
    if (T) {
	delete[]T;
	T = NULL;
    }
    if (lastT) {
	delete[]lastT;
	lastT = NULL;
    }
    if (vec_alpha) {
	delete [] vec_alpha ;
	vec_alpha = NULL ;
    }
    if (mat_alpha) {
	delete [] mat_alpha ;
	mat_alpha = NULL ;
    }
    if (part) {
	delete[]part;
	part = NULL;
    }
    if (beta) {
	delete[]beta;
	beta = NULL;
    }
    if (W) {
	delete[]W;
	W = NULL;
    }
    if (Yt) {
	delete[]Yt;
	Yt = NULL;
    }
    if (mask_X) {
	delete[]mask_X;
	mask_X = NULL;
    }
    if (mask_Y) {
	delete[]mask_Y;
	mask_Y = NULL;
    }
    if (ag_match) {
	delete[]ag_match;
	ag_match = NULL;
    }
    if (nag_match) {
	delete[]nag_match;
	nag_match = NULL;
    }


    X = new FL[nX * data_dim];
    memset(X, 0, nX * data_dim * sizeof(FL));
    Y = new FL[nY * data_dim];
    memset(Y, 0, nY * data_dim * sizeof(FL));

    data_mask_X = new int[nX];
    memset(data_mask_X, 0, nX * sizeof(int));
    data_mask_Y = new int[nY];
    memset(data_mask_Y, 0, nY * sizeof(int));

    covmat = new FL[data_dim * data_dim];
    memset(covmat, 0, data_dim * data_dim * sizeof(FL));
    inv_covmat = new FL[data_dim * data_dim];
    memset(inv_covmat, 0, data_dim * data_dim * sizeof(FL));
    T = new FL[(data_dim + 1) * (data_dim + 1)];
    memset(T, 0, (data_dim + 1) * (data_dim + 1) * sizeof(FL));
    lastT = new FL[(data_dim + 1) * (data_dim + 1)];
    memset(lastT, 0, (data_dim + 1) * (data_dim + 1) * sizeof(FL));

    part = new FL[nX] ;
    memset(part, 0, nX * sizeof(FL));
    beta = new FL[nY];
    memset(beta, 0, nY * sizeof(FL));
    W = new FL[nY * data_dim];
    memset(W, 0, nY * data_dim * sizeof(FL));
    Yt = new FL[nY * data_dim];
    memset(Yt, 0, nY * data_dim * sizeof(FL));

    mask_X = new int[nX];
    memset(mask_X, 0, nX * sizeof(int));
    mask_Y = new int[nY];
    memset(mask_Y, 0, nY * sizeof(int));
    ag_match = new int[nX * 2];
    memset(ag_match, 0, 2 * nX * sizeof(int));
    match_available = false;

    covmat_ = cvMat(data_dim, data_dim, CV_FL, covmat);
    inv_covmat_ = cvMat(data_dim, data_dim, CV_FL, inv_covmat);

    int countzeros = 0;
    int count = 0;

    for (int i = 0; i < nX; i++) {
	countzeros = 0;
	for (int d = 0; d < data_dim; d++) {
	    X[i * data_dim + d] = l_x[i * data_dim + d];
	    if (X[i * data_dim + d] == 0.0)
		countzeros++;
	}
	if (countzeros == data_dim) {
	    data_mask_X[i] = 0;
	    count++;
	} else
	    data_mask_X[i] = 1;
    }
    PRINT_(fprintf(stderr, " %d masked points in loaded X\n", count));
    count = 0;
    for (int j = 0; j < nY; j++) {
	countzeros = 0;
	for (int d = 0; d < data_dim; d++) {
	    Y[j * data_dim + d] = l_y[j * data_dim + d];
	    if (Y[j * data_dim + d] == 0.0)
		countzeros++;
	}
	if (countzeros == data_dim) {
	    data_mask_Y[j] = 0;
	    count++;
	} else
	    data_mask_Y[j] = 1;
    }
    PRINT_(fprintf(stderr, " %d masked points in loaded Y\n", count));



    if (p_filename)
	strcpy(template_filename, p_filename);
    else
	sprintf(template_filename, "%s", "tmp/match");


    DBG_(fprintf(stderr, "Data loaded\n"));

#ifdef SAVE_TEMP_DATA
    print_flmatrix(X, "tmp/X", nX, data_dim);

    print_flmatrix(Y, "tmp/Y", nY, data_dim);
#endif
    sigma_initialized = false;

    return 0;
}

//------------------------------------------------------------------------------------
// EM functions
//------------------------------------------------------------------------------------



void Match::checkEmMode () {


    char mode[256];

    switch (em_mode) {
    case ISO_ANNEAL:
	strcpy(mode, "ISO ANNEAL");
	break;
    case GLOBAL_COV:
	strcpy(mode, "GLOBAL COVARIANCE");
	break;
    case USE_ESTIMATE:
	strcpy(mode, "USE ESTIMATED");
	break;

	    default:
	strcpy(mode, "UNKNOWN MODE");
	break;
    }

    PRINT_COLOR_(fprintf(stderr, "EM_MODE has been set to: %s\n", mode),
		 RED);


}


int Match::EM(INIT_MODE p_init_m, EM_MODE p_em_m, int *p_int_arg,
	      FL * p_fl_arg)
{

    DBG3_(fprintf(stderr, "Match::EM()\n"));

    em_mode = p_em_m;

    checkEmMode () ;

    initEM(p_init_m, p_int_arg, p_fl_arg);

    PRINT_COLOR_(fprintf(stderr, " EM() LOOP\n"), RED);

    int l_run = 1;
    while (l_run) {
	switch (step) {
	case E_STEP:
	    if (!E())
		step = M_STEP;
	    else
		l_run = 0;
	    break;
	case M_STEP:
	    M();
	    step = EVAL;
	    printMatches();
	    break;
	case EVAL:
	    l_run = evalTermination();
	    step = E_STEP;
	    break;
	case NONE_EM:
	    //for intialization when no E, M or Eval is needed (starts then with E)
	    step = E_STEP;
	    break;
	default:
	    ERROR_(fprintf(stderr, "step should be 'M' or 'E'\n"),
		   ERROR_UNKNOWN_MODE);
	    break;
	}
    }

    PRINT_COLOR_(fprintf(stderr, "EM(): END\n"), RED);

    printOutputs();

        return 0;
}




int Match::initIsoAnneal( ) 
{
    //outlier = K_OUT*pow(sigma,dim); //Radu's proposition
    //outlier_term = (1-nclass*pow(sigma,dim)/K_OUT); // if sigma_out changing with sigma_in
    //outlier_term = (factorial(dim/2)/pow(sigma,dim/2.0))*(1.0 - nclass/K_OUT); //factorial only valid for odd dimensions
    //outlier_term = K_OUT * factorial(dim/2)/pow(sigma,dim/2.0);

    outlier_term = k_out;

    nClass = nY;

    if (iter > 0) {
	nClass -= n_unmatched[iter - 1];
    }

    if (!sigma_initialized)
	initSigma(FROM_XY);
    sigma2 = sigma ;


    PRINT_COLOR_(fprintf(stderr, "Iso_anneal:"
		   "\n norm %g (max value of the probability density function)\n "
			 "sigma %g sigma2 %g \n", norm_fact, sigma, sigma2), GREEN);


    // The sign - is put latter in the exponential...
    sigma2 = 0.5 / sigma2;


    DBG_(fprintf(stderr, "End of init_iso_anneal() \n"));
    return 0;
}


int Match::initGlobalCov(FL * &p_distv)
{

    if (!p_distv) {
	p_distv = new FL[data_dim];
    }
    FL l_cond = (FL) cvInvert(&covmat_, &inv_covmat_, CV_SVD_SYM);	//method should be the same used in the evaluation test


    
    //outlier_term = K_OUT * factorial(data_dim/2)/sqrt(det_covmat);
    outlier_term = k_out;

    //norm_fact=(pow(2.0*M_PI,data_dim/2.0)*sqrt(det_covmat));                  
    norm_fact = 1.0;

    DBG_(fprintf
	 (stderr, "OUTLIER outlier_term distribution term: %g\n",
	  outlier_term));

    if (l_cond == 0) {
	ERROR_(fprintf(stderr, "Covariance matrix was not inverted\n"),
	       ERROR_ILL_COND);
    }
    return 0;
}




FL Match::computeAlphaijFromCovariance( FL * p_x_idx, FL * p_y_idx,
					FL * &p_distv )
{
    
    FL alpha = 0 ;
     int incX = 1;
    int incY = 1;
    double l_alpha = 1;
    double l_beta = 0;

    FL *l_dist_out = new FL[data_dim] ;
    memset(l_dist_out, 0, data_dim * sizeof(FL)) ;

   
    for (int d = 0; d < data_dim; d++)
	p_distv[d] = *(p_x_idx + d) - *(p_y_idx + d);	//X[i*data_dim+d] - Yt[j*data_dim+d];
    
    
    
    dsymv_("L", &data_dim, &l_alpha, inv_covmat, &data_dim,
	   p_distv, &incX, &l_beta,
	   l_dist_out, &incY);
    FL l_expo =
	-(ddot_
	      (&data_dim, l_dist_out, &incX,
	       p_distv, &incY) / 2);


    alpha = norm_fact * exp(l_expo);
    
    if (alpha > norm_fact) {	// norm_fact is the maximum value the function can take
	ERROR_(fprintf(stderr, "Error: alpha: %g > norm: %g sigma: %g \n",
		       alpha, norm_fact, sigma), ERROR_VARIABLE_INIT);
    }
    
    return alpha ;
    
}






FL Match::computeAlphaijFromEuclideanDistance( FL * p_x_idx, FL * p_y_idx, FL * &p_distv )
{
    
    FL l_dist, l_dist2 = 0.0;
    FL alpha = 0 ;
    for (int d = 0; d < data_dim; d++) {
	l_dist = *(p_x_idx + d) - *(p_y_idx + d);	//X[i*data_dim+d] - Yt[j*data_dim+d];
	l_dist2 += (l_dist) * (l_dist);
    }
    
    alpha = norm_fact * exp(-sigma2 * l_dist2);


    // norm_fact is the maximum value the function can take
    if (alpha > norm_fact) {	// norm_fact is the maximum value the function can take
	ERROR_(fprintf(stderr, "Error: alpha: %g > norm: %g sigma: %g \n",
		       alpha, norm_fact, sigma), ERROR_VARIABLE_INIT);
    }
    return alpha ;
}


int Match::E()
{

    //Estimate the posterior probabilities

    iter++;

    PRINT_COLOR_(fprintf(stderr, "\nEM(): Iteration %d \n", iter), RED);

    PRINT_COLOR_(fprintf(stderr, "-->E()\n"), RED);

    if (vec_alpha) {
	delete [] vec_alpha ;
    }
    if ( mat_alpha ) {
	delete [] mat_alpha ;
	mat_alpha = NULL ;
    }

    
    vec_alpha = new FL[ nX * nY ];
    memset(vec_alpha, 0, nX * nY * sizeof(FL)) ;

    if (!vec_alpha) {
	FATAL_(fprintf
	       (stderr, "Not enough memory to allocate alpha\n"), FATAL_MEMORY_ALLOCATION);
    }
    mat_alpha = new FL* [ nX ] ;
    if (!mat_alpha) {
	WARNING_(fprintf(stderr, "Match::E()::ERROR\n"));
	FATAL_(fprintf
	       (stderr, "Not enough memory to allocate alpha\n"), FATAL_MEMORY_ALLOCATION);
    }
    for (int i = 0 ; i < nX ; ++i) {
	mat_alpha [i] = (vec_alpha + i * nY)  ;
    }


    memset(part, 0, nX * sizeof(FL));

    PRINT_COLOR_(fprintf
		 (stderr,
		  "-->Estimating likelihoods (Observations X to new class centers Yt)\n"),
		 GREEN);



    FL *l_alpha_idx;


    norm_fact = 1 ;

    FL *l_distv = NULL;

    int l_retval = 0 ;

    switch (em_mode) {
			
    case ISO_ANNEAL:
    case USE_ESTIMATE:
	l_retval = initIsoAnneal( ) ;
	computeAlpha = &Match::computeAlphaijFromEuclideanDistance ;	
	computeAlphaAndPart( ) ;

	break;

    case GLOBAL_COV:
	l_retval += initGlobalCov( l_distv );
	computeAlpha = &Match::computeAlphaijFromCovariance ;
	if (l_retval) {
	    FATAL_(fprintf
		   (stderr,
		    "Covariance matrix was not inverted and all values were filled with zeros\n"),
		   FATAL_ILL_COND);
	}
	computeAlphaAndPart( );

	break;
	    default:
	FATAL_(fprintf
	       (stderr, "E(): not supported EM_MODE %d \n", em_mode),
	       FATAL_UNKNOWN_MODE);
	break;
    }

    if (l_retval) {
	ERROR_(fprintf(stderr, " EM LINE initialization FAILED\n"),
	       ERROR_VARIABLE_INIT);
    }

    //normalize rows of alpha to make them probabilities including the uniform distribution
    //mask elements below a certain probability OUT_TH and those whose partition is to close to zero
    
    PRINT_COLOR_(fprintf
		 (stderr,
		  "-->Normalizing and finding outliers (observations Xi not belonging to any class Yj)\n"),
		 GREEN);




    FL l_probasum;
    int l_outliers = 0;
    for (int i = 0; i < nX; i++) {
	l_alpha_idx = mat_alpha [i];

	if ( part[i] < zero_th ) {
	    mask_X[i] = 0;	// to prevent dividing by zero
	    l_outliers++;


	} else {
	    mask_X[i] = 1;
	    l_probasum = 0.0;
	    FL l_out_norm = 1.0 / (part[i] + outlier_term);


	    for (int j = 0; j < nY; j++, ++l_alpha_idx) {
		if (data_mask_Y[j] == 0) {		    
		    continue;
		}
		*(l_alpha_idx) *= l_out_norm;
		
		l_probasum += *(l_alpha_idx) * l_out_norm;
		
	    }
	    if (l_probasum < out_th ) {
		mask_X[i] = 0;
		l_outliers++;
	    }

	
#ifdef PRINT
	    if (i % 1000 == 0)
	      fprintf(stderr,
		      "probasum[%d]= %g probability of class being an inlier \n",
		      i, l_probasum);
#endif
	}
	

    }
    n_outliers[iter] = l_outliers;
    PRINT_COLOR_(fprintf
		 (stderr, "Number of outliers: %d\n", n_outliers[iter]),
		 GREEN);


#ifdef DEBUG_3
    for (int i = 0 ; i < nX ;++i) {
	for (int j = 0 ; j < nY ;++j) {
	    std::cout << mat_alpha[i][j] << " " ;
	}
	std::cout << std::endl ;
    }
#endif

    	//getchar () ;
    return 0;
}


void Match::computeAlphaAndPart( )
{
    FL *l_x_idx, *l_y_idx;


    PRINT_COLOR_(fprintf(stderr, "Compute Alpha and Part \n"), GREEN);


    l_x_idx = X;

    int i = 0;
    int j = 0;

    //Original code



#ifdef ENABLE_EM_PARALLELS
    int l_chunk = 100 ;
    int l_NumOfThreads = omp_get_max_threads();

#else
    int l_NumOfThreads = 1;
#endif

    FL **l_distv = new FL *[l_NumOfThreads];	//NULL ;
    FL **l_dummy_v = new FL *[l_NumOfThreads];	//NULL ;
    FL **l_dist_out = new FL *[l_NumOfThreads];	//NULL ;


    for ( i = 0; i < l_NumOfThreads; ++i) {
	l_distv[i] = new FL[data_dim];
	l_dummy_v[i] = new FL[data_dim];
	l_dist_out[i] = new FL[data_dim];
	memset (l_distv[i], 0, data_dim * sizeof(FL)) ;
	memset (l_dummy_v[i], 0, data_dim * sizeof(FL)) ;
	memset (l_dist_out[i], 0, data_dim * sizeof(FL)) ;
    }

    i = 0 ;

    


#ifdef ENABLE_EM_PARALLELS
#pragma omp parallel shared(l_chunk) private( d, l_expo, l_x_idx, l_y_idx, j, i)
    {

	int l_threadNum = omp_get_thread_num();
#else
	int l_threadNum = 0;
#endif

#ifdef ENABLE_EM_PARALLELS
#pragma omp for schedule(dynamic,l_chunk) nowait
#endif
	for (i = 0; i < nX; i++) {
	    l_x_idx = X + i * data_dim;
	    
	    part[i] = 0.0;
	    
	    if (data_mask_X[i] == 0) {
		continue;
	    }
	    l_y_idx = Yt;
	    
	    for (j = 0; j < nY; j++, l_y_idx += data_dim) {
		if (data_mask_Y[j] == 0) {
		    continue;
		}

		*(mat_alpha[i] + j) = (this->*computeAlpha)(l_x_idx, l_y_idx, l_distv[l_threadNum]) ;

		part[i] += *(mat_alpha[i] + j);

	    }
#ifdef PRINT
	    if (i % 1000 == 0)
		fprintf(stderr, "part[%d] = %g\n", i, part[i]);
#endif

	}


#ifdef ENABLE_EM_PARALLELS
    }				// End of parallel section
#endif
    

    for (int i = 0; i < l_NumOfThreads; ++i) {
	delete[]l_distv[i];
	delete[]l_dummy_v[i];
	delete[]l_dist_out[i];
    }

    delete[]l_distv;
    delete[]l_dummy_v;
    delete[]l_dist_out;


    DBG_(fprintf(stderr, "End of Compute Alpha and Part\n"));


}


int Match::M()
{
    //Estimate cluster contributions and the mask of data points Y
    //Estimate the transformation matrix
    //Apply the transformation to the points
    //Evaluate the error
    //Estimate the covariance matrix

    PRINT_COLOR_(fprintf(stderr, "-->M()\n"), RED);

    //Fill mask_Y
    //Find the overall contribution of X points to a each point in Y
    //Found sum the columns of alpha
    //Points below a given value are left unmatched

    PRINT_(fprintf(stderr, "-->Mask unmatched points\n"));

    sum_beta = 0.0;
    int l_unmatched = 0;
  
    int *l_data_mask_Y_idx = data_mask_Y;

    int j = 0;

    FL l_sum = 0;

#ifdef ENABLE_EM_PARALLELS
    int l_chunk = 100;

#pragma omp parallel shared(l_sum, l_unmatched, l_data_mask_Y_idx, l_chunk) private(j)
    {

#pragma omp for schedule(dynamic,l_chunk) nowait
#endif
	for (int j = 0; j < nY; j++) {
	    beta[j] = 0.0;

	    //if data is not masked
	    if (*(l_data_mask_Y_idx + j) == 1) {
		for (int i = 0; i < nX; i++ ) {
		    beta[j] += *(mat_alpha[i] + j);	// *(alpha + alpha_idx);
		}
	    } 
	    else {
	      PRINT_(std::cout << "data_mask_Y of " << j << " is set to 0" <<std::endl) ;
	    }
	    //if the total probability for the Y[j] is less than beta_th the point is declared unmatched
	    if (beta[j] < beta_th) {
		mask_Y[j] = 0;
		l_unmatched ++ ;

	    }
	    //else the mask is turned on and we cumul the overall probability in sumbeta
	    else {
		mask_Y[j] = 1;
		l_sum += beta[j];
	    }
#ifdef PRINT
	    if (j > 280 && j < 290)
		fprintf(stderr, "beta[%d] = %g\n", j, beta[j]);
#endif
	}

	//	getchar() ;
#ifdef ENABLE_EM_PARALLELS
    }				/* end of parallel section */
#endif

    sum_beta = l_sum;

    if ( init_mode == FIEDLER && iter < 0 )
      ++iter ;
    n_unmatched[iter] = l_unmatched;


    PRINT_COLOR_(fprintf(stderr, "-->Find clusters contributions to Y\n"),
		 GREEN);

    //Fill W 
    //find the weighted contributions X points to be actually compared with Y
    FL l_factor;

    //Reinitialize the W values
    bzero(W, nY * data_dim * sizeof(FL));

    //Initialize pointers to arrays
    FL *l_W_idx = W;
    FL *l_X_idx = X;
    int *l_mask_X_idx = mask_X;

    int i = 0;
    j = 0;
    int d = 0;

#ifdef ENABLE_EM_PARALLELS
#pragma omp parallel shared( l_W_idx, l_chunk) private(l_factor,  l_X_idx, l_mask_X_idx, j, i, d)
    {

#pragma omp for schedule(dynamic, l_chunk) nowait
#endif
	for (j = 0; j < nY; j++) {
	    if (mask_Y[j] == 1) {	//mask_Y[j]
		l_X_idx = X;
		l_mask_X_idx = mask_X;

		int index_j = j * data_dim;


		FL norm_on_column = 1.0 / (beta[j]) ;

		for (i = 0; i < nX; i++) {

		    if (*(l_mask_X_idx) == 1) {	//mask[i]


			l_factor = *(mat_alpha[i] + j) * norm_on_column ;	//factor  = (alpha[i*nY+j]) / (beta[j]);
			for (d = 0; d < data_dim; d++) {
			    *(l_W_idx + index_j + d) += l_factor * *(l_X_idx + d) ;	//W[j*data_dim+d] += factor * X[i*data_dim+d];
			}
		    }
		    l_X_idx += data_dim;
		    ++l_mask_X_idx;
		}

	    }
	}
#ifdef ENABLE_EM_PARALLELS
    }				/* end of parallel section */
#endif
#ifdef SAVE_TEMP_DATA
    print_flmatrix(W, "tmp/W", nY, data_dim);
#endif


    //Estimate Transformation matrix, apply it and estimate the error
    //TODO: accesible options

    PRINT_COLOR_(fprintf
		 (stderr,
		  "-->Estimate transformation, apply it and measure error\n"),
		 GREEN);


    //weighted = 1;
    //centered = 1; // 0
    //rot_only = 0;     
    int l_retval = 0;
    switch (transfo_mode) {
    case ROTATION_ONLY:
    case ORTHOGONAL:
    case ROT_AND_TRANS:
    case ORTH_AND_TRANS:
	l_retval += huang(weighted, centered, rot_only);
	l_retval += transform();
	break;
	    }

    error[iter] = estimateError();


    computeArgMaxMatch();
    computeNArgMaxMatch();

    
    //results of match are used by update_min_sigma
    computeMCovariance();









    return l_retval;

}

int Match::computeMCovariance()
{
    PRINT_COLOR_(fprintf(stderr, "-->Estimate the new covariance \n"),GREEN);

    //Estimate Covariance
    switch (em_mode) {
    case ISO_ANNEAL:
	if (!sigma_initialized)
	    initSigma(FROM_WYT);
	else
	    updateMinSigma();
	sigma = kapa * sigma;
	PRINT_COLOR_(fprintf(stderr, "New sigma: %g\n", sigma), GREEN);
	break;
    case GLOBAL_COV:
	updateCov();
#ifdef SAVE_TEMP_DATA
	print_flmatrix(covmat, "tmp/covmat", data_dim, data_dim);
#endif
	break;

    case USE_ESTIMATE:	
	    if (!use_alpha_line) {
		sigma = updateSigma();
	    }
	    else {
		sigma = lineUpdateSigma () ;
	    }

	    PRINT_COLOR_(fprintf(stderr, "New sigma %g \n", sigma), RED);
	break ;
    default:
	FATAL_(fprintf
	       (stderr, "M(): em_mode %d not implemented\n", em_mode),
	       FATAL_UNKNOWN_MODE);
	break;
    }
    return 0;
}

int Match::evalTermination()
{


    PRINT_COLOR_(fprintf(stderr, "-->EVAL()\n"), RED);
    int l_retval = 1;

    switch (em_mode) {
    case USE_ESTIMATE:
    case ISO_ANNEAL:
	//check if reached minimum sigma 
	if (sigma < min_sigma) {
	    PRINT_COLOR_(fprintf
			 (stderr,
			  "EVAL_TERMINATION: Reached minsigma %g < %g \n",
			  sigma, min_sigma), GREEN);
	    l_retval = 0;
	}
	break;
    case GLOBAL_COV:
	{
	    FL l_det_covmat = cvDet(&covmat_);
	    if (l_det_covmat < zero_th) {
		WARNING_(fprintf
			 (stderr,
			  "EVAL_TERMINATION: Global Covariance matrix is singular (det: %g)\n",
			  l_det_covmat));
		l_retval = 0;
	    }


	    FL l_cond = (FL) cvInvert(&covmat_, &inv_covmat_, CV_SVD_SYM);	//method should be the same used in the likelihood

	    if (l_cond == 0) {
		WARNING_(fprintf
			 (stderr,
			  "Covariance matrix inversion will fail (result of inversion %g)\n",
			  l_cond));
		l_retval = 0;
	    }
	}
	break;
	    default:
	FATAL_(fprintf
	       (stderr, "EVAL_TERMINATION: EM_MODE %d not supported\n",
		em_mode), FATAL_UNKNOWN_MODE);
	break;
    }

    //chek if maximum number of iterations reached
    if (iter >= max_iter) {
	PRINT_COLOR_(fprintf
		     (stderr,
		      "EVAL_TERMINATION: Reached maximum number of iterations %d\n",
		      max_iter), GREEN);
	l_retval = 0;
    }

    if (error[iter] < min_error) {
	PRINT_COLOR_(fprintf
		     (stderr,
		      "EVAL_TERMINATION: Reached min error %g  < %g \n",
		      error[iter], min_error), GREEN);
	l_retval = 0;
    }
    if (iter != 0 && iter < max_iter) {
	//check if reached the minimum change in the transformation
	//TODO this can actually be suppressed by evaluating the error change
	FL l_diff;
	changeT[iter] = 0.0;
	for (int k = 0; k < (data_dim + 1) * (data_dim + 1); k++) {
	    l_diff = lastT[k] - T[k];
	    changeT[iter] += (l_diff) * (l_diff);
	}
	PRINT_COLOR_(fprintf(stderr, "Change in T: %g\n", changeT[iter]),
		     GREEN);
	if (changeT[iter] < min_changeT) {
	    PRINT_COLOR_(fprintf
			 (stderr,
			  "EVAL_TERMINATION: Reached min change in T %g < %g \n",
			  changeT[iter], min_changeT), GREEN);
	    l_retval = 0;
	}
    }
    
    return l_retval;
}



int Match::dimensionSelection(int p_new_dim, int *p_mask_dim)
{

    PRINT_COLOR_(std::cout << "Performing dimension selection" <<std::endl, RED) ;
    DBG_(fprintf(stderr, " NEW Dimension %d\n", p_new_dim));

    if ((!X) || (!Y)) {
	FATAL_(fprintf
	       (stderr,
		" ERROR: Can not change the dimension of empty pointers\n"),
	       FATAL_MEMORY_ALLOCATION);
    }
    if (p_new_dim <= 1) {
	FATAL_(fprintf
	       (stderr,
		" ERROR: Less than 2 dimension retained change the discriminant treshold\n"),
	       FATAL_ERROR);
    }


    FL *l_newX = new FL[p_new_dim * nX];
    bzero(l_newX, p_new_dim * nX * sizeof(FL));
    FL *l_newY = new FL[p_new_dim * nY];
    bzero(l_newY, p_new_dim * nY * sizeof(FL));
    int l_countd = 0;
    // Copying selected dimensions
    for (int d = 0; d < data_dim; d++) {
	if (p_mask_dim[d] == 1) {
	    //copy X to the new X
	    for (int i = 0; i < nX; i++) {
		l_newX[i * p_new_dim + l_countd] = X[i * data_dim + d];
	    }
	    //copy Yt to the new Y
	    for (int j = 0; j < nY; j++) {
		l_newY[j * p_new_dim + l_countd] = Y[j * data_dim + d];
	    }
	    l_countd++;
	}
    }
    // Copying corresponding histograms
    if (vec_hist_X && vec_hist_Y && vec_hist_FY) {
	FL *l_new_vec_hist_X = new FL[p_new_dim * numOfBins];
	bzero(l_new_vec_hist_X, p_new_dim * numOfBins * sizeof(FL));
	FL *l_new_vec_hist_Y = new FL[p_new_dim * numOfBins];
	bzero(l_new_vec_hist_Y, p_new_dim * numOfBins * sizeof(FL));
	l_countd = 0;
	for (int d = 0; d < data_dim; d++) {
	    if (p_mask_dim[d] == 1) {
		//copy X to the new X
		PRINT_(std::cout << "Copying dimension " << d << std::
		       endl);
		for (int i = 0; i < numOfBins; i++) {
		    l_new_vec_hist_X[l_countd * numOfBins + i] =
			vec_hist_X[d * numOfBins + i];
		}
		//copy Yt to the new Y
		for (int j = 0; j < numOfBins; j++) {
		    l_new_vec_hist_Y[l_countd * numOfBins + j] =
			vec_hist_Y[d * numOfBins + j];
		}
		l_countd++;
	    }
	}

	delete[]vec_hist_X;
	delete[]vec_hist_Y;


#ifdef DEBUG_3
	std::cout << "Displaying the two Histograms: " <<std::endl ;
	std::cout << "Attached to X : " <<std::endl ;
	for (int j = 0; j < numOfBins; j++) {
	    for (int d = 0; d < p_new_dim; d++) {
		std::cout << l_new_vec_hist_X[ d * numOfBins + j] << " " ;
	    }
	    std::cout <<std::endl ;
	}
	std::cout << "Attached to Y : " <<std::endl ;
	for (int j = 0; j < numOfBins; j++) {
	    for (int d = 0; d < p_new_dim; d++) {
		std::cout << l_new_vec_hist_Y[ d * numOfBins + j] << " " ;
	    }
	    std::cout <<std::endl ;
	}
#endif
	vec_hist_Y = l_new_vec_hist_Y;
	vec_hist_X = l_new_vec_hist_X;
    }


    if (l_countd != p_new_dim) {
	WARNING_(fprintf
		 (stderr,
		  " THE MASK IS NOT COHERENT WITH THE NEW DIMENSION!!!!!! (ext newdim %d here %d)\n",
		  p_new_dim, l_countd));
    }
    //delete  pointers to X and Y    
    delete[]X;
    delete[]Y;


    X = l_newX;
    Y = l_newY;



    //reinitialize all dimension dependent variables
    data_dim = p_new_dim;

    if (covmat) {
	delete[]covmat;
	covmat = NULL;
    }
    if (inv_covmat) {
	delete[]inv_covmat;
	inv_covmat = NULL;
    }
    if (T) {
	delete[]T;
	T = NULL;
    }
    if (lastT) {
	delete[]lastT;
	lastT = NULL;
    }
    if (W) {
	delete[]W;
	W = NULL;
    }
    if (Yt) {
	delete[]Yt;
	Yt = NULL;
    }


    covmat = new FL[data_dim * data_dim];
    memset(covmat, 0, data_dim * data_dim * sizeof(FL));
    inv_covmat = new FL[data_dim * data_dim];
    memset(inv_covmat, 0, data_dim * data_dim * sizeof(FL));
    T = new FL[(data_dim + 1) * (data_dim + 1)];
    memset(T, 0, (data_dim + 1) * (data_dim + 1) * sizeof(FL));
    lastT = new FL[(data_dim + 1) * (data_dim + 1)];
    memset(lastT, 0, (data_dim + 1) * (data_dim + 1) * sizeof(FL));

    W = new FL[nY * data_dim];
    memset(W, 0, nY * data_dim * sizeof(FL));

    Yt = new FL[nY * data_dim];
    memset(Yt, 0, nY * data_dim * sizeof(FL));

    covmat_ = cvMat(data_dim, data_dim, CV_FL, covmat);
    inv_covmat_ = cvMat(data_dim, data_dim, CV_FL, inv_covmat);




    return 0;

}



//------------------------------------------------------------------------------------
//TRANSFORMATIONS
//------------------------------------------------------------------------------------
int Match::transform()
{

    PRINT_COLOR_(fprintf(stderr, "-->Applying estimated transformation\n"),RED);

    bzero(Yt, nY * data_dim * sizeof(FL));

    FL * l_yt_idx = Yt ;
    FL * l_y_idx = Y ;
    int ind_d = 0 ;
    for (int j = 0; j < nY; j++) {

	for (int d = 0; d < data_dim; d++) {
	    ind_d = d * (data_dim + 1) ;
	    //load translation
	    *(l_yt_idx + d) = T[ind_d + data_dim] ;
	    //add Rotation
	    for (int k = 0; k < data_dim; k++) {	//Inner product
		*(l_yt_idx + d) += T[ind_d + k] * *(l_y_idx + k) ;
	    }
	}

	l_yt_idx += data_dim ;
	l_y_idx += data_dim ;
    }

#ifdef SAVE_TEMP_DATA
    print_flmatrix(Yt, "tmp/Yt", nY, data_dim);
#endif

    Yt_modified = true ;

    return 0;
}

FL Match::estimateError()
{

    PRINT_(fprintf(stderr, "-->Estimating error between (W,Yt) \n"));

    //evaluate the error
    //check if reached the minimum expected error
    //TODO : should I weight the error????
    FL l_diff, l_out_err;
    int l_count = 0;
    l_out_err = 0.0;
    int ind_j = 0;

    for (int j = 0; j < nY; j++) {
	if (mask_Y[j] == 1) {
	    l_count++;
	    ind_j = j * data_dim;
	    for (int d = 0; d < data_dim; d++) {
		l_diff = W[ind_j + d] - Yt[ind_j + d];
		l_out_err += (l_diff) * (l_diff);
	    }
	}
    }

    if (l_count == 0) {
	ERROR_(fprintf(stderr, "The error can not be estimated \n"),
	       ERROR_EMPTY_DATA);
    }

    l_out_err /= (FL) l_count;

    PRINT_COLOR_(fprintf
		 (stderr, "Mean square error (W, Yt): %g\n", l_out_err),
		 GREEN);

    return l_out_err;
}

int Match::huang(int p_hweighted, int p_hcentered, int p_hrot_only)
{
    //computes transformation between Y and W
    //ignores masked Y 

    PRINT_COLOR_(fprintf
		 (stderr,
		  "-->Using Huang function to estimate the Orthogonal Transformation\n"),
		 RED);
    FL *l_centerw = new FL[data_dim];
    FL *l_centery = new FL[data_dim];
    FL *l_YY = NULL;
    FL *l_WW = NULL;
    FL *l_H = new FL[data_dim * data_dim];
    FL *l_R = new FL[data_dim * data_dim];
    FL *l_t = NULL;
    FL l_weight;

    if (p_hweighted)
	PRINT_COLOR_(fprintf(stderr, "huang: weighted option on.\n"),
		     BLUE);

    //Compute means and center data
    if (p_hcentered) {
	PRINT_COLOR_(fprintf
		     (stderr, "huang: Assuming data is already centered \n"),BLUE);
	l_WW = W;		//point to original data
	l_YY = Y;
    } else {

	//If not centered then center
	l_YY = new FL[nY * data_dim];	//intermediate values;
	l_WW = new FL[nY * data_dim];
	bzero(l_YY, nY * data_dim * sizeof(FL));
	bzero(l_WW, nY * data_dim * sizeof(FL));

	//initialize means
	bzero(l_centerw, data_dim * sizeof(FL));
	bzero(l_centery, data_dim * sizeof(FL));

	//estimate means or baricenters
	int l_count = 0;
	for (int j = 0; j < nY; j++) {
	    if (mask_Y[j] == 1) {
		if (p_hweighted) {
		    l_weight = beta[j] / sum_beta ;
		}
		else {
		    l_weight = 1.0;
		    l_count++;
		}
		for (int d = 0; d < data_dim; d++) {
		    l_centerw[d] += l_weight * W[j * data_dim + d];
		    l_centery[d] += l_weight * Y[j * data_dim + d];
		}
	    }
	}
	//print_flmatrix(centerw,"tmp/sumw",data_dim,1);
	//print_flmatrix(centery,"tmp/sumy",data_dim,1);
	if (!p_hweighted) {
	    if (l_count == 0) {
		ERROR_(fprintf
		       (stderr,
			"Dividing by zero when calculating means. All the points are masked\n"),
		       ERROR_EMPTY_DATA);
	    } else {

		for (int d = 0; d < data_dim; d++) {
		    l_centerw[d] /= (FL) l_count;
		    l_centery[d] /= (FL) l_count;
		}
	    }
	}
#ifdef SAVE_TEMP_DATA
	print_intmatrix(mask_Y, "tmp/mask_Y", nY, 1);
	//print_flmatrix(centerw,"tmp/centerw",data_dim,1);
	//print_flmatrix(centery,"tmp/centery",data_dim,1);
#endif

	//substract from the original points
	for (int j = 0; j < nY; j++) {
	    for (int d = 0; d < data_dim; d++) {
		if (mask_Y[j] == 1) {
		    l_WW[j * data_dim + d] =
			W[j * data_dim + d] - l_centerw[d];
		    l_YY[j * data_dim + d] =
			Y[j * data_dim + d] - l_centery[d];
		}
	    }
	}
    }








#ifdef SAVE_TEMP_DATA
    //print_flmatrix(W,"tmp/W",nY,data_dim);
    //print_flmatrix(Y,"tmp/Y",nY,data_dim);
#endif

    //construct the H matrix
    bzero(l_H, data_dim * data_dim * sizeof(FL));

    for (int j = 0; j < nY; j++) {
	if (mask_Y[j] == 1) {
	    if (p_hweighted)
		l_weight = beta[j] / sum_beta;
	    else
		l_weight = 1.0;
	    for (int k = 0; k < data_dim; k++) {
		for (int l = 0; l < data_dim; l++) {
		    l_H[k * data_dim + l] +=
			l_weight * l_YY[j * data_dim +
					k] * l_WW[j * data_dim + l];
		}
	    }
	}
    }

    FL l_sumh = 0.0;
    for (int k = 0; k < data_dim * data_dim; k++) {
	l_sumh += l_H[k];
    }

    //ORTHOGONAL MATRIX 
    CvMat l_R_ = cvMat(data_dim, data_dim, CV_FL, l_R);
    cvSetZero(&l_R_);
    if (fabs(l_sumh) < zero_th) {

	WARNING_(fprintf
		 (stderr,
		  "huang: H is SINGULAR. Probably one pointset centered at the mean\n"
		  "Filling transformation with identity.\n"));
#ifdef SAVE_TEMP_DATA
	print_flmatrix(l_H, "tmp/H", data_dim, data_dim);
#endif
	//Identity
	for (int k = 0; k < data_dim; k++)
	    l_R[k * data_dim + k] = 1.0;

    } else {
	//Decompose the H matrix
	CvMat *l_U = cvCreateMat(data_dim, data_dim, CV_FL);
	CvMat *l_V = cvCreateMat(data_dim, data_dim, CV_FL);
	CvMat *l_D = cvCreateMat(data_dim, 1, CV_FL);
	CvMat l_H_ = cvMat(data_dim, data_dim, CV_FL, l_H);

	//cvSVD A=U*W*V'
	cvSVD(&l_H_, l_D, l_U, l_V);

	//Obtain the Rotation Matrix R=V*U'

	cvGEMM(l_V, l_U, 1.0, NULL, 0.0, &l_R_, CV_GEMM_B_T);

	//ROTATION or ORTHOGONAL
	//negating a column negates the determinant, and thus negating an odd (but not even) 
	//number of columns negates the determinant.
	if (p_hrot_only) {
	    PRINT_COLOR_(std::
			 cout << "HUANG : Only Rotations are estimated" <<
			 std::endl, BLUE);
	    double l_det;
	    l_det = cvDet(&l_R_);
	    PRINT_COLOR_(fprintf
			 (stderr, "huang: Rotation only on. Determinant = %g \n",
			  l_det),GREEN);
	    if (l_det < 0.0) {
		for (int d = 0; d < data_dim; d++) {
		    l_R[d * data_dim + data_dim - 1] *= -1.0;
		}
		l_det = cvDet(&l_R_);
		PRINT_COLOR_(fprintf(stderr, "Correcting the determinant, since < 0. New determinant = %g \n", l_det), RED);
		
	    }
	}
	cvReleaseMat(&l_U);
	cvReleaseMat(&l_V);
	cvReleaseMat(&l_D);
    }


    //Save last transformation to check for min variation
    //Interchanging pointers
    FL *l_dummy;
    l_dummy = lastT;
    lastT = T;
    T = l_dummy;

    //Reinitialize the transformation matrix
    memset(T, 0, (data_dim + 1) * (data_dim + 1) * sizeof(FL));
    T[(data_dim + 1) * (data_dim + 1) - 1] = 1.0;





    //copy the Rotation Matrix to the global transformation T
    for (int k = 0; k < data_dim; k++) {
	for (int l = 0; l < data_dim; l++) {
	    T[k * (data_dim + 1) + l] = l_R[k * data_dim + l];
	}
    }


    //TRANSLATION
    if (!p_hcentered) {
	PRINT_(fprintf(stderr, "huang: Translation option on.\n"));
	//estimate the translation parameters
	//t = cw' - R*cy'
	l_t = new FL[data_dim];
	bzero(l_t, data_dim * sizeof(FL));
	CvMat l_t_ = cvMat(data_dim, 1, CV_FL, l_t);
	CvMat l_cy_ = cvMat(data_dim, 1, CV_FL, l_centery);
	CvMat l_cw_ = cvMat(data_dim, 1, CV_FL, l_centerw);
	cvGEMM(&l_R_, &l_cy_, -1.0, &l_cw_, 1.0, &l_t_, 0);


	//copy the result to the global transformation T
	for (int d = 0; d < data_dim; d++) {
	    T[(data_dim + 1) * d + data_dim] = l_t[d];
	}
#ifdef SAVE_TEMP_DATA
	//print_flmatrix(t,"tmp/tr",data_dim,1);
	//print_flmatrix(R,"tmp/R",data_dim,data_dim);
#endif
    }


    printT();


    if (!p_hcentered) {
	delete[]l_YY;
	delete[]l_WW;
	delete[]l_t;

    }
    delete[]l_centerw;
    delete[]l_centery;
    delete[]l_H;
    delete[]l_R;


    PRINT_(fprintf
	   (stderr,
	    "huang: orthogonal transformation estimated. huang function end \n"));

    return 0;
}



//----------------------------------------------------------------------
// INITIALIZATION & TERMINATION
//----------------------------------------------------------------------
int Match::initEM(INIT_MODE p_init_m, int *p_int_arg, FL * p_fl_arg)
{

  HASH_MAP_PARAM_(int, "n_match_em", Int, n_max_match) ;

    init_mode = p_init_m;


    PRINT_(fprintf(stderr, "\nEM(): START\n"));

    int l_max_it = 0 ;
    HASH_MAP_PARAM_(int, "max_it", Int, l_max_it) ;
    double l_min_sigma ;
    double l_min_e ;
    double l_min_t ;
    HASH_MAP_PARAM_(double, "min_sigma", Double, l_min_sigma) ;
    HASH_MAP_PARAM_(double, "min_e", Double, l_min_e) ;
    HASH_MAP_PARAM_(double, "min_t", Double, l_min_t) ;
    loadTerminationCriteria( l_max_it, 
			     l_min_sigma, 
			     l_min_e,  
			     l_min_t);

    PRINT_(fprintf(stderr, "--->Init mask\n"));

    PRINT_(std::cout << "nX : " << nX << " nY : " << nY <<std::endl) ;
    for (int i = 0; i < nX; i++)
	mask_X[i] = 1;
    for (int j = 0; j < nY; j++)
	mask_Y[j] = 1;

	
    int *l_count = NULL;
    int *l_dim_mask = NULL;
    int l_count_dim = 0;

    switch (init_mode) {
	
    case EFUNC:
	PRINT_COLOR_(fprintf(stderr, "--->INIT : match eigenfunctions \n"),
		     GREEN);
	matchEigenFunc();
	
	if (!sigma_initialized) {
	    initSigma(FROM_NN);	// for ISO_ANNEAL
	}
	
	initCov();		// for GLOBAL_COV method

	step = NONE_EM;		// Start synchronized with the first E step            
	
	break;

    case UNIFORM:
	PRINT_COLOR_(fprintf
		     (stderr,
		      "--->INIT : starting with Identity matrix \n"),
		     GREEN);
	//Does nothing
	//It actually resumes to start with an I transformation
	//Leaves T undetermined since by giving an identical probability
	//to each observation is the same as centering everything at the
	//mean. By directly applying M() step,all W will be set to the same coordinates
	//H will be singular and replaced by the identity.
	
	

	memcpy(Yt, Y, nY * data_dim * sizeof(FL));
	initSigma(FROM_XY);
	initCov();
	step = NONE_EM;
	break;


    case ALPHA:
	PRINT_COLOR_(fprintf
		     (stderr, "--->INIT : starting with alpha matrix \n"),
		     GREEN);

	if (p_int_arg == NULL) {
	    FATAL_(fprintf
		   (stderr, "ALPHA initialization needs an argument\n"),
		   FATAL_MEMORY_ALLOCATION);
	}
	for (int i = 0; i < nX; i++) {
	    part[i] = 0.0;
	    for (int i = 0; i < nX; i++) {
		mat_alpha[i][nX - 1] = p_int_arg[i * 2];
		part[i] += mat_alpha[i][nX - 1];
	    }
	}


	PRINT_(fprintf(stderr, "-->Normalizing and filling mask\n"));
	for (int i = 0; i < nX; i++) {
	    mask_X[i] = 1;
	    if (part[i] < zero_th ) {
		mask_X[i] = 0;	// to prevent dividing by zero
	    } else {
		//probasum = 0.0;
		for (int j = 0; j < nY; j++) {
		    mat_alpha[i][j] = mat_alpha[i][j] / (part[i]);	// + uniform);
		}
	    }
	}
	step = M_STEP;
	break;

    
    case TRANSFO:{



	PRINT_COLOR_(fprintf
		     (stderr,
		      "--->INIT : starting with a pre-defined matrix (loaded from a file).\n"),
		     GREEN);
	PRINT_(fprintf(stderr, "Copy input argument to transfo\n"));
	if (p_fl_arg == NULL) {
	    FATAL_(fprintf
		   (stderr,
		    "TRANSFO initialization needs an argument\n"),
		   FATAL_MEMORY_ALLOCATION);
	}
	//copy input transformation
	//mask the dimensions
	l_dim_mask = new int[data_dim];
	bzero(l_dim_mask, data_dim * sizeof(int));
	l_count_dim = 0;
	for (int i = 0; i < data_dim; i++) {
	    for (int j = 0; j < data_dim; j++) {
		T[i * (data_dim + 1) + j] =
		    p_fl_arg[i * (data_dim) + j];
		if ((i < data_dim)
		    && (T[i * (data_dim + 1) + j] != 0.0)) {
		    l_dim_mask[i] = 1;
		}
	    }
	    if ((i < data_dim) && (l_dim_mask[i] == 1)) {
		l_count_dim++;
	    }
	}
	if (!initT) {
	    init_dim = data_dim;
	    initT = new int[(data_dim + 1) * (data_dim + 1)];
	    memset(initT, 0,
		   (data_dim + 1) * (data_dim + 1) * sizeof(int));
	    for (int d = 0; d < data_dim; d++) {
		for (int e = 0; e < data_dim; e++) {
		    initT[d * (data_dim + 1) + e] =
			(int) T[d * (data_dim + 1) + e];
		}
	    }
	}

	PRINT_COLOR_(fprintf
		     (stderr,
		      "Found %d unmasked dimensions in the initialization T\n",
		      l_count_dim), GREEN);

	//Apply the result and save it as the original Y                        
	transform();

	memcpy(Y, Yt, nY * data_dim * sizeof(FL));

	//select the dimensions
	dimensionSelection(l_count_dim, l_dim_mask);
	memcpy(Yt, Y, nY * data_dim * sizeof(FL));

	//Fill match to update covariance when using sigma
	if (nag_match) {
	    delete[]nag_match;
	    nag_match = NULL;
	}
	nag_match = new int[nY * n_max_match * 2];
	memset (nag_match, 0, nY * n_max_match * 2 * sizeof(int)) ;

	if (Yt_modified) {
	    if (closest_near_nei_YtX == NULL) {
		closest_near_nei_YtX= new int[nY * n_max_match] ;
	    }
	    findNN(closest_near_nei_YtX, Yt, nY, X, nX, data_dim, n_max_match);
	    Yt_modified = false ;
	}
	for (int j = 0, m = 0; j < nY; j++) {
	    int ind_j = j * n_max_match ;
	    for (int k = 0; k < n_max_match; k++, m += 2) {
		nag_match[m] = j;
		nag_match[m + 1] = closest_near_nei_YtX[ind_j + k];
	    }
	}
	if (!sigma_initialized) {
	    PRINT_COLOR_(std::cout << "Initializing sigma" <<std::endl , GREEN) ;
	    initSigma(FROM_NN);	// for ISO_ANNEAL
	}
	initCov();		// for GLOBAL_COV method
	    
	//	computeMCovariance();	//uses resutlts of best matches saved in (n)agmatch
	    

	step = NONE_EM;	// Start synchronized with the first E step

	break;

    }
    case MATCH:
	PRINT_COLOR_(fprintf
		     (stderr,
		      "--->INIT : starting with an initial matching. \n"),
		     GREEN);

	if (p_int_arg == NULL) {
	    FATAL_(fprintf
		   (stderr, "MATCH initialization needs an argument\n"),
		   FATAL_MEMORY_ALLOCATION);
	} else {
	    l_count = new int[nY];
	    //init count and masks
	    for (int j = 0; j < nY; j++) {
		l_count[j] = 0;
		for (int d = 0; d < data_dim; d++) {
		    W[j * data_dim + d] = 0.0;
		}
	    }

	    PRINT_(fprintf
		   (stderr,
		    "Fill W with baricenters of X(%d) matches\n mask Y(%d) unmatched points\n",
		    nX, nY));
	    //fill X->W and mask_Y with read matches
	    //fill mask_Y
	    //fill ag_match
	    int indx, indy;
	    for (int i = 0; i < nX; i++) {
		if (!xy_inversed) {
		    indx = p_int_arg[i * 2];
		    indy = p_int_arg[i * 2 + 1];
		} else {
		    indy = p_int_arg[i * 2];
		    indx = p_int_arg[i * 2 + 1];
		}
		ag_match[i * 2] = indx;
		ag_match[i * 2 + 1] = indy;
		if ((indx >= 0) && (indy >= 0)) {
		    mask_Y[indy] = 1;
		    l_count[indy]++;
		    for (int d = 0; d < data_dim; d++) {
			W[indy * data_dim + d] += X[indx * data_dim + d];
		    }
		}
	    }
	    //normalize W (accounts for multiple matches)
	    for (int j = 0; j < nY; j++) {
		if (l_count[j] > 1) {
		    for (int d = 0; d < data_dim; d++) {
			W[j * data_dim + d] /= (FL) l_count[j];
		    }
		}
	    }


	    //find current transformation and apply it 
	    weighted = 1;	//0
	    centered = 1;	//0
	    rot_only = 0;

	    huang(weighted, centered, rot_only);


	    transform();

	    printArgMaxMatch("first_match");	//should be equal to init_match

	    initSigma(FROM_WYT);	// for ISO_ANNEAL

	    initCov();		// for GLOBAL_COV method

	    step = NONE_EM;	// Start synchronized with the first E step

	}
	break;
    case EIGVAL: {

	PRINT_COLOR_(fprintf
		     (stderr,
		      "--->INIT : The initial match is done usinf Ordered Eigen values.\n"),
		     GREEN);
	PRINT_(fprintf(stderr, "Copy input argument to transfo\n"));
	//copy input transformation
	//mask the dimensions
	l_dim_mask = new int[data_dim];
	bzero(l_dim_mask, data_dim * sizeof(int));
	l_count_dim = 0;
	for (int i = 0; i < data_dim; i++) {
	    for (int j = 0; j < data_dim; j++) {
		if (i ==j) {
		    T[i * (data_dim + 1) + j] = 1 ;
		    l_dim_mask[i] = 1;
		}
	    }
	    if ((i < data_dim) && (l_dim_mask[i] == 1)) {
		l_count_dim++;
	    }
	}
	if (!initT) {
	    init_dim = data_dim;
	    initT = new int[(data_dim + 1) * (data_dim + 1)];
	    memset(initT, 0,
		   (data_dim + 1) * (data_dim + 1) * sizeof(int));
	    for (int d = 0; d < data_dim; d++) {
		for (int e = 0; e < data_dim; e++) {
		    initT[d * (data_dim + 1) + e] =
			(int) T[d * (data_dim + 1) + e];
		}
	    }
	}

	PRINT_COLOR_(fprintf
		     (stderr,
		      "Found %d unmasked dimensions in the initialization T\n",
		      l_count_dim), GREEN);

	//Apply the result and save it as the original Y                        
	transform();

	memcpy(Y, Yt, nY * data_dim * sizeof(FL));

	//select the dimensions
	dimensionSelection(l_count_dim, l_dim_mask);
	memcpy(Yt, Y, nY * data_dim * sizeof(FL));

	//Fill match to update covariance when using sigma
	if (nag_match) {
	    delete[]nag_match;
	    nag_match = NULL;
	}
	nag_match = new int[nY * n_max_match * 2];



	if (Yt_modified) {
	    if (closest_near_nei_YtX == NULL) {
		closest_near_nei_YtX= new int[nY * n_max_match] ;
	    }
	    findNN(closest_near_nei_YtX, Yt, nY, X, nX, data_dim, n_max_match) ;
	    Yt_modified = false ;
	}
	for (int j = 0, m = 0; j < nY; j++) {
	    int ind_j = j * n_max_match ;
	    for (int k = 0; k < n_max_match; k++, m += 2) {
		nag_match[m] = j;
		nag_match[m + 1] = closest_near_nei_YtX[ind_j + k];
	    }
	}

	if (!sigma_initialized)
	    initSigma(FROM_XY);	// for ISO_ANNEAL
	//init_sigma_fromXY(); // for ISO_ANNEAL
	initCov();		// for GLOBAL_COV method
	step = NONE_EM;	// Start synchronized with the first E step

	break;

    }

    default:
	FATAL_(fprintf
	       (stderr, "init(): mode %d not recognized\n", em_mode),
	       FATAL_UNKNOWN_MODE);
	break;
    }
#ifdef SAVE_TEMP_DATA
    //print_flmatrix(alpha,"tmp/alpha",nX,nY);
#endif

    if (l_count)
	delete[]l_count;
    if (l_dim_mask)
	delete[]l_dim_mask;

    PRINT_(fprintf(stderr, "\nEM(): Initialization End\n"));

    return 0;

}


int Match::sort(FL * p_sorted, int p_n, int p_dim, int *p_orderout)
{

    if (!p_sorted) {
	FATAL_(fprintf(stderr, "Can not copy result to an empty vector\n"),
	       FATAL_MEMORY_ALLOCATION);
    }
    //  print_flmatrix(sorted,"tmp/sorted", n, vdim);  
    int *l_order = NULL;
    if (p_orderout == NULL) {
	l_order = new int[p_n * p_dim];

    } else {
	l_order = p_orderout;
    }

    memset(l_order, 0, p_n * p_dim * sizeof(int));
    for (int i = 0; i < p_n; i++) {
	for (int d = 0; d < p_dim; d++) {
	    l_order[i * p_dim + d] = i;
	}
    }

    //sort each dimension of X (from big to small)
    for (int d = 0; d < p_dim; d++) {
	for (int i = 1; i < p_n; i++) {
	    int c = i * p_dim + d;	//points to current element
	    while ((c - p_dim >= 0) && (p_sorted[c] > p_sorted[c - p_dim])) {	//c-dim is equivalent to (i-1)*dim
		FL l_aux = p_sorted[c];
		p_sorted[c] = p_sorted[c - p_dim];
		p_sorted[c - p_dim] = l_aux;
		int l_aux_ord = l_order[c];
		l_order[c] = l_order[c - p_dim];
		l_order[c - p_dim] = l_aux_ord;
		c -= p_dim;
	    }
	}
    }
    if (p_orderout == NULL) {
	delete[]l_order;
    }
    return 0;
}


int Match::matchEigenFunc()
{

    if (!vec_hist_X)
	vec_hist_X = new FL[numOfBins * data_dim];
    bzero(vec_hist_X, numOfBins * data_dim * sizeof(FL));

    if (!vec_hist_Y)
	vec_hist_Y = new FL[numOfBins * data_dim];
    bzero(vec_hist_Y, numOfBins * data_dim * sizeof(FL));

    if (!vec_hist_FY)
	vec_hist_FY = new FL[numOfBins * data_dim];
    bzero(vec_hist_FY, numOfBins * data_dim * sizeof(FL));

    C_Histo.setParameters(data_dim, numOfBins, template_filename);
    C_Histo.fillAllHistograms(X, data_mask_X, Y, data_mask_Y, vec_hist_X,
			      vec_hist_Y, vec_hist_FY, nX, nY);

    #ifdef SAVE_TEMP_DATA
    print_flmatrix(vec_hist_X, "tmp/vec_hist_X", data_dim, numOfBins);
    print_flmatrix(vec_hist_Y, "tmp/vec_hist_Y", data_dim, numOfBins);
    print_flmatrix(vec_hist_FY, "tmp/vec_hist_Flipped", data_dim,
		   numOfBins);
#endif


    FL *l_error_hist = new FL[data_dim * data_dim];
    memset(l_error_hist, 0, data_dim * data_dim * sizeof(FL));
    FL *l_error_hist_Flipped = new FL[data_dim * data_dim];
    memset(l_error_hist_Flipped, 0, data_dim * data_dim * sizeof(FL));

    FL *l_assignment_hist = new FL[data_dim];
    memset(l_assignment_hist, 0, data_dim * sizeof(FL));

    FL l_cost = 0;
    int l_new_dim = data_dim;
    int *l_dim_mask = new int[data_dim];
    for (int i = 0; i < data_dim; ++i) {
	l_dim_mask[i] = 1;
    }


    C_Histo.distance(histogram_distance,
		     vec_hist_X, vec_hist_Y, vec_hist_FY,
		     l_error_hist, l_error_hist_Flipped);

    C_Histo.assign(assignment_method,
		   l_assignment_hist, &l_cost,
		   l_error_hist, l_error_hist_Flipped, T);


    if (!initT) {
	init_dim = data_dim;
	initT = new int[(data_dim + 1) * (data_dim + 1)];
	memset(initT, 0, (data_dim + 1) * (data_dim + 1) * sizeof(int));
	for (int d = 0; d < data_dim; d++) {
	    for (int e = 0; e < data_dim; e++) {
		initT[d * (data_dim + 1) + e] =
		    (int) T[d * (data_dim + 1) + e];
	    }
	}
    } else {
	WARNING_(std::
		 cout << "Using allready loaded match matrix: " << std::
		 endl);
    }

    if (discriminant_ratio > 0.0) {
	PRINT_COLOR_(fprintf(stderr, "Applying discriminant selection \n"),
		     GREEN);
	C_Histo.searchNBestAssignment(discriminant_ratio, l_error_hist, T,
				      data_dim, &l_new_dim, l_dim_mask);
    }


    delete[]l_assignment_hist;
    delete[]l_error_hist_Flipped;
    delete[]l_error_hist;

    if (remove_first) {

	WARNING_(fprintf
		 (stderr,
		  "When saving the initT, we add the 0th eigen vector match, even if remove_first activated\n"));
	FL *l_Ttemp = NULL;
	l_Ttemp = new FL[(data_dim + 2) * (data_dim + 2)];
	memset(l_Ttemp, 0, (data_dim + 2) * (data_dim + 2) * sizeof(FL));
	l_Ttemp[0] = 1;
	for (int i = 0; i < data_dim + 1; ++i) {
	    for (int j = 0; j < data_dim + 1; ++j) {
		l_Ttemp[(i + 1) * (data_dim + 2) + (j + 1)] =
		    T[i * (data_dim + 1) + j];
	    }
	}

	strcpy(filename, template_filename);
	strcat(filename, ".initT");
	print_flmatrix(l_Ttemp, filename, data_dim + 2, data_dim + 2);

	delete[]l_Ttemp;

    } else {
	strcpy(filename, template_filename);
	strcat(filename, ".initT");
	print_flmatrix(T, filename, data_dim + 1, data_dim + 1);
    }

    transform();

    //copy result to Y;
    memcpy(Y, Yt, data_dim * nY * sizeof(FL));

    dimensionSelection(l_new_dim, l_dim_mask);
    memcpy(Yt, Y, data_dim * nY * sizeof(FL));

    delete[]l_dim_mask;

    return 0;
}


int Match::loadTerminationCriteria(int p_maxiter, FL p_minsigma,
				   FL p_minerror, FL p_minchangeT)
{

    iter = -1;			//to asure sigma will be properly initialized


    max_iter = p_maxiter;

    if (n_outliers) {
	delete[]n_outliers;
	n_outliers = NULL;
    }
    n_outliers = new int[max_iter];
    memset(n_outliers, 0, max_iter * sizeof(int));

    if (n_unmatched) {
	delete[]n_unmatched;
	n_unmatched = NULL;
    }
    n_unmatched = new int[max_iter];
    memset(n_unmatched, 0, max_iter * sizeof(int));

    if (error) {
	delete[]error;
	error = NULL;
    }
    error = new FL[max_iter];
    memset(error, 0, max_iter * sizeof(FL));

    if (changeT) {
	delete[]changeT;
	changeT = NULL;
    }
    changeT = new FL[max_iter];
    memset(changeT, 0, max_iter * sizeof(FL));

	
	
	
    min_sigma = p_minsigma;
    min_changeT = p_minchangeT;
    min_error = p_minerror;

    PRINT_COLOR_(fprintf
		 (stderr,
		  "Loading termination criteria: \n \tmaixter: %d,\n\tminsigma: %g,\n\tminerror: %g,\n\tminchangeT: %g\n",
		  max_iter, min_sigma, min_error, min_changeT), RED);
	
    return 0;
}



//-------------------------------------------------------------------------------------
// SIGMA AND COVARIANCES and statistics
//-------------------------------------------------------------------------------------
int Match::factorial(int p_number)
{
    int l_temp;
    if (p_number <= 1)
	return 1;
    l_temp = p_number * factorial(p_number - 1);
    return l_temp;
}







int Match::findNN(int *p_result, FL * p_vec1, int p_n1, FL * p_vec2,
		  int p_n2, int p_dim, int p_nn)
{

    PRINT_COLOR_(std::cout << "Finding Nearest neighbors" <<std::endl, RED) ;

    //for each point in vec1 find its nn nearest neighbors
    //bestnn is the resultant (n1xnn) vector

    if (!p_result) {
	FATAL_(fprintf
	       (stderr,
		"vector (n1*nn) indexes should be allocated first\n"),
	       FATAL_MEMORY_ALLOCATION);
    }

    bzero(p_result, p_n1 * p_nn * sizeof(int));
    FL *l_distnn = NULL;
    int *l_bestnn = NULL;
    l_distnn = new FL[p_nn];	//distances of the best nn found so far
    l_bestnn = new int[p_nn];
    int l_worse = 0;		//index of the worst neighbor so far

    for (int i = 0; i < p_n1; i++) {

	bzero(l_distnn, p_nn * sizeof(FL));
	bzero(l_bestnn, p_nn * sizeof(int));
	l_worse = 0;

	for (int j = 0; j < p_n2; j++) {
	    FL l_dist = 0.0;
	    for (int d = 0; d < p_dim; d++) {
		FL l_dij = p_vec1[i * p_dim + d] - p_vec2[j * p_dim + d];
		l_dist += (l_dij) * (l_dij);
	    }
	    if (j < p_nn) {	//just fill the first nn values
		l_distnn[j] = l_dist;
		l_bestnn[j] = j;
		if (l_dist > l_distnn[l_worse]) {
		    l_worse = j;
		}
	    } else {		//only replace if best than worse found so far
		if (l_dist < l_distnn[l_worse]) {
		    l_distnn[l_worse] = l_dist;
		    l_bestnn[l_worse] = j;
		    //find new worse best neighbor
		    for (int k = 0; k < p_nn; k++) {
			if (l_distnn[k] > l_distnn[l_worse]) {
			    l_worse = k;
			}
		    }
		}
	    }
	}
	for (int k = 0; k < p_nn; k++) {
	    p_result[i * p_nn + k] = l_bestnn[k];
	}
    }


    if (l_distnn)
	delete[]l_distnn;

    if (l_bestnn)
	delete[]l_bestnn;

    PRINT_COLOR_(std::cout << "End()" <<std::endl, GREEN) ;
    return 0;
}


int Match::initCovFromNN(int p_nn)
{
    PRINT_COLOR_(fprintf
		 (stderr,
		  "Initializing cov from %d nearest neighbors between X and QYt for sigma initialization\n",
		  p_nn), GREEN);



    if ( Yt_modified ) {
	if (closest_near_nei_YtX == NULL) {
	    closest_near_nei_YtX= new int[nY * p_nn] ;
	}
	findNN(closest_near_nei_YtX, Yt, nY, X, nX, data_dim, p_nn) ;
	Yt_modified = false ;
    }

    FL *l_diff = NULL;
    l_diff = new FL[nY * p_nn * data_dim];
    bzero(l_diff, nY * p_nn * data_dim * sizeof(FL));

    FL *l_diff_ = l_diff;
    FL *l_yt_ = Yt;
    int ind_nn = 0;
    for (int j = 0; j < nY; j++, l_yt_ += data_dim) {
	for (int k = 0; k < nY; k++) {
	    int indi = closest_near_nei_YtX[ind_nn + k] * data_dim;
	    for (int d = 0; d < data_dim; d++, ++l_diff_) {
		*(l_diff_) = *(l_yt_ + d) - X[indi + d];
	    }
	}

	ind_nn += p_nn;
    }
    FL *l_stdv = new FL[data_dim];
    std(nY * p_nn, data_dim, l_diff, l_stdv);


    int l_max = 0;
    for (int d = 1; d < data_dim; d++) {
	if (l_stdv[d] > l_stdv[l_max]) {
	    l_max = d;
	    DBG_(fprintf(stderr, "max std[%d]: %g\n", d, l_stdv[d]));
	}
    }
    sigma = l_stdv[l_max];
    PRINT_COLOR_(fprintf(stderr, "sigma from Cov NN %g \n", sigma), RED);
    sigma_initialized = true;

    delete[]l_diff;
    delete[]l_stdv;
    return 0;
}





int Match::initSigma( SIGMA_INIT_TYPE p_type) {

    switch (p_type) {
    case FROM_XY:
	computeSigmaFromXY();
	break ;
    case FROM_XYT:
	computeSigmaFromXYt() ;
	break ;
    case FROM_WYT:	
	computeSigmaFromWYt();
	break ;
    case FROM_NN: {
      int l_num_of_nearest_nei = 0 ;
      HASH_MAP_PARAM_(int, "num_of_nearest_nei", Int, l_num_of_nearest_nei) ;
      computeSigmaFromNN (l_num_of_nearest_nei) ;
    }
	break ;
    default:
	ERROR_(fprintf (stderr,"UNKNOWN SIGMA computation mode\n"),     ERROR_UNKNOWN_MODE ) ;
    }

	return 0;

}




int Match::computeSigmaInit(FL * p_A, int p_na, FL * p_B, int p_nb)
{
    int l_n = p_nb;

    if (p_na < p_nb)
	l_n = p_na;

    FL *l_diff = new FL[l_n * data_dim];
    bzero(l_diff, l_n * data_dim * sizeof(FL));
    int ind = 0;
    FL *l_stdv = new FL[data_dim];

    if (use_alpha_line) {
	for (int j = 0; j < l_n; j += line_sample) {
	    for (int d = 0; d < data_dim; d++) {
		ind = j * data_dim + d;
		l_diff[ind] = p_B[ind] - p_A[ind];
	    }
	}
    }
    else  {
	for (int j = 0; j < l_n; j++) {
	    for (int d = 0; d < data_dim; d++) {
		ind = j * data_dim + d;
		l_diff[ind] = p_B[ind] - p_A[ind];
	    }
	}
    }

    if (use_alpha_line) {
	int l_n1 = l_n / line_sample;
	std(l_n1, data_dim, l_diff, l_stdv);
    }
    else {
	std(l_n, data_dim, l_diff, l_stdv);
    }

#ifdef SAVE_TEMP_DATA
    print_flmatrix(l_stdv, "tmp/stdv", 1, data_dim);
#endif

    int l_max = 0;
    for (int d = 1; d < data_dim; d++) {
	PRINT_(fprintf(stderr, " std[%d]: %g\n", d, l_stdv[d]));
	if (l_stdv[d] > l_stdv[l_max]) {
	    l_max = d;
	    DBG_(fprintf(stderr, "max std[%d]: %g\n", d, l_stdv[d]));
	}
    }
    sigma = l_stdv[l_max];
    sigma_out = sigma;

    delete[]l_diff;
    delete[]l_stdv;
    sigma_initialized = true;


    return 0;


}

int Match::computeSigmaFromNN(int p_nn)
{

    if  (p_nn != n_max_match ) {
	Yt_modified = true ;
	delete [] closest_near_nei_YtX ;
	closest_near_nei_YtX = NULL ;
    }
    if ( Yt_modified || (p_nn != n_max_match )) {
	std::cout << p_nn << " " << n_max_match <<std::endl ;

	PRINT_(fprintf
	       (stderr,
		"Finding nearest neighbors between X and QYt for sigma initialization\n"));
	if (closest_near_nei_YtX == NULL) {
	    closest_near_nei_YtX= new int[nY * p_nn] ;
	}
	findNN(closest_near_nei_YtX, Yt, nY, X, nX, data_dim, p_nn);
	Yt_modified = false ;
    }
#ifdef SAVE_TEMP_DATA
    print_intmatrix(l_closenn, "tmp/nearest_neighbors", nY, p_nn);
#endif

    FL *l_diff = NULL;
    l_diff = new FL[nY * p_nn * data_dim];
    bzero(l_diff, nY * p_nn * data_dim * sizeof(FL));

    FL *l_diff_ = l_diff;
    FL *l_yt_ = Yt;
    int ind_nn = 0;
    for (int j = 0; j < nY; j++, l_yt_ += data_dim) {

	for (int k = 0; k < p_nn; k++) {
	    if (closest_near_nei_YtX[ind_nn + k] > nX) {
		WARNING_(fprintf
			 (stderr,
			  "Error in calculation of nearest neighbors %d should be < %d \n",
			  closest_near_nei_YtX[ind_nn + k], nX));
		continue;
	    }
	    int indi = closest_near_nei_YtX[ind_nn + k] * data_dim;
	    for (int d = 0; d < data_dim; d++, ++l_diff_) {
		*(l_diff_) = *(l_yt_ + d) - X[indi + d];
	    }
	}
	ind_nn += p_nn;
    }

    FL *l_stdv = new FL[data_dim];
    std(nY * p_nn, data_dim, l_diff, l_stdv);

    int l_max = 0;
    for (int d = 1; d < data_dim; d++) {
	if (l_stdv[d] > l_stdv[l_max]) {
	    l_max = d;
	    DBG_(fprintf(stderr, "max std[%d]: %g\n", d, l_stdv[d]));
	}
    }
    sigma = l_stdv[l_max];
    PRINT_COLOR_(fprintf(stderr, "sigma from NN %g \n", sigma), RED);
    sigma_initialized = true;

    delete[]l_diff;
    delete[]l_stdv;

    return 0;
}



int Match::computeSigmaFromWYt()
{

    PRINT_COLOR_(fprintf
		 (stderr,
		  "Initializing sigma from the error between Yt and W \n"),
		 GREEN);

    computeSigmaInit(W, nY, Y, nY);

    PRINT_COLOR_(fprintf(stderr, "sigma from WYt %g \n", sigma), RED);

    return 0;
}



int Match::computeSigmaFromXY()
{

    PRINT_COLOR_(fprintf
		 (stderr,
		  "Initializing sigma from the error between Y and X \n"),
		 GREEN);

    computeSigmaInit(X, nX, Y, nY);

    PRINT_COLOR_(fprintf(stderr, "sigma from XY %g \n", sigma), RED);

    return 0;
}

int Match::computeSigmaFromXYt()
{
    PRINT_COLOR_(fprintf
		 (stderr,
		  "Initializing sigma from the error between Yt and X \n"),
		 GREEN);

    computeSigmaInit(X, nX, Yt, nY);


    PRINT_COLOR_(fprintf(stderr, "sigma from XYt %g \n", sigma), RED);

    return 0;
}



int Match::initCov()
{
    //Assumes sigma has been already initialized and filss covmat with a sigma diagonal
    if (!sigma_initialized) {
	FATAL_(fprintf
	       (stderr,
		"Sigma has to be initialized before initializing covariance matrix"),
	       FATAL_VARIABLE_INIT);
    }

    memset(covmat, 0, data_dim * data_dim * sizeof(FL));
    for (int d = 0; d < data_dim; d++) {
	covmat[d * data_dim + d] = sigma;
    }

#ifdef SAVE_TEMP_DATA
    print_flmatrix(covmat, "tmp/covmat", data_dim, data_dim);
#endif

    return 0;
}



int Match::initMinSigma () {
    computeClusterMeanAndMaxDistance () ;

    min_sigma = max_closest_dist ;


    return 0;

}



int Match::updateMinSigma()
{

    PRINT_COLOR_(fprintf
		 (stderr,
		  "Estimating min-sigma from standard deviation of the best match/matches: (X,Y)\n"),
		 GREEN);

    min_sigma = computeSigmaFromMatches () ;
    PRINT_COLOR_(fprintf(stderr, "New min sigma %g \n", min_sigma), RED);

    return 0;

}



FL Match::computeSigmaFromMatches()
{


    FL *l_diff = NULL;
    int *l_match_mask = NULL;
    int l_idiff;

    // estimate differences and match mask
    int indx, indy;

    //prepare to store the stdv
    FL *l_stdv = new FL[data_dim];

    if (!nag_match) {
	l_diff = new FL[nX * data_dim];
	memset(l_diff, 0, nX * data_dim * sizeof(FL));

	l_match_mask = new int[nX];
	memset(l_match_mask, 0, nX * sizeof(int));

	for (int i = 0; i < nX; i++) {
	    indx = ag_match[i * 2];
	    indy = ag_match[i * 2 + 1];
	    if ((indx != -1) && (indy != -1)) {
		l_match_mask[i] = 1;
		l_idiff = i * data_dim;
		for (int d = 0; d < data_dim; d++) {
		    l_diff[l_idiff + d] =
			X[indx * data_dim + d] - Yt[indy * data_dim + d];
		}
	    }
	}

#ifdef SAVE_TEMP_DATA
	print_flmatrix(l_diff, "tmp/x-yt", nX, data_dim);
	print_intmatrix(l_match_mask, "tmp/mask_match", nX, 1);
#endif 
	std(nX, data_dim, l_diff, l_stdv, l_match_mask);
    } else {
	l_diff = new FL[nY * n_max_match * data_dim];
	memset(l_diff, 0, nY * n_max_match * data_dim * sizeof(FL));

	l_match_mask = new int[nY * n_max_match];
	memset(l_match_mask, 0, nY * n_max_match * sizeof(int));
	for (int j = 0; j < n_max_match * nY; j++) {
	    indy = nag_match[j * 2];
	    indx = nag_match[j * 2 + 1];
	    if ((indy >= nY) || (indx >= nX)) {
		WARNING_(fprintf
			 (stderr,
			  "Indexes found by nagmatch are incoherent %d > %d, or %d > %d\n",
			  indx, nX, indy, nY));
		continue;
	    }
	    if ((indx != -1) && (indy != -1)) {
		l_match_mask[j] = 1;
		l_idiff = j * data_dim;
		for (int d = 0; d < data_dim; d++) {
		    l_diff[l_idiff + d] =
			X[indx * data_dim + d] - Yt[indy * data_dim + d];
		}
	    }

	}
#ifdef SAVE_TEMP_DATA
	print_flmatrix(l_diff, "tmp/x-yt", n_max_match * nY, data_dim);
	print_intmatrix(l_match_mask, "tmp/mask_match", n_max_match * nY,
			1);
#endif
	std(n_max_match * nY, data_dim, l_diff, l_stdv, l_match_mask);
    }

    FL l_isostd = 0.0;
    for (int d = 0; d < data_dim; d++) {
	l_isostd += l_stdv[d];
    }
    l_isostd /= (FL) data_dim;




    delete[]l_stdv;
    delete[]l_diff;
    delete[]l_match_mask;

    sigma_initialized = true ;

    return l_isostd ;


}

FL Match::updateSigma() {

    PRINT_COLOR_(std::cout << "Updating sigma" <<std::endl,GREEN) ;

    FL alpha_sum = 0 ;
    FL l_sigma = 0 ;
    FL l_dist = 0 ;
    FL l_diff = 0 ;

    FL l_dist_max = 0 ;
    FL l_dist_min = 100000000 ;
    FL l_mean_dist = 0 ;
    FL l_alpha_sum = 0 ;
    if (mat_alpha) {
	for (int  i = 0 ; i < nX ; ++i ) { 
	    l_alpha_sum = 0 ;
	    for (int  j = 0 ; j < nY ; ++j ) {
		l_dist = 0 ;
		for (int d = 0; d < data_dim; d++) {
		    l_diff = X[i * data_dim + d] - Yt[j * data_dim + d] ;
		    l_dist += l_diff * l_diff ;
		}
		l_mean_dist += l_dist ;

		if ( l_dist_max < l_dist )
		    l_dist_max = l_dist ;
		if ( l_dist_min > l_dist )
		    l_dist_min = l_dist ;

		l_alpha_sum += mat_alpha[i][j] ;

		l_sigma += mat_alpha[i][j] * l_dist ;

		alpha_sum += mat_alpha[i][j] ;
	    }
		
	}
	l_sigma = l_sigma / ( data_dim * alpha_sum) ;

	//	std::cout << std::endl ;
    }
    else {

	std::cout << "Alpha is NOT taken into account" <<std::endl ;
	FATAL_(fprintf(stderr, "Alpha(ij) have not been computed before updating sigma. \n"), FATAL_VARIABLE_INIT) ;

    }

    DBG_(std::cout << "MAXIMUM DISTANCE IS " << l_dist_max << " MIN DIST IS : " << l_dist_min << " MEAN DIST IS : " << l_mean_dist / (nX * nY) << std::endl) ;

    min_sigma = l_dist_min / 2 ;
    sigma_initialized = true ;
    return l_sigma ;

}


int Match::updateCov()
{
    //Covariance matrix from Yt- W with mask_Y
    FL *l_diff = new FL[nY * data_dim];
    int ind = 0;
    for (int j = 0; j < nY; j++) {
	for (int d = 0; d < data_dim; d++, ind++) {
	    //ind = j*data_dim+d;
	    l_diff[ind] = Yt[ind] - W[ind];
	}
    }
    covariance(nY, data_dim, l_diff, mask_Y);

    delete[]l_diff;

    return 0;
}

int Match::mean(int p_n, int p_dim, const FL * p_vec, FL * p_mean,
		int *p_mask)
{

    bzero(p_mean, p_dim * sizeof(FL));

    int l_count = 0;
    int l_mask_i;
    for (int i = 0; i < p_n; i++) {
	if (!p_mask)		// || ((mask)&&(mask[i]))
	    l_mask_i = 1;
	else
	    l_mask_i = p_mask[i];
	if (l_mask_i == 1) {
	    for (int d = 0; d < p_dim; d++)
		p_mean[d] += p_vec[i * p_dim + d];
	    l_count++;
	}
    }

    for (int d = 0; d < p_dim; d++) {
	p_mean[d] /= (FL) l_count;
    }

    return 0;
}



int Match::std(int p_n, int p_dim, const FL * p_vector, FL * p_stdv,
	       int *p_mask, FL * p_mean)
{

    int l_no_mean = 0;
    FL *l_mean = NULL;

    //mean
    if (p_mean == NULL) {
	l_mean = new FL[p_dim];
	l_no_mean = 1;
	mean(p_n, p_dim, p_vector, l_mean, p_mask);
#ifdef SAVE_TEMP_DATA
	//      print_flmatrix(l_mean,"tmp/mean",1,dim);
#endif
    } else
	l_mean = p_mean;
#ifdef SAVE_TEMP_DATA
    print_intmatrix(mask,"tmp/mask_Xy",n,1);
#endif
    //init std
    bzero(p_stdv, p_dim * sizeof(FL));
    int l_count = 0;

    //std
    FL l_diff;
    int l_mask_i;
    for (int i = 0; i < p_n; i++) {
	if (p_mask)
	    l_mask_i = p_mask[i];
	else
	    l_mask_i = 1;
	if (l_mask_i == 1) {
	    for (int d = 0; d < p_dim; d++) {
		l_diff = p_vector[i * p_dim + d] - l_mean[d];
		p_stdv[d] += (l_diff) * (l_diff);
	    }
	    l_count++;
	}
    }

    for (int d = 0; d < p_dim; d++) {
	p_stdv[d] /= (FL) (l_count - 1);
	DBG_(fprintf(stderr, "std[%d]: %g\n", d, p_stdv[d]));
    }

    if (l_no_mean == 1)
	delete[]l_mean;
    return 0;
}



int Match::covariance(int p_n, int p_dim, const FL * p_vector, int *p_mask,
		      FL * p_mean)
{

    int l_no_mean = 0;
    FL *l_mean = NULL;
    if (p_mean == NULL) {
	l_mean = new FL[p_dim];
	l_no_mean = 1;
	mean(p_n, p_dim, p_vector, l_mean, p_mask);
    } else
	l_mean = p_mean;

    //init cov
    bzero(covmat, p_dim * p_dim * sizeof(FL));

    int l_mask_i;
    FL *l_diff = NULL;
    l_diff = new FL[p_dim];
    bzero(l_diff, p_dim * sizeof(FL));
    int l_count = 0;
    for (int i = 0; i < p_n; i++) {
	if (p_mask)
	    l_mask_i = p_mask[i];
	else
	    l_mask_i = 1;
	if (l_mask_i == 1) {
	    for (int d = 0; d < p_dim; d++)
		l_diff[d] = p_vector[i * p_dim + d] - l_mean[d];
	    for (int d = 0; d < p_dim; d++) {
		for (int k = 0; k < p_dim; k++) {
		    covmat[d * p_dim + k] += l_diff[d] * l_diff[k];
		}
	    }
	    l_count++;
	}
    }

    for (int k = 0; k < p_dim * p_dim; k++) {
	covmat[k] /= (FL) (l_count);
    }
    if (l_diff)
	delete[]l_diff;
    if (l_no_mean == 1)
	delete[]l_mean;

    return 0;
}



void Match::computeClusterMeanAndMaxDistance () {
    int *l_closenn = NULL;
    l_closenn = new int[nY * 2];	//indexes of the best nn found so far
    findNN( l_closenn, Y, nY, Y, nY, data_dim, 2 );
    FL l_dist = 0 ;
    FL l_diff ;
    FL l_max_dist = 0 ;
    for (int i = 0 ; i < nY ; ++i) {
	for (int k = 0 ; k < 2 ; ++k ) {
	    for (int d = 0 ; d < data_dim ; ++d) {
		l_diff = Y[i * data_dim + d ] - Y[l_closenn[(i * 2 + k)] * data_dim + d ] ;
		l_dist += l_diff * l_diff ;				
	    }
	    mean_cluster_dist += l_dist ;
	    if (l_max_dist < l_dist)
		l_max_dist = l_dist ;
	    l_dist = 0 ;
		
	}
    }
    mean_cluster_dist /=  nY ;
    max_closest_dist = l_max_dist ;
    PRINT_COLOR_(std::cout << "Mean distance between cluster nodes is : " << mean_cluster_dist << " Max closest distance is : " << max_closest_dist << std::endl, RED) ;
}



//----------------------------------------------------------------------
// MATCHING
//---------------------------------------------------------------------

bool Match::isMatchAvailable()
{
    return match_available;
}


// C-Function provided to g_hash_table_new_full in build_dist_mat.
// It allows to free the memory allocated for the hash table.
void DeleteHashTable_SparseStorage(gpointer data)
{
    if (data) {
	delete(SparseStorage *) data;
    }
    data = NULL;

}





// To be saved in all_match
int Match::computeNArgMaxMatch()
{

    PRINT_COLOR_(fprintf
		 (stderr,
		  "-->Finding the %d most probable matches (for each class Yj, contributing observations Xi)\n",
		  n_max_match), GREEN);
    if (nag_match)
	delete[]nag_match;
    nag_match = new int[nY * n_max_match * 2];



    int l_min_best_ind;		// the index of the worst (lowest ) value found so far
    FL l_min_best_val;		// the worst (lowest ) value found so far

    for (int k = 0; k < 2 * n_max_match * nY; k++) {
	nag_match[k] = -1;
    }

    int j = 0;

#ifdef ENABLE_EM_PARALLELS
    int l_NumOfThreads = omp_get_max_threads();
    int l_chunk = 100;
#else
    int l_NumOfThreads = 1;
#endif

    int **l_best = new int *[l_NumOfThreads];	//indexes of greatest values found so far in the row alpha(i,:)

    for (int i = 0; i < l_NumOfThreads; ++i) {
	l_best[i] = new int[n_max_match];

    }
#ifdef ENABLE_EM_PARALLELS
#pragma omp parallel shared(l_chunk) private( j, l_min_best_val, l_min_best_ind )
    {

	int l_threadNum = omp_get_thread_num();
#else
	int l_threadNum = 0;
#endif
#ifdef ENABLE_EM_PARALLELS
#pragma omp for schedule(dynamic,l_chunk) nowait
#endif
	for (j = 0; j < nY; j++) {

	    int idx = j * 2 * n_max_match;

	    //look for the right matches if any
	    if (mask_Y[j] == 1) {

		//initialize best values and indexes
		for (int k = 0; k < n_max_match; k++)
		    l_best[l_threadNum][k] = -1;
		l_min_best_ind = 0;
		l_min_best_val = -1.0;

		for (int i = 0; i < nX; i++) {
		    //if match with higher probability is found
		    if ((mask_X[i] == 1)
			&& (mat_alpha[i][j] > l_min_best_val)) {
			l_best[l_threadNum][l_min_best_ind] = i;	//replace the worse match found so far

			//find the worst to be replaced next time

			

			l_min_best_val = mat_alpha[i][j];
			for (int k = 0; k < n_max_match; k++) {
			    if (l_best[l_threadNum][k] == -1) {	// not yet filled, use as worse
				l_min_best_ind = k;
				l_min_best_val = -1.0;	// TODO: Also added this line. Verify 
				break;	//TODO: I changed this from a continue. Verify is correct
			    }

			    if (mat_alpha[l_best[l_threadNum][k]][j] < l_min_best_val) {
				l_min_best_ind = k;
				l_min_best_val = mat_alpha[l_best[l_threadNum][k]][j] ; 
			    }
			}
		    }
		}
#ifdef DEBUG_2
		if (j % 1000 == 0) {
		    fprintf(stderr, "matches for [%d]:", j);
		    for (int k = 0; k < n_max_match; k++) {
			fprintf(stderr, "%d, ", l_best[l_threadNum][k]);
		    }
		    fprintf(stderr, "\n");
		}
#endif


		//copy ordered best to nag_match                        
		int ind_max = 0;
		int l_max;	//index of the current best match in decreasing order
		for (int k = 0; k < n_max_match; k++) {
		    l_max = -1;
		    ind_max = 0;
		    //find the index and the value of the best max
		    for (int l = 0; l < n_max_match; l++) {
			if (l_best[l_threadNum][l] == -1)
			    continue;
			if (l_max == -1) {	// if unset set to the current index
			    l_max = l;
			    ind_max = l_best[l_threadNum][l_max] * nY + j;
			} else if (mat_alpha[l_best[l_threadNum][l]][j] >
				   mat_alpha[l_best[l_threadNum][l_max]][j]) {
			    l_max = l;
			    ind_max = l_best[l_threadNum][l_max] * nY + j;
			}
		    }
		    if (l_max == -1)
			break;	// all available values copied

		    //j matches best[max]
		    int ind_idx = idx + 2 * k;
		    nag_match[ind_idx] = j;
		    nag_match[ind_idx + 1] = l_best[l_threadNum][l_max];
		    l_best[l_threadNum][l_max] = -1;

		    //		    if (nag_match[ind_idx] != nag_match[ind_idx + 1])
		    //			std::cout << "The ground truth is not respected... "<<nag_match[ind_idx] << " " << nag_match[ind_idx + 1]  <<std::endl ;

		}
	    }

#ifdef DEBUG
	    if (j % 1000 == 0) {
		fprintf(stderr, "mask[%d]: %d W[%d]: ", j, mask_Y[j], j);
		for (int d = 0; d < data_dim; d++)
		    fprintf(stderr, "%g ", W[j * data_dim + d]);
		fprintf(stderr, "\n");
		fprintf(stderr, "matches for [%d]:", j);
		for (int k = 0; k < n_max_match; k++) {
		    fprintf(stderr, "%d, ", nag_match[idx + 2 * k + 1]);
		}
		fprintf(stderr, "\n\n");
	    }
#endif
	}
#ifdef ENABLE_EM_PARALLELS
    }				// END Parallel
#endif



    for (int i = 0; i < l_NumOfThreads; ++i) {
	delete[]l_best[i];
    }
    delete[]l_best;

    match_available = true;
    return 0;
}



// To be saved in main_match. 
int Match::computeArgMaxMatch()
{

    PRINT_COLOR_(fprintf
		 (stderr,
		  "-->Finding most probable match (for each observation Xi max probability of assignment to a class Yj) \n"),
		 GREEN);

    int l_best_idx = 0;
    FL l_best_val;
    int l_match_idx = 0;
    int l_alpha_idx = 0;

    int i = 0;

#ifdef ENABLE_EM_PARALLELS
    int l_chunk = 100;
#pragma omp parallel shared(l_chunk) private( i, l_best_val, l_best_idx, l_match_idx, l_alpha_idx )
    {

#pragma omp for schedule(dynamic,l_chunk) nowait
#endif

	for (i = 0; i < nX; i++ /*, l_match_idx+=2 */ ) {
	    l_match_idx = 2 * i;
	    if (mask_X[i] == 1) {
		l_alpha_idx = i * nY;
		ag_match[l_match_idx] = i;
		l_best_val = 0.0;
		for (int j = 0; j < nY; j++, l_alpha_idx++) {
		    //		    std::cout <<  mat_alpha[i][j] << " " ;

		    if ((mask_Y[j] == 1)
			&& (mat_alpha[i][j] > l_best_val)) {
			// std::cout << "(" << j << ") " ;
			l_best_idx = j;
			l_best_val = mat_alpha[i][j] ;
		    }
		    /*
		      else if (mask_Y[j] == 0) {
		      std::cout << "mask_Y of " << j << " is set to 0" <<std::endl ; 
		      //			getchar () ;
		      }
		    */
		}


		//		std::cout << std::endl ;
		ag_match[l_match_idx + 1] = l_best_idx;
	    } else {
		ag_match[l_match_idx] = -1;
		ag_match[l_match_idx + 1] = -1;
	    }
	}

#ifdef ENABLE_EM_PARALLELS
    }
#endif
    match_available = true;
    return 0;
}

//----------------------------------------------------------------------
// PRINT AND SAVE
//---------------------------------------------------------------------


int Match::printArgMaxMatch(const char *filename)
{

    FILE *f = NULL;

    PRINT_COLOR_(fprintf(stderr, "Printing argmax match in %s\n", filename),BLACK);

    f = fopen(filename, "w");
    if (!f) {
	ERROR_(fprintf
	       (stderr, "Could not open file %s to save argmax matching\n",
		filename), ERROR_OPEN_FILE);
    }
    fprintf(f, "%d \t %d\n", nX + 2, 2);
    for (int i = 0; i < nX; i++) {
	if (ag_match[i * 2] >= nX || ag_match[i * 2 + 1] >= nY)
	    WARNING_(fprintf
		     (stderr,
		      "MATCH ERROR: assignment beyond the voxelsize (%d < %d)||(%d < %d) \n",
		      ag_match[i * 2], nX, ag_match[i * 2 + 1], nY));
	if (!xy_inversed)
	    fprintf(f, "%d \t %d \n", ag_match[i * 2],
		    ag_match[i * 2 + 1]);
	else
	    fprintf(f, "%d \t %d \n", ag_match[i * 2 + 1],
		    ag_match[i * 2]);

    }
    //to set up the size while reading
    if (!xy_inversed) {
	fprintf(f, "%d \t %d \n", nX - 1, -1);
	fprintf(f, "%d \t %d \n", -1, nY - 1);
    } else {
	fprintf(f, "%d \t %d \n", -1, nX - 1);
	fprintf(f, "%d \t %d \n", nY - 1, -1);
    }

    fclose(f);

    
    return 0;
}

int Match::printNArgMaxMatch(const char *filename)
{

    if (!nag_match) {
	ERROR_(fprintf
	       (stderr,
		"Can not print the n first matches. Call nargmax_match() first\n"),
	       ERROR_MEMORY_ALLOCATION);
    }

    PRINT_COLOR_(fprintf(stderr, "Printing nargmax match in %s\n", filename),BLACK);

    FILE *f = NULL;
    f = fopen(filename, "w");
    if (!f) {
	ERROR_(fprintf
	       (stderr, "Could not open file %s to save argmax matching\n",
		filename), ERROR_OPEN_FILE);
    }
    fprintf(f, "%d %d\n", nY * n_max_match + 2, 2);

    for (int j = 0; j < nY; j++) {
	int indj = j * n_max_match * 2;
	for (int k = 0; k < n_max_match; k++, indj += 2) {
	    if (nag_match[indj] >= nY || nag_match[indj + 1] >= nX)
		WARNING_(fprintf
			 (stderr,
			  "MATCH ERROR: assignment beyond the voxelsize (%d < %d)||(%d < %d) \n",
			  nag_match[indj], nX, nag_match[indj + 1], nY));
	    if (!xy_inversed)
		fprintf(f, "%d \t %d \n", nag_match[indj + 1],
			nag_match[indj]);
	    else
		fprintf(f, "%d \t %d \n", nag_match[indj],
			nag_match[indj + 1]);
	}
    }
    // to ensure that the right maximum index is printed 
    // When reading this should be detected automatically
    // treated as an outlier/unmatched 
    // since not rewriten when read 
    // leave this at the end (after printing the real values)
    if (!xy_inversed) {
	fprintf(f, "%d \t %d \n", -1, nY - 1);
	fprintf(f, "%d \t %d \n", nX - 1, -1);
    } else {
	fprintf(f, "%d \t %d \n", nY - 1, -1);
	fprintf(f, "%d \t %d \n", -1, nX - 1);
    }
    fclose(f);

        return 0;
}












int Match::printBestMatch(const char *filename)
{
    if (!best_one_to_one_match) {
	ERROR_(fprintf
	       (stderr,
		"Can not print the best matches... Call bestOneToOneMatching() first.\n"
		"ONLY AVAILABLE IN ALPHA LINE MODE\n"),
	       ERROR_MEMORY_ALLOCATION);
    }

    PRINT_COLOR_(fprintf(stderr, "Printing best match in %s\n", filename), BLACK);

    FILE *f = NULL;
    f = fopen(filename, "w");
    if (!f) {
	ERROR_(fprintf
	       (stderr, "Could not open file %s to save argmax matching\n",
		filename), ERROR_OPEN_FILE);
    }

    fprintf(f, "%d %d\n", nY, 2);

    for (int j = 0; j < nY; j++) {
	int indj = j * 2;
	if (!xy_inversed)
	    fprintf(f, "%d \t %d \n", best_one_to_one_match[indj],
		    best_one_to_one_match[indj + 1]);
	else
	    fprintf(f, "%d \t %d \n", best_one_to_one_match[indj + 1],
		    best_one_to_one_match[indj]);
    }

    // to ensure that the right maximum index is printed 
    // When reading this should be detected automatically
    // treated as an outlier/unmatched 
    // since not rewriten when read 
    // leave this at the end (after printing the real values)
    if (!xy_inversed) {
	fprintf(f, "%d \t %d \n", nY - 1, -1);
	fprintf(f, "%d \t %d \n", -1, nX - 1);
    } else {
	fprintf(f, "%d \t %d \n", -1, nY - 1);
	fprintf(f, "%d \t %d \n", nX - 1, -1);
    }
    fclose(f);


    return 0;
}





int Match::printOutputs()
{

    if (error) {
	strcpy(filename, template_filename);
	strcat(filename, ".error");
	print_flmatrix(error, filename, iter, 1);
    }

    if (changeT) {
	strcpy(filename, template_filename);
	strcat(filename, ".converg");
	print_flmatrix(changeT, filename, iter, 1);
    }

    if (n_outliers) {
	strcpy(filename, template_filename);
	strcat(filename, ".outl");
	print_intmatrix(n_outliers, filename, iter, 1);
    }

    if (n_unmatched) {
	strcpy(filename, template_filename);
	strcat(filename, ".unmatch");
	print_intmatrix(n_unmatched, filename, iter, 1);
    }
    return 0;
}

int Match::printMatches()
{

    strcpy(filename, template_filename);
    printArgMaxMatch(filename);

    sprintf(filename, "%s_%04d", template_filename, iter);
    printArgMaxMatch(filename);

    strcpy(filename, template_filename);
    strcat(filename, "_all");
    printNArgMaxMatch(filename);

    sprintf(filename, "%s_all_%04d", template_filename, iter);
    printNArgMaxMatch(filename);

    strcpy(filename, template_filename);
    strcat(filename, "_best");
    printBestMatch(filename);

    sprintf(filename, "%s_best_%04d", template_filename, iter);
    printBestMatch(filename);

    return 1;
}

int Match::printT()
{
    strcpy(filename, template_filename);
    strcat(filename, ".T");
    print_flmatrix(T, filename, data_dim + 1, data_dim + 1);

    sprintf(filename, "%s.T_%04d", template_filename, iter);
    print_flmatrix(T, filename, data_dim + 1, data_dim + 1);

    return 0;
}

//----------------------------------------------------------------------
// SET PARAMETERS
//---------------------------------------------------------------------


void Match::setStep(EM_STEP step_in)
{
    step = step_in;
}

void Match::setMode(EM_MODE mode_in)
{
    em_mode = mode_in;
}
int Match::setTransfoMode(TRANSFO_MODE mode_in )
{
    transfo_mode = mode_in;
    weighted = true;		//Use beta as weights
    centered = false;		//If true do not estimate translation
    rot_only = false;		//Forces the result to be a rotation
    PRINT_COLOR_(std::cout << "TRANSFO MODE: ", RED);
    switch (transfo_mode) {
    case ROTATION_ONLY:
	PRINT_COLOR_(std::cout << "ROTATION_ONLY" << std::endl, RED);
	centered = true;
	rot_only = true;
	break;
    case ORTHOGONAL:
	PRINT_COLOR_(std::cout << "ORTHOGONAL" << std::endl, RED);
	centered = true;
	rot_only = false;
	break;
    case ROT_AND_TRANS:
	PRINT_COLOR_(std::cout << "ROT_AND_TRANS" << std::endl, RED);
	centered = false;
	rot_only = true;
	break;
    case ORTH_AND_TRANS:
	PRINT_COLOR_(std::cout << "ORTH_AND_TRANS" << std::endl, RED);
	centered = false;
	rot_only = false;
	break;
	
    default:
	FATAL_(std::
	       cout << "Transfo mode not specified " << transfo_mode <<
	       std::endl, FATAL_UNKNOWN_MODE);
	break;
    }

    return 0;
}

int Match::setOutlierConstant(FL val)
{
    if (val <= 0.0) {
	ERROR_(fprintf
	       (stderr, "Outlier constant should be a positive number\n"),
	       ERROR_VARIABLE_INIT);
    }
    k_out = val;
    return 0;
}


int Match::setHistoParams(int numBins, HISTOGRAM_DISTANCE histDist,
			  ASSIGNMENT_METHOD assignMeth, FL disc_ratio)
{
    numOfBins = numBins;
    histogram_distance = histDist;
    assignment_method = assignMeth;
    discriminant_ratio = disc_ratio;
    char l_assign[100];
    char l_dist[100];

    switch (histogram_distance) {
    case BINTOBIN:
	strcpy(l_dist, "BIN_TO_BIN");
	break;
#ifdef USE_DIFFUSION_DISTANCE
    case DIFFUSION:
	strcpy(l_dist, "DIFFUSION");
	break;
#endif
#ifdef USE_EMDL1_DISTANCE
    case EMDL1:
	strcpy(l_dist, "EMD L1");
	break;
#endif
    default:
	strcpy(l_dist, "UNKNOWN MODE");
	break;
    }


    switch (assignMeth) {
    case OPTIMAL:
	strcpy(l_assign, "OPTIMAL");
	break;
    case SUBOPTIMAL1:
	strcpy(l_assign, "SUB-OPTIMAL 1");
	break;
    case SUBOPTIMAL2:
	strcpy(l_assign, "SUB-OPTIMAL 2");
	break;
    case FABIO:
	strcpy(l_assign, "FABIO IDEA");
	break;
    default:
	strcpy(l_assign, "UNKNOWN MODE");
	break;
    }

    PRINT_COLOR_(fprintf(stderr, "Loading Histogram parameters:\n"
			 "\tHistogram Distance Method: %s,\n"
			 "\tAssignment Method: %s,\n"
			 "\tDiscriminant Ratio: %g\n",
			 l_dist, l_assign, discriminant_ratio), RED);

    return 0;
}

int Match::setSigma(FL val)
{
    if (val < 0.0) {
	ERROR_(fprintf(stderr, "sigma should be a positive number\n"),
	       ERROR_VARIABLE_INIT);
    }
    if (val == 0) {
	PRINT_(fprintf(stderr, "Sigma will be automatically computed\n"));
	return 1;
    }
    sigma = val;
    sigma_initialized = true;
    return 0;
}


//----------------------------------------------------------------------
// ACCES TO INTERNAL VARIABLES
//---------------------------------------------------------------------

int Match::getDataDim()
{
    return data_dim;
}

int Match::getDimOfX()
{
    return nX;
}

int Match::getDimOfY()
{
    return nY;
}

FL *Match::getX()
{
    return X;
}

FL *Match::getY()
{
    return Y;
}

FL *Match::getYt()
{
    return Yt;
}

FL *Match::getW()
{
    return W;
}

int *Match::getMaskX()
{
    return mask_X;
}

int *Match::getMaskY()
{
    return mask_Y;
}

EM_STEP Match::getStep()
{
    return step;
}

EM_MODE Match::getMode()
{
    return em_mode;
}

int Match::getNMaxMatch()
{
    return n_max_match;
}

int *Match::getNAgMatch()
{
    return nag_match;
}

int *Match::getAgMatch()
{
    return ag_match;
}

FL Match::getSigma()
{
    return sigma;
}

CvMat *Match::getCovMatCV()
{
    return &covmat_;
}

int *Match::getInitT()
{
    return initT;
}

int Match::getInitDim()
{
    return init_dim;
}

int Match::getNumOfBins()
{
    return numOfBins;
}

TRANSFO_MODE Match::getTransfoMode()
{
    return transfo_mode;
}


void Match::setRemoveFirst(bool remove)
{
    remove_first = remove;
}


FL *Match::getVecHistX()
{

    return vec_hist_X;
}

FL *Match::getVecHistY()
{

    return vec_hist_Y;
}

FL *Match::getVecHistFY()
{

    return vec_hist_FY;
}





void Match::enableEvalSmoothness (bool on) {
    eval_smoothness = on ;
}






/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
/*
 *  neighbors.h
 *  embedmat
 *
 *  Created by Diana Mateus and David Knossow on 12/15/07.
 *  Copyright 2007 INRIA All rights reserved.
 *
 * This class provides functions to compute
 * the connectivity of a graph. In practice, we provide a max distance 
 * in termes of numbers of voxels around a given voxel. 
 * This class provides a connectivity matrix, in which for each line,
 * namely each voxel, the indices of the connected voxels are stored.
 *
 *
 */

#ifndef NEIGHBORS_H
#define NEIGHBORS_H
#include <fstream>
#include <string>
#include <iostream>


extern "C" {
#include "glib.h"
}
#include "QGLViewer/qglviewer.h"
#include "Constants.h"
#include "types.h"
#include "utilities.h"
class Neighborhood {
  private:
    // Store the connectivity in terms of weights on the edges. Actually, weights are, for now integer.
    FL * weights;
    FL **mat_weights;

    // number of voxels. Number of nodes of the graph.
    int nvoxels;

    // Maximum connected nodes to take into account.
    int depth;

    // Overall total of connected componnents (sum over all voxels of connected componnents).
    int nzelems;		// total number of neis for all voxels

    // Allocating memory using Hash Tables. Takes less space and optimize searches for later computations
    //GHashTable* hashdist ;
    GHashTable **hash_dist_vec;

    // same as nz_elems
    int hash_elems;

    //Initial connectivity
    int *num_nei;
    int *neighbors;

    //Connectivity after propagation
    int *num_nei_k;
    int *nei_k;

    // max number of neighbors for the requested depth. 
    int total_length;

    //Some nei files have a offset. Default is 0;
    int nei_offset ;

    int  max_num_neig ;


  public: Neighborhood();
    ~Neighborhood();



    // Release memory.
    void releaseUnused();

    //memory allocation functions
    void allocateWeights(bool matrix_on = false);

    //read connectivity file. This file contains the 1-ring neighbors for each voxel.
    int readNeighbors(char *filename);

    // Load neighbors from a provided array. It loads the 1-ring neighbors.
    int loadNeighbors(int *numNei_in, int *nei_in);

    //propagate neighborhoods 
    //fills weights together with matWeights. weights are ordered such that matWeights[i][0]
    //points to the first neighbor of i in weights, the other neighbors follow allways in
    //the sparse format (i1,j1,val1, i2,j2,val2, .... ). 
    int fillSparseMatWeights();

    // Calls subfunctions to compute the distance matrix. It is dedicated to the local geodesic distance.
    void computeDistGraph(int k);

    // Sort the matweight ;
    void sortMatWeight();


    //Internal functions for propagating neighbors for local geodesic distance computation
    int initHashDistNei(int max_depth);
    int propagateDist(int max_depth);

    // Print Functions
    void printSparseWeights(char *filename);

    // Check if the distance matrix is built.
    bool isLoaded();

    // Number of voxels, or nodes in the graph
    int getNumOfVoxels();

    // Number of non zero elements in the sparse distance matrix.
    int getNzelems();


    FL **getSparseGeodesicMatrix();	//matWeights in matrix format
    FL *getWeights();		//weights in vector format

    //acces to initial connecitivity
    int *getNumOfNearestNei();	//get number of nearest neighbors per element;
    int *getConnectivity();	//nvoxels * MAX_NUM_NEIG

    //access to connectivity after propagation
    GHashTable **getHashDistVec();
    int *getNumOfNei();
    int *getNei();
    int getTotalLength();

};

#endif

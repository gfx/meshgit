/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
#include <iostream>
#include <fstream>
using namespace std;

#include "histograms.h"


void Histogram::assignmentOptimal(FL * p_assignment, FL * p_cost,
				  FL * p_distMatrix, int p_nOfRows,
				  int p_nOfColumns)
{

    FL *l_distMatrix, *l_distMatrixTemp, *l_distMatrixEnd, *l_columnEnd,
	l_value, l_minValue;
    bool *l_coveredColumns, *l_coveredRows, *l_starMatrix,
	*l_newStarMatrix, *l_primeMatrix;
    int l_nOfElements, l_minDim, l_row, l_col;

#ifdef CHECK_FOR_INF
    bool l_infiniteValueFound;
    FL l_maxFiniteValue, l_infValue;
#endif

    /* initialization */
    *p_cost = 0;
    for (l_row = 0; l_row < p_nOfRows; l_row++)
	p_assignment[l_row] = -1;

    /* generate working copy of distance Matrix */
    /* check if all matrix elements are positive */
    l_nOfElements = p_nOfRows * p_nOfColumns;

    l_distMatrix = new FL[l_nOfElements];
    l_distMatrixEnd = l_distMatrix + l_nOfElements;
    for (l_row = 0; l_row < l_nOfElements; l_row++) {
	l_distMatrix[l_row] = p_distMatrix[l_row];
    }


    /* memory allocation */
    l_coveredColumns = new bool[p_nOfColumns];	// (bool *)mxCalloc(nOfColumns,  sizeof(bool));
    bzero(l_coveredColumns, p_nOfColumns * sizeof(bool));
    l_coveredRows = new bool[p_nOfRows];	//(bool *)mxCalloc(nOfRows,     sizeof(bool));
    bzero(l_coveredRows, p_nOfRows * sizeof(bool));
    l_starMatrix = new bool[l_nOfElements];	//(bool *)mxCalloc(nOfElements, sizeof(bool));
    bzero(l_starMatrix, l_nOfElements * sizeof(bool));
    l_primeMatrix = new bool[l_nOfElements];	//(bool *)mxCalloc(nOfElements, sizeof(bool));
    bzero(l_primeMatrix, l_nOfElements * sizeof(bool));
    l_newStarMatrix = new bool[l_nOfElements];	//(bool *)mxCalloc(nOfElements, sizeof(bool)); /* used in step4 */
    bzero(l_newStarMatrix, l_nOfElements * sizeof(bool));

    /* preliminary steps */
    if (p_nOfRows <= p_nOfColumns) {
	l_minDim = p_nOfRows;

	for (l_row = 0; l_row < p_nOfRows; l_row++) {
	    /* find the smallest element in the row */
	    l_distMatrixTemp = l_distMatrix + l_row;
	    l_minValue = *l_distMatrixTemp;
	    l_distMatrixTemp += p_nOfRows;
	    while (l_distMatrixTemp < l_distMatrixEnd) {
		l_value = *l_distMatrixTemp;
		if (l_value < l_minValue)
		    l_minValue = l_value;
		l_distMatrixTemp += p_nOfRows;
	    }

	    /* subtract the smallest element from each element of the row */
	    l_distMatrixTemp = l_distMatrix + l_row;
	    while (l_distMatrixTemp < l_distMatrixEnd) {
		*l_distMatrixTemp -= l_minValue;
		l_distMatrixTemp += p_nOfRows;
	    }

	}

	/* Steps 1 and 2a */
	for (l_row = 0; l_row < p_nOfRows; l_row++)
	    for (l_col = 0; l_col < p_nOfColumns; l_col++)
		if (l_distMatrix[l_row + p_nOfRows * l_col] == 0)
		    if (!l_coveredColumns[l_col]) {
			l_starMatrix[l_row + p_nOfRows * l_col] = true;
			l_coveredColumns[l_col] = true;
			break;
		    }
    } else {			/* if(nOfRows > p_nOfColumns) */

	l_minDim = p_nOfColumns;

	for (l_col = 0; l_col < p_nOfColumns; l_col++) {
	    /* find the smallest element in the column */
	    l_distMatrixTemp = l_distMatrix + p_nOfRows * l_col;
	    l_columnEnd = l_distMatrixTemp + p_nOfRows;

	    l_minValue = *l_distMatrixTemp++;
	    while (l_distMatrixTemp < l_columnEnd) {
		l_value = *l_distMatrixTemp++;
		if (l_value < l_minValue)
		    l_minValue = l_value;
	    }

	    /* subtract the smallest element from each element of the column */
	    l_distMatrixTemp = l_distMatrix + p_nOfRows * l_col;
	    while (l_distMatrixTemp < l_columnEnd)
		*l_distMatrixTemp++ -= l_minValue;
	}

	/* Steps 1 and 2a */
	for (l_col = 0; l_col < p_nOfColumns; l_col++)
	    for (l_row = 0; l_row < p_nOfRows; l_row++)
		if (l_distMatrix[l_row + p_nOfRows * l_col] == 0)
		    if (!l_coveredRows[l_row]) {
			l_starMatrix[l_row + p_nOfRows * l_col] = true;
			l_coveredColumns[l_col] = true;
			l_coveredRows[l_row] = true;
			break;
		    }
	for (l_row = 0; l_row < p_nOfRows; l_row++)
	    l_coveredRows[l_row] = false;

    }

    /* move to step 2b */
    step2b(p_assignment, l_distMatrix, l_starMatrix, l_newStarMatrix,
	   l_primeMatrix, l_coveredColumns, l_coveredRows, p_nOfRows,
	   p_nOfColumns, l_minDim);

    /* compute cost and remove invalid assignments */
    computeAssignmentCost(p_assignment, p_cost, p_distMatrix, p_nOfRows);

    /* free allocated memory */

    delete[]l_distMatrix;
    delete[]l_coveredColumns;
    delete[]l_coveredRows;
    delete[]l_starMatrix;
    delete[]l_primeMatrix;
    delete[]l_newStarMatrix;

    return;
}

/********************************************************/
void Histogram::buildAssignmentVector(FL * p_assignment,
				      bool * p_starMatrix, int p_nOfRows,
				      int p_nOfColumns)
{
    int l_row, l_col;

    for (l_row = 0; l_row < p_nOfRows; l_row++)
	for (l_col = 0; l_col < p_nOfColumns; l_col++)
	    if (p_starMatrix[l_row + p_nOfRows * l_col]) {
		p_assignment[l_row] = l_col;
		break;
	    }
}

/********************************************************/
void Histogram::computeAssignmentCost(FL * p_assignment, FL * p_cost,
				      FL * p_distMatrix, int p_nOfRows)
{
    int l_row, l_col;

    for (l_row = 0; l_row < p_nOfRows; l_row++) {
	l_col = (int) p_assignment[l_row];

	if (l_col >= 0) {
	    *p_cost += p_distMatrix[l_row + p_nOfRows * l_col];
	}
    }
}

/********************************************************/
void Histogram::step2a(FL * p_assignment, FL * p_distMatrix,
		       bool * p_starMatrix, bool * p_newStarMatrix,
		       bool * p_primeMatrix, bool * p_coveredColumns,
		       bool * p_coveredRows, int p_nOfRows,
		       int p_nOfColumns, int p_minDim)
{
    bool *l_starMatrixTemp, *l_columnEnd;
    int l_col;

    /* cover every column containing a starred zero */
    for (l_col = 0; l_col < p_nOfColumns; l_col++) {
	l_starMatrixTemp = p_starMatrix + p_nOfRows * l_col;
	l_columnEnd = l_starMatrixTemp + p_nOfRows;
	while (l_starMatrixTemp < l_columnEnd) {
	    if (*l_starMatrixTemp++) {
		p_coveredColumns[l_col] = true;
		break;
	    }
	}
    }

    /* move to step 3 */
    step2b(p_assignment, p_distMatrix, p_starMatrix, p_newStarMatrix,
	   p_primeMatrix, p_coveredColumns, p_coveredRows, p_nOfRows,
	   p_nOfColumns, p_minDim);
}

/********************************************************/
void Histogram::step2b(FL * p_assignment, FL * p_distMatrix,
		       bool * p_starMatrix, bool * p_newStarMatrix,
		       bool * p_primeMatrix, bool * p_coveredColumns,
		       bool * p_coveredRows, int p_nOfRows,
		       int p_nOfColumns, int p_minDim)
{
    int l_col, l_nOfCoveredColumns;

    /* count covered columns */
    l_nOfCoveredColumns = 0;
    for (l_col = 0; l_col < p_nOfColumns; l_col++)
	if (p_coveredColumns[l_col])
	    l_nOfCoveredColumns++;

    if (l_nOfCoveredColumns == p_minDim) {
	/* algorithm finished */
	buildAssignmentVector(p_assignment, p_starMatrix, p_nOfRows,
			      p_nOfColumns);
    } else {
	/* move to step 3 */
	step3(p_assignment, p_distMatrix, p_starMatrix, p_newStarMatrix,
	      p_primeMatrix, p_coveredColumns, p_coveredRows, p_nOfRows,
	      p_nOfColumns, p_minDim);
    }

}

/********************************************************/
void Histogram::step3(FL * p_assignment, FL * p_distMatrix,
		      bool * p_starMatrix, bool * p_newStarMatrix,
		      bool * p_primeMatrix, bool * p_coveredColumns,
		      bool * p_coveredRows, int p_nOfRows,
		      int p_nOfColumns, int p_minDim)
{
    bool l_zerosFound;
    int l_row, l_col, l_starCol;

    l_zerosFound = true;
    while (l_zerosFound) {
	l_zerosFound = false;
	for (l_col = 0; l_col < p_nOfColumns; l_col++)
	    if (!p_coveredColumns[l_col])
		for (l_row = 0; l_row < p_nOfRows; l_row++)
		    if ((!p_coveredRows[l_row])
			&& (p_distMatrix[l_row + p_nOfRows * l_col] == 0)) {
			/* prime zero */
			p_primeMatrix[l_row + p_nOfRows * l_col] = true;

			/* find starred zero in current row */
			for (l_starCol = 0; l_starCol < p_nOfColumns;
			     l_starCol++)
			    if (p_starMatrix
				[l_row + p_nOfRows * l_starCol])
				break;

			if (l_starCol == p_nOfColumns) {	/* no starred zero found */
			    /* move to step 4 */
			    step4(p_assignment, p_distMatrix, p_starMatrix,
				  p_newStarMatrix, p_primeMatrix,
				  p_coveredColumns, p_coveredRows,
				  p_nOfRows, p_nOfColumns, p_minDim, l_row,
				  l_col);
			    return;
			} else {
			    p_coveredRows[l_row] = true;
			    p_coveredColumns[l_starCol] = false;
			    l_zerosFound = true;
			    break;
			}
		    }
    }

    /* move to step 5 */
    step5(p_assignment, p_distMatrix, p_starMatrix, p_newStarMatrix,
	  p_primeMatrix, p_coveredColumns, p_coveredRows, p_nOfRows,
	  p_nOfColumns, p_minDim);
}

/********************************************************/
void Histogram::step4(FL * p_assignment, FL * p_distMatrix,
		      bool * p_starMatrix, bool * p_newStarMatrix,
		      bool * p_primeMatrix, bool * p_coveredColumns,
		      bool * p_coveredRows, int p_nOfRows,
		      int p_nOfColumns, int p_minDim, int p_row, int p_col)
{
    int l_n, l_starRow, l_starCol, l_primeRow, l_primeCol;
    int l_nOfElements = p_nOfRows * p_nOfColumns;

    /* generate temporary copy of starMatrix */
    for (l_n = 0; l_n < l_nOfElements; l_n++)
	p_newStarMatrix[l_n] = p_starMatrix[l_n];

    /* star current zero */
    p_newStarMatrix[p_row + p_nOfRows * p_col] = true;

    /* find starred zero in current column */
    l_starCol = p_col;
    for (l_starRow = 0; l_starRow < p_nOfRows; l_starRow++)
	if (p_starMatrix[l_starRow + p_nOfRows * l_starCol])
	    break;

    while (l_starRow < p_nOfRows) {
	/* unstar the starred zero */
	p_newStarMatrix[l_starRow + p_nOfRows * l_starCol] = false;

	/* find primed zero in current row */
	l_primeRow = l_starRow;
	for (l_primeCol = 0; l_primeCol < p_nOfColumns; l_primeCol++)
	    if (p_primeMatrix[l_primeRow + p_nOfRows * l_primeCol])
		break;

	/* star the primed zero */
	p_newStarMatrix[l_primeRow + p_nOfRows * l_primeCol] = true;

	/* find starred zero in current column */
	l_starCol = l_primeCol;
	for (l_starRow = 0; l_starRow < p_nOfRows; l_starRow++)
	    if (p_starMatrix[l_starRow + p_nOfRows * l_starCol])
		break;
    }

    /* use temporary copy as new starMatrix */
    /* delete all primes, uncover all rows */
    for (l_n = 0; l_n < l_nOfElements; l_n++) {
	p_primeMatrix[l_n] = false;
	p_starMatrix[l_n] = p_newStarMatrix[l_n];
    }
    for (l_n = 0; l_n < p_nOfRows; l_n++)
	p_coveredRows[l_n] = false;

    /* move to step 2a */
    step2a(p_assignment, p_distMatrix, p_starMatrix, p_newStarMatrix,
	   p_primeMatrix, p_coveredColumns, p_coveredRows, p_nOfRows,
	   p_nOfColumns, p_minDim);
}

/********************************************************/
void Histogram::step5(FL * p_assignment, FL * p_distMatrix,
		      bool * p_starMatrix, bool * p_newStarMatrix,
		      bool * p_primeMatrix, bool * p_coveredColumns,
		      bool * p_coveredRows, int p_nOfRows,
		      int p_nOfColumns, int p_minDim)
{
    FL l_h, l_value;
    int l_row, l_col;

    /* find smallest uncovered element h */
    l_h = 1000000;		//mxGetInf();   
    for (l_row = 0; l_row < p_nOfRows; l_row++)
	if (!p_coveredRows[l_row])
	    for (l_col = 0; l_col < p_nOfColumns; l_col++)
		if (!p_coveredColumns[l_col]) {
		    l_value = p_distMatrix[l_row + p_nOfRows * l_col];
		    if (l_value < l_h)
			l_h = l_value;
		}

    /* add h to each covered row */
    for (l_row = 0; l_row < p_nOfRows; l_row++)
	if (p_coveredRows[l_row])
	    for (l_col = 0; l_col < p_nOfColumns; l_col++)
		p_distMatrix[l_row + p_nOfRows * l_col] += l_h;

    /* subtract h from each uncovered column */
    for (l_col = 0; l_col < p_nOfColumns; l_col++)
	if (!p_coveredColumns[l_col])
	    for (l_row = 0; l_row < p_nOfRows; l_row++)
		p_distMatrix[l_row + p_nOfRows * l_col] -= l_h;

    /* move to step 3 */
    step3(p_assignment, p_distMatrix, p_starMatrix, p_newStarMatrix,
	  p_primeMatrix, p_coveredColumns, p_coveredRows, p_nOfRows,
	  p_nOfColumns, p_minDim);
}


void Histogram::assignmentSuboptimal2(double *p_assignment, double *p_cost,
				      double *p_distMatrix, int p_nOfRows,
				      int p_nOfColumns)
{
    int l_n = 0, l_row = 0, l_col = 0, l_tmpRow = 0, l_tmpCol =
	0, l_nOfElements = 0;
    double l_value, l_minValue, *l_distMatrix, l_inf;

    l_inf = 1e10;

    /* make working copy of distance Matrix */
    l_nOfElements = p_nOfRows * p_nOfColumns;
    l_distMatrix = new double[l_nOfElements];	//(double *)mxMalloc(nOfElements * sizeof(double));
    bzero(l_distMatrix, l_nOfElements * sizeof(double));
    for (l_n = 0; l_n < l_nOfElements; l_n++)
	l_distMatrix[l_n] = p_distMatrix[l_n];

    /* initialization */
    *p_cost = 0;
    for (l_row = 0; l_row < p_nOfRows; l_row++)
	p_assignment[l_row] = -1.0;

    /* recursively search for the minimum element and do the assignment */
    while (true) {
	/* find minimum distance observation-to-track pair */
	l_minValue = l_inf;
	for (l_row = 0; l_row < p_nOfRows; l_row++)
	    for (l_col = 0; l_col < p_nOfColumns; l_col++) {
		l_value = l_distMatrix[l_row + p_nOfRows * l_col];
		if (l_value != l_inf && (l_value < l_minValue)) {
		    l_minValue = l_value;
		    l_tmpRow = l_row;
		    l_tmpCol = l_col;
		}
	    }

	if (l_minValue != l_inf) {
	    p_assignment[l_tmpRow] = l_tmpCol;
	    *p_cost += l_minValue;
	    for (l_n = 0; l_n < p_nOfRows; l_n++)
		l_distMatrix[l_n + p_nOfRows * l_tmpCol] = l_inf;
	    for (l_n = 0; l_n < p_nOfColumns; l_n++)
		l_distMatrix[l_tmpRow + p_nOfRows * l_n] = l_inf;
	} else
	    break;

    }				/* while(true) */

}







void Histogram::assignmentFabio(double *p_distMatrix, int p_nOfRows,
				int p_nOfColumns)
{

    double l_inf = 1e8;
    double *l_Mins = new double[p_nOfRows];
    double *l_SecMins = new double[p_nOfRows];
    double *l_whichMins = new double[p_nOfRows];
    double *l_whichSecMins = new double[p_nOfRows];

    double *l_discriminant = new double[p_nOfRows];
    double *l_sorted_discriminant = new double[p_nOfRows];
    int *l_order_discriminant = new int[p_nOfRows];

    int i = 0;
    int j = 0;


    for (int j = 0; j < p_nOfRows; ++j) {
	l_Mins[j] = l_inf;
	l_SecMins[j] = l_inf;
    }
    for (i = 0; i < p_nOfRows; ++i) {
	for (j = 0; j < p_nOfColumns; ++j) {
	    if (l_Mins[i] > p_distMatrix[i * p_nOfColumns + j]) {
		l_Mins[i] = p_distMatrix[i * p_nOfColumns + j];
		l_whichMins[i] = j;
	    }

	}
    }

    for (i = 0; i < p_nOfRows; ++i) {
	for (j = 0; j < p_nOfColumns; ++j) {
	    if (l_SecMins[i] > p_distMatrix[i * p_nOfColumns + j]
		&& p_distMatrix[i * p_nOfColumns + j] != l_Mins[i]) {
		l_SecMins[i] = p_distMatrix[i * p_nOfColumns + j];
		l_whichSecMins[i] = j;
	    }
	}
    }

    for (int j = 0; j < p_nOfRows; ++j) {
	l_discriminant[j] = l_SecMins[j] - l_Mins[j];
    }


    // Sort discriminant values .. Resort whichMins and whichSecMins
    PRINT_(std::cout << "Unsorted discriminant" << std::endl);
#ifdef PRINT
    for (i = 0; i < p_nOfRows; ++i) {
	std::cout << l_discriminant[i] << std::endl;
    }
#endif


    hist_sort(l_discriminant, p_nOfRows, 1, l_order_discriminant);

#ifdef PRINT
    for (i = 0; i < p_nOfRows; ++i) {
	for (j = 0; j < p_nOfColumns; ++j) {
	    std::cout << p_distMatrix[i * p_nOfColumns + j] << " ";
	}
	std::cout << std::endl;
    }
    std::cout << std::endl;
    PRINT_(std::cout << "Sorted discriminant" << std::endl);

    for (i = 0; i < p_nOfRows; ++i) {
	std::
	    cout << l_discriminant[i] << " " << l_order_discriminant[i] <<
	    std::endl;
    }
#endif

    delete[]l_whichSecMins;
    delete[]l_whichMins;
    delete[]l_sorted_discriminant;
    delete[]l_discriminant;
    delete[]l_order_discriminant;
    delete[]l_Mins;
    delete[]l_SecMins;


}





void Histogram::searchNBestAssignment(FL p_discriminant_ratio,
				      FL * p_distmat, FL * p_initTransfo,
				      int p_dim, int *p_newdim,
				      int *p_dimmask)
{

    double l_inf = 1e8;
    double *l_Mins = new double[p_dim];
    double *l_SecMins = new double[p_dim];
    double *l_whichMins = new double[p_dim];
    double *l_whichSecMins = new double[p_dim];

    sprintf(filename, "%s_histolog", template_filename);
    std::fstream log(filename, fstream::out | fstream::app);
    if (log.is_open() == 0)
	std::
	    cout << "Can not write log printing on standard output" <<
	    std::endl;
    else
	std::
	    cout << "Results of searchNBestAssignment printed to log: " <<
	    filename << std::endl;



    for (int j = 0; j < p_dim; ++j) {
	l_Mins[j] = l_inf;
	l_SecMins[j] = l_inf;
	l_whichMins[j] = -1;
	l_whichSecMins[j] = -1;
    }

    //Printout the distance matrix
    if (log.is_open() == 0)
	std::cout << std::endl << "Distance Matrix" << std::endl;
    else
	log << std::endl << "Distance Matrix" << std::endl;

    for (int i = 0; i < p_dim; ++i) {
	for (int j = 0; j < p_dim; ++j) {
	    if (log.is_open() == 0)

		std::cout << p_distmat[i * p_dim + j] << " ";
	    else
		log << p_distmat[i * p_dim + j] << " ";
	}
	if (log.is_open() == 0)
	    std::cout << std::endl;
	else
	    log << std::endl;
    }

    //Find the selected values per line by checking the initTransfo matrix
    //Store the value in Mins
    //Store the column where they where found in whichMins
    //Verify only one value exists per line
    for (int i = 0; i < p_dim; ++i) {
	for (int j = 0; j < p_dim; ++j) {
	    if (p_initTransfo[i * (p_dim + 1) + j] != 0) {	//what is this for?
		if (l_Mins[i] != l_inf) {
		    std::cout << " Error in the Hungarian approach..." << std::endl;	//why is this an error????
		}
		l_Mins[i] = p_distmat[i * p_dim + j];
		l_whichMins[i] = j;
	    }
	}
    }

    //Find second smallest values per line
    //Store the column where they where found in whichSecMins

    for (int i = 0; i < p_dim; ++i) {
	for (int j = 0; j < p_dim; ++j) {
	    if (p_distmat[i * p_dim + j] < l_SecMins[i]
		&& p_distmat[i * p_dim + j] > l_Mins[i]) {
		l_SecMins[i] = p_distmat[i * p_dim + j];
		l_whichSecMins[i] = j;
	    }
	}
    }

    //Print out smallest and second smallest values per line
    if (log.is_open() == 0) {
	std::cout << std::endl << "Mins1" << std::endl;
	for (int i = 0; i < p_dim; ++i) {
	    std::cout << l_Mins[i] << " " << l_whichMins[i] << std::endl;
	}

	std::cout << std::endl << "Second Min" << std::endl;
	for (int i = 0; i < p_dim; ++i) {
	    std::cout << l_SecMins[i] << " " << l_whichSecMins[i] << std::
		endl;
	}
    } else {
	log << std::endl << "Mins1" << std::endl;
	for (int i = 0; i < p_dim; ++i) {
	    log << l_Mins[i] << " " << l_whichMins[i] << std::endl;
	}

	log << std::endl << "Second Min" << std::endl;
	for (int i = 0; i < p_dim; ++i) {
	    log << l_SecMins[i] << " " << l_whichSecMins[i] << std::endl;
	}

    }


    //Find the difference between smallest and second smallest values

    double *l_discriminant = new double[p_dim];	// VALGRIND Address 0x9AF6C08 is 0 bytes after a block of size 72 alloc'
    bzero(l_discriminant, p_dim * sizeof(double));
    for (int j = 0; j < p_dim; ++j) {
	if (l_SecMins[j] != l_inf)
	    l_discriminant[j] = l_SecMins[j] - l_Mins[j];
    }



    double *l_sorted_discriminant = new double[p_dim];
    int *l_order_discriminant = new int[p_dim];

    int i = 0;

    if (log.is_open() == 0) {
	std::cout << std::endl << "Unsorted discriminant" << std::endl;
	for (i = 0; i < p_dim; ++i) {
	    std::cout << l_discriminant[i] << std::endl;
	}
	std::cout << std::endl;
    } else {
	log << std::endl << "Unsorted discriminant" << std::endl;
	for (i = 0; i < p_dim; ++i) {
	    log << l_discriminant[i] << std::endl;
	}
	log << std::endl;
    }

    hist_sort(l_discriminant, p_dim, 1, l_order_discriminant);

    if (log.is_open() == 0) {
	std::cout << "Sorted discriminant" << std::endl;
	for (i = 0; i < p_dim; ++i) {
	    std::
		cout << l_discriminant[i] << " " << l_order_discriminant[i]
		<< std::endl;
	}
	std::cout << std::endl;
    } else {
	log << "Sorted discriminant" << std::endl;
	for (i = 0; i < p_dim; ++i) {
	    log << l_discriminant[i] << " " << l_order_discriminant[i] <<
		std::endl;
	}
	log << std::endl;
    }

// Rely on ratio : if dis1 > dis2 and dis1/dis2 < DISCRIMINANT_RATIO keep dis2
    int l_count = 0;
    memset(p_dimmask, 0, p_dim * sizeof(int));
    i = 0;

    FL l_prev_dis = l_discriminant[i];
    if (log.is_open() == 0)
	std::
	    cout << "Ratios (check < " << p_discriminant_ratio << ")" <<
	    std::endl;
    else
	log << "Ratios(check < " << p_discriminant_ratio << ")" << std::
	    endl;

    while ((i < p_dim)
	   && (l_prev_dis / l_discriminant[i] < p_discriminant_ratio)) {

	if (log.is_open() == 0)
	    std::cout << l_prev_dis / l_discriminant[i] << std::endl;
	else
	    log << l_prev_dis / l_discriminant[i] << std::endl;
	p_dimmask[l_order_discriminant[i]] = 1;
	l_prev_dis = l_discriminant[i];
	++l_count;
	++i;
    }


    if (log.is_open() == 0)
	std::cout << std::endl;
    else
	log << std::endl;

    if (log.is_open() == 0)
	std::cout << "Dim Mask " << std::endl;
    else
	log << "Dim Mask " << std::endl;
    for (int i = 0; i < p_dim; ++i) {
	if (log.is_open() == 0)
	    std::cout << p_dimmask[i] << std::endl;
	else
	    log << p_dimmask[i] << std::endl;
    }
    if (log.is_open() == 0)
	std::cout << std::endl;
    else
	log << std::endl;

// Rely on absolute threshold : if dis >  DISCRIMINANT_TH keep dis       
#if 0
    count = 0;
    for (int i = 0; i < p_dim; ++i) {
	if (discriminant[i] > DISCRIMINANT_TH) {
	    dimmask[order_discriminant[i]] = 1;
	    ++count;
	} else {
	    dimmask[order_discriminant[i]] = 0;
	}
    }
#endif



    // Modifying T here.
    for (i = 0; i < p_dim; ++i) {
	if (p_dimmask[i] == 0)
	    memset((p_initTransfo + i * (p_dim + 1)), 0,
		   p_dim * sizeof(FL));
    }



    PRINT_COLOR_(std::
		 cout << "T after the selection has been done : " << std::
		 endl, GREEN);
    for (int i = 0; i < p_dim; i++) {
	for (int j = 0; j < p_dim; j++) {
	    std::cout << p_initTransfo[i * (p_dim + 1) + j] << " ";
	}
	std::cout << std::endl;
    }
    std::cout << std::endl;


    *p_newdim = l_count;

    if (log.is_open() == 0)
	log.close();


    delete[]l_whichSecMins;
    delete[]l_whichMins;
    delete[]l_sorted_discriminant;
    delete[]l_discriminant;
    delete[]l_order_discriminant;
    delete[]l_Mins;
    delete[]l_SecMins;

}



int Histogram::hist_sort(FL * p_sorted, int p_n, int p_dim,
			 int *p_orderout)
{

    if (!p_sorted) {
	ERROR_(fprintf(stderr, "Can not copy result to an empty vector\n"),
	       ERROR_EMPTY_DATA);
    }

    int *l_order = NULL;
    if (p_orderout == NULL) {
	l_order = new int[p_n * p_dim];

    } else {
	l_order = p_orderout;
    }

    memset(l_order, 0, p_n * p_dim * sizeof(int));
    for (int i = 0; i < p_n; i++) {
	for (int d = 0; d < p_dim; d++) {
	    l_order[i * p_dim + d] = i;
	}
    }

    //sort each dimension of X (from big to small)
    for (int d = 0; d < p_dim; d++) {
	//fprintf (stderr, "sorting dimension %d of X \n", d);
	for (int i = 1; i < p_n; i++) {
	    int l_c = i * p_dim + d;	//points to current element
	    while ((l_c - p_dim >= 0) && (p_sorted[l_c] > p_sorted[l_c - p_dim])) {	//c-dim is equivalent to (i-1)*dim
		FL l_aux = p_sorted[l_c];
		p_sorted[l_c] = p_sorted[l_c - p_dim];
		p_sorted[l_c - p_dim] = l_aux;
		int l_aux_ord = l_order[l_c];
		l_order[l_c] = l_order[l_c - p_dim];
		l_order[l_c - p_dim] = l_aux_ord;
		l_c -= p_dim;
	    }
	}
    }
    if (p_orderout == NULL) {
	delete[]l_order;
    }
    return 0;
}

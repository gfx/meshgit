/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
/*
 *  legal.cpp
 *  SpecMatch
 *
 *  Created by David Knossow on 10/8/08.
 *  Copyright 2007 INRIA. All rights reserved.
 *
*/
#include "legal.h"



void printIntroLegal (char *progname) {
    std::cout << progname << std::endl ;
    std::cout << "Copyright (C) 2007,2008 INRIA / INPG and GNU GPL. Authors: Diana Mateus, David Knossow and Radu Horaud." <<  std::endl ;
    std::cout << "This program comes with ABSOLUTELY NO WARRANTY (see LICENSE.txt)." <<  std::endl ;
    std::cout << "This is free software, and you are welcome to redistribute it under certain conditions  (see LICENSE.txt)." <<  std::endl ;

std::cout << std::endl ;
std::cout << std::endl ;

}

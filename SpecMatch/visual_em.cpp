/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
/*
 *  visual_em.cpp
 *  match
 *
 *  Created by Diana Mateus and David Knossow on 9/30/07.
 *  Copyright 2007 INRIA. All rights reserved.
 *
 *
 */


#include "visual_em.h"



using namespace std;

VisualMatch::VisualMatch()
{


  animate_on = 0;		//wait until initialize;

  ellip_scale = NULL;

  for (int i = 0; i < 3; i++)
    rotvec[i] = 0.0;

  for (int i = 0; i < 9; i++)
    rotmat[i] = 0.0;
  scale = 1.0;

  seld[0] = 0;
  seld[1] = 1;
  seld[2] = 2;

  current_sel_dim = 2;

  save_on = false;

  num_of_eig = 0;
  nX = 0;
  nY = 0;
  X = NULL;
  Y = NULL;
  Yt = NULL;
  W = NULL;
  maskX = NULL;
  maskY = NULL;
  match_available = false;
  nmaxmatch = 0;
  agmatch = NULL;
  nagmatch = NULL;
  covmat_ = NULL;
  nmaxmatch = 0;
  match_available = false;


#ifdef ENABLE_DISP_HISTOGRAM
  C_Histo_disp_X = NULL;
  C_Histo_disp_Y = NULL;
  C_Histo_disp_FY = NULL;
#endif

  copied = false;
  modified_data = false;
  EM_done = false;

  C_EM = NULL;

  old_nX = -1 ;
	
  disable_draw = true;
  drawing = false;
}



VisualMatch::~VisualMatch()
{

  DBG3_(fprintf(stderr, "VisualMatch:~:visualMatch()\n"));

  releaseCopiedData();

  if (ellip_scale)
    delete[]ellip_scale;
  if (C_EM) {
#ifdef  ENABLE_EM_THREAD
#ifdef QT3
    if (C_EM->running()) {
      C_EM->terminate();
      C_EM->exit();
    }
#endif
#ifdef QT4
    if (C_EM->isRunning()) {
      C_EM->terminate();
      C_EM->exit();
    }
#endif
#endif
    delete C_EM;
  }

}


int VisualMatch::visualEM(INIT_MODE p_init_m, EM_MODE p_em_m,
			  int *p_int_arg, FL * p_fl_arg)
{
  DBG_VISU_(fprintf(stderr, "VisualMatch::visualEM()\n"));


  C_EM->setMode(p_em_m);

  char mode[256];

  switch (p_em_m) {
  case ISO_ANNEAL:
    strcpy(mode, "ISO ANNEAL");
    break;
  case GLOBAL_COV:
    strcpy(mode, "GLOBAL COVARIANCE");
    break;
  case USE_ESTIMATE:
    strcpy(mode, "USE ESTIMATED");
    break;
      default:
    strcpy(mode, "UNKNOWN MODE");
    break;
  }

  PRINT_COLOR_(fprintf(stderr, "EM_MODE has been set to: %s\n", mode),
	       RED);


  C_EM->initEM(p_init_m, p_int_arg, p_fl_arg);

  DBG_VISU_(fprintf(stderr, "VisualMatch::visualEM()\n"));
  makeCurrent();
  updateGL();
  disable_draw = true;
  switch (C_EM->getStep()) {
  case E_STEP:
    C_EM->E();
    C_EM->setStep(M_STEP);
    break;
  case M_STEP:
    C_EM->M();
    C_EM->setStep(EVAL);
    break;
  case EVAL:
    animate_on = C_EM->evalTermination();
    C_EM->setStep(E_STEP);
    break;
  case NONE_EM:
    C_EM->setStep(E_STEP);
    break;
  default:
    FATAL_(fprintf(stderr, "step should be 'M' or 'E'\n"),
	   FATAL_UNKNOWN_MODE);
    break;
  }


  copyData();

  findScale();

  if (C_EM->getMode() == GLOBAL_COV)
    covarianceEllipsoid();	// to initalize ellip_scale and rotmat of the ellipses

  animate_on = 1;

  updateGL();

  DBG_VISU_(fprintf(stderr, "Starting Animation\n"));
  startAnimation();

  return 0;
}

int VisualMatch::findScale()
{

  DBG_(fprintf(stderr, "-->Finding common scale (for visualization)\n"));
  float *l_minval = new float[num_of_eig];
  float *l_maxval = new float[num_of_eig];
  //Look in X
  for (int i = 0; i < nX; i++) {
    for (int d = 0; d < num_of_eig; d++) {
      if (i == 0) {
	l_minval[d] = (float) X[d];
	l_maxval[d] = (float) X[d];
      } else {
	if (X[i * num_of_eig + d] < l_minval[d])
	  l_minval[d] = (float) X[i * num_of_eig + d];
	if (X[i * num_of_eig + d] > l_maxval[d])
	  l_maxval[d] = (float) X[i * num_of_eig + d];
      }
    }
  }
  //Look in Y
  for (int j = 0; j < nY; j++) {
    for (int d = 0; d < num_of_eig; d++) {
      if (Y[j * num_of_eig + d] < l_minval[d])
	l_minval[d] = (float) Y[j * num_of_eig + d];
      if (Y[j * num_of_eig + d] > l_maxval[d])
	l_maxval[d] = (float) Y[j * num_of_eig + d];
    }
  }
  //find max delta
  scale = l_maxval[0] - l_minval[0];
  for (int d = 1; d < num_of_eig; d++) {
    if (scale < (l_maxval[d] - l_minval[d]))
      scale = l_maxval[d] - l_minval[d];
  }
  delete[]l_minval;
  delete[]l_maxval;
  return 0;
}

void VisualMatch::draw()
{
  double l_cube_size = 0 ;
  HASH_MAP_PARAM_(double, "cube_size", Double, l_cube_size) ;


  DBG_VISU_(std::cout << "VisualMatch::draw()" << std::endl);

  if (!disable_draw) {

    drawing = true;

    int l_dx = seld[0];
    int l_dy = seld[1];
    int l_dz = seld[2];

    char l_dimstring[64];
    sprintf(l_dimstring, " Dims: %d, %d, %d", l_dx, l_dy, l_dz);
    glColor3f(0.0, 0.0, 0.0);
    drawText(10, 20, l_dimstring);

    GLdouble modelview[16];
    glGetDoublev(GL_MODELVIEW_MATRIX, modelview);

    // Draw Line Matches
    if (show_matches && match_available) {
      glMatrixMode(GL_MODELVIEW);
      glLoadMatrixd(modelview);
      glLineWidth(0.8f);
      glEnable(GL_LINE_STIPPLE);
      glLineStipple(1, 0x000F);

      int l_n = 0;
      if (show_best_class) {
	l_n = nX;
      }
      else if (show_best_observations) {
	l_n = nmaxmatch * nY;
      }

      float l_x, l_y, l_z;
      int ind1 = 0;
      int ind2 = 0;
      glColor3f(0.4, 0.4, 0.4);
      glBegin(GL_LINES);
      int l_count = 0;
      for (int i = 0; i < l_n;) {
	if (show_best_class) {
	  ind1 = agmatch[i * 2];
	  ind2 = agmatch[i * 2 + 1];
	} else if (show_best_observations) {
	  ind2 = nagmatch[i * 2];
	  ind1 = nagmatch[i * 2 + 1];
	  l_count++;
	}
	if ((ind1 != -1) && (ind2 != -1) &&
	    (maskX[ind1] == 1) && (maskY[ind2] == 1)) {

	  l_x = (float) (X[ind1 * num_of_eig + l_dx]) / scale - model_sep[0] ;
	  l_y = (float) (X[ind1 * num_of_eig + l_dy]) / scale - model_sep[1] ;
	  l_z = (float) (X[ind1 * num_of_eig + l_dz]) / scale - model_sep[2] ;
	  glVertex3f(l_x, l_y, l_z);
	  l_x =
	    (float) (Yt[ind2 * num_of_eig + l_dx]) / scale +
	    model_sep[0];
	  l_y =
	    (float) (Yt[ind2 * num_of_eig + l_dy]) / scale +
	    model_sep[1];
	  l_z =
	    (float) (Yt[ind2 * num_of_eig + l_dz]) / scale +
	    model_sep[2];
	  glVertex3f(l_x, l_y, l_z);

	  if (show_best_class)
	    i += line_sample;

	  else if (show_best_observations) {
	    if (l_count == nmaxmatch) {
	      i += line_sample * nmaxmatch;
	      l_count = 0;
	    } else
	      i++;
	  }
	}
	else {
	  i++;
	}
      }
      glEnd();
      glDisable(GL_LINE_STIPPLE);

    }
    glTranslatef(-model_sep[0], -model_sep[1], -model_sep[2]);
    if (X) {		// Green  
      if ( old_nX > -1 ) {
	for (int i = 0; i < old_nX; i++) {
	  if (maskX[i] == 1)
	    glColor3f(0.0, 0.8, 0.0);
	  else
	    glColor3f(0.9, 0, 0);
				
	  drawCube((float) (X[i * num_of_eig + l_dx]) / scale,
		   (float) (X[i * num_of_eig + l_dy]) / scale,
		   (float) (X[i * num_of_eig + l_dz]) / scale,
		   l_cube_size);
	}
	for (int i = old_nX; i < nX; i++) {
	  if (maskX[i] == 1)
	    glColor3f(0.8, 0, 0.8);
	  else
	    glColor3f(0.9, 0, 0);
				
	  drawCube((float) (X[i * num_of_eig + l_dx]) / scale,
		   (float) (X[i * num_of_eig + l_dy]) / scale,
		   (float) (X[i * num_of_eig + l_dz]) / scale,
		   l_cube_size);
	}
      }
      else{
	for (int i = 0; i < nX; i++) {
	  if (maskX[i] == 1)
	    glColor3f(0.0, 0.8, 0.0);
	  else
	    glColor3f(0.5, 0.5, 0.5);
				
	  drawCube((float) (X[i * num_of_eig + l_dx]) / scale,
		   (float) (X[i * num_of_eig + l_dy]) / scale,
		   (float) (X[i * num_of_eig + l_dz]) / scale,
		   l_cube_size);
	}
			
      }
    }

    glMatrixMode(GL_MODELVIEW);
    glLoadMatrixd(modelview);
    glTranslatef(model_sep[0], model_sep[1], model_sep[2]);

    if (Yt) {		//Blue

      for (int j = 0; j < nY; j++) {
	if (maskY[j] == 1)
	  glColor3f(0.0, 0.0, 0.8);
	else
	  glColor3f(0.5, 0.5, 0.5);
	drawCube((float) (Yt[j * num_of_eig + l_dx]) / scale,
		 (float) (Yt[j * num_of_eig + l_dy]) / scale,
		 (float) (Yt[j * num_of_eig + l_dz]) / scale,
		 l_cube_size);
      }
    }
#if 0
    if (W) {		//Purple
      glMatrixMode(GL_MODELVIEW);
      glLoadMatrixd(modelview);
      glTranslatef(-model_sep[0], -model_sep[1],
		   -model_sep[2] / 2.0);
      for (int j = 0; j < nY; j++) {
	if (maskY[j] == 1)
	  glColor3f(0.5, 0.2, 1.0);
	else
	  glColor3f(0.5, 0.5, 0.5);

	drawCube((float) (W[j * num_of_eig + l_dx]) / scale,
		 (float) (W[j * num_of_eig + l_dy]) / scale,
		 (float) (W[j * num_of_eig + l_dz]) / scale,
		 l_cube_size);
	if (nY % line_sample == 0) {
	  glColor3f(0.5, 0.2, 0.2);
	  float y[3];
	  y[dx] =
	    (float) (W[j * num_of_eig + dx] +
		     0.3 * (Yt[j * num_of_eig + dx] -
			    W[j * num_of_eig + dx])) / scale;
	  y[dy] =
	    (float) (W[j * num_of_eig + dy] +
		     0.3 * (Yt[j * num_of_eig + dy] -
			    W[j * num_of_eig + dy])) / scale;
	  y[dz] =
	    (float) (W[j * num_of_eig + dz] +
		     0.3 * (Yt[j * num_of_eig + dz] -
			    W[j * num_of_eig + dz])) / scale;
	  glBegin(GL_LINES);
	  glVertex3f((float) (W[j * num_of_eig + dx]) / scale,
		     (float) (W[j * num_of_eig + dy]) / scale,
		     (float) (W[j * num_of_eig + dz]) / scale);
	  //glVertex3f((float)(Yt[j*num_of_eig+dx])/scale,(float)(Yt[j*num_of_eig+dy])/scale,(float)(Yt[j*num_of_eig+dz])/scale);
	  glVertex3f(y[dx], y[dy], y[dz]);
	  glEnd();
	}
      }
    }
#endif




    // Draw covariance ellipsoids           
    if (show_ellipses && Yt) {
      glMatrixMode(GL_MODELVIEW);
      glLoadMatrixd(modelview);
      for (int j = 0; j < nY; j = j + line_sample) {
	if (maskX[j] == 1) {
	  glPushMatrix();
	  glColor3f(0.7, 0.7, 0.7);
	  glTranslatef((float) (Yt[j * num_of_eig + l_dx]) /
		       scale + model_sep[0],
		       (float) (Yt[j * num_of_eig + l_dy]) /
		       scale + model_sep[1],
		       (float) (Yt[j * num_of_eig + l_dz]) /
		       scale + model_sep[2]);
	  switch (em_mode) {
	  case USE_ESTIMATE:
	  case ISO_ANNEAL:
	    glutWireSphere(sigma / scale, 10, 10);
	    break;
	  case GLOBAL_COV:
	    glScalef(ellip_scale[seld[0]] / scale,
		     ellip_scale[seld[1]] / scale,
		     ellip_scale[seld[2]] / scale);
	    glRotatef(rotangle, rotvec[0], rotvec[1],
		      rotvec[2]);
	    glutWireSphere(1.0, 16, 16);
	    break;
	    
	  default:
	    FATAL_(fprintf
		   (stderr, "This EM mode does not exists\n"),
		   FATAL_UNKNOWN_MODE);
	    break;
	  }
	  glPopMatrix();
	}
      }
    }
    glMatrixMode(GL_MODELVIEW);
    glLoadMatrixd(modelview);

    //save snapshot

    if (save_on) {
      PRINT_COLOR_(fprintf(stderr, "Saving image\n"), GREEN);
      saveSnapshot(true, false);
    }
    drawing = false;
  }


  DBG_VISU_(std::cout << "VisualMatch::draw() return " << std::endl);



}

void VisualMatch::init()
{

  HASH_MAP_PARAM_(double, "model_sep", Double, model_sep[0]) ; 
  model_sep[1] = 0.0;
  model_sep[2] = -model_sep[0] ;



  setKeyDescription(Qt::Key_Space, "Toggles Animation ON/OFF");
  setKeyDescription(Qt::Key_S, "Toggle the image saving");
  setKeyDescription(Qt::Key_E, "Display or hide covariance ellispes");
  setKeyDescription(Qt::Key_M, "Display or hide matches between shapes");
  setKeyDescription(Qt::Key_1,
		    "Select the dimension 1 to update (while showing the embeddings)");
  setKeyDescription(Qt::Key_2,
		    "Select the dimension 2 to update (while showing the embeddings)");
  setKeyDescription(Qt::Key_3,
		    "Select the dimension 3 to update (while showing the embeddings)");
  setKeyDescription(Qt::Key_Plus, "Update the selected dimension");
  setKeyDescription(Qt::Key_Minus, "Update the selected dimension");
#ifdef ENABLE_DISP_HISTOGRAM
  setKeyDescription(Qt::Key_I, "Show Histograms.");
#endif

  // Disabling here the standard keyboard key binding
  setShortcut(STEREO, 0) ;
  setShortcut(DISPLAY_FPS, 0) ;
  setShortcut(CAMERA_MODE, 0) ;

  DBG3_(fprintf(stderr, "VisualMatch::init()\n"));
  //do not show all the ellipsoids nor all the matches
  HASH_MAP_PARAM_(int, "sample", Int, line_sample ) ;


  //show selected dimensions
  setTextIsEnabled(true);


  // Restore previous Shape state.
  restoreStateFromFile();

  // Define lighting
  float mat_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
  float mat_shininess[] = { 50.0f };
  float light_position_0[] = { 1.0f, 1.0f, 0.0f, 0.0f };
  float light_position_1[] = { -1.0f, -1.0f, 0.0f, 0.0f };
  float light_position_2[] = { -1.0f, 1.0f, 0.0f, 0.0f };
  float light_position_3[] = { 1.0f, -1.0f, 0.0f, 0.0f };
  float light_position_4[] = { 0.0f, 1.0f, 0.0f, 0.0f };
  float light_position_5[] = { 1.0f, 0.0f, 0.0f, 0.0f };


  glClearColor(1.0, 1.0, 1.0, 0.0);
  glShadeModel(GL_SMOOTH);

  float global_ambient[] = { 0.8f, 0.8f, 0.8f, 1.0f };
  glLightModelfv(GL_LIGHT_MODEL_AMBIENT, global_ambient);

  glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
  glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
  glLightfv(GL_LIGHT0, GL_POSITION, light_position_0);
  glLightfv(GL_LIGHT1, GL_POSITION, light_position_1);
  glLightfv(GL_LIGHT2, GL_POSITION, light_position_2);
  glLightfv(GL_LIGHT3, GL_POSITION, light_position_3);
  glLightfv(GL_LIGHT4, GL_POSITION, light_position_4);
  glLightfv(GL_LIGHT5, GL_POSITION, light_position_5);


  glEnable(GL_LIGHTING);
  glEnable(GL_LIGHT0);
  glEnable(GL_DEPTH_TEST);

  animate_on = false;
  setAnimationPeriod(100);
  animate_on = false;
  disable_draw = true;
  show_ellipses = false;
  show_matches = false;
  show_best_observations = false;
  show_best_class = false;

  setSnapshotFileName("aligned");
  setSnapshotFormat("PNG");

  DBG3_(fprintf(stderr, "VisualMatch::init() return\n"));
}

void VisualMatch::keyPressEvent(QKeyEvent * e)
{
  int l_new_dim;
  switch (e->key()) {
  case Qt::Key_Space:
    if (animate_on) {
      animate_on = false;
      setAnimationPeriod(100);
    } else {
      animate_on = true;
      setAnimationPeriod(100);
    }
    updateGL();
    break;
  case Qt::Key_X:
    if (save_on) {
      PRINT_COLOR_(std::cout << "Saving Images had been disabled" <<std::endl , RED) ;
      save_on = false;
    } else {
      PRINT_COLOR_(std::cout << "Saving Images had been enabled" <<std::endl , RED) ;
      save_on = true;
    }
    break;
  case Qt::Key_E:
    toggleEllipses();
    updateGL();
    break;
  case Qt::Key_M:
    toggleMatches();
    updateGL();
    break;
  case Qt::Key_1:
    current_sel_dim = 0;
    updateGL();
    break;
  case Qt::Key_2:
    current_sel_dim = 1;
    updateGL();
    break;
  case Qt::Key_3:
    current_sel_dim = 2;
    updateGL();
    break;
  case Qt::Key_Plus:
    l_new_dim = seld[current_sel_dim];
    l_new_dim++;
    if (l_new_dim >= num_of_eig)
      l_new_dim = 0;
    seld[current_sel_dim] = l_new_dim;
    updateGL();
    break;
  case Qt::Key_Minus:
    l_new_dim = seld[current_sel_dim];
    l_new_dim--;
    if (l_new_dim < 0)
      l_new_dim = num_of_eig - 1;
    seld[current_sel_dim] = l_new_dim;
    updateGL();
    break;
#ifdef ENABLE_DISP_HISTOGRAM
  case Qt::Key_I:
    showHistograms();
    break;
#endif
  default:
    QGLViewer::keyPressEvent(e);

  }
}


#ifdef ENABLE_DISP_HISTOGRAM
void VisualMatch::showHistograms()
{
  if (C_Histo_disp_X == NULL)
    C_Histo_disp_X = new HistoWidget();
  if (C_Histo_disp_Y == NULL)
    C_Histo_disp_Y = new HistoWidget();
  if (C_Histo_disp_FY == NULL)
    C_Histo_disp_FY = new HistoWidget();


#if QT_VERSION < 0x040000
  C_Histo_disp_X->setCaption("Histogram for Green Curve");
  C_Histo_disp_Y->setCaption("Histogram for Blue Curve");
  C_Histo_disp_FY->setCaption("Histogram for Flipped blue Curve");

#else
  C_Histo_disp_X->setWindowTitle("Histogram for Green Curve");
  C_Histo_disp_Y->setWindowTitle("Histogram for Blue Curve");
  C_Histo_disp_FY->setWindowTitle("Histogram for Flipped blue Curve");

#endif

  int *l_hist = new int[num_of_eig];

  for (int i = 0; i < num_of_eig; ++i)
    l_hist[i] = C_EM->C_Histo.getNumberOfBins();


  QColor *l_colormap = new QColor[num_of_eig];
  QColor *l_color1 = new QColor[num_of_eig];
  QColor *l_color2 = new QColor[num_of_eig];


  int l_ncolor = num_of_eig;
  DBG2_(fprintf(stderr, "Building colormap with %d colors\n", l_ncolor));

  float l_r, l_g, l_b, l_deltar, l_deltag, l_deltab, l_minr, l_ming,
    l_minb;
  l_deltar = 0.8f;
  l_deltag = 0.8f;
  l_deltab = 0.8f;
  l_minr = 0.1f;
  l_ming = 0.1f;
  l_minb = 0.1f;

  float l_colval = 0.0f;
  for (int i = 0; i < l_ncolor; i++) {
    int ind = i;
    if (ind < (int) round((float) (l_ncolor) / 3.0f)) {
      l_colval = 3.0f * (float) ind / (float) (l_ncolor);
      l_r = l_deltar + l_minr;
      l_g = l_deltag * l_colval + l_ming;
      l_b = l_minb;
    } else if (ind < (int) round(2.0f * (float) (l_ncolor) / 3.0f)) {
      ind = ind - (int) round((float) (l_ncolor) / 3.0f);
      l_colval = 3.0f * (float) ind / (float) (l_ncolor);
      l_r = l_deltar * (1.0f - l_colval) + l_minr;
      l_g = l_deltag + l_ming;
      l_b = l_deltab * l_colval + l_minb;
    } else {
      ind = ind - (int) round(2.0f * (float) (l_ncolor) / 3.0f);
      l_colval = 3.0f * (float) ind / (float) (l_ncolor);
      l_r = l_minr;
      l_g = l_deltag * (1.0f - l_colval) + l_ming;
      l_b = l_deltab + l_minb;
    }
    l_colormap[i] = QColor((int) round(l_r * 255),
			   (int) round(l_g * 255),
			   (int) round(l_b * 255));
  }


  int l_dim = num_of_eig;
  int l_init_dim = C_EM->getInitDim();

  int *l_initT = C_EM->getInitT();

#ifdef DEBUG_2
  for (int d = 0; d < l_init_dim; d++) {
    for (int e = 0; e < l_init_dim; e++) {
      std::cout << l_initT[d * (l_init_dim + 1) + e] << " ";
    }
    std::cout << std::endl;
  }
#endif

  for (int i = 0; i < l_dim; ++i) {
    for (int j = 0; j < l_dim; ++j) {
      DBG2_(std::
	    cout << l_initT[i * (num_of_eig + 1) +
			    j] << " " << std::endl);
      if (l_initT[i * (l_init_dim + 1) + j]) {
	DBG2_(std::
	      cout << "Setting colors for " << i << " " << j <<
	      std::endl);
	l_color1[i] = l_colormap[i];
	l_color2[j] = l_colormap[i];
      }
    }
  }

  int l_rows = 1;
  int l_cols = (int) ceil((float) num_of_eig / (float) l_rows);	//1;

  C_Histo_disp_X->setDataToDisplay(num_of_eig, l_rows, l_cols,
				   C_EM->getVecHistX(), l_hist);
  C_Histo_disp_Y->setDataToDisplay(num_of_eig, l_rows, l_cols,
				   C_EM->getVecHistY(), l_hist);
  C_Histo_disp_FY->setDataToDisplay(num_of_eig, l_rows, l_cols,
				    C_EM->getVecHistFY(), l_hist);




  C_Histo_disp_X->show();
  C_Histo_disp_Y->show();
  C_Histo_disp_FY->show();


  //    C_Histo_disp_X->setBackGroundColors( color1) ;
  //    C_Histo_disp_Y->setBackGroundColors( color2) ;

  delete[]l_colormap;
  delete[]l_color1;
  delete[]l_color2;

}
#endif



int VisualMatch::covarianceEllipsoid()
{
  DBG_(fprintf(stderr, "covariance_ellipsoid\n"));

  //Given a covariance matrix find the principal axis and fill the ellip_scale and rotation parameters

  if (!ellip_scale)
    ellip_scale = new FL[num_of_eig];

  //print_flmatrix(covmat,"tmp/covmat",num_of_eig,num_of_eig);
  CvMat *l_U = cvCreateMat(num_of_eig, num_of_eig, CV_FL);
  CvMat *l_V = cvCreateMat(num_of_eig, num_of_eig, CV_FL);
  CvMat *l_D = cvCreateMat(num_of_eig, 1, CV_FL);

  //cvSVD A=U*W*V'
  cvSVD(covmat_, l_D, l_U, l_V);

  //Fill rotmat for the 3 chosen dimensions   
  int dimi, dimj;
  for (int i = 0; i < 3; i++) {
    dimi = seld[i];
    for (int j = 0; j < 3; j++) {
      dimj = seld[j];
      rotmat[i * 3 + j] = (FL) cvmGet(l_V, dimi, dimj);
    }
  }


  for (int i = 0; i < num_of_eig; i++)
    ellip_scale[i] = (FL) sqrt((FL) cvmGet(l_D, i, 0));

  cvReleaseMat(&l_U);
  cvReleaseMat(&l_V);
  cvReleaseMat(&l_D);

  return 0;
}




int VisualMatch::rotmatrix2rotvec()
{
  //whatch out for singularities at 0 and 180

  //The rotation angle
  FL l_dummy = 0.0;
  int l_selected_dim;
  for (int i = 0; i < 3; i++) {
    l_selected_dim = seld[i];
    l_dummy += rotmat[l_selected_dim * num_of_eig + l_selected_dim];	// sum of diagonal elemnts
  }
  rotangle = acos((l_dummy - 1.0) / 2.0);

  //The rotation vector

  int l_dimx, l_dimy, l_dimz;
  l_dimx = seld[0];
  l_dimy = seld[1];
  l_dimz = seld[2];

  FL l_norm, l_xy, l_yz, l_xz;
  l_xy =
    rotmat[l_dimy * num_of_eig + l_dimx] - rotmat[l_dimx * num_of_eig +
						  l_dimy];
  l_yz =
    rotmat[l_dimz * num_of_eig + l_dimy] - rotmat[l_dimy * num_of_eig +
						  l_dimz];
  l_xz =
    rotmat[l_dimx * num_of_eig + l_dimz] - rotmat[l_dimz * num_of_eig +
						  l_dimx];
  l_norm = sqrt(l_xy * l_xy + l_yz * l_yz + l_xz * l_xz);

  if (l_norm == 0) {
    ERROR_(fprintf
	   (stderr,
	    "Error while calculating normalization factor for the rotation vector. Norm = 0.0\n"),
	   1);
  }

  rotvec[0] = l_yz / l_norm;
  rotvec[1] = l_xz / l_norm;
  rotvec[2] = l_xy / l_norm;

  return 0;
}



int VisualMatch::toggleEllipses()
{
  if (show_ellipses)
    show_ellipses = false;
  else
    show_ellipses = true;
  return 0;
}


int VisualMatch::toggleMatches()
{
  if (show_matches && show_best_class) {
    show_best_class = false;
    if (nagmatch) {
      show_best_observations = true;

      PRINT_(std::
	     cout << "Showing best " << nmaxmatch << " observations"
	     << std::endl);

    } else
      show_matches = false;
  } else if (show_matches && show_best_observations) {
    show_best_class = false;
    show_best_observations = false;
    show_matches = false;
  } else {
    show_best_class = true;
    show_matches = true;
    show_best_observations = false;
    PRINT_(std::cout << "Showing best class" << std::endl);
  }
  return 0;
}


void VisualMatch::animate()
{

  DBG_VISU_(fprintf(stderr, "VisualMatch: animate()\n"));

  if (drawing) {
    DBG3_(fprintf
	  (stderr,
	   "VisualMatch: animate():: wait until drawing is finished\n"));
  }
    
  else {
    disable_draw = true;
    
    if (animate_on) {
      
#ifdef ENABLE_EM_THREAD
      
#if QT_VERSION < 0X040000
      if (C_EM->finished())
#else
      if (C_EM->isFinished())
#endif
	  
#else
      if (EM_done)
#endif
     {
       DBG3_(fprintf
	     (stderr, "VisualMatch:::animate(): eval termination\n"));
       
       animate_on = C_EM->evalTermination();
       releaseCopiedData();
       copyData();
       
       // perform Ellipse computations....
       if (C_EM->getMode() == GLOBAL_COV)
	 covarianceEllipsoid();
       
       //print results
       C_EM->printMatches();
       
       if (!animate_on) {
	 PRINT_COLOR_(fprintf(stderr, "EM() visual : END\n"), RED);
	 
	 C_EM->printOutputs();
	 	 stopAnimation();
       }
       EM_done = false;
     }
#if QT_VERSION<0X040000
      if (animate_on && !C_EM->running())
#else
      if (animate_on && !C_EM->isRunning())
#endif
      {	  
#ifdef ENABLE_EM_THREAD
	C_EM->start();
#else
	  
	if (!C_EM->getAlphaLineMode()) {
	  C_EM->E();
	  C_EM->M();
	}
	else {
	  C_EM->lineE();
	  C_EM->lineM();
	  }
#endif
	EM_done = true;
      }
      
    } else {
      PRINT_COLOR_(fprintf(stderr, "EM animation stopped \n"), GREEN);
    }
    disable_draw = false;
    
  }
  
  DBG_VISU_(fprintf(stderr, "VisualMatch:::animate() return()\n"));
}



void VisualMatch::copyData()
{
  DBG3_(fprintf(stderr, "Copying Data \n"));

  disable_draw = true ;

  num_of_eig = C_EM->getDataDim();
  nX = C_EM->getDimOfX();
  nY = C_EM->getDimOfY();
  old_nX = nX ;
  	
  if (X)
    delete[]X;
  X = new FL[nX * num_of_eig];
  memcpy(X, C_EM->getX(), nX * num_of_eig * sizeof(FL));

  if (Y)
    delete[]Y;
  Y = new FL[nY * num_of_eig];
  memcpy(Y, C_EM->getY(), nY * num_of_eig * sizeof(FL));

  if (Yt)
    delete[]Yt;
  Yt = new FL[nY * num_of_eig];
  memcpy(Yt, C_EM->getYt(), nY * num_of_eig * sizeof(FL));

  if (W)
    delete[]W;
  W = new FL[nY * num_of_eig];
  memcpy(W, C_EM->getW(), nY * num_of_eig * sizeof(FL));

  if (maskX)
    delete[]maskX;
  maskX = new int[nX];
  memcpy(maskX, C_EM->getMaskX(), nX * sizeof(int));

  if (maskY)
    delete[]maskY;
  maskY = new int[nY];
  memcpy(maskY, C_EM->getMaskY(), nY * sizeof(int));

  if (agmatch)
    delete[]agmatch;
  agmatch = new int[2 * nX];
  memcpy(agmatch, C_EM->getAgMatch(), 2 * nX * sizeof(int));

  if (nagmatch) {
    delete[]nagmatch;
    nagmatch = NULL;
  }
  if (C_EM->getNAgMatch()) {
    nmaxmatch = C_EM->getNMaxMatch();
    nagmatch = new int[2 * nY * nmaxmatch];
    memcpy(nagmatch, C_EM->getNAgMatch(),
	   2 * nY * nmaxmatch * sizeof(int));
  }

  em_mode = C_EM->getMode();
  match_available = C_EM->isMatchAvailable();
  sigma = C_EM->getSigma();
  covmat_ = C_EM->getCovMatCV();

  disable_draw = false ;
  copied = true;
}

void VisualMatch::releaseCopiedData()
{

  DBG3_(fprintf(stderr, "Releasing Copied Data  \n"));
  if (Yt)
    delete[]Yt;
  Yt = NULL;
  if (X)
    delete[]X;
  X = NULL;
  if (Y)
    delete[]Y;
  Y = NULL;
  if (W)
    delete[]W;
  W = NULL;
  if (maskX)
    delete[]maskX;
  maskX = NULL;
  if (maskY)
    delete[]maskY;
  maskY = NULL;
  if (agmatch)
    delete[]agmatch;
  agmatch = NULL;
  if (nagmatch)
    delete[]nagmatch;
  nagmatch = NULL;
}





int VisualMatch::loadData(int p_nx, int p_ny, int p_dim, FL * p_X,
			  FL * p_Y, char *p_filename)
{

  if ( C_EM == NULL ) {
    C_EM = new ThreadMatch();
    C_EM->init () ;
  }

  C_EM->loadData(p_nx, p_ny, p_dim, p_X, p_Y, p_filename);
  return 0;
}

int VisualMatch::setHistoParams(int p_nbins, HISTOGRAM_DISTANCE p_histDist,
				ASSIGNMENT_METHOD p_assignMethod,
				FL p_disc_ratio)
{
  C_EM->setHistoParams(p_nbins, p_histDist, p_assignMethod,
		       p_disc_ratio);
  return 0;
}

int VisualMatch::setTransfoMode(TRANSFO_MODE p_mode )
{
  return C_EM->setTransfoMode(p_mode );
  return 0;
}

int VisualMatch::setSigma(FL p_val)
{
  return C_EM->setSigma(p_val);
}

int VisualMatch::setOutlierConstant(FL p_val)
{
  return C_EM->setOutlierConstant(p_val);
}

int VisualMatch::setLineSample(int p_val)
{
  return C_EM->setLineSample(p_val);
}


void VisualMatch::setAlphaLineMode (bool on) 
{
  C_EM->setAlphaLineMode(on);
}
 
void VisualMatch::setRemoveFirst(bool p_remove_first)
{
  C_EM->setRemoveFirst(p_remove_first);
}

void VisualMatch::setModelSep (float x, float y, float z) {


  if (ABS(x) > 0.7)
    x = 0.5 ;
  if (ABS(y) > 0.7)
    y = 0.5 ;
  if (ABS(z) > 0.7)
    z = 0.5 ;

  model_sep[0] = x ;
  model_sep[1] = y ;
  model_sep[2] = z ;

}

/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
#include "constantReader.h"
#include "Constants.h"
#include "utilities.h"



int readConstantFileSettings (char *filename) {

  QDomDocument doc( "DefaultSettings" ) ;

  PRINT_COLOR_(std::cout << "Setting default parameters read from " << filename << std::endl, RED) ; 

  QFile file( filename );
  if( !file.open( QIODevice::ReadOnly ) ) {
    exit (-1);
  }
  if( !doc.setContent( &file ) )
    {
      file.close();
      std::cout << "Could not read the DOM elements" << std::endl ;
      return -2;
    }
  file.close();

  QDomElement docElem = doc.documentElement();
  
  QDomNode n = docElem.firstChild();
  while(!n.isNull()) {
    QDomElement e = n.toElement(); // try to convert the node to an element.
    if(!e.isNull()) {

      if (e.attribute("type", "none").toStdString() == std::string("int")) {
	std::pair < const std::string, Variant > p = make_pair(e.tagName().toStdString(),
							       Variant(e.attribute("value").toInt(), e.tagName().toStdString())) ;
	hash_map_global_params.insert(p) ;
	
	DBG_(std::cout << p.first << " " << p.second.Int() <<std::endl) ;
      }
      else if (e.attribute("type", "none").toStdString() == std::string("float")) {
	std::pair < const std::string, Variant > p = make_pair(e.tagName().toStdString(),
							       Variant(e.attribute("value").toFloat(), e.tagName().toStdString())) ;
	hash_map_global_params.insert(p) ;
	DBG_(std::cout << p.first << " " << p.second.Float() <<std::endl) ;
      }
      else if (e.attribute("type", "none").toStdString() == std::string("double")) {
	std::pair < const std::string, Variant > p = make_pair(e.tagName().toStdString(),
							       Variant(e.attribute("value").toDouble(), e.tagName().toStdString())) ;
	hash_map_global_params.insert(p) ;
	DBG_(std::cout << p.first << " " << p.second.Double() <<std::endl) ;
      }
      else if (e.attribute("type", "none").toStdString() == std::string("bool")) {
	std::pair < const std::string, Variant > p = make_pair(e.tagName().toStdString(),
							       Variant(e.attribute("value").toInt(), e.tagName().toStdString())) ;
	hash_map_global_params.insert(p) ;
	DBG_(std::cout << p.first << " " << p.second.Bool() <<std::endl) ;
      }
      else {
	WARNING_(std::cout << "no types for " << e.tagName().toStdString() <<std::endl) ;
      }
    }
    n = n.nextSibling();
  }
  
  return 0 ;  

}


/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
/*
 *  eigs.h
 *  match
 * 
 * Code adapted from:
 *   Scot Shaw
 * 30 August 1999
 *
 */


// Begin with some standard include files.

#include <cmath>
#include <iostream>
#include <fstream>
#include <iomanip>

#ifdef MACOSX
#include  <Accelerate/Accelerate.h>
//Accelerate framework contains altivec framework which
//in turn contains lapack and blas
#endif

#include "utilities.h"
#include "eigs.h"

Eigs::Eigs()
{

  Tvec = NULL;
  T = NULL;
  tlen = 0;
  eig_vecs = NULL;
  eig_vals = NULL;

  //parameters intialization
  mat_size = 0;
  num_of_eig_vec = 0;
  strcpy(bmat, "I");		// Av = lv non generalized decomposition
  strcpy(which, "LM");	//look for the smallest eigenvalues
  tol = 0.0;
  rvec = 1;			//also calculate eigenvectors
  modifyT = 0;		// avoids deleteing allocations done outside the class
  //put to 1 whenever creating a copy
}

Eigs::~Eigs()
{

  if ((T) && (modifyT)) {
    delete[]T;
    T = NULL;
  }

  if ((Tvec) && (modifyT)) {
    delete[]Tvec;
    Tvec = NULL;
  }

  if (eig_vecs) {
    delete[]eig_vecs;
    eig_vecs = NULL;
  }
  if (eig_vals) {
    delete[]eig_vals;
    eig_vals = NULL;
  }

}

void Eigs::release()
{

  if ((T) && (modifyT)) {
    delete[]T;
    T = NULL;
  }

  if ((Tvec) && (modifyT)) {
    delete[]Tvec;
    Tvec = NULL;
  }

  if (eig_vecs) {
    delete[]eig_vecs;
    eig_vecs = NULL;
  }
  if (eig_vals) {
    delete[]eig_vals;
    eig_vals = NULL;
  }
}


int Eigs::dsaupd()
{
  if (!T) {
    std::
      cout <<
      "Can not perform eigen-decomposition, matrix has not yet ben loaded "
	   << std::endl;
    return 1;
  }

  if (eig_vals) {
    delete[]eig_vals;
    eig_vals = NULL;
  }

  if (eig_vecs) {
    delete[]eig_vecs;
    eig_vecs = NULL;
  }
  // Re-Allocate values 
  eig_vals = new double[num_of_eig_vec];
  memset(eig_vals,0, num_of_eig_vec * sizeof(double)) ;
  eig_vecs = new double[mat_size * num_of_eig_vec];
  memset(eig_vecs,0, mat_size * num_of_eig_vec * sizeof(double)) ;

  PRINT_COLOR_(fprintf(stderr, "Calling dsaupd \n"), RED);


  int ido = 0;		/* Initialization of the reverse communication parameter. */

  double *resid;
  resid = new double[mat_size];

  int ncv = 4 * num_of_eig_vec;	/* The largest number of basis vectors that will
				   be used in the Implicitly Restarted Arnoldi
				   Process.  Work per major iteration is
				   proportional to N*NCV*NCV. */
  if (ncv > mat_size)
    ncv = mat_size;

  double *v;
  int ldv = mat_size;
  v = new double[ldv * ncv];

  int *iparam;
  iparam = new int[11];	/* An array used to pass information to the routines
			   about their functional modes. */
  iparam[0] = 1;		// Specifies the shift strategy (1->exact)
  iparam[2] = 3 * mat_size;	// Maximum number of iterations
  iparam[6] = 1;		/* Sets the mode of dsaupd.
				   1 is exact shifting,
				   2 is user-supplied shifts,
				   3 is shift-invert mode,
				   4 is buckling mode,
				   5 is Cayley mode. */

  int *ipntr;
  ipntr = new int[11];	/* Indicates the locations in the work array workd
			   where the input and output vectors in the
			   callback routine are located. */

  double *workd;
  workd = new double[3 * mat_size];

  double *workl;
  workl = new double[ncv * (ncv + 8)];

  int lworkl = ncv * (ncv + 8);	/* Length of the workl array */

  int info = 0;		/* Passes convergence information out of the iteration
			   routine. */

  int *select;
  select = new int[ncv];
  double *d;
  d = new double[2 * ncv];	/* This vector will return the eigenvalues from
				   the second routine, dseupd. */
  double sigma;
  int ierr;

  /* Here we enter the main loop where the calculations are
     performed.  The communication parameter ido tells us when
     the desired tolerance is reached, and at that point we exit
     and extract the solutions. */

  do {
    dsaupd_(&ido, bmat, &mat_size, which, &num_of_eig_vec, &tol, resid,
	    &ncv, v, &ldv, iparam, ipntr, workd, workl,
	    &lworkl, &info);
    if ((ido == 1) || (ido == -1))
      av(mat_size, workd + ipntr[0] - 1, workd + ipntr[1] - 1);
  } while ((ido == 1) || (ido == -1));

  /* From those results, the eigenvalues and vectors are
     extracted. */

  if (info < 0) {
    std::cout << "Error with dsaupd, info = " << info << "\n";
    std::cout << "Check documentation in dsaupd\n\n";
	

  } else {
    dseupd_(&rvec, "All", select, d, v, &ldv, &sigma, bmat,
	    &mat_size, which, &num_of_eig_vec, &tol, resid, &ncv, v,
	    &ldv, iparam, ipntr, workd, workl, &lworkl, &ierr);

    if (ierr != 0) {
      std::cout << "Error with dseupd, info = " << ierr << std::endl ;;
      std::cout << "Check the documentation of dseupd" <<std::endl ;;
    } else if (info == 1) {
      std::cout << "Maximum number of iterations reached."<<std::endl ;;
    } else if (info == 3) {
      std::cout << "No shifts could be applied during implicit"<<std::endl ;;
      std::cout << "Arnoldi update, try increasing NCV."<<std::endl ;;
    }

    /* Before exiting, we copy the solution information over to
       the arrays of the calling program, then clean up the
       memory used by this routine.  For some reason, when I
       don't find the eigenvectors I need to reverse the order of
       the values. */
  }

#ifdef DEBUG_3
  std::cout << "EigVals ( "<<num_of_eig_vec << ") : " <<std::endl ;
#endif

  if (rvec == 0) {		// copy eigenvalues

    FATAL_(std::cout << "Reversing of the eigenvalues is not yet implemented" <<std::endl, FATAL_UNKNOWN_MODE) ;
	
  } else if (rvec == 1) {	// copy both
    PRINT_(std::cout << "Not reversing" <<std::endl) ;
      
    for (int i = 0; i < num_of_eig_vec; i++) {
      eig_vals[i] = d[i];
    }
      
#ifdef DEBUG_3
    for (int i = 0; i < num_of_eig_vec; i++) {
      std::cout << d[i] << std::endl ;
    }
    for (int j = 0; j < mat_size; j++) {
      for (int i = 0; i < num_of_eig_vec; i++) {
	std::cout << v[i * mat_size + j] << " " ; 
      }
      std::cout << std::endl ;
    }
    std::cout << std::endl ;
    std::cout << std::endl ;
#endif

    for (int i = 0; i < num_of_eig_vec; i++)
      for (int j = 0; j < mat_size; j++)
	eig_vecs[j * num_of_eig_vec + i] = v[i * mat_size + j];
  }

  delete[]resid;
  delete[]v;
  delete[]iparam;
  delete[]ipntr;
  delete[]workd;
  delete[]workl;
  delete[]select;
  delete[]d;
  PRINT_COLOR_(fprintf(stderr, "dsaupd finished \n"), RED);

  return 0;
}

void Eigs::av(int p_n, double *p_in, double *p_out)
{
  for (int i = 0; i < p_n; i++)
    p_out[i] = 0;
  for (int i = 0; i < tlen; i++)
    p_out[(int) T[i][0]] += p_in[(int) T[i][1]] * T[i][2];
}


int Eigs::setParams(int p_nev, char *p_which, char *p_bmat, int p_evec_on)
{
  //copy p_nev
  int l_retval = 0;
  if (p_nev > mat_size) {
    std::
      cout << "Requested number of eigenvalues " << p_nev <<
      " is bigger than the matrix size " << mat_size <<
      "(Check if matrix was loaded)" << std::endl;
    l_retval = 1;
    return l_retval;
  }
  if (p_nev <= 0) {
    std::cout << "Requested number of eigenvalues is <= 0 " << std::
      endl;
    l_retval = 1;
    return l_retval;
  }
  num_of_eig_vec = p_nev;

  //copy which
  if (strcmp(p_bmat, "I")) {
    std::
      cout <<
      "Cases where B matrix is different from I not yet implemented "
	   << std::endl;
    l_retval = 1;
  }
  strcpy(bmat, "I");		// Av = lv non generalized decomposition

  //copy bmat
  if ((strcmp(p_which, "SM")) && (strcmp(p_which, "LM")) && (strcmp(p_which, "SA")) && (strcmp(p_which, "LA"))) {
    std::
      cout << "Requested case " << p_which <<
      " not implemented. Set to LM" << std::endl;
    strcpy(which, "LM");
    l_retval = 1;
  } else
    strcpy(which, p_which);
  //estimate or not the eigenvectors
  if ((p_evec_on != 0) && (p_evec_on != 1)) {
    std::cout << "evec_on should be either 1 or 0. Set to 1" << std::
      endl;
    l_retval = 1;
    rvec = 1;
  } else
    rvec = p_evec_on;
  return l_retval;
}

FL *Eigs::getEigVecs()
{
  return eig_vecs;
}

FL *Eigs::getEigVals()
{
  return eig_vals;
}


int Eigs::getNumOfEigVec()
{
  return num_of_eig_vec;
}

int Eigs::getMatSize()
{
  return mat_size;
}

char *Eigs::getWhich()
{
  return which;
}



void Eigs::loadDummyMatrix()
{
  /*
    Here we generate the matrix T for the multiplication.
    It helps if we know the number of non-zero matrix
    elements before we find them, so that we can save on
    storage space.  If not, generate enough storage space
    for T to cover an upper limit.
  */
  num_of_eig_vec = 3;
  mat_size = 4;
  tlen = mat_size * mat_size;
  if ((Tvec) && (modifyT)) {
    delete[]Tvec;
    Tvec = NULL;
  }
  if ((T) && (modifyT)) {
    delete[]T;
    T = NULL;
  }
  Tvec = new FL[tlen * 3];
  T = allocateDMat(Tvec, tlen, 3);

  tlen = 0;
  for (int i = 0; i < mat_size; i++)
    for (int j = 0; j < mat_size; j++) {
      T[tlen][0] = i;
      T[tlen][1] = j;
      T[tlen][2] = pow(i + 1, j + 1) + pow(j + 1, i + 1);
      tlen++;
    }
  modifyT = 1;
}

int Eigs::loadSparseMatrix(int p_n, int p_tlen, double **p_T)
{
  // Use externally initialized matrix
  // Avoid deallocating
  if ((Tvec) && (modifyT)) {
    delete[]Tvec;
    Tvec = NULL;
  }
  if ((T) && (modifyT)) {
    delete[]T;
    T = NULL;
  }


  mat_size = p_n;
  tlen = p_tlen;
  T = p_T;
  modifyT = 0;		//prevent to write on the memory allocated outside the class (avoid freeing p_T)

#ifdef DEBUG_FULL
  {
    std::cout << "Sparse Matrix (double): " <<std::endl ;
    for (int i = 0; i < tlen; i++) {
      std::cout << p_T[i][0] << " " << p_T[i][1] << " " << p_T[i][2] <<std::endl ;
    }

    FL l_temp_mat[p_n * p_n] ;
    memset(l_temp_mat, 0, p_n * p_n * sizeof(FL)) ;
    
    for (int i = 0; i < tlen; i++) {
      l_temp_mat[ (int)p_T[i][0] * p_n + (int)p_T[i][1]] = p_T[i][2] ;
    }
    for (int i = 0; i < p_n; i++) {
      for (int j = 0; j < p_n; j++) {
	if (l_temp_mat[i* p_n +j]) {
	  PRINT_COLOR_(fprintf(stderr, "%.04f ", l_temp_mat[i* p_n +j] ), RED) ;
	}
	else {
	  fprintf(stderr, "%.04f ", l_temp_mat[i* p_n +j] ) ;
	}
      }
      std::cout << std::endl ;
    }
    std::cout << std::endl ;
  }
#endif
#ifdef DEBUG_3 
  {
    int l_max_data_size = 0 ;
    HASH_MAP_PARAM_(int, "max_data_size", Int, l_max_data_size) ;
    if ( p_n < l_max_data_size ) {
      FL **l_temp_mat ;
      l_temp_mat = new FL *[p_n] ;
      for (int i = 0 ; i < p_n ; ++i) {
	l_temp_mat[i] = new FL[p_n] ;
	memset(l_temp_mat[i], 0, p_n * sizeof(FL)) ;
      }
      
      for (int i = 0; i < tlen; i++) {
	l_temp_mat[ (int)p_T[i][0]][(int)p_T[i][1]] = p_T[i][2] ;
      }
      for (int i = 0; i < p_n; i++) {
	for (int j = 0; j < p_n; j++) {
	  if ( l_temp_mat[i][j] != l_temp_mat[j][i] )
	    std::cout << std::setprecision (20) <<"Mat is not sym on " << i << " " << j << " (" <<l_temp_mat[i][j] << " , " <<l_temp_mat[j][i] << ") " <<   std::endl ;
	}
      }
      
      for (int i = 0 ; i < p_n ; ++i) {
	delete [] l_temp_mat[i];
      }
      delete [] l_temp_mat ;
    }
    else {
      WARNING_(std::cout << "Cannot check properly the matrix symmetry. Data size is too large. Skipping this step." <<std::endl) ;
    }
  }
#endif



  return 0;
}

int Eigs::loadSparseMatrix(int p_n, int p_tlen, int **p_T)
{
  // Use externally initialized matrix
  // But copying because in int and not double format

  if ((Tvec) && (modifyT)) {
    delete[]Tvec;
    Tvec = NULL;
  }
  if ((T) && (modifyT)) {
    delete[]T;
    T = NULL;
  }
  //Reinit sizes and matrix T 
  mat_size = p_n;
  tlen = p_tlen;
  Tvec = new FL[tlen * 3];
  T = allocateDMat(Tvec, tlen, 3);



  for (int i = 0; i < tlen; i++) {
    T[i][0] = (FL) p_T[i][0];
    T[i][1] = (FL) p_T[i][1];
    T[i][2] = (FL) p_T[i][2];
  }
  modifyT = 1;

#ifdef DEBUG_FULL
  std::cout << "Sparse Matrix: " <<std::endl ;
  for (int i = 0; i < tlen; i++) {
    std::cout << p_T[i][0] << " " << p_T[i][1] << " " << p_T[i][2] <<std::endl ;
  }
#endif
#ifdef DEBUG_3
  {
    FL l_temp_mat[p_n * p_n] ;
    memset(l_temp_mat, 0, p_n * p_n * sizeof(FL)) ;
    
    for (int i = 0; i < tlen; i++) {
      l_temp_mat[ (int)p_T[i][0] * p_n + (int)p_T[i][1]] = p_T[i][2] ;
    }
    for (int i = 0; i < p_n; i++) {
      for (int j = 0; j < p_n; j++) {
	if ( l_temp_mat[i* p_n +j] != l_temp_mat[j * p_n + i ] )
	  std::cout << "Mat is not sym on " << i << " " << j << std::endl ;
      }
    }
    fflush(stdout) ;
    fflush(stdin) ;
    getchar() ;
  }
#endif

  return 0;
}

int Eigs::loadSparseMatrix(int p_n, int p_tlen, float **p_T)
{
  // Use externally initialized matrix
  // But copying because in int and not double format

  if ((Tvec) && (modifyT)) {
    delete[]Tvec;
    Tvec = NULL;
  }
  if ((T) && (modifyT)) {
    delete[]T;
    T = NULL;
  }
  //Reinit sizes and matrix T 
  mat_size = p_n;
  tlen = p_tlen;
  Tvec = new FL[tlen * 3];
  T = allocateDMat(Tvec, tlen, 3);



  for (int i = 0; i < tlen; i++) {
    T[i][0] = (FL) p_T[i][0];
    T[i][1] = (FL) p_T[i][1];
    T[i][2] = (FL) p_T[i][2];
  }
  modifyT = 1;

#ifdef DEBUG_FULL
  std::cout << "Sparse Matrix: " <<std::endl ;
  for (int i = 0; i < tlen; i++) {
    std::cout << p_T[i][0] << " " << p_T[i][1] << " " << p_T[i][2] <<std::endl ;
  }
#endif
#ifdef DEBUG_3
  {
    FL l_temp_mat[p_n * p_n] ;
    memset(l_temp_mat, 0, p_n * p_n * sizeof(FL)) ;
    
    for (int i = 0; i < tlen; i++) {
      l_temp_mat[ (int)p_T[i][0] * p_n + (int)p_T[i][1]] = p_T[i][2] ;
    }
    for (int i = 0; i < p_n; i++) {
      for (int j = 0; j < p_n; j++) {
	if ( l_temp_mat[i* p_n +j] != l_temp_mat[j * p_n + i ] )
	  std::cout << "Mat is not sym on " << i << " " << j << std::endl ;
      }
    }
    fflush(stdout) ;
    fflush(stdin) ;
    getchar() ;
  }
#endif
  return 0;
}

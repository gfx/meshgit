#!/bin/sh


if [ $OSTYPE == "darwin9.0" ]
    then
     qm='qmake'

    "$qm" -spec macx-g++ match.pro
    sleep 2
    make -j 4
    sleep 2
    "$qm" -spec macx-g++ visu.pro
    sleep 2
    make -j 4
    sleep 2
    "$qm" -spec macx-g++ embed.pro
    sleep 2
    make -j 4
    sleep 2
elif [ $OSTYPE == "linux-gnu" ]
    then
    qmake-qt4 match.pro 
    sleep 2
    make -j 4
    sleep 2
    qmake-qt4 visu.pro
    sleep 2
    make -j 4
    sleep 2
    qmake-qt4 embed.pro 
    sleep 2
    make
    sleep 2
fi

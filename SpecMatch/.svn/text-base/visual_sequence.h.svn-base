/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
/*
 *  visual_sequence.h
 *  
 *
 *  Created by Diana Mateus and David Knossow on 11/6/07.
 *  Copyright 2007 INRIA. All rights reserved.
 *
 * This class derives from the Sequence class. it provides
 * a GUI to display the voxels, meshes, embeddings and histograms.
 * It is used by embedmat.
 */

#ifndef VISUAL_SEQUENCE_H
#define VISUAL_SEQUENCE

#include <QGLViewer/qglviewer.h>

#ifndef QT_VERSION
#include <qglobal.h>
#endif




#include "types.h"
#include "Constants.h"

#include "embed.h"
#include "embedding.h"
#include "sequence.h"

#ifdef MACOSX
#include <QEvent.h>
#endif


#if QT_VERSION < 0X040000
# include <qpopupmenu.h>
#else
# include <QMenu>
# include <QKeyEvent>
#endif

#include <qmap.h>

#ifdef ENABLE_DISP_HISTOGRAM
#if QT_VERSION > 0x040000
#include "DispHistograms.Qt4.h"
#else
#include "DispHistograms.h"
#endif
#endif

#include "histograms.h"




class VisualSequence:public QGLViewer, public Sequence {

  private:
    // Is animation on or off. It must be on to launch 
    // for the first time the processes. It is switched off 
    // automatically right after. Enabling animation allows
    // to process sequences or display sequence of embeddings.
    bool animate_on;

    // Current displayed eigenfunction
    int current_ef;

    // Enum type of what to display. Set according to keys have been pressed.
    DISPLAY display;

    // Allows to initialize the first launch of the process.
    bool first_shown;

    //show (from a file) or estimate the embedding
    bool embed_on;

    
    //Histograms
    FL *vec_hist;
    bool save_on;
    Histogram C_Histo;

#ifdef ENABLE_DISP_HISTOGRAM
    HistoWidget *C_Histo_Disp;
#endif

  protected: virtual void draw();
    virtual void init();
    virtual void animate();
    virtual void keyPressEvent(QKeyEvent * e);
    virtual QString helpString() const;
    void toggleAnimation();
    int updateDisplay();

    // Increments or decrements ef.
    int updateEigenfunction(int add_to_ef);

  public:
     VisualSequence();
    ~VisualSequence();

    // Release all allocations
    void visualRelease();

    // Launch the core processings
    int process();
    // Go to next frame
    int next();
    // Go to previous frame
    int previous();

    //Sets embed_on.
    int setEmbeddingOn(bool on);

    
    //Additional 2D plot. Add Colorbar to the display when displaying eigenvectors.
    int drawColorbar();


#ifdef ENABLE_DISP_HISTOGRAM
    //Showing histograms
    int fillHistograms();
    int showHistograms();
    int updateHistograms();
#endif

};
#endif

/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
/*
 *  utilities.h
 *  SpecMatch
 * 
 *  Created by Diana Mateus and David Knossow on 10/6/07.
 *  Copyright 2007 INRIA. All rights reserved.
 *
 * Provides functions or macros for convenience.
*/

#ifndef UTILS_H
#define UTILS_H

#include <stdio.h>
#include <stdlib.h>

extern "C" {
#include "glib.h"
} 

#include "types.h"

#include "EulerAngles.h"

// Defines for sake of debugging or watching the evolution of the algorithm
#define PRINT_COLOR
//#define PRINT
//#define SAVE_TEMP_DATA
//#define DEBUG
//#define DEBUG_2
//#define DEBUG_3
//#define DEBUG_FULL
//#define DEBUG_SPREAD


// Defining colors for printing output on stderr, stdout.
#define GREEN 32
#define RED 31
#define BLACK 30
#define BLUE 34

/*
30    Black
31    Red
32    Green
33    Yellow
34    Blue
35    Magenta
36    Cyan
37    White

Background colors
40    Black
41    Red
42    Green
43    Yellow
44    Blue
45    Magenta
46    Cyan
47    White
*/


#ifdef DEBUG_VISU
#define DBG_VISU_(_x)       ((void)(_x))
#else
#define DBG_VISU_(_x)       ((void)0)
#endif



#ifdef DEBUG_3
#define DEBUG_2
#define DEBUG
#define DBG3_(_x)       ((void)(_x))
#define DBGout3 std::cout
#else
#define DBG3_(_x)       ((void)0)
#define DBGout3 if(0) cerr
#endif

#ifdef DEBUG_2
#define DEBUG
#define DBG2_(_x)       ((void)(_x))
#define DBGout2 std::cout
#else
#define DBG2_(_x)       ((void)0)
#define DBGout2 if(0) cerr
#endif

#ifdef DEBUG
#define DBG_(_x)       ((void)(_x))
#define DBGout std::cout
#else
#define DBG_(_x)       ((void)0)
#define DBGout if(0) cerr
#endif


#ifdef PRINT
#define PRINT_(_x)     ((void)(_x))
#define PRINTout std::cout
#else
#define PRINT_(_x)     ((void)0)
#define PRINTout if(0) cerr
#endif

#ifdef PRINT_COLOR
#define PRINT_COLOR_(_X, COL) (void)fprintf(stderr, "%c[%dm", 0x1B, (int)COL) ; _X ; (void)fprintf(stderr, "%c[%dm", 0x1B,BLACK)
#else
#define PRINT_COLOR_(_X, COL)     ((void)0)
#endif


#define FATAL_(_X, CODE) _X ; exit((int)CODE) ;

#define ERROR_(_X, CODE) {_X ; return((int)CODE) ;}


#define WARNING_(_X) _X ;


#ifndef DEBUG
#define BGout if(0) cerr
#endif

#define DEBUG_HASH_MAP_PARAMS

#ifdef DEBUG_HASH_MAP_PARAMS 
#define HASH_MAP_PARAM_(TYPE, NAME, FUNC, OUT) \
  do {									\
    hash_map< std::string, Variant, hash_func >::iterator it = hash_map_global_params.find(NAME) ; \
    if (it == hash_map_global_params.end()) {				\
      std::cout << NAME << " does not exists" <<std::endl ;		\
      exit (0) ;							\
    }									\
    else {								\
      OUT = (TYPE)it->second.FUNC () ;					\
    }									\
  } while(0)
#else
#define HASH_MAP_PARAM_(TYPE, NAME, FUNC, OUT) \
  do {									\
    hash_map< std::string, Variant, hash_func >::iterator it = hash_map_global_params.find(NAME) ; \
    OUT = (TYPE)it->second.FUNC () ;					\
  } while(0)
#endif








#ifndef MAX
#define MAX(a,b)	((a)>(b)?(a):(b))
#endif


extern "C" {

    // Allocate a matrix from a vector.
    // double format
    FL **allocateDMat(FL * vec, int size_h, int size_w);
    // float format
    float **allocateFMat(float *vec, int size_h, int size_w);
    // integer format
    int **allocateIMat(int *vec, int size_h, int size_w);


    FULL_MAT_TYPE **allocateMat(FULL_MAT_TYPE * vec, int size_h, int size_w);

    // Store in a file a matrix (stored as a vector)
    // FL format
    int print_flmatrix(FL * matrix, const char *filename, int rows,
		       int cols);
    // FULL_MAT_TYPE format
    int print_full_matrix(FULL_MAT_TYPE * matrix, const char *filename, int rows,
		       int cols);
    // Integer format
    int print_intmatrix(int *matrix, const char *filename, int rows,
			int cols);
    // float format
    int print_floatmatrix(float *matrix, const char *filename, int rows,
			  int cols);


    // Compare to values ((SparseStorage*)d1->value) and returns -1 if d1 > d2.
    int compareFuncOnSparseStorage(const void *d1, const void *d2);
    // Provided for the sake of C-sort function

    // Compares (FL*)d1 sand (FL*)d2. Returns -1 if d1 < d2.
    int compareFunc(const void *d1, const void *d2);

    // Compares (FL*)d1 sand (FL*)d2. Returns -1 if d1 > d2.
    int compareValueFuncDec(const void *d1, const void *d2);

    // Compares (FL*)(d1 + 2) sand (FL*)(d2 + 2). Returns -1 if d1 < d2.
    int compareFuncSortVertexY(const void *d1, const void *d2) ;

    // Delete hashtable data storage.
    void DeleteHashTable_value(gpointer data) ;


    
    //Performs P' = temp * Point.
    float * RotatePoint(double **temp, float * Point) ;

    //Performs M = Rx(in(0)) * Ry(in(1)) * Rz(in(2))
    void ConvertXYZ2Mat ( float * in, HMatrix M) ;

    //Generate a Gaussian noise 
    double gauss_rand (double mean, double dev) ;

}
#endif

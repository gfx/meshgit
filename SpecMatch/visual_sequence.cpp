/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
/*
 *  visual_sequence.cpp
 *  
 *
 *  Created by Diana Mateus and David Knossow on 11/6/07.
 *  Copyright 2007 INRIA. All rights reserved.
 *
 */


#include "visual_sequence.h"

#define PROJECT_SAMPLE 3

VisualSequence::VisualSequence()
{
    C_Voxel = NULL;
    C_Embedding = NULL;
    C_Mesh = NULL;

        
    n_frames = 0;

    voxel_basename = NULL;

    pattern = NULL;


    nei_basename = NULL;
    lbl_basename = NULL;
    embed_basename = NULL;
    display = VOXEL_DISP;

    current_ef = 0;

    embed_on = true;		// if false and the file of the embedding is set tries to read from it
    save_on = false;
    
#ifdef ENABLE_DISP_HISTOGRAM
    //histograms
    C_Histo_Disp = NULL;
#endif
    vec_hist = NULL;

    //animation
    first_shown = false;
    animate_on = true;




    thres_embed_small_norms = -1 ;


    startAnimation();
}

VisualSequence::~VisualSequence()
{
    if (C_Voxel) {
	for (int i = 0; i < n_frames; i++)
	    delete C_Voxel[i];
	delete[]C_Voxel;
    }
    if (C_Mesh) {
	for (int i = 0; i < n_frames; i++)
	    delete C_Mesh[i];
	delete[]C_Mesh;
    }
    

    
    if (C_Embedding) {
	for (int i = 0; i < n_frames; i++)
	    delete C_Embedding[i];
	delete[]C_Embedding;
    }
    if (voxel_basename)
	delete[]voxel_basename;
    if (nei_basename)
	delete[]nei_basename;
    if (embed_basename)
	delete[]embed_basename;
    if (filename)
	delete[]filename;
    if (pattern)
	delete[]pattern;
    }

void VisualSequence::visualRelease()
{

    release();
    }

QString VisualSequence::helpString() const
{
  QString text("<h2>E m b e d</h2>");
  text += "Use the mouse to move the camera around the object. ";
  text += "You can respectively revolve around, zoom and translate with the three mouse buttons. ";
  text += "Left and middle buttons pressed together rotate around the camera view direction axis<br><br>";
  text += "Press <b>A</b> for the world axis, ";
  text += "<b>Alt+Return</b> for full screen mode and <b>Control+S</b> to save a snapshot. ";
  text += "See the <b>Keyboard</b> tab in this window for a complete shortcut list.<br><br>";
  text += "Double clicks automates single click actions: A left button double click aligns the closer axis with the camera (if close enough). ";
  text += "A middle button double click fits the zoom of the camera and the right button re-centers the scene.<br><br>";
  text += "Press <b>Escape</b> to exit the viewer.";
  return text;
}



void VisualSequence::init()
{
    DBG_(fprintf(stderr, "VisualSequence init\n"));


    
    if (n_frames > 1) {
	setKeyDescription(Qt::Key_N, "Go to next time instant");
	setKeyDescription(Qt::Key_B, "Go to previous time instant");
    }


    if (C_Voxel) {
	setKeyDescription(Qt::Key_P, "Display Voxel grid as Points");
	setKeyDescription(Qt::Key_V, "Display Voxels");
        setKeyDescription(Qt::Key_L, "Display Labels");
	    }
    
    
    setKeyDescription(Qt::Key_E,
		      "Display Embeddings and run through them forward");
    
    if (C_Mesh) {
	setKeyDescription(Qt::Key_M, "Display the Mesh");
    }
    
    setKeyDescription(Qt::Key_C, "Display the Connectivity Graph");

    setKeyDescription(Qt::Key_F,
		      "Display the eigenfunctions and run through them forward");
    setKeyDescription(Qt::Key_D,
		      "Display the eigenfunctions and run through them backward");


    setKeyDescription(Qt::Key_S, "Toggles savings");


    // Disabling here the standard keyboard key binding
    setShortcut(STEREO, 0) ;
    setShortcut(DISPLAY_FPS, 0) ;
    setShortcut(CAMERA_MODE, 0) ;

#ifdef ENABLE_DISP_HISTOGRAM
    setKeyDescription(Qt::Key_I, "Shows histograms");
#endif

    //show selected dimensions
    setTextIsEnabled(true);

    // Restore previous state.
    restoreStateFromFile();

    // Define lighting
    float mat_specular[] = { 0.9f, 0.9f, 0.9f, 1.0f };
    float mat_shininess[] = { 50.0f };
    float light_position_0[] = { 1.0f, 1.0f, 0.0f, 0.0f };
    float light_position_1[] = { -1.0f, -1.0f, 0.0f, 0.0f };
    float light_position_2[] = { -1.0f, 1.0f, 0.0f, 0.0f };
    float light_position_3[] = { 1.0f, -1.0f, 0.0f, 0.0f };
    float light_position_4[] = { 0.0f, 1.0f, 0.0f, 0.0f };
    float light_position_5[] = { 1.0f, 0.0f, 0.0f, 0.0f };

    glClearColor(1.0, 1.0, 1.0, 0.0);
    glShadeModel(GL_SMOOTH);

    float global_ambient[] = { 0.9f, 0.9f, 0.9f, 1.0f };
    glLightModelfv(GL_LIGHT_MODEL_AMBIENT, global_ambient);

    glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
    glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);
    glLightfv(GL_LIGHT0, GL_POSITION, light_position_0);
    glLightfv(GL_LIGHT1, GL_POSITION, light_position_1);
    glLightfv(GL_LIGHT2, GL_POSITION, light_position_2);
    glLightfv(GL_LIGHT3, GL_POSITION, light_position_3);
    glLightfv(GL_LIGHT4, GL_POSITION, light_position_4);
    glLightfv(GL_LIGHT5, GL_POSITION, light_position_5);


    glEnable(GL_LIGHTING);
    glEnable(GL_LIGHT0);
    glEnable(GL_DEPTH_TEST);

    setAnimationPeriod(100);

    DBG_(fprintf(stderr, "VisualSequence Initialized\n"));
    setSnapshotFileName("reconst3d");
    setSnapshotFormat("PNG");



}

void VisualSequence::draw()
{
    char l_text[64];
    int l_list;

    if (save_on) {
	sprintf(l_text, " Save images is enabled (press S to disable)" );
	glColor3f(0.0, 0.0, 0.0);
	drawText(200, 15, l_text);
    }


    switch (display) {
    case POINT_DISP:
	if (C_Voxel && C_Voxel[current_f]) {
	    C_Voxel[current_f]->drawVoxelsetPoints();
	}
	break;
    case VOXEL_DISP:
	if (C_Voxel && C_Voxel[current_f]) {
	    C_Voxel[current_f]->drawVoxelsetList();
	}
	break;
    case LABEL_DISP:
	if (C_Voxel && C_Voxel[current_f]) {
	    C_Voxel[current_f]->drawVoxelsetList();
	}
	break;
    case EIGENFUNCTION_DISP:
	if (C_Voxel && C_Voxel[current_f]) {
	    C_Voxel[current_f]->drawVoxelsetList();
	    drawColorbar();
	    FL *eval = C_Embedding[current_f]->getEigenvalues();
	    sprintf(l_text, " Eigenfunction %d Eigenvalue %g", current_ef,
		    eval[current_ef]);
	    glColor3f(0.0, 0.0, 0.0);
	    drawText(10, 30, l_text);
	} else if (C_Mesh && C_Mesh[current_f]) {
	    C_Mesh[current_f]->drawMeshsetFacets();
	    
	    drawColorbar();
	    FL *eval = C_Embedding[current_f]->getEigenvalues();
	    sprintf(l_text, " Eigenfunction %d Eigenvalue %g", current_ef,
		    eval[current_ef]);
	    glColor3f(0.0, 0.0, 0.0);
	    drawText(10, 30, l_text);
	    
	}
		
	break;
    case GRAPH_DISP:
	if (C_Voxel && C_Voxel[current_f]) {
	    if (nei_basename && !C_Voxel[current_f]->isGraphLoaded()) {
		sprintf(filename, pattern, nei_basename,
			current_f + start);
		strcat(filename, ".nei");
		FL *vox = C_Voxel[current_f]->getVoxels();

		if (C_Embed.C_Mat_dist.
		    loadData(vox, C_Voxel[current_f]->getNumOfVoxels(),
			     C_Voxel[current_f]->getDim())) {
		    WARNING_(std::
			     cout << "Can not display the connectivity..."
			     << std::endl);
		    break;
		}
		C_Embed.C_Mat_dist.setParameters(dist_mode, K, epsilon,
						 seq_depth);
		if (C_Embed.C_Mat_dist.setNeiFilename(filename)) {
		    WARNING_(std::
			     cout << "Can not display the connectivity..."
			     << std::endl);
		    break;
		}
		if (C_Embed.C_Mat_dist.fillNeighborhoodMatrix()) {
		    WARNING_(std::
			     cout << "Can not display the connectivity..."
			     << std::endl);
		    break;
		}
		C_Voxel[current_f]->loadGraph(C_Embed.C_Mat_dist.
					      getNzelems(),
					      C_Embed.C_Mat_dist.
					      getDistMatrix());
	    }

	    C_Voxel[current_f]->drawVoxelsetGraph();
	}
	if (C_Mesh && C_Mesh[current_f]) {
	    C_Mesh[current_f]->drawMeshsetFacets( true ) ; // drawedges and facets.
	    C_Mesh[current_f]->drawMeshsetNei( ) ;    
	}
		break;
    case EMBED_DISP:
	if (C_Embedding && C_Embedding[current_f]) {
	    l_list = int (current_ef) / 3;
	    C_Embedding[current_f]->drawCoordsList(l_list);
	    
	    drawColorbar();
	    sprintf(l_text, " Eigenfunction %d, (%d %d %d)", current_ef,
		    l_list * 3, l_list * 3 + 1, l_list * 3 + 2);
	    glColor3f(0.0, 0.0, 0.0);
	    drawText(10, 30, l_text);
	    
	}
	break;
	    case MESH_POINT_DISP:
	if (C_Mesh && C_Mesh[current_f]) {
	    C_Mesh[current_f]->drawMeshsetFacets();

	}
		break;
		
    default:
	WARNING_(fprintf
		 (stderr, "Incorrect VisualSequence::draw() option\n"));
	break;
    }

    sprintf(l_text, "Frame %d", current_f);
    glColor3f(0.0, 0.0, 0.0);
    drawText(10, 15, l_text);
    if (save_on) {
	PRINT_(fprintf(stderr, "Saving image\n"));
	saveSnapshot(true, false);
    }

}


void VisualSequence::animate()
{
    if (animate_on) {
	next();
	if (!first_shown) {
	    first_shown = true;
	    animate_on = false;
	    stopAnimation();
	}
    } else
	stopAnimation();
}

int VisualSequence::next()
{
    ++current_f;
    if ((current_f >= n_frames) || (current_f < 0)) {
	current_f = 0;
    }
    int l_retval = process();
    return l_retval;
}

int VisualSequence::previous()
{
    --current_f;
    if ((current_f >= n_frames) || (current_f < 0)) {
	current_f = n_frames - 1;
    }
    int l_retval = process();
    return l_retval;
}

int VisualSequence::updateDisplay()
{
  int color_map_size = 0 ;
  HASH_MAP_PARAM_( int, "colormap_std_size", Int, color_map_size ) ;
  int color_depth = 0 ;
  HASH_MAP_PARAM_( int, "color_depth", Int, color_depth ) ;
  //Updates colors and lists
  if (C_Voxel && C_Voxel[current_f]) {
    if (C_Voxel[current_f]->isLoaded()) {
      switch (display) {
      case VOXEL_DISP:
	C_Voxel[current_f]->resetVoxelColor();
	C_Voxel[current_f]->buildVoxelsetList();
	break;
      case POINT_DISP:
	C_Voxel[current_f]->resetVoxelColor();
	C_Voxel[current_f]->buildPointList();
	break;
      case GRAPH_DISP:
	C_Voxel[current_f]->resetVoxelColor();
	C_Voxel[current_f]->buildPointList();
	break;
      case EIGENFUNCTION_DISP:
	if ( color_map_size < C_Voxel[current_f]->getNumOfVoxels()) {
	  C_Voxel[current_f]->buildColormap(color_map_size);
	}
	else {
	  C_Voxel[current_f]->buildColormap(C_Voxel[current_f]->getNumOfVoxels()) ;
	}
	C_Voxel[current_f]->fillColorWithFunctionValue(current_ef);
	C_Voxel[current_f]->buildVoxelsetList() ;
	break;
      case LABEL_DISP:
	if ( color_map_size < C_Voxel[current_f]->getNumOfVoxels()) {
	  C_Voxel[current_f]->buildColormap(color_map_size);
	}
	else {
	  C_Voxel[current_f]->buildColormap(C_Voxel[current_f]->getNumOfVoxels()) ;
	}
	C_Voxel[current_f]->fillLabelColorValue();
	C_Voxel[current_f]->buildVoxelsetList();
	break;
				case MESH_POINT_DISP:
			  			case EMBED_DISP:
		break;
	    }
	}
    }
    if (C_Mesh && C_Mesh[current_f] && C_Embedding
	&& C_Embedding[current_f] && C_Mesh[current_f]->isLoaded()
	&& C_Embedding[current_f]->isLoaded()) {
	if (display == EIGENFUNCTION_DISP) {
	  if ( color_map_size < C_Mesh[current_f]->getNumOfVertex()) {
		C_Mesh[current_f]->buildColormap(color_map_size);
	    } 
	    else {
		C_Mesh[current_f]->buildColormap(C_Mesh[current_f]->getNumOfVertex());
	    }

	    C_Mesh[current_f]->fillColorWithFunctionValue(current_ef);
	}
	else if (display == GRAPH_DISP) {
	    C_Mesh[current_f]->setAllPointsColor(0.5, 0.5, 0.5) ;
	}
	else if (display == MESH_POINT_DISP) {
	    C_Mesh[current_f]->setAllPointsColor(0.5, 0.5, 0.5) ;
	}

	C_Mesh[current_f]->buildMeshsetPointList();
    }

        




    if (C_Embedding) {
	if (C_Embedding[current_f]) {
	    if (display == EMBED_DISP) {
		if (C_Voxel && C_Voxel[current_f]) {
		    if ( color_map_size < C_Voxel[current_f]->getNumOfVoxels()) {
			C_Voxel[current_f]->buildColormap(color_map_size);
		    }
		    else {
			C_Voxel[current_f]->buildColormap(C_Voxel[current_f]->getNumOfVoxels()) ;
		    }
		    C_Voxel[current_f]->
			fillColorWithFunctionValue(current_ef);
		    C_Embedding[current_f]->loadColor(C_Voxel[current_f]->
						      getColorVector(),
						      C_Voxel[current_f]->
						      getNumOfVoxels(),
						      color_depth);
		}
		
		if (C_Mesh && C_Mesh[current_f]) {
		    if ( color_map_size < C_Mesh[current_f]->getNumOfVertex()) {
			C_Mesh[current_f]->buildColormap(color_map_size);
		    } 
		    else {
			C_Mesh[current_f]->buildColormap(C_Mesh[current_f]->getNumOfVertex());
		    }
		    
		    C_Mesh[current_f]->
			fillColorWithFunctionValue(current_ef);

		    C_Embedding[current_f]->loadColor(C_Mesh[current_f]->
						      getColorVector(),
						      C_Mesh[current_f]->
						      getNumOfVertex(),
						      color_depth);
		}
				

		if ( thres_embed_small_norms > 0 ) {
		    C_Embedding[current_f]->removeNonRelevantPoints ( thres_embed_small_norms ) ;
		}

		C_Embedding[current_f]->normalizeEmbedding(normalize_embed, 1) ;
		C_Embedding[current_f]->buildCoordsLists() ;

	    }
	}
    }


    updateGL();
    return 0;
}


int VisualSequence::process()
{

    //LOAD voxels 
    if (voxel_basename && (!C_Voxel[current_f]
			   || (C_Voxel[current_f]
			       && !C_Voxel[current_f]->isLoaded()))) {
	loadVoxelset();

	//TODO: make a flag to check labels loaded correctly
	if (lbl_basename) {
	    loadLabels();
	}
	
    }

    else if (mesh_basename && (!C_Mesh[current_f]
			  || (C_Mesh[current_f]
			      && !C_Mesh[current_f]->isLoaded()))) {
	loadMeshset();
	//TODO: make a flag to check labels loaded correctly
	if (lbl_basename) {
	    loadLabels();
	}

    }
        

    // READ previously calculated embeddings
    if ((!embed_on) && (embed_basename) &&
	(!C_Embedding[current_f] || !C_Embedding[current_f]->isLoaded())) {

	loadEmbedding();

	//if succesfully loaded
	if (C_Embedding[current_f]->isLoaded()) {
	    if ( thres_embed_small_norms > 0 ) {
		C_Embedding[current_f]->removeNonRelevantPoints ( thres_embed_small_norms ) ;
	    }
	    C_Embedding[current_f]->normalizeEmbedding(normalize_embed, 1) ;
	    C_Embedding[current_f]->buildCoordsLists();
	    if (C_Voxel && C_Voxel[current_f])
		C_Voxel[current_f]->
		    loadFunctionValue(C_Embedding[current_f]->getCoords(),
				      C_Embedding[current_f]->getDim());

	    if (C_Mesh && C_Mesh[current_f])
		C_Mesh[current_f]->
		    loadFunctionValue(C_Embedding[current_f]->getCoords(),
				      C_Embedding[current_f]->getDim());
	    	    	    
	}
	//current_ef = 0; //current eigenfunction
    }

    //EMBED VOXELS
    else if ((embed_on) && C_Voxel && (C_Voxel[current_f])
	     && (C_Voxel[current_f]->isLoaded())
	     && ((!C_Embedding[current_f])
		 || (C_Embedding[current_f]
		     && !C_Embedding[current_f]->isLoaded()))) {

	callEmbed();

	//Save the results in embedding

	C_Embedding[current_f]->loadEmbedding(C_Embed.getE(),
					      C_Embed.getNPoints(),
					      C_Embed.getEmbeddedDim(),
					      C_Embed.getEmbeddedDim());
	C_Embedding[current_f]->loadEigenvalues(C_Embed.getEigenValues(),
						C_Embed.getEmbeddedDim());

	if (C_Voxel[current_f]) {
	    //load connectivity to show;
	    C_Voxel[current_f]->loadGraph(C_Embed.C_Mat_dist.getNzelems(),
					  C_Embed.C_Mat_dist.
					  getDistMatrix());
	    C_Voxel[current_f]->loadFunctionValue(C_Embed.getE(),
						  C_Embed.
						  getEmbeddedDim());
	    current_ef = 0;	//current eigenfunction
	}
	    }
    //EMBED MESHES
    else if ((embed_on) && C_Mesh && C_Mesh[current_f]
	     && C_Mesh[current_f]->isLoaded() && ((!C_Embedding[current_f])
						  ||
						  (C_Embedding[current_f]
						   &&
						   !C_Embedding
						   [current_f]->
						   isLoaded()))) {

	callMeshEmbed();

	C_Embedding[current_f]->loadEmbedding(C_Embed.getE(),
					      C_Embed.getNPoints(),
					      C_Embed.getEmbeddedDim(),
					      C_Embed.getEmbeddedDim());
	C_Embedding[current_f]->loadEigenvalues(C_Embed.getEigenValues(),
						C_Embed.getEmbeddedDim());

	if (C_Mesh[current_f]) {
	    //load connectivity to show;
	    C_Mesh[current_f]->loadGraph(C_Embed.C_Mat_dist.getNzelems(),
					 C_Embed.C_Mat_dist.
					  getDistMatrix()) ;
	    C_Mesh[current_f]->loadFunctionValue(C_Embed.getE(),
						 C_Embed.getEmbeddedDim());
	    current_ef = 0;	//current eigenfunction
	}
	
    }
        


#ifdef ENABLE_DISP_HISTOGRAM
    //HISTOGRAMS
    if (C_Embedding[current_f] && C_Embedding[current_f]->isLoaded()
	&& C_Histo_Disp && C_Histo_Disp->isVisible()) {
	PRINT_(std::cout << "Process: refresh histograms" << std::endl);
	fillHistograms();
	updateHistograms();
    }
#endif
    updateDisplay();
    return 0;
}


void VisualSequence::keyPressEvent(QKeyEvent * e)
{

    switch (e->key()) {

	    case Qt::Key_N:	
	if (n_frames > 1) {
	    next();
	    updateGL();
	}
	break;

    case Qt::Key_B:
	if (n_frames > 1) {
	    previous();
	    updateGL();
	}
	    break;

    case Qt::Key_M:
	if (C_Mesh)
	  display = MESH_POINT_DISP;
			updateDisplay();
	break;

    case Qt::Key_P:
	if (C_Voxel) {
	    if (display != POINT_DISP) {
		display = POINT_DISP;
		updateDisplay();
	    }
	}
	break;

    case Qt::Key_E:
	if ((C_Voxel && !C_Voxel[current_f])
	    || (C_Mesh && !C_Mesh[current_f]) 
	    	    )
	    break;
	if (display == EMBED_DISP) {
	    updateEigenfunction(1);
	    updateDisplay();
	} else {
	    display = EMBED_DISP;
	    updateDisplay();
	}

	break;

    case Qt::Key_V:
	if (display != VOXEL_DISP) {
	    display = VOXEL_DISP;
	    updateDisplay();
	}
	break;

    case Qt::Key_C:
	if (display != GRAPH_DISP) {
	    display = GRAPH_DISP;
	    updateDisplay();
	}
	break;
    case Qt::Key_F:
	if ((C_Voxel && !C_Voxel[current_f])
	    || (C_Mesh && !C_Mesh[current_f])
	    	    )
	    break;
	if (display == EIGENFUNCTION_DISP)
	    updateEigenfunction(1);
	else
	    display = EIGENFUNCTION_DISP;
	updateDisplay();
	break;

    case Qt::Key_D:
	if ((C_Voxel && !C_Voxel[current_f])
	    || (C_Mesh && !C_Mesh[current_f])
	    	    )
	    break;
	if (display == EIGENFUNCTION_DISP) {
	    updateEigenfunction(-1);
	} else
	    display = EIGENFUNCTION_DISP;
	updateDisplay();
	break;

    case Qt::Key_L:
	if (C_Voxel) {
	    display = LABEL_DISP;
	    updateDisplay();
	}
	break;
	    case Qt::Key_S:
	if (save_on)
	    save_on = false;
	else
	    save_on = true;
	break;

#ifdef ENABLE_DISP_HISTOGRAM
    case Qt::Key_I:
	fillHistograms();

	if (C_Histo_Disp == NULL)
	    showHistograms();
	else if (!C_Histo_Disp->isVisible())
	    C_Histo_Disp->show();
	else if (C_Histo_Disp->isVisible())
	    updateHistograms();
	break;
#endif
    default:
	QGLViewer::keyPressEvent(e);

    }



    updateGL();
}

int VisualSequence::updateEigenfunction(int addval)
{
    current_ef += addval;
    if (C_Voxel) {
	if (current_ef < 0)
	    current_ef = C_Voxel[current_f]->getNumOfFunctions() - 1 ;
	if (current_ef >= C_Voxel[current_f]->getNumOfFunctions())
	    current_ef = 0;
    }
    if (C_Mesh) {
	if (current_ef < 0)
	    current_ef =  C_Mesh[current_f]->getNumOfFunctions() - 1 ;
	if (current_ef >= C_Mesh[current_f]->getNumOfFunctions())
	    current_ef = 0;
    }
            return current_ef;
}

void VisualSequence::toggleAnimation()
{
    animate_on = !animate_on;
    if (animate_on)
	startAnimation();
    else
	stopAnimation();
}

int VisualSequence::setEmbeddingOn(bool on)
{
    embed_on = on;
    return 0;
}



int VisualSequence::drawColorbar()
{
    int l_min_col = 0, l_max_col = 0, l_zero_col = 0;
    FL l_minf = 0, l_maxf = 0;
    GLfloat l_y = 0;
    int l_nc = 0;

    float *l_col = NULL;
    int l_col_depth = 0;

    GLfloat l_x0 = 10.0;
    GLfloat l_y0 = 50.0;

    GLfloat l_w, l_h;

    startScreenCoordinatesSystem();

    // Anti-aliassed characters
    glPushAttrib(GL_ALL_ATTRIB_BITS);
    glDisable(GL_LIGHTING);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_BLEND);
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_LINE_SMOOTH);
    glLineWidth(1.0);

    l_w = (float) width() / 20.0;
    l_h = (float) height() / 2.0;


    glBegin(GL_QUAD_STRIP);

    if (C_Voxel && C_Voxel[current_f]) {
	l_min_col = C_Voxel[current_f]->getMinCol();
	l_max_col = C_Voxel[current_f]->getMaxCol();
	l_zero_col = C_Voxel[current_f]->getZeroCol();

	l_minf = C_Voxel[current_f]->getFuncMin(current_ef);
	l_maxf = C_Voxel[current_f]->getFuncMax(current_ef);

	l_y = l_y0;
	l_nc = C_Voxel[current_f]->getNumOfColor();
	l_col = C_Voxel[current_f]->getColormap();
	l_col_depth = C_Voxel[current_f]->getColorDepth();
    }

    if (C_Mesh && C_Mesh[current_f]) {
	l_min_col = C_Mesh[current_f]->getMinCol();
	l_max_col = C_Mesh[current_f]->getMaxCol();
	l_zero_col = C_Mesh[current_f]->getZeroCol();



	l_minf = C_Mesh[current_f]->getFuncMin(current_ef);
	l_maxf = C_Mesh[current_f]->getFuncMax(current_ef);

	l_y = l_y0;
	l_nc = C_Mesh[current_f]->getNumOfColor();
	l_col = C_Mesh[current_f]->getColormap();
	l_col_depth = C_Mesh[current_f]->getColorDepth();
    }

    
    char l_text1[64];
    char l_text2[64];
    char l_text3[64];
    memset(l_text1, 0, 64 * sizeof(char)) ;
    memset(l_text2, 0, 64 * sizeof(char)) ;
    memset(l_text3, 0, 64 * sizeof(char)) ;
    for (int i = 0; i < l_nc; ++i) {
	int l_c = l_nc - i - 1;
	glColor3f(l_col[l_c * l_col_depth], l_col[l_c * l_col_depth + 1],
		  l_col[l_c * l_col_depth + 2]);
	glVertex2f(l_x0, l_y);
	glVertex2f(l_x0 + l_w, l_y);
	l_y += l_h / l_nc;
    }
    glEnd();

    for (int i = 0; i < l_nc; ++i) {
	int l_c = l_nc - i - 1;
	glColor3f(l_col[l_c * l_col_depth], l_col[l_c * l_col_depth + 1],
		  l_col[l_c * l_col_depth + 2]);
	
	
	if (l_c == l_min_col) {
	    sprintf(l_text1, "%f", l_minf);
	    l_y = l_y0 + (i + 1) * l_h / l_nc;
	    drawText((int) (l_x0 + 2.0 * l_w), (int) (l_y), l_text1);
	}
	if (l_c == l_max_col) {
	    sprintf(l_text2, "%f", l_maxf);
	    l_y = l_y0 + (i + 1) * l_h / l_nc;
	    drawText((int) (l_x0 + 2.0 * l_w), (int) (l_y), l_text2);
	}
	if (l_c == l_zero_col) {
	    sprintf(l_text3, "%f", 0.0);
	    l_y = l_y0 + (i + 1) * l_h / (l_nc);
	    drawText((int) (l_x0 + 2.0 * l_w), (int) (l_y), l_text3);
	}
    }
    glPopAttrib();
    stopScreenCoordinatesSystem();

    return 0;
}

#ifdef ENABLE_DISP_HISTOGRAM
int VisualSequence::fillHistograms()
{
    if (!(C_Embedding && C_Embedding[current_f])) {
	ERROR_(fprintf
	       (stderr,
		"Can not fill histograms, embedding not loaded \n"),
	       ERROR_EMPTY_DATA);
    }

    if (C_Embedding && C_Embedding[current_f]->isLoaded()) {
	FL *l_X = C_Embedding[current_f]->getCoords();
	int *l_mask = NULL;
	if (C_Voxel && C_Voxel[current_f]
	    && C_Voxel[current_f]->isLoaded())
	    l_mask = C_Voxel[current_f]->getMask() ;
	else if (C_Mesh && C_Mesh[current_f]
		 && C_Mesh[current_f]->isLoaded())
	    l_mask = C_Mesh[current_f]->getMask() ;
		int l_dim = C_Embedding[current_f]->getDim();
	int l_n = C_Embedding[current_f]->getNumOfPoints();

	if (vec_hist) {
	    delete[]vec_hist;
	    vec_hist = NULL;
	}

	int l_num_of_bins = 0 ;
	  HASH_MAP_PARAM_( int, "num_of_bins", Int, l_num_of_bins ) ;

	vec_hist = new FL[l_num_of_bins * l_dim];
	bzero(vec_hist,l_num_of_bins * l_dim * sizeof(FL));

	C_Histo.setParameters(l_dim, l_num_of_bins, filename);
	C_Histo.fillSingleHistogram(l_X, l_mask, vec_hist, l_n);

    }
    return 0;
}


int VisualSequence::showHistograms()
{
    if (!(C_Embedding && C_Embedding[current_f])) {
	ERROR_(fprintf
	       (stderr,
		"Can not display histograms, embedding not loaded\n"),
	       ERROR_EMPTY_DATA);
    }
    if (C_Histo_Disp == NULL)
	C_Histo_Disp = new HistoWidget();

    PRINT_(std::cout << "Showing Histograms" << std::endl);


#if QT_VERSION < 0x040000
    C_Histo_Disp->setCaption("Eigenfunction Histograms");
#else
    C_Histo_Disp->setWindowTitle("Eigenfunction Histograms");
#endif




    int l_dim = C_Histo.getDim();
    int *l_hist = new int[l_dim];

    for (int i = 0; i < l_dim; ++i)
	l_hist[i] = C_Histo.getNumberOfBins();

    int l_cols = 4;
    int l_rows = (int) ceil((float) l_dim / (float) l_cols);
    C_Histo_Disp->setDataToDisplay(l_dim, l_rows, l_cols, vec_hist,
				   l_hist);
    C_Histo_Disp->show();
    delete[]l_hist;

    return 0;
}

int VisualSequence::updateHistograms()
{

    PRINT_(std::cout << "Updating Histograms" << std::endl);

    int l_dim = C_Histo.getDim();
    int *l_hist = new int[l_dim];

    for (int i = 0; i < l_dim; ++i)
	l_hist[i] = C_Histo.getNumberOfBins();


    int l_cols = 4;
    int l_rows = (int) ceil((float) l_dim / (float) l_cols);
    C_Histo_Disp->setDataToDisplay(l_dim, l_rows, l_cols, vec_hist,
				   l_hist);

    C_Histo_Disp->update();

    delete[]l_hist;

    return 0;
}
#endif

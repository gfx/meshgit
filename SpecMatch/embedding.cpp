/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
/*
 *  embedding.cpp
 *  embed
 *
 *  Created by Diana Mateus and David Knossow on 9/14/07.
 *  Copyright 2007 INRIA. All rights reserved.
 *
 */

#include "embedding.h"

using namespace std;

Embedding::Embedding()
{

    loaded = false;
    center_embedding = false;
    coords = NULL;
    coords_transfo = NULL;
    nlist = 0;
    trio_list = NULL;
    eigenvalues = NULL;
    point_size = 3.0;
    current_list = 0;
    color_vec = NULL;
    color_depth = 3;
    color[0] = 0.8;
    color[1] = 0.2;
    color[2] = 0.2;
        
    
}


Embedding::~Embedding()
{

    if (coords) {
	delete [] coords;
	coords = NULL ;
    }
    if (trio_list) {
      for (int i = 0; i < nlist; i++)
	if (trio_list[i] != 0) 
	  glDeleteLists(trio_list[i], 1);
      delete [] trio_list;
    }
    
    if (eigenvalues)
	delete[]eigenvalues;

    if (color_vec)
	delete[]color_vec;

    
}





int Embedding::loadEigenvalues(FL * p_eigenvals, int p_dim)
{

    PRINT_(fprintf(stderr, "Loading Eigenvalues ...\n"));

    if (!p_eigenvals) {
	ERROR_(fprintf
	       (stderr,
		"Can not load eigenvalues from an empty pointer\n"),
	       ERROR_EMPTY_DATA);
    }

    if (dim != p_dim) {
	ERROR_(fprintf
	       (stderr,
		"The number of eigenvalues does not correspond to the current loaded embedding \n"),
	       ERROR_VARIABLE_INIT);
    }

    if (eigenvalues) {
	delete[]eigenvalues;
	eigenvalues = NULL;
    }
    eigenvalues = new FL[dim];

    for (int i = 0; i < dim; i++) {
	eigenvalues[i] = p_eigenvals[i];
    }
    PRINT_(fprintf(stderr, " ... Loaded\n"));

    return 0;
}

//////////////////////////////////////////////
// ATTENTION, SI ON A UN REMOVE FIRST DANS LES EIGENFUNCTIONS, POURQUOI PAS ICI ?????
//
// SI NOUS POUVONS SELECTIONNE UN SOUS ENSEMBLE DE EIGENVECTOR, POURQUOI PAS SELECTIONNER LE MEME SOUS ENSEMBLE DE EIGENVALUES ????
//////////////////////////////////////////////
int Embedding::readEigenvalues(const char *p_filename, int p_dim)
{

    PRINT_(fprintf(stderr, " Reading Eigenvalues from %s \n", p_filename));

    if (!p_filename) {
	ERROR_(fprintf
	       (stderr, "Can not read eigenvalues file not specified\n"),
	       ERROR_OPEN_FILE);
    }

    if (dim != p_dim) {
	ERROR_(fprintf
	       (stderr,
		"The number of eigenvalues does not correspond to the current loaded embedding (Read embedding first) \n"),
	       ERROR_VARIABLE_INIT);
    }

    if (eigenvalues) {
	delete[]eigenvalues;
	eigenvalues = NULL;
    }

    eigenvalues = new FL[dim];
    bzero(eigenvalues, dim * sizeof(FL));

    std::fstream infile;
    infile.open(p_filename);

    if (infile.is_open() == 0) {
	ERROR_(fprintf(stderr, "File %s not found \n", p_filename),
	       ERROR_OPEN_FILE);
    }


    for (int i = 0; i < dim; i++) {
	infile >> eigenvalues[i];
    }
    infile.close();

    return 0;

}


int Embedding::loadEmbedding(FL * p_E, int p_n, int p_dim, int p_vec_dim,
			     bool p_remove_first)
{

    if (!p_E) {
	ERROR_(fprintf
	       (stderr, "Can not load embedding from an empty pointer\n"),
	       ERROR_EMPTY_DATA);
    }

    PRINT_(fprintf
	   (stderr,
	    "Loading Embedding with %d points on %d dimensions ...", p_n,
	    p_dim));

    npoints = p_n;
    dim = p_dim;

    ////////////////////////////////////////////////////
    // POURQUOI NOUS ENLEVONS UNE DIMENSIONS SI NOUS FAISONS LE REMOVE FIRST.
    ////////////////////////////////////////////////////
    if (p_remove_first) {
	PRINT_(fprintf(stderr, "Removing first dimension\n"));
	dim--;
    }
    //Allocate memory for embeddings
    if (coords) {
	delete[]coords;
	coords = NULL;
    }
    coords = new FL[dim * npoints];

    FL *l_mean = new FL[dim];
    FL *l_minval = new FL[dim];
    FL *l_maxval = new FL[dim];

    for (int i = 0; i < dim; i++) {
	l_mean[i] = 0.0;
	l_minval[i] = 0.0;
	l_maxval[i] = 0.0;
    }

    //Copy coords
    for (int i = 0; i < npoints; i++) {
	for (int j = 0, d = 0; j < p_dim; j++, d++) {
	    if (p_remove_first && j == 0) {
		d--;
		continue;
	    }
	    coords[i * dim + d] = p_E[i * p_vec_dim + j];


	    l_mean[d] += (FL) coords[i * dim + d];

	    if ((coords[i * dim + d] < l_minval[d]) || (i == 0))
		l_minval[d] = (FL) coords[i * dim + d];

	    if ((coords[i * dim + d] > l_maxval[d]) || (i == 0))
		l_maxval[d] = (FL) coords[i * dim + d];
	}
	DBG_VISU_(fprintf(stderr, "\n"));
    }
    for (int j = 0; j < dim; j++)
	l_mean[j] = l_mean[j] / ((FL) npoints);

    PRINT_(fprintf(stderr, " ... Loaded\n"));

    //estimate center and scale

    FL l_deltamax = fabs(l_maxval[0] - l_minval[0]);

    for (int j = 1; j < dim; j++) {
	if (l_maxval[j] - l_minval[j] > l_deltamax)
	    l_deltamax = fabs(l_maxval[j] - l_minval[j]);
    }

    //center data 
    if (center_embedding) {
	for (int i = 0; i < npoints; i++) {
	    for (int j = 0; j < dim; j++) {
		coords[i * dim + j] = coords[i * dim + j] - l_mean[j];
	    }
	}
    }

    scale = l_deltamax;

    delete[]l_mean;
    delete[]l_minval;
    delete[]l_maxval;


    //Prepare the number of trios of 3 dimensions to show
    if (trio_list) {
      for (int i = 0; i < nlist; i++)
	if (trio_list[i] != 0 )
	  glDeleteLists(trio_list[i], 1);
      delete[]trio_list;
      trio_list = NULL;
    }

    nlist = (int) round((FL) dim / 3.0);
    trio_list = new GLuint[nlist];
    for (int i = 0; i < nlist; i++)
      trio_list[i] = 0 ;
    loaded = true;
    
    return 0;
}



int Embedding::readEmbedding(const char *p_filename, int p_dim,
			     bool p_remove_first, int p_sample_rate)
{

    FILE *f = NULL;
    f = fopen(p_filename, "r");

    if (!f) {
	ERROR_(fprintf(stderr, " %s file not found\n", p_filename),
	       ERROR_OPEN_FILE);
    }

    PRINT_COLOR_(fprintf
		 (stderr, " Reading Embedding from : %s\n", p_filename),
		 GREEN);

    int l_iret;
    char *l_ret = NULL;

    char l_dummy[64];
    //Read number of points and number of dimensions
    if (!(l_ret = fgets(l_dummy, sizeof(l_dummy), f))) {
	ERROR_(fprintf(stderr, "Error reading the function values file\n"),
	       ERROR_OPEN_FILE);
    }

    int l_f_dim;
    sscanf(l_dummy, "%d %d", &npoints, &l_f_dim);

    char *l_buf = NULL;
    int l_bufsize = l_f_dim * 64;
    l_buf = new char[l_bufsize];	//try to ensure everything fits all dimensions of a line fit in the buffer
    l_bufsize *= sizeof(char);
    memset(l_buf, 0, l_bufsize);

    //if no dimension was specified read it from file else force dimension to be in_dim
    if (p_dim == 0)
	dim = l_f_dim;
    else
	dim = p_dim;

    int l_read_dim = dim;
    if (p_remove_first) {
	PRINT_(fprintf(stderr, "Removing first dimension\n"));
	dim--;
    }
    npoints = npoints / p_sample_rate;
    PRINT_(fprintf
	   (stderr, " Allocating memory for %d points on %d dimensions\n",
	    npoints, dim));

    //Allocate memory for embeddings
    if (coords) {
	delete[]coords;
	coords = NULL;
    }
    coords = new FL[dim * npoints];



    FL *l_mean = new FL[dim];
    FL *l_minval = new FL[dim];
    FL *l_maxval = new FL[dim];

    for (int i = 0; i < dim; i++) {
	l_mean[i] = 0.0;
	l_minval[i] = 0.0;
	l_maxval[i] = 0.0;
    }

    int l_count;

    //Read coords
    char l_car;
    float l_dummyf;
    int l_inc;
    for (int i = 0, sample = p_sample_rate; i < npoints; sample++) {
	if (i != 0) {
	    PRINT_(fprintf(stderr, "\r|"));
	    //fprintf(stderr,"\r \r");
	}

	PRINT_(fprintf(stderr, "%d|", i));

	l_ret = fgets(l_buf, l_bufsize, f);

	if (sample == p_sample_rate) {
	    l_count = 0;
	    for (int j = 0, d = 0; j < l_read_dim; j++, d++) {
		sscanf(&l_buf[l_count], "%s", l_dummy);
		l_iret = sscanf(l_dummy, "%g", &l_dummyf);
		//TODO: Why doesnot work: iret = sscanf(&buf[count],"%g", &dummyf);
		if (l_iret == -1)
		    WARNING_(fprintf
			     (stderr,
			      "\n Embedding: There was a problem reading the file\n"));

		if (p_remove_first && j == 0) {
		    d--;
		} else {
		    coords[i * dim + d] = (FL) l_dummyf;

		    l_mean[d] += (FL) coords[i * dim + d];

		    if ((coords[i * dim + d] < l_minval[d]) || (i == 0))
			l_minval[d] = (FL) coords[i * dim + d];

		    if ((coords[i * dim + d] > l_maxval[d]) || (i == 0))
			l_maxval[d] = (FL) coords[i * dim + d];
		}
		//look for next value
		//skip read string
		l_inc = strlen(l_dummy);
		l_count = l_count + (int) l_inc;
		//look for next non space value
		do {
		    l_count++;
		    sscanf(&l_buf[l_count], "%c", &l_car);
		} while (l_car == ' ');

	    }
	    i++;
	    sample = 0;
	}
    }
    for (int j = 0; j < dim; j++)
	l_mean[j] = l_mean[j] / ((FL) npoints);

    PRINT_(fprintf(stderr, "\nEnd of function read_embedding\n"));

    fclose(f);
    if (l_buf)
	delete[]l_buf;

    //estimate center and scale
    FL l_deltamax = fabs(l_maxval[0] - l_minval[0]);

    for (int j = 1; j < dim; j++) {
	if (l_maxval[j] - l_minval[j] > l_deltamax)
	    l_deltamax = fabs(l_maxval[j] - l_minval[j]);
    }

    //center data 
    if (center_embedding) {
	for (int i = 0; i < npoints; i++) {
	    for (int j = 0; j < dim; j++) {
		coords[i * dim + j] = coords[i * dim + j] - l_mean[j];
	    }
	}
    }
#ifdef DEBUG_3
    std::cout << "Embedding stored: " <<std::endl ;
    for (int i = 0; i < npoints; i++) {
	for (int j = 0; j < dim; j++) {
	    std::cout << coords[i * dim + j] << " ";
	}
	std::cout << std::endl;
    }
    std::cout << std::endl;
#endif
    scale = l_deltamax;

    delete[]l_mean;
    delete[]l_minval;
    delete[]l_maxval;

    //Prepare the number of trios of 3 dimensions to show
    if (trio_list) {
	for (int i = 0; i < nlist; i++)
	    glDeleteLists(trio_list[i], 1);
	delete[]trio_list;
	trio_list = NULL;
    }
    nlist = (int) floor((FL) dim / 3.0);
    trio_list = new GLuint[nlist];
	for (int i = 0; i < nlist; i++)
		trio_list[i] = 0 ;
    loaded = true;

    return 0;
}




int Embedding::loadColor(float *color_in, int n, int depth)
{
    color_depth = 3 ;
    if (depth != -1) {
	color_depth = depth;
    }
    if (n != npoints) {
	ERROR_(fprintf
	       (stderr,
		"Can not load color values of a vector with different length\n"),
	       ERROR_VARIABLE_INIT);
    }
    if (color_vec) {
	delete[]color_vec;
	color_vec = NULL;
    }

    color_vec = new FL[color_depth * npoints];
    int ind = 0;
    int ind_in = 0;


    for (int i = 0; i < npoints; i++) {

	color_vec[ind] = color_in[ind_in];
	color_vec[ind + 1] = color_in[ind_in + 1];
	color_vec[ind + 2] = color_in[ind_in + 2];
	if (color_depth == 4)
	    color_vec[ind + 3] = 1;

	ind_in += color_depth;
	ind += color_depth;
    }

    return 0;
}


int Embedding::buildCoordsLists()
{
    if (!coords) {
	ERROR_(fprintf
	       (stderr,
		"Can not build coord list. Embedding has not been yet loaded\n"),
	       ERROR_EMPTY_DATA);
    }
    //Prepare the number of trios of 3 dimensions to show


    if (trio_list) {
	for (int i = 0; i < nlist; i++)
	    glDeleteLists(trio_list[i], 1);
	delete[]trio_list;
	trio_list = NULL;
    }
    nlist = (int) floor((FL) dim / 3.0);
    trio_list = new GLuint[nlist];
	for (int i = 0; i < nlist; i++)
		trio_list[i] = 0 ;


    FL x, y, z;
    for (int l = 0; l < nlist; l++) {
	trio_list[l] = glGenLists(1);
	//fprintf(stderr, "Embedding: list %d : dims %d-%d\n", (int)trio_list[l], l*3+1,l*3+3);
	glNewList(trio_list[l], GL_COMPILE);

	glPointSize(point_size);
	if (!color_vec)
	    glColor3f(color[0], color[1], color[2]);
	glBegin(GL_POINTS);

	for (int i = 0; i < npoints; i++) {
	    if (color_vec) {
		glColor3f(color_vec[i * color_depth],
			  color_vec[i * color_depth + 1],
			  color_vec[i * color_depth + 2]);
		//		std::cout << "color: " <<color_vec[i * color_depth] << " " << color_vec[i * color_depth + 1] << " " << color_vec[i * color_depth + 2] << std::endl ;
	    }
	    x = (float) (coords[i * dim + l * 3] / scale);
	    y = (float) (coords[i * dim + l * 3 + 1] / scale);
	    z = (float) (coords[i * dim + l * 3 + 2] / scale);
	    glVertex3f(x, y, z);
	}
	glEnd();
	glEndList();

    }
    return 0;
}





int Embedding::drawCoordsList()
{
    if (!trio_list) {
	ERROR_(fprintf
	       (stderr,
		"Cant draw coords list. List has not been yet built\n"),
	       ERROR_EMPTY_DATA);
    }
    glCallList(trio_list[current_list]);
    return 0;
}


int Embedding::drawCoordsList(int list_id)
{
    if (!trio_list) {
	ERROR_(fprintf
	       (stderr,
		"Cant draw coords list. List has not been yet built\n"),
	       ERROR_EMPTY_DATA);
    }
    if ((list_id < 0) || (list_id >= nlist)) {
	ERROR_(fprintf
	       (stderr,
		"Required list_id exceeds number of built lists\n"),
	       ERROR_VARIABLE_INIT);
    }
    glCallList(trio_list[list_id]);

    return 0;
}

FL *Embedding::getEigenvalues()
{
    return eigenvalues;
}

FL *Embedding::getCoords()
{

    return coords;
}

bool Embedding::isLoaded()
{
    return loaded;
}

int Embedding::getNumOfPoints()
{
    return npoints;
}

int Embedding::getDim()
{
    return dim;
}

int Embedding::getNumOfList()
{
    return nlist;
}

int Embedding::getCurrentList()
{
    return current_list;
}

int Embedding::setCurrentList(int new_id)
{
    if (new_id < 0) {
	current_list = nlist;
	return 1;
    };
    if (new_id > nlist) {
	current_list = nlist;
	return 1;
    }
    current_list = new_id;
    return 0;
}

FL Embedding::getScale()
{
    return scale;
}




int Embedding::normalizeEmbedding(NORMALIZATION_EMBEDDING p_type, int p_offset)
{

    FL *maxval = new FL[dim];
    switch (p_type) {
    case NO_NORM_EMBED:
	PRINT_COLOR_(std::cout << "Not normalizing. Scale is " <<scale << std::endl, BLUE) ;
	break;
    case NORM_MAX:	
	PRINT_COLOR_(std::cout << "Normalizing each eigenvector by max(abs(eigenvector))" <<std::endl, BLUE) ;
	for (int i = 0; i < npoints; i++) {
	    for (int d = 0; d < dim; d++) {
		if (maxval[d] < fabs(coords[i * dim + d])) {
		    maxval[d] = fabs(coords[i * dim + d]);
		}
	    }
	}
	for (int i = 0; i < npoints; i++) {
	    for (int d = 0; d < dim; d++) {
		coords[i * dim + d] /= maxval[d];
	    }
	}
	break;
    case NORM_UNIT:
	PRINT_COLOR_(std::cout << "Normalizing to unit sphere all the points with a scale of " <<scale << std::endl, BLUE) ;
	FL Sum ;
	scale = 1 ;
	for (int i = 0; i < npoints; i++) {
	    Sum = 0 ;
	    for (int d = 0; d < dim; d++) {
		Sum += (coords[i * dim + d] * coords[i * dim + d]) ;
	    }
	    if (Sum) {
		for (int d = p_offset; d < dim; d++) {
		    coords[i * dim + d] /= (scale * sqrt(Sum)) ;
		}					
	    }
	}
	
	break ;
    default:
	break;
    }


    delete [] maxval ;

    
    return 0;
}


void Embedding::removeNonRelevantPoints ( FL p_threshold ) {

    FL Sum ;
    for (int i = 0; i < npoints; i++) {
	Sum = 0 ;
	for (int d = 1; d < dim; d++) {
	    Sum += (coords[i * dim + d] * coords[i * dim + d]) ;
	}
	if (Sum < p_threshold) {
	    for (int d = 0; d < dim; d++) {
		coords[i * dim + d] = 0 ;
	    }
	}
    }
}


// Operations on the embedding
int Embedding::project(FL * projection, FL * function, int funcdim,
		       int maxdim, int sample)
{
    //project a multidimensional function in to the basis formed by a selection(maxdim) of eigenvalues and eigenfunctions 
    //vec = eigenvectors , weights = eigenvalues
    //funcdim is the dimension of function
    //maxdim is the number of eigenvectors to consider
    //returns the aproximation of function
    //result should have the same size as function
    //sampling rate to just overload some and not all the points in the function (default all)
    if (!projection) {
	ERROR_(fprintf(stderr, "Allocate projection result first \n "),
	       ERROR_EMPTY_DATA);
    }

    if (maxdim > dim) {
	ERROR_(fprintf
	       (stderr,
		"Can not project on a dimension larger than the size of the embedding \n"),
	       ERROR_VARIABLE_INIT);
    }

    if (!coords) {
	ERROR_(fprintf(stderr, "Can not project, empty embedding \n"),
	       ERROR_EMPTY_DATA);
    }
    bzero(projection, npoints * funcdim * sizeof(FL));

    //normalizes the eigenvectors to have unit norm
    FL *Ei_;
    FL *Ej_;

    FL *norm = NULL;
    /*
       norm=new FL[dim];
       bzero(norm,dim*sizeof(FL));
       for (int d = 0; d < dim; d++){
       FL sqsum=0.0;
       Ei_ = coords;
       for (int i = 0; i < npoints; i++ ,Ei_+=dim){
       sqsum+= (Ei_[d])*(Ei_[d]);
       }
       std::cout<< "L2 Norm of eigenvector "<< d << ":" << sqsum<<std::endl; 
       norm[d]=1.0/sqsum;
       }
     */

    //Basis = E*E' to get the projection basis
    PRINT_(std::
	   cout << "Sparse Projection on " << maxdim << " dimensions" <<
	   std::endl);
    FL *basis = NULL;
    basis = new FL[npoints];	//creates only line per line

    FL *basis_;
    FL *func_;
    FL *proj_;

    Ei_ = coords;
    func_ = function;
    proj_ = projection;
    FL error = 0.0;
    for (int i = 0; i < npoints;
	 i += sample, Ei_ += sample * dim, func_ +=
	 sample * funcdim, proj_ += sample * funcdim) {
	//Build row of projection matrix
	bzero(basis, npoints * sizeof(FL));
	basis_ = basis;
	Ej_ = coords;
	for (int j = 0; j < npoints; j++, Ej_ += dim, ++basis_) {
	    for (int d = 1; d < maxdim; d++) {
		//TODO: this should be dependent on the params.sqeignorm parameter 
		//*(basis_) += (eigenvalues[d]) *(Ei_[d]) * (Ej_[d]); //basis[j]+= eigenvals[d] *E[i*dim+d]*E[j*dim+d];
		*(basis_) += (eigenvalues[d]) * (Ei_[d]) * (Ej_[d]);
	    }
	}

	//Project on row;
	basis_ = basis;
	for (int j = 0; j < npoints; j++, ++basis_) {
	    for (int d = 0; d < funcdim; d++) {
		proj_[d] += (*basis_) * (func_[d]);	//projection[i*funcdim+d] += basis[j]*function[i*funcdim+d];
	    }
	}

	//Measure distortion;
	for (int d = 0; d < funcdim; d++) {
	    FL dif = func_[d] - proj_[d];
	    error += dif * dif;
	}
    }

    error = sqrt(error);
    PRINT_(std::cout << "Total error in samples " << error << std::endl);

    if (basis)
	delete[]basis;

    if (norm)
	delete[]norm;

    return 0;

}



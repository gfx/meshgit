/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
#ifndef _VARIANT_H
#define _VARIANT_H
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <iostream>

#include "types.h"
#include "utilities.h"

typedef struct {
  FL X ;
  FL Y ;
  FL Z ;  
} point_coords ;

typedef struct {
  float r ;
  float g ;
  float b ;
  float a ;
} point_colors ;

class Variant {

 private:
  union {
    int *_pInt ;
    float *_pFloat ;
    double *_pDouble ;
    std::string *_pString ;
    point_coords *_pCoord ;
    point_colors *_pColor ;
  } _value ;
  enum VALUE_TYPE {NONE_V_T, INT, FLOAT, DOUBLE, STRING, COORD, COLOR} ;
  
  const VALUE_TYPE value_type ;
  std::string v_name ;
 public:
  inline Variant(const std::string & name="NO SUCH VARIABLE" )
    :value_type(NONE_V_T), v_name(name)
    {
      std::cout << "No such variable" <<std::endl ;
      _value._pInt = NULL ; 
    }
  inline Variant(int i, const std::string & name )
    :value_type(INT), v_name(name)
    {
      _value._pInt = new int(i) ; 
    }
  inline Variant(float f, const std::string & name)
    :value_type(FLOAT), v_name(name) 
    {
      _value._pFloat = new float (f) ; 
    }
  inline Variant(double d, const std::string & name)
    :value_type(DOUBLE), v_name(name) 
    {
      _value._pDouble = new double(d) ; 
    }
  inline Variant(std::string s, const std::string & name)
    :value_type(STRING) , v_name(name)
    {
      _value._pString = new std::string(s) ;
    }


  inline Variant(point_coords c, const std::string & name)
    :value_type(COORD) , v_name(name)
    {
      _value._pCoord = new point_coords ;
      _value._pCoord->X = c.X ;
      _value._pCoord->Y = c.Y ;
      _value._pCoord->Z = c.Z ;
    }

  inline Variant(point_colors c, const std::string & name)
    :value_type(COLOR) , v_name(name)
    {
      _value._pColor = new point_colors ;
      _value._pColor->r = c.r ;
      _value._pColor->g = c.g ;
      _value._pColor->b = c.b ;
      _value._pColor->a = c.a ;
    }
    
  inline ~Variant() {
    switch ( value_type ) 
      {
      case INT:
	delete _value._pInt ;
	break ;
	
      case FLOAT:
	delete _value._pFloat ;
	break ;
	
	case DOUBLE:
	  delete _value._pDouble ;
	  break ;
	  
	case STRING:
	  delete _value._pString ;
	  break ;
      case COORD:
	delete _value._pCoord ;
	break ;

      case COLOR:
	delete _value._pColor ;
	break ;
      case NONE_V_T:
      default:
	break ;
	}	    
    }
    

  inline int &Bool() 
    {
      if (value_type == INT &&  (*(_value._pInt) == 0 || *(_value._pInt) == 1)) 
	return *(_value._pInt); 
      else 
	FATAL_(std::cout << "Asking for BOOL (" << v_name <<") but it is a " << value_type << std::endl, FATAL_UNKNOWN_FORMAT) ; 
    }
  inline int Bool() const 
    {
      if (value_type == INT &&  (*(_value._pInt) == 0 || *(_value._pInt) == 1)) 
	return *(_value._pInt); 
      else 
	FATAL_(std::cout << "Asking for BOOL (" << v_name <<") but it is a " << value_type << std::endl, FATAL_UNKNOWN_FORMAT) ; 
    }

  inline int &Int() 
    {
      if (value_type == INT ) 
	return *(_value._pInt); 
      else 
	FATAL_(std::cout << "Asking for INT (" << v_name <<") but it is a " << value_type << std::endl, FATAL_UNKNOWN_FORMAT) ; 
    }
  inline int Int() const 
    {      
      if (value_type == INT ) 
	return *(_value._pInt); 
      else 
	FATAL_(std::cout << "Asking for INT (" << v_name <<") but it is a " << value_type << std::endl, FATAL_UNKNOWN_FORMAT) ; 
    }
  
  inline float &Float() 
    {
      if (value_type == FLOAT ) 
	return *(_value._pFloat); 
      else 
	FATAL_(std::cout << "Asking for FLOAT (" << v_name <<") but it is a " << value_type << std::endl, FATAL_UNKNOWN_FORMAT) ;
  }
  inline float Float() const 
    {
      if (value_type == FLOAT ) 
	return *(_value._pFloat); 
      else 
	FATAL_(std::cout << "Asking for FLOAT (" << v_name <<") but it is a " << value_type << std::endl, FATAL_UNKNOWN_FORMAT) ;
    }
  
  inline double &Double() 
    {
      if (value_type == DOUBLE ) 
	return *(_value._pDouble); 
      else 
	FATAL_(std::cout << "Asking for DOUBLE (" << v_name << " " << *(_value._pInt) <<") but it is a " << value_type << std::endl, FATAL_UNKNOWN_FORMAT) ;
    } 
  
  inline double Double() const 
    {
      if (value_type == DOUBLE ) 
	return *(_value._pDouble); 
      else 
	FATAL_(std::cout << "Asking for DOUBLE (" << v_name <<") but it is a " << value_type << std::endl, FATAL_UNKNOWN_FORMAT) ;
    } 
  
  inline std::string &String() 
    {
      if (value_type == STRING ) 
	return *(_value._pString); 
      else 
	FATAL_(std::cout << "Asking for STRING (" << v_name <<") but it is a " << value_type << std::endl, FATAL_UNKNOWN_FORMAT) ;
    } 
  inline std::string String() const 
    {
      if (value_type == STRING ) 
	return *(_value._pString); 
      else 
	FATAL_(std::cout << "Asking for STRING (" << v_name <<") but it is a " << value_type << std::endl, FATAL_UNKNOWN_FORMAT) ;
    }





  inline point_coords &Coord() 
    {
      if (value_type == COORD ) 
	return *(_value._pCoord); 
      else 
	FATAL_(std::cout << "Asking for COORD (" << v_name <<") but it is a " << value_type << std::endl, FATAL_UNKNOWN_FORMAT) ;
    } 
  inline point_coords Coord() const 
    {
      if (value_type == COORD ) 
	return *(_value._pCoord); 
      else 
	FATAL_(std::cout << "Asking for COORD (" << v_name <<") but it is a " << value_type << std::endl, FATAL_UNKNOWN_FORMAT) ;
    }

  inline point_colors &Color() 
    {
      if (value_type == COLOR ) 
	return *(_value._pColor); 
      else 
	FATAL_(std::cout << "Asking for COLOR (" << v_name <<") but it is a " << value_type << std::endl, FATAL_UNKNOWN_FORMAT) ;
    } 
  inline point_colors Color() const 
    {
      if (value_type == COLOR ) 
	return *(_value._pColor); 
      else 
	FATAL_(std::cout << "Asking for COLOR (" << v_name <<") but it is a " << value_type << std::endl, FATAL_UNKNOWN_FORMAT) ;
    }

  

  inline Variant(const Variant & in)
    :value_type(in.value_type), v_name(in.v_name)
    {

      switch ( value_type ) 
	{
	case INT:
	  _value._pInt = new int(in.Int ()) ;
	  break ;
	  
	case FLOAT:
	  _value._pFloat = new float(in.Float ()) ;
	  break ;

	case DOUBLE:
	  _value._pDouble = new double(in.Double ()) ;
	  break ;

	case STRING:
	  _value._pString = new std::string(in.String ()) ;
	  break ;
	case COORD:
	  _value._pCoord = new point_coords ;
	  _value._pCoord->X = in.Coord ().X ;
	  _value._pCoord->Y = in.Coord ().Y ;
	  _value._pCoord->Z = in.Coord ().Z ;
	  break ;
	case COLOR:
	  _value._pColor = new point_colors ;
	  _value._pColor->r = in.Color ().r ;
	  _value._pColor->g = in.Color ().g ;
	  _value._pColor->b = in.Color ().b ;
	  _value._pColor->a = in.Color ().a ;
	  break ;
	case NONE_V_T:
	default:
	  break ;
	}
    }


} ;
#endif

/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
/*
 *  embed.h
 *  embed
 *
 *  Created by Diana Mateus and David Knossow on 10/7/07.
 *  Copyright 2007 INRIA. All rights reserved.
 *
 * This class provides the core processings to compute the embeddings of the 3D shape.
 * From a Distance matrix, namely the edges of the graph, it performs an eigenfunction decomposition
 * and provides than the eigenvectors or equivalently the embeddings.
 *
 * This class provides different methods to perform the embeddings : 
 * - Local linear embedding  (not implented).
 * - Laplacian eigenmaps
 * - Isomaps (not implented).
 *
 * To compute the Laplacian eigenmaps, different approaches had been tested. All of them are still accessible by setting the 
 * laplacian parameter in the params structure.
 *
 * 
 * The distance matrix is transformed in a Weight matrix. Three methods are available:
 * - a simple copy of the distance matrix
 * - replace all distance by 1
 * - compute the gaussian of the distance. The sigma factor can be computed or set manually
 *
 */

#ifndef EMBED_H
#define EMBED_H


#include <cv.h>
#include "Constants.h"
#include "types.h"
#include "utilities.h"
#include "timer.h"


#include "eigs.h"
#include "generalized_eigs.h"

#include "distances.h"


struct PARAMS {

    //describing the method used to perform the embedding. See above.
    METHOD method;

    // Only for laplacian embeddings topological/gaussian weights
    EDGES edges;

    // If method is the laplacian eigenmap, different options are possible to compute the SVD.
    LAPLACIAN laplacian;

    //method details
    // Number of eigenvectors to compute out of the weighted distance matrix.
    int n_eigenfunc;

    //regularization weight for LLE when embedding space has a higher dimension than the orginal one
    FL regular;

    // laplacian tolerance. Remove exponential values below this: =0.
    FL tolerance;

    //Scaling factor for laplacian matrix. If sigma is initialized to 0, this factor is calculated (only first time) then used
    // for the whole sequence. If different from 0, the assigned sigma is used for the whole sequence.
    FL sigma;


    //optional normalizations 
    bool remove_first;
    bool sq_eig_norm;

    
    NORMALIZATION_FORMAT normalization_format;

    // Set the normalization matrix (Diagonal matrix) with the approximation of voronoi area instand of the some of
    // coefficients of the laplacian matrix.
    bool set_D_as_Voronoi;

    // In case of DIFFUSION DISTANCE, the diffusion is taken into account by doing  LAMBDA^{depth}
    int eigenvalue_power;

};

class Embed {
  private:

    // See above.
    PARAMS params;

    // Status. Check if data had been loaded prior to perform the embeddings.
    bool loaded;

    // Allos to remove disconnected componnent in the voxel set or the mesh. 
    int *mask;

    // number of voxels or mesh vertices.   
    int npoints;


    // number of non zero elements in sparse distance matrices
    int nzelems_W;

    //number of non zero elements in M
    int nzelems_M;

    //pointers to the matrices containing the distances. The distances are from all points to all neighbors.
    //no memory allocation / deallocation inside the function or class.
    FL *sparse_W;

    //sparse matrix in the format (i,j,value). This matrix is built using sparseW.  
    FL **mat_sparse_W;


        // Stores the distances from all points to all other points.
    FULL_MAT_TYPE *full_W;

    //transformed matrices (ready to be decomposed);
    FULL_MAT_TYPE **mat_full_W;

    // fullM is built out of Wf (fullM[id] = exp(factor*Wf[idf])). 
    FULL_MAT_TYPE *full_M;

    //points to fullM
    FULL_MAT_TYPE **mat_full_M;
    

    // Depending on the Laplacian approach, sparseM is built differently out of sparseW.
    FL *sparse_M;
    //Matrix version of sparseM
    FL **mat_sparse_M;

    // Degree matrix (actually a nx1 vector with the diagonal)
    FL *D;

        //Call Embedding routines
    int lle();
    
        int isomap();
    int sparseIsomap();
    int fullIsomap();
        
    // Call subfunctions to perform the laplacian embedding.
    int laplacian();

    //Subfunctions for laplacian embedding
    // Compute the median value of a sparse matrix. // Called by sparse_scale
    FL sparseMedian(FL * sparse_mat, int size);

    // Compute the sigma factor for the laplacian sparse matrix.
    FL sparseScale(FL * sparse_mat, int size);


        // Compute the median value for the laplacian dense matrix.
    FULL_MAT_TYPE fullMedian(FULL_MAT_TYPE * array, int nelems);
    // Compute the sigma factor for the laplacian dense matrix.
    FULL_MAT_TYPE fullScale(FULL_MAT_TYPE * array, int nelems);
    
    // When the SVD is performed, depending on the method used, the eigenvectors can be stored from
    // the smallest value to the biggest or vice et versa. We need the eigenvalues (and associated eigenvectors) to
    // ordered from small to big.
    bool invert_order;

    // "LM" for largestmost and "SM" for smallest eigenvalues
    char which[3];

    // "I" for Av=lv and "G" for generalized Av=lBv
    char right_matrix[2];

    // Sotre the output of the SVD, after having applied the mask (to remove disconnected componnents)
    FL *eigen_vecs;

    // Stores the Voronoi area to normalize the laplacian.
    FL *Voronoi;

    bool enable_cut_disconnected_componnents ;
    
        FL* Points; // Keep a pointer to the initial data. Useful for LLE embedding on meshes
        
  protected:

    //eigendecomp
    // Perform the eigenfunction decomposition either on sparse or dence matrices. Call subfunctions
    int eigenDecomposition();

        // Obvious
    int fullEigenDecomposition();
    
    // Obvious
    int sparseEigenDecomposition();

    //Embedding result
    //dimension of the embedded space
    int embed_dim;

    //Embedding 
    FL *E;

    // Store eigenvalues.
    FL *eigen_values;
    
    
    // Allow to destroy or not the point full_M. Default is true.
    bool modify_M ;
    // Allow to destroy or not the point full_W. Default is true.
    bool modify_W ;
  public:
     Embed();
    ~Embed();
    void init () ;

    void release();

        // Compute the laplacian of the dense distance matrix.
    int fullLaplacian();
    
    // Compute the laplacian of the sparse distance matrix.
    int sparseLaplacian();

    //Class for pairwise relationships
    Distances C_Mat_dist;

    //Class to link to lapack function
    int maskDisconnectedComponents();
    int n_masked;

    //Functions to calculate the kernel matrix
    int sparsePairwiseMatrix(FL * data, int in_n, int in_nzelems);	// (nzerox3) functions to set the pointer to raw data
        int fullPairwiseMatrix(FULL_MAT_TYPE * data, int in_n);	// (nxn) functions to set the pointer to raw data
    
    //Function to find the coordinates of the embedding space (perform mapping)
    // prepares for eigendecomposition
    // Calls the distance matrix computation.
    // Create 
    int fillEmbeddingMatrices();	//
    int findEmbeddingCoords();	//calls fill_embedding_matrices and eigendecomposition

    // This function is an overload of int find_embedding_coords(). It alows to avoid all processing steps. 
    // We provide maually all required data.
    int findEmbeddingCoords(FL * spars_w, int n_elem /*num of vertex */ ,
			    int nz_elems);

    



    
    //Acces to Laplacian in matrix form (Ms) 
    int getNzelemsM();
    int getNzelemsW();
    FL *getD();
    FL *getLaplacian();		//Ms use fill_Ms to fill it from sparseM
    FL *getWeights();

    //for debugging
    int saveSparseMat(char *name, FL ** mat, int nz);
    int saveSparseVec(char *name, FL * mat, int nz);
    int saveDoubleMat(char *name, FL * mat, int n1, int n2);	//saves full matrices
    int saveFloatMat(char *name, float * mat, int n1, int n2);
    int printIntMatrix(int *matrix, const char *filename, int rows,
		       int cols);
    int saveFullMat(char *name, FULL_MAT_TYPE * mat, int n1, int n2);
    //saving results
    int saveEmbedding(char *name);
    int saveEigenValues(char *name);
    int saveDistMat(char *name);
    int saveSparseM(char *name);
    int saveParams(char *name);

    
    
    //set parameters
    int setMethod(METHOD method, LAPLACIAN laplacian, EDGES edges);
    int setSigma(FL in_sigma);
    int setNEigenFunc(int n_eigenfunc);
    int setRemoveFirst(bool remove);
    int setSqEigNorm(bool eignorm);
    int setNormalizationFormat(NORMALIZATION_FORMAT format);
    void enableCutDisconnectedComps (bool on) ;


    // In case of diffusion distance, the depth is taken into account by using (eigenvalue)^(power) 
    // instead of propagating the distance.
    int setNormalizationPower(int power);

        int setRegular(FL regular);

    //get parameters
    bool isLoaded();
    int getNPoints();
    int getNEigenFunc();
    bool getRemoveFirst();
    bool getSqEigNorm();
    NORMALIZATION_FORMAT getNormalizationFormat();
        FL getRegular();
    FL *getE();
    FL *getEigenValues();
    int getEmbeddedDim();


    void setDVecAsVoronoi(FL * in_voronoi);
  

        int loadPoints(FL* p_points);
    };

#endif

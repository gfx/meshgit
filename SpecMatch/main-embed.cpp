/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
/*
 *  main-embed.cpp
 *  
 *
 *  Created by Diana Mateus and David Knossow on 11/5/07.
 *  Copyright 2007INRIA. All rights reserved.
 *
 */

/*
 *  embedmat is provided to build the embeddings of both voxels and meshes in a metric space.
 *  This code refers to the paper accepted at CVPR 2008: 
 *  Articulated Shape Matching Using Laplacian Eigenfunctions and Unsupervised Point Registration by Diana mateus et al.
 *  
 *  It takes as input :
 *      - Meshes or Voxels.
 *      - A file describing the connectivity of each vertex to its neighbors
 *      - A set of parameters that are described in the help command "embedmat -h".
 * As an output, the code provides a file containing the coordinates of all vertices in the embeding space.
 */

#include <getopt.h>
#include <iostream>
#include <stdio.h>
#include <qapplication.h>
#include <ext/hash_map>

#include "CommandOpt.h"
using namespace std;

#ifndef QT_VERSION
#include <qglobal.h>
#endif

#include "legal.h"

#include "voxel.h"
#include "mesh.h"
#include "visual_sequence.h"
#include "sequence.h"
#include "embed.h"


#include "constantReader.h"
#include "variant.h" 

hash_map < std::string , Variant, hash_func > hash_map_global_params ;


void printHelp()
{
  fprintf(stderr, "\nUsage : embedmat \n"
	  "\t [-a] embedding mode 0:NJW 1: SHIMEILA 2:EIGENMAPS 3:ADJACENCY 4:SIMPLE_EMAP 5: DIFFUSION_DISTANCE (DEFAULT: 2 )\n"
	  	  "\t [-c] nei_basename (connectivity) (ONLY REQUIRED IF BUILDING THE EMBEDING FOR VOXELS.)\n"
	  "\t [-d] dimension of the embedding space (DEFAULT: 10 ) \n"
	  "\t [-e] embed_basename (save the result) (REQUIRED) \n"
	  "\t [-f] embed method (embedding method) (DEFAULT: LAP) \n"
	  "\t [-g] graphics on (DEFAULT: NO )\n"
	  "\t [-h] Help. Display this. \n"
	  "\t [-i]  Window title (DEFAULT: Application name ) \n"
	  "\t [-j] Remove first eigen vector when performing SVD. No arguments required.  (DEFAULT: true ) \n"
	  "\t [-k] k or depth according to mode (DEFAULT: 3 )\n"
	  "\t [-l] labels_for synthetic data (NOT REQUIRED)\n"
	  "\t [-m] distance mode  "
	  	  	  "2:LOCAL_GEO "
	  	  	  "4:FULL_GEO "
	  
	  "(DEFAULT : LOCAL_GEO)\n"
	  "\t [-n] n_frames (DEFAULT : 1) \n"
	  "\t [-o] embed on (1) or off (0). Computes or display the allready computed embeddings. (DEFAULT: 0)\n"
	  "\t [-p] Voxel (or mesh) file pattern. Can be %%s(.off)... (REQUIRED)\n"
	  "\t [-r] voxel mode (0: list of points 1: list of points and function values 2: voxelgrid (DEFAULT: 0) \n"
	  "\t [-s] start frame (DEFAULT: 0)\n"
	  "\t [-t] sigma parameter for the laplacian. Forces the sigma to be the same for all frames (if not set, it is computed automatically).\n"
	  "\t [-u] normalization format for eigenvectors ( 0: none, 1: (sqrt(lambda))^(-1), 2:sqrt(lambda), 3:lambda) \n"
	  "\t [-w] mesh_basename (REQUIRED) \n"
	  "\t [-x] voxel_basename  (REQUIRED)\n"
	  "\t [-y] force to use the \"r\"-ring as the distance (0: OFF, 1: ON). (DEFAULT: OFF )\n"
	  "\t [--graph-distance] Distance type between graph nodes (EUCLIDIAN:0, NORMALIZED_EUCLIDIAN:1, COTAN_WEIGHT:2, NORMALIZED_COTAN_WEIGHT:3)\n"
	  
	  	  "\t [--edge] or [-z] edge type (0:TOPO, 1:GAUSSIAN or 2:NOTHING) (DEFAULT: 1)\n"
	  "\t [--use-depth] or [-y] force to use the \"r\"-ring as the distance (0: OFF, 1: ON). (DEFAULT: OFF )\n"
	  );
  exit(-1);
}


int main(int argc, char **argv)
{





  PRINT_COLOR(printIntroLegal(), BLACK) ;

  // Read command line arguments.
  int count = argc;
  if (count == 1) {
    printHelp();
    exit(-1);
  }


  readConstantFileSettings ("CONSTANT.xml") ;

  GRAPH_DISTANCE_TYPE graph_distance_type = EUCLIDIAN ;

  int n_frames = 1;
  char *voxel_basename = NULL;
  VOXEL_TYPE voxel_type = VOXEL;
  char *mesh_basename = NULL;
      char *lbl_basename = NULL;
  char *nei_basename = NULL;
  char *embed_basename = NULL;
  char *win_title = NULL;
  char *pattern = NULL;
  int start = -1;

  //Turn on/off the embedding (read or calculate)
  int embed_on = 0;

  //Turn on/off the graphics interface
  int graphics_on = 0;

  //embedding parameters
  int embed_method = -1;
    
  int dist_mode = -1;
  int lap_mode = -1;
  int k = -1;
  FL epsilon = 0.0;
  int n_eigenfunc = 0;
  FL sigma = 0.0;
  int opt;
  int nbOpt = 0;
  int edges = -1;
  int use_depth_as_dist = -1;
  bool remove_first = false;


  bool cut_disc_comps = true ;
  FL thres_embed_small_norms = -1 ;

  NORMALIZATION_EMBEDDING normalize_embed = NO_NORM_EMBED ;

  int normalization_format = 0;
  struct option long_options[] = {
    {"graph-distance", 1, 0, GRAPH_DISTANCE},
            {"edge", 1, 0, EDGE_TYPE},
    {"use-depth", 1, 0, USE_DEPTH_AS_DIST},
    {"cut-disc-comp", 1, 0, CUT_DISC_COMP},
    {"normalize-embed", 1, 0, NORMALIZE_EMBED},
    {"cut-small-norm", 1, 0, CUT_SMALL_NORM},
    {"adjency-distance-mode", 1, 0, ADJENCY_MATRIX_TYPE},
    {"embed-method", 1, 0, EMBED_METHOD},
    {"laplacian-type", 1, 0, LAPLACIAN_TYPE},
    {"title", 1, 0, WIN_TITLE},
    {0, 0, 0, 0}
  } ;
  int option_index = 0;

  do {
    opt =
      getopt_long(argc, argv, "a:b:c:d:e:f:g:hi:j:k:l:m:n:o:p:r:s:t:u:w:x:y:z:v",
		  long_options, &option_index);
    if (opt != -1) {
      nbOpt++;
      switch (opt) {

      case LAPLACIAN_TYPE :
      case 'a':
	lap_mode = atoi(optarg);
	break;

      case 'b':
	epsilon = atof(optarg);
	break;

      case 'c':
	nei_basename = strdup(optarg);
	break;

      case 'd':
	n_eigenfunc = atoi(optarg);
	break;

      case 'e':
	embed_basename = strdup(optarg);
	break;

      case EMBED_METHOD:
      case 'f':
	embed_method = atoi(optarg);
	break;
      case 'g':
	graphics_on = atoi(optarg);
	break;
      case 'h':
	printHelp();
	break;

      case WIN_TITLE:
      case 'i':
	win_title = strdup(optarg);
	break;

      case REMOVE_FIRST :
      case 'j':
	if (!atoi(optarg)) 
	  remove_first = true;
	else 
	  remove_first = false;
	break;

      case 'k':
	k = atoi(optarg);
	break;

      case 'l':
	lbl_basename = strdup(optarg);
	break;

      case ADJENCY_MATRIX_TYPE:
      case 'm':
	dist_mode = atoi(optarg);
	break;

      case 'n':
	n_frames = atoi(optarg);
	break;

      case 'o':
	embed_on = atoi(optarg);
	break;

      case 'p':
	pattern = strdup(optarg);
	break;

      case 'r':
	voxel_type = (VOXEL_TYPE) atoi(optarg);
	break;

      case 's':
	start = atoi(optarg);
	break;

      case 't':
	sigma = atof(optarg);
	break;

      case 'u':
	normalization_format = atoi(optarg);
	break;

      case 'w':
	mesh_basename = strdup(optarg);
	break;

      case 'x':
	voxel_basename = strdup(optarg);
	break;

      case 'y':
	use_depth_as_dist = atoi(optarg);
	break;

      case USE_DEPTH_AS_DIST:
	use_depth_as_dist = atoi(optarg);
	break;

      case EDGE_TYPE:
      case 'z':
	edges = atoi(optarg);
	break;

      case GRAPH_DISTANCE:
	graph_distance_type= (GRAPH_DISTANCE_TYPE) atoi(optarg);
	break ;

	
	
      case CUT_DISC_COMP:
	if (atoi(optarg))
	  cut_disc_comps = true ;
	else 
	  cut_disc_comps = false ;
	break;
      case NORMALIZE_EMBED:
	normalize_embed = (NORMALIZATION_EMBEDDING) atoi (optarg) ;
	break;
      case CUT_SMALL_NORM:
	thres_embed_small_norms = (FL)atof(optarg) ;
	break ;
      default:
	PRINT_COLOR(std::cout << "Unknown option : " << optarg <<std::endl, RED) ;
	printHelp();

      }
    }
  } while (opt != -1);


  if (graphics_on)
    PRINT_COLOR_(cout << "Type H to display help" << endl, GREEN);

  if (n_frames == 0) {
    WARNING_(fprintf
	     (stderr,
	      "n in the command line should be set to at list 1, default is 1 \n"));
    n_frames = 1 ;
  }

  if ( !remove_first ) {
    PRINT_COLOR_(fprintf
		 (stderr,
		  "By default, the 0th eigenvector is not removed. To remove it from storage, use [-j] option\n"),
		 BLUE);
  }

  if (mesh_basename == NULL && 
      voxel_basename == NULL 
                  ) {
    ERROR_(fprintf
	   (stderr,
	    "Options -x (for voxels) or -w (for meshes)   must be set \n"),
	   ERROR_BAD_OPTION)
      }

  if (embed_method == -1) {
    WARNING_(fprintf
	     (stderr,
	      "The mode to compute the embeddings is set by default to LAPLACIAN. Use [-f] to set it.\n"));
    embed_method = LAP ;
  }

  if (lap_mode == -1) {
    WARNING_(fprintf
	     (stderr,
	      "The mode to compute the laplacian is set by default to EIGENMAPS. Use [-a] to set it.\n"));
    lap_mode = EIGENMAPS;
  }

  if (k == -1) {
    WARNING_(fprintf
	     (stderr,
	      "The neighborhood depth should be set in the command line with option [-k]. By default, it is 1. \n"));
    k = 1;
  }
  if (n_eigenfunc == 0) {
    WARNING_(fprintf
	     (stderr,
	      "The number of eigenfunctions to compute should be set in the command line using [-d] option. By default, it is 10"));
    n_eigenfunc = 10;
  }
  if (embed_basename == NULL) {
    ERROR_(fprintf
	   (stderr,
	    "A name for embedding files (without the extension .emb) must be provided with option -e.\n"),
	   ERROR_BAD_OPTION);
  }
  if (edges == -1) {
    WARNING_(fprintf
	     (stderr,
	      "The method to compute the edges of the graph should be provided in the command line using -z option. By default, it is set the GAUSSIAN method\n"));
    edges = GAUSSIAN;
  }

  if (mesh_basename && use_depth_as_dist == -1) {
    WARNING_(fprintf
	     (stderr,
	      "The method to compute the distances between the vertices of the mesh should be set using -y option. By default it is set to euclidian distance. The other possibility is to use the ring number as a distance (-y 1) \n"));
    use_depth_as_dist = 0;
    graph_distance_type = (GRAPH_DISTANCE_TYPE)0 ;
  }

  if (voxel_basename && !nei_basename) {
    ERROR_(fprintf
	   (stderr,
	    "A name pattern for connectivity file must be provided with option -c.\n"),
	   ERROR_BAD_OPTION);
  }

  if (dist_mode == -1) {
    WARNING_(fprintf
	     (stderr,
	      "The method to compute the distance between the vertices in a neighborhood should be provided in the command line using -m option. By default, it is set the LOCAL GEODESIC method\n"));
    dist_mode = LOCAL_GEO;
  }


  if (pattern == NULL) {
    ERROR_(fprintf
	   (stderr,
	    "A name pattern for voxel or mesh files must be provided with option -p. Generally, %%s(.off, for meshes) should be sufficient. For a sequence of files, %%s%%d(.off) might be a pattern (%%d is replaced by start ID, and latter filled for subsequent IDs of the sequence).\n"),
	   ERROR_BAD_OPTION);
  }
  if (pattern && start == -1) {
    WARNING_(fprintf
	     (stderr,
	      "The ID of the voxel or meshset is not provided, it should be with option -s. By default, it is 0. \n"));
    start = 0;
  }


  if (embed_method == (int) ISOMAP) {
    WARNING_(fprintf(stderr,"Embed mode is set to ISOMAP. We force the adjency matrix to be set to FULL_GEO \n")) ;
    dist_mode = (int)FULL_GEO ;
  }

  // If we use a normalised distance between nodes, the normalisation should be 
  // done in the Generalized Eigenfunction Decompisition framework (Laplace-Beltrami 
  // Eigenfunctions for Deformation Invariant Shape Representation Raif M. Rustamov, SGP 07).
  // In case of COTAN_WEIGHT being used, no GAUSSIAN weightening is required.
  if ( use_depth_as_dist ==0 && graph_distance_type != EUCLIDIAN ) {
    PRINT_COLOR_(std::cout << "In case of cotan-weights, or normalized distances the exponential weightening is not required"<<std::endl, BLUE) ;
    PRINT_COLOR_(std::cout << "In case of cotan-weights, or normalized distances setting the Eigenfunction decomposition so that Wv=lDv."<<std::endl, BLUE) ;
    //	edges = NONE ;
    //	lap_mode = SHIMEILA ;
  }

    
  if (graphics_on == 1) {
    QApplication application(argc, argv);

#ifndef MACOSX
    glutInit(&argc, argv);
#endif

    VisualSequence visual_seq;
    char title[100];
    if (voxel_basename)
      sprintf(title, "Embedding of voxels");
    else if (mesh_basename)
      sprintf(title, "Embedding of meshes");
    else if (embed_basename) {
      sprintf(title, "Display of embeddings");
    } 
    
    
    else {
      fprintf(stderr,
	      "Options [-x], [-w]   are required. It sets the input vertices filename (do ./embedmat -h for more info).");
	    
    }
    if (win_title) {
      sprintf(title, "Embedding of %s", win_title);
      std::cout << title <<std::endl ;
    }
#if QT_VERSION < 0x040000
    visual_seq.setCaption(title);
#else
    std::cout << "setting win title for QT4" << std::endl ;
    visual_seq.setWindowTitle(title);
#endif

    if (embed_on) {
      PRINT_COLOR_(fprintf
		   (stderr,
		    "The soft will compute the embeddings \n"), RED);
      visual_seq.setEmbeddingOn(true);
    } else {
      PRINT_COLOR_(fprintf
		   (stderr,
		    "The soft WON'T compute the embeddings, but display them \n"),
		   RED);
      visual_seq.setEmbeddingOn(false);
    }


    PRINT_(fprintf(stderr, "Loading all parameters\n"));

    // Loading the parameters to read the voxels or meshes and their connectivity.
    if (voxel_basename) {
      PRINT_(std::cout << "Loading a voxel set" << std::endl);
      visual_seq.loadVoxelParams(voxel_basename, pattern, n_frames,
				 start, nei_basename, embed_basename,
				 lbl_basename, voxel_type);
    } else if (mesh_basename) {
      PRINT_(std::cout << "Loading a mesh" << std::endl);
      visual_seq.loadMeshParams(mesh_basename, pattern, n_frames,
				start, embed_basename);
    }
        	
    // Loading the parameters to create or read the embedings.
    visual_seq.loadEmbeddingParams(embed_method,lap_mode, dist_mode, n_eigenfunc, k,
				   epsilon, sigma, edges,
				   use_depth_as_dist, (GRAPH_DISTANCE_TYPE) graph_distance_type,
				   (bool) remove_first,
				   (NORMALIZATION_FORMAT)
				   normalization_format, cut_disc_comps, normalize_embed, thres_embed_small_norms);


#if QT_VERSION < 0x040000
    application.setMainWidget(&visual_seq);
#else
    visual_seq.setWindowTitle(win_title);
#endif
    // Display the main window.
    visual_seq.show();

    // Execute the main loop.
    application.exec();

    // When main loop exited, release all data.
    visual_seq.visualRelease();
  } else {
    // Loading the parameters to read the voxels or meshes and their connectivity.
    Sequence seq;

    if (!embed_on) {
      FATAL_(fprintf
	     (stderr,
	      "Graphical option should be set to display embeddings. Set -g 1 to display embeddings or -o 1 to compute embeddings \n"),
	     FATAL_BAD_OPTION);
    }

    DBG_(fprintf(stderr, "Loading all parameters\n"));
    if (voxel_basename) {
      PRINT_(std::cout << "Loading a voxel set" << std::endl);
      seq.loadVoxelParams(voxel_basename, pattern, n_frames, start,
			  nei_basename, embed_basename, lbl_basename,
			  voxel_type);
    } else if (mesh_basename) {
      PRINT_(std::cout << "Loading a mesh" << std::endl);

      seq.loadMeshParams(mesh_basename, pattern, n_frames, start,
			 embed_basename);
    }
                
    // Loading the parameters to create the embedings.
    PRINT_(fprintf(stderr, "Load Embedding parameters \n"));

    seq.loadEmbeddingParams(embed_method,lap_mode, dist_mode, n_eigenfunc, k,
			    epsilon, sigma, edges, use_depth_as_dist, (GRAPH_DISTANCE_TYPE) graph_distance_type,
			    (bool) remove_first, (NORMALIZATION_FORMAT)
			    normalization_format, cut_disc_comps, normalize_embed);

    // Execute the main loop. Computes the embeddings.
    seq.go();

    seq.release();
  }



  if (win_title)
    free(win_title);
  if (nei_basename)
    free(nei_basename);
  if (embed_basename)
    free(embed_basename);
  if (lbl_basename)
    free(lbl_basename);
  if (pattern)
    free(pattern);
  if (mesh_basename)
    free(mesh_basename);
  if (voxel_basename)
    free(voxel_basename);
      PRINT_COLOR_(fprintf(stderr, "Quitting \n"), RED);

  exit(0);
}

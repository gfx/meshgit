/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
/*
 *  visual_shape.h
 *  visu
 *
 *  Created by Diana Mateus and David Knossow on 10/6/07.
 *  Modified by Avinash Sharma on 11/12/08.
 *  Copyright 2007 INRIA. All rights reserved.
 *
 * This class provides the Graphical interface to visualize
 * the matching computed from match code.
 *
 * It reads files as Voxels, Meshes and Embeddings.
 * It loads the matching output
 * It displays the matching either with colors or 
 * with lines that link matched points.
 * 
*/

#ifndef VISUAL_SHAPE_H
#define VISUAL_SHAPE_H


#include <QGLViewer/qglviewer.h>
#include <qmessagebox.h>
#include "Constants.h"
#include "EulerAngles.h"
#include "shape.h"


class VisualShape:public QGLViewer, public Shape {

  protected:
  virtual void draw();
  virtual void init();
  virtual void animate();
  virtual void keyPressEvent(QKeyEvent * e);  
  
    private:
  
  //limits of part to be shown:
  //load with  set_MinMaxValuesForDisplay
  double x_match_min;
    double x_match_max;
    double y_match_min;
    double y_match_max;
    double z_match_min;
    double z_match_max;

    //matches       
    //show matches or not 
    bool show_match;

    
    //put color of set 1 on line matches
    bool color_match1;

    //put color of set 2 on line matches
    bool color_match2;

    //animation of matches: display "set_size" by "set_size" matches starting from "set_first"
    int set_first;		//index of first match to be shown (set_first = 0 ; set_first += set_size.
    int set_size;		//size of the set to be shown 


    // Number of outliers in the shapes (points that have been matched)
    int outliers;

    // Is animation on or of
    bool animate_on;

    // Store dimensions to be displayed
    int seld[3];

    // Currrent selected dimension to be displayed.
    int current_sel_dim;

    // Copy colors of the match colors on the mesh.
    float *copyColorFromMatch(int n1, int n2, float *color1, float *color2,
			      int col_depth, int copy_from);

    // Visual sampling of the line matches.
    int vizu_sampling ;

  public: 
    VisualShape();
    ~VisualShape();

    // type of data to show (refer to types.h)
    DISPMATCH dispmode;

    // Start/Stop annimation (while pressing Space bar)
    void toggleAnimation();
    //Load next set of matches.
    int next();
    //Load previous set of matches.
    int previous();


    // display voxels
    GLuint BIN_SHAPE_MATCHLIST;
    GLuint PROBA_SHAPE_MATCHLIST;
    
    //display embeddings

    //Store list of embeddings to be displayed. Each list is a list of triplet of points. 
    GLuint *trio_list_1;
    GLuint *trio_list_2;
    //used to build the lists for all available dimensions of the embeddings
    GLuint *BIN_EMBED_MATCHLIST;
    GLuint *PROBA_EMBED_MATCHLIST;

    //number of lists and current list
    int embed3d_nlist, embed3d_id;

    //separation between the two models in the opengl context
    float model_sep[3];

    //rotation between the two models in the opengl context
    float model_rot[3];

    // Load Color Maps and build the lists of data to be displayed.
    void initColorsAndShapes();

    // reads the matches and transfo files, each time is called.
    void refreshAllInputs();

    // Shows matches in groups of set_size starting at set_first
    //calls build_binary_shape_match
    int animateMatch();

    
    //OpenGL code to display the matches (as lines) on the 3D shapes.
    int buildBinaryShapeMatch(FL * v1, FL * v2, float *color1,
			      float *color2, int dim1, int dim2, double s1,
			      double s2, int col_depth, int sample_rate,
			      bool Bbox);

    //OpenGL code to display the matches (as lines) on the 3D shapes. Calls
    // buildBinaryShapeMatch and set the code as a list.
    int buildBinaryShapeMatchList(FL * v1, FL * v2, float *color1,
				  float *color2, int dim1, int dim2,
				  double s1, double s2, int col_depth,
				  int sample_rate, bool Bbox);
    int buildProbaShapeMatchList(FL * v1, FL * v2, float *color1,
				 float *color2, int dim1, int dim2,
				 int col_depth, int sample_rate,
				 int max_matches_per_el);


         
    //OpenGL code to display the matches (as lines) on the mesh.
    //Calls buildProbaShapeMatchList.
    int buildBinaryMeshMatchList(int sample_rate = 1);
    int buildProbaMeshMatchList(int sample_rate =
				hash_map_global_params["vizu_sample"].Int(), int max_matches_per_el =
				hash_map_global_params["n_display_match"].Int());
    
    //OpenGL code to display the matches (as lines) on the Graph.
    //Calls buildProbaShapeMatchList.
    int buildBinaryGraphMatchList(int p_sample_rate) ;
    
    //OpenGL code to display the matches (as lines) on the voxel set.
    //Calls buildProbaShapeMatchList.
    int buildBinaryVoxelMatchList(int sample_rate = 1);
    int buildProbaVoxelMatchList(int sample_rate =
				 hash_map_global_params["vizu_sample"].Int(), int max_matches_per_el =
				 hash_map_global_params["n_display_match"].Int());



    //OpenGL code to display the matches (as lines) on the voxel set. 
    //Does not call the generic code.
    int buildBinaryEmbedMatchList(int sample_rate = hash_map_global_params["vizu_sample"].Int());
    int buildProbaEmbedMatchList(int sample_rate =
				 hash_map_global_params["vizu_sample"].Int(), int max_matches_per_el =
				 hash_map_global_params["n_display_match"].Int());
    
    
    // Reads consecutive match files (e.g. temporal evolution of matches for a given pair of embeddings).
    int updateMatchFiles(int i);


    //display all created lists
    int drawBinShapeMatchList();
    int drawBinEmbedMatchList();
    int drawProbaShapeMatchList();
    int drawProbaEmbedMatchList();

    
    // draw embeddings after applying InitT and T.    
    int buildTransfoCoordsLists();
    int drawTransfoCoordsList(int which, int list = 0);

    // Bounding Box to be set to display only part of the matches (One Body part for instance).
    int setMinMaxValuesForDisplay(double xmin, double xmax, double ymin,
				  double ymax, double zmin, double zmax);

    // Set the translation and Rotation factors between the two shapes (only for dislpay purposes)
    void setModelSep (float x, float y, float z) ;
    void setModelRot (float x, float y, float z) ;

    // Sets the visual sampling of line matches...
    void setVizuSampleRate (int p_sampling_rate) ;

    //Draw the canonical frame attached to the 3D shape.
    void drawReferenceFrame (float r, float g, float b) ;


    


    //Enable or Disable the file saving.
    bool save_on ;



    };
#endif

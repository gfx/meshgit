/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
#ifndef _DISPHISTOGRAMS_H_
#define _DISPHISTOGRAMS_H_
/*
 *  DispHistograms.Qt4.cpp
 *  Common to multiple programs
 *
 *  Created by Diana Mateus and David Knossow on 20/1/08.
 *  Copyright 2008 INRIA. All rights reserved.
 *
 */

/*
 * This code is based on a sample provided with Qwt library. 
 * It is dedicated to display histograms. 
 * In practice, it displays the histograms of the Eigenfunctions 
 * computed in embed.cpp.
 *
 */


#include <iostream>

#include "histogram_item.h"
#include "ui_DispHistograms.Qt4.h"


#include <QWidget>
#include <QPen>
#include <qwt_plot.h>
#include <qwt_plot_grid.h>
#include <qwt_plot_marker.h>
#include <qwt_interval_data.h>


#include "types.h"
#include "utilities.h"


class HistoWidget:public QWidget, public Ui::HistoWidgetForm {
  Q_OBJECT public:
    HistoWidget();
    ~HistoWidget();


    // Stores the histograms values
    FL *histo_data;

    // Number of histograms to display.
    int num_hist;

    // Organization of the widget. Number of rows.
    int rows;
    // Organization of the widget. Number of columns.
    int cols;

    // Point to graphical container of the histogram
    QwtPlot **plot;

    // Point to graphical container of the grid of histograms
    QwtPlotGrid *grid;

    // Point to the data to be displayed as histogram
    HistogramItem *histograms;

    // Size of each bar of the histogram.
    QwtArray < QwtDoubleInterval > *intervals;

    // Data to be displayed in histograms (HistogramItem)
    QwtArray < double >*values;

    // Release all allocated data.
    void releaseAll();

    // Set the background color.
    void setBackGroundColors(QColor * color);

    // Fills interval and values from histodata.
    int setDataToDisplay(int N, int Nx, int Ny, FL * data, int *Histsizes);


    public slots:
	// Drawing events
    void showEvent(QShowEvent * e);
    void closeEvent(QCloseEvent * e);
    void resizeEvent(QResizeEvent * e);
    void update();



};
#endif

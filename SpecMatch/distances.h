/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
#ifndef DISTANCES_H
#define DISTANCES_H
/*
 *  distances.h
 *  embedmat
 *
 *  Created by Diana Mateus and David Knossow on 11/14/07.
 *  Copyright 2007 INRIA All rights reserved.
 *
 * This class provides functions to compute the distance 
 * between nodes in the graph (built out of the voxels).
 * Out of that distance matrix, the laplacian is computed by the embed class.
 * Different methods are implemented to compute the distances.
 * In practice, we tend to use a sparse distance computation using the LOCAL_GEO option. 
 * Refer to below comments for the implemented methods.
 *
 */

#include "neighbors.h"
#include "utilities.h"



class Distances {
  private:

    // Provides functionalities to compute the connectivity of a graph.
    Neighborhood * C_Nei;

    // Connectivity file name provided as an option in the command line. 
    // If not provided, find_neighbors() will try to discover the connectivity automaticaly.
    char *nei_filename;		//[1024];

    // Status. Is the distance matrix either is computed (or read from a file) or not yet. 
    bool loaded;

    // The weight matrix might allocated in an other class (Neighborhood) and then should not be deleted 
    // in the current class. This flags prevents from deallocation here.
    bool modify_W;

    // It is a copy of data (voxels) loaded from outside the class. Filled in load_data function.
    // This array is used if no connectivity file is provided (in all modes).
    FL *X;

    // Generally equal to 3. It is the dimension in which X is represented. 
    int dimx;



    //Number of nodes of the graph. Size of square matrix (nxn)
    int n_nodes;

    //Counter of nonzero elements for sparse matrices
    int nzelems;

        //vectors
    FULL_MAT_TYPE *full_W;			//Connectivity (weights for LLE) (original distance for Laplacian) 
    

    // Store the weights in a sparse manner (i,j,value).
    FL *sparse_W;

    //matrices pointing to memory vectors fullW and sparseW
    FL **W;
        FULL_MAT_TYPE **FW;
    
    //parameters
    int K;			//max number of neighbors (0 if not taken into account). Used in SPARSE mode.
    FL epsilon;			//max square distance from the initial point (0 if not taken into account). Used in SPARSE mode.
    NEIGHBORHOOD nei_mode;	//sparse or full matrices (LLE sparse) (ISOMAP full) (LAPLACIAN both)
    int depth;			//for geodesic distances. it corresponds to the r-ring distance.

    //internal functions called by fill_neighborhood_matrix according to the parameters

    
    
        int computeFullGeodesicDistances();
    int propagate(FULL_MAT_TYPE* _Wirow,int row,int count_last_added, FULL_MAT_TYPE* values1, FULL_MAT_TYPE* values2, int* nei_n, FL** nei_mat);
    
    int computeLocalGeodesicDistances();

        

    //if no file specified for local_geodesic_distance try to establish connectivity
    //from the data itself assuming integer voxel coordinates
    void findNeighbors(int *&num_nei, int *&neighborhood);
    int search(int *v);

  public:

     Distances();
    ~Distances();

    // Load the voxels.
    int loadData(FL * data, int in_n, int in_dimx);

    // Fill the connectivity, or distance matrix, with respect to the required method. Calls sub functions.
    int fillNeighborhoodMatrix();
	
        //access for meshes
    int computeFullGeodesicDistances(FL* p_sparse_W,int p_n_elems, int p_nzelems);
        
    
    // Set the parameters to compute the distance matrix.
    int setParameters(NEIGHBORHOOD mode, int in_K, FL in_eps,
		      int in_depth);
    
    // Obvious
    int setNeiFilename(char *filename);
    
    // Compute W[i][j]^2
    int squareDistMat();
    
    // Is distance matrix computed ?
    bool isLoaded();
    
    // Is distance matrix stored in a sparse manner ?
    bool isSparse();

    // Number of nodes in the graph.
    int getNumOfNodes();

    // If the matrix is sparse, number of elements in the sparse matrix.
    int getNzelems();

    // Returns the "K"-nearest neighbors or the maximum number of neighbors taken in the epsilon ball.
    int getK();
    // Returns the radius of the ball used for computing the sparse distance matrix without using the connectivity.
    FL getEpsilon();
    // Returns the "r"-ring.
    int getDepth();

    // Returns the distance matrix as a form of matrix
    FL *getDistMatrix();

        // Returns the Fulldistance matrix as a form of a vector
    FULL_MAT_TYPE *getFullDistVector();
        
    // Returns the distance matrix as a form of a vector
    FL *getSparseDistVector();
    
        // Gives access to loaded and centered data
    FL* getData();
    int getDimData();
    

    // Get the mode of distance computation
    NEIGHBORHOOD getNeiMode();
    
    //Load a sparse matrix from external data
    int loadSparseDistance(FL* p_sparse_w, int p_n_elem, int p_nz_elems) ;
    
    
};
#endif

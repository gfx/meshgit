/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
/*
 *  main-match.cpp
 *  
 *
 *  Created by Diana Mateus and David Knossow on 11/5/07.
 *  Copyright 2007 INRIA. All rights reserved.
 *
 */

#include <getopt.h>
#include <stdio.h>
#include <qapplication.h>
#ifdef MACOSX
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#ifndef QT_VERSION
#include <qglobal.h>
#endif

#include "legal.h"

#include "CommandOpt.h"
#include "mesh.h"
#include "voxel.h"
#include "embedding.h"

#include "match_em.h"

#include "visual_shape.h"
#include "visual_em.h"
#include "constantReader.h"
hash_map < std::string , Variant, hash_func > hash_map_global_params ;

#define EM_M ISO_ANNEAL
#define INIT_M UNIFORM


using namespace std;

void printUsage()
{

    fprintf(stderr, 
	    "\nUsage : match \n"
	    "\t [--v1] voxelset1 \n\t [--v2] voxelset2 \n"
	    "\t [--e1] embedfile1 \n\t [--e2] embedfile2 \n"
	    "\t [--f1] vox_and_embed file1 \n\t [--f2] vox_and_embed file2 \n"
	    "\t [--c1] connectivity file 1 \n\t [--c2] connectivity file 2 \n"
	    "\t [--dim] embedding dim \n"
	    "\t [--remove-first] remove first dimension of embeddings \n"
	    "\t [--embed] estimate voxelset embedding\n"
	    "\t [--em] EM_MODE corresponding to EM evolution\n"
	    "\t [--init] INIT_MODE: integer corresponding to enum\n"
	    "\t [--sigma] Value of initial covariance sphere \n"	    
	    "\t [--kout] Value of outlier constant \n"
	    "\t [--transfo] TRANSFO_MODE: enum of possible transformation modes\n"
	    "\t [--bins] Number of histogram bins\n"
	    "\t [--ratio] discriminant ratio for dimension selection (-1 off)\n"
	    "\t [--title] Title of the window"
	    "\t [--sample] sample alpha ij lines (select 1 over sample lines from the alpha matrix)\n"
	    "\t [--evaluate] Enable the performance evaluation of the matching. \n"
	    "\t [-h] Print this \n"
	    "\t [-d] Display or not the matching process \n"
	    "\t [-b] binary match file \n"
	    "\t [-p] proba match file\n"
	    "\t [-o] template file name for all temporary outputs\n"
	    "\t [-t] filename of initial transformation\n"
	    "\t [-l] labels_for synthetic data (two files)\n"
	    "\t [-x] [-y] [-z] set the translation of a shape, for display purpose\n");
    exit(-1);
}




int main(int argc, char **argv)
{

    // Read command lines arguments.
    QApplication application(argc, argv);

    PRINT_COLOR(printIntroLegal(), BLACK) ;

    readConstantFileSettings ("CONSTANT.xml") ;
 



#ifndef MACOSX
    glutInit(&argc, argv);
#endif

    // Instantiate the viewer.
    int disp_on = 0;
    int emb_dim = 3;
    int count = argc;
    char *embed1 = NULL;
    char *embed2 = NULL;
    char *voxel1 = NULL;
    char *voxel2 = NULL;
    char *voxembed1 = NULL;
    char *voxembed2 = NULL;
    char *connect1 = NULL;
    char *connect2 = NULL;
    char *bin_match = NULL;
    char *proba_match = NULL;
    char *template_name = NULL;
    char *transfo_file = NULL;
    char *labels1 = NULL;
    char *labels2 = NULL;
    char *win_title = NULL;
        bool remove_first = false;
    bool eval_matching = false ;

    int num_of_bins = 0 ;
    HASH_MAP_PARAM_(int, "num_of_bins", Int, num_of_bins) ;


    FL discriminant_ratio = -1;

    NORMALIZATION_EMBEDDING normalize_embed = NO_NORM_EMBED ;

    double xmin = -1, xmax = 1, ymin = -1, ymax = 1, zmin = -1, zmax = 1;

    FL sigma = 0.0;
    FL kout = 0.0;
    int line_sample = 1;
    EM_MODE em_mode = EM_M;	//load with defaults
    INIT_MODE init_mode = INIT_M;	//load with defaults
    TRANSFO_MODE transfo_mode = ORTH_AND_TRANS;	//default

    HISTOGRAM_DISTANCE hist_dist = BINTOBIN;
    ASSIGNMENT_METHOD ass_meth = SUBOPTIMAL1;

    
    float trans_x = 0.5 ;
    float trans_y = 0.5 ;
    float trans_z = 0.5 ;

    // In case of a huge dataset (more the 8000 vertices/voxels/nodes), we switch to
    // a mode of computation that is slower but requires less memory.
    bool use_alpha_line = false ;


    if (count == 1) {
	WARNING_(fprintf(stderr, "The program needs some arguments \n") );
	printUsage();
    }

    int opt = 0;
    int nbOpt = 0;
    struct option long_options[] = {
	/* Option long name, Has an argument or not , returns a value or not,
	   if not returns the id (here 260, must be different for each long name
	   and different than ASCII code corresponding to single characters). */
	{"e1", 1, 0, EMBED1},
	{"e2", 1, 0, EMBED2},
	{"v1", 1, 0, VOXEL1},
	{"v2", 1, 0, VOXEL2},
	{"f1", 1, 0, VOXEMBED1},
	{"f2", 1, 0, VOXEMBED2},
	{"remove-first", 1, 0, REMOVE_FIRST},
	{"normalize-embed", 1, 0, NORMALIZE_EMBED},
	{"c1", 1, 0, CONNECT1},
	{"c2", 1, 0, CONNECT2},
	{"dim", 1, 0, DIM},
	{"xmin", 1, 0, XMIN},
	{"xmax", 1, 0, XMAX},
	{"ymin", 1, 0, YMIN},
	{"ymax", 1, 0, YMAX},
	{"zmin", 1, 0, ZMIN},
	{"zmax", 1, 0, ZMAX},
	{"em", 1, 0, EM},
	{"init", 1, 0, INIT},
	{"sigma", 1, 0, SIGMA},
	{"kout", 1, 0, OUTLIER_K},
	{"transfo", 1, 0, T_MODE},
	{"histDist", 1, 0, HIST_DIST},
	{"AssignMeth", 1, 0, ASSIGN},
	{"bins", 1, 0, BINS},
	{"ratio", 1, 0, RATIO},
	{"sample", 1, 0, LINE_SAMPLE},
	{"title", 1, 0, WIN_TITLE},
	{"evaluate", 1, 0, EVALUATE},
	
		{0, 0, 0, 0}
    };
    int option_index = 0;
    do {
	opt = getopt_long(argc, argv, "b:d:hl:o:p:t:x:y:z:",
			  long_options, &option_index);
	if (opt != -1) {
	    nbOpt++;
	    switch (opt) {
	    case EMBED1:
		embed1 = strdup(optarg);
		break;		// e1
	    case EMBED2:
		embed2 = strdup(optarg);
		break;		// e2                  

	    case VOXEL1:
		voxel1 = strdup(optarg);
		break;		// v1
	    case VOXEL2:
		voxel2 = strdup(optarg);
		break;		// v2

	    case VOXEMBED1:
		voxembed1 = strdup(optarg);
		break;		//v1 and e1
	    case VOXEMBED2:
		voxembed2 = strdup(optarg);
		break;		//v2 and e2

	    case CONNECT1:
		connect1 = strdup(optarg);
		break;
	    case CONNECT2:
		connect2 = strdup(optarg);
		break;
	    case DIM:
		emb_dim = atoi(optarg);
		break;		// dim
	    case REMOVE_FIRST:
		if (atoi(optarg)) 
		    remove_first = true;
		else 
		    remove_first = false;
		break;

	    case XMIN:
		xmin = atoi(optarg);
		break;		//xmin
	    case XMAX:
		xmax = atoi(optarg);
		break;		//xmax
	    case YMIN:
		ymin = atoi(optarg);
		break;		//ymin
	    case YMAX:
		ymax = atoi(optarg);
		break;		//ymax
	    case ZMIN:
		zmin = atoi(optarg);
		break;		//zmin
	    case ZMAX:
		zmax = atoi(optarg);
		break;		//zmax

	    case EM:
		em_mode = (EM_MODE) atoi(optarg);
		break;		// em
	    case INIT:
		init_mode = (INIT_MODE) atoi(optarg);
		break;		// init
			    case SIGMA:
	      sigma = atof(optarg);
	      break;
	    case T_MODE:
	      transfo_mode = (TRANSFO_MODE) atoi(optarg);
		break;		//transfo
	    case OUTLIER_K:
		kout = atof(optarg);
		break;
	    case LINE_SAMPLE:
		line_sample = atoi(optarg);
		break;

	    case HIST_DIST:
		hist_dist = (HISTOGRAM_DISTANCE) atoi(optarg);
		break;		// histDist
	    case ASSIGN:
		ass_meth = (ASSIGNMENT_METHOD) atoi(optarg);
		break;		// AssignMeth
	    case BINS:
		num_of_bins = atoi(optarg);
		break;
	    case RATIO:
		discriminant_ratio = atof(optarg);
		break;
	    case WIN_TITLE:
		win_title = strdup(optarg);
		break;

	    case NORMALIZE_EMBED:
		normalize_embed = (NORMALIZATION_EMBEDDING) atoi (optarg);
		break;

	    case EVALUATE:
		eval_matching = true ;
		break ;
			    case 'h':
		printUsage();
		break;
	    case 'b':
		bin_match = strdup(optarg);
		break;
	    case 'o':
		template_name = strdup(optarg);
		break;
	    case 'p':
		proba_match = strdup(optarg);
		break;
	    case 'd':
		disp_on = atoi(optarg);
		break;
	    case 'l':
		labels1 = strdup(optarg);
		break;
	    case 't':
		transfo_file = strdup(optarg);
		break;
	    case 'x':
		trans_x = atof(optarg);
		break ;
	    case 'y':
		trans_y = atof(optarg);
		break ;
	    case 'z':
		trans_z = atof(optarg);
		break ;

	    default:
		WARNING_(fprintf(stderr, "%d option is unknown \n",opt)) ;
		printUsage();

		break;
	    }
	}
    } while (opt != -1);




    if (disp_on)
	PRINT_COLOR_(cout << "Type H to display help" << endl, GREEN);

    if (remove_first) {
	PRINT_COLOR_(fprintf
		     (stderr,
		      "The remove first option is activated. Check if embeddings or loaded files match this requirement\n"),
		     BLUE);
	PRINT_COLOR_(fprintf
		     (stderr,
		      "Also, the --dim option should be \"required dim + 1\". The option remove_first does dim = dim -1. \n"),
		     BLUE);
	sleep(2);
    }



    Shape shape;
    Match match;
    VisualMatch vmatch;

    match.init() ;

    int *int_pointer = NULL;
    FL *fl_pointer = NULL;

    // READ
    int load_result = 0;
	

    if (voxel1)
	load_result += shape.C_Voxel1->readPoints(voxel1);

    if (voxel2)
	load_result += shape.C_Voxel2->readPoints(voxel2);

    if (embed1)
	load_result +=
	    shape.C_Embed1->readEmbedding(embed1, emb_dim, remove_first);

    if (embed2)
	load_result +=
	    shape.C_Embed2->readEmbedding(embed2, emb_dim, remove_first);

    if (voxembed1) {
	load_result +=
	    shape.C_Voxel1->readVoxelsAndFunctionValue(voxembed1);
	load_result +=
	    shape.C_Embed1->loadEmbedding(shape.C_Voxel1->
					  getFunctionValue(),
					  shape.C_Voxel1->getNumOfVoxels(),
					  emb_dim,
					  shape.C_Voxel1->
					  getNumOfFunctions(),
					  remove_first);
    }

    if (voxembed2) {
	load_result +=
	    shape.C_Voxel2->readVoxelsAndFunctionValue(voxembed2);
	load_result +=
	    shape.C_Embed2->loadEmbedding(shape.C_Voxel2->
					  getFunctionValue(),
					  shape.C_Voxel2->getNumOfVoxels(),
					  emb_dim,
					  shape.C_Voxel1->
					  getNumOfFunctions(),
					  remove_first);
    }

    if (bin_match)
	load_result += shape.readBinaryMatch(bin_match);

    if (proba_match)
	load_result += shape.readProbabilisticMatch(proba_match);

    if ((labels1) && (labels2)) {
	load_result += shape.C_Voxel1->readLabelValue(labels1);
	load_result += shape.C_Voxel1->readLabelValue(labels2);
    }



    int dim = 0;
    if (shape.C_Voxel1->isLoaded() && shape.C_Voxel2->isLoaded())
	dim = shape.C_Voxel1->getDim();
    if (shape.C_Embed1->isLoaded() && shape.C_Embed2->isLoaded())
	dim = shape.C_Embed1->getDim();

    if (init_mode == TRANSFO && transfo_file) {

	shape.setApplyInitTOnce(true);	// Read and Apply InitT only once.
	load_result +=
	    shape.readTransfo(transfo_file, dim, dim, remove_first);

    } else if (init_mode == TRANSFO && transfo_file == NULL) {
	FATAL_(std::
	       cout <<
	       "When initialization is set to TRANSFO, a file should be provided using -t option"
	       << std::endl, FATAL_BAD_OPTION);
    }
    if (load_result) {
	FATAL_(fprintf(stderr, "Problem while loading files \n"),
	       FATAL_OPEN_FILE);
    }
    //Load initialization files
    PRINT_COLOR_(fprintf(stderr, "MATCHING INITIALIZATION \n"), RED);
    //Pass inputs if any 




    switch (init_mode) {
    case MATCH:
	if (!bin_match) {
	    FATAL_(fprintf
		   (stderr,
		    "No binary match file was specified as input for the MATCH em initialization\n"),
		   FATAL_BAD_OPTION);
	}
	PRINT_COLOR(fprintf
		    (stderr,
		     "Using binary match %s with %d matches to initialize alpha \n",
		     bin_match, shape.getNumOfMatch()), GREEN);
	int_pointer = new int[2 * shape.getNumOfMatch()];
	shape.getMatch(&int_pointer);
	break;
    case TRANSFO:
	if (!transfo_file) {
	    FATAL_(fprintf
		   (stderr,
		    "No file was specified as input for the TRANSFO em initialization\n"),
		   FATAL_BAD_OPTION);
	}

	DBGout << "Building init_transfo with : " << dim << " dimensions"
	    << std::endl;
	fl_pointer = new FL[(dim) * (dim)];
	shape.getTransfo(&fl_pointer, remove_first, dim);

#ifdef DEBUG
	std::
	    cout << "Showing transfo in main match with dim=" << dim <<
	    std::endl;
	for (int i = 0; i < (dim); i++) {
	    for (int j = 0; j < (dim); j++) {
		std::cout << fl_pointer[i * (dim) + j] << " ";
	    }
	    std::cout << std::endl;
	}
	std::cout << std::endl;
#endif
	break;
			
    case EFUNC:
	break ;
    default:
	break;

    }
    //Match voxel sets

    DBG_(fprintf(stderr, "MATCH (load data)\n"));

    int data_size = 0 ;


    if (voxel1 && voxel2) {
	PRINT_COLOR_(fprintf(stderr, "MATCHING voxelsets\n"), GREEN);
	DBG_(fprintf(stderr, "-->loading voxels\n"));
	FL *vox1 = shape.C_Voxel1->getVoxels();
	FL *vox2 = shape.C_Voxel2->getVoxels();

	data_size = MAX(shape.C_Voxel1->getNumOfPoints() , shape.C_Voxel2->getNumOfPoints()) ;

	if (!disp_on) {
	    match.loadData(shape.C_Voxel1->getNumOfVoxels(),
			   shape.C_Voxel2->getNumOfVoxels(),
			   shape.C_Voxel1->getDim(), vox1, vox2,
			   template_name);
	} else {
	    fprintf(stderr, "VISUAL MATCH on\n");

	    vmatch.loadData(shape.C_Voxel1->getNumOfVoxels(),
			    shape.C_Voxel2->getNumOfVoxels(),
			    shape.C_Voxel1->getDim(), vox1, vox2,
			    template_name);
	    vmatch.setHistoParams(num_of_bins, hist_dist, ass_meth,
				  discriminant_ratio);

	    vmatch.setTransfoMode(transfo_mode );
	}

    }
    //Match embedded representation
    if (shape.C_Embed1->isLoaded() && shape.C_Embed2->isLoaded()) {
	PRINT_COLOR_(fprintf(stderr, "MATCHING embedding spaces\n"),
		     GREEN);
	DBG_(fprintf(stderr, "-->loading embedded representations\n"));
	FL *coords1 = shape.C_Embed1->getCoords();
	FL *coords2 = shape.C_Embed2->getCoords();

	shape.C_Embed1->normalizeEmbedding(normalize_embed);
	shape.C_Embed2->normalizeEmbedding(normalize_embed);



	data_size = MAX(shape.C_Embed1->getNumOfPoints() , shape.C_Embed2->getNumOfPoints()) ;


	if (!disp_on) {
	    	    match.loadData(shape.C_Embed1->getNumOfPoints(),
			   shape.C_Embed2->getNumOfPoints(),
			   shape.C_Embed1->getDim(), coords1, coords2,
			   template_name);
	} else {
	    	    vmatch.loadData(shape.C_Embed1->getNumOfPoints(),
			    shape.C_Embed2->getNumOfPoints(),
			    shape.C_Embed1->getDim(), coords1, coords2,
			    template_name);

	}
    }

    int l_max_data_size = 0 ;
    HASH_MAP_PARAM_(int, "max_data_size", Int, l_max_data_size) ;
    if (data_size > l_max_data_size) {
	std::cout << "We force the use of a mode that is slower but requires less memory" <<std::endl ;
	use_alpha_line = true ;

    }

    

    // Set other parameters and RUN EM
    if (!disp_on) {
	if (eval_matching)
	    match.enableEvalSmoothness (true) ;
	PRINT_COLOR_(fprintf(stderr, "Start off-screen MATCHING\n"), RED);
	match.setHistoParams(num_of_bins, hist_dist, ass_meth,
			     discriminant_ratio);
	match.setTransfoMode(transfo_mode );
	match.setRemoveFirst(remove_first);
	match.setSigma(sigma);
	match.setOutlierConstant(kout);



	match.setAlphaLineMode (use_alpha_line) ;




	if ( use_alpha_line ) {
	    match.setLineSample(line_sample);
	    match.lineEM(init_mode, em_mode, int_pointer, fl_pointer);
	}
	else {
	    match.EM(init_mode, em_mode, int_pointer, fl_pointer);
	}

    } else {
	PRINT_COLOR_(fprintf(stderr, "Starting on-screen MATCHING\n"),
		     RED);

	vmatch.setModelSep(trans_x, trans_y, trans_z) ;


	if (eval_matching)
	    match.enableEvalSmoothness (true) ;

	if (win_title)
#if QT_VERSION < 0x040000
	    vmatch.setCaption(win_title);
#else
	    vmatch.setWindowTitle(win_title);
#endif
	vmatch.setHistoParams(num_of_bins, hist_dist, ass_meth,
			      discriminant_ratio);
	vmatch.setTransfoMode(transfo_mode );
	vmatch.setSigma(sigma);
	vmatch.setOutlierConstant(kout);
	vmatch.setLineSample(line_sample);

	vmatch.setAlphaLineMode (use_alpha_line) ;


#if QT_VERSION < 0x040000
	application.setMainWidget(&vmatch);
#else
	vmatch.setWindowTitle(win_title);
#endif
	vmatch.show();
	vmatch.setRemoveFirst(remove_first);

	vmatch.visualEM(init_mode, em_mode, int_pointer, fl_pointer);

	application.exec();

    }

    if (int_pointer)
	delete[]int_pointer;

    if (fl_pointer)
	delete[]fl_pointer;

    shape.releaseAll();


    if (embed1)
	free(embed1);
    if (embed2)
	free(embed2);

    if (voxel1)
	free(voxel1);
    if (voxel2)
	free(voxel2);

    if (voxembed1)
	free(voxembed1);
    if (voxembed2)
	free(voxembed2);


    if (connect1)
	free(connect1);
    if (connect2)
	free(connect2);
    if (bin_match)
	free(bin_match);
    if (proba_match)
	free(proba_match);

    if (template_name)
	free(template_name);
    if (transfo_file)
	free(transfo_file);
    if (labels1)
	free(labels1);
    if (labels2)
	free(labels2);

    if (win_title)
	free(win_title);


    return 0;
}

/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/

/*
 *  Constants.h
 *  SpecMatch
 *
 *  Created by David Knossow on 08/10/08.
 *  Copyright 2007 INRIA. All rights reserved.
 *
 * This file contains defines for constants used in the code
 */
#ifndef _CONSTANTS_H
#define _CONSTANTS_H
#include <iostream>
#include <string> 
#include "variant.h"
#include <ext/hash_map>


#if __GNUC_MINOR__ == 0
using namespace std;               // Pour GCC 3.0
using namespace __gnu_cxx;       // Pour GCC 3.1 et plus
#else
using namespace __gnu_cxx;       // Pour GCC 3.1 et plus
#endif 




struct hash_func
{
  size_t operator()( const std::string s1) const
  {
    return hash<const char *> () (s1.c_str()) ;
  }
};

struct hash_func_pair_int_int
{
  size_t operator()( const std::pair<int,int> & p_pair ) const
  {
    return hash<int> () (p_pair.first) + hash<int> () (p_pair.second);
  }
};




extern hash_map < std::string , Variant, hash_func > hash_map_global_params ;





#if 0

// Data size above which, we switch to a less memory greedy algorithm. 
// The counterpart: it is slower.
// The idea is that the program allocate a matrix of data_size * data_size * sizeof(double).
// For huge data_size, it is not possible. So instead of storing the computations in memory, we 
// recompute everytime it is required.
// MAX_DATA_SIZE of 8000 involves less than 512Mo of memory.
#define MAX_DATA_SIZE 8000

// Number max of matches displayed for one point in visual_shape
#define NMATCH 10


/***************************************************/
/* MATCHING */
/***************************************************/
// Defines the outlier constant in the assignement coefficients
#define K_OUT 0.2

// In case of selecting the ISO_ANNEAL approach for the EM, 
// it defines the temperature factor.
#define KAPA 0.95



 // Defines the threshold under which, we consider a model cluster
 // to be an outlier.
#define OUT_TH 1e-50		// includes outlier class

 // Defines the threshold under which, we consider an 
 // observation to be an unmatched point.
#define BETA_TH sqrt(DBL_MIN) //1e-50

 // Max number of iterations in the EM algorithm
#define MAX_IT 4000

 // if computed sigma is less than MIN_SIGMA, the EM stops.
#define MIN_SIGMA 1e-9		//initial minimum sigma

 // If the estimated transformation has changed less (in terms of Froebenius norm) 
 // than MIN_T, the EM stops.
#define MIN_T 1e-10

 // If the Error has changed less than MIN_E, the EM stops
#define MIN_E 1e-10


 // Defines the threshold under which an observation is no more attached to a cluster.
#define ZERO_TH sqrt(DBL_MIN)

 // Max number of observations to attach to a cluster.
#define N_MATCH 5		//number of observations per class

#define STR_LENGTH 2048

// Set the number of closest neighbors to look for
// when calling computeSigmaFromNN
#define NUM_OF_NEAREST_NEI 5



/***************************************************/
/* HISTOGRAMS */
/***************************************************/
// Default number of bins in histograms. It is set using the --bins option in match.
#define NUM_OF_BINS 300

// When performing the bin to bin distance, we allow for translation, vertical and horizontal scaling.
#define TRANS_BINS 25

// Horizontal stretching of the histogram, when performing the bin to bin distance
#define HOR_SCALE_MIN 0.8
#define HOR_SCALE_MAX 2.1
#define HOR_SCALE_STEP 0.1

//Vertical stretching of the histograms, when performing the bin to bin distance
#define VER_SCALE_MIN 0.9
#define VER_SCALE_MAX 1.1
#define VER_SCALE_STEP 0.1


/***************************************************/
/* COLORS */
/***************************************************/

// Colomap size for display purposes.
#define COLORMAP_STD_SIZE 64



#define BUFF_SIZE 2048

// Size of cubes to be drawn to represent voxels or 3D points.
#define CUBE_SIZE 0.01


#define GRAPH_SAMPLING_RATE 1000


// Defines the size of points to be displayed, for voxels or meshes.
#define POINT_SIZE 10

// Define the Color depth (r,g,b,a)
#define COLOR_DEPTH 4


// It is essiantially for meshes
// Generally, a mesh facet is a triangle or a squarre. So 5 vertices per facets is sufficient.
#define MAX_NUM_VERT_PER_FACETS 5

// For one vertex, the max number of connected points. It should be 6 in a regularized mesh...
// But we are not leaving in the Bisounours world... So 15 is Ok.
#define MAX_NUM_OF_NEI 15

// For one vertex, the max number of attached facets. It should be 6 in a regularized mesh...
// But we are not leaving in the Bisounours world... So 15 is Ok.
#define MAX_NUM_FACETS_PER_VERTEX 20


// If we decide to do a spread of the connectivity, it is the max number of points that can
// be contained in a single neighborhood. This value can be changed.
#define TOTAL_MAX_NUM_NEI 300

// When asking to draw NEI set sets on the 3D shape, we sample the number of points on which we draw
// the neighbors.
#define NEI_DRAW_SAMPLE_RATE 20

// When reading the connectivity file for the voxels, it is the max number of connected points to be read.
#define MAX_NUM_NEIG 100




#define MEDIAN_MAX_NUM_TRIALS 10

#define DEF_REMOVE_FIRST true
#define SQEIGNORM true

#define NORM_FACTOR LAMBDA


#define GAUSSIAN_HUBER_ROBUST_SCALE_ESTIMATOR 1.4826

// Set the number of matches to be displayed together if 
// displaying only part of the matches.
#define SET_NUM_OF_MATCH_TO_DISP 1000




// Defines the sample rate of the matching
#define SAMPLE 1

// Defines the sampling rate for the vizualisation
#define VIZU_SAMPLE 2


// Defines a translation factor to distinct two shapes drawn in match.
#define MODEL_SEP 0.0

#endif
#endif

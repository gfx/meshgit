/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
/*
 *  eigs.h
 *  embed
 * 
 * Code adapted from:
 *   Scot Shaw
 * 30 August 1999
 *
 */

#ifndef EIGS_H
#define EIGS_H


#include <cmath>
#include <iostream>
#include "types.h"

extern "C" void dsaupd_(int *ido, char *bmat, int *n, char *which,
			int *nev, double *tol, double *resid, int *ncv,
			double *v, int *ldv, int *iparam, int *ipntr,
			double *workd, double *workl, int *lworkl,
			int *info);

extern "C" void dseupd_(int *rvec, char *All, int *select, double *d,
			double *v1, int *ldv1, double *sigma,
			char *bmat, int *n, char *which, int *nev,
			double *tol, double *resid, int *ncv, double *v,
			int *ldv, int *iparam, int *ipntr, double *workd,
			double *workl, int *lworkl, int *ierr);

class Eigs {

  private:
    double *eig_vecs;
    double *eig_vals;		// hold the resultant eigenvalues and eigenvectors

    FL *Tvec;
    FL **T;			// sparse matrix format T[i][0] and T[i][1] hold the indexes
    // remember to convert them to int when using the indexes
    // T[i][2] holds the value
    int tlen;			// The total number of non-zero values (used to read T)
    int mat_size;		// Size of the matrix (n x n)
    int num_of_eig_vec;		// Number of eigenvalues / eigenvectors
    int rvec;			// 0 if only computing eigenvalues / 1 if also eigenvectors
    void av(int n, double *in, double *out);	//internal sparse matrix multiplication method         
    // out = T*in where in is a vector
    // used by the dsaup function and communicates
    // with the fortran functions

    //Fortran  function parameters

    char bmat[2];		/* Specifies that the right hand side matrix
				   should be the identity matrix; this makes
				   the problem a standard eigenvalue problem.
				   Setting bmat = "G" would have us solve the
				   problem Av = lBv (this would involve using
				   some other programs from BLAS, however). */

    char which[3];
    // Ask for the nev eigenvalues of smallest
    // magnitude.  The possible options are
    //LM: largest magnitude
    //SM: smallest magnitude
    //LA: largest real component
    //SA: smallest real compoent
    //LI: largest imaginary component
    //SI: smallest imaginary component 

    double tol;			/* Sets the tolerance; tol<=0 specifies machine precision */
    int modifyT;		// 0 don't delete T matrix / 1 created internally then delete it at output

  public:

     Eigs();
    ~Eigs();
    void release();
    int dsaupd();		// call to fortran and commuication
    void loadDummyMatrix();	//original test
    int loadSparseMatrix(int in_n, int in_tlen, double **in_T);	//only pass pointer problem when deleting
    int loadSparseMatrix(int in_n, int in_tlen, int **in_T);	//makes a copy
    int loadSparseMatrix(int in_n, int in_nzelems, float **in_T);	//makes a copy
    int load_hash_table();
    int setParams(int in_nev, char *in_which = "LM", char *in_bmat =
		  "I", int evec_on = 1);
    FL *getEigVecs();
    FL *getEigVals();
    int getNumOfEigVec();
    int getMatSize();
    char *getWhich();
};

#endif

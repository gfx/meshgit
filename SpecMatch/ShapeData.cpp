/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
/*
 *  ShapeData.cpp
 *  
 *
 *  Created by Diana Mateus and David Knossow on 10/5/08.
 *  Copyright 2007 INRIA. All rights reserved.
 *
 */

#include "ShapeData.h"

ShapeData::ShapeData()
{

    loaded = false;

    dim = 3;

    points = NULL;		// can be either vertices or voxels
    points_color = NULL;	// // can be either colors for vertices or voxels
    vector_field = NULL;

    colormap = NULL;
    ncolor = 0;
    function_value = NULL;

    npoints = 0;

    scale = 1.0;

    filename = NULL;

    POINTSET = (uint) (0);	// ID of the list of points
    VECTORFIELD = (uint) (0);	//ID of the vector field list
    connect = NULL;
    ncolor = 0;
    n_functions = 0;
    func_min = NULL;
    func_max = NULL;
    mask = NULL;

    min_color = 0 ;
    max_color = 0 ;

    g_cube_size = -1;
    str_length = -1 ;

    color_depth = -1 ;
    nzelems = 0;
}


ShapeData::~ShapeData()
{

    if (points)
	delete[]points;

    if (colormap)
	delete[]colormap;

    if (points_color)
	delete[]points_color;

    if (function_value)
	delete[]function_value;

    if (vector_field) {
	delete[]vector_field;
	vector_field = NULL;
    }

    if (filename)
	delete[]filename;

    if (connect)
	delete[]connect;

    if (func_min)
	delete[]func_min;

    if (func_max)
	delete[]func_max;

    if (mask)
	delete[]mask;

}
void ShapeData::init () {
  HASH_MAP_PARAM_(int, "color_depth", Int, color_depth) ;

}

int ShapeData::releaseData()
{

    PRINT_(std::cout << "ShapeData: releaseData()" << std::endl);

    if (points) {
	delete[]points;
	points = NULL;
	npoints = 0;
    }
    if (colormap) {
	delete[]colormap;
	colormap = NULL;
	ncolor = 0;
    }
    if (function_value) {
	delete[]function_value;
	function_value = NULL;
    }
    if (vector_field) {
	delete[]vector_field;
	vector_field = NULL;
    }
    if (connect) {
	delete[]connect;
	connect = NULL;
    }
    if (func_min) {
	delete[]func_min;
	func_min = NULL;
    }
    if (func_max) {
	delete[]func_max;
	func_max = NULL;
    }
    if (points_color) {
	delete[]points_color;
	points_color = NULL;
    }
    if (mask) {
	delete[]mask;
	mask = NULL;
    }
    if (filename) {
	delete[]filename;
	filename = NULL;
    }

    loaded = false;

    return 0;
}


int ShapeData::loadGraph(int p_nzelems, FL * p_distmat, int p_offset)
{
    DBG_(fprintf(stderr, "Loading graph \n"));

    if (p_distmat == NULL) {
	ERROR_(fprintf
	       (stderr,
		"Trying to load graph from empty distance matrix\n"),
	       ERROR_EMPTY_DATA);
    }
    if (connect) {
	delete[]connect;
	connect = NULL;
    }

    nzelems = p_nzelems;
    connect = new int[nzelems * 2];

    //copy
    for (int i = 0; i < nzelems; i++) {
	connect[i * 2] = (int) p_distmat[3 * i] + p_offset;
	connect[i * 2 + 1] = (int) p_distmat[3 * i + 1] + p_offset;
    }
    return 0;
}


int ShapeData::loadGraph(int p_nzelems, int *p_distmat, int p_offset)
{

    DBG_(fprintf(stderr, "Loading graph \n"));

    if (p_distmat == NULL) {
	ERROR_(fprintf
	       (stderr,
		"Trying to load graph from empty distance matrix\n"),
	       ERROR_EMPTY_DATA);
    }

    if (connect) {
	delete[]connect;
	connect = NULL;
    }

    nzelems = p_nzelems;
    connect = new int[nzelems * 2];

    //copy
    for (int i = 0; i < nzelems; i++) {
	connect[i * 2] = p_distmat[3 * i] + p_offset;
	connect[i * 2 + 1] = p_distmat[3 * i + 1] + p_offset;
    }
    return 0;

}


int ShapeData::buildPointsetList(float p_cube_size)
{
    DBG_VISU_(fprintf(stderr, "Building point-set list\n"));
    if (!points || !loaded) {
	ERROR_(fprintf
	       (stderr,
		"Can not build point set list. Pointset not yet loaded\n"),
	       ERROR_MEMORY_ALLOCATION);
    }
    //Delete previous list
    if (POINTSET != (uint) (0)) {
	glDeleteLists(POINTSET, 1);
	POINTSET = (uint) (0);
    }
    //Create new list
    POINTSET = glGenLists(1);
    DBG_VISU_(fprintf(stderr, "point set: list %d\n", (int) POINTSET));

    //Make list with voxels
    if (p_cube_size != -1.0f) {
	if (glIsList(POINTSET)) {
	    glNewList(POINTSET, GL_COMPILE);
	    DBG_VISU_(fprintf
		  (stderr, " New pointset list with %d cubes\n", npoints));
	    float x, y, z;
	    for (int i = 0; i < npoints; i++) {
		glColor3f(points_color[i * color_depth],
			  points_color[i * color_depth + 1],
			  points_color[i * color_depth + 2]);
		x = (float) (points[i * dim] / scale);
		y = (float) (points[i * dim + 1] / scale);
            if (dim == 3) {
		z = (float) (points[i * dim + 2] / scale);
            }
            else {
                z = 1.0 / scale ;   
            }
            
	    drawCube(x, y, z, p_cube_size);
	    DBG_VISU_(fprintf
		  (stdout, "List pos %d  %g %g %g \n", i, x, y, z));
	    DBG_VISU_(fprintf(stdout, "List pos %d:  %g %g %g \n", i, points_color[i * color_depth], points_color[i * color_depth + 1], points_color[i * color_depth + 2]));	//points[i*dim],points[i*dim+1],points[i*dim+2]));
	    }
	    
	    glEndList();
	}
    }
    //Make list with points
    else if (p_cube_size == -1.0f) {
      int point_size = 0 ;
      HASH_MAP_PARAM_(int, "point_size", Int, point_size ) ;
      
      if (glIsList(POINTSET)) {
	glNewList(POINTSET, GL_COMPILE);
	glPointSize( point_size ) ;
	glBegin(GL_POINTS);
	float x, y, z;
	    for (int i = 0; i < npoints; i++) {
		glColor3f(points_color[i * color_depth],
			  points_color[i * color_depth + 1],
			  points_color[i * color_depth + 2]);
		x = (float) (points[i * dim] / scale);
		y = (float) (points[i * dim + 1] / scale);
            if (dim == 3) {
            z = (float) (points[i * dim + 2] / scale);
            }
            else {
                z = 1 / scale ;     
            }
                glVertex3f(x, y, z);
		DBG_VISU_(fprintf
		      (stdout, "List pos %d  %g %g %g \n", i, x, y, z));
		DBG_VISU_(fprintf(stdout, "List pos %d  %g %g %g \n", i, points_color[i * color_depth], points_color[i * color_depth + 1], points_color[i * color_depth + 2]));	//points[i*dim],points[i*dim+1],points[i*dim+2]));
	    }
	    glEnd();

	    glEndList();
	}
    }


    return 0;
}

int ShapeData::buildVectorfieldList()
{
    DBG_VISU_(fprintf(stderr, "Building vector field list\n"));

    if (!points || !vector_field || !loaded) {
	FATAL_(fprintf
	       (stderr,
		"Can not build vector field list. Pointset not yet loaded\n"),
	       FATAL_MEMORY_ALLOCATION);
    }
    //Delete previous list
    if (VECTORFIELD != (uint) (0)) {
	glDeleteLists(VECTORFIELD, 1);
	VECTORFIELD = (uint) (0);
    }
    //Create new list
    VECTORFIELD = glGenLists(1);
    DBG_VISU_(fprintf(stderr, "Vector field: list %d\n", (int) VECTORFIELD));

    //Make list with voxels
    if (glIsList(VECTORFIELD)) {
	glNewList(VECTORFIELD, GL_COMPILE);
	DBG_VISU_(fprintf
	      (stderr, " New vector field list with %d cubes\n", npoints));
	glPointSize(1.5f);	//1.0
	glColor3f(0.5, 0.5, 0.5);	//(0.7,0.7,0.7)
	glBegin(GL_POINTS);
	float x, y, z;
	for (int i = 0; i < npoints; i++) {
	    x = (float) (points[i * dim] / scale);
	    y = (float) (points[i * dim + 1] / scale);
	    if (dim == 3)
		z = (float) (points[i * dim + 2] / scale);
	    else 
		z = 0.5 ;
	    glVertex3f(x, y, z);
	}
	glEnd();

	glColor3f(0.1, 0.8, 0.1);
	glLineWidth(1.2f);	//1.5
	glBegin(GL_LINES);
	for (int i = 0; i < npoints; i++) {
	    if (mask && !mask[i])
		continue;
	    //glColor3f(points_color[i*color_depth], points_color[i*color_depth+1], points_color[i*color_depth+2]);
	    x = (float) (points[i * dim] / scale);
	    y = (float) (points[i * dim + 1] / scale);
	    if (dim == 3)
		z = (float) (points[i * dim + 2] / scale);
	    else 
		z = 0.5 ;	
	    glVertex3f(x, y, z);
	    x += (float) (vector_field[i * dim] / scale);
	    y += (float) (vector_field[i * dim + 1] / scale);
	     if (dim == 3)
		 z += (float) (vector_field[i * dim + 2] / scale);
	    glVertex3f(x, y, z);
	    DBG_VISU_(fprintf(stdout, "List pos %d  %g %g %g \n", i, x, y, z));
	    DBG_VISU_(fprintf(stdout, "List pos %d:  %g %g %g \n", i, points_color[i * color_depth], points_color[i * color_depth + 1], points_color[i * color_depth + 2]));	//points[i*dim],points[i*dim+1],points[i*dim+2]));
	}
	glEnd();
	glEndList();
    }

    return 0;
}

int ShapeData::drawVectorfieldList()
{
    if (!loaded || !vector_field) {
	ERROR_(std::cout << "Vector field not loaded yet" << std::endl,
	       ERROR_MEMORY_ALLOCATION);
    }

    glCallList(VECTORFIELD);

    return 0;
}


int ShapeData::setColordepth(int depth)
{

    color_depth = depth;

    return 0;
}


int ShapeData::buildColormap(int in_ncolor, int *colorind)
{

    if (!points) {
	ERROR_(fprintf
	       (stderr,
		"Can not build colormap, pointset not loaded yet\n"),
	       ERROR_EMPTY_DATA);
    }
    if (colormap) {
	delete[]colormap;
	colormap = NULL;
    }

    ncolor = in_ncolor;
    colormap = new float[color_depth * ncolor];
    DBG_VISU_(fprintf
	  (stderr, "Building colormap with %d colors\n", in_ncolor));

    float r, g, b, deltar, deltag, deltab, minr, ming, minb;
    deltar = 0.8f;
    deltag = 0.8f;
    deltab = 0.8f;
    minr = 0.1f;
    ming = 0.1f;
    minb = 0.1f;
    int ind;
    float colval = 0.0f;
    for (int i = 0; ((i < npoints) && (i < ncolor)); i++) {
	if (colorind)
	    ind = colorind[i];
	else
	    ind = i;
	if (ind < 0) {		//outlier 
	    r = g = b = 0.5;
	} else if (ind < (int) round((float) (ncolor) / 3.0f)) {
	    colval = 3.0f * (float) ind / (float) (ncolor);
	    r = minr;
	    g = deltag * (colval) + ming;
	    b = deltab + minb;
	} else if (ind < (int) round(2.0f * (float) (ncolor) / 3.0f)) {
	    ind = ind - (int) round((float) (ncolor) / 3.0f);
	    colval = 3.0f * (float) ind / (float) (ncolor);
	    r = deltar * (colval) + minr;
	    g = deltag + ming;
	    b = deltab * (1.0f - colval) + minb;
	} else {
	    ind = ind - (int) round(2.0f * (float) (ncolor) / 3.0f);
	    colval = 3.0f * (float) ind / (float) (ncolor);
	    r = deltar + minr;
	    g = deltag * (1.0f - colval) + ming;
	    b = minb;


	}

	/*
	   // Useful to remove red point in hilton illus
	   if (r > 0.9 && b < 0.4 && g < 0.5) {
	   colormap[i*color_depth] = 0.5;
	   colormap[i*color_depth+1] = 0.5;
	   colormap[i*color_depth+2] = 0.5;
	   }
	   else {
	 */
	colormap[i * color_depth] = r;
	colormap[i * color_depth + 1] = g;
	colormap[i * color_depth + 2] = b;
	//        }
	if (color_depth == 4)
	    colormap[i * color_depth + 3] = 1;
	DBG_VISU_(fprintf
	      (stderr, "color %d : %g %g %g\n", ind,
	       colormap[i * color_depth], colormap[i * color_depth + 1],
	       colormap[i * color_depth + 2]));

    }
    return 0;
}

int ShapeData::buildGrayColormap(int p_ncolor, int *p_colorind)
{
    if (!points) {
	ERROR_(fprintf
	       (stderr,
		"Can not build colormap, pointset not loaded yet\n"),
	       ERROR_EMPTY_DATA);
    }
    if (colormap) {
	delete[]colormap;
	colormap = NULL;
    }

    ncolor = p_ncolor;
    colormap = new float[color_depth * ncolor];
    DBG_VISU_(fprintf(stderr, "Building colormap with %d colors\n", p_ncolor));

    float l_r, l_g, l_b, l_deltar, l_deltag, l_deltab, l_minr, l_ming,
	l_minb;
    l_deltar = 0.7f;
    l_deltag = 0.7f;
    l_deltab = 0.7f;
    l_minr = 0.1f;
    l_ming = 0.1f;
    l_minb = 0.1f;
    int ind;
    float l_colval = 0.0f;
    for (int i = 0; ((i < npoints) && (i < ncolor)); i++) {
	if (p_colorind)
	    ind = p_colorind[i];
	else
	    ind = i;
	if (ind < 0) {		//outlier 
	    l_r = 0.8;
	    l_g = 0.2;
	    l_b = 0.2;
	}

	l_colval = (float) ind / (float) (ncolor);
	l_r = l_deltar * l_colval + l_minr;
	l_g = l_deltag * l_colval + l_ming;
	l_b = l_deltab * l_colval + l_minb;
	colormap[i * color_depth] = l_r;
	colormap[i * color_depth + 1] = l_g;
	colormap[i * color_depth + 2] = l_b;
	if (color_depth == 4)
	    colormap[i * color_depth + 3] = 1;
	DBG_VISU_(fprintf
	      (stderr, "color %d : %g %g %g\n", ind,
	       colormap[i * color_depth], colormap[i * color_depth + 1],
	       colormap[i * color_depth + 2]));
    }

    return 0;
}

int ShapeData::fillColorWithFunctionValue(int p_f)
{
    if (!colormap) {
	ERROR_(fprintf
	       (stderr,
		"Colormap has to be built before trying to fill color with function value\n"),
	       ERROR_MEMORY_ALLOCATION);
    }
    if (!function_value) {
	ERROR_(fprintf
	       (stderr,
		"Function values have to be loaded before trying to fill color with function value\n"),
	       ERROR_MEMORY_ALLOCATION);
    }
    DBG_VISU_(fprintf(stderr, "Filling color values of dimension %d \n", p_f));

    FL factor;
    //FL globmin;

    DBG_VISU_(std::cout << "In fillColorWithFunctionValue, p_f : " << p_f << " " << n_functions << std::endl) ;

    if ((func_max[p_f] - func_min[p_f]) < 1e-12)
	factor = 0.0;
    else
	factor = ((FL) (ncolor - 1)) / (func_max[p_f] - func_min[p_f]);

    int ind;
    int l_masked = 0;

    for (int i = 0; i < npoints; i++) {
	if ((mask) && (!mask[i])) {
	    DBG_VISU_(fprintf(stderr, "point %d masked\n", i));
	    points_color[i * color_depth] = 0.5;
	    points_color[i * color_depth + 1] = 0.5;
	    points_color[i * color_depth + 2] = 0.5;
	    if (color_depth == 4)
		points_color[i * color_depth + 3] = 1;
	    l_masked++;
	    continue;
	}


	ind =
	    (int) round(factor *
			(function_value[i * n_functions + p_f] -
			 func_min[p_f]));

	if ((ind < 0) || (ind >= ncolor)) {
	    WARNING_(fprintf(stderr,
		    "Quantized function value[%d]: %d beyond quantized limits (%d, %d). val %g Factor %g \n",
		    i, ind, 0, ncolor - 1,
			     function_value[i * n_functions + p_f], factor));
	    points_color[i * color_depth] = 0.5;
	    points_color[i * color_depth + 1] = 0.5;
	    points_color[i * color_depth + 2] = 0.5;
	    if (color_depth == 4)
		points_color[i * color_depth + 3] = 1;
	} else {
	    points_color[i * color_depth] = colormap[ind * color_depth];
	    points_color[i * color_depth + 1] =
		colormap[ind * color_depth + 1];
	    points_color[i * color_depth + 2] =
		colormap[ind * color_depth + 2];
	    if (color_depth == 4)
		points_color[i * color_depth + 3] = 1;
	}
	DBG_VISU_(fprintf
	      (stdout, "color[%d]= %d (%g %g %g) \n", i, ind,
	       points_color[i * color_depth],
	       points_color[i * color_depth + 1],
	       points_color[i * color_depth + 2]) );
    }

    DBG_VISU_(fprintf(stderr, "Masked points %d\n", l_masked));


    //colorbar settings
    zero_color = (int) round(factor * (func_max[p_f] - func_min[p_f]) / 2 );

    min_color = 0;
    max_color = (int) round(factor * (func_max[p_f] - func_min[p_f]));

    return 0;
}



int ShapeData::initFunctionValue(int p_nfunc)
{

    //Allocate memory for function_value
    if (function_value) {
	delete[]function_value;
	function_value = NULL;
    }
    DBG_(fprintf(stderr, "Initializing %d functions \n", p_nfunc));

    if (func_min) {
	delete[]func_min;
	func_min = NULL;
    }
    if (func_max) {
	delete[]func_max;
	func_max = NULL;
    }
    if (mask) {
	delete[]mask;
	mask = NULL;
    }
    n_functions = p_nfunc;
    function_value = new FL[npoints * n_functions];
    bzero(function_value, npoints * n_functions * sizeof(FL));
    func_min = new FL[ n_functions ] ;
    func_max = new FL[ n_functions ] ;


    return 0;

}

void ShapeData::createMask()
{
    DBG_(fprintf
	 (stderr,
	  "Mask points whose sum over all dimesions =0 (%d points - %d fct values) \n",
	  npoints, n_functions));

    int l_count = 0;
    //for a simple dimension mask when the value is equal to -1;
    if (n_functions == 1) {
	mask = new int[npoints];
	for (int i = 0; i < npoints; i++) {
	    if (function_value[i] == -1.0)
		mask[i] = 0;
	    else
		mask[i] = 1;
	}
    }
    //mask whenever all dimensions of a function are equal to zero
    if (n_functions > 1) {
	mask = new int[npoints];
	for (int i = 0; i < npoints; i++) {
	    int d = 0;
	    for (; d < n_functions; d++) {
		if (function_value[i * n_functions + d] != 0.0)
		    break;
	    }
	    if (d == n_functions) {
		mask[i] = 0;
		l_count++;
	    } else
		mask[i] = 1;
	}
    }
    DBG_(fprintf(stderr, "%d points where found masked\n", l_count));

}

int ShapeData::loadFunctionValue(FL * p_values, int p_nfunc)
{
    if (!p_values) {
	ERROR_(fprintf
	       (stderr, "Can not load functions from an empty pointer\n"),
	       ERROR_MEMORY_ALLOCATION);
    }
    initFunctionValue(p_nfunc);

    PRINT_(fprintf(stderr, "Loading functions : %d\n", n_functions));
    FL l_minval, l_maxval;

    //Copy and Set min and max for quantization
    for (int i = 0; i < n_functions; i++) {
	l_minval = 0.0;
	l_maxval = 0.0;
	func_min[i] = 0.0 ;
	func_max[i] = 0.0 ;


	for (int j = 0; j < npoints; j++) {



	    FL l_val = p_values[j * n_functions + i];
	    function_value[j * n_functions + i] = l_val;

	    if ((l_val < l_minval) || (j == 0))
		l_minval = l_val;
	    if ((l_val > l_maxval) || (j == 0))
		l_maxval = l_val;
	}
	
	
	if (l_minval >= 0.0 && l_maxval >= 0.0) {
	    func_min[i] = l_minval;
	    func_max[i] = l_maxval;
	} else if (l_minval <= 0.0 && l_maxval <= 0.0) {
	    func_min[i] = l_maxval;
	    func_max[i] = l_minval;
	} else {
	    func_min[i] = l_minval;
	    func_max[i] = l_maxval;
	}


#if 0
if (fabs(l_minval) >= fabs(l_maxval)) {
	    func_min[i] = l_minval;
	    func_max[i] = -l_minval;
	} else if (fabs(l_maxval) > fabs(l_minval)) {
	    func_min[i] = -l_maxval;
	    func_max[i] = l_maxval;
	}
#endif

	DBG2_(fprintf
	      (stderr, "Function %d has minval %g and maxval %g\n", i,
	       func_min[i], func_max[i]));
    }


    createMask();


    return 0;

}
int ShapeData::loadFunctionValue(int *p_values, int p_nfunc)
{

    if (!p_values) {
	ERROR_(fprintf
	       (stderr, "Can not load functions from an empty pointer\n"),
	       ERROR_MEMORY_ALLOCATION);
    }
    initFunctionValue(p_nfunc);

    FL l_minval, l_maxval;

    l_minval = p_values[0];
    l_maxval = p_values[0];
    //Copy and Set min and max for quantization
    for (int i = 0; i < n_functions; i++) {
	for (int j = 0; j < npoints; j++) {
	    FL l_val = (FL) (p_values[j * n_functions + i]);

	    function_value[j * n_functions + i] = l_val;
	    if (l_val == -1)
		l_val = 0;
	    if ((l_val < l_minval) || (j == 0))
		l_minval = l_val;
	    if ((l_val > l_maxval) || (j == 0))
		l_maxval = l_val;
	}
	func_min[i] = l_minval;
	func_max[i] = l_maxval;
	DBG2_(fprintf
	      (stderr, "Function test %d has minval %g and maxval %g\n", i,
	       func_min[i], func_max[i]));
    }

    createMask();
    return 0;

}


int *ShapeData::getMask()
{
    return mask;
}

int ShapeData::getNumOfFunctions()
{
    return n_functions;
}


float *ShapeData::getColorVector()
{
    return points_color;
}

int ShapeData::getColorDepth()
{
    return color_depth;
}

bool ShapeData::isLoaded()
{
    return loaded;
}

FL *ShapeData::getPoints()
{
    return points;
}

FL *ShapeData::getFunctionValue()
{
    return function_value;
}

FL *ShapeData::getVectorfield()
{
    return vector_field;
}

int ShapeData::getNumOfPoints()
{
    return npoints;
}

void ShapeData::setDim (int p_dim) {
    dim = p_dim ;
}

int ShapeData::getDim()
{
    return dim;
}

FL ShapeData::getScale()
{
    return scale;
}

int ShapeData::getNumOfColor()
{
    return ncolor;
}


float *ShapeData::getColormap()
{
    return colormap;
}


int ShapeData::getNzelems()
{
    return nzelems;
}


int ShapeData::setScale(FL s)
{
    scale = s;
    return 0;
}

int ShapeData::resetPointColor()
{

    if (loaded && points_color) {
	for (int i = 0; i < npoints; i++) {
	    points_color[i * color_depth] = 0.2;
	    points_color[i * color_depth + 1] = 0.4;
	    points_color[i * color_depth + 2] = 0.7;
	}
    }
    return 0;
}

int ShapeData::getPointColor(int ind, float *r, float *g, float *b)
{
    *r = points_color[ind * color_depth];
    *g = points_color[ind * color_depth + 1];
    *b = points_color[ind * color_depth + 2];
    return 0;
}
int ShapeData::setPointColor(int ind, float *r, float *g, float *b)
{
    points_color[ind * color_depth] = *r;
    points_color[ind * color_depth + 1] = *g;
    points_color[ind * color_depth + 2] = *b;
    return 0;
}


int ShapeData::setAllPointsColor(float r, float g, float b)
{

    for (int i = 0; i < npoints; i++) {
	points_color[i * color_depth] = r;
	points_color[i * color_depth + 1] = g;
	points_color[i * color_depth + 2] = b;
    }
    return 0;
}




//for colorbar
int ShapeData::getMinCol()
{
    return min_color;
}

int ShapeData::getMaxCol()
{
    return max_color;
}

int ShapeData::getZeroCol()
{
    return zero_color;
}

FL ShapeData::getFuncMin(int f)
{
    if (func_min)
	return func_min[f];
    else
	return 0;
}

FL ShapeData::getFuncMax(int f)
{
    if (func_max)
	return func_max[f];
    else
	return 0;
}


int ShapeData::drawPointsetList()
{

    if (!loaded) {
	std::cout << "Not loaded yet" << std::endl;
	return 1;
    }

    glCallList(POINTSET);

    return 0;
}





int ShapeData::fillColorWithColormap()
{

    if (!points) {
	fprintf(stderr,
		"Can not build colormap, pointset not loaded yet\n");
	return 1;
    }

    if ((!colormap) || ((colormap) && (ncolor != npoints))) {
	fprintf(stderr, "Building full colormap\n");
	buildColormap(npoints);
    }

    for (int i = 0; i < npoints; i++) {
	points_color[i * color_depth] = colormap[i * color_depth];
	points_color[i * color_depth + 1] = colormap[i * color_depth + 1];
	points_color[i * color_depth + 2] = colormap[i * color_depth + 2];
	DBG_VISU_(fprintf
	      (stderr, "color %d : %g %g %g\n", i,
	       points_color[i * color_depth],
	       points_color[i * color_depth + 1],
	       points_color[i * color_depth + 2]));
    }

    return 0;
}



void ShapeData::setColorVector(float *v)
{

    DBG_VISU_(std::cout << " SET COLOR VECTOR " << std::endl);
    if (points_color) {
	delete[]points_color;
	points_color = NULL;
    }

    points_color = new float[color_depth * npoints];

    for (int i = 0; i < npoints * color_depth; i++) {

	points_color[i] = v[i];

    }
}

/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
/*
 *  neighbors.cpp
 *  embedmat
 *
 *  Created by Diana Mateus and David Knossow on 12/15/07.
 *  Copyright 2007 INRIA All rights reserved.
 *
 */
#include "neighbors.h"
#include <iostream>


Neighborhood::Neighborhood()
{

    num_nei = NULL;
    neighbors = NULL;
    num_nei_k = NULL;
    nei_k = NULL;
    mat_weights = NULL;
    weights = NULL;
    nvoxels = 0;
    depth = 1;
    hash_elems = 0;
    nzelems = 0;

    hash_dist_vec = NULL;

    nei_offset = -1 ;

    HASH_MAP_PARAM_(int, "max_num_neig", Int, max_num_neig) ;
}

Neighborhood::~Neighborhood()
{

    if (neighbors)
	delete[]neighbors;

    if (num_nei)
	delete[]num_nei;

    if (mat_weights)
	delete[]mat_weights;

    if (num_nei_k)
	delete[]num_nei_k;

    if (nei_k)
	delete[]nei_k;

    if (weights)
	delete[]weights;

    if (hash_dist_vec) {
	for (int i = 0; i < nvoxels; ++i) {
	    g_hash_table_destroy(hash_dist_vec[i]);
	}
	delete[]hash_dist_vec;
	hash_dist_vec = NULL;
    }
}

void Neighborhood::releaseUnused()
{

    if (neighbors) {
	delete[]neighbors;
	neighbors = NULL;
    }

    if (num_nei) {
	delete[]num_nei;
	num_nei = NULL;
    }

    if (num_nei_k) {
	delete[]num_nei_k;
	num_nei_k = NULL;
    }

    if (nei_k) {
	delete[]nei_k;
	nei_k = NULL;
    }

}

int Neighborhood::loadNeighbors(int *p_num_nei, int *p_nei)
{

    if (!p_num_nei || !p_nei) {
	ERROR_(std::
	       cout << "Can not load neighbors from empty vectors" << std::
	       endl, ERROR_VARIABLE_INIT);
    }

    if (num_nei) {
	delete[]num_nei;
	num_nei = NULL;
    }

    num_nei = new int[nvoxels];
    memset(num_nei, 0, nvoxels * sizeof(int));

    if (neighbors) {
	delete[]neighbors;
	neighbors = NULL;
    }
    neighbors = new int[max_num_neig * nvoxels];

    memcpy(num_nei, p_num_nei, nvoxels * sizeof(int));
    memcpy(neighbors, p_nei, nvoxels * max_num_neig * sizeof(int));

    return 0;
}

int Neighborhood::readNeighbors(char *p_filename)
{

    if (p_filename == NULL) {
	ERROR_(std::
	       cout <<
	       "Can not load connectivity file from a (NULL) filename\n" <<
	       std::endl, ERROR_OPEN_FILE);
    }
    char l_next_string[256];
    PRINT_(std::cout << "Loading Nei file " << p_filename << "... ");

    std::ifstream in(p_filename);

    if (in.is_open() == 0) {
	ERROR_(std::cout << "File " << p_filename << " not found" << std::
	       endl, ERROR_OPEN_FILE);
    }

    while (in.peek() == ' ' || in.peek() == '\n')
	in.get();
    while (in.peek() == '#') {
	in.getline(l_next_string, 256);	// skip comment
    }
    in >> nvoxels;
    if (nvoxels == 0) {
	ERROR_(std::
	       cout << "Loading Cancelled. Num of voxels == 0" << std::
	       endl, ERROR_VARIABLE_INIT);
    }

    if (num_nei) {
	delete[]num_nei;
	num_nei = NULL;
    }
    num_nei = new int[nvoxels];
    memset(num_nei, 0, nvoxels * sizeof(int));

    if (neighbors) {
	delete[]neighbors;
	neighbors = NULL;
    }
    neighbors = new int[max_num_neig * nvoxels];

    int nbNei = 0;
    for (int c = 0, i = 0; c < nvoxels; c++) {
	in >> i;
	if (nei_offset) {
	    if (i == 0) {
		nei_offset = 0 ;
	    }
	    else if ( i == 1 ){
		nei_offset = 1 ;
	    }
	}
	    
	if (i != c + nei_offset) {
	    WARNING_(std::
		     cout << "There might be an error in the neighborhod file " << i << " " << c << std::endl);
	    getchar () ;
	}
	in >> nbNei;

	num_nei[c] = nbNei;
	int nb = 0;
	while (nb < nbNei) {
	    in >> neighbors[c * max_num_neig + nb];
	    ++nb;
	}
    }
    in.close();
    PRINT_(std::cout << "... Loaded" << std::endl);
    return 0;
}



int Neighborhood::initHashDistNei(int p_max_depth)
{

    if (!neighbors) {
	std::
	    cout << "Initial connectivity should be loaded (.nei file)" <<
	    std::endl;
	return 1;
    }
    if (num_nei_k) {
	delete[]num_nei_k;
	num_nei_k = NULL;
    }
    if (nei_k) {
	delete[]nei_k;
	nei_k = NULL;
    }

    total_length = (2 * p_max_depth + 1) * (2 * p_max_depth + 1) * (2 * p_max_depth + 1);	// max number of neighbors for the requested depth.

    // Initialization of the numbers of neighbors. Read from the NEI file and stored in numNei
    num_nei_k = new int[nvoxels];	//stores the real number of nei per voxel. (max:  6,27,125,7^3....);                                
    memcpy(num_nei_k, num_nei, nvoxels * sizeof(int));

    //Initalization of the nei vector with the indices of the neighbors

    nei_k = new int[total_length * nvoxels];
    bzero(nei_k, nvoxels * total_length * sizeof(int));

    PRINT_(std::cout << "Initialize the hash table of distances" << std::
	   endl);

    hash_elems = 0;

    if (hash_dist_vec) {
	for (int i = 0; i < nvoxels; ++i) {
	    g_hash_table_remove_all(hash_dist_vec[i]);
	}
	delete[]hash_dist_vec;
    }
    hash_dist_vec = new GHashTable *[nvoxels];

    for (int i = 0; i < nvoxels; ++i) {
	hash_dist_vec[i] = g_hash_table_new(NULL, NULL);
	int nk = i * max_num_neig;
	for (int j = 0; j < num_nei[i]; ++j) {
	    int n_curr = neighbors[nk + j] - nei_offset;	//current neighbor of curr_ind
	    g_hash_table_insert(hash_dist_vec[i], GINT_TO_POINTER(n_curr),
				GINT_TO_POINTER(1));
	    hash_elems++;
	}
    }

    return 0;

}



int Neighborhood::propagateDist(int p_max_depth)
{

    // Initialized here with 27 neis.
    //(for a depth of 1 it is 27 for a depth it is 125)
    PRINT_COLOR_(fprintf(stderr, "Start the propagation \n"), GREEN);

    //Start propagation

    //mask to avoid revisiting the neighbors
    bool *l_neidone = new bool[nvoxels];
    bzero(l_neidone, nvoxels * sizeof(bool));


    int l_count = 0;
    int curr_ind = 0;
    int l_total_count = 0;

    int l_depth;

    for (int i = 0; i < nvoxels; ++i) {
	l_depth = 1;		// initialisation of the depth (0 is one voxel and 1 is 27 voxels)
	// Copy current nei in nei_k ;
	memset(nei_k, 0, total_length * sizeof(int));
	memset(l_neidone, 0, nvoxels * sizeof(bool));

	memcpy(nei_k, neighbors + i * max_num_neig,
	       num_nei[i] * sizeof(int));

	l_count = num_nei_k[i];

	while (l_depth < p_max_depth) {
	    l_depth++;
	    for (int j = 0; j < num_nei_k[i]; ++j) {
		curr_ind = nei_k[j] - 1;	// current neighbor of i

		if (curr_ind > -1 && !l_neidone[curr_ind]) {	//if voxel ni has not been visited check its neighbors for i-nk links 

		    int nk = curr_ind * max_num_neig;	// position of the nei in the vector neighbor

		    for (int k = 0; k < num_nei[curr_ind]; ++k) {
			int n_curr = neighbors[nk + k] - 1;	//current neighbor of curr_ind 
			if (i != n_curr
			    && !g_hash_table_lookup(hash_dist_vec[i],
						    GINT_TO_POINTER
						    (n_curr))) {
			    g_hash_table_insert(hash_dist_vec[i],
						GINT_TO_POINTER(n_curr),
						GINT_TO_POINTER(l_depth));

			    nei_k[l_count] = n_curr + 1;
			    ++l_count;
			    ++hash_elems;
			}
		    }
		    l_neidone[curr_ind] = true;
		}
	    }
	    num_nei_k[i] = l_count;
	}

	l_total_count += num_nei_k[i];
    }

    delete[]l_neidone;
    l_neidone = NULL;

    delete[]nei_k;
    nei_k = NULL;

    return 0;
}


void Neighborhood::computeDistGraph(int p_max_depth)
{
    //// ADD A CHECK OF LOACL_GEO ???????? 

    if (p_max_depth < 1) {
	std::
	    cout << "computeDistGraph:: the depth has not been set:" <<
	    p_max_depth << std::endl;
	return;
    }

    PRINT_COLOR_(fprintf
		 (stderr,
		  "Computing Sparse Geodesic Distance Graph with max_depth = %d \n",
		  p_max_depth), RED);

    initHashDistNei(p_max_depth);
    propagateDist(p_max_depth);

    nzelems = hash_elems;
    PRINT_(fprintf
	   (stderr, "%d non zero elements %d %d \n", nzelems, hash_elems,
	    nvoxels * nvoxels));

    //allocate and then fill weights that serve as an output of the routine
    //They follow the sparse format of the eigs.cpp T matrix except here is int and there is double    

    fillSparseMatWeights();

    if (nei_k) {
	delete[]nei_k;
	nei_k = NULL;
    }

}







void Neighborhood::sortMatWeight()
{

    PRINT_COLOR_(fprintf(stderr, "Sorting Weight matrices \n"), GREEN);
    FL *l_sorted = NULL;

    for (int i = 0; i < nvoxels; ++i) {

	if (l_sorted)
	    delete[]l_sorted;
	l_sorted = new FL[3 * num_nei_k[i]];

	memcpy(l_sorted, mat_weights[i], 3 * num_nei_k[i] * sizeof(FL));

	qsort(l_sorted, num_nei_k[i], 3 * sizeof(FL), compareFunc);	// (const void * d1, const void *d2)

	memcpy(mat_weights[i], l_sorted, 3 * num_nei_k[i] * sizeof(FL));

    }

    if (l_sorted)
	delete[]l_sorted;



}






int Neighborhood::fillSparseMatWeights()
{
    if (!num_nei_k) {
	ERROR_(std::
	       cout <<
	       " The matrix with neighbors after propagation does not exist (check if free_neik() was called)"
	       << std::endl, ERROR_EMPTY_DATA);
    }
    if (weights) {
	delete[]weights;
	weights = NULL;
    }
    weights = new FL[3 * nzelems];

    if (mat_weights) {
	delete[]mat_weights;
	mat_weights = NULL;
    }
    mat_weights = new FL *[nvoxels];

    int l_count_nei = 0;
    int ni = 0;
    for (int i = 0; i < nvoxels; i++) {
	mat_weights[i] = &(weights[ni]);
	ni += num_nei_k[i] * 3;
	l_count_nei += num_nei_k[i];
    }

    int l_total_count = 0;
    GList *l_tempKey;
    GList *l_tempValues;
    GList *l_keys;
    GList *l_values;
    int l_count = 0;
    int l_cumu_count = 0;

    FL *l_t_weights = weights;
    int l_size = 0;
    for (int i = 0; i < nvoxels; ++i) {
	l_count = 0;
	l_keys = g_hash_table_get_keys(hash_dist_vec[i]);
	l_values = g_hash_table_get_values(hash_dist_vec[i]);


	l_tempKey = l_keys;
	l_tempValues = l_values;
	l_size = 3 * (g_list_length(l_keys));
	while (l_tempKey) {
	    int t = GPOINTER_TO_INT(l_tempKey->data);
	    int j = t;
	    *(l_t_weights) = (FL) i;
	    *(l_t_weights + 1) = (FL) j;
	    *(l_t_weights + 2) = (FL) GPOINTER_TO_INT(l_tempValues->data);

	    l_t_weights += 3;
	    l_total_count++;
	    l_count++;
	    l_tempKey = l_tempKey->next;
	    l_tempValues = l_tempValues->next;
	}

	//      t_weights += size ;

	l_cumu_count += 3 * l_count;

	if (l_count != num_nei_k[i])
	    std::
		cout << " At position " << i << " " << l_count << " != " <<
		num_nei_k[i] << std::endl;

	g_list_free(l_keys);
	g_list_free(l_values);

    }

    if (l_total_count != nzelems)
	std::
	    cout << "There is an error in Fill Sparse  Dist Matrix " <<
	    l_total_count << " != " << nzelems << std::endl;

    for (int i = 0; i < nvoxels; ++i) {
	g_hash_table_destroy(hash_dist_vec[i]);
    }
    delete[]hash_dist_vec;
    hash_dist_vec = NULL;


    sortMatWeight();

    return 0;
}


void Neighborhood::printSparseWeights(char *p_filename)
{

    PRINT_(std::cout << "Printing Sparse Matrix to " << p_filename << std::
	   endl);
    FILE *f = fopen(p_filename, "wb");
    fprintf(f, "%d\n", nzelems);
    for (int i = 0; i < nzelems; i++) {
	fprintf(f, "%d %d %d\n", (int) weights[i * 3],
		(int) weights[i * 3 + 1], (int) weights[i * 3 + 2]);
    }
    fclose(f);
}


bool Neighborhood::isLoaded()
{
    if (nvoxels > 0)
	return true;
    else
	return false;

    return true;
}

int Neighborhood::getNumOfVoxels()
{
    return nvoxels;
}

int Neighborhood::getNzelems()
{
    return nzelems;
}

FL **Neighborhood::getSparseGeodesicMatrix()
{
    return mat_weights;
}

FL *Neighborhood::getWeights()
{
    return weights;
}

int *Neighborhood::getNumOfNearestNei()
{
    return num_nei;
}

int *Neighborhood::getConnectivity()
{
    return neighbors;
}

GHashTable **Neighborhood::getHashDistVec()
{
    return hash_dist_vec;
}

int *Neighborhood::getNumOfNei()
{
    if (!num_nei_k)
	fprintf(stderr, "Warning: numNeik is empty\n");
    return num_nei_k;
}

int *Neighborhood::getNei()
{

    PRINT_(std::cout << "Building nei_k from the matweight matrix" << std::
	   endl);
    nei_k = new int[total_length * nvoxels];
    bzero(nei_k, total_length * nvoxels * sizeof(int));
    int *l_t_nei_k = nei_k;
    for (int i = 0; i < nvoxels; ++i) {
	for (int k = 0; k < num_nei_k[i]; ++k) {
	    *(l_t_nei_k + k) = mat_weights[i][k];
	}
	l_t_nei_k += total_length;
    }

    return nei_k;
}

int Neighborhood::getTotalLength()
{
    return total_length;
}

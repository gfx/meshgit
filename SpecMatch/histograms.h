/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
/*
 *  histograms.h
 *  SpecMatch
 *
 *  Created by Diana Mateus and David Knossow on 10/7/07.
 *  Copyright 2007 INRIA. All rights reserved.
 *
 * This class provides histogram's related functions. It allows for building histograms,
 * compute the distance between two histograms in different ways (Bin ToBin, EMD, Diffusion Distance.
 *
 */
#ifndef _HISTOGRAMS_H
#define _HISTOGRAMS_H
#include <math.h>

#include "Constants.h"
#include "types.h"
#include "utilities.h"

#ifdef USE_DIFFUSION_DISTANCE
#include "dd_head.h"
#endif
#ifdef USE_EMDL1_DISTANCE
#include "emdL1.h"
#endif

class Histogram {

  private:
    // File name template to save data.
    char *template_filename;

    // Set from the file template and required extension.
    char *filename;

    // Number of eigencvectors to built histograms from.
    int dim;

    // Number of bins per histograms.
    int num_of_bins;


    //bin to bin parameters
    FL hscale_min;
    FL hscale_max;
    FL hscale_step;
    FL vscale_min;
    FL vscale_max;
    FL vscale_step;

    // States for the way of building the histograms (see types.h).
    HISTOGRAM_NORMALIZATION histogram_nomalization;
  public:
     Histogram();
    ~Histogram();

    // Set the main parameters to build the histograms. 
    // dim: dimension of eigenspace (number of histograms to build.
    // nbins: number of bins per histogrmas.
    // Template: file name template to save data in.
    void setParameters(int dim, int nbins, char *Template);



    // Set global_min_value and global_max_valu to the min(min(Vect)) and max(max(Vect)). The output is a value.
    void setGlobalMinMaxFunc(FL * Vect, int *mask, int length,
			     FL * global_min_value, FL * global_max_value);

    // Set min_value and max_valu to the min(Vect) and max(Vect). The output is an array.
    void setMinMaxFunc(FL * Vect, int *mask, int length, FL * min_value,
		       FL * max_value);

    // Fill the histogram's bins given the histogram's normalization method.
    void fillHistogramMinMax(FL * Hist, FL * Vect, int *mask, int length,
			     FL * min, FL * max, FL Glob_min, FL Glob_max);


    // Build one histogram from an eigenvector.
    // X: the eigenvector
    // maskx : if set, histogram do not take into account the disconnected componnent (mask = 1 or 0 for each coordinate of the eigenvector)
    // vecHist: Histogram built
    // count: size of the eigenvector.
    void fillSingleHistogram(FL * X, int *maskX, FL * vecHist, int count);

    // Calls FillSingleHistogram for each eigenvector (XX, YY) and fills also the flipped version of YY.
    void fillAllHistograms(FL * XX, int *maskXX, FL * YY, int *maskYY,
			   FL * vecHistX, FL * vecHistY, FL * vecHistYF,
			   int countx, int county);


    



    //Distance Measurements. Calls following functions.
    void distance(HISTOGRAM_DISTANCE HistogramDistance,
		  FL * vecHistX, FL * vecHistY, FL * vecHistFlippedY,
		  FL * error_hist, FL * error_hist_Flipped);

    void computeBintoBinDifference(FL * vecHistX, FL * vecHistY,
				   FL * vecHistFlippedY, FL * error_hist,
				   FL * error_hist_Flipped);
    void computeEMDL1Dist(FL * h1, FL * h2, FL * h2F, double *distmat,
			  double *distmat_flipped);
    void computeDiffusionDist(FL * h1, FL * h2, FL * h2F, double *distmat,
			      double *distmat_flipped);

    // Sub-function for BintoBinDifference. (Compute the difference between each bins of 2 histograms)
    double difference(FL * hist1, FL * hist2, int num_bin, double scale,
		      double scale_h, int trans);



    // Assingment Functions. Finds the histograms that are more alike according to a given distance matrix
    void assign(ASSIGNMENT_METHOD AssignmentMethod,
		FL * assignment_hist, FL * cost,
		FL * error_hist, FL * error_hist_Flipped, FL * T);

    void computeFabioAssignment(FL * assignment_hist, FL * error_hist,
				FL * error_hist_Flipped, FL * T);
    void computeOptimalAssignment(FL * assignment_hist, FL * cost,
				  FL * error_hist, FL * error_hist_Flipped,
				  FL * T);
    void computeSubOptimalAssignment(FL * assignment_hist, FL * cost,
				     FL * error_hist,
				     FL * error_hist_Flipped, FL * T);


    // Deffined in hungarian.cpp        
    FL *get_T();


    void assignmentFabio(double *distMatrixIn, int nOfRows,
			 int nOfColumns);

    // Retrive the lowest values for each column. Check for redundancy on a column and remove it... The global cost may not be optimal.
    void assignmentSuboptimal2(double *assignment, double *cost,
			       double *distMatrixIn, int nOfRows,
			       int nOfColumns);


    // Hungarian algorithm, with optimal cost. Call following functions.
    void assignmentOptimal(FL * assignment, FL * cost, FL * distMatrix,
			   int nOfRows, int nOfColumns);

    // Sub functions for the hungarian approach.
    void buildAssignmentVector(FL * assignment, bool * starMatrix,
			       int nOfRows, int nOfColumns);
    void computeAssignmentCost(FL * assignment, FL * cost, FL * distMatrix,
			       int nOfRows);
    void step2a(FL * assignment, FL * distMatrix, bool * starMatrix,
		bool * newStarMatrix, bool * primeMatrix,
		bool * coveredColumns, bool * coveredRows, int nOfRows,
		int nOfColumns, int minDim);
    void step2b(FL * assignment, FL * distMatrix, bool * starMatrix,
		bool * newStarMatrix, bool * primeMatrix,
		bool * coveredColumns, bool * coveredRows, int nOfRows,
		int nOfColumns, int minDim);
    void step3(FL * assignment, FL * distMatrix, bool * starMatrix,
	       bool * newStarMatrix, bool * primeMatrix,
	       bool * coveredColumns, bool * coveredRows, int nOfRows,
	       int nOfColumns, int minDim);
    void step4(FL * assignment, FL * distMatrix, bool * starMatrix,
	       bool * newStarMatrix, bool * primeMatrix,
	       bool * coveredColumns, bool * coveredRows, int nOfRows,
	       int nOfColumns, int minDim, int row, int col);
    void step5(FL * assignment, FL * distMatrix, bool * starMatrix,
	       bool * newStarMatrix, bool * primeMatrix,
	       bool * coveredColumns, bool * coveredRows, int nOfRows,
	       int nOfColumns, int minDim);



    // From the output of the asignment, take the most discrimininative ones.
    void searchNBestAssignment(FL discriminant_ratio, FL * distmat,
			       FL * initTransfo, int dim, int *newdim,
			       int *dimmask);


    int hist_sort(FL * sorted, int n, int vdim, int *orderout);

    void setHistogramNormalization(HISTOGRAM_NORMALIZATION hist_norm);


    // Returns nbins 
    int getNumberOfBins();

    // Returns number of built histograms.
    int getDim();

};
#endif

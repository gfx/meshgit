/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
/*
 *  distances.cpp
 *  embedmat
 *
 *  Created by Diana Mateus and David Knossow on 11/14/07.
 *  Copyright 2007 INRIA. All rights reserved.
 *
 */

#include "distances.h"

#define STR_LENGTH 2048

Distances::Distances()
{
	
  nzelems = 0;
  dimx = 0;
  n_nodes = 0;
  depth = 0;
  loaded = false;
	
  X = NULL;
    full_W = NULL;
    sparse_W = NULL;
	
  W = NULL;
    FW = NULL;
  
  C_Nei = NULL;
	

  // when set to false, it means it is a pointer from an other class....
  // and thus should not be released.
  modify_W = true;
  nei_filename = NULL;
	
}

Distances::~Distances()
{
  if (C_Nei)
    delete C_Nei;
	
  if (sparse_W && modify_W) {
    delete [] sparse_W;
    sparse_W = NULL ;
  }
  if (W)
    delete[]W;

    if (FW)
    delete[]FW;
    if (nei_filename)
    delete[]nei_filename;
	
  if (X)
    delete[]X;
}

int Distances::fillNeighborhoodMatrix()
{				
  int l_retval = 0;
  switch (nei_mode) {
			
    
    
  case LOCAL_GEO:
    l_retval = computeLocalGeodesicDistances();
    break;
			
      case FULL_GEO:
    l_retval = computeFullGeodesicDistances();
    break;
    
    
  default:
    WARNING_(fprintf
	     (stderr, "Inexistent neighborhood estimation method\n"));
    break;
  }
  PRINT_(fprintf
	 (stderr, "Neighborhood Filled (%d / %d non zero elements)\n",
	  nzelems, n_nodes * n_nodes));
	
  return l_retval;
	
}




int Distances::computeFullGeodesicDistances()
{
  //first compute connectivity then propagate
		
  PRINT_COLOR_(fprintf(stderr, "Local geodesic distance\n"), RED);
	
  if (C_Nei) {
    delete C_Nei;
    C_Nei = NULL;
  }
  C_Nei = new Neighborhood;
	
  if (!nei_filename) {
    PRINT_COLOR_(fprintf
		 (stderr, "Local geodesic distance from data\n"),
		 GREEN);
    int *l_neighborhood = NULL;
    int *l_num_nei = NULL;
    findNeighbors(l_num_nei, l_neighborhood);
    C_Nei->loadNeighbors(l_num_nei, l_neighborhood);
    if (l_neighborhood)
      delete[]l_neighborhood;
    if (l_num_nei)
      delete[]l_num_nei;
  } else {
    PRINT_COLOR_(fprintf
		 (stderr, "Local geodesic distance from %s .\n",
		  nei_filename), GREEN);
    C_Nei->readNeighbors(nei_filename);
  }
	
  if (C_Nei->getNumOfVoxels() != n_nodes) {
    FATAL_(fprintf(stderr, "Error reading Neighborhood file.\n"
		   "The number of voxels does not correspond to the loaded voxelset "
		   "(loaded %d , neighborhood file %d) \n", n_nodes,
		   C_Nei->getNumOfVoxels()), FATAL_ERROR);
  }
	
  C_Nei->computeDistGraph(depth);
	
  //To avoid confusion remove sparse mat
  //TODO what happens if modify_W is false?
  if (sparse_W && modify_W) {
    delete [] sparse_W;
    sparse_W = NULL;
  }
  if (W && modify_W) {
    delete[]W;
    W = NULL;
  }
	
  n_nodes = C_Nei->getNumOfVoxels();
  int l_nzelems = C_Nei->getNzelems();
	
  //keep connectivity in l_sparse_W until copied to full matrix
	
  FL* l_sparse_W = C_Nei->getWeights();
	
  if (!l_sparse_W) {
    ERROR_(fprintf(stderr, "Empty local geodesic matrix\n"),
	   ERROR_EMPTY_DATA);
  }
		    
  PRINT_(fprintf(stderr, "Full geodesics: Initial geodesic connectivity finished\n"));
	
  //reset full matrix
  if (full_W) {
    delete[]full_W;
    full_W = NULL;
  }
  if (W && modify_W) {
    delete[]W;
    W = NULL;
  }

  //fill full with sparse
  PRINT_(fprintf(stderr,"fill full matrix with connectivity matrix \n"));

  nzelems = n_nodes * n_nodes;
  full_W = new FULL_MAT_TYPE[nzelems];
  FW = allocateMat(full_W, n_nodes, n_nodes);
  memset(full_W, 0, sizeof(FULL_MAT_TYPE) * nzelems );
		
  int idr,idc;
  for(int k=0;k<l_nzelems;k++){
    idr = (int)l_sparse_W[k*3];
    idc = (int)l_sparse_W[k*3+1];
    full_W[idr*n_nodes+idc]=(FULL_MAT_TYPE)l_sparse_W[k*3+2];
  }
  l_sparse_W = NULL;

  /*int* l_n_nei = C_Nei->getNumOfNearestNei();	//get number of nearest neighbors per element;
    int* l_nei = C_Nei->getConnectivity();	//nvoxels * MAX_NUM_NEIG
	
    for (int i=0; i<n_nodes; i++){
    for (int j=0; j<=l_n_nei[i]; j++){
    int k = l_nei[j];
    full_W[i*n_nodes+k] = 1.0;
    DBG3_(fprintf(stderr,"(%d/%d:%d,%d)",j,l_n_nei[i],i, l_nei[j]));
    }
    }*/

	
#ifdef DEBUG_3
  {
    //verify symmetry
    int sum = 0;
    for (int i=0; i < n_nodes; i++){
      FULL_MAT_TYPE* _Wirow = &(full_W[i*n_nodes]);
      for (int j=i+1; j< n_nodes; j++){
	int i_value = (int)_Wirow[j];
	int j_value = (int)full_W[j*n_nodes+i];
	if (i_value > j_value){
	  sum +=(i_value-j_value);
	  DBG3_(fprintf(stderr,"Errsym(%d,%d,%d)\n",i,j,sum));
	}
	else{
	  sum +=(j_value-i_value);
	  DBG3_(fprintf(stderr,"Errsym(%d,%d,%d)\n",i,j,sum));
	}
      }
    }
    DBG_(fprintf(stderr,"Symmetry error %d\n",sum));
  }
#endif

  DBG3_(print_full_matrix(full_W,"FULL_GEO_START_MATRIX", n_nodes, n_nodes));
		
  //Recover sparse form of connectivity for propagation
  FL**l_nei_mat = C_Nei->getSparseGeodesicMatrix();
  int *l_num_nei = C_Nei->getNumOfNei();
	
  if (!l_nei_mat)
    ERROR_(fprintf(stderr, "Empty local geodesic matrix\n"),
	   ERROR_EMPTY_DATA);
  if (!l_num_nei)
    ERROR_(fprintf(stderr, "Empty local number of neighbors\n"),
	   ERROR_EMPTY_DATA);
	
  nzelems = n_nodes * n_nodes;	
	
  //Recursive propagation
  PRINT_(fprintf(stderr,"Recursive propagation\n"));

  //Prepare memory to iterate between current and previous estimation 
  int changed = 0;
  FULL_MAT_TYPE* l_values1 = new FULL_MAT_TYPE[n_nodes];
  FULL_MAT_TYPE* l_values2 = new FULL_MAT_TYPE[n_nodes];

  //For each row expand one neighborhood at a time

  for(int i = 0; i<n_nodes; i++){
    FULL_MAT_TYPE* _Wirow = &(full_W[i*n_nodes]); //pointer to current row
    memcpy (l_values1, _Wirow, n_nodes * sizeof(FULL_MAT_TYPE));
    memset (l_values2,0,n_nodes*sizeof(FL));
    changed += propagate(_Wirow,i,0, l_values1,l_values2, l_num_nei, l_nei_mat);
    //changed += propagate(_Wirow,i,0, l_values1,l_values2, l_n_nei, l_nei);

  }
  PRINT_(fprintf(stderr,"Propagation finished\n"));

  //Free memory
  if( l_values1 ){
    delete[]l_values1;
    l_values1=NULL;
  }
  if( l_values2 ){
    delete[]l_values2;
    l_values2 = NULL;
  }
  DBG_(fprintf(stderr,"%d changes before symmetrization\n",changed));

  //symetrize the matrix
  for (int i=0; i < n_nodes; i++)	{
    FULL_MAT_TYPE* _Wirow = &(full_W[i*n_nodes]); 
    for (int j=0; j< n_nodes; j++){
      int i_value = (int)_Wirow[j];
      int j_value = (int)full_W[j*n_nodes+i];
      if ( j_value != 0 ){
	if (( i_value == 0 )||( j_value < i_value )){
	  _Wirow[j] = (FULL_MAT_TYPE)j_value;
	  DBG3_(fprintf(stderr,"(%d,%d,%d <- %d,%d,%d) ",i,j,i_value,j,i,j_value));
	  changed++;
	}
      }
      else if ( i_value !=0 ){
	if (( j_value == 0 )||(i_value < j_value)){
	  full_W[j*n_nodes+i] = (FULL_MAT_TYPE)i_value;
	  DBG3_(fprintf(stderr,"sym(%d,%d,%d -> %d,%d,%d) ",i,j,i_value,j,i,j_value));
	  changed++;						
	}
      }
    }
  }
  DBG_(fprintf(stderr,"%d changes after symmetrization\n",changed));

#ifdef DEBUG
  {
    //verify symmetry
    int sum = 0 ;
    for (int i=0; i < n_nodes; i++){
      FULL_MAT_TYPE* _Wirow = &(full_W[i*n_nodes]);
      for (int j=i+1; j< n_nodes; j++){
	int i_value = (int)_Wirow[j];
	int j_value = (int)full_W[j*n_nodes+i];
	if (i_value > j_value){
	  sum +=(i_value-j_value);
	  DBG3_(fprintf(stderr,"Errsym(%d,%d,%d)\n",i,j,sum));
	}
	else{
	  sum +=(j_value-i_value);
	  DBG3_(fprintf(stderr,"Errsym(%d,%d,%d)\n",i,j,sum));
	}
      }
    }
    DBG_(fprintf(stderr,"Error in symmetrization %d\n",sum));
  }
#endif
	
#ifdef DEBUG	
  //printout number of zeros
  int l_zeros = 0;
  int l_nonzeros = 0;
  FULL_MAT_TYPE l_max=0;
  for (int i=0; i < n_nodes; i++){
    for (int j=0; j< n_nodes; j++){
      if(full_W[i*n_nodes+j]==0)
	l_zeros++;
      else
	l_nonzeros++;
      if(full_W[i*n_nodes+j]>l_max)
	l_max=full_W[i*n_nodes+j];			
    }
  }
  DBG_(fprintf(stderr, "zeros = %d/%d nonzeros = %d/%d max = %g\n",l_zeros, nzelems, l_nonzeros, nzelems, l_max));
#endif
	
	
  C_Nei->releaseUnused();
  if (C_Nei) {
    delete C_Nei;
    C_Nei = NULL;
  }

  PRINT_(fprintf(stderr, "Finish computing full geodesic matrix\n"));
	
  return 0;
}



int Distances::loadSparseDistance(FL* p_sparse_w, int p_n_elem, int p_nz_elems) {
  if (sparse_W && modify_W) {
    delete [] sparse_W;
    sparse_W = NULL;
  }
  if (W && modify_W) {
    delete[]W;
    W = NULL;
  }
    if (full_W) {
    delete[]full_W;
    full_W = NULL;
  }
    if (W && modify_W) {
    delete[]W;
    W = NULL;
  }
    
  modify_W = false ;
  n_nodes = p_n_elem ;
  nzelems = p_nz_elems ;
  sparse_W = p_sparse_w ;
  W = allocateDMat(sparse_W, nzelems, 3);
  modify_W = false ;

  return 0 ;

}

// Get paramaters from mesh
// mesh::getFLSparseMatWeight();
// mesh::getNumOfNeik();
int Distances::computeFullGeodesicDistances(FL* p_sparse_W,int p_n_elems, int p_nzelems)
{
  //first compute connectivity then propagate
    
  PRINT_COLOR_(fprintf(stderr, "Loading local geodesic distances\n"), RED);
    
  //To avoid confusion remove sparse mat
  //TODO what happens if modify_W is false?
  if (sparse_W && modify_W) {
    delete [] sparse_W;
    sparse_W = NULL;
  }
  if (W && modify_W) {
    delete[]W;
    W = NULL;
  }
    
  //load from paramaters 
  n_nodes = p_n_elems;
  int l_nzelems = p_nzelems;
  FL* l_sparse_W = p_sparse_W;
    
  if (!l_sparse_W) {
    ERROR_(fprintf(stderr, "Empty local geodesic matrix\n"),
	   ERROR_EMPTY_DATA);
  }
    
  PRINT_(fprintf(stderr, "Initial geodesic connectivity loaded with %d point and %d neis\n",n_nodes, l_nzelems));
    
  //reset full matrix
  if (full_W) {
    delete[]full_W;
    full_W = NULL;
  }
  if (W && modify_W) {
    delete[]W;
    W = NULL;
  }
    
  //fill full with sparse
  PRINT_(fprintf(stderr,"fill full matrix with connectivity matrix \n"));
    
  nzelems = n_nodes * n_nodes;
  full_W = new FULL_MAT_TYPE[nzelems];
  FW = allocateMat(full_W, n_nodes, n_nodes);
  memset(full_W, 0, sizeof(FULL_MAT_TYPE) * nzelems);
    
  int idr,idc;
  for(int k=0;k<l_nzelems;k++){
    idr = (int)l_sparse_W[k*3];
    idc = (int)l_sparse_W[k*3+1];
    full_W[idr*n_nodes+idc]=(FULL_MAT_TYPE)l_sparse_W[k*3+2];
  }
    
    
#ifdef DEBUG
  //verify symmetry
  int sum = 0 ;
  for (int i=0; i < n_nodes; i++){
    FULL_MAT_TYPE* _Wirow = &(full_W[i*n_nodes]);
    for (int j=i+1; j< n_nodes; j++){
      int i_value = (int)_Wirow[j];
      int j_value = (int)full_W[j*n_nodes+i];
      if (i_value > j_value){
	sum +=(i_value-j_value);
	DBG3_(fprintf(stderr,"Errsym(%d,%d,%d)\n",i,j,sum));
      }
      else{
	sum +=(j_value-i_value);
	DBG3_(fprintf(stderr,"Errsym(%d,%d,%d)\n",i,j,sum));
      }
    }
  }
  DBG_(fprintf(stderr,"Symmetry error %d\n",sum));
#endif
    
    
  //Recover sparse form of connectivity for propagation
  //Build matrix assuming data is ordered
  FL**l_nei_mat = new FL*[n_nodes];
  int *l_num_nei = new int[n_nodes];
  for(int i = 0; i < n_nodes; i++){
    l_nei_mat[i] = NULL;
    l_num_nei[i] = 0;
  }
    
  //extract number of neighbors
  //asume elements are ordered and no lonely items exist
  //count neighbors per line and point matrix to each line
  int l_current_row=0;
  l_nei_mat[0] = l_sparse_W;
  int l_count = 0;
  for(int l = 0; l < l_nzelems*3; l=l+3){
    int i = (int)l_sparse_W[l];
    if (l_current_row != i){
      if (l_count>0){
	l_nei_mat[i] = &(l_sparse_W[l]);
	l_num_nei[l_current_row] = l_count;
	l_current_row = i;
	l_count=1;
      }
    }else
      l_count++;
  }
  l_num_nei[n_nodes-1] = l_count;
	
#ifdef DEBUG_3
  //for(int l = 0; l < l_nzelems; l=l+3)
  //	fprintf(stderr,"(%d,%d,%g)",(int)l_sparse_W[l],(int)l_sparse_W[l+1],l_sparse_W[l+2]);
	
  int l_nei_count =0;
  for(int i = 0; i < n_nodes; i++){
    l_nei_count= l_nei_count+l_num_nei[i];
    if(l_num_nei[i]==0)
      fprintf(stderr,"Lonely element %d \n",i);
  }
  if (l_nei_count!=l_nzelems)
    fprintf(stderr,"There is an error the sum of neighborhoods should be equal to the number of nzelems (nz:%d nei:%d)",l_nzelems,l_nei_count);

  for(int l = 0; l < l_nzelems;){
    int i = (int)l_sparse_W[l*3];

    for (int j =0; j < l_num_nei[i]; j++,l++){
      fprintf(stderr,"(%d %d %g:%d,%d,%g)",(int)l_sparse_W[l*3],(int)l_sparse_W[l*3+1],l_sparse_W[l*3+2],
	      (int)l_nei_mat[i][j*3],(int)l_nei_mat[i][j*3+1],l_nei_mat[i][j*3+2]);
    }
    getchar();
  }
#endif

  if (!l_nei_mat)
    ERROR_(fprintf(stderr, "Empty local geodesic matrix\n"),
	   ERROR_EMPTY_DATA);
  if (!l_num_nei)
    ERROR_(fprintf(stderr, "Empty local number of neighbors\n"),
	   ERROR_EMPTY_DATA);
	
  nzelems = n_nodes * n_nodes;	
	
  //Recursive propagation
  PRINT_(fprintf(stderr,"Recursive propagation\n"));

  //Prepare memory to iterate between current and previous estimation 
  int changed = 0;
  FULL_MAT_TYPE* l_values1 = new FULL_MAT_TYPE[n_nodes];
  FULL_MAT_TYPE* l_values2 = new FULL_MAT_TYPE[n_nodes];

  //For each row expand one neighborhood at a time

  for(int i = 0; i<n_nodes; i++){
    FULL_MAT_TYPE* _Wirow = &(full_W[i*n_nodes]); //pointer to current row
    memcpy (l_values1, _Wirow, n_nodes*sizeof(FULL_MAT_TYPE));
    memset (l_values2,0,n_nodes*sizeof(FULL_MAT_TYPE));
    changed += propagate(_Wirow, i, 0, l_values1, l_values2, l_num_nei, l_nei_mat);
    //changed += propagate(_Wirow,i,0, l_values1,l_values2, l_n_nei, l_nei);

  }
	
  PRINT_(fprintf(stderr,"Propagation finished\n"));

  //Free memory
  if( l_values1 ){
    delete[]l_values1;
    l_values1=NULL;
  }
  if( l_values2 ){
    delete[]l_values2;
    l_values2 = NULL;
  }
  DBG_(fprintf(stderr,"%d changes before symmetrization\n",changed));

  //symetrize the matrix
  for (int i=0; i < n_nodes; i++)	{
    FULL_MAT_TYPE* _Wirow = &(full_W[i*n_nodes]); 
    for (int j=0; j< n_nodes; j++){
      int i_value = (int)_Wirow[j];
      int j_value = (int)full_W[j*n_nodes+i];
      if (j_value!=0){
	if ((i_value==0)||(j_value < i_value)){
	  _Wirow[j] = (FULL_MAT_TYPE)j_value;
	  DBG3_(fprintf(stderr,"(%d,%d,%d <- %d,%d,%d) ",i,j,i_value,j,i,j_value));
	  changed++;
	}
      }
      else if (i_value!=0.0){
	if ((j_value==0.0)||(i_value < j_value)){
	  full_W[j*n_nodes+i] = (FULL_MAT_TYPE)i_value;
	  DBG3_(fprintf(stderr,"sym(%d,%d,%d -> %d,%d,%d) ",i,j,i_value,j,i,j_value));
	  changed++;						
	}
      }
    }
  }
  DBG_(fprintf(stderr,"%d changes after symmetrization\n",changed));

#ifdef DEBUG
  //verify symmetry
  for (int i=0; i < n_nodes; i++){
    FULL_MAT_TYPE* _Wirow = &(full_W[i*n_nodes]);
    for (int j=i+1; j< n_nodes; j++){
      int i_value = (int)_Wirow[j];
      int j_value = (int)full_W[j*n_nodes+i];
      if (i_value > j_value){
	sum +=(i_value-j_value);
	DBG3_(fprintf(stderr,"Errsym(%d,%d,%d)\n",i,j,sum));
      }
      else{
	sum +=(j_value-i_value);
	DBG3_(fprintf(stderr,"Errsym(%d,%d,%d)\n",i,j,sum));
      }
    }
  }
  DBG_(fprintf(stderr,"Error in symmetrization %d\n",sum));
#endif
	
#ifdef DEBUG	
  //printout number of zeros
  int l_zeros = 0;
  int l_nonzeros = 0;
  FULL_MAT_TYPE l_max=0;
  for (int i=0; i < n_nodes; i++){
    for (int j=0; j< n_nodes; j++){
      if(full_W[i*n_nodes+j]==0)
	l_zeros++;
      else
	l_nonzeros++;
      if(full_W[i*n_nodes+j]>l_max)
	l_max=full_W[i*n_nodes+j];			
    }
  }
  DBG_(fprintf(stderr, "zeros = %d/%d nonzeros = %d/%d max = %g\n",l_zeros, nzelems, l_nonzeros, nzelems, l_max));
#endif

  DBG3_(print_full_matrix(full_W,"FULL_GEO_MATRIX", n_nodes, n_nodes));
	
  if (l_nei_mat)
    delete[]l_nei_mat;
  l_nei_mat=NULL;
  if(l_num_nei)
    delete[]l_num_nei;
  l_num_nei=NULL;

  PRINT_(fprintf(stderr, "Finish computing full geodesic matrix\n"));
	
  return 0;
}



int Distances::propagate(FULL_MAT_TYPE* _Wirow,int row,int count_last_added, FULL_MAT_TYPE* values1, FULL_MAT_TYPE* values2, int* nei_n, FL** nei_mat)
{
  //values1 contains the entries of the matrix who where last modified and need to be propagated
  //values2 should be free and is used here to fill it with the results of the propagation
	
  int new_count = 0;
	
  //fprintf(stderr,"row %d: ",i);
  for(int j = 0; j<n_nodes; j++){
    if (j!=row){
      if (values1[j]>0){
	//if j was added to the neighbors of i, try to propage the neighbors of i(row) with the neighbors of j
	FULL_MAT_TYPE dist_ij = _Wirow[j];
	//look for the neighbors of j
	FL* _Wjrow = nei_mat[j];
	for (int l=0; l < nei_n[j] ; l++, _Wjrow+=3){
	  //for (int l=0; l < nei_n[j] ; l++){
	  if ((int)_Wjrow[0]!=j)
	    fprintf(stderr,"Error reading neighbors in SparseGeodesicMatrix(%d,%d):(%d,%d)",row,j,(int)_Wjrow[0],(int)_Wjrow[1]);
	  int k = (int)_Wjrow[1];						
	  //nt k = nei_mat[l]; 
	  if(row!=k){
	    FULL_MAT_TYPE dist_jk = (FULL_MAT_TYPE)_Wjrow[2];
	    //FL dist_jk = (FL)1.0;
	    FULL_MAT_TYPE dist_ik = dist_ij + dist_jk;
	    if ((_Wirow[k]==0.0) || (_Wirow[k] > dist_ik)){
	      //fprintf(stderr,"(%d,%d,%g->%g) ",i,k,_Wirow[k],dist_ik);
	      //if first time changed or found a better value
	      //keep in a separate vector the result for next propagation
	      if ((values2[k]==0.0)||(values2[k] > dist_ik)){
		//fprintf(stderr,"(%d,%d,%g->%g) ",row,k,values2[k],dist_ik);
		values2[k]=dist_ik;
		new_count++;
	      }
	    }
	  }
	}
	//once propagated reset values1 to zero;
	values1[j]=0.0;
      }
    }
  }
	
  //verify values1 is empty
#ifdef DEBUG
  FULL_MAT_TYPE sum = 0.0;
  for (int j=0; j < n_nodes; j++){
    if (values1[j] != 0.0){
      sum += values1[j];
      DBG_(fprintf(stderr,"In row %d entry %d of values1 is %g instead of zero\n",row,j,values1[j]));	
    }
  }
  if (sum !=0.0 )
    DBG_(fprintf(stderr,"This vector should be empty: %g\n",sum));	
#endif	
	
  int ret_val = 0;
  if(new_count == 0){
    DBG3_(fprintf(stderr,"Finished propagating row %d\n", row));
    ret_val = count_last_added;
  }
  else{
    //copy result to full_W
    for (int k = 0; k < n_nodes; k++){
      if (values2[k]>0.0){
	//We have only stored the values to change if next condition is valid there is a problem in the code
	if (( values2[k] > _Wirow[k]) && (_Wirow[k] !=0.0))
	  DBG_(fprintf(stderr,"Propagation Error: This condition should never arrive\n"));
	DBG3_(fprintf(stderr,"copying(%d,%d,%g->%g) \n",row,k,_Wirow[k],values2[k]));
	//Definitively copy values to original matrix					
	_Wirow[k] = values2[k];
      }
    }
    // Recursive call to propagate
    ret_val = propagate(_Wirow,row,new_count,values2,values1,nei_n,nei_mat);
		
    // Keep record of how many changes have been made
    ret_val+=count_last_added;
  }
		
  return 	ret_val;
}





int Distances::computeLocalGeodesicDistances()
{
	
  PRINT_COLOR_(fprintf(stderr, "Local geodesic distance\n"), RED);
	
  if (C_Nei) {
    delete C_Nei;
    C_Nei = NULL;
  }
  C_Nei = new Neighborhood;
	
  if (!nei_filename) {
    PRINT_COLOR_(fprintf
		 (stderr, "Local geodesic distance from data\n"),
		 GREEN);
    int *l_neighborhood = NULL;
    int *l_num_nei = NULL;
    findNeighbors(l_num_nei, l_neighborhood);
    C_Nei->loadNeighbors(l_num_nei, l_neighborhood);
    if (l_neighborhood)
      delete[]l_neighborhood;
    if (l_num_nei)
      delete[]l_num_nei;
  } else {
    PRINT_COLOR_(fprintf
		 (stderr, "Local geodesic distance from %s .\n",
		  nei_filename), GREEN);
    C_Nei->readNeighbors(nei_filename);
  }
	
  if (C_Nei->getNumOfVoxels() != n_nodes) {
    FATAL_(fprintf(stderr, "Error reading Neighborhood file.\n"
		   "The number of voxels does not correspond to the loaded voxelset "
		   "(loaded %d , neighborhood file %d) \n", n_nodes,
		   C_Nei->getNumOfVoxels()), FATAL_ERROR);
  }
	
	
  C_Nei->computeDistGraph(depth);
	
	
  if (sparse_W && modify_W) {
    delete[]sparse_W;
    sparse_W = NULL;
  }
  if (W && modify_W) {
    delete[]W;
    W = NULL;
  }
	
  n_nodes = C_Nei->getNumOfVoxels();
  nzelems = C_Nei->getNzelems();
	
	
  sparse_W = C_Nei->getWeights();
  modify_W = false;
	
  if (!sparse_W) {
    ERROR_(fprintf(stderr, "Empty local geodesic matrix\n"),
	   ERROR_EMPTY_DATA);
  }
	
  W = allocateDMat(sparse_W, nzelems, 3);
	
  C_Nei->releaseUnused();
	
  PRINT_(fprintf(stderr, "Local geodesic distances finished\n"));
	
	
  return 0;
}


void Distances::findNeighbors(int *&numNei, int *&neighbors)
{
  //Assumes integer voxel values
  //preprocess if it is not the case
  PRINT_(fprintf(stderr, "Initial connectivity from data\n"));
	
	
  numNei = new int[n_nodes];
  memset(numNei, 0, n_nodes * sizeof(int));
  int l_max_num_nei ;
  HASH_MAP_PARAM_(int, "max_num_neig", Int, l_max_num_nei ) ;

  neighbors = new int[l_max_num_nei * n_nodes];
  memset(numNei, 0, l_max_num_nei * n_nodes * sizeof(int));
	
  //first find limits in the three dimensions;
  int *l_min = new int[dimx];
  int *l_max = new int[dimx];
  FL *l_X_ = X;
  for (int i = 0; i < n_nodes; i++) {
    for (int d = 0; d < dimx; d++, ++l_X_) {
      if (i == 0 || *l_X_ < l_min[d])
	l_min[d] = *l_X_;
      if (i == 0 || *l_X_ > l_max[d])
	l_max[d] = *l_X_;
    }
  }
	
  //for each voxel find if each posible neighbor is filled
  l_X_ = X;
  int *l_v1 = new int[dimx];
  int *l_v2 = new int[dimx];
  int l_item;
  for (int i = 0; i < n_nodes; i++, l_X_ += dimx) {
    for (int d = 0; d < dimx; d++) {
      l_v1[d] = (int) l_X_[d];
    }
    //for each dimension look +1/-1 elements (6 nei based)
    for (int d = 0; d < dimx; d++) {
      memcpy(l_v2, l_v1, dimx * sizeof(int));
      l_v2[d] = l_v1[d] + 1;
      l_item = search(l_v2);
      if (l_item >= 0) {
	if (numNei[i] + 1 >= l_max_num_nei)
	  std::
	    cout <<
	    "Ignoring neighbor, not enough space allocated, change MAX_NUM_NEIG"
		 << std::endl;
	else {
	  neighbors[numNei[i] * l_max_num_nei] = l_item;
	  numNei[i]++;
	}
				
      }
      //repeat with -1
      memcpy(l_v2, l_v1, dimx * sizeof(int));
      l_v2[d] = l_v1[d] - 1;
      l_item = search(l_v2);
      if (l_item >= 0) {
	if (numNei[i] + 1 >= l_max_num_nei)
	  std::
	    cout <<
	    "Ignoring neighbor, not enough space allocated, change MAX_NUM_NEIG"
		 << std::endl;
	else {
	  neighbors[numNei[i] * l_max_num_nei] = l_item;
	  numNei[i]++;
	}
      }
    }
		
  }
  if (l_v1)
    delete[]l_v1;
  if (l_v2)
    delete[]l_v2;
	
  if (l_min)
    delete[]l_min;
  if (l_max)
    delete[]l_max;
	
	
}

int Distances::search(int *p_v)
{
	
  for (int i = 0; i < n_nodes; i++) {
    for (int d = 0; d < dimx; d++) {
      if (p_v[d] != (int) X[i * dimx + d])
	continue;
    }
    return i;
  }
  return -1;
}

int Distances::squareDistMat()
{
    if ((!sparse_W) && (!full_W)) {
    ERROR_(fprintf(stderr, "Can not square empty matrix\n"),
	   ERROR_EMPTY_DATA);
  }
  	
  if (sparse_W) {
    for (int i = 0; i < nzelems; i++) {
      W[i][2] = (W[i][2]) * (W[i][2]);
    }
  }
    else if (full_W) {
    for (int i = 0; i < n_nodes * n_nodes; i++) {
      full_W[i] = (full_W[i]) * (full_W[i]);
    }
  }
    return 0;
}


int Distances::loadData(FL * p_data, int p_n, int p_dimx)
{
	
  if (p_data == NULL) {
    ERROR_(fprintf
	   (stderr,
	    "Distances: Trying to load with an empty pointer\n"),
	   ERROR_EMPTY_DATA);
  }
	
  n_nodes = p_n;
  dimx = p_dimx;
  PRINT_(fprintf
	 (stderr,
	  "Distances: Loading data with %d points and %d dimensions\n",
	  n_nodes, dimx));
	
  if (X) {
    delete[]X;
    X = NULL;
  }
  X = new FL[n_nodes * dimx];
  memset(X, 0, n_nodes * dimx * sizeof(FL)) ;

  FL *l_mean = new FL[dimx];
  FL *l_minv = new FL[dimx];
  FL *l_maxv = new FL[dimx];
  FL *l_delta = new FL[dimx];
  for (int d = 0; d < dimx; d++)
    l_mean[d] = 0.0;
	
  for (int d = 0; d < dimx; d++) {
    l_minv[d] = X[d];
    l_maxv[d] = X[d];
  }
  for (int i = 0; i < n_nodes; i++) {
    for (int d = 0; d < dimx; d++) {
      X[i * dimx + d] = p_data[i * dimx + d];
      l_mean[d] += X[i * dimx + d];
      if (X[i * dimx + d] < l_minv[d])
	l_minv[d] = X[i * dimx + d];
      if (X[i * dimx + d] > l_maxv[d])
	l_maxv[d] = X[i * dimx + d];
    }
  }
	
  for (int d = 0; d < dimx; d++) {
    l_mean[d] = l_mean[d] / n_nodes;
    l_delta[d] = l_maxv[d] - l_minv[d];
  }
	
  FL l_scale = l_delta[0];
  for (int d = 1; d < dimx; d++) {
    if (l_delta[d] > l_scale)
      l_scale = l_delta[d];
  }
	
  //center & scale to enhance conditioning
  PRINT_(fprintf
	 (stderr,
	  "-->Centering and (scaling) input data for numerical estability\n"));
  for (int i = 0; i < n_nodes; i++) {
    for (int d = 0; d < dimx; d++) {
      X[i * dimx + d] -= l_mean[d];
    }
  }
  loaded = true;
	
  PRINT_(fprintf(stderr, "-->Data loaded\n"));
  delete[]l_mean;
  delete[]l_minv;
  delete[]l_maxv;
  delete[]l_delta;
  return 0;
}

FL* Distances::getData(){
	return X;
}

int Distances::getDimData(){
	return dimx;
}




int Distances::setParameters(NEIGHBORHOOD p_mode, int p_K, FL p_eps,
			     int p_depth)
{
  nei_mode = p_mode;
  K = p_K;
  epsilon = p_eps;
  depth = p_depth;
	
	
  return 0;
}

int Distances::setNeiFilename(char *filename)
{
  if (!filename) {
    ERROR_(fprintf(stderr, "Empty filename\n"), ERROR_OPEN_FILE);
  }
  if (nei_filename) {
    delete[]nei_filename;
    nei_filename = NULL;
  }
  nei_filename = new char[STR_LENGTH];
  strcpy(nei_filename, filename);
	
  return 0;
}

FL *Distances::getDistMatrix()
{
	
  if (W) {
    switch (nei_mode) {
          case LOCAL_GEO:
      return sparse_W;
      break;
            
          case FULL_GEO:
      FATAL_(std::cout << "getFullDistVector should be called to get FULL dist matrix" <<std::endl, FATAL_BAD_FUNC_CALL) ; 
      break;
      
    default:
      ERROR_(fprintf
	     (stderr,
	      "The method to built the distance matrix is not known\n"),
	     NULL);
    }
  } else
    ERROR_(fprintf(stderr, "Distance matrix W hasn't been filled\n"),
	   NULL);
  return NULL;
	
}

FULL_MAT_TYPE *Distances::getFullDistVector(){
  if ((sparse_W) && (full_W)) {
    ERROR_(fprintf(stderr, "Both sparse and Full matrix are filled\n"),
	   NULL);
  }
  if (full_W) {
    return full_W;
  }
  else {
    ERROR_(fprintf(stderr, "Distance matrix W hasn't been filled\n"),
	   NULL);
  }
}

FL *Distances::getSparseDistVector()
{
    if ((sparse_W) && (full_W)) {
    ERROR_(fprintf(stderr, "Both sparse and Full matrix are filled\n"),
	   NULL);
  }
  	
  if (sparse_W) {
    return sparse_W;
  }
  else {
    ERROR_(fprintf(stderr, "Sparse Distance matrix W hasn't been filled\n"),
	   NULL);
  }
}

bool Distances::isLoaded()
{
  return loaded;
}

bool Distances::isSparse()
{
  if (sparse_W)
    return true;
  else
    return false;
}

int Distances::getNumOfNodes()
{
  return n_nodes;
}

int Distances::getNzelems()
{
  return nzelems;
}

int Distances::getK()
{
  return K;
}

FL Distances::getEpsilon()
{
  return epsilon;
}

int Distances::getDepth()
{
  return depth;
}

NEIGHBORHOOD Distances::getNeiMode()
{
  return nei_mode;
}



#if 0
#endif

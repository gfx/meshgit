/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
/*
 *  GEigs.cpp
 *  embed
 *
 *   Code adapted from:
 *   Scot Shaw
 *   30 August 1999
 *
 */



// Begin with some standard include files.


#include <cmath>
#include <iostream>
#include <fstream>
#include <iomanip>

//Accelerate framework contains altivec framework which
//in turn contains lapack and blas
#ifdef MACOSX
#include  <Accelerate/Accelerate.h>
#endif


#include "generalized_eigs.h"
#include "utilities.h"
#include <fenv.h>




GEigs::GEigs()
{

    Tvec = NULL;
    T = NULL;			//Left hand side SPARSE matrix
    M = NULL;			//Right hand side DIAGONAL matrix

    tlen = 0;
    eig_vecs = NULL;
    eig_vals = NULL;

    //parameters intialization
    mat_size = 0;
    num_of_eig_vec = 0;
    strcpy(bmat, "I");		// Av = lv non generalized decomposition
    strcpy(which, "LM");	//look for the largest eigenvalues
    tol = 0.0;
    rvec = 1;			//also calculate eigenvectors

    modifyT = 0;		// avoids deleteing allocations done outside the class
    //put to 1 whenever creating a copy

    modifyM = 0;		// avoids deleteing allocations done outside the class
    //put to 1 whenever creating a copy
}

GEigs::~GEigs()
{

    if ((M) && (modifyM)) {
	delete[]M;
	M = NULL;
    }

    if ((T) && (modifyT)) {
	delete[]T;
	T = NULL;
    }

    if ((Tvec) && (modifyT)) {
	delete[]Tvec;
	Tvec = NULL;
    }

    if (eig_vecs) {
	delete[]eig_vecs;
	eig_vecs = NULL;
    }
    if (eig_vals) {
	delete[]eig_vals;
	eig_vals = NULL;
    }
}


void GEigs::release()
{

    if ((M) && (modifyM)) {
	delete[]M;
    }

    if ((T) && (modifyT)) {
	delete[]T;
    }

    if ((Tvec) && (modifyT)) {
	delete[]Tvec;
    }

    if (eig_vecs) {
	delete[]eig_vecs;
    }
    if (eig_vals)
	delete[]eig_vals;
}

int GEigs::dsaupd()
{
    if (!T) {
	std::
	    cout <<
	    "Can not perform eigen-decomposition, matrix has not yet been loaded "
	    << std::endl;
	return 1;
    }


    if (!M) {
	if (!strcmp(bmat, "G")) {
	    std::
		cout <<
		"Can not perform eigen-decomposition, right hand matrix M has not yet been loaded "
		<< std::endl;
	    return 1;
	}
    }


    if (eig_vals) {
	delete[]eig_vals;
	eig_vals = NULL;
    }

    if (eig_vecs) {
	delete[]eig_vecs;
	eig_vecs = NULL;
    }
    // Re-Allocate values 
    
    eig_vals = new double[num_of_eig_vec];
    memset(eig_vals,0, num_of_eig_vec * sizeof(double)) ;
    eig_vecs = new double[mat_size * num_of_eig_vec];
    memset(eig_vecs,0, mat_size * num_of_eig_vec * sizeof(double)) ;

    PRINT_COLOR_(fprintf(stderr, "Calling dsaupd \n"), RED);

    int ido = 0;		/* Initialization of the reverse communication parameter. */

    double *resid;
    resid = new double[mat_size];

    int ncv = 4 * num_of_eig_vec;	/* The largest number of basis vectors that will
					   be used in the Implicitly Restarted Arnoldi
					   Process.  Work per major iteration is
					   proportional to N*NCV*NCV. */
    if (ncv > mat_size)
	ncv = mat_size;

    double *v;
    int ldv = mat_size;
    v = new double[ldv * ncv];
    memset(v, 0, ldv * ncv * sizeof(double)) ;


    int *iparam;
    iparam = new int[11];	/* An array used to pass information to the routines
				   about their functional modes. */
    iparam[0] = 1;		// Specifies the shift strategy (1->exact)
    iparam[2] = 3 * mat_size;	// Maximum number of iterations
    iparam[6] = 2;		/* Sets the mode of dsaupd.
				   1 is exact shifting,
				   2 is user-supplied shifts,
				   3 is shift-invert mode,
				   4 is buckling mode,
				   5 is Cayley mode. */


    int *ipntr;
    ipntr = new int[11];	/* Indicates the locations in the work array workd
				   where the input and output vectors in the
				   callback routine are located. */

    double *workd;
    workd = new double[3 * mat_size];

    double *workl;
    workl = new double[ncv * (ncv + 8)];

    int lworkl = ncv * (ncv + 8);	/* Length of the workl array */

    int info = 0;		/* Passes convergence information out of the iteration
				   routine. */

    int *select;
    select = new int[ncv];
    double *d;
    d = new double[2 * ncv];	/* This vector will return the eigenvalues from */
    memset(d, 0, 2 * ncv * sizeof(double)) ; /* the second routine, dseupd. */
    double sigma;
    int ierr;


    /* Here we enter the main loop where the calculations are
       performed.  The communication parameter ido tells us when
       the desired tolerance is reached, and at that point we exit
       and extract the solutions. */
    if (!strcmp(bmat, "G")) {
	do {
	    dsaupd_(&ido, bmat, &mat_size, which, &num_of_eig_vec, &tol,
		    resid, &ncv, v, &ldv, iparam, ipntr, workd, workl,
		    &lworkl, &info);
	    //     std::cout << "Computing "<< ido << std::endl ;
	    if ((ido == 1) || (ido == -1)) {
		av(mat_size, workd + ipntr[0] - 1, workd + ipntr[1] - 1);
		memcpy(workd + ipntr[0] - 1, workd + ipntr[1] - 1, mat_size * sizeof(double));	// copy workd+ipntr[1]-1 in workd+ipntr[0]-1
		invMAv(mat_size, workd + ipntr[1] - 1);	// !!!!!!! IN PLACE FUNCTION   // M^-1 * workd+ipntr[1]-1 in  workd+ipntr[1]-1              
	    }

	    else if (ido == 2) {
		mv(mat_size, workd + ipntr[0] - 1, workd + ipntr[1] - 1);	// performs M*x
	    }
	} while ((ido == 1) || (ido == -1) || (ido == 2));
    } else if (!strcmp(bmat, "I")) {
	do {
	    dsaupd_(&ido, bmat, &mat_size, which, &num_of_eig_vec, &tol,
		    resid, &ncv, v, &ldv, iparam, ipntr, workd, workl,
		    &lworkl, &info);
	    if ((ido == 1) || (ido == -1)) {
		av(mat_size, workd + ipntr[0] - 1, workd + ipntr[1] - 1);
	    }

	} while ((ido == 1) || (ido == -1));
    } else {
	std::cout << "Unsuported mode for decomposition" << std::endl;
	return 1;
    }


    /* From those results, the eigenvalues and vectors are
       extracted. */

    if (info < 0) {
	std::cout << "Error with dsaupd, info = " << info << "\n";
	std::cout << "Check documentation in dsaupd\n\n";
    } else {
	dseupd_(&rvec, "All", select, d, v, &ldv, &sigma, bmat,
		&mat_size, which, &num_of_eig_vec, &tol, resid, &ncv, v,
		&ldv, iparam, ipntr, workd, workl, &lworkl, &ierr);

	if (ierr != 0) {
	    std::cout << "Error with dseupd, info = " << ierr << "\n";
	    std::cout << "Check the documentation of dseupd.\n\n";
	} else if (info == 1) {
	    std::cout << "Maximum number of iterations reached.\n\n";
	} else if (info == 3) {
	    std::cout << "No shifts could be applied during implicit\n";
	    std::cout << "Arnoldi update, try increasing NCV.\n\n";
	}

	/* Before exiting, we copy the solution information over to
	   the arrays of the calling program, then clean up the
	   memory used by this routine.  For some reason, when I
	   don't find the eigenvectors I need to reverse the order of
	   the values. */
    }



    if (rvec == 0) {		// copy eigenvalues
	FATAL_(std::cout << "Reversing of the eigenvalues is not yet implemented" <<std::endl, FATAL_UNKNOWN_MODE) ;
    } else if (rvec == 1) {	// copy both

#ifdef DEBUG_3
	std::cout << "Not Reversing" <<std::endl ;
#endif
	for (int i = 0; i < num_of_eig_vec; i++) {
	    eig_vals[i] = d[i];
	}
	for (int i = 0; i < num_of_eig_vec; i++) {
	    for (int j = 0; j < mat_size; j++) {
		eig_vecs[j * num_of_eig_vec + i] = v[i * mat_size + j];
	    }
	}
    }

    delete[]resid;
    delete[]v;
    delete[]iparam;
    delete[]ipntr;
    delete[]workd;
    delete[]workl;
    delete[]select;
    delete[]d;

    PRINT_COLOR_(fprintf(stderr, "dsaupd finished \n"), RED);

    return 0;
}

void GEigs::av(int p_n, double *p_in, double *p_out)
{

    bzero(p_out, p_n * sizeof(double));
    int i = 0;
    while (i < tlen) {
	p_out[(int) T[i][0]] += p_in[(int) T[i][1]] * T[i][2];
	++i;
    }
}



void GEigs::invMAv(int p_n, double *p_Ax)
{

//  performs : y <-- inv(M) * A * x

    // perform inM^(-1)*inAx
    // overwrite inAx
    int i = 0;
    while (i < p_n) {
	if (M[i] > 1e-12) {
	    p_Ax[i] = p_Ax[i] / M[i];
	} else {

	    DBG_(std::
		cerr << "D[" << i <<
		"] < 1e-12 : We still have disconnected componnents in the VoxelGrid "
		 << std::endl);
	}
	++i;
    }
}


void GEigs::mv(int p_n, double *in, double *p_out)
{

//performs : y <-- M * x
    bzero(p_out, p_n * sizeof(double));
    int i = 0;
    while (i < p_n) {
	p_out[i] = M[i] * in[i];
	++i;
    }
}

int GEigs::setParams(int p_nev, char *p_which, char *p_bmat, int evec_on)
{

    int l_retval = 0;
    if (p_nev > mat_size) {
	std::
	    cout << "Requested number of eigenvalues " << p_nev <<
	    " is bigger than the matrix size " << mat_size << ". Set to n"
	    << std::endl;
	p_nev = mat_size;
	l_retval = 1;
    }
    if (p_nev <= 0) {
	std::
	    cout << "Requested number of eigenvalues is <= 0. Set to 0 " <<
	    std::endl;
	p_nev = 0;
	l_retval = 1;
    }
    num_of_eig_vec = p_nev;

    //copy which
    strcpy(bmat, p_bmat);	// case I :Av = lv non generalized decomposition
    // case G : Av = lBv generalized decomposition

    //copy bmat
    if ((strcmp(p_which, "SM")) && (strcmp(p_which, "LM")) && (strcmp(p_which, "SA")) && (strcmp(p_which, "LA"))) {
	std::
	    cout << "Requested case " << p_which <<
	    " not implemented. Set to LM" << std::endl;
	strcpy(which, "LM");
	l_retval = 1;
    } else
	strcpy(which, p_which);

    //estimate or not the eigenvectors
    if ((evec_on != 0) && (evec_on != 1)) {
	std::cout << "evec_on should be either 1 or 0. Set to 1" << std::
	    endl;
	evec_on = 1;
	l_retval = 1;
    } else
	rvec = evec_on;

    return l_retval;
}

FL *GEigs::getEigVecs()
{
    return eig_vecs;
}

FL *GEigs::getEigVals()
{
    return eig_vals;
}

int GEigs::getNumOfEigVec()
{
    return num_of_eig_vec;
}

int GEigs::getMatSize()
{
    return mat_size;
}

char *GEigs::getWhich()
{
    return which;
}

char *GEigs::getBMat()
{
    return bmat;
}



int GEigs::loadSparseMatrix(int p_n, int p_tlen, double **p_T)
{
    // Use externally initialized matrix
    // Avoid deallocating
    if (!p_T) {
	std::cout << "GEigs::load_sparse_matrix() Empty matrix" << std::
	    endl;
	return 1;
    }

    PRINT_(std::
	   cout << "GEigs::Loading Matrix of size " << p_n << " nz " <<
	   p_tlen << std::endl);

    if ((Tvec) && (modifyT)) {
	delete[]Tvec;
	Tvec = NULL;
    }
    if ((T) && (modifyT)) {
	delete[]T;
	T = NULL;
    }

    mat_size = p_n;
    tlen = p_tlen;
    T = p_T;
    modifyT = 0;		//prevent to write on the memory allocated outside the class (avoid freeing p_T)






#ifdef DEBUG_FULL
    std::cout << "Sparse Matrix: " <<std::endl ;
    for (int i = 0; i < tlen; i++) {
	std::cout << p_T[i][0] << " " << p_T[i][1] << " " << p_T[i][2] <<std::endl ;
    }

    FL  l_temp_mat[p_n * p_n] ;
    memset(l_temp_mat,0, p_n * p_n * sizeof(FL)) ;
    for (int i = 0; i < tlen; i++) {
	l_temp_mat[ (int)p_T[i][0] * p_n + (int)p_T[i][1]] = p_T[i][2] ;
    }
    for (int i = 0; i < p_n; i++) {
	for (int j = 0; j < p_n; j++) {
	    if (l_temp_mat[i* p_n +j]) {
		PRINT_COLOR_(fprintf(stderr, "%.04f ", l_temp_mat[i* p_n +j] ), RED) ;
	    }
	    else {
		fprintf(stderr, "%.04f ", l_temp_mat[i* p_n +j] ) ;
	    }
	}
	std::cout << std::endl ;
    }
    std::cout << std::endl ;
#endif
#ifdef DEBUG_3 
    {
    FL l_temp_mat[p_n * p_n] ;
    memset(l_temp_mat, 0, p_n * p_n * sizeof(FL)) ;
    
    for (int i = 0; i < tlen; i++) {
	l_temp_mat[ (int)p_T[i][0] * p_n + (int)p_T[i][1]] = p_T[i][2] ;
    }
    for (int i = 0; i < p_n; i++) {
	for (int j = 0; j < p_n; j++) {
	  if ( l_temp_mat[i* p_n +j] != l_temp_mat[j * p_n + i ] )
	    std::cout << std::setprecision (20) << "Mat is not sym on " << i << " " << j << " (" <<l_temp_mat[i* p_n +j] << " , " <<l_temp_mat[j * p_n + i ] << ") " <<   std::endl ;
	}
    }
    fflush(stdout) ;
    fflush(stdin) ;
    getchar() ;
    delete [] l_temp_mat ;
    }
#endif


    return 0;
}

int GEigs::loadSparseMatrix(int p_n, int p_tlen, int **p_T)
{
    // Use externally initialized matrix
    // But copying because in int and not double format

    if ((Tvec) && (modifyT)) {
	delete[]Tvec;
	Tvec = NULL;
    }
    if ((T) && (modifyT)) {
	delete[]T;
	T = NULL;
    }
    //Reinit sizes and matrix T 
    mat_size = p_n;
    tlen = p_tlen;
    Tvec = new double[tlen * 3];
    T = allocateDMat(Tvec, tlen, 3);
    double *tTvec = Tvec;

    for (int i = 0; i < tlen; i++) {
	*tTvec = (double) p_T[i][0];
	*(tTvec + 1) = (double) p_T[i][1];
	*(tTvec + 2) = (double) p_T[i][2];
	tTvec += 3;
    }

    modifyT = 1;
    return 0;
}

int GEigs::loadSparseMatrix(int p_n, int p_tlen, float **p_T)
{
    // Use externally initialized matrix
    // But copying because in int and not double format

    if ((Tvec) && (modifyT)) {
	delete[]Tvec;
	Tvec = NULL;
    }
    if ((T) && (modifyT)) {
	delete[]T;
	T = NULL;
    }
    //Reinit sizes and matrix T 
    mat_size = p_n;
    tlen = p_tlen;
    Tvec = new double[tlen * 3];
    T = allocateDMat(Tvec, tlen, 3);

    double *l_Tvec = Tvec;

    for (int i = 0; i < tlen; i++) {
	*l_Tvec = (double) p_T[i][0];
	*(l_Tvec + 1) = (double) p_T[i][1];
	*(l_Tvec + 2) = (double) p_T[i][2];
	l_Tvec += 3;
    }

    modifyT = 1;
    return 0;
}





int GEigs::loadDiagonalMatrix(int p_n, float *p_M)
{
    // Use externally initialized matrix
    // But copying because in int and not double format

    if ((M) && (modifyM)) {
	delete[]M;
	M = NULL;
    }
    //Reinit sizes and matrix M 
    if (p_n != mat_size) {
	std::
	    cout << "Load left side matrix before the diagonal matrix" <<
	    std::endl;
    }

    M = new double[mat_size];
    for (int i = 0; i < mat_size; i++) {
	*(M + i) = (double) *(p_M + i);
    }
    modifyM = 1;
    return 0;
}


int GEigs::loadDiagonalMatrix(int p_n, int *p_M)
{
    // Use externally initialized matrix
    // But copying because in int and not double format

    if ((M) && (modifyM)) {
	delete[]M;
	M = NULL;
    } else {
	std::cout << "M cannot be deleted...." << std::endl;
    }
    //Reinit sizes and matrix M 
    if (p_n != mat_size) {
	std::
	    cout <<
	    "T may not be intialized ... should be before the diagonal matrix"
	    << std::endl;
    }

    M = new double[mat_size];

    for (int i = 0; i < mat_size; i++) {
	*(M + i) = (double) *(p_M + i);
    }
    modifyM = 1;
    return 0;
}


int GEigs::loadDiagonalMatrix(int p_n, double *p_M)
{

    PRINT_(std::cout << "GEigs::Loading the diagonal matrix" << std::endl);

    // Use externally initialized matrix
    // But copying because in int and not double format

    if ((M) && (modifyM)) {
	delete[]M;
	M = NULL;
    }
    //Reinit sizes and matrix M 
    if (p_n != mat_size) {
	std::
	    cout <<
	    "T may not be intialized ... should be before the diagonal matrix"
	    << std::endl;
    }

    M = new double[mat_size];
    memcpy(M, p_M, mat_size * sizeof(double));
    modifyM = 1;

    return 0;
}

/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
/*
 *  types.h
 *  
 *
 *  Created by Diana Mateus and David Knossow on 11/1/07.
 *  Modified by Avinash Sharma on 11/12/08.
 *  Copyright 2007 INRIA. All rights reserved.
 *
 * In this file we collected all definitions and enums used in the different codes
 */

#ifndef TYPES_H
#define TYPES_H

#define CV_FL 6
// We work with double precision. Working with float might possible, but the compatibility has not been checked
#define FL double
#define FULL_MAT_TYPE float



// Structure to store a single element in a sparse matrix.
typedef struct {
    int i;
    int j;
    FL value;
} SparseStorage;



// Types of voxels to be displayed. Generaly, we use VOXEL.
enum VOXEL_TYPE {
   
    VOXEL,                      // Reads voxels from a file
    VOXEL_EMBED,                // Reads voxels and embeddings from a file
    GRID                        // Reads Matlab voxel grid.
};

// Type of data to be display
enum DISPLAY {
    POINT_DISP,			// Draw shape as points
    VOXEL_DISP,			// Draw voxels as squares
    LABEL_DISP,			// Draw voxels with labeled colors
    EMBED_DISP,			// Show embeddings
    EIGENFUNCTION_DISP,		// Show the voxels/meshes with colors suited to eigenfunctions
    GRAPH_DISP,			// Show the connectivity between points for a small amount of points
        MESH_POINT_DISP,		// Show meshes as points.
        };


// USEFUL FOR LAPRLS APPROACH TO BUILD K*L MATRIX
// USEFUL FOR MESH TO BUILD THE DISTANCE MATRIX

// Store values for Hashtable. Glib hash tables can only store pointer values. 
// The structure stores the value, and the hash table stores the pointer to the structure.
typedef struct {
    FL prev_val;
    FL val;
    int i ;
    int j ;
} HashTable_value;

// In visualization part, it allows to select for the kind of data
// to display. 
enum DISPMATCH {
    SHAPE3D,			// Voxels/Meshes
    EMBED,			// Embedings
    ANIMATE_MATCH,		// show groups of matches of size set_size
        INTERPOLATE_VIEW           // Rotate the view around an axis
};



// Used in embedmat. Select the type of edges used to compute
// the embeddings.
enum EDGES {
    TOPOLOGICAL,		// Replace the distance between nodes of the graph by 1
    GAUSSIAN,			// Compute distance between two nodes of the graphs and take the exponential of the distance
    NONE			// The weight matrix is made of the distances
};

enum METHOD {
        LLE = 0,                        // Computes the LLE
        LAP = 1,			// Laplacian eigenmap
        ISOMAP = 2,                     // Computes the isomaps
    };


// When using the laplacian eigenmap to perform the embedding, 
// different matrices can be decomposed
enum LAPLACIAN {
    NJW,			// D^(-1/2)(D-W)D^(-1/2)v = lv : M = D^(-1/2)(D-W)D^(-1/2)
    SHIMEILA,			// Wv = lDv : M = W
    EIGENMAPS,			// (D-W)v = lDv : M = D-W
    ADJACENCY,			// Wv = lv : M = W
    SIMPLE_EMAP,		// (D-W)v = lv : M=D-W 
    DIFFUSION_DISTANCE		// D^(-1)(W)v = lv : M = D^(-1/2)(W)D^(-1/2)
};



// Normalization factor for the eigenvectors
enum NORMALIZATION_FORMAT {
    NO_NORM,			// No normalization
    INV_SQRT_LAMBDA,		// 1/sqrt(eigenvalue)
    SQRT_LAMBDA,		// sqrt(eigenvalue)
    LAMBDA			// eigenvalue. Should be used in the case of DIFFUSION_MAP
};


enum NORMALIZATION_EMBEDDING {
    NO_NORM_EMBED,	         // No normalization
    NORM_MAX,		         // embed / max(abs(embed))
    NORM_UNIT,		         // Sum_{j} {embed_{i,j}} = 1. Each point lies on a unit sphere.
};

// Normalization of the histogram (horizontal scale). Scale the vectors before building the histograms
enum HISTOGRAM_NORMALIZATION {
    GLOBAL_NORM,		// Take the min and max over all eigenvectors (both shapes) and normalize 
    INDIVIDUAL_NORM,		// Take the min and max over all eigenvectors for EACH shape and normalize
    CENTER_NORM			// Center the histograms around zero (takes MAX( fabs(Min), fabs(Max)))
};

// Distance type between histograms
enum HISTOGRAM_DISTANCE {
    BINTOBIN,			// Compute brutal Bin to Bin difference. 
                                // For different translations, different scales, and
                                // take the lowest distance
    #ifdef USE_DIFFUSION_DISTANCE
    DIFFUSION,			// Compute diffusion distance between histograms
    #endif
    #ifdef USE_EMDL1_DISTANCE
    EMDL1,			// Compute EMD with the L1 norm
    #endif
    H_TEST_ALL
};

// Once distance are computed, performs the "hungarian"
// approach to complete the assignment
enum ASSIGNMENT_METHOD {
    OPTIMAL,
    SUBOPTIMAL1,
    SUBOPTIMAL2,
    FABIO,
    TEST_ALL
};


// Method to initialize the EM algorithm
enum INIT_MODE {
    ALPHA,			// A file containing matrix alpha (weight matrix) must be provided 
    TRANSFO,			// A file containing the initial Permutation/Sign flip matrix is provided
    UNIFORM,			// The matching is uniformly distributed over the embeddings.
    MATCH,			// A file with initial matching is provided
    EFUNC,			// Use the eignevectors to initialize the matching
    EIGVAL,                      // Use the eigenvalues ordering to initialize the matching.
    //START_REMOVE_FOR_RELEASE_FIEDLER
    //Added by AVINASH
    FIEDLER
    //END_REMOVE_FOR_RELEASE
};

// 
enum EM_MODE {
    ISO_ANNEAL = 0,		        // Weights of the matrix are :  norm_fact * exp(sigma2 * dist2)
    GLOBAL_COV = 1,			// Take the covariance into account.
        USE_ESTIMATE = 3,
    };

enum SIGMA_INIT_TYPE {
    FROM_XY,
    FROM_XYT,
    FROM_NN,
    FROM_WYT
};


// Steps of the EM algorithm. Aim is to facilitate the jumps between the steps.
enum EM_STEP {
    E_STEP,
    M_STEP,
    EVAL,
    NONE_EM
};

// What kind of transformation to compute in the M step.
enum TRANSFO_MODE {
    ROTATION_ONLY,
    ORTHOGONAL,
    ROT_AND_TRANS,
    ORTH_AND_TRANS,
    };


enum NEIGHBORHOOD {
            LOCAL_GEO=2,			// Compute the distance between the nodes that are connected at a given connectivity depth.
            FULL_GEO=4
    };


enum GRAPH_DISTANCE_TYPE {
    EUCLIDIAN,                  // Distances between the nodes are taken as the euclidean distance.
    NORMALIZED_EUCLIDIAN,       // Distances between the nodes are taken as the euclidean distance normalized 
                                // by the Voronoi area of cells associated to both nodes
    COTAN_WEIGHT,               // Distances between the nodes are taken as the cotan-weights.
    NORMALIZED_COTAN_WEIGHT     // Distances between the nodes are taken as the cotan-weights normalized 
                                // by the Voronoi area of cells associated to both nodes
} ;


enum FATAL_CODE {
    FATAL_MEMORY_ALLOCATION = 10,
    FATAL_UNKNOWN_MODE,
    FATAL_ILL_COND,
    FATAL_ERROR,
    FATAL_UNKNOWN_FORMAT,
    FATAL_OPEN_FILE,
    FATAL_VARIABLE_INIT,
    FATAL_BAD_OPTION,
    FATAL_BAD_FUNC_CALL
};


enum ERROR_CODE {
    ERROR_OPEN_FILE = 100,
    ERROR_EMPTY_DATA,
    ERROR_MEMORY_ALLOCATION,
    ERROR_VARIABLE_INIT,
    ERROR_UNKNOWN_MODE,
    ERROR_ILL_COND,
    ERROR_BAD_OPTION
};






#endif

/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
/*
 *  match_em_line.cpp
 *  
 *
 *  Created by Diana Mateus and David Knossow on 9/23/07.
 *  Copyright 2007 INRIA. All rights reserved.
 *
 * Functions provided in this file allows to compute the point registration using less 
 * memory than the standard approach. The alpha matrix is not allocate but only line
 * per line computation is done.
 * For other comments reffer to match_em.cpp
 */

#include <cmath>
#include <stdio.h>
#include <math.h>
#include <cv.h>
#include <highgui.h>

#include <iostream>


#ifndef MACOSX
#include <omp.h>
#endif


#include "match_em.h"

#include "timer.h"

using namespace std;


//------------------------------------------------------------------------------------
// EM functions (line per line version)
//------------------------------------------------------------------------------------

int Match::lineEM(INIT_MODE p_init_m, EM_MODE p_em_m, int *p_int_arg,
		  FL * p_fl_arg)
{

    DBG3_(fprintf(stderr, "Match::lineEM()\n"));

    em_mode = p_em_m;

    checkEmMode() ;

    initEM(p_init_m, p_int_arg, p_fl_arg);

    PRINT_COLOR_(fprintf(stderr, " lineEM() LOOP\n"), RED);

    int l_run = 1;
    while (l_run) {
	switch (step) {
	case E_STEP:
	    if ( !lineE() )
		step = M_STEP ;
	    else
		l_run = 0 ;
	    break;
	case M_STEP:
	    lineM();
	    step = EVAL;
	    printMatches();
	    break;
	case EVAL:
	    l_run = evalTermination();
	    step = E_STEP;
	    break;
	case NONE_EM:
	    //for intialization when no E, M or Eval is needed (starts then with E)
	    step = E_STEP;
	    break;
	default:
	    FATAL_(fprintf(stderr, "step should be 'M' or 'E'\n"),
		   FATAL_UNKNOWN_MODE);
	    break;
	}
    }

    PRINT_COLOR_(fprintf(stderr, "EM() line: END\n"), RED);

    printOutputs();

        return 0;
}



int Match::initSigma( )
{
    //outlier = K_OUT*pow(sigma,dim); //Radu's proposition
    //outlier_term = (1-nclass*pow(sigma,dim)/K_OUT); // if sigma_out changing with sigma_in
    //outlier_term = (factorial(dim/2)/pow(sigma,dim/2.0))*(1.0 - nclass/K_OUT); //factorial only valid for odd dimensions
    //outlier_term = K_OUT * factorial(dim/2)/pow(sigma,dim/2.0);

    outlier_term = k_out;

    nClass = nY;

    if (iter > 0) {
	nClass -= n_unmatched[iter - 1];
    }

    if (!sigma_initialized) {
	if (!use_alpha_line) {
	    updateSigma() ;
	}
	else {
	    lineUpdateSigma () ;
	}
    }
    //Sigma is allready sqarred.
    sigma2 = sigma ;
    norm_fact = 1.0;		//1.0/pow(2.0*M_PI*sigma,dim/2.0);          


    PRINT_(fprintf(stderr, "Iso_sigma:"
		   "\n norm %g (max value of the probability density function)\n "
		   "sigma %g sigma2 %g \n", norm_fact, sigma, sigma2));


    // The sign - is put latter in the exponential...
    sigma2 = 0.5 / sigma2;


    DBG_(fprintf(stderr, "End of init_iso_anneal() \n"));

    return 0;
}














int Match::lineE()
{

    int l_retval = 0;
    iter++;

    PRINT_COLOR_(fprintf(stderr, "\nlineE(): Iteration %d \n", iter), RED);

    PRINT_COLOR_(fprintf(stderr, "-->lineE()\n"), RED);

    //reset part and beta
    bzero(part, nX * sizeof(FL));
    bzero(beta, nY * sizeof(FL));

    //reset match
    for (int i = 0; i < nX * 2; i++)
	ag_match[i] = -1;

    //reset masks
    memset(mask_X, 0, nX * sizeof(int));
    memset(mask_Y, 0, nY * sizeof(int));

    //INIT allocations and constant values for iteration

    FL *l_distv = NULL;
    CvMat *l_dummy = NULL;
    norm_fact = 1 ;


    switch (em_mode) {

    case ISO_ANNEAL:
    case USE_ESTIMATE:
	l_retval += initIsoAnneal( ) ;
	computeAlpha = &Match::computeAlphaijFromEuclideanDistance ;
	break;

    case GLOBAL_COV:
	l_retval += initGlobalCov( l_distv );
	computeAlpha = &Match::computeAlphaijFromCovariance ;
	break;

	    default:
	FATAL_(fprintf
	       (stderr, "lineE(): not supported EM_MODE %d \n", em_mode),
	       FATAL_UNKNOWN_MODE);
	break;

    }

    if (l_retval) {
	ERROR_(fprintf(stderr, " EM LINE initialization FAILED\n"),
	       ERROR_VARIABLE_INIT);
    }

    DBG_(fprintf(stderr, "Allocate alpha row of %d elements\n", nY));

    FL *l_alpha_row = NULL;
    l_alpha_row = new FL[nY];
    memset(l_alpha_row, 0 , nY * sizeof(FL)) ;

    //Begin with the line per line functions
    int l_outliers = 0;
    sum_beta = 0.0;
    int l_unmatched = 0;

    //Fill alpha per rows;
    FL *l_x_idx, *l_y_idx;
    l_x_idx = X;

    for (int i = 0; i < nX;
	 i += line_sample, l_x_idx += data_dim * line_sample) {

	if (data_mask_X[i] != 0) {

	    bzero(l_alpha_row, nY * sizeof(FL));

	    //Compute the alpha value for line i
	    l_y_idx = Yt;
	    for (int j = 0; j < nY; j++, l_y_idx += data_dim) {

		if (data_mask_Y[j] == 0) {
		    continue;
		}
		l_alpha_row[j] = (this->*computeAlpha)(l_x_idx, l_y_idx, l_distv) ;

		part[i] += l_alpha_row[j];
	    }
	}


	// Normalization of the line
	FL l_probasum = 0.0;	//row sum after normalization

	if (part[i] < zero_th) {
	    mask_X[i] = 0;	// to prevent dividing by zero
	    l_outliers++;
	} else {
	    mask_X[i] = 1;
	    FL l_out_norm = 1.0 / (part[i] + outlier_term);
	    for (int j = 0; j < nY; j++) {
		if (data_mask_Y[j] == 0)
		    continue;
		l_alpha_row[j] *= l_out_norm;
		l_probasum += l_alpha_row[j];
	    }
	    if (l_probasum < out_th) {
		mask_X[i] = 0;
		l_outliers++;
	    }
	}

	// Ready for M
	// Beta sum 
	// fill ag_match (used in visualmatch)
	if (mask_X[i]) {
	    ag_match[i * 2] = i;
	    FL l_max = 0.0;
	    for (int j = 0; j < nY; j++) {
		if (!data_mask_Y[j])
		    continue;
		beta[j] += l_alpha_row[j];
		if (l_alpha_row[j] > l_max) {
		    l_max = l_alpha_row[j];
		    ag_match[i * 2 + 1] = j;
		}
	    }
	}
#ifdef PRINT
	// Print line results
	if (i % 1000 == 0) {
	    fprintf(stderr, "part[%d] = %g\n", i, part[i]);
	    fprintf(stderr,
		    "probasum[%d]= %g probability of class being an inlier \n",
		    i, l_probasum);
	    fprintf(stderr, "match[%d]= %d\n", i, ag_match[i * 2 + 1]);
	}
#endif

    }

    DBG_(fprintf(stderr, "Summarize outliers and unmatched\n"));


    // Summarize outliers
    n_outliers[iter] = l_outliers;
    PRINT_COLOR_(fprintf
		 (stderr, "Number of outliers: %d\n", n_outliers[iter]),
		 GREEN);

    // Summarize unmatched
    for (int j = 0; j < nY; j++) {
	if (beta[j] < beta_th) {
	    mask_Y[j] = 0;
	    l_unmatched++;
	} else {
	    mask_Y[j] = 1;
	    sum_beta += beta[j];
	}
    }

    n_unmatched[iter] = l_unmatched;
    PRINT_COLOR_(fprintf
		 (stderr, "Number of unmatched points: %d\n",
		  n_unmatched[iter]), GREEN);

    if (l_alpha_row) {
	delete[]l_alpha_row;
	l_alpha_row = NULL;
    }

    if (l_distv) {
	delete[]l_distv;
	l_distv = NULL;
    }

    if (l_dummy) {
	cvReleaseMat(&l_dummy);
    }

    match_available = true;

    return l_retval;
}










FL Match::lineUpdateSigma() {

    PRINT_(std::cout << "Computing line sigma" <<std::endl );

    FL l_sigma = 0 ;
    FL l_dist = 0 ;
    FL l_diff = 0 ;

    FL l_probasum = 0 ;

    FL *l_distv = NULL;

    FL* l_alpha_row = NULL ;
    l_alpha_row = new FL[nY];
    memset(l_alpha_row, 0 , nY * sizeof(FL)) ;

    int l_outliers = 0 ;
    FL *l_x_idx, *l_y_idx;
    l_x_idx = X;

    for ( int i = 0; i < nX; i += line_sample, l_x_idx += data_dim * line_sample ) {
	if (data_mask_X[i] != 0) {

	    bzero(l_alpha_row, nY * sizeof(FL));

	    //Compute the alpha value for line i
	    l_y_idx = Yt;
	    if (sigma_initialized) {
		for (int j = 0; j < nY; j++, l_y_idx += data_dim) {    
		    if (data_mask_Y[j] == 0) {
			continue;
		    }		    
		    l_alpha_row[j] = (this->*computeAlpha)(l_x_idx, l_y_idx, l_distv) ;
		    
		    part[i] += l_alpha_row[j];
		    
		}
	    }
	    else {
		for (int j = 0; j < nY; j++) {    
		    l_alpha_row[j] = 1.0 / nY ;
		}
		    part[i] = 1 ;
	    }
	    if (part[i] < zero_th) {
		//		std::cout << "In sigma, outlier detected" <<std::endl ;
		mask_X[i] = 0;	// to prevent dividing by zero
		
		l_outliers++;
	    } else {
		mask_X[i] = 1 ;
		FL l_out_norm = 1.0 / (part[i] + outlier_term);
		for (int j = 0; j < nY; j++) {
		    if (data_mask_Y[j] == 0)
			continue;
		    l_alpha_row[j] *= l_out_norm;
		    l_probasum += l_alpha_row[j];
		}	
	    }
	    l_y_idx = Yt;	    	    
	    for (int j = 0; j < nY; j++, l_y_idx += data_dim) {
		l_dist = 0 ;
		for (int d = 0; d < data_dim; d++) {
		    l_diff = *(l_x_idx + d) - *(l_y_idx + d) ;
		    l_dist += l_diff * l_diff ;
		}
		l_sigma += l_alpha_row[j] * l_dist ;
	    }
	}
    }

    std::cout << "Nb of outliers : " << l_outliers << " " << l_sigma << " " << l_probasum << std::endl ;

    l_sigma = l_sigma / ( data_dim * l_probasum ) ;
    
    sigma_initialized = true ;
    
    return l_sigma ;
    
}







int Match::evalBestMatch(int p_i, FL p_val, int *&p_best_id,
			 FL * &p_best_val, int &p_worst_id,
			 FL & p_worst_val)
{


    p_best_id[p_worst_id] = p_i;
    p_best_val[p_worst_id] = p_val;
    p_worst_val = p_val;

    //find the lowest value in the best array to replace next time
    for (int k = 0; k < n_max_match; k++) {
	if (p_best_id[k] == -1) {	//point worst_id to empty elements 
	    p_worst_id = k;
	    p_worst_val = -1.0;
	    break;
	}
	if (p_best_val[k] < p_worst_val) {	//point to lowest
	    p_worst_id = k;
	    p_worst_val = p_best_val[k];
	}
    }
    return 0;
}

int Match::fillNAgMatch(int p_j, int *p_best_id, FL * p_best_val)
{

    int *l_b_id = new int[n_max_match];
    memcpy(l_b_id, p_best_id, n_max_match * sizeof(int));
    FL *l_b_val = new FL[n_max_match];
    memcpy(l_b_val, p_best_val, n_max_match * sizeof(FL));

    int idx = p_j * 2 * n_max_match;
    for (int l = 0; l < n_max_match; l++, idx += 2) {
	int l_bi = -1;
	FL l_bval = -1.0;
	for (int k = 0; k < n_max_match; k++) {
	    if (l_b_val[k] > l_bval) {
		l_bi = k;
		l_bval = l_b_val[k];
	    }
	}
	if (l_bi == -1) {
	    nag_match[idx] = p_j;
	    nag_match[idx + 1] = -1;
	} else {
	    nag_match[idx] = p_j;
	    nag_match[idx + 1] = l_b_id[l_bi];
	    l_b_val[l_bi] = -1;
	    l_b_id[l_bi] = -1;
	}
    }

    match_available = true;

    delete[]l_b_id;
    delete[]l_b_val;

    return 0;
}




// C-Function provided to g_hash_table_new_full in build_dist_mat.
// It allows to free the memory allocated for the hash table.
void DeleteHashTable(gpointer p_data)
{

    if (p_data) {
	delete(HashTable_value *) p_data;
    }
    p_data = NULL;

}



int Match::lineM()
{

    PRINT_COLOR_(fprintf(stderr, "lineM(): Iteration %d \n", iter), RED);

    int l_retval = 0;
    FL *l_distv = NULL;
    CvMat l_distv_;
    CvMat *l_dummy = NULL;
    l_distv = new FL[data_dim];
    l_distv_ = cvMat(1, data_dim, CV_FL, l_distv);
    l_dummy = cvCreateMat(1, data_dim, CV_FL);

    DBG_(fprintf(stderr, "Allocate alpha col of %d elements\n", nX));

    FL *l_alpha_col = NULL;
    l_alpha_col = new FL[nX];

    //prepare baricenters
    bzero(W, nY * data_dim * sizeof(FL));

    //prepare best match
    if (!nag_match)
	nag_match = new int[nY * n_max_match * 2];
    for (int k = 0; k < 2 * n_max_match * nY; k++)
	nag_match[k] = -1;

    FL *l_best_val = NULL;
    l_best_val = new FL[n_max_match];
    int *l_best_id = NULL;
    l_best_id = new int[n_max_match];



    GHashTable **l_hash_obs_to_class = new GHashTable *[nX];
    for (int i = 0; i < nX; ++i) {
	l_hash_obs_to_class[i] =
	    g_hash_table_new_full(NULL, NULL, NULL,
				  (GDestroyNotify) DeleteHashTable);
    }


    PRINT_COLOR_(fprintf
		 (stderr,
		  "Find cluster contributions (W: baricenters) and update match\n"),
		 GREEN);

    //Reloop over alpha elements in a column-wise fashion
    FL *l_x_idx, *l_y_idx, *l_w_idx;
    l_y_idx = Yt;
    l_w_idx = W;
    for (int j = 0; j < nY; j++, l_y_idx += data_dim, l_w_idx += data_dim) {
	if (mask_Y[j] == 0) {
	    continue;
	}
	//reset column
	bzero(l_alpha_col, nX * sizeof(FL));

	//initialize match
	for (int k = 1; k < n_max_match; k++) {
	    l_best_id[k] = -1;
	    l_best_val[k] = -1.0;
	}
	FL l_worst_val = -1.0;
	int l_worst_id = 0;


	//Recalculate columns of alpha to approximate W
	l_x_idx = X;
	for (int i = 0; i < nX; i++, l_x_idx += data_dim) {
	    if (mask_X[i] == 0)
		continue;
	    norm_fact = 1.0 / (part[i] + outlier_term);
	    l_alpha_col[i] = (this->*computeAlpha)(l_x_idx, l_y_idx, l_distv ) ;


	    //Find cluster contributions
	    FL l_factor = (l_alpha_col[i]) / (beta[j]);
	    for (int d = 0; d < data_dim; d++)
		*(l_w_idx + d) += *(l_x_idx + d) * l_factor;

	    //update match
	    if (l_alpha_col[i] > l_worst_val) {
		l_retval +=
		    evalBestMatch(i, l_alpha_col[i], l_best_id, l_best_val,
				  l_worst_id, l_worst_val);
	    }
	}

	// for each class store all associated obs.
	l_retval += fillNAgMatch(j, l_best_id, l_best_val);


	// Given all the selected observations, select the best associated class.
	l_retval +=
	    fillOneToOneMatchHash(j, l_best_id, l_best_val,
				  l_hash_obs_to_class);
#ifdef PRINT
	if (j % 1000 == 0) {
	    fprintf(stderr, "mask[%d]: %d W[%d]: ", j, mask_Y[j], j);
	    for (int d = 0; d < data_dim; d++)
		fprintf(stderr, "%g ", W[j * data_dim + d]);
	    fprintf(stderr, "\nmatches for[%d] : ", j);
	    int idx = j * n_max_match * 2;
	    for (int k = 0; k < n_max_match; k++, idx += 2)
		fprintf(stderr, "%d ", nag_match[idx + 1]);
	    fprintf(stderr, "\n");
	}
#endif

    }



    getBestOnetoOneAssignment(l_hash_obs_to_class);


    if (l_hash_obs_to_class) {
	for (int i = 0; i < nX; ++i) {
	    g_hash_table_destroy(l_hash_obs_to_class[i]);
	}
	delete[]l_hash_obs_to_class;
    }


    if (l_alpha_col) {
	delete[]l_alpha_col;
	l_alpha_col = NULL;
    }
    if (l_best_id) {
	delete[]l_best_id;
	l_best_id = NULL;
    }
    if (l_best_val) {
	delete[]l_best_val;
	l_best_val = NULL;
    }
    if (l_distv) {
	delete[]l_distv;
	l_distv = NULL;
    }
    if (l_dummy) {
	cvReleaseMat(&l_dummy);
    }
    //Find Transformation

    PRINT_COLOR_(fprintf(stderr, "-->Find and apply transformation\n"), RED);

    //weighted = 1;
    //centered = 0; 
    //rot_only = 0;     
    switch (transfo_mode) {
    case ROTATION_ONLY:
    case ORTHOGONAL:
    case ROT_AND_TRANS:
    case ORTH_AND_TRANS:
	l_retval += huang(weighted, centered, rot_only);
	l_retval += transform();
	break;
	    }


    error[iter] = estimateError();
    l_retval += computeMCovariance();

    return l_retval;
}




int Match::fillOneToOneMatchHash(int p_class_idx, int *p_bestAssObsIdx,
				 FL * p_bestAssObsProb,
				 GHashTable ** p_obs_to_class)
{


    DBGout << p_class_idx << " (" << n_max_match << ") ";


    for (int i = 0; i < n_max_match; ++i) {
	if (p_bestAssObsProb[i] == -1) {
	    continue;
	}
	HashTable_value *l_value = new HashTable_value;
	l_value->val = p_bestAssObsProb[i];
	DBGout << p_bestAssObsIdx[i] << " ";

	g_hash_table_insert(p_obs_to_class[p_bestAssObsIdx[i]],
			    GINT_TO_POINTER(p_class_idx), l_value);
    }
    DBGout << std::endl;
    return 0;
}




int Match::getBestOnetoOneAssignment(GHashTable ** p_obs_to_class)
{

    GList *l_tempKey;
    GList *l_tempValues;

    if (best_one_to_one_match)
	delete[]best_one_to_one_match;

    best_one_to_one_match = new int[2 * nX];
    for (int i = 0; i < nX; ++i) {
	GList *l_keys;
	GList *l_values;
	l_keys = g_hash_table_get_keys(p_obs_to_class[i]);
	l_values = g_hash_table_get_values(p_obs_to_class[i]);

	l_tempKey = l_keys;
	l_tempValues = l_values;
	FL l_best_val = 0;
	int l_best_id = -1;

	while (l_tempKey) {
	    int j = GPOINTER_TO_INT(l_tempKey->data);

	    HashTable_value *l_temp =
		(HashTable_value *) (l_tempValues->data);

	    if (l_best_val < l_temp->val) {

		l_best_val = l_temp->val;
		l_best_id = j;
	    }

	    l_tempKey = l_tempKey->next;
	    l_tempValues = l_tempValues->next;
	}

	best_one_to_one_match[2 * i] = i;
	best_one_to_one_match[2 * i + 1] = l_best_id;
	g_list_free(l_keys);
	g_list_free(l_values);
    }
    return 0;
}




int Match::setLineSample(int p_val)
{
    line_sample = p_val;
    return 0;
}

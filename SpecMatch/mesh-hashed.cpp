/*
 *  mesh.cpp
 *  SpecMatch
 *
 *  Created by Diana Mateus and David Knossow on 8/16/07.
 *  The NEI manager had been re-written on March 2009.
 *  Copyright 2007 INRIA. All rights reserved.
 *
 */

#include <stdio.h>

#include "mesh-hashed.h"
#include "circulator.h"


// hash_map is made of pairs.
// it.first returns the key, 
// it->second return the data


// For compatibility issues, we keep the vertex, color and approx_voronoi_area arrays.
// Should disappear soon.

using namespace std;

Mesh::Mesh()
{


  center_mesh = true;

  NEI_colormap = NULL;

  vertex = NULL;
  vertex_normal = NULL;
  vertex_color = NULL;

  MESHNEISET = -1;

  nvertex = 0;
  nfacets = 0;
  nedges = 0;

  total_num_nei = NULL;

  weights = NULL;
  mat_weights = NULL;


  vec_depth = NULL;
  mat_depth = NULL;


  approx_voronoi_area = NULL;

  graph_distance_type = EUCLIDIAN ;

  use_depth_as_dist = false;
  scale = 1;


  revert_index_sort_mesh = NULL ;

  enable_sort_mesh = false ;


  hash_map_vertices.clear () ;


}



Mesh::~Mesh()
{
  //Delete array of vertices 
  releaseData();
  // vertex points on a pointer called points that is release in Release_Data.
  vertex = NULL;


  if (revert_index_sort_mesh)
    delete [] revert_index_sort_mesh ;

  if (NEI_colormap)
    delete[]NEI_colormap;




  hash_map_vertices.clear () ;
  hash_map_facets.clear () ;


  
  if (approx_voronoi_area) {
    delete[]approx_voronoi_area;
    approx_voronoi_area = NULL;
  }
  

  if (weights) {
    delete [] weights;
    weights = NULL;
  }

  if (mat_weights) {
    delete[]mat_weights;
    mat_weights = NULL;
  }

  if (vec_depth) {
    delete[]vec_depth;
    vec_depth = NULL;
  }

  if (mat_depth) {
    delete[]mat_depth;
    mat_depth = NULL;
  }

  if (total_num_nei) {
    delete[]total_num_nei;
    total_num_nei = NULL;
  }

}


void Mesh::setDepthAsDist(bool p_status)
{

  use_depth_as_dist = p_status;

}


void Mesh::setGraphDistanceType(GRAPH_DISTANCE_TYPE p_graph_dist)
{

  graph_distance_type = p_graph_dist ;

}

int Mesh::readOffFile(char *p_offFileName)
{

  char l_next_string[256];


  init () ;


  in.open(p_offFileName);

  setColordepth(color_depth);

  if (! in.is_open() ) {
    FATAL_(fprintf(stderr, "Could not open %s \n", p_offFileName),
	   FATAL_OPEN_FILE) ;
  }

  while (in.peek() == ' ' || in.peek() == '\n')
    in.get();

  while (in.peek() == '#') {
    in.getline(l_next_string, 256);	// skip comment
  }

  in >> l_next_string;
  if (!strcmp(l_next_string, "OFF")) {
    PRINT_(std::cout << "Loading OFF file " << p_offFileName << std::
	   endl);
    readOff();
  } else if (!strcmp(l_next_string, "NOFF")) {
    PRINT_(std::
	   cout << "Loading NOFF file (with normals) " << p_offFileName
	   << std::endl);
    readNOff();
  } else if (!strcmp(l_next_string, "COFF")) {
    PRINT_(std::
	   cout << "Loading COFF file (with colors) " << p_offFileName
	   << std::endl);
    readCOff();
  } else {
    FATAL_(fprintf
	   (stderr, "%s : Unknown OFF file format \n", l_next_string),
	   FATAL_UNKNOWN_FORMAT);
  }



  findMeanAndScale(center_mesh);


  hash_map_vertices.clear () ;
  hash_map_vertices.resize( nvertex ) ;
  hash_map_facets.resize( nfacets ) ;

  MapVertex map_mesh ;
  for (int i = 0; i < nvertex; ++i) {
    map_mesh.id = i ;
    map_mesh.unsorted_id = i ;
    map_mesh.exists = true ;
    map_mesh.dist = 0 ;
    map_mesh.prev_dist = 0 ;
    map_mesh.depth = 1 ;
    map_mesh.voronoi_area = 1 ;
    map_mesh.X = vertex[3 * i + 0];
    map_mesh.Y = vertex[3 * i + 1];
    if ( dim == 3 ) {
      map_mesh.Z = vertex[3 * i + 2];
    }
    map_mesh.vertex_normal = NULL ;
    hash_map_vertices[i].first = map_mesh ;
  }

  readFacets();

  if (enable_sort_mesh) {
    PRINT_COLOR_(std::cout << "Sort the mesh..." <<std::endl, RED) ;
    sortMeshY () ;
  }


  fillFacetNormals();

  in.close();

  PRINT_(std::cout << "Compute Neighborhoods and Distances" << std::
	 endl) ;


  computeNeis();


  if (!use_depth_as_dist) {
    switch (graph_distance_type) {
    case EUCLIDIAN:
      PRINT_COLOR_(std::cout << "Computing the euclidian distance between the vertices of the mesh" <<std::endl , GREEN) ;
      computeNormalizedEuclidianWeight(false) ;
      break ;
    case NORMALIZED_EUCLIDIAN:
      PRINT_COLOR_(std::cout << "Computing the NORMALIZED euclidian distance between the vertices of the mesh" <<std::endl , GREEN) ;
      computeNormalizationWeight() ;
	    
      computeNormalizedEuclidianWeight(false) ;

      break ;
    case COTAN_WEIGHT:
      PRINT_COLOR_(std::cout << "Computing the cotan-weight between the vertices of the mesh" <<std::endl , GREEN) ;
      computeCotanWeight();
	    
      break ;

    case NORMALIZED_COTAN_WEIGHT:
      PRINT_COLOR_(std::cout << "Computing the NORMALIZED cotan-weight between the vertices of the mesh" <<std::endl , GREEN) ;
      computeNormalizationWeight() ;

      computeCotanWeight() ;

      break ;
    }
  }

  loaded = true;

  DBG_(std::cout << "OFF File has been read" << std::endl);

  return 0;
}


void Mesh::readOff()
{

  in >> npoints >> nfacets >> nedges;
  nvertex = npoints;

  PRINT_(std::
	 cout << "Loading : " << nvertex << " vertices with " << nfacets
	 << " facets and " << nedges << " edges" << std::endl);


  if (vertex)
    delete[]vertex;

  points = new double[dim * nvertex];
  vertex = points;
  memset(vertex, 0, dim * nvertex * sizeof(double));

  if (vertex_color)
    delete[]vertex_color;

  points_color = new float[color_depth * nvertex];
  vertex_color = points_color;
  memset(vertex_color, 0, color_depth * nvertex * sizeof(float));


  for (int i = 0; i < nvertex; i++) {

    in >> vertex[dim * i + 0] >> vertex[dim * i +
					1] >> vertex[dim * i + 2];
    vertex_color[color_depth * i + 0] = 0.5;
    vertex_color[color_depth * i + 1] = 0.5;
    vertex_color[color_depth * i + 2] = 0.5;
    vertex_color[color_depth * i + 3] = 1;

    while (in.peek() != '\n')
      in.get();
  }

  // we do not close the file, it will be used in read facets.

}

void Mesh::readNOff()
{

  in >> npoints >> nfacets >> nedges;

  nvertex = npoints;

  PRINT_(std::
	 cout << "Loading : " << nvertex << " vertices with " << nfacets
	 << " facets and " << nedges << " edges" << std::endl);

  if (vertex)
    delete[]vertex;

  points = new double[dim * nvertex];
  vertex = points;
  memset(vertex, 0, dim * nvertex * sizeof(double));

  if (vertex_color)
    delete[]vertex_color;

  points_color = new float[color_depth * nvertex];
  vertex_color = points_color;
  memset(vertex_color, 0, color_depth * nvertex * sizeof(float));

  if (vertex_normal)
    delete[]vertex_normal;

  vertex_normal = new double[dim * nvertex];
  memset(vertex_normal, 0, dim * nvertex * sizeof(double));


  for (int i = 0; i < nvertex; i++) {
    in >> vertex[dim * i + 0] >> vertex[dim * i +
					1] >> vertex[dim * i + 2];
    in >> vertex_normal[dim * i + 0] >> vertex_normal[dim * i +
						      1] >>
      vertex_normal[dim * i + 2];
    vertex_color[color_depth * i + 0] = 0.5;
    vertex_color[color_depth * i + 1] = 0.5;
    vertex_color[color_depth * i + 2] = 0.5;
    vertex_color[color_depth * i + 3] = 1;

    while (in.peek() != '\n')
      in.get();
  }

  // we do not close the file, it will be used in read facets.

}

void Mesh::readCOff()
{

  in >> npoints >> nfacets >> nedges;

  nvertex = npoints;

  PRINT_(std::
	 cout << "Loading : " << nvertex << " vertices with " << nfacets
	 << " facets and " << nedges << " edges" << std::endl);

  if (vertex)
    delete[]vertex;

  points = new double[dim * nvertex];
  vertex = points;
  memset(vertex, 0, dim * nvertex * sizeof(double));

  if (vertex_color)
    delete[]vertex_color;

  points_color = new float[color_depth * nvertex];
  vertex_color = points_color;
  memset(vertex_color, 0, color_depth * nvertex * sizeof(float));

  for (int i = 0; i < nvertex; i++) {
    in >> vertex[dim * i + 0] >> vertex[dim * i +
					1] >> vertex[dim * i + 2];
    in >> vertex_color[color_depth * i +
		       0] >> vertex_color[color_depth * i +
					  1] >>
      vertex_color[color_depth * i +
		   2] >> vertex_color[color_depth * i + 3];
    while (in.peek() != '\n')
      in.get();
  }
}



void Mesh::readFacets()
{


  MapFacet sFacet ;
  int l_num = 0 ; 
  int id = 0 ;
  for (int i = 0; i < nfacets; ++i) {
    sFacet.vertices.clear () ;
    in >> l_num ;

    for (int j = 0; j < l_num; ++j) {
      in >> id ;
      sFacet.vertices.push_back(id) ;

      hash_map_vertices[id].first.facets.push_back(i) ;
    }

    hash_map_facets[i] = sFacet ;


    while (in.peek() != '\n')
      in.get();

  }
	
  // the file is closed outside of this function.

}


void Mesh::findMeanAndScale(bool p_center)
{

  FL *l_mean = NULL;
  l_mean = new FL[dim];

  FL *l_minval = NULL;
  l_minval = new FL[dim];

  FL *l_maxval = NULL;
  l_maxval = new FL[dim];

  for (int i = 0; i < dim; i++) {
    l_mean[i] = 0.0;
    l_minval[i] = 0.0;
    l_maxval[i] = 0.0;
  }

  int idim = 0;

  // Find the center of mass of the mesh (each vertex hax mass=1).
  for (int i = 0; i < nvertex; i++) {
    
    for (int d = 0; d < dim; d++) {
      l_mean[d] += vertex[idim + d];
      if ((vertex[idim + d] < l_minval[d]) || (i == 0))
	l_minval[d] = vertex[idim + d];
      if ((vertex[idim + d] > l_maxval[d]) || (i == 0))
	l_maxval[d] = vertex[idim + d];
    }
    idim += 3;
  }

  idim = 0;

  for (int d = 0; d < dim; d++)
    l_mean[d] = l_mean[d] / ((FL) nvertex);

  // Center the mesh if required.
  if (p_center) {
    for (int i = 0; i < nvertex; i++) {
      for (int d = 0; d < dim; d++) {
	vertex[idim + d] -= l_mean[d];
      }
      idim += 3;
    }
  }
  // Find the scale factor of the mesh
  FL l_deltamax = fabs(l_maxval[0] - l_minval[0]);
  for (int j = 1; j < dim; j++) {
    if (l_maxval[j] - l_minval[j] > l_deltamax)
      l_deltamax = fabs(l_maxval[j] - l_minval[j]);
  }

  scale = l_deltamax;
  delete[]l_maxval;
  delete[]l_minval;
  delete[]l_mean;
}




void Mesh::computeNeis()
{
  //  int *l_t_facets = facets;
  int ind_j;
  MapVertex map_mesh ;
  MapFacet map_facet ;
  for (int i = 0; i < nfacets; ++i) {

    map_facet = hash_map_facets[i] ;

    std::list<int>::iterator it = map_facet.vertices.begin() ;
    Circulator < std::list<int>::iterator > circ ( map_facet.vertices.begin(), map_facet.vertices.end()) ;
    for ( int j = 0 ; j <  map_facet.vertices.size() ; ++j ) {


      ++it ;
      ind_j = *(circ) ;

      int l_k_left = *(--circ) ; //j - 1 ;
      ++circ ;
      int l_k_right = *(++circ) ; //j + 1 ;
     
      map_mesh = hash_map_vertices[l_k_left].first ;

      map_mesh.exists = true ;
      map_mesh.dist = 0 ;
      map_mesh.prev_dist = 0 ;
      map_mesh.depth = 1 ;

      if ( dim > 3 ) {
	FATAL_(std::cout << "map_mesh do not handle more than 3 dimensions" <<std::endl , FATAL_VARIABLE_INIT) ;
      }

      hash_map_vertices[ind_j].second.insert(make_pair(l_k_left,map_mesh)) ;

      map_mesh = hash_map_vertices[l_k_right].first ;
      
      map_mesh.exists = true ;
      map_mesh.dist = 0 ;
      map_mesh.prev_dist = 0 ;
      map_mesh.depth = 1 ;
      hash_map_vertices[ind_j].second.insert(make_pair(l_k_right,map_mesh)) ;

    }

  }
  
#ifdef DEBUG_3
  for (int i = 0; i < nvertex ; ++i) {
    for (hash_map< int, MapVertex >::iterator it = hash_map_vertices[i].second.begin() ; it != hash_map_vertices[i].second.end() ; ++it ) {
      if (  hash_map_vertices[it->first].second.find(i) ==  hash_map_vertices[i].second.end() ) {
	FATAL_(std::cout << i << " " << it->first << "is not symetric" <<std::endl, FATAL_VARIABLE_INIT) ;
      }
    }

  }
#endif


}


void Mesh::computeCotanWeight()
{


  MapVertex *map_mesh = new MapVertex[2];
  int l_count = 0;
  int l_morecount = 0;
  int l_lesscount = 0;

#ifdef DEBUG_3
  std::cout << "Values for the Cotangent weights" <<std::endl ;
#endif

  std::vector< std::pair < MapVertex, hash_map < int, MapVertex > > >::iterator it_i ;
  std::vector< std::pair < MapVertex, hash_map < int, MapVertex > > >::iterator it_pi ;

  for (int i = 0; i < nvertex; ++i) {

    it_i =  hash_map_vertices.begin () ;
    std::advance(it_i, i) ;

    MapVertex mesh_map_i =  it_i->first ;

    for (hash_map< int, MapVertex >::iterator it = it_i->second.begin() ; it != it_i->second.end() ; ++it ) {


      int pi = it->first ;

      it_pi =  hash_map_vertices.begin () ;
      std::advance(it_pi, pi) ;


      MapVertex mesh_map_pi =  it_pi->first ;

      if (it->second.dist == 0) {
	map_mesh[0].X = 0 ;
	map_mesh[0].Y = 0 ;
	map_mesh[0].Z = 0 ;
	map_mesh[1].X = 0 ;
	map_mesh[1].Y = 0 ;
	map_mesh[1].Z = 0 ;

	l_count = 0;



	for (hash_map< int, MapVertex >::iterator k = it_i->second.begin() ; k != it_i->second.end() ; ++k ) {
	  
	  for (hash_map< int, MapVertex >::iterator l = it_pi->second.begin() ; l != it_pi->second.end() ; ++l ) {
	    if ( k->first == l->first) {
	      if (l_count < 2) {
		map_mesh[l_count] = k->second ;
	      } 
	      else {
		// the mesh is not correct for the cotan weight computations	
		l_morecount++;
	      }
	      
	      l_count++;
	      
	    }	// end of if
	  }		// end of for l
	}		// end of for k
	
	if (l_count < 2) {
	  //  the mesh is not correct for the cotan weight computations
	  PRINT_(cout << i << " and " << pi <<
		 " There is less than 2 point in common..." << std::
		 endl);
	  
	  l_lesscount++;
	}

	else {
	  if (l_count >= 3 ) {
	    // the mesh is not correct for the cotan weight computations
	    PRINT_(cout << i << " and " << pi <<
		   " There is more than 2 point (" << l_count
		   << ") in common... " << endl);
	    l_morecount++;
	  }


	  // Core computation.
	  FL l_dist = 1;
	  {
	    FL l_va1[3];
	    FL l_va2[3];
	    l_va1[0] =
	      mesh_map_i.X - map_mesh[0].X ;
	    l_va1[1] =
	      mesh_map_i.Y - map_mesh[0].Y ;
	    l_va1[2] =
	      mesh_map_i.Z - map_mesh[0].Z ;
		
	    l_va2[0] =
	      mesh_map_pi.X - map_mesh[0].X ;
	    l_va2[1] =
	      mesh_map_pi.Y - map_mesh[0].Y ;
	    l_va2[2] =
	      mesh_map_pi.Z - map_mesh[0].Z ;
		    
	    FL l_nva1 =
	      sqrt(l_va1[0] * l_va1[0] + l_va1[1] * l_va1[1] +
		   l_va1[2] * l_va1[2]);
	    FL l_nva2 =
	      sqrt(l_va2[0] * l_va2[0] + l_va2[1] * l_va2[1] +
		   l_va2[2] * l_va2[2]);
		    
	    FL l_cosA =
	      l_va1[0] * l_va2[0] + l_va1[1] * l_va2[1] +
	      l_va1[2] * l_va2[2];
	    l_cosA = l_cosA / (l_nva1 * l_nva2);
		    
	    FL l_sinA = sqrt(1 - l_cosA * l_cosA);

	    FL l_cotanA = l_cosA / l_sinA;
		    
	    FL l_vb1[3];
	    FL l_vb2[3];
	    l_vb1[0] =
	      mesh_map_i.X - map_mesh[1].X ;
	    l_vb1[1] =
	      mesh_map_i.Y - map_mesh[1].Y ;
	    l_vb1[2] =
	      mesh_map_i.Z - map_mesh[1].Z ;
		    
	    l_vb2[0] =
	      mesh_map_pi.X - map_mesh[1].X ;
	    l_vb2[1] =
	      mesh_map_pi.Y - map_mesh[1].Y ;
	    l_vb2[2] =
	      mesh_map_pi.Z - map_mesh[1].Z ;
		    
	    FL l_nvb1 =
	      sqrt(l_vb1[0] * l_vb1[0] + l_vb1[1] * l_vb1[1] +
		   l_vb1[2] * l_vb1[2]);
	    FL l_nvb2 =
	      sqrt(l_vb2[0] * l_vb2[0] + l_vb2[1] * l_vb2[1] +
		   l_vb2[2] * l_vb2[2]);
		    
		    
	    FL l_cosB =
	      l_vb1[0] * l_vb2[0] + l_vb1[1] * l_vb2[1] +
	      l_vb1[2] * l_vb2[2];
	    l_cosB = l_cosB / (l_nvb1 * l_nvb2);
	    
	    FL l_sinB = sqrt(1 - l_cosB * l_cosB);
	    
	    FL l_cotanB = l_cosB / l_sinB;
	    
	    
	    
	    l_dist = 0.5 * (l_cotanA + l_cotanB) ;
	  }
	  

	  FL normalize = 1 ;
	  if ( mesh_map_i.voronoi_area != 0 ||  mesh_map_pi.voronoi_area != 0 ) {
	    normalize =  2.0 / ( mesh_map_i.voronoi_area + mesh_map_pi.voronoi_area ) ; 
	  }

	  it->second.dist = l_dist * normalize ;
	  it->second.depth = 1 ;
	  hash_map< int, MapVertex >::iterator it = it_pi->second.find(i) ;
	  if (it != it_pi->second.end()) {
	    it->second.dist = l_dist * normalize ;
	    it->second.prev_dist = l_dist * normalize ;
	    it->second.depth = 1 ;
	  }
	}
      }
    } // for j (mesh_nei_count)
#ifdef DEBUG_3
    std::cout << std::endl ;
#endif
  }				// for i (nvertex)
  
  delete [] map_mesh ;

  
  DBG_(cout << "Total number of point more then 2 comon nei : " <<
       l_morecount << " and less : " << l_lesscount << std::endl);
}


void Mesh::computeNormalizationWeight()
{


  int l_max_num_facets_per_vertex = 0 ;
  HASH_MAP_PARAM_(int, "max_num_facets_per_vertex", Int, l_max_num_facets_per_vertex ) ;
    
  double l_area = 0;


  FL l_a1x = 0, l_a1y = 0, l_a1z = 0;
  FL l_a2x = 0, l_a2y = 0, l_a2z = 0;
  FL l_ax = 0, l_ay = 0, l_az = 0;

  int l = 0;

  std::vector< std::pair < MapVertex, hash_map < int, MapVertex > > >::iterator it = hash_map_vertices.begin () ;
  int ind_facet = 0 ;
  int l_count = 0 ;
  while ( it != hash_map_vertices.end() ) {
    
    MapVertex mesh_map_it = it->first ;
    l_area = 0 ;
    

    std::list<int>::iterator facet = it->first.facets.begin() ;
    while ( facet != it->first.facets.end() ) {
      
      ind_facet = *facet ;
      l = 0;
      std::list<int>::iterator it_v = hash_map_facets[ind_facet].vertices.begin () ;
      while ( it_v != hash_map_facets[ind_facet].vertices.end()) {

	int ind_vert = *it_v ; 	
	
	if (ind_vert != l_count) {
	  if (l) {
	    l_a1x =
	      hash_map_vertices[ind_vert].first.X - mesh_map_it.X ;
	    l_a1y =
	      hash_map_vertices[ind_vert].first.Y - mesh_map_it.Y ;
	    l_a1z =
	      hash_map_vertices[ind_vert].first.Z - mesh_map_it.Z ;
	  } else {
	    l_a2x =
	      hash_map_vertices[ind_vert].first.X - mesh_map_it.X ;
	    l_a2y =
	      hash_map_vertices[ind_vert].first.Y - mesh_map_it.Y ;
	    l_a2z =
	      hash_map_vertices[ind_vert].first.Z - mesh_map_it.Z ;
	  }
	  ++l;
	}
	++it_v ;
      }

      l_ax = l_a1y * l_a2z - l_a1z * l_a2y;
      l_ay = l_a1z * l_a2x - l_a1x * l_a2z;
      l_az = l_a1x * l_a2y - l_a1y * l_a2x;

      l_area += sqrt(l_ax * l_ax + l_ay * l_ay + l_az * l_az) / 2;

      ++facet ;
    }
    it->first.voronoi_area = l_area ;
    ++it ;
    
  }


}


FL *Mesh::getVoronoiAreas()
{

  // For compatibility issues, we keep the voronoi as an array...

  if (approx_voronoi_area) 
    delete [] approx_voronoi_area ;

  approx_voronoi_area = new FL[nvertex];
  bzero(approx_voronoi_area, nvertex * sizeof(FL));
  std::vector< std::pair < MapVertex, hash_map < int, MapVertex > > >::iterator it = hash_map_vertices.begin () ;
  FL *l_t_approx_voronoi_area =  approx_voronoi_area ;
  while ( it != hash_map_vertices.end() ) {
    *(l_t_approx_voronoi_area) = it->first.voronoi_area ;
    ++l_t_approx_voronoi_area ;
  }    
  return approx_voronoi_area;

}


void Mesh::computeNormalizedEuclidianWeight(bool p_normalize)
{



#ifdef DEBUG_3
  std::cout << "Distance Computation: " <<std::endl ;
#endif
  std::vector< std::pair < MapVertex, hash_map < int, MapVertex > > >::iterator it = hash_map_vertices.begin () ;
  for (int i = 0; i < nvertex; ++i) {

    for (hash_map< int, MapVertex >::iterator it_nei = it->second.begin() ; it_nei != it->second.end() ; ++it_nei ) {
      // hash_map is made of pairs.
      // it->first returns the key, 
      // it->second return the data
	
      int pi = it_nei->first ;

	
      FL va1[3];
      FL va2[3];
      va1[0] = it->first.X ;
      va1[1] = it->first.Y ;
      va1[2] = it->first.Z ;
	
      va2[0] = hash_map_vertices[pi].first.X ;
      va2[1] = hash_map_vertices[pi].first.Y ;
      va2[2] = hash_map_vertices[pi].first.Z ;
      FL a = va1[0] - va2[0];
      FL b = va1[1] - va2[1];
      FL c = va1[2] - va2[2];
	
	
      FL dist = sqrt(a * a + b * b + c * c);
#ifdef DEBUG_3
      std::cout<< i << " "<< pi << " (" << va1[0] << " " << va1[1] << " " << va1[2] << ") " << " (" << va2[0] << " " << va2[1] << " " << va2[2] << ") dist=" << dist <<std::endl ;
#endif
      if (p_normalize && (hash_map_vertices[i].first.voronoi_area !=0 || hash_map_vertices[pi].first.voronoi_area != 0))
	dist = dist * 2.0 / (hash_map_vertices[i].first.voronoi_area + hash_map_vertices[pi].first.voronoi_area) ;
      it_nei->second.dist = dist ;
      it_nei->second.prev_dist = dist ;
      it_nei->second.depth = 1 ;
		  
    }

    ++it ;

  }
    
}





int Mesh::fillFacetNormals()
{
  /*TODO: overwriting normal for repeating nodes. Average is needed, except for sharp changing
    surfaces as a cube where this method is correct, probably leave both just in case
    calculate n1+n2+n3+n4 and then normalize it. (You can get a better
    average if you weight the normals by the size of the angles at the shared intersection.)
  */

  for (int i = 0; i < nfacets; i++) {

    if (hash_map_facets[i].vertices.size() < 2) {
      PRINT_(fprintf
	     (stderr, "Face %d has less than 3 vertices\n", i));
      continue;
    }
    Circulator < std::list<int>::iterator > circ ( hash_map_facets[i].vertices.begin(), hash_map_facets[i].vertices.end()) ;

    int ind = *(circ) ;
    int ind_1 = *(++circ) ;
    int ind_2 = *(++circ) ;

    computeOneNormal(ind,
		     ind_1,
		     ind_2,
		     hash_map_facets[i].normal);

  }

  return 0;
}


int Mesh::computeOneNormal(int p_v0, int p_v1, int p_v2, double p_normal[3])
{
  double a[3];
  double b[3];

  // calculate the vectors A and B
  // note that v[3] is defined with counterclockwise winding in mind

  a[0] = hash_map_vertices[p_v0].first.X - hash_map_vertices[p_v1].first.X ;
  a[1] = hash_map_vertices[p_v0].first.Y - hash_map_vertices[p_v1].first.Y ;
  a[2] = hash_map_vertices[p_v0].first.Z - hash_map_vertices[p_v1].first.Z ;

  b[0] = hash_map_vertices[p_v1].first.X - hash_map_vertices[p_v2].first.X ;
  b[1] = hash_map_vertices[p_v1].first.Y - hash_map_vertices[p_v2].first.Y ;
  b[2] = hash_map_vertices[p_v1].first.Z - hash_map_vertices[p_v2].first.Z ;


  // calculate the cross product and place the resulting vector
  // into the address specified in parameters
  p_normal[0] = (double) ((a[1] * b[2]) - (a[2] * b[1]));
  p_normal[1] = (double) ((a[2] * b[0]) - (a[0] * b[2]));
  p_normal[2] = (double) ((a[0] * b[1]) - (a[1] * b[0]));

  // normalize
  float l_len =
    (double) sqrt((p_normal[0] * p_normal[0]) +
		  (p_normal[1] * p_normal[1]) +
		  (p_normal[2] * p_normal[2]));
  if (l_len == 0.0) {
    l_len = 1.0f;
  }

  for (int i = 0; i < 3; i++)
    p_normal[i] /= l_len;

  return 0;
}







void Mesh::buildDistMat(int p_max_depth)
{

  PRINT_(std::cout << "Start DISTMAT" << std::endl);

  if (total_num_nei) {
    delete[]total_num_nei;
    total_num_nei = NULL;
  }

  total_num_nei = new int[nvertex];
  std::vector< std::pair < MapVertex, hash_map < int, MapVertex > > > hash_map_vertices_orig = hash_map_vertices ;
  int l_depth = 1;

  double l_length = 0 ;

  for (int i = 0 ; i < p_max_depth ; ++i ) {
    l_length += 6 * pow(2,i) ;
  }

  int l_ilength =  0 ;
  if (p_max_depth == 0 )
    HASH_MAP_PARAM_(int, "max_num_of_nei", Int, l_ilength ) ;
  else 
    l_ilength = (int) l_length ;
  if (l_ilength > 1900000000) {
    FATAL_(fprintf
	   (stderr,
	    "MaxDepth is too high to build the distance matrix.\n"
	    "The maximum number of element to be stored is too high with max_depth = %d \n"
	    "Please consider reducing max_depth, or make sure you have less than %d neighbors per vertex \n"
	    "In the latter case, set MAX_NUM_OF_NEI to the correct value in Constant.h \n",
	    p_max_depth, l_ilength), FATAL_MEMORY_ALLOCATION);
  }

  bool *l_nei_done = new bool[nvertex];
  bzero(l_nei_done, nvertex * sizeof(bool));


  int curr_ind = 0;



#ifdef DEBUG_SPREAD
  std::cout << "SPREAD STEP " <<std::endl ;
#endif
  // SPREAD FUNCTION
  //Create a queue in which we store the current map. 
  
  
  deque <std::pair<int, MapVertex> > l_Q ;
  
  for (int i = 0; i < nvertex; ++i) {

    l_depth = 1;
    
    memset(l_nei_done, 0, nvertex * sizeof(bool) ) ;
    
    while (l_depth < p_max_depth) {
      l_depth++;

      l_Q.clear() ;
      int l_count = 0 ;
      // Copy the content of the map.
      for (hash_map< int, MapVertex >::iterator it = hash_map_vertices[i].second.begin() ; it != hash_map_vertices[i].second.end() ; ++it ) {
	l_Q.push_front(std::make_pair(it->first, it->second)) ;

	++l_count ;
      }
      

#ifdef DEBUG_SPREAD
      std::cout <<  "STARTING RING NUMBER "<< l_depth << " for vertex " << i << std::endl ;
      fflush(stdout) ;
      fflush(stdin) ;
      getchar () ;
      
#endif
      std::pair<int, MapVertex> p ;
      deque< std::pair<int, MapVertex> >::iterator it = l_Q.begin() ; 
      l_count = 0 ;
      while ( it != l_Q.end() ) {
	curr_ind = it->first ;

	if (!l_nei_done[curr_ind]) {
	  for (hash_map< int, MapVertex >::iterator it_1 = hash_map_vertices_orig[curr_ind].second.begin() ; it_1 != hash_map_vertices_orig[curr_ind].second.end() ; ++it_1 ) {


	    if (it_1->first != i && hash_map_vertices[i].second.find(it_1->first) == hash_map_vertices[i].second.end() ) {
	      p.second = it_1->second ;
	      p.first = it_1->first ;
	      if (!use_depth_as_dist) {
		p.second.prev_dist = p.second.dist ;
		p.second.dist += it_1->second.dist ;
		hash_map_vertices[i].second.insert(p) ;
	      }
	      else {
		p.second.depth = l_depth ;
		hash_map_vertices[i].second.insert(p) ;
	      }
	    } // if (it->first != i && ....
	    else if ( !use_depth_as_dist && it_1->first != i ) {

	      FL l_dist = hash_map_vertices[i].second[it_1->first].prev_dist + it_1->second.dist ;
	      if (l_dist <  hash_map_vertices[i].second[it_1->first].dist ) {
		
		hash_map_vertices[i].second[it_1->first].dist = l_dist ;
		hash_map_vertices[i].second[it_1->first].depth = l_depth ;
	      } // if (l_dist <  hash_map_vertices[i][it->first].dist
	      
	    } // else if ( !use_depth_as_dist && it->first != i )
	    
	  } // for (hash_map< int, MapVertex >::iterator it = hash_map_vertices[curr_ind] ....
	  
	  l_nei_done[curr_ind] = true;

	} // if (!l_nei_done[curr_ind])  ...
	++l_count ;
	++it ;

      } // while ( !l_Q.empty() )

      // Update the previous computed distance to the current one for the next iteration step in depth and distance computation.
      if (!use_depth_as_dist) {
	it = l_Q.begin() ; 
	while ( it != l_Q.end() ) {
	  curr_ind = it->first ;	// current neighbor of i
	  for (hash_map< int, MapVertex >::iterator it_1 = hash_map_vertices[curr_ind].second.begin() ; it_1 != hash_map_vertices[i].second.end() ; ++it_1 ) {

	    if ( hash_map_vertices[i].second.find(it_1->first) !=  hash_map_vertices[i].second.end() ) {
	      hash_map_vertices[i].second[it_1->first].prev_dist = hash_map_vertices[i].second[it_1->first].dist ;
	    }
	  } // for (hash_map< int, MapVertex >::iterator it_1
	  ++it ;
	} // while ( it != l_Q.end() )
      } // if (!use_depth_as_dist)



    } // end of while on depth.
  } // end of for on nvertex

  for (int i = 0; i < nvertex; ++i) {
    nzelems += hash_map_vertices[i].second.size () ;
  }
  
  PRINT_(std::cout << "Total number of NEI : " << nzelems << std::
	 endl);

  l_Q.clear () ;
  hash_map_vertices_orig.clear () ;

  delete [] l_nei_done;


  fillSparseWeight();



  PRINT_(std::cout << "End DISTMAT" << std::endl);

}



int Mesh::fillSparseWeight()
{

  if (weights) {
    delete[]weights;
    weights = NULL;
  }
  if (mat_weights) {
    delete[]mat_weights;
    mat_weights = NULL;
  }

  weights = new FL[3 * nzelems];

  memset(weights, 0, 3 * nzelems * sizeof(FL));

  mat_weights = new FL *[nvertex];
  if (vec_depth) {
    delete [] vec_depth;
    vec_depth = NULL;
  }

  if (mat_depth) {
    delete[]mat_depth;
    mat_depth = NULL;
  }

  vec_depth = new int[3 * nzelems];

  memset(vec_depth, 0, 3 * nzelems * sizeof(int));

  mat_depth = new int *[nvertex]; 

  int l_total_count = 0;
  int l_count = 0;

  // Filling Matweights.
  mat_weights[0] = weights ;
  mat_depth[0] = vec_depth ;
  int l_cumu_count = 0;
#ifdef DEBUG_FULL
  std::cout << "Weight mat:" <<std::endl ;
#endif
  for (int i = 0; i < nvertex; ++i) {
    l_count = 0;
    hash_map< int, MapVertex >::iterator it = hash_map_vertices[i].second.begin() ;

    while ( it != hash_map_vertices[i].second.end() ) {
      weights[l_total_count * 3] = (FL) i ;
      weights[l_total_count * 3 + 1] = (FL) it->first ;
      vec_depth[l_total_count * 3] = i;
      vec_depth[l_total_count * 3 + 1] =  it->first ;


      if (!use_depth_as_dist) {
	weights[l_total_count * 3 + 2] = it->second.dist ;
      }
      else {
	weights[l_total_count * 3 + 2] = it->second.depth ;
      }    
      vec_depth[l_total_count * 3 + 2] = it->second.depth ;


#ifdef DEBUG_FULL
      std::cout << weights[l_total_count * 3] << " " << weights[l_total_count * 3 + 1] << " " << weights[l_total_count * 3 + 2] <<std::endl ;
#endif
      ++it ;
      l_total_count++;
      l_count++;
    }
    l_cumu_count += 3 * l_count;

    if (i < nvertex - 1) {
      mat_weights[i + 1] = weights + l_cumu_count;
      mat_depth[i + 1] = vec_depth + l_cumu_count;
    }


  }

  if (l_total_count != nzelems) {
    FATAL_(fprintf
	   (stderr,
	    "There is an error in Fill Sparse Mesh Dist Matrix: %d != %d \n",
	    l_total_count, nzelems), FATAL_ERROR);
  }
  // Releasing memory




  sortMatWeight();




  //  if (!use_depth_as_dist) {
#if 0
  symmeterizeMatWeight();
#endif 
  checkSymmeterizeMatWeight();
#ifdef DEBUG_FULL
  fflush(stdout) ;
  fflush(stdin) ;
  getchar () ;
#endif
  //  }




  return 0;

}




void Mesh::sortMatWeight()
{
  FL *l_sorted = NULL;



  for (int i = 0; i < nvertex; ++i) {

    if (l_sorted)
      delete[]l_sorted;
    l_sorted = new FL[3 * hash_map_vertices[i].second.size()];

    memcpy(l_sorted, mat_weights[i],
	   3 * hash_map_vertices[i].second.size() * sizeof(FL));
    // CompareFunc() is provided in utilities.h. 
    qsort(l_sorted, hash_map_vertices[i].second.size(), 3 * sizeof(FL), compareFunc);

    memcpy(mat_weights[i], l_sorted,
	   3 * hash_map_vertices[i].second.size() * sizeof(FL));

  }

  if (l_sorted)
    delete[]l_sorted;

}


void Mesh::symmeterizeMatWeight()
{

  PRINT_(fprintf(stderr, "Symeterize the Weight matrix\n"));



  int j = 0 ;
  int k = 0 ;

  for (int i = 0; i < nvertex; ++i) {
    hash_map< int, MapVertex >::iterator it = hash_map_vertices[i].second.begin() ;    
    j = 0 ;
    while ( it != hash_map_vertices[i].second.end() ) {
      k = 0 ;
      hash_map< int, MapVertex >::iterator it2 = hash_map_vertices[it->first].second.begin() ;
      while ( it2 != hash_map_vertices[it->first].second.end() ) {

	if (i ==
	    mat_weights[(int) mat_weights[i][3 * j + 1]][3 * k +
							 1]) {
	  mat_weights[i][3 * j + 2] +=
	    mat_weights[(int) mat_weights[i][3 * j + 1]][3 *
							 k +
							 2];
	  mat_weights[i][3 * j + 2] /= 2;
	  mat_weights[(int) mat_weights[i][3 * j + 1]][3 * k +
						       2] =
	    mat_weights[i][3 * j + 2];

	  break;

	}
	++k;
	++it2 ;
      }
      ++j;
      ++it ;
    }
  }
}




void Mesh::checkSymmeterizeMatWeight()
{
  FL l_error = 0;

  int k = 0 ;
  int l = 0 ;

  for (int i = 0; i < nvertex; ++i) {
    hash_map< int, MapVertex >::iterator it = hash_map_vertices[i].second.begin() ;    
    k = 0 ;
    while ( it != hash_map_vertices[i].second.end() ) {
      l = 0 ;
      hash_map< int, MapVertex >::iterator it2 = hash_map_vertices[it->first].second.begin() ;
      while ( it2 != hash_map_vertices[it->first].second.end() ) {
	
	if (i ==
	    mat_weights[(int) mat_weights[i][3 * k + 1]][3 * l +
							 1]) {
	  
	  FL a =
	    (mat_weights[i][3 * k + 2] -
	     mat_weights[(int) mat_weights[i][3 * k + 1]][3 *
							  l +
							  2]);
	  if (a !=0)
	    std::cout << "Not symetric on : " << i << " " << (int) mat_weights[i][3 * k + 1] << " "<< mat_weights[i][3 * k + 2]<< " "<<  mat_weights[(int) mat_weights[i][3 * k + 1]][3 * l + 2] <<std::endl ;
	  l_error += a * a;

	  break;
	}
	++l;
	++it2 ;
      }
      ++k;
      ++it ;
    }
  }


  for (int i = 0; i < nvertex; ++i) {
    hash_map< int, MapVertex >::iterator it = hash_map_vertices[i].second.begin() ;    
    while ( it != hash_map_vertices[i].second.end() ) {
      hash_map< int, MapVertex >::iterator it2 = hash_map_vertices[it->first].second.find(i) ;
      if (it2 == hash_map_vertices[it->first].second.end()) {
	FATAL_(std::cout << "Not symmetric on " << i << " " << it->first <<std::endl, FATAL_VARIABLE_INIT) ;
      }
      else if (it2->second.depth != it->second.depth) {
	FATAL_(std::cout << i << " " << it->first << " d not have the same depth" <<std::endl, FATAL_VARIABLE_INIT) ;
      }
      ++it ;
    }
  }
  PRINT_COLOR_(std::cout << "Error for symmeterize is : " << l_error << std::
	       endl, BLUE);
}



FL *Mesh::getFLSparseWeight()
{

  return weights;
}

FL **Mesh::getFLSparseMatWeight()
{

  return mat_weights;
}

int **Mesh::getSparseMatDepth()
{
  return mat_depth;
}

int Mesh::buildMeshsetPointList()
{
  int res = buildPointsetList();
  return res;
}

int Mesh::buildMeshsetList()
{
  int res = buildPointsetList();
  return res;
}

int Mesh::drawMeshsetPoint()
{
  int res = drawPointsetList();
  return res;
}

int Mesh::drawMeshsetList()
{
  int res = drawPointsetList();
  return res;
}

int Mesh::drawMeshsetFacets(bool drawEdges)
{

  std::vector<MapFacet>::iterator it_facet = hash_map_facets.begin() ; 

  if (loaded) {
    int ind = 0;
    for (int i = 0; i < nfacets; i++) {


      glBegin(GL_POLYGON);
      //      std::cout << it_facet->normal[0] << ", " <<  it_facet->normal[1] <<", " << it_facet->normal[2] <<std::endl ;
      glNormal3f(it_facet->normal[0], it_facet->normal[1], it_facet->normal[2]) ;
      //      glNormal3f(facet_normal[i * dim], facet_normal[i * dim + 1], facet_normal[i * dim + 2]);


      std::list<int>::iterator it_vert = it_facet->vertices.begin() ;

      for (int j = 0; j < it_facet->vertices.size(); j++) {
	
	ind = *(it_vert) ;

	MapVertex map_vert = hash_map_vertices[ind].first ;

	glColor4f(points_color[ind * color_depth],
		  points_color[ind * color_depth + 1],
		  points_color[ind * color_depth + 2], 0.5);
	glVertex3f((float) (map_vert.X) / scale,
		   (float) (map_vert.Y) / scale,
		   (float) (map_vert.Z) / scale);

	++it_vert ;
      }
      glEnd();


      if (drawEdges) {
	glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
	glLineWidth(3);
	glBegin(GL_POLYGON);

	glNormal3f(it_facet->normal[0], it_facet->normal[1], it_facet->normal[2]);
	it_vert = it_facet->vertices.begin() ;
	for (int j = 0; j < it_facet->vertices.size(); j++) {
	  ind = *(it_vert) ;

	  MapVertex map_vert = hash_map_vertices[ind].first ;

	  glColor4f(0.0f, 0.0f, 0.0f, 0);
	  glVertex3f((float) (map_vert.X) / scale,
		     (float) (map_vert.Y) / scale,
		     (float) (map_vert.Z) / scale);

	  ++it_vert ;
	}
	glEnd();
      }
      glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

      ++it_facet ;
    }
  }
  return (0);

}


void Mesh::buildNeiColormap(int p_depth)
{

  if (NEI_colormap) {
    delete[]NEI_colormap;
    NEI_colormap = NULL;
  }

  NEI_colormap = new float[color_depth * p_depth];

  DBG2_(fprintf
	(stderr, "Building NEI_colormap with %d colors\n", p_depth));

  float l_r, l_g, l_b, l_deltar, l_deltag, l_deltab, l_minr, l_ming,
    l_minb;
  l_deltar = 0.8f;
  l_deltag = 0.8f;
  l_deltab = 0.8f;
  l_minr = 0.1f;
  l_ming = 0.1f;
  l_minb = 0.1f;

  int ind;
  float l_colval = 0.0f;

  int l_ncolor = p_depth + 1;

  for (int i = 0; i < p_depth; i++) {

    ind = i;
    if (ind < 0) {		//outlier 
      l_r = l_g = l_b = 0.5;
    } else if (ind < (int) round((float) (l_ncolor) / 3.0f)) {
      l_colval = 3.0f * (float) ind / (float) (l_ncolor);
      l_r = l_minr;
      l_g = l_deltag * l_colval + l_ming;
      l_b = l_deltab + l_minb;
    } else if (ind < (int) round(2.0f * (float) (l_ncolor) / 3.0f)) {
      ind = ind - (int) round((float) (l_ncolor) / 3.0f);
      l_colval = 3.0f * (float) ind / (float) (l_ncolor);
      l_r = l_deltar * l_colval + l_minr;
      l_g = l_deltag + l_ming;
      l_b = l_deltab * (1.0f - l_colval) + l_minb;
    } else {
      ind = ind - (int) round(2.0f * (float) (l_ncolor) / 3.0f);
      l_colval = 3.0f * (float) ind / (float) (l_ncolor);
      l_r = l_deltar + l_minr;
      l_g = l_deltag * (1.0f - l_colval) + l_ming;
      l_b = l_minb;


    }

    NEI_colormap[i * color_depth] = l_r;
    NEI_colormap[i * color_depth + 1] = l_g;
    NEI_colormap[i * color_depth + 2] = l_b;
    if (color_depth == 4)
      NEI_colormap[i * color_depth + 3] = 1;
    DBG3_(fprintf
	  (stderr, "color %d : %g %g %g\n", ind,
	   NEI_colormap[i * color_depth],
	   NEI_colormap[i * color_depth + 1],
	   NEI_colormap[i * color_depth + 2]));

  }


}




int Mesh::buildMeshsetNeiList()
{
  //    double scale = 1 ;

  float l_x, l_y, l_z;
  int l_nei_draw_sample_rate ;

  HASH_MAP_PARAM_(int, "nei_draw_sample_rate", Int, l_nei_draw_sample_rate) ;

  if (mat_weights == NULL)
    return 0;
  if (loaded) {
    if (MESHNEISET != (uint) (-1)) {
      PRINT_(fprintf
	     (stderr,
	      "Destroying previously created list. Be sure this is not done inside a glBegin-glEnd cycle\n"));
      glDeleteLists(MESHNEISET, 1);
      MESHNEISET = (uint) (-1);
    }
    MESHNEISET = glGenLists(1);

    if (glIsList(MESHNEISET)) {
      glNewList(MESHNEISET, GL_COMPILE);



      std::vector< std::pair < MapVertex, hash_map < int, MapVertex > > >::iterator it = hash_map_vertices.begin() ;
      
      do {
	if (it->first.id % l_nei_draw_sample_rate == 0) {
	  l_x = (float) (it->first.X / scale);
	  l_y = (float) (it->first.Y / scale);
	  l_z = (float) (it->first.Z / scale);
	  // Drawing the current point
	  GLUquadric *quad = gluNewQuadric();
	  glPushMatrix();
	  glTranslated(l_x, l_y, l_z);
	  glColor3f(1, 0, 0);
	  gluSphere(quad, 0.002, 10, 10);
	  glPopMatrix();
	  // Drawing the neighbors of the current point
	  hash_map < int, MapVertex >::iterator it_nei = it->second.begin() ;
	  while (it_nei != it->second.end() ) {
	    
	    int ind = it_nei->first ;
	    int color_index =  it_nei->second.depth ;
	    
	    if (NULL || NEI_colormap) {
	      glColor3f(NEI_colormap[color_index],
			NEI_colormap[color_index + 1],
			NEI_colormap[color_index + 2]);
	    } else {
	      
	      PRINT_(fprintf
		     (stderr,
		      "The NEI colormap for the mesh is not set. Using hardcoded colors. \n"));
	      
	    }
	    
	    l_x = (float) (it_nei->second.X / scale);
	    l_y = (float) (it_nei->second.Y / scale);
	    l_z = (float) (it_nei->second.Z / scale);
	  
	    GLUquadric *quad = gluNewQuadric();
	    glPushMatrix();
	    glTranslated(l_x, l_y, l_z);
	    gluSphere(quad, 0.002, 10, 10);
	    glPopMatrix();
	  
	    ++it_nei ;
	  
	  }
	}
	++it ;
	//	std::advance(it, l_nei_draw_sample_rate) ;
      } while ( it != hash_map_vertices.end() ) ;

      glEndList();
    }
  }

  return 0;

}


int Mesh::drawMeshsetNei()
{

  if (!loaded)
    return 1;
  if (MESHNEISET == (uint) (-1))
    buildMeshsetNeiList();
  glCallList(MESHNEISET);


  return (0);

}


FL *Mesh::getVertices()
{
  return getPoints();
}


int Mesh::getNumOfVertex()
{
  return getNumOfPoints();
}

int *Mesh::getNumOfNeik()
{
  return total_num_nei;
}




bool std_compareFuncSortVertexY(const std::pair<MapVertex , hash_map < int, MapVertex > >& lhs, const std::pair<MapVertex, hash_map < int, MapVertex > >& rhs)
{
  return lhs.first.Y < rhs.first.Y ;
}


//The sort on vertx array is kept for compatibility issues...
void Mesh::sortMeshY () {

  FL *l_sort = new FL[ (dim+1) * nvertex ] ;

  revert_index_sort_mesh = new int[nvertex] ;

  // Sorting points
  FL *l_t_sort = l_sort ;
  FL *l_t_vert = vertex ;
  for (int  i = 0 ; i < nvertex ; ++i) {
    *l_t_sort = (FL)i ;
    memcpy((l_t_sort + 1), l_t_vert, dim * sizeof(FL)) ;
    l_t_sort += (dim +1) ;
    l_t_vert += dim ;
  }

  qsort (l_sort, nvertex, (dim +1) * sizeof(FL), compareFuncSortVertexY) ;

  
  std::sort(hash_map_vertices.begin(), hash_map_vertices.end(), std_compareFuncSortVertexY);

  std::vector< std::pair < MapVertex, hash_map < int, MapVertex > > >::iterator  it = hash_map_vertices.begin() ;
  int id = 0 ;
  while ( it !=  hash_map_vertices.end() ) {
    it->first.id = id ;
    ++id ;
    ++it ;
  }
  
  it = hash_map_vertices.begin() ;
  while ( it !=  hash_map_vertices.end() ) {
    std::list <int>::iterator it_l = it->first.facets.begin() ;
    while ( it_l !=  it->first.facets.end() ) {
      int idx = *it_l ;
      *it_l = hash_map_vertices[idx].first.id ;
      ++it_l ;
    }
    ++it ;
  }


  l_t_sort = l_sort ;
  l_t_vert = vertex ;
  for (int  i = 0 ; i < nvertex ; ++i) {
    memcpy( l_t_vert, (l_t_sort + 1), dim * sizeof(FL)) ;
    revert_index_sort_mesh[(int)(*l_t_sort )] = i ;
    l_t_sort += (dim +1) ;
    l_t_vert += dim ;
  }
  
  delete [] l_sort ;
}

int *Mesh::getIndexSorting ( ) {

  return revert_index_sort_mesh ;
    
}



void Mesh::enableSortMesh(bool on) {
  enable_sort_mesh = on ;
}

/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/

/*
 *  DispHistograms.ui.h
 *  Common to multiple programs
 *
 *  Created by Diana Mateus and David Knossow on 11/10/07.
 *  Copyright 2007 INRIA. All rights reserved.
 *
 * This code is based on a sample provided with Qwt library. 
 * It is dedicated to display histograms. 
 * In practice, it displays the histograms of the Eigenfunctions 
 * computed in embed.cpp.
 *
 */
#include <iostream>
#include "types.h"
#include "utilities.h"
#include <qwidget.h>
#include <qpen.h>
#include <qwt_plot.h>
#include <qwt_plot_grid.h>
#include <qwt_plot_marker.h>
#include <qwt_interval_data.h>


void HistoWidget::init()
{
    histograms = NULL;
    grid = NULL;
    intervals = NULL;
    plot = NULL;
    values = NULL;
    num_hist = 0;
    histo_data = NULL;
    rows = 0;
    cols = 0;
}

void HistoWidget::destroy()
{
    if (histograms)
	delete[]histograms;
    histograms = NULL;
    if (grid)
	delete[]grid;
    grid = NULL;
    if (intervals)
	delete[]intervals;
    intervals = NULL;
    if (values)
	delete[]values;
    values = NULL;
    num_hist = 0;
}




void HistoWidget::closeEvent(QCloseEvent * e)
{

    releaseAll();

    close(true);

    hide();
}

void HistoWidget::releaseAll()
{
    DBG_(std::cout << "Releasing Histogram data" << std::endl);
    if (histograms)
	delete[]histograms;
    histograms = NULL;
    if (grid)
	delete[]grid;
    grid = NULL;
    if (intervals)
	delete[]intervals;
    intervals = NULL;
    if (values)
	delete[]values;
    values = NULL;
    num_hist = 0;
}

int HistoWidget::setDataToDisplay(int p_N, int p_Nx, int p_Ny, FL * p_data,
				  int *p_hist_sizes)
{

    if (p_N == 0)
	return 1;

    if (p_data == NULL) {
	ERROR_(fprintf(stderr, "No histograms set\n"), 1);
    }

    if (p_hist_sizes == NULL)
	return 1;


    if (num_hist != 0 && num_hist != p_N)
	releaseAll();

    num_hist = p_N;
    rows = p_Nx;
    cols = p_Ny;

    PRINT_(std::cout << "Loading histogram data " << std::endl);


    if (histo_data == NULL)
	histo_data = p_data;

    if (plot == NULL) {
	plot = new QwtPlot *[num_hist];

	for (int i = 0; i < num_hist; ++i) {
	    plot[i] = new QwtPlot(this);
	}
    }

    if (histograms == NULL)
	histograms = new HistogramItem[num_hist];


    for (int i = 0; i < num_hist; ++i) {
	histograms[i].setColor(Qt::darkCyan);
    }

    if (intervals == NULL)
	intervals = new QwtArray < QwtDoubleInterval >[num_hist];

    if (values == NULL)
	values = new QwtArray < double >[num_hist];

    int l_count = 0;
    double l_pos = 0.0;
    for (int i = 0; i < num_hist; ++i) {
	l_pos = 0;
	intervals[i].resize(p_hist_sizes[i]);
	values[i].resize(p_hist_sizes[i]);
	for (int j = 0; j < p_hist_sizes[i]; ++j) {
	    intervals[i][j] = QwtDoubleInterval(l_pos, l_pos + double (1));
	    values[i][j] = p_data[l_count];
	    ++l_count;
	    l_pos += 1;
	}
    }
    for (int i = 0; i < num_hist; ++i) {
	histograms[i].setData(QwtIntervalData(intervals[i], values[i]));
	histograms[i].attach(plot[i]);

    }
    return 0;
}

void HistoWidget::showEvent(QShowEvent * e)
{


    this->setPaletteBackgroundColor(white);

    int l_w = 300;
    int l_h = 200;
    this->resize(l_w * cols, l_h * rows);
    int i = 0;
    for (int j = 0; j < rows; ++j) {
	int l_posy = l_h * j;
	for (int k = 0; k < cols; ++k, ++i) {

	    if (i < num_hist) {
		int l_posx = (k % l_w) * l_w;
		plot[i]->setGeometry(QRect(l_posx, l_posy, l_w, l_h));
		plot[i]->replot();
		plot[i]->setCanvasBackground(white);
		plot[i]->show();
	    }
	}
    }
}

void HistoWidget::resizeEvent(QResizeEvent * e)
{

    if (cols == 0 || rows == 0)
	return;

    QSize l_s = this->size();
    int l_w = (int) floor(l_s.width() / cols);
    int l_h = (int) floor(l_s.height() / rows);

    int i = 0;
    for (int j = 0; j < rows; ++j) {
	int l_posy = l_h * j;
	for (int k = 0; k < cols; ++k, ++i) {
	    if (i < num_hist) {
		int l_posx = (k % l_w) * l_w;
		plot[i]->setGeometry(QRect(l_posx, l_posy, l_w, l_h));
		plot[i]->replot();
		plot[i]->show();
	    }
	}
    }

}

void HistoWidget::update()
{

    for (int i = 0; i < num_hist; ++i) {
	plot[i]->replot();
    }

}


void HistoWidget::setBackGroundColors(QColor * p_color)
{

    for (int i = 0; i < num_hist; ++i) {
	plot[i]->setCanvasBackground(p_color[i]);
	plot[i]->replot();
    }

}

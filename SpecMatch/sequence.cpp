/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
/*
 *  sequence.cpp
 *  
 *
 *  Created by Diana Mateus and David Knossow on 11/6/07.
 *  Copyright 2007 INRIA. All rights reserved.
 *
 * 
 */

#include "sequence.h"


Sequence::Sequence()
{

  graph_distance_type = EUCLIDIAN ;
  
  embed_method = LAP;


  C_Voxel = NULL;
  C_Mesh = NULL;
  C_Embedding = NULL;
    
  voxel_basename = NULL;
  embed_basename = NULL;
  mesh_basename = NULL;
      pattern = NULL;

  filename = new char[STRLEN];
  nei_basename = NULL;
  lbl_basename = NULL;


  n_frames = 0;
  current_f = 0;
  function_loaded = false;

  voxel_format = VOXEL ;

  K = 0;

  use_depth_as_dist = false;

  enable_save_distmat = false;
  enable_save_eigvalues = true;
  enable_save_parameters = true;
  
  

  normalization_factor_power = 1;
  depth = 1;


  normalize_embed = NO_NORM_EMBED ;

  thres_embed_small_norms = -1 ;

}

Sequence::~Sequence()
{

  if (C_Voxel) {
    for (int i = 0; i < n_frames; i++)
      delete C_Voxel[i];
    delete[]C_Voxel;
  }

  if (C_Mesh) {
    for (int i = 0; i < n_frames; i++)
      delete C_Mesh[i];
    delete[]C_Mesh;
  }

    
  if (C_Embedding) {
    for (int i = 0; i < n_frames; i++)
      delete C_Embedding[i];
    delete [] C_Embedding;
  }

  if (filename)
    delete[]filename;
  if (voxel_basename)
    delete[]voxel_basename;
  if (mesh_basename)
    delete[]mesh_basename;
      if (nei_basename)
    delete[]nei_basename;
  if (embed_basename)
    delete[]embed_basename;
  if (pattern)
    delete[]pattern;

}

void Sequence::release()
{
  PRINT_(fprintf(stderr, "Sequence: release() \n"));

  C_Embed.release();

  if (filename) {
    delete[]filename;
    filename = NULL;
  }
  if (C_Voxel) {
    for (int i = 0; i < n_frames; i++) {
      delete C_Voxel[i];
    }
    delete[]C_Voxel;
    C_Voxel = NULL;
  }

  if (C_Mesh) {
    for (int i = 0; i < n_frames; i++)
      delete C_Mesh[i];
    delete[]C_Mesh;
    C_Mesh = NULL;
  }
        
  if (C_Embedding) {
    for (int i = 0; i < n_frames; i++)
      delete C_Embedding[i];
    delete[]C_Embedding;
    C_Embedding = NULL;
  }


  if (voxel_basename) {
    delete[]voxel_basename;
    voxel_basename = NULL;
  }
  if (mesh_basename) {
    delete[]mesh_basename;
    mesh_basename = NULL;
  }
      if (nei_basename) {
    delete[]nei_basename;
    nei_basename = NULL;
  }
  if (embed_basename) {
    delete[]embed_basename;
    embed_basename = NULL;
  }
  if (filename) {
    delete[]filename;
    filename = NULL;
  }
  if (pattern) {
    delete[]pattern;
    pattern = NULL;
  }

  PRINT_(fprintf(stderr, "Sequence: release() end \n"));

}


int Sequence::go()
{


  // For all frames, compute the embeddings.
  while (current_f < n_frames) {

    DBG_(fprintf(stderr, "-->FRAME %d\n", current_f));
    if (C_Voxel) {
      // Read the file.
      loadVoxelset();
      // Compute the embedding.
      if (callEmbed())
	return 1;

      if (C_Voxel[current_f]) {
	delete C_Voxel[current_f];
	C_Voxel[current_f] = NULL;
      }
      if (C_Embedding[current_f]) {
	delete C_Embedding[current_f];
	C_Embedding[current_f] = NULL;
      }
    }
    if (C_Mesh) {
      loadMeshset();
      callMeshEmbed();
      if (C_Mesh[current_f]) {
	delete C_Mesh[current_f];
	C_Mesh[current_f] = NULL;
      }
      if (C_Embedding[current_f]) {
	delete C_Embedding[current_f];
	C_Embedding[current_f] = NULL;
      }

    }
        





    current_f++;
  }
  return 0;
}



void Sequence::setEmbedParameters () {
  // set embedding parameters
  C_Embed.init () ;
  C_Embed.setMethod(embed_method, lap_mode, edge_type);
  C_Embed.C_Mat_dist.setParameters(dist_mode, K, epsilon, depth);
  C_Embed.setNEigenFunc(n_eigenfunc);
  C_Embed.setRemoveFirst(remove_first);
  C_Embed.setNormalizationFormat(normalization_format);
  if (lap_mode == DIFFUSION_DISTANCE) {
    PRINT_(std::cout << "normalization_factor_power : " << normalization_factor_power << std::endl );
    C_Embed.setNormalizationPower(normalization_factor_power);
  }
  if (sigma > 0.0)
    C_Embed.setSigma(sigma);


  C_Embed.enableCutDisconnectedComps (cut_disc_comps) ;

}


int Sequence::callEmbed()
{
  int l_retval = 0;
  DBG_(fprintf
       (stderr,
	"-->Starting to compute the embedding on VOXELS from frame %d\n",
	current_f));

  // CALCULATE EMBEDDING 
  if (!C_Voxel[current_f]) {
    ERROR_(fprintf
	   (stderr,
	    "-->Calling VOXEL embed failed, voxel[%d] is not initialized \n",
	    current_f), ERROR_EMPTY_DATA);
  }

  if (C_Embedding[current_f]) {
    delete C_Embedding[current_f];
    C_Embedding[current_f] = NULL;
  }

  C_Embedding[current_f] = new Embedding;

  if ((C_Voxel[current_f]->isLoaded())
      && (C_Voxel[current_f]->getNumOfVoxels() > 0)) {

    DBG_(fprintf(stderr, "-->Start Estimation of Pairwise matrix\n"));

    //Set Neighborhoods Filename
    if (nei_basename) {
      sprintf(filename, pattern, nei_basename, current_f + start);
      strcat(filename, ".nei");
      l_retval = C_Embed.C_Mat_dist.setNeiFilename(filename);
      if (l_retval)
	return l_retval;
    }
    //reinitialize with new data
    FL *l_vox = C_Voxel[current_f]->getVoxels();
    C_Embed.C_Mat_dist.loadData(l_vox,
				C_Voxel[current_f]->getNumOfVoxels(),
				C_Voxel[current_f]->getDim());

    setEmbedParameters () ;


    //EMBED
    DBG_(fprintf(stderr, "-->Start Embedding\n"));
    C_Embed.findEmbeddingCoords() ;
    DBG_(fprintf(stderr, "-->End Embedding\n"));

    printOutputs () ;

  } else {
    ERROR_(fprintf
	   (stderr,
	    "-->Calling VOXEL embed failed, voxel[%d]->getNunOfVoxels() might be <= 0 or may noy not be loaded \n",
	    current_f), ERROR_VARIABLE_INIT);
  }

  return l_retval;
}





int Sequence::callMeshEmbed()
{
  DBG_(fprintf
       (stderr,
	"-->Starting to compute the embedding on MESH from frame %d\n",
	current_f));

  if (!C_Mesh[current_f]) {
    ERROR_(fprintf
	   (stderr,
	    "-->Calling MESH  embed failed, mesh[%d] is not initialized \n",
	    current_f), ERROR_EMPTY_DATA);
  }

  if (C_Embedding[current_f]) {
    delete C_Embedding[current_f];
    C_Embedding[current_f] = NULL;
  }

  C_Embedding[current_f] = new Embedding;

  // The distance from a point to its neighbors is computed. 
#if 0
  if (C_Mesh[current_f]->isLoaded()) {
    C_Mesh[current_f]->buildDistMat(depth);
    C_Mesh[current_f]->buildNeiColormap(depth);
  }
#endif

  if ((C_Mesh[current_f]->isLoaded())
      && (C_Mesh[current_f]->getNumOfVertex() > 0)) {



        if (embed_method == LLE){
      C_Embed.loadPoints(C_Mesh[current_f]->getVertices());
    }
        DBG_(fprintf(stderr, "-->Start Estimation of Pairwise matrix\n"));

    setEmbedParameters () ;



    //EMBED
    DBG_(fprintf(stderr, "-->Start Embedding\n"));
    C_Embed.findEmbeddingCoords(C_Mesh[current_f]->getFLSparseWeight(),
				C_Mesh[current_f]->getNumOfVertex(),
				C_Mesh[current_f]->getNzelems());
    DBG_(fprintf(stderr, "-->End Embedding\n"));

    printOutputs () ;
  } else {
    ERROR_(fprintf
	   (stderr,
	    "-->Calling MESH embed failed, mesh[%d]->get_nvertex() might be <= 0 or may not be loaded \n",
	    current_f), ERROR_VARIABLE_INIT);
  }

  return 0;
}



void Sequence::printOutputs () {


  char temp_filename[STRLEN] ;
  memset(temp_filename,0, STRLEN * sizeof(char)) ;
  //save results
  bool print_on = true;
  if ((print_on) && (embed_basename)) {
    //sprintf(embed_basename, "tmp/voxel");
    WARNING_(fprintf(stderr, "-->Save Embedding\n"));
    sprintf(temp_filename, pattern, embed_basename, current_f + start);
    sprintf(filename, "%s.emb", temp_filename);
    C_Embed.saveEmbedding(filename);
            
    if (enable_save_distmat) {
      WARNING_(fprintf(stderr, "-->Save Distance\n"));
      sprintf(temp_filename, pattern, embed_basename,
	      current_f + start);
      sprintf(filename, "%s.dist", temp_filename);
      C_Embed.saveDistMat(filename);
    }
            
    if (enable_save_eigvalues) {
      WARNING_(fprintf(stderr, "-->Save Eigenvalues\n"));
      sprintf(temp_filename, pattern, embed_basename,
	      current_f + start);
      sprintf(filename, "%s.eval", temp_filename);
      C_Embed.saveEigenValues(filename);
    }
            
    if (enable_save_parameters) {
      WARNING_(fprintf
	       (stderr, "-->Save embedding parameters\n"));
      sprintf(temp_filename, pattern, embed_basename,
	      current_f + start);
      sprintf(filename, "%s.params", temp_filename);
      C_Embed.saveParams(filename);
    }
    


      }

}

int Sequence::loadVoxelset()
{

  PRINT_COLOR_(fprintf
	       (stderr,
		"\n------------------NEXT-------------------\n"), RED);
  DBG_(fprintf(stderr, "-->Reading voxelset\n"));

  if (voxel_basename) {

    if (C_Voxel && C_Voxel[current_f]
	&& C_Voxel[current_f]->isLoaded())
      DBG_(fprintf(stderr, "Overwriting loaded voxelset\n"));
    if (C_Voxel && C_Voxel[current_f]) {
      delete C_Voxel[current_f];
      C_Voxel[current_f] = NULL;
    }
    C_Voxel[current_f] = new Voxel;

    sprintf(filename, pattern, voxel_basename, current_f + start);
    DBG_(fprintf
	 (stderr, "Reading frame %d : %s\n", current_f + start,
	  filename));


    switch (voxel_format) {
    case VOXEL:
      C_Voxel[current_f]->readPoints(filename);
      break;
    case VOXEL_EMBED:
      C_Voxel[current_f]->readVoxelsAndFunctionValue(filename);
      function_loaded = true;
      break;
    case GRID:
      C_Voxel[current_f]->readMatlabVoxelGrid(filename);
      break;
    }
    return 0;
  } else {
    ERROR_(fprintf
	   (stderr, "The voxel filename is not set correctly \n"),
	   ERROR_OPEN_FILE);
  }
}

int Sequence::loadMeshset()
{
  PRINT_COLOR_(fprintf
	       (stderr,
		"\n------------------NEXT--------------------\n"), RED);
  DBG_(fprintf(stderr, "-->Reading meshset\n"));
  if (mesh_basename) {
    if (C_Mesh[current_f] && C_Mesh[current_f]->isLoaded())
      DBG_(fprintf(stderr, "Overwriting loaded meshset\n"));
    if (C_Mesh[current_f]) {
      delete C_Mesh[current_f];
      C_Mesh[current_f] = NULL;
    }
    C_Mesh[current_f] = new Mesh;

    sprintf(filename, pattern, mesh_basename, current_f + start);
    DBG_(fprintf
	 (stderr, "Reading frame %d : %s\n", current_f + start,
	  filename));

    C_Mesh[current_f]->setDepthAsDist(use_depth_as_dist);
    C_Mesh[current_f]->setGraphDistanceType(graph_distance_type);
    C_Mesh[current_f]->readOffFile(filename);
    C_Mesh[current_f]->buildDistMat( depth ) ;
    C_Mesh[current_f]->buildNeiColormap(depth);
    return 0;
  } else {
    ERROR_(fprintf
	   (stderr, "The mesh filename is not set correctly \n"),
	   ERROR_OPEN_FILE);
  }
}






int Sequence::loadLabels()
{
  if (lbl_basename) {
    DBG_(fprintf(stderr, "-->Reading labels\n"));
    sprintf(filename, pattern, lbl_basename, current_f + start);
    strcat(filename, ".lbl");
    C_Voxel[current_f]->readLabelValue(filename);
    return 0;
  } else {
    ERROR_(fprintf
	   (stderr, "The label filename is not set correctly \n"),
	   ERROR_OPEN_FILE);
  }
}

int Sequence::loadEmbedding()
{
  if (embed_basename) {
    if (C_Embedding[current_f]) {
      delete C_Embedding[current_f];
      C_Embedding[current_f] = NULL;
    }

    C_Embedding[current_f] = new Embedding;

    PRINT_(fprintf(stderr, "-->Reading embedding\n"));
    sprintf(filename, pattern, embed_basename, current_f + start);
    strcat(filename, ".emb");
    C_Embedding[current_f]->readEmbedding(filename, n_eigenfunc,
					  remove_first);
    sprintf(filename, pattern, embed_basename, current_f + start);
    strcat(filename, ".eval");
    C_Embedding[current_f]->readEigenvalues(filename,
					    C_Embedding[current_f]->
					    getDim());
    return 0;
  } else {
    ERROR_(fprintf
	   (stderr, "The embedding filename is not set correctly \n"),
	   ERROR_OPEN_FILE);
  }
}





int Sequence::loadEmbeddingParams( int p_method,
				   int p_lap_mode, int p_dist_mode,
				   int p_dim, int p_k, FL p_eps, FL p_sigma,
				   int p_edge_type,
				   bool p_use_depth_as_dist, GRAPH_DISTANCE_TYPE p_graph_distance,
				   bool p_remove_first,
				   NORMALIZATION_FORMAT p_format, bool p_cut_disc_comps, NORMALIZATION_EMBEDDING p_normalize_embed, FL p_thres_embed_small_norms )
{

  embed_method = (METHOD) p_method;
  dist_mode = (NEIGHBORHOOD) p_dist_mode ;	//FULL,SPARSE, LOCAL_GEO,LLR, FULL_GEO 
  lap_mode = (LAPLACIAN) p_lap_mode ;	//NJW,SHIMEILA,EIGENMAPS,ADJACENCY, DIFFUSION_DISTANCE
  n_eigenfunc = p_dim;



  if (dist_mode == LOCAL_GEO) {

    depth = p_k;
    seq_depth = p_k;
    K = 0;
    epsilon = 0.0;

    if (lap_mode == DIFFUSION_DISTANCE) {
      normalization_factor_power = depth;
      depth = 1;
    }

  }
    sigma = p_sigma;
  edge_type = (EDGES) p_edge_type;
  use_depth_as_dist = p_use_depth_as_dist ;
  graph_distance_type = p_graph_distance ;
  remove_first = p_remove_first;

  normalization_format = p_format;

  normalize_embed = p_normalize_embed ;

  cut_disc_comps = p_cut_disc_comps ;

  thres_embed_small_norms = p_thres_embed_small_norms ;

  return 0;
}
int Sequence::loadVoxelParams(char *p_voxel_basename, char *p_pattern,
			      int p_nframes, int p_start,
			      char *p_nei_basename, char *p_embed_basename,
			      char *p_lbl_basename, VOXEL_TYPE p_format)
{
  if (voxel_basename) {
    delete[]voxel_basename;
    voxel_basename = NULL;
  }
  if (nei_basename) {
    delete[]nei_basename;
    nei_basename = NULL;
  }
  if (embed_basename) {
    delete[]embed_basename;
    embed_basename = NULL;
  }
  if (lbl_basename) {
    delete[]lbl_basename;
    lbl_basename = NULL;
  }
  if (pattern) {
    delete[]pattern;
    pattern = NULL;
  }
  if (p_voxel_basename) {
    voxel_basename = new char[STRLEN];
    sprintf(voxel_basename, "%s", p_voxel_basename);
  }
  if (p_pattern) {
    pattern = new char[STRLEN];
    sprintf(pattern, "%s", p_pattern);
  }


  start = p_start;
  n_frames = p_nframes;

  if (p_nei_basename) {
    nei_basename = new char[STRLEN];
    sprintf(nei_basename, "%s", p_nei_basename);
  }
  if (p_embed_basename) {
    embed_basename = new char[STRLEN];
    sprintf(embed_basename, "%s", p_embed_basename);
  }
  if (p_lbl_basename) {
    lbl_basename = new char[STRLEN];
    sprintf(lbl_basename, "%s", p_lbl_basename);
  }
  DBG_(fprintf
       (stderr, "Sequence %s with %d frames\n", voxel_basename,
	n_frames));

  if (C_Voxel) {
    for (int i = 0; i < n_frames; ++i) {
      delete C_Voxel[i];
    }
    delete[]C_Voxel;
    C_Voxel = NULL;
  }
  if (C_Embedding) {
    for (int i = 0; i < n_frames; ++i) {
      delete C_Embedding[i];
    }
    delete[]C_Embedding;
    C_Embedding = NULL;
  }

  C_Voxel = new Voxel *[n_frames];
  C_Embedding = new Embedding *[n_frames];
  for (int i = 0; i < n_frames; i++) {
    if (C_Voxel)
      C_Voxel[i] = NULL;
    if (C_Embedding)
      C_Embedding[i] = NULL;
  }

  voxel_format = p_format;


  return 0;
}




int Sequence::loadMeshParams(char *p_mesh_basename, char *p_pattern,
			     int p_nframes, int p_start,
			     char *p_embed_basename)
{
  if (mesh_basename) {
    delete[]mesh_basename;
    mesh_basename = NULL;
  }
  if (nei_basename) {
    delete[]nei_basename;
    nei_basename = NULL;
  }
  if (embed_basename) {
    delete[]embed_basename;
    embed_basename = NULL;
  }
  if (lbl_basename) {
    delete[]lbl_basename;
    lbl_basename = NULL;
  }
  if (pattern) {
    delete[]pattern;
    pattern = NULL;
  }
  if (p_mesh_basename) {
    mesh_basename = new char[STRLEN];
    sprintf(mesh_basename, "%s", p_mesh_basename);
  }
  if (p_pattern) {
    pattern = new char[STRLEN];
    sprintf(pattern, "%s", p_pattern);
  }


  start = p_start;
  n_frames = p_nframes;

  if (p_embed_basename) {
    embed_basename = new char[STRLEN];
    sprintf(embed_basename, "%s", p_embed_basename);
  }

  DBG_(fprintf
       (stderr, "Sequence %s with %d frames\n", mesh_basename,
	n_frames));

  if (C_Mesh) {
    delete C_Mesh;
    C_Mesh = NULL;
  }
  if (C_Embedding) {
    for (int i = 0; i < n_frames; i++) {
      delete C_Embedding[i];
    }
    delete[]C_Embedding;
    C_Embedding = NULL;
  }

  C_Mesh = new Mesh *[n_frames];
  C_Embedding = new Embedding *[n_frames];
  for (int i = 0; i < n_frames; i++) {
    if (C_Mesh)
      C_Mesh[i] = NULL;
    if (C_Embedding)
      C_Embedding[i] = NULL;
  }

  return 0;
}




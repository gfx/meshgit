/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
/*
 *  visual_shape.cpp
 *  visu
 *
 *  Created by Diana Mateus and David Knossow on 10/6/07.
 *  Modified by Avinash Sharma on 11/12/08.
 *  Copyright 2007 INRIA. All rights reserved.
 *
 */



#if QT_VERSION > 0x04000
#include <QMenu>
#include <QKeyEvent>
#include <QString>
#else
# include <qevent.h>
#include <qstring.h>
#endif

#include "visual_shape.h"

using namespace std;

VisualShape::VisualShape()
{

  //list initialization
  BIN_EMBED_MATCHLIST = NULL;
  PROBA_EMBED_MATCHLIST = NULL;
  BIN_SHAPE_MATCHLIST = (uint) (-1);

  
  PROBA_SHAPE_MATCHLIST = (uint) (-1);

  //embedding list init 
  embed3d_nlist = 0;
  embed3d_id = 0;

  //trio_list
  trio_list_1 = NULL;
  trio_list_2 = NULL;

  

  //display parameters
  save_on = false;
  dispmode = SHAPE3D;
  animate_on = false;
  model_sep[0] = 0.5f;	//0.5 for mannequin 
  model_sep[1] = 0.0f;	// 0.2f;//0.5 for hand
  model_sep[2] = 0.0f;


  model_rot[0] = 0.5f;	//0.5 for mannequin 
  model_rot[1] = 0.0f;	// 0.2f;//0.5 for hand
  model_rot[2] = 0.0f;

  show_match = false;
  color_match1 = false;
  color_match2 = false;
  set_first = 0;
  set_size = 0;


  seld[0] = 0;
  seld[1] = 1;
  seld[2] = 2;
  current_sel_dim = 2;

  

  }



VisualShape::~VisualShape()
{

  if (BIN_SHAPE_MATCHLIST != (uint) (-1)) {
    glDeleteLists(BIN_SHAPE_MATCHLIST, 1);
  }
  
  if (PROBA_SHAPE_MATCHLIST != (uint) (-1))
    glDeleteLists(PROBA_SHAPE_MATCHLIST, 1);

  if (PROBA_EMBED_MATCHLIST) {
    for (int i = 0; i < embed3d_nlist; i++) {
      if (PROBA_EMBED_MATCHLIST[i] != (uint) (-1))
	glDeleteLists(PROBA_EMBED_MATCHLIST[i], 1);
    }
    delete[]PROBA_EMBED_MATCHLIST;
  }

  if (BIN_EMBED_MATCHLIST) {
    for (int i = 0; i < embed3d_nlist; i++) {
      if (BIN_EMBED_MATCHLIST[i] != (uint) (-1))
	glDeleteLists(BIN_EMBED_MATCHLIST[i], 1);
    }
    delete[]BIN_EMBED_MATCHLIST;
  }

  if (trio_list_1) {
    for (int i = 0; i < embed3d_nlist; i++)
      if (trio_list_1[i] != (uint) (-1)) {
	glDeleteLists(trio_list_1[i], 1);
	trio_list_1[i] = (uint) (-1);
      }
    delete[]trio_list_1;
    trio_list_1 = NULL;
  }
  if (trio_list_2) {
    for (int i = 0; i < embed3d_nlist; i++)
      if (trio_list_2[i] != (uint) (-1)) {
	glDeleteLists(trio_list_2[i], 1);
	trio_list_2[i] = (uint) (-1);
      }
    delete [] trio_list_2;
    trio_list_2 = NULL;
  }


  
}




// Drawing function
void VisualShape::draw()
{
  DBG_VISU_(fprintf(stderr, "VisualShape::draw()\n"));
  GLdouble l_modelview[16];
  glGetDoublev(GL_MODELVIEW_MATRIX, l_modelview);

  switch (dispmode) {
  case SHAPE3D:
    if ((C_Voxel1->isLoaded()) && (C_Voxel2->isLoaded()) && (match)) {
      if (show_match)
	drawBinShapeMatchList();	//depends on model_sep

      glLoadMatrixd(l_modelview);
      glMatrixMode(GL_MODELVIEW);
      glTranslatef(model_sep[0], model_sep[1], model_sep[2]);
      C_Voxel1->drawVoxelsetList();

      glLoadMatrixd(l_modelview);

      glTranslatef(-model_sep[0], -model_sep[1], -model_sep[2]);
      glRotatef(model_rot[0], 1,0,0) ;
      glRotatef(model_rot[1], 0,1,0) ;
      glRotatef(model_rot[2], 0,0,1) ;

      C_Voxel2->drawVoxelsetList();
    }

    if ((C_Mesh1->isLoaded()) && (C_Mesh2->isLoaded()) && (match) && (!C_Mesh3->isLoaded())) {
      if (show_match)
	drawBinShapeMatchList();	//depends on model_sep
      
      glLoadMatrixd(l_modelview);
      glMatrixMode(GL_MODELVIEW);
      
      glPushMatrix () ;
      glTranslatef(model_sep[0], model_sep[1], model_sep[2]);
      C_Mesh1->drawMeshsetFacets();
      glPopMatrix () ;
      
      
      glPushMatrix () ;
      glTranslatef(-model_sep[0], -model_sep[1], -model_sep[2]);
      glRotatef(model_rot[0],1,0,0) ;
      glRotatef(model_rot[1], 0,1,0) ;
      glRotatef(model_rot[2], 0,0,1) ;
      
      C_Mesh2->drawMeshsetFacets();
      glPopMatrix () ;
      
      
    }

    else if ((C_Mesh1->isLoaded()) || (C_Mesh2->isLoaded()) || (C_Mesh3->isLoaded()) ||  (C_Mesh4->isLoaded())) {


      glLoadMatrixd(l_modelview);
      glMatrixMode(GL_MODELVIEW);
      if (C_Mesh1->isLoaded()) {
	glPushMatrix () ;
	glTranslatef(3 * model_sep[0], 3 * model_sep[1], 3 * model_sep[2]);
	C_Mesh1->drawMeshsetFacets();
	glPopMatrix () ;
      }
      if (C_Mesh3->isLoaded()) {
	glPushMatrix () ;
	glTranslatef(model_sep[0], model_sep[1], model_sep[2]);
	C_Mesh3->drawMeshsetFacets();
	glPopMatrix () ;
      }
      if (C_Mesh4->isLoaded()) {
	glPushMatrix () ;
		
	glTranslatef(-model_sep[0], -model_sep[1], -model_sep[2]);
	C_Mesh4->drawMeshsetFacets();
	glPopMatrix () ;
      }
      if (C_Mesh2->isLoaded()) {
	glPushMatrix () ;
		
	glLoadMatrixd(l_modelview);
	glTranslatef(- (3 * model_sep[0]), - (3 * model_sep[1]), - (3 * model_sep[2]));
	glRotatef(model_rot[0],1,0,0) ;
	glRotatef(model_rot[1], 0,1,0) ;
	glRotatef(model_rot[2], 0,0,1) ;
		
	C_Mesh2->drawMeshsetFacets();
	glPopMatrix () ;
      }





    }
        break;
  case EMBED:
    if ((C_Embed1->isLoaded()) && (C_Embed2->isLoaded()) && (match)) {

      embed3d_id = 0;
      if (show_match) {
	drawBinEmbedMatchList();	//depends on model_sep
      }
      glLoadMatrixd(l_modelview);
      glMatrixMode(GL_MODELVIEW);

      glTranslatef( model_sep[0], model_sep[1], model_sep[2]);
      drawTransfoCoordsList(0, embed3d_id);
	
      glLoadMatrixd(l_modelview); 

      glTranslatef( -model_sep[0], -model_sep[1], -model_sep[2]);
      drawTransfoCoordsList(1, embed3d_id);
        
    }
    break;

  case ANIMATE_MATCH:
    if ((C_Voxel1->isLoaded()) && (C_Voxel2->isLoaded()) && (match)) {
      animateMatch();
      glLoadMatrixd(l_modelview);
      glMatrixMode(GL_MODELVIEW);
      glTranslatef(model_sep[0], model_sep[1], model_sep[2]);
      C_Voxel1->drawVoxelsetList();

      glLoadMatrixd(l_modelview);
      glTranslatef(-model_sep[0], -model_sep[1], -model_sep[2]);
      glRotatef(model_rot[0],1,0,0) ;
      glRotatef(model_rot[1], 0,1,0) ;
      glRotatef(model_rot[2], 0,0,1) ;

      C_Voxel2->drawVoxelsetList();
    }
    else if ((C_Mesh1->isLoaded()) && (C_Mesh2->isLoaded()) && (match)) {
      animateMatch();

      glLoadMatrixd(l_modelview);
      glMatrixMode(GL_MODELVIEW);
      glTranslatef(model_sep[0], model_sep[1], model_sep[2]);
      C_Mesh1->drawMeshsetFacets();

      glLoadMatrixd(l_modelview);
      glTranslatef(-model_sep[0], -model_sep[1], -model_sep[2]);
      glRotatef(model_rot[0],1,0,0) ;
      glRotatef(model_rot[1], 0,1,0) ;
      glRotatef(model_rot[2], 0,0,1) ;
      C_Mesh2->drawMeshsetFacets();

    }
    break;
    
    

  default:
    break;
  }
  if (save_on ) {
    QString l_qs = snapshotFileName();
    if (match_last_it == match_first_it) {
#if QT_VERSION < 0x040000
      PRINT_(fprintf(stderr, "Saving image %s\n", l_qs.ascii()));
#else
      PRINT_(fprintf
	     (stderr, "Saving image %s\n", (char *) l_qs.data()));
#endif
      saveSnapshot(true, false);
    } else if (!match_saved) {
#if QT_VERSION < 0x040000
      PRINT_(fprintf(stderr, "Saving image %s\n", l_qs.ascii()));
#else
      PRINT_(fprintf
	     (stderr, "Saving image %s\n", (char *) l_qs.data()));
#endif
      saveSnapshot(true, true);
      match_saved = true;
    }
  }






  //  if (selectedName() > -1)
    //    std::cout << "Mesh index selected is : " << selectedName() << std::endl ;











}

void VisualShape::animate()
{

  DBG3_(fprintf(stderr, "VisualShape::animate()"));
  if (animate_on && dispmode == ANIMATE_MATCH) {
    refreshAllInputs();
    updateGL();
  }

}

int VisualShape::next()
{
  if (dispmode == ANIMATE_MATCH) {
    set_first += set_size;
    if ((set_first > nmatch)) {
      set_first = 0;
    }
    DBG2_(fprintf(stderr, "next %d %d\n", set_first, set_size));
  }
  return 1;
}

int VisualShape::previous()
{
  if (dispmode == ANIMATE_MATCH) {
    set_first -= set_size;
    if ((set_first < 0)) {
      set_first = nmatch - set_size - 1;
    }
    DBG2_(fprintf(stderr, "next %d %d\n", set_first, set_size));
  }
  return 1;
}

void VisualShape::toggleAnimation()
{
  animate_on = !animate_on;
  if (animate_on)
    startAnimation();
  else
    stopAnimation();
}


int VisualShape::updateMatchFiles(int i)
{
  if (!match_pattern
      && !(voxel_pattern || mesh_pattern || embed_pattern)) {
    ERROR_(fprintf
	   (stderr,
	    "Can not update match file. No pattern has been loaded\n"
	    "Use Shape::set_matchfile_pattern\n"), ERROR_OPEN_FILE);
  }



  //Update Match
  char *l_new_filename = NULL;
  int l_str_length = 0 ;
  HASH_MAP_PARAM_(int, "str_length", Int, l_str_length) ;
    
  l_new_filename = new char[l_str_length];

  int l_read_fail = 0;
  if (voxel_pattern) {
    DBG_(fprintf
	 (stderr, "Update Voxels %d %d\n", match_it,
	  match_it + i * match_delta));
    sprintf(l_new_filename, voxel_pattern, match_it);
    l_read_fail += C_Voxel1->readPoints(l_new_filename);
    sprintf(l_new_filename, voxel_pattern, match_it + i * match_delta);
    l_read_fail += C_Voxel2->readPoints(l_new_filename);
  }
  if (mesh_pattern) {
    DBG_(fprintf
	 (stderr, "Update Mesh %d %d \n", match_it,
	  match_it + i * match_delta));
    sprintf(l_new_filename, mesh_pattern,
	    match_it /*+ i*match_delta */ );
    l_read_fail += C_Mesh1->readOffFile(l_new_filename);


    sprintf(l_new_filename, mesh_pattern, match_it + i * match_delta);
    l_read_fail += C_Mesh2->readOffFile(l_new_filename);
  }
  if (embed_pattern) {
    DBG_(fprintf
	 (stderr, "Update Embedding %d %d\n", match_it,
	  match_it + i * match_delta));
    sprintf(l_new_filename, embed_pattern, match_it);
    l_read_fail += C_Embed1->readEmbedding(l_new_filename);
    sprintf(l_new_filename, embed_pattern, match_it + i * match_delta);
    l_read_fail += C_Embed2->readEmbedding(l_new_filename);
  }
  // depends on previous loaded voxels
  if (match_pattern) {
    DBG_(fprintf(stderr, "Update Match\n"));
    sprintf(l_new_filename, "%s_%04d", match_pattern, match_it);
    std::cout << "Reading match file : " << l_new_filename << std::
      endl;
    l_read_fail += readBinaryMatch(l_new_filename);	//internaly overwrites the binmatch_filename variable;
    setSnapshotFileName(save_filename);
    setSnapshotCounter(match_it);
  }
  if (l_new_filename)
    delete[]l_new_filename;



  if (enable_reshape_binary_match) {
    PRINT_COLOR_(std::cout << "Sorting the matchings" <<std::endl,RED) ;
    reshapeBinaryMatch ( C_Mesh1->getIndexSorting (), C_Mesh2->getIndexSorting () ) ;
  }


  if (l_read_fail) {
    WARNING_(fprintf(stderr, "Problem while updating files\n"));
  } else
    initColorsAndShapes();


  if (match_it + i * match_delta > match_last_it) {
    match_it = match_first_it;
  } else if (match_it + i * match_delta < 0) {
    match_it =
      match_last_it;
  } else
    match_it += i * match_delta;

  DBG_(fprintf
       (stderr,
	"Update sequence with match pattern : %s \n\t Match Iteration %d\n",
	match_pattern, match_it));




  match_saved = false;

  return 0;
}

void VisualShape::keyPressEvent(QKeyEvent * e)
{

  int l_newd = 0;
  int l_dim = 0;

  if (C_Embed2)
    l_dim = C_Embed2->getDim();

  switch (e->key()) {
  case Qt::Key_Space:
    //	toggleAnimation();
    if (save_on) {	    
      setSnapshotFormat("PNG");
      saveSnapshot( );
      save_on = false ;
    }
	
    break;

      case Qt::Key_B:
    HASH_MAP_PARAM_(int, "set_num_of_match_to_disp", Int, set_size) ;
    show_match = true;
    if (dispmode != ANIMATE_MATCH)
      dispmode = ANIMATE_MATCH;
    else
      previous();
    updateGL();
    break;

  case Qt::Key_N:
    HASH_MAP_PARAM_(int, "set_num_of_match_to_disp", Int, set_size) ;
    show_match = true;
    if (dispmode != ANIMATE_MATCH)
      dispmode = ANIMATE_MATCH;
    else
      next();
    updateGL();
    break;

  case Qt::Key_U:
    refreshAllInputs();
    updateGL();
    break;


  case Qt::Key_C:
    if (!color_match1 && !color_match2) {
      color_match1 = true;
      color_match2 = false;
    } else if (color_match1 && !color_match2) {
      color_match1 = false;
      color_match2 = true;
    } else if (color_match2 && !color_match1) {
      color_match1 = true;
      color_match2 = true;
    } else if (color_match2 && color_match1) {
      color_match1 = false;
      color_match2 = false;

    }

    if ((C_Voxel1->isLoaded()) && (C_Voxel2->isLoaded()) && (match)) {
      buildBinaryVoxelMatchList(vizu_sampling);
    }
    if ((C_Mesh1->isLoaded()) && (C_Mesh2->isLoaded()) && (match)) {
      buildBinaryMeshMatchList(vizu_sampling);
    }
    if (C_Embed1->isLoaded() && C_Embed2->isLoaded() && match) {
      buildBinaryEmbedMatchList(vizu_sampling);
    }
        updateGL();

    break;

  case Qt::Key_M:
    if (dispmode == ANIMATE_MATCH) {
      dispmode = SHAPE3D ;
      set_size = nmatch;
      set_first = 0;
      show_match = false ;
    }
    if (show_match) {
      show_match = false;
    } else {
      show_match = true;
      set_size = nmatch;
      set_first = 0;
    }
    updateGL();
    break;

  case Qt::Key_E:
    if (C_Embed1->isLoaded() && C_Embed2->isLoaded()) {
      dispmode = EMBED;
    }
    else {
      WARNING_(std::cout <<"Embeddings should be loaded using the [-e1] and [-e2] options, in the command line" <<std::endl) ;
    }
    updateGL();

    break;
  case Qt::Key_V:
    if (dispmode != SHAPE3D)
      dispmode = SHAPE3D;
    initColorsAndShapes();
    updateGL();
    break;
  case Qt::Key_F:
    updateMatchFiles(1);
    updateGL();
    break;
  case Qt::Key_D:
    updateMatchFiles(-1);
    updateGL();
    break;
    	
    
  case Qt::Key_X:
    setSnapshotFileName(save_filename);
    if (save_on)
      save_on = false;
    else
      save_on = true;
    break;




  case Qt::Key_P:
    if (dispmode != SHAPE3D)
      dispmode = SHAPE3D;
    if (C_Voxel2) {
      C_Voxel2->setAllPointsColor(0.0, 0.8, 0.0);
      C_Voxel2->buildPointsetList();
    }
    if (C_Voxel1) {
      C_Voxel1->setAllPointsColor(0.0, 0.0, 0.8);
      C_Voxel1->buildPointsetList();
    }
    if (C_Mesh1) {
      C_Mesh1->setAllPointsColor(0.5, 0.5, 0.5);
      C_Mesh1->buildPointsetList();
    }
    if (C_Mesh2) {
      C_Mesh2->setAllPointsColor(0.5, 0.5, 0.5);
      C_Mesh2->buildPointsetList();
    }
        updateGL();
    break;
  case Qt::Key_1:
    current_sel_dim = 0;
    updateGL();
    break;
  case Qt::Key_2:
    current_sel_dim = 1;
    updateGL();
    break;
  case Qt::Key_3:
    current_sel_dim = 2;
    updateGL();
    break;
      case Qt::Key_Plus:
    l_newd = seld[current_sel_dim];
    l_newd++;
    if (l_newd >= l_dim)
      l_newd = 0;
    seld[current_sel_dim] = l_newd;
    buildTransfoCoordsLists();
    buildBinaryEmbedMatchList();
        updateGL();
    break;
  case Qt::Key_Minus:
    l_newd = seld[current_sel_dim];
    l_newd--;
    if (l_newd < 0)
      l_newd = l_dim -1;
    seld[current_sel_dim] = l_newd;
    buildTransfoCoordsLists();
    buildBinaryEmbedMatchList();
        updateGL();
    break;

  default:
    QGLViewer::keyPressEvent(e);

  }
}

void VisualShape::init()
{
  DBG3_(fprintf(stderr, "VisualShape::init()\n"));

  HASH_MAP_PARAM_(int, "vizu_sample", Int, vizu_sampling) ;



  setKeyDescription(Qt::Key_Space, "Toggles Annimation ON/OFF");
  setKeyDescription(Qt::Key_U, "Re-read the match file on the disk");
  setKeyDescription(Qt::Key_B, "Displays part of the matches");
  setKeyDescription(Qt::Key_N, "Displays part of the matches");
  setKeyDescription(Qt::Key_C,
		    "Toggles the color modes to display matches");
  setKeyDescription(Qt::Key_E, "Display embeddings");
  
  setKeyDescription(Qt::Key_V,
		    "Re-initialize the 3D shape");
  setKeyDescription(Qt::Key_X, "Toggles Save Images ON/OFF");
  setKeyDescription(Qt::Key_P, "Display shape as points");
  setKeyDescription(Qt::Key_1, "Select 0th dimension");
  setKeyDescription(Qt::Key_2, "Select 1th dimension");
  setKeyDescription(Qt::Key_3, "Select 2th dimension");

  setKeyDescription(Qt::Key_Plus,
		    "Change forward the selected dimension");
  setKeyDescription(Qt::Key_Minus,
		    "Change backward the selected dimension");

  setShortcut(ENABLE_TEXT , 0) ;
  setShortcut(SAVE_SCREENSHOT, 0) ;  
  setShortcut(ANIMATION , 0) ;
  setShortcut(INCREASE_FLYSPEED, 0) ;	
  setShortcut(DECREASE_FLYSPEED, 0) ;
  setShortcut(EDIT_CAMERA, 0) ;
  setShortcut(STEREO, 0) ;
  setShortcut(DISPLAY_FPS, 0) ;
  setShortcut(CAMERA_MODE, 0) ;

  setMouseBindingDescription(Qt::ControlModifier + Qt::RightButton, "Activate the REMOVE state when performing pair selection");
  setMouseBindingDescription(Qt::ShiftModifier + Qt::RightButton, "Activate the ADD state when performing pair selection");
  setMouseBindingDescription(Qt::ShiftModifier + Qt::LeftButton, "Select on point on a mesh");
  setMouseBinding(Qt::ShiftModifier + Qt::RightButton, NO_CLICK_ACTION);
  setMouseBinding(Qt::ControlModifier + Qt::RightButton, NO_CLICK_ACTION);
  // Restore previous Shape state.
  restoreStateFromFile();


  // Define lighting
  float mat_shininess[] = { 100.0f };

    glClearColor(1.0, 1.0, 1.0, 1.0);


  float global_ambient[] = { 0.8f, 0.8f, 0.8f, 0.5f };
  glLightModelfv(GL_LIGHT_MODEL_AMBIENT, global_ambient);

  glShadeModel(GL_SMOOTH);

  glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);



  glEnable(GL_DEPTH_TEST);

  show_match = false;

  
  initColorsAndShapes();

  
  setAnimationPeriod(4000);

  setSnapshotFileName("match_snapshot");
  setSnapshotFormat("PNG");


  DBG3_(fprintf(stderr, "VisualShape::init() return \n"));

}


void VisualShape::initColorsAndShapes()
{
  DBG3_(fprintf(stderr, "VisualShape::initColorsAndShapes()\n"));

  //reset match params
  set_first = 0;
  set_size = nmatch;

  //voxels
  int *l_singlematch = NULL;
  int *l_matchf1 = NULL;
  int *l_matchf2 = NULL;

  if ((C_Voxel1->isLoaded()) && (C_Voxel2->isLoaded()) && (match)) {
    //fprintf(stderr,"Prepare lists for voxelset matching\n");

    bool l_separated_colormaps = false;	//build a colormap for each voxelset

    if (l_separated_colormaps) {
      l_singlematch = new int[nmatch];
      for (int i = 0; i < nmatch; i++)
	l_singlematch[i] = match[2 * i + match_id[0]];
      C_Voxel1->buildColormap(nmatch, l_singlematch);
      C_Voxel1->fillColorWithColormap();
      for (int i = 0; i < nmatch; i++)
	l_singlematch[i] = match[2 * i + match_id[1]];
      C_Voxel2->buildColormap(nmatch, l_singlematch);
      C_Voxel2->fillColorWithColormap();

      C_Voxel1->buildVoxelsetList();
      C_Voxel2->buildVoxelsetList();
    }

    else {			//SET Colors with match
      int l_n, l_n1, l_n2;
      l_n1 = C_Voxel1->getNumOfVoxels();
      l_matchf1 = new int[l_n1];	//color indices of pointset 1
      l_n2 = C_Voxel2->getNumOfVoxels();
      l_matchf2 = new int[l_n2];	//color indices of pointset 2
      l_n = min(l_n1, l_n2);
      C_Voxel1->buildColormap(l_n);
      C_Voxel2->buildColormap(l_n);

      int l_copy_color_from = match_id[0];	//copy color from voxel1 
      if (l_n == l_n2)	//unless C_Voxel2 has less number of voxels
	l_copy_color_from = match_id[1];
      for (int i = 0; i < l_n1; i++)
	l_matchf1[i] = -1;
      for (int i = 0; i < l_n2; i++)
	l_matchf2[i] = -1;
      int ind1, ind2, l_col;
      for (int i = 0; i < nmatch; i++) {
	ind1 = match[2 * i + match_id[0]];
	ind2 = match[2 * i + match_id[1]];
	if (ind1 < 0 || ind2 < 0)
	  continue;
	l_col = match[2 * i + l_copy_color_from];
	if ((ind1 >= l_n1) || (ind2 >= l_n2)) {
	  WARNING_(fprintf
		   (stderr,
		    "(Error: i %d ind1 %d ind2 %d color %d matchid(%d,%d)) ",
		    i, ind1, ind2, l_col, match_id[0],
		    match_id[1]));
	} else {	// if not assigned already assign color (keep the first color assigned usually the best)
	  if (l_matchf1[ind1] == -1)
	    l_matchf1[ind1] = l_col;
	  if (l_matchf2[ind2] == -1)
	    l_matchf2[ind2] = l_col;
	}
      }

      C_Voxel1->loadFunctionValue(l_matchf1, 1);
      C_Voxel1->fillColorWithFunctionValue(0);
      C_Voxel2->loadFunctionValue(l_matchf2, 1);
      C_Voxel2->fillColorWithFunctionValue(0);
      FL l_s1 = C_Voxel1->getScale();
      FL l_s2 = C_Voxel2->getScale();
      if (l_s1 > l_s2) {
	C_Voxel2->setScale(l_s1);
	l_s2 = l_s1;
      } else if (l_s2 > l_s1) {
	C_Voxel1->setScale(l_s2);
	l_s1 = l_s2;
      }
      C_Voxel1->buildVoxelsetList();
      C_Voxel2->buildVoxelsetList();


      //BUILD MATCH LIST
      buildBinaryVoxelMatchList(vizu_sampling);

    }
  }


  else if ((C_Mesh1->isLoaded()) && (C_Mesh2->isLoaded()) && (match)) {
    //fprintf(stderr,"Prepare lists for meshset matching\n");

    bool l_separated_colormaps = false;	//build a colormap for each meshset

    if (l_separated_colormaps) {
      l_singlematch = new int[nmatch];
      for (int i = 0; i < nmatch; i++)
	l_singlematch[i] = match[2 * i + match_id[0]];
      C_Mesh1->buildColormap(nmatch, l_singlematch);
      C_Mesh1->fillColorWithColormap();
      for (int i = 0; i < nmatch; i++)
	l_singlematch[i] = match[2 * i + match_id[1]];
      C_Mesh2->buildColormap(nmatch, l_singlematch);
      C_Mesh2->fillColorWithColormap();

      C_Mesh1->buildMeshsetList();
      C_Mesh2->buildMeshsetList();
    }

    else {			//SET Colors with match
      int l_n, l_n1, l_n2;
      l_n1 = C_Mesh1->getNumOfVertex();
      l_matchf1 = new int[l_n1];	//color indices of pointset 1
      l_n2 = C_Mesh2->getNumOfVertex();
      l_matchf2 = new int[l_n2];	//color indices of pointset 2
      l_n = min(l_n1, l_n2);
      C_Mesh1->buildColormap(l_n);
      C_Mesh2->buildColormap(l_n);

      int l_copy_color_from = match_id[0];	//copy color from C_Mesh1 
      if (l_n == l_n2)	//
	l_copy_color_from = match_id[1];
      for (int i = 0; i < l_n1; i++)
	l_matchf1[i] = -1;
      for (int i = 0; i < l_n2; i++)
	l_matchf2[i] = -1;
      int ind1, ind2, l_col;
      for (int i = 0; i < nmatch; i++) {
	ind1 = match[2 * i + match_id[0]];
	ind2 = match[2 * i + match_id[1]];
	if (ind1 < 0 || ind2 < 0)
	  continue;
	l_col = match[2 * i + l_copy_color_from];
	if ((ind1 >= l_n1) || (ind2 >= l_n2)) {
	  WARNING_(fprintf
		   (stderr,
		    "(Error: i %d ind1 %d ind2 %d color %d matchid(%d,%d)) ",
		    i, ind1, ind2, l_col, match_id[0],
		    match_id[1]));
	} else {	// if not assigned already assign color (keep the first color assigned usually the best)
	  if (l_matchf1[ind1] == -1)
	    l_matchf1[ind1] = l_col;
	  if (l_matchf2[ind2] == -1)
	    l_matchf2[ind2] = l_col;
	}
	DBG_VISU_(fprintf
		  (stderr, "Match color[%d] vertices(%d %d): %d\n", i,
		   ind1, ind2, l_col));
      }

      C_Mesh1->loadFunctionValue(l_matchf1, 1);
      C_Mesh1->fillColorWithFunctionValue(0);
      C_Mesh2->loadFunctionValue(l_matchf2, 1);
      C_Mesh2->fillColorWithFunctionValue(0);

      C_Mesh1->buildMeshsetList();
      C_Mesh2->buildMeshsetList();

      //BUILD MATCH LIST
      buildBinaryMeshMatchList(vizu_sampling);

    }
  }
  //embedding
  if ((C_Embed1->isLoaded()) && (C_Embed2->isLoaded()) && (match)) {
    buildTransfoCoordsLists();
    buildBinaryEmbedMatchList(vizu_sampling);
  }

  

  if (l_singlematch)
    delete[]l_singlematch;
  if (l_matchf1)
    delete[]l_matchf1;
  if (l_matchf2)
    delete[]l_matchf2;

  DBG3_(fprintf(stderr, "VisualShape::initColorsAndShapes() return\n"));

}




int VisualShape::buildBinaryShapeMatch(FL * p_v1, FL * p_v2,
				       float *p_color1, float *p_color2,
				       int p_dim1, int p_dim2, double p_s1,
				       double p_s2, int p_col_depth,
				       int p_sample_rate, bool p_Bbox)
{

  HMatrix M ;
  float v[3] ;
  v[0] = model_rot[0] ;
  v[1] = model_rot[1] ;
  v[2] = model_rot[2] ;
  ConvertXYZ2Mat ( v, M)  ;
  float p[3] ;
  double **mat ;
  double *vmat = new double [9] ;
  mat = allocateDMat(vmat, 3, 3) ;
  for (int i = 0 ; i < 3 ; ++i)
    for (int j = 0 ; j < 3 ; ++j)
      vmat[i * 3 + j] = M[i][j] ;
    
    
  int ind1, ind2;
  GLfloat l_x1, l_y1, l_z1, l_x2, l_y2, l_z2;

  outliers = 0;
  float l_r = 0.5f;
  float l_g = 0.5f;
  float l_b = 0.5f;

  glEnable(GL_LINE_STIPPLE);
  glLineStipple(1, 0x0F0F);

  glLineWidth(0.1f);
  glColor3f(l_r, l_g, l_b);
  glBegin(GL_LINES);

  bool l_draw = true;
  for (int i = set_first; i < min(set_first + set_size, nmatch);
       i += p_sample_rate) {



    ind1 = match[2 * i + match_id[0]];
    ind2 = match[2 * i + match_id[1]];
	
    l_draw = true;
	
    if ((ind1 == -1) || (ind2 == -1)) {
      outliers++;
    } else {
      int i1 = ind1 * p_dim1;
      int i2 = ind2 * p_dim2;

      l_x1 = p_v1[i1];	//(float)(vox2[ind2*vox_dim2]/vox_scale2) - model_sep[0];
      l_y1 = p_v1[i1 + 1];	//(float)(vox2[ind2*vox_dim2+1]/vox_scale2) - model_sep[1];
      l_z1 = p_v1[i1 + 2];	//(float)(vox2[ind2*vox_dim2+2]/vox_scale2) - model_sep[2];


      if (p_Bbox) {
	l_draw = false;
	if (l_z1 > z_match_min && l_z1 < z_match_max) {
	  if (l_y1 > y_match_min && l_y1 < y_match_max) {
	    if (l_x1 > x_match_min && l_x1 < x_match_max) {
	      l_draw = true;

	    }
	  }
	}
      }
      if (l_draw) {

	l_x2 = p_v2[i2];	//(float)(vox2[ind2*vox_dim2]/vox_scale2) - model_sep[0];
	l_y2 = p_v2[i2 + 1];	//(float)(vox2[ind2*vox_dim2+1]/vox_scale2) - model_sep[1];
	l_z2 = p_v2[i2 + 2];	//(float)(vox2[ind2*vox_dim2+2]/vox_scale2) - model_sep[2];

	l_x2 = (float) (l_x2 / p_s2) ; // - model_sep[0];
	l_y2 = (float) (l_y2 / p_s2) ; // - model_sep[1];
	l_z2 = (float) (l_z2 / p_s2) ; // - model_sep[2];

	l_x1 = (float) (l_x1 / p_s1) + model_sep[0];
	l_y1 = (float) (l_y1 / p_s1) + model_sep[1];
	l_z1 = (float) (l_z1 / p_s1) + model_sep[2];

	if (p_color1) {
	  int c = ind1 * p_col_depth;
	  glColor3f(p_color1[c], p_color1[c + 1],
		    p_color1[c + 2]);
	}
	if (p_color2) {
	  int c = ind2 * p_col_depth;
	  glColor3f(p_color2[c], p_color2[c + 1],
		    p_color2[c + 2]);
	}
	glVertex3f(l_x1, l_y1, l_z1);



	p[0] = l_x2 ;
	p[1] = l_y2 ;
	p[2] = l_z2 ;
	float *v1 = RotatePoint(mat, p ) ;
	v1[0] -= model_sep[0] ;
	v1[1] -= model_sep[1] ;
	v1[2] -= model_sep[2] ;

	glVertex3f(v1[0], v1[1], v1[2]);
	free(v1) ;
	v1 = NULL ;
      }
    }
  }
  glEnd();
  glDisable(GL_LINE_STIPPLE);
  DBG_(fprintf
       (stderr,
	"Found %d outliers + unmatched data points in the current match\n",
	outliers));

  delete [] vmat ;
  delete [] mat ;

  return 0;
new float[3] ; //
}

int VisualShape::buildBinaryShapeMatchList(FL * p_v1, FL * p_v2,
					   float *p_color1,
					   float *p_color2, int p_dim1,
					   int p_dim2, double p_s1,
					   double p_s2, int p_col_depth,
					   int p_sample_rate, bool p_Bbox)
{


  if (BIN_SHAPE_MATCHLIST != (uint) (-1)) {
    glDeleteLists(BIN_SHAPE_MATCHLIST, 1);
    BIN_SHAPE_MATCHLIST = (uint) (-1);
  }
  BIN_SHAPE_MATCHLIST = glGenLists(1);
  glNewList(BIN_SHAPE_MATCHLIST, GL_COMPILE);


  buildBinaryShapeMatch(p_v1, p_v2, p_color1, p_color2, p_dim1, p_dim2,
			p_s1, p_s2, p_col_depth, p_sample_rate, p_Bbox);

  glEndList();

  PRINT_(fprintf
	 (stderr, "Building Match List: Outliers+Unmatched: %d\n",
	  outliers));
  return 0;
}





int VisualShape::buildBinaryVoxelMatchList(int p_sample_rate)
{

  if ((!C_Voxel1->isLoaded()) || (!C_Voxel2->isLoaded())) {
    ERROR_(fprintf
	   (stderr,
	    "Can not build binary match list. Voxelsets have not yet been loaded\n"),
	   ERROR_EMPTY_DATA);
  }

  if (!match) {
    ERROR_(fprintf(stderr, "No binary match has been loaded\n"),
	   ERROR_EMPTY_DATA);
  }

  int l_vox_dim1 = C_Voxel1->getDim();
  int l_vox_dim2 = C_Voxel2->getDim();
  double l_vox_s1 = C_Voxel1->getScale();
  double l_vox_s2 = C_Voxel2->getScale();
  if (l_vox_s1 > l_vox_s2) {
    C_Voxel2->setScale(l_vox_s1);
    l_vox_s2 = l_vox_s1;
  } else if (l_vox_s2 > l_vox_s1) {
    C_Voxel1->setScale(l_vox_s2);
    l_vox_s1 = l_vox_s2;
  }
  FL *l_vox1 = C_Voxel1->getVoxels();
  FL *l_vox2 = C_Voxel2->getVoxels();

  int l_col_depth = 0;
  float *l_color1 = NULL;
  float *l_color2 = NULL;
  if (color_match1) {
    l_color1 = C_Voxel1->getColorVector();
    l_col_depth = C_Voxel1->getColorDepth();
  }
  if (color_match2) {
    l_color2 = C_Voxel2->getColorVector();
    l_col_depth = C_Voxel2->getColorDepth();
  }
  buildBinaryShapeMatchList(l_vox1, l_vox2, l_color1, l_color2,
			    l_vox_dim1, l_vox_dim2, l_vox_s1, l_vox_s2,
			    l_col_depth, p_sample_rate, false);


  return 0;
}



int VisualShape::buildBinaryMeshMatchList(int p_sample_rate)
{

  if ((!C_Mesh1->isLoaded()) || (!C_Mesh2->isLoaded())) {
    ERROR_(fprintf
	   (stderr,
	    "Can not build binary match list. Meshsets have not yet been loaded\n"),
	   ERROR_EMPTY_DATA);
  }

  if (!match) {
    ERROR_(fprintf(stderr, "No binary match has been loaded\n"),
	   ERROR_EMPTY_DATA);
  }

  int l_me_dim1 = C_Mesh1->getDim();
  int l_me_dim2 = C_Mesh2->getDim();
  double l_me_s1 = C_Mesh1->getScale();
  double l_me_s2 = C_Mesh2->getScale();

  DBG_(std::
       cout << "THE SCALES ARE : " << l_me_s1 << " " << l_me_s2 << std::
       endl);

  FL *l_me1 = C_Mesh1->getVertices();
  FL *l_me2 = C_Mesh2->getVertices();

  int l_col_depth = 0;

  float *l_color1 = NULL;
  float *l_color2 = NULL;
  int l_n1 = C_Mesh1->getNumOfVertex();
  int l_n2 = C_Mesh2->getNumOfVertex();

  if (color_match1 && !color_match2) {
    C_Mesh1->fillColorWithColormap();
    int l_copy_from = 0;
    l_color1 = C_Mesh1->getColorVector();
    l_col_depth = C_Mesh1->getColorDepth();
    l_color2 = new float[l_n2 * l_col_depth];
    l_color2 =
      copyColorFromMatch(l_n1, l_n2, l_color1, l_color2, l_col_depth,
			 l_copy_from);
    C_Mesh2->setColorVector(l_color2);
    delete[]l_color2;
    l_color2 = NULL;

  } else if (!color_match1 && color_match2) {
    C_Mesh2->fillColorWithColormap();
    int l_copy_from = 1;
    l_color2 = C_Mesh2->getColorVector();
    l_col_depth = C_Mesh2->getColorDepth();
    l_color1 = new float[l_n1 * l_col_depth];
    l_color1 =
      copyColorFromMatch(l_n1, l_n2, l_color1, l_color2, l_col_depth,
			 l_copy_from);
    C_Mesh1->setColorVector(l_color1);
    delete[]l_color1;
    l_color1 = NULL;

  } else if (!color_match1 && !color_match2) {

    C_Mesh1->setAllPointsColor(0.5, 0.5, 0.5) ;
    C_Mesh2->setAllPointsColor(0.5, 0.5, 0.5) ;

  } else {
    C_Mesh1->fillColorWithColormap();
    C_Mesh2->fillColorWithColormap();
  }
  C_Mesh1->buildMeshsetList();
  C_Mesh2->buildMeshsetList();

  l_color1 = NULL;
  l_color2 = NULL;


  if (color_match1) {
    l_color1 = C_Mesh1->getColorVector();
    l_col_depth = C_Mesh1->getColorDepth();
  }
  if (color_match2) {
    l_color2 = C_Mesh2->getColorVector();
    l_col_depth = C_Mesh2->getColorDepth();
  }



  buildBinaryShapeMatchList(l_me1, l_me2, l_color1, l_color2,
			    l_me_dim1, l_me_dim2, l_me_s1, l_me_s2,
			    l_col_depth, p_sample_rate, false);

  return 0;
}







































int VisualShape::animateMatch()
{
  DBG3_(fprintf(stderr, "VisualShape::animate_match()\n"));

  int l_dim1 = 0;
  int l_dim2 = 0;
  double l_s1 = 1;
  double l_s2 = 1;
  FL *l_v1 = NULL;
  FL *l_v2 = NULL;
  int l_col_depth = 0;
  float *l_color1 = NULL;
  float *l_color2 = NULL;

  if (C_Voxel1->isLoaded() && C_Voxel2->isLoaded()) {
    l_dim1 = C_Voxel1->getDim();
    l_dim2 = C_Voxel2->getDim();
    l_s1 = C_Voxel1->getScale();
    l_s2 = C_Voxel2->getScale();

    l_v1 = C_Voxel1->getVoxels();
    l_v2 = C_Voxel2->getVoxels();
    if (color_match2) {
      l_color2 = C_Voxel2->getColorVector();
      l_col_depth = C_Voxel2->getColorDepth();
    }
    if (color_match1) {
      l_color1 = C_Voxel1->getColorVector();
      l_col_depth = C_Voxel1->getColorDepth();
    }

  }
  if (C_Mesh1->isLoaded() && C_Mesh2->isLoaded()) {
    l_dim1 = C_Mesh1->getDim();
    l_dim2 = C_Mesh2->getDim();
    l_s1 = C_Mesh1->getScale();
    l_s2 = C_Mesh2->getScale();

    l_v1 = C_Mesh1->getVertices();
    l_v2 = C_Mesh2->getVertices();
    if (color_match2) {
      l_color2 = C_Mesh2->getColorVector();
      l_col_depth = C_Mesh2->getColorDepth();
    }
    if (color_match1) {
      l_color1 = C_Mesh1->getColorVector();
      l_col_depth = C_Mesh1->getColorDepth();
    }
  }

  if (l_v1 == NULL || l_v2 == NULL) {
    return 1;
  }
  if (!match) {
    ERROR_(fprintf(stderr, "No binary match has been loaded\n"),
	   ERROR_EMPTY_DATA);
  }

  buildBinaryShapeMatch(l_v1, l_v2, l_color1, l_color2, l_dim1, l_dim2,
			l_s1, l_s2, l_col_depth, vizu_sampling, false);

  return 0;
}

int VisualShape::buildProbaShapeMatchList(FL * p_v1, FL * p_v2,
					  float *p_color1, float *p_color2,
					  int p_dim1, int p_dim2,
					  int p_col_depth,
					  int p_sample_rate,
					  int p_max_matches_per_el)
{


  if (!proba_matrix) {
    ERROR_(fprintf(stderr, "No probability matrix has been loaded\n"),
	   ERROR_EMPTY_DATA);
  }
  if ((pm_dim[0] == p_dim1) || (pm_dim[1] == p_dim2)) {
    match_id[0] = 0;
    match_id[1] = 1;
  } else if ((pm_dim[0] == p_dim2) || (pm_dim[1] == p_dim1)) {
    match_id[0] = 1;
    match_id[1] = 0;
  } else {
    ERROR_(fprintf
	   (stderr,
	    "Size of proba matrix does not agree with loaded voxelsets\n"),
	   ERROR_VARIABLE_INIT);
  }

  //Look for the first max_matches_per_el matches
  int **l_best_matches = new int *[pm_dim[0]];
  for (int i = 0; i < pm_dim[0]; i = i + p_sample_rate) {
    l_best_matches[i] = new int[p_max_matches_per_el];
  }

  double l_last_max, l_current_max;
  int ind = 0;
  for (int i = 0; i < pm_dim[0]; i += p_sample_rate) {
    l_last_max = 1.1;
    for (int el = 0; el < p_max_matches_per_el; el++) {
      ind = -1;
      l_current_max = 0.0;
      for (int j = 0; j < pm_dim[1]; j++) {
	if ((proba_matrix[i][j] > l_current_max)
	    && (proba_matrix[i][j] < l_last_max))
	  ind = j;
      }
      l_best_matches[i][el] = ind;
      l_last_max = proba_matrix[i][ind];
    }
  }

  if (PROBA_SHAPE_MATCHLIST != (uint) (-1)) {
    glDeleteLists(PROBA_SHAPE_MATCHLIST, 1);
    PROBA_SHAPE_MATCHLIST = (uint) (-1);
  }
  PROBA_SHAPE_MATCHLIST = glGenLists(1);
  glNewList(PROBA_SHAPE_MATCHLIST, GL_COMPILE);

  DBG_(fprintf(stderr, "match: list %d\n", (int) PROBA_SHAPE_MATCHLIST));

  int ind1, ind2, l_dim;
  GLfloat l_x, l_y, l_z;
  outliers = 0;
  float l_r = 0.5f;
  float l_g = 0.5f;
  float l_b = 0.5f;

  glColor3f(l_r, l_g, l_b);

  l_dim = p_dim1;
  /*
  glEnable(GL_LINE_STIPPLE);
  glLineStipple(1, 0x0F0F);
  */

  glLineWidth(0.2f);
  glBegin(GL_LINES);
  for (int i = 0; i < pm_dim[0]; i = i + p_sample_rate) {
    for (int el = 0; el < p_max_matches_per_el; el++) {
      ind1 = i;
      ind2 = l_best_matches[i][el];
      if (ind2 != -1) {
	int i1 = ind1 * p_dim1;
	int i2 = ind2 * p_dim2;

	if (p_color1) {
	  int c = ind1 * p_col_depth;
	  glColor3f(p_color1[c], p_color1[c + 1],
		    p_color1[c + 2]);
	}
	if (p_color2) {
	  int c = ind2 * p_col_depth;
	  glColor3f(p_color2[c], p_color2[c + 1],
		    p_color2[c + 2]);
	}

	l_x = (float) p_v1[i1] + model_sep[0];
	l_y = (float) p_v1[i1 + 1] + model_sep[1];
	l_z = (float) p_v1[i1 + 2] + model_sep[2];
	glVertex3f(l_x, l_y, l_z);
	l_x = (float) p_v2[i2] - model_sep[0];
	l_y = (float) p_v2[i2 + 1] - model_sep[1];
	l_z = (float) p_v2[i2 + 2] - model_sep[2];
	glVertex3f(l_x, l_y, l_z);


      }
    }
  }
  glEnd();
  glDisable(GL_LINE_STIPPLE);
  glEndList();

  for (int i = 0; i < pm_dim[0]; i = i + p_sample_rate) {
    delete[]l_best_matches[i];
  }
  delete[]l_best_matches;

  return 0;
}



int VisualShape::buildProbaVoxelMatchList(int p_sample_rate,
					  int p_max_matches_per_el)
{

  if ((!C_Voxel1->isLoaded()) || (!C_Voxel2->isLoaded())) {
    ERROR_(fprintf
	   (stderr,
	    "Can not buld proba match list. Voxelsets have not yet been loaded\n"),
	   ERROR_EMPTY_DATA);
  }


  int l_dim1 = C_Voxel1->getNumOfVoxels();
  int l_dim2 = C_Voxel2->getNumOfVoxels();

  FL *l_v1 = C_Voxel1->getVoxels();
  FL *l_v2 = C_Voxel2->getVoxels();

  int l_col_depth = 0;
  float *l_color1 = NULL;
  float *l_color2 = NULL;
  if (color_match1) {
    l_color1 = C_Voxel1->getColorVector();
    l_col_depth = C_Voxel1->getColorDepth();
  }
  if (color_match2) {
    l_color2 = C_Voxel2->getColorVector();
    l_col_depth = C_Voxel2->getColorDepth();
  }

  buildProbaShapeMatchList(l_v1, l_v2, l_color1, l_color2,
			   l_dim1, l_dim2, l_col_depth,
			   p_sample_rate, p_max_matches_per_el);

  return 0;
}


int VisualShape::buildProbaMeshMatchList(int p_sample_rate,
					 int p_max_matches_per_el)
{

  if ((!C_Mesh1->isLoaded()) || (!C_Mesh2->isLoaded())) {
    ERROR_(fprintf
	   (stderr,
	    "Can not buld proba match list. Meshsets have not yet been loaded\n"),
	   ERROR_EMPTY_DATA);
  }

  int l_dim1 = C_Mesh1->getNumOfVertex();
  int l_dim2 = C_Mesh2->getNumOfVertex();

  FL *l_v1 = C_Mesh1->getVertices();
  FL *l_v2 = C_Mesh2->getVertices();
  int l_col_depth = 0;

  float *l_color1 = NULL;
  float *l_color2 = NULL;
  if (color_match1) {
    l_color1 = C_Mesh1->getColorVector();
    l_col_depth = C_Mesh1->getColorDepth();
  }
  if (color_match2) {
    l_color2 = C_Mesh2->getColorVector();
    l_col_depth = C_Mesh2->getColorDepth();
  }

  buildProbaShapeMatchList(l_v1, l_v2, l_color1, l_color2,
			   l_dim1, l_dim2, l_col_depth,
			   p_sample_rate, p_max_matches_per_el);


  return 0;
}


int VisualShape::buildBinaryEmbedMatchList(int p_sample_rate)
{


  if (coords_transfo_2 == NULL) {
    return 0;
  }
  if (coords_transfo_1 == NULL) {
    return 0;
  }

  if (!C_Embed1->isLoaded() || !C_Embed2->isLoaded()) {
    ERROR_(fprintf
	   (stderr,
	    "Can not build embed match list. Embedding coords have not yet been loaded\n"),
	   ERROR_EMPTY_DATA);
  }

  if (!match) {
    ERROR_(fprintf(stderr, "No binary match has been loaded\n"),
	   ERROR_EMPTY_DATA);
  }

  if (BIN_EMBED_MATCHLIST) {
    for (int l = 0; l < embed3d_nlist; l++) {
      if (BIN_EMBED_MATCHLIST[l] != (uint) (-1)) {
	glDeleteLists(BIN_EMBED_MATCHLIST[l], 1);
	BIN_EMBED_MATCHLIST[l] = (uint) (-1);
      }
    }
    delete[]BIN_EMBED_MATCHLIST;
    BIN_EMBED_MATCHLIST = NULL;
  }

  embed3d_nlist = mat_transfo_dim / 3;
  BIN_EMBED_MATCHLIST = new GLuint[embed3d_nlist];
  for (int l = 0; l < embed3d_nlist; l++)
    BIN_EMBED_MATCHLIST[l] = (uint) (-1);

  int l_emb_dim1 = C_Embed1->getDim();
  double l_embed_scale1 = C_Embed1->getScale();
  double l_embed_scale2 = C_Embed2->getScale();

  int l_emb_dim2 = 0;
  if (init_transfo) {
    l_emb_dim2 = mat_transfo_dim;
    l_emb_dim1 = mat_transfo_dim;
  } else
    l_emb_dim2 = C_Embed2->getDim();

  int l_col_depth = 0;
  float *l_color1 = NULL;
  float *l_color2 = NULL;

  if (C_Voxel1 && C_Voxel2) {
    if (color_match1) {
      l_color1 = C_Voxel1->getColorVector();
      l_col_depth = C_Voxel1->getColorDepth();
    }
    if (color_match2) {
      l_color2 = C_Voxel2->getColorVector();
      l_col_depth = C_Voxel2->getColorDepth();
    }
  }
  int l_dx = seld[0];
  int l_dy = seld[1];
  int l_dz = seld[2];

  for (int l = 0; l < embed3d_nlist; l++) {
    BIN_EMBED_MATCHLIST[l] = glGenLists(1);
    glNewList(BIN_EMBED_MATCHLIST[l], GL_COMPILE);
    PRINT_(fprintf
	   (stderr, "Embed match: list %d : dims %d-%d\n",
	    (int) BIN_EMBED_MATCHLIST[l], l + 1, l + 3));

    int ind1, ind2;
    GLfloat l_x, l_y, l_z;
    outliers = 0;
    float l_r = 0.2f;
    float l_g = 0.2f;
    float l_b = 0.2f;
    glColor3f(l_r, l_g, l_b);

    glLineWidth(1.5f);
    /*
    glEnable(GL_LINE_STIPPLE);
    glLineStipple(1, 0x0F0F);
    */

    for (int i = 0; i < nmatch; i = i + p_sample_rate) {
      ind1 = match[2 * i + match_id[0]];
      ind2 = match[2 * i + match_id[1]];
      //fprintf(stderr,"(%d %d) ", ind1, ind2);
      if ((ind1 == -1) || (ind2 == -1)) {
	outliers++;
	glColor3f(0.9, 0.0, 0.0);
	
	if (ind1 != -1) {
	  l_x =
	    (float) (coords_transfo_1[ind1 * l_emb_dim1 + l_dx] /
		     l_embed_scale1) + model_sep[0];
	  l_y =
	    (float) (coords_transfo_1[ind1 * l_emb_dim1 + l_dy] /
		     l_embed_scale1) + model_sep[1];
	  l_z =
	    (float) (coords_transfo_1[ind1 * l_emb_dim1 + l_dz] /
		     l_embed_scale1) + model_sep[2];
	  drawCube(l_x, l_y, l_z, 0.005) ;
	}
	else if (ind2 != -1) {
	  l_x =
	    (float) (coords_transfo_2[ind2 * l_emb_dim2 + l_dx] /
		     l_embed_scale2) - model_sep[0];
	  l_y =
	    (float) (coords_transfo_2[ind2 * l_emb_dim2 + l_dy] /
		     l_embed_scale2) - model_sep[1];
	  l_z =
	    (float) (coords_transfo_2[ind2 * l_emb_dim2 + l_dz] /
		     l_embed_scale2) - model_sep[2];
	  drawCube(l_x, l_y, l_z, 0.005) ;
	}
      } else {
	glBegin(GL_LINES);
	glColor3f(0.7, 0.7, 0.7);

	l_x =
	  (float) (coords_transfo_1[ind1 * l_emb_dim1 + l_dx] /
		   l_embed_scale1) + model_sep[0];
	l_y =
	  (float) (coords_transfo_1[ind1 * l_emb_dim1 + l_dy] /
		   l_embed_scale1) + model_sep[1];
	l_z =
	  (float) (coords_transfo_1[ind1 * l_emb_dim1 + l_dz] /
		   l_embed_scale1) + model_sep[2];
	glVertex3f(l_x, l_y, l_z);



	l_x =
	  (float) (coords_transfo_2[ind2 * l_emb_dim2 + l_dx] /
		   l_embed_scale2) - model_sep[0];
	l_y =
	  (float) (coords_transfo_2[ind2 * l_emb_dim2 + l_dy] /
		   l_embed_scale2) - model_sep[1];
	l_z =
	  (float) (coords_transfo_2[ind2 * l_emb_dim2 + l_dz] /
		   l_embed_scale2) - model_sep[2];
	glVertex3f(l_x, l_y, l_z);
	glEnd();
	
      }
    }
    glDisable(GL_LINE_STIPPLE);
    glEndList();
  }
  return 0;
}

int VisualShape::buildProbaEmbedMatchList(int p_sample_rate,
					  int p_max_matches_per_el)
{

  if ((!C_Embed1->isLoaded()) || (!C_Embed2->isLoaded())) {
    ERROR_(fprintf
	   (stderr,
	    "Can not build embed proba match list. Embedding coords have not yet been loaded\n"),
	   ERROR_EMPTY_DATA);
  }

  if (!proba_matrix) {
    ERROR_(fprintf(stderr, "No probability matrix has been loaded\n"),
	   ERROR_EMPTY_DATA);
  }
  if ((pm_dim[0] == C_Embed1->getNumOfPoints())
      || (pm_dim[1] == C_Embed2->getNumOfPoints())) {
    match_id[0] = 0;
    match_id[1] = 1;
  } else if ((pm_dim[0] == C_Embed2->getNumOfPoints())
	     || (pm_dim[1] == C_Embed1->getNumOfPoints())) {
    match_id[0] = 1;
    match_id[1] = 0;
  } else {
    ERROR_(fprintf
	   (stderr,
	    "Size of proba matrix does not agree with loaded voxelsets\n"),
	   ERROR_VARIABLE_INIT);
  }


  //Look for the first max_matches_per_el matches
  int **l_best_matches = new int *[pm_dim[0]];
  for (int i = 0; i < pm_dim[0]; i = i + p_sample_rate) {
    l_best_matches[i] = new int[p_max_matches_per_el];
  }

  double l_last_max, l_current_max;
  int ind = 0;
  for (int i = 0; i < pm_dim[0]; i = i + p_sample_rate) {
    l_last_max = 1.1;
    for (int el = 0; el < p_max_matches_per_el; el++) {
      ind = -1;
      l_current_max = 0.0;
      for (int j = 0; j < pm_dim[1]; j++) {
	if ((proba_matrix[i][j] > l_current_max)
	    && (proba_matrix[i][j] < l_last_max))
	  ind = j;
      }
      l_best_matches[i][el] = ind;
      l_last_max = proba_matrix[i][ind];
    }
  }
  if (PROBA_EMBED_MATCHLIST) {
    for (int l = 0; l < embed3d_nlist; l++) {
      if (PROBA_EMBED_MATCHLIST[l] != (uint) (-1)) {
	glDeleteLists(PROBA_EMBED_MATCHLIST[l], 1);
	PROBA_EMBED_MATCHLIST[l] = (uint) (-1);
      }
    }
    delete[]PROBA_EMBED_MATCHLIST;
    PROBA_EMBED_MATCHLIST = NULL;
  }

  embed3d_nlist = C_Embed1->getNumOfList();
  PROBA_EMBED_MATCHLIST = new GLuint[embed3d_nlist];
  for (int l = 0; l < embed3d_nlist; l++)
    PROBA_EMBED_MATCHLIST[l] = (uint) (-1);


  int l_emb_dim = C_Embed1->getDim();
  for (int l = 0; l < C_Embed1->getNumOfList(); l++) {
    PROBA_EMBED_MATCHLIST[l] = glGenLists(1);
    glNewList(PROBA_EMBED_MATCHLIST[l], GL_COMPILE);
    DBG_(fprintf
	 (stderr, "Embed match: list %d : dims %d-%d\n",
	  (int) PROBA_EMBED_MATCHLIST[l], l + 1, l + 3));

    int ind1, ind2;
    GLfloat l_x, l_y, l_z;
    outliers = 0;
    float l_r = 0.5f;
    float l_g = 0.5f;
    float l_b = 0.5f;

    glLineWidth(1.5f);
    /*
    glEnable(GL_LINE_STIPPLE);
    glLineStipple(1, 0x0F0F);
    */
    glBegin(GL_LINES);
    for (int i = 0; i < pm_dim[0]; i = i + p_sample_rate) {
      for (int el = 0; el < p_max_matches_per_el; el++) {
	ind1 = i;
	ind2 = l_best_matches[i][el];
	if (ind2 != -1) {
	  C_Voxel1->getVoxelColor(ind1, &l_r, &l_g, &l_b);
	  glColor3f(l_r, l_g, l_b);
	  l_x =
	    (float) C_Embed1->coords[ind1 * l_emb_dim + l] +
	    model_sep[0];
	  l_y =
	    (float) C_Embed1->coords[ind1 * l_emb_dim + l +
				     1] + model_sep[1];
	  l_z =
	    (float) C_Embed1->coords[ind1 * l_emb_dim + l +
				     2] + model_sep[2];
	  glVertex3f(l_x, l_y, l_z);
	  l_x =
	    (float) C_Embed2->coords[ind2 * l_emb_dim + l] -
	    model_sep[0];
	  l_y =
	    (float) C_Embed2->coords[ind2 * l_emb_dim + l +
				     1] - model_sep[1];
	  l_z =
	    (float) C_Embed2->coords[ind2 * l_emb_dim + l +
				     2] - model_sep[2];
	  glVertex3f(l_x, l_y, l_z);


	}
      }
    }
    glEnd();
    glDisable(GL_LINE_STIPPLE);
    glEndList();
  }

  for (int i = 0; i < pm_dim[0]; i = i + p_sample_rate) {
    delete[]l_best_matches[i];
  }
  delete[]l_best_matches;


  return 0;
}

int VisualShape::setMinMaxValuesForDisplay(double p_xmin, double p_xmax,
					   double p_ymin, double p_ymax,
					   double p_zmin, double p_zmax)
{

  //Define a region to zoom
  x_match_min = p_xmin;
  x_match_max = p_xmax;
  y_match_min = p_ymin;
  y_match_max = p_ymax;
  z_match_min = p_zmin;
  z_match_max = p_zmax;

  return 1;
}

int VisualShape::buildTransfoCoordsLists()
{
  if (!C_Embed1->isLoaded() || !C_Embed2->isLoaded()) {
    ERROR_(fprintf
	   (stderr,
	    "Can not build transfo embed list. Embedding coords have not yet been loaded\n"),
	   ERROR_EMPTY_DATA);
  }

  int l_dim = 0;

  if (mat_transfo_dim == 0) {
    mat_transfo_dim = C_Embed2->getDim();
  }
  embed3d_nlist = C_Embed1->getNumOfList() ;
  l_dim = mat_transfo_dim;

  if (init_transfo)
    l_dim = mat_transfo_dim;


  apply_init_transfo_once = false;

  transformEmbedCoord(false);

  apply_init_transfo_once = true;

  int l_dx = seld[0];
  int l_dy = seld[1];
  int l_dz = seld[2];

  if (!coords_transfo_2) {
    ERROR_(fprintf(stderr, "Can not build coord list\n"),
	   ERROR_EMPTY_DATA);
  }
  //Prepare the number of trios of 3 dimensions to show
  if (trio_list_2) {
    for (int i = 0; i < embed3d_nlist; i++)
      if (trio_list_2[i] != (uint) (-1)) {
	glDeleteLists(trio_list_2[i], 1);
	trio_list_2[i] = (uint) (-1);
      }
    delete [] trio_list_2;
    trio_list_2 = NULL;
  }

  trio_list_2 = new GLuint[embed3d_nlist];


  int l_npoints = C_Embed2->getNumOfPoints();

  FL l_scale = C_Embed2->getScale();

  float l_x, l_y, l_z;

  for (int l = 0; l < embed3d_nlist; l++) {
    trio_list_2[l] = glGenLists(1);

    glNewList(trio_list_2[l], GL_COMPILE);

    glPointSize(2);

    glColor3f(0, 0.8, 0);
    for (int i = 0; i < l_npoints; i+=vizu_sampling) {

      l_x = (float) (coords_transfo_2[i * l_dim + l_dx] / l_scale);
      l_y = (float) (coords_transfo_2[i * l_dim + l_dy] / l_scale);
      l_z = (float) (coords_transfo_2[i * l_dim + l_dz] / l_scale);

      drawCube (l_x, l_y, l_z, 0.01 ) ;
    }
    int ind1 = 0 ;
    int ind2 = 0 ;
    for (int i = 0; i < nmatch; i = i + vizu_sampling) {
      ind1 = match[2 * i + match_id[0]];
      ind2 = match[2 * i + match_id[1]];
      if ( ind2 != -1 ) {
	l_x = (float) (coords_transfo_2[ind2 * l_dim + l_dx] / l_scale);
	l_y = (float) (coords_transfo_2[ind2 * l_dim + l_dy] / l_scale);
	l_z = (float) (coords_transfo_2[ind2 * l_dim + l_dz] / l_scale);
	
	drawCube (l_x, l_y, l_z, 0.01 ) ;
      }
    }
    glEndList();
  }


  if (!coords_transfo_1) {
    ERROR_(fprintf(stderr, "Can not build coord list\n"),
	   ERROR_EMPTY_DATA);
  }
  //Prepare the number of trios of 3 dimensions to show
  if (trio_list_1) {
    for (int i = 0; i < embed3d_nlist; i++)
      if (trio_list_1[i] != (uint) (-1)) {
	glDeleteLists(trio_list_1[i], 1);
	trio_list_1[i] = (uint) (-1);
      }
    delete[]trio_list_1;
    trio_list_1 = NULL;
  }

  trio_list_1 = new GLuint[embed3d_nlist];

  l_dim = 0;

  if (init_transfo)
    l_dim = mat_transfo_dim;
  else
    l_dim = C_Embed1->getDim();

  l_npoints = C_Embed1->getNumOfPoints();
  l_scale = C_Embed1->getScale();

  for (int l = 0; l < embed3d_nlist; l++) {
    trio_list_1[l] = glGenLists(1);

    glNewList(trio_list_1[l], GL_COMPILE);

    glPointSize(2);

    glColor3f(0, 0, 0.8);
    //    glBegin(GL_POINTS);

    for (int i = 0; i < l_npoints; i+=vizu_sampling) {

      l_x = (float) (coords_transfo_1[i * l_dim + l_dx] / l_scale);
      l_y = (float) (coords_transfo_1[i * l_dim + l_dy] / l_scale);
      l_z = (float) (coords_transfo_1[i * l_dim + l_dz] / l_scale);
      drawSphere (l_x, l_y, l_z, 0.007 ) ;
      //glVertex3f(l_x, l_y, l_z);
    }
    //   glEnd();
    glEndList();

  }


  return 0;
}


int VisualShape::drawBinShapeMatchList()
{
  glCallList(BIN_SHAPE_MATCHLIST);
  return 0;
}


int VisualShape::drawProbaShapeMatchList()
{
  glCallList(PROBA_SHAPE_MATCHLIST);
  return 0;
}

int VisualShape::drawBinEmbedMatchList()
{
  if (BIN_EMBED_MATCHLIST) {
    glCallList(BIN_EMBED_MATCHLIST[embed3d_id]);
    return 0;
  } else
    return 1;
}

int VisualShape::drawProbaEmbedMatchList()
{
  if (PROBA_EMBED_MATCHLIST) {
    glCallList(PROBA_EMBED_MATCHLIST[embed3d_id]);
    return 0;
  } else
    return 1;
}






void VisualShape::refreshAllInputs()
{
  DBG3_(fprintf(stderr, "VisualShape::RefreshAllInputs()"));
  int l_res = 0;
  if (transfo_filename) {
    l_res = readTransfo();
    if (!l_res) {
      buildTransfoCoordsLists();
      PRINT_(std::cout << "Update the transformation" << std::endl);
    }
  }

  if (binmatch_filename) {
    l_res = readBinaryMatch();
    if (!l_res) {
      PRINT_(std::cout << "Update the matching" << std::endl);
      initColorsAndShapes();
    }
  }

}


int VisualShape::drawTransfoCoordsList(int p_which, int p_list)
{

  if (p_which == 1) {
    if (!trio_list_2) {
      ERROR_(fprintf
	     (stderr,
	      "Cant draw coords list. List has not been yet built\n"),
	     ERROR_EMPTY_DATA);
    }
    
    glCallList(trio_list_2[p_list]);
  } else if (p_which == 0) {
    if (!trio_list_1) {
      ERROR_(fprintf
	     (stderr,
	      "Cant draw coords list. List has not been yet built\n"),
	     ERROR_EMPTY_DATA);
    }
    glCallList(trio_list_1[p_list]);
  }
  return 0;
}




float *VisualShape::copyColorFromMatch(int p_n1, int p_n2, float *p_color1,
				       float *p_color2, int p_col_depth,
				       int p_copy_from)
{
  //copies the colormap color1 into color2

  PRINT_(std::cout << " COPY COLOR " << std::endl);
  int ind1, ind2;
  float l_r = 0.2f;
  float l_g = 0.2f;
  float l_b = 0.2f;

  if (p_copy_from == 0) {
    for (int i = 0; i < p_n2; i++) {
      p_color2[i * p_col_depth] = l_r;
      p_color2[i * p_col_depth + 1] = l_g;
      p_color2[i * p_col_depth + 2] = l_b;
    }
  } else if (p_copy_from == 1) {
    for (int i = 0; i < p_n1; i++) {
      p_color1[i * p_col_depth] = l_r;
      p_color1[i * p_col_depth + 1] = l_g;
      p_color1[i * p_col_depth + 2] = l_b;
    }
  }

  for (int i = set_first; i < min(set_first + set_size, nmatch); i++) {
    ind1 = match[2 * i + match_id[0]];
    ind2 = match[2 * i + match_id[1]];

    if ((ind1 != -1) && (ind2 != -1)) {
      int i1 = ind1 * p_col_depth;
      int i2 = ind2 * p_col_depth;

      if (p_copy_from == 0) {
	p_color2[i2] = p_color1[i1];
	p_color2[i2 + 1] = p_color1[i1 + 1];
	p_color2[i2 + 2] = p_color1[i1 + 2];
      }
      if (p_copy_from == 1) {
	p_color1[i1] = p_color2[i2];
	p_color1[i1 + 1] = p_color2[i2 + 1];
	p_color1[i1 + 2] = p_color2[i2 + 2];
      }
    }
  }

  if (p_copy_from == 0)
    return p_color2;
  else if (p_copy_from == 1)
    return p_color1;
  else
    return NULL;

}

void VisualShape::setModelSep (float x, float y, float z) {
  

  if (ABS(x) > 1)
    x = 0.5 ;
  if (ABS(y) > 1)
    y = 0.5 ;
  if (ABS(z) > 1)
    z = 0.5 ;

  model_sep[0] = x ;
  model_sep[1] = y ;
  model_sep[2] = z ;


}


void VisualShape::setModelRot (float x, float y, float z) {
  model_rot[0] = x ;
  model_rot[1] = y ;
  model_rot[2] = z ;
}


void VisualShape::setVizuSampleRate (int p_sampling_rate) {
    
  vizu_sampling = p_sampling_rate ;
}



void VisualShape::drawReferenceFrame (float r, float g, float b) {

  float scale = 6 ;

  GLUquadric *quad = gluNewQuadric();
  glPushMatrix();
  glColor3f(r,g,b);
  gluCylinder ( quad, scale * 0.002, scale * 0.002, scale * 0.05, scale * 10, scale * 10) ;
  glTranslated(0, 0, scale * 0.05) ;
  gluCylinder ( quad, scale * 0.005, 0.0000, scale * 0.007, scale * 10, scale * 10) ;
  glPopMatrix();
    

    
  glPushMatrix();
  glRotatef(90, 0,1,0) ;

  glColor3f(r,g,b);
  gluCylinder ( quad, scale * 0.002, scale * 0.002, scale * 0.05, scale * 10, scale * 10) ;
  glTranslated(0, 0, scale * 0.05) ;
  gluCylinder ( quad, scale * 0.005, 0.0000, scale * 0.007, scale * 10, scale * 10) ;
  glPopMatrix();
   


  glPushMatrix();
  glRotatef(90, 1,0,0) ;

  glColor3f(r,g,b);
  gluCylinder ( quad, scale * 0.002, scale * 0.002, scale * 0.05, scale * 10, scale * 10) ;
  glTranslated(0, 0, scale * 0.05) ;
  gluCylinder ( quad, scale * 0.005, 0.0000, scale * 0.007, scale * 10, scale * 10) ;
  glPopMatrix();
}

/**************************************************************************************************/
/**************************************************************************************************/
/**************************************************************************************************/
/**************************************************************************************************/

/* I implement here a method to manually and easily intialize the Laplacian RLS approach. */
/* It consists of a code to perform manually the correspondances between the meshes.      */

/**************************************************************************************************/
/**************************************************************************************************/
/**************************************************************************************************/
/**************************************************************************************************/


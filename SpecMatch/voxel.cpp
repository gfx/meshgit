/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
/*
 *  voxel.cpp
 *  SpecMatch
 *
 *  Created by Diana Mateus and David Knossow on 8/26/07.
 *  Copyright 2007 INRIA. All rights reserved.
 *
 */


#include <QGLViewer/qglviewer.h>
#include <stdio.h>

#include "voxel.h"


using namespace std;


Voxel::Voxel()
{

  init () ;
  HASH_MAP_PARAM_(double, "cube_size", Double, cube_size) ;
  nvoxels = 0;

  HASH_MAP_PARAM_(int, "str_length", Int, str_length) ;

  voxel = NULL;
  voxel_color = NULL;
  
  lbl_value = NULL;
    lbl_scale_factor = 1;

    memset(bounding_box, 0, 3 * sizeof(int));
}


Voxel::~Voxel()
{

    if (lbl_value)
	delete[]lbl_value;

    // See ShapeData.h
    releaseData();


}


int Voxel::readPoints(const char *p_file, int p_sample_rate)
{


    if (p_sample_rate != 1) {
	WARNING_(fprintf
		 (stderr,
		  "In readPoints, sample_rate=%d. IT MUST MATCH WITH THE CONNECTIVITY FILE IF LOADED \n",
		  p_sample_rate));
    }


    DBG_(fprintf(stderr, "-->readPoints()\n"));
    if (!p_file) {
	FATAL_(fprintf(stderr, "Filename is empty \n"), FATAL_OPEN_FILE);
    }

    in.open(p_file, fstream::in);
    if (!in.is_open()) {
	FATAL_(fprintf(stderr, "Could not open %s \n", p_file),
	       FATAL_OPEN_FILE);
    }

    char l_next_string[256];
    if (filename) {
	delete[]filename;
	filename = NULL;
    }
    
    filename = new char[str_length];
    sprintf(filename, "%s", p_file);

    while (in.peek() == ' ' || in.peek() == '\n') {
	in.get();
    }
    while (in.peek() == '#')
	in.getline(l_next_string, 256);	// skip comment

    // If voxels are allready lodaed, release memory.
    releaseData();


    voxel = NULL;
    in >> npoints;

    char l_line[str_length];
    // Ending the line
    in.getline(l_line, str_length);


    DBG_(fprintf(stderr, " Allocating memory for %d voxels\n", npoints));

    //Allocate memory for voxels        
    npoints = npoints / p_sample_rate;

    points = new FL[npoints * dim];

    //Point voxel data to generic data points
    nvoxels = npoints;
    voxel = points;

    // Allows to compute the center of mass (mass=1 for each voxel) of the shape. It allows to center the data.
    FL *l_mean = NULL;
    l_mean = new FL[dim];

    // min coordinates of voxels. It allows to compute the the scale of the shape. 
    FL *l_minval = NULL;
    l_minval = new FL[dim];

    // max coordinates of voxels. It allows to compute the the scale of the shape.
    FL *l_maxval = NULL;
    l_maxval = new FL[dim];

    // Stores a line of the file. According to format there should be 3 or 4 elements per line
    float *l_val = NULL;
    l_val = new float[4];

    for (int i = 0; i < dim; i++) {
	l_mean[i] = 0.0;
	l_minval[i] = 0.0;
	l_maxval[i] = 0.0;
    }

    DBG_(fprintf(stderr, " Reading voxels from : %s ... ", p_file));

    int l_iret = 0;

    for (int i = 0, sample = p_sample_rate; i < nvoxels; sample++) {

	l_iret = 0;

	in.getline(l_line, str_length);
	l_iret =
	    sscanf(l_line, "%g %g %g %g", &l_val[0], &l_val[1], &l_val[2],
		   &l_val[3]);

	int idim = i * dim;

	if (sample == p_sample_rate) {
	    if (l_iret == 3) {
		voxel[idim] = (FL) l_val[0];
		voxel[idim + 1] = (FL) l_val[1];
		voxel[idim + 2] = (FL) l_val[2];
	    } else {
		voxel[idim] = (FL) l_val[1];
		voxel[idim + 1] = (FL) l_val[2];
		voxel[idim + 2] = (FL) l_val[3];
	    }

	    for (int d = 0; d < dim; d++) {
		l_mean[d] += voxel[idim + d];
		if ((voxel[idim + d] < l_minval[d]) || (i == 0))
		    l_minval[d] = voxel[idim + d];
		if ((voxel[idim + d] > l_maxval[d]) || (i == 0))
		    l_maxval[d] = voxel[idim + d];
	    }

	    i++;
	    sample = 0;
	}
    }

    in.close();


    DBG_(fprintf(stderr, "Finding mean and scale..."));
    for (int d = 0; d < dim; d++)
	l_mean[d] = l_mean[d] / ((FL) nvoxels);

    //center data
    for (int i = 0; i < nvoxels; i++) {
	for (int d = 0; d < dim; d++) {
	    voxel[i * dim + d] = voxel[i * dim + d] - l_mean[d];
	}
    }

    //find scale
    FL l_deltamax = fabs(l_maxval[0] - l_minval[0]);
    for (int j = 1; j < dim; j++) {
	if (l_maxval[j] - l_minval[j] > l_deltamax)
	    l_deltamax = fabs(l_maxval[j] - l_minval[j]);
    }

    scale = l_deltamax;

    //initialize color
    points_color = new float[npoints * color_depth];
    setColordepth(color_depth);

    // WE SET Voxel->VOXEL_COLOR = ShapeData->POINTS_COLOR 
    voxel_color = points_color;

    for (int i = 0; i < npoints; i++) {
	voxel_color[i * color_depth] = 0.5f;
	voxel_color[i * color_depth + 1] = 0.5f;
	voxel_color[i * color_depth + 2] = 0.5f;
    }

    // Releasing locally intialized data.
    if (l_val)
	delete[]l_val;
    if (l_mean)
	delete[]l_mean;
    if (l_minval)
	delete[]l_minval;
    if (l_maxval)
	delete[]l_maxval;

    // The voxel set has been loaded. General flag declared in ShapeData.h.
    loaded = 1;

    DBG_(fprintf(stderr, "End of readPoints \n"));


    return 0;

}

int Voxel::readMatlabVoxelGrid(const char *p_file, int p_sample_rate)
{

    DBG_(fprintf(stderr, "-->readMatlabVoxelGrid()\n"));
    if (!p_file) {
	FATAL_(fprintf(stderr, "Filename is empty \n"), FATAL_OPEN_FILE);
    }

    in.open(p_file, fstream::in);
    if (!in.is_open()) {
	FATAL_(fprintf(stderr, "Could not open %s \n", p_file),
	       FATAL_OPEN_FILE);
    }
    if (filename) {
	delete[]filename;
	filename = NULL;
    }
    filename = new char[str_length];
    sprintf(filename, "%s", p_file);


    //count lines
    char l_line[str_length];
    int l_count = 0;
    while (!in.eof()) {
	in.getline(l_line, str_length);
	l_count++;
    }
    npoints = l_count - 1;
    in.close();
    DBG_(fprintf(stderr, " Allocating memory for %d voxels\n", npoints));


    releaseData();

    in.open(p_file, fstream::in);
    in.getline(l_line, 1024);

    //fill bounding box;
    sscanf(l_line, "vox = zeros(%d,%d,%d)", &bounding_box[0],
	   &bounding_box[1], &bounding_box[2]);
    DBG_(fprintf
	 (stderr, " Bounding vox %d %d %d\n", bounding_box[0],
	  bounding_box[1], bounding_box[2]));

    //Allocate memory for voxels        
    npoints = npoints / p_sample_rate;
    points = new FL[npoints * dim];

    //Point voxel data to generic data points
    nvoxels = npoints;
    voxel = points;

    FL *l_mean = NULL;
    l_mean = new FL[dim];

    FL *l_minval = NULL;
    l_minval = new FL[dim];

    FL *l_maxval = NULL;
    l_maxval = new FL[dim];

    float *l_val = NULL;
    l_val = new float[3];	//According to format there should be 3 or 4 elements per line

    for (int i = 0; i < dim; i++) {
	l_mean[i] = 0.0;
	l_minval[i] = 0.0;
	l_maxval[i] = 0.0;
    }

    DBG_(fprintf(stderr, " Reading voxels from : %s ... ", p_file));
    //Read voxels
    int l_iret = 0;

    for (int i = 0, sample = p_sample_rate; i < nvoxels; sample++) {
	l_iret = 0;

	in.getline(l_line, 1024);
	l_iret =
	    sscanf(l_line, "vox(%g,%g,%g) = 1", &l_val[0], &l_val[1],
		   &l_val[2]);

	int idim = i * dim;
	if (sample == p_sample_rate) {
	    voxel[idim] = (FL) l_val[0];
	    voxel[idim + 1] = (FL) l_val[1];
	    voxel[idim + 2] = (FL) l_val[2];

	    for (int d = 0; d < dim; d++) {
		l_mean[d] += voxel[idim + d];
		if ((voxel[idim + d] < l_minval[d]) || (i == 0))
		    l_minval[d] = voxel[idim + d];
		if ((voxel[idim + d] > l_maxval[d]) || (i == 0))
		    l_maxval[d] = voxel[idim + d];
	    }

	    i++;
	    sample = 0;
	}
    }

    in.close();

    DBG_(fprintf(stderr, "Finding mean and scale..."));
    for (int d = 0; d < dim; d++)
	l_mean[d] = l_mean[d] / ((FL) nvoxels);

    //center data
    for (int i = 0; i < nvoxels; i++) {
	for (int d = 0; d < dim; d++) {
	    voxel[i * dim + d] = voxel[i * dim + d] - l_mean[d];
	}
    }

    //find scale
    FL l_deltamax = fabs(l_maxval[0] - l_minval[0]);
    for (int j = 1; j < dim; j++) {
	if (l_maxval[j] - l_minval[j] > l_deltamax)
	    l_deltamax = fabs(l_maxval[j] - l_minval[j]);
    }

    scale = l_deltamax;

    //initialize color

    points_color = new float[npoints * color_depth];
    setColordepth(color_depth);

    voxel_color = points_color;

    for (int i = 0; i < npoints; i++) {
	voxel_color[i * color_depth] = 0.5f;
	voxel_color[i * color_depth + 1] = 0.5f;
	voxel_color[i * color_depth + 2] = 0.5f;
    }

    if (l_val)
	delete[]l_val;
    if (l_mean)
	delete[]l_mean;
    if (l_minval)
	delete[]l_minval;
    if (l_maxval)
	delete[]l_maxval;


    loaded = 1;

    DBG_(fprintf(stderr, "End of read_matlab_voxelgrid \n"));

    return 0;

}

int Voxel::buildPointList()
{
    int res = buildPointsetList(cube_size);
    return res;
}

int Voxel::buildVoxelsetList()
{
    int res = buildPointsetList(cube_size);
    return res;
}


int Voxel::readFunctionValue(const char *p_filename)
{

    if (p_filename == NULL) {
	ERROR_(std::
	       cout << "in readFunctionValue, can not open an empty string"
	       << std::endl, ERROR_OPEN_FILE);
    }

    std::fstream infile;
    infile.open(filename);

    if (infile.is_open() == 0) {
	FATAL_(fprintf(stderr, "Could not open %s \n", filename),
	       FATAL_OPEN_FILE);
    }

    int l_nvox, l_nfunc;
    infile >> l_nvox;
    infile >> l_nfunc;

    DBG_(fprintf
	 (stderr, "Reading %d function with %d value from %s\n", l_nfunc,
	  l_nvox, p_filename));

    // Store temporarly the function values. 
    FL *l_functionvals = NULL;
    l_functionvals = new FL[l_nvox * l_nfunc];

    bzero(l_functionvals, l_nvox * l_nfunc * sizeof(FL));
    for (int i = 0, l_count = 0; i <= l_nvox; i++) {
	for (int j = 0; j <= l_nfunc; j++, l_count++) {
	    infile >> l_functionvals[l_count];
	}
    }
    infile.close();

    // Set the function value associated with each voxels.
    loadFunctionValue(l_functionvals, l_nfunc);

    // Release locally allocated memory
    if (l_functionvals)
	delete[]l_functionvals;

    return 0;
}



int Voxel::readVoxelsAndFunctionValue(const char *p_filename)
{
    PRINT_(std::cout << "-->read voxels and embedding()" << std::endl);

    if (filename) {
	delete[]filename;
	filename = NULL;
    }

    filename = new char[1024];
    sprintf(filename, "%s", p_filename);

    std::fstream infile;
    infile.open(filename);

    if (infile.is_open() == 0) {
	FATAL_(fprintf(stderr, "Could not open %s \n", p_filename),
	       FATAL_OPEN_FILE);
    }
    //free data depending on nvoxels

    releaseData();
    int l_buff_size = 0 ;
    HASH_MAP_PARAM_(int, "buff_size", Int, l_buff_size) ;

    char l_buffer[l_buff_size];
    while (infile.peek() == '#' || infile.peek() == ' '
	   || infile.peek() == '\n') {
	infile.getline(l_buffer, l_buff_size);
    }

    //Read the number of voxels and nfunctions
    infile >> npoints;
    nvoxels = npoints;
    infile >> n_functions;
    DBG_(std::cout << " Reading " << nvoxels << " voxels" << std::endl);
    DBG_(std::cout << " Reading " << n_functions << " functions" << std::
	 endl);

    //Allocate memory for voxels
    dim = 3;
    points = new FL[nvoxels * dim];
    memset(voxel, 0, nvoxels * dim * sizeof(FL));

    voxel = points;

    FL *l_mean = NULL;
    l_mean = new FL[dim];
    memset(l_mean, 0, dim * sizeof(FL));

    FL *l_minval = NULL;
    l_minval = new FL[dim];
    memset(l_minval, 0, dim * sizeof(FL));

    FL *l_maxval = NULL;
    l_maxval = new FL[dim];
    memset(l_maxval, 0, dim * sizeof(FL));
    FL *l_functionvals = NULL;
    l_functionvals = new FL[nvoxels * n_functions];
    bzero(l_functionvals, nvoxels * n_functions * sizeof(FL));

    int l_dummy;
    int l_val;
    for (int i = 0; i < nvoxels; i++) {
	//Read dummy mask
	infile >> l_dummy;

	//Read voxels and           
	//fill mean and min and max values          
	for (int d = 0; d < dim; d++) {
	    infile >> l_val;
	    voxel[i * dim + d] = (FL) l_val;
	    l_mean[d] += l_val;
	    if ((l_val < l_minval[d]) || (i == 0))
		l_minval[d] = l_val;
	    if ((l_val > l_maxval[d]) || (i == 0))
		l_maxval[d] = l_val;
	}

	//Read functions        
	for (int j = 0; j < n_functions; j++) {
	    infile >> l_functionvals[i * n_functions + j];
	}
    }
    infile.close();

    loadFunctionValue(l_functionvals, n_functions);

    DBG_(std::cout << "Finding mean and scale" << std::endl);
    for (int d = 0; d < dim; d++)
	l_mean[d] = l_mean[d] / ((FL) nvoxels);

    //center data
    for (int i = 0; i < nvoxels; i++) {
	for (int d = 0; d < dim; d++) {
	    voxel[i * dim + d] -= l_mean[d];
	}
    }

    //find scale
    FL l_deltamax = fabs(l_maxval[0] - l_minval[0]);
    for (int j = 1; j < dim; j++) {
	if (l_maxval[j] - l_minval[j] > l_deltamax)
	    l_deltamax = fabs(l_maxval[j] - l_minval[j]);
    }

    scale = l_deltamax;

    //initialize color  
    points_color = new float[nvoxels * color_depth];
    voxel_color = points_color;
    for (int i = 0; i < nvoxels; i++) {
	voxel_color[i * color_depth] = 0.5f;
	voxel_color[i * color_depth + 1] = 0.5f;
	voxel_color[i * color_depth + 2] = 0.5f;
    }


    if (l_mean)
	delete[]l_mean;
    if (l_minval)
	delete[]l_minval;
    if (l_maxval)
	delete[]l_maxval;
    if (l_functionvals)
	delete[]l_functionvals;

    loaded = 1;

    return 0;
}




FL *Voxel::getVoxels()
{
    return getPoints();
}

int Voxel::isGraphLoaded()
{
    if (connect)
	return 1;
    else
	return 0;
}

int Voxel::getNumOfVoxels()
{
    return getNumOfPoints();
}


int Voxel::readLabelValue(const char *p_file)
{

    FILE *f = NULL;
    PRINT_(fprintf
	   (stderr, " Reading label values from file %s\n", p_file));

    f = fopen(p_file, "r");
    if (!f) {
	ERROR_(fprintf(stderr, " %s file not found\n", p_file),
	       ERROR_OPEN_FILE);
    }


    char l_buf[str_length];
    char *l_ret = NULL;

    do {
	if (!(l_ret = fgets(l_buf, sizeof(l_buf), f))) {
	    ERROR_(fprintf
		   (stderr, "Error reading the function values file\n"),
		   ERROR_OPEN_FILE);
	}

    } while ((l_buf[0] == '#') || (l_buf[0] == '\n') || (l_buf[0] == ' '));

    sscanf(l_buf, "%d", &nvoxels);
    DBG_(std::cout << "Nb voxels : " << nvoxels << std::endl);

    //Allocate memory for function_value
    if (lbl_value) {
	delete[]lbl_value;
	lbl_value = NULL;
    }
    lbl_value = new FL[nvoxels];

    FL l_minval, l_maxval;
    l_minval = 0.0;
    l_maxval = 0.0;


    //Read values
    for (int i = 0; i < nvoxels; i++) {
	if (!(l_ret = fgets(l_buf, sizeof(l_buf), f))) {
	    ERROR_(fprintf
		   (stderr, "Error reading the function values file\n"),
		   ERROR_OPEN_FILE);
	}

	lbl_value[i] = atoi(l_buf);
	if ((lbl_value[i] < l_minval) || (i == 0))
	    l_minval = lbl_value[i];
	if ((lbl_value[i] > l_maxval) || (i == 0))
	    l_maxval = lbl_value[i];
    }

    fclose(f);

    lbl_min = l_minval;
    lbl_max = l_maxval;

    return 0;

}

int Voxel::fillLabelColorValue()
{
    if (!colormap) {
	ERROR_(fprintf
	       (stderr,
		"Colormap has to be built before trying to fill color with function value\n"),
	       ERROR_EMPTY_DATA);
    }
    if (!lbl_value) {
	ERROR_(fprintf
	       (stderr,
		"Function values have to be loaded before trying to fill color with function value\n"),
	       ERROR_EMPTY_DATA);
    }

    int ind;
    for (int i = 0; i < nvoxels; i++) {
	ind = (int) round(lbl_scale_factor * (lbl_value[i] - lbl_min));
	if (lbl_value[i] >= 0) {
	    voxel_color[i * color_depth] = colormap[ind * color_depth];
	    voxel_color[i * color_depth + 1] =
		colormap[ind * color_depth + 1];
	    voxel_color[i * color_depth + 2] =
		colormap[ind * color_depth + 2];
	    if (color_depth == 4)
		voxel_color[i * color_depth + 3] = 1;
	} else {
	    voxel_color[i * color_depth] = 0.5;
	    voxel_color[i * color_depth + 1] = 0.5;
	    voxel_color[i * color_depth + 2] = 0.5;
	    if (color_depth == 4)
		voxel_color[i * color_depth + 3] = 1;
	}
    }
    return 0;
}

FL *Voxel::getLabels()
{
    return lbl_value;
}

int *Voxel::getBoundingBox()
{
    return bounding_box;
}

int Voxel::drawVoxelsetList()
{
    int res = drawPointsetList();

    return res;
}

int Voxel::drawVoxelsetPoints()
{
    int res = drawPointsetList();
    return res;
}

int Voxel::drawVoxelsetGraph()
{
    // nvox number of nvoxels in the external routine (to verify it corresponds)
    // nn number of neighbors per voxel
    // Matrix of neighbors
    if (connect) {
	glColor3f(0.75, 0.35, 0.0);
	glLineWidth(2.0f);
	glBegin(GL_LINES);
	FL l_x1, l_y1, l_z1, l_x2, l_y2, l_z2;
	int l_graph_sampling_rate ;
	HASH_MAP_PARAM_(int, "graph_sampling_rate", Int, l_graph_sampling_rate) ;

	int l_delta = l_graph_sampling_rate;
	for (int i = 0; i < nzelems; i++) {
	    int ni = connect[i * 2];
	    if (ni % l_delta == 0)  {
		int nj = connect[i * 2 + 1];
		l_x1 = (float) (voxel[ni * dim] / scale);
		l_y1 = (float) (voxel[ni * dim + 1] / scale);
		l_z1 = (float) (voxel[ni * dim + 2] / scale);
		l_x2 = (float) (voxel[nj * dim] / scale);
		l_y2 = (float) (voxel[nj * dim + 1] / scale);
		l_z2 = (float) (voxel[nj * dim + 2] / scale);
		glVertex3f(l_x1, l_y1, l_z1);
		glVertex3f(l_x2, l_y2, l_z2);
		DBG2_(fprintf
		      (stderr, "%g %g %g\n", voxel[nj * dim],
		       voxel[nj * dim + 1], voxel[nj * dim + 2]));
	    }
	}
	glEnd();
    } else {
	DBG_(std::
	     cout << "Drawing connection graph failed : connect = NULL" <<
	     std::endl);
    }

    drawVoxelsetPoints();
    return (0);
}





int Voxel::resetVoxelColor()
{
    return resetPointColor();
}
int Voxel::setVoxelColor(int p_ind, float *p_r, float *p_g, float *p_b)
{
    return setPointColor(p_ind, p_r, p_g, p_b);
}

int Voxel::getVoxelColor(int p_ind, float *p_r, float *p_g, float *p_b)
{
    return getPointColor(p_ind, p_r, p_g, p_b);
}

void Voxel::setLblScaleFactor(FL p_scale)
{

    lbl_scale_factor = p_scale;
}

/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
/*
  Taken form libit - Library for basic source and channel coding functions
  Copyright (C) 2005-2006 Vivien Chappelier, Herve Jegou
*/

/*
  Timer, only for linux
  Copyright (C) 2006 Herve Jegou
  Thanks to Pierre-Hugues Joalland for the original code
*/

#ifndef __timer_h_
#define __timer_h_

#include <sys/time.h>
#include <sys/resource.h>



typedef struct {
    unsigned short status;	/* timing status : 0 <=> idle, 1 <=> active */

    struct timeval time_now_wall;	/* now registered wall time */
    struct timeval time_last_wall;	/* last registered wall time */
    double amount_wall;		/* amount of wall time */

    struct rusage time_now_cpu;	/* now registered cpu (user + syst) time  */
    struct rusage time_last_cpu;	/* last registered cpu (user + syst) time */
    double amount_user;		/* amount of user time */
    double amount_system;	/* amount of system time */
} it_timer_t;


extern "C" {
/* Initialization of the timer */
    it_timer_t *timer_new();

/* Reinitialization of the timer */
    void timer_rtz(it_timer_t * timer);

/* Free the timer */
    void timer_free(it_timer_t * timer);

/* Set the timer on or off */
    void timer_on(it_timer_t * timer);
    void timer_off(it_timer_t * timer);

/* Return the different kind of usages (CPU is system+user)*/
    double timer_wall(it_timer_t * timer);
    double timer_user(it_timer_t * timer);
    double timer_system(it_timer_t * timer);
    double timer_cpu(it_timer_t * timer);
}
#endif

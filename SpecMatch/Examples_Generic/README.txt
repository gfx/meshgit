We provide here a set of examples to illustrate the use of this
software.

This directory contains the scripts to run the embedding, the matching
and the visualization of the results.

In each sub-directory, the data are included and the default options also. 

For each examples:      - 2 meshes/voxel-sets are provided.
			- 1 File to set the variables for all the
scripts.


#############################################################
#########################IMPORTANT###########################
#############################################################

Gorilla: 
The provided poses are taken from the TOSCA database
avalaible here: 
http://tosca.cs.technion.ac.il/data_3d.html

The 2 poses are courtesy of M. and A. Bronstein.

One can look at:
A. M. Bronstein, M. M. Bronstein, R. Kimmel, Calculus of
non-rigid surfaces for geometry and texture manipulation, IEEE
Trans. Visualization and Computer Graphics, Vol. 13/5, pp. 902-913,
2007.



Wolf: 
The provided poses are taken from the TOSCA database
avalaible here: 
http://tosca.cs.technion.ac.il/data_3d.html

The 2 poses are courtesy of M. and A. Bronstein.

One can look at:
A. M. Bronstein, M. M. Bronstein, R. Kimmel, Calculus of
non-rigid surfaces for geometry and texture manipulation, IEEE
Trans. Visualization and Computer Graphics, Vol. 13/5, pp. 902-913,
2007.


Manequin:
The 2 poses are proprietary. One should email the authors for
redistribution of the two poses.

Dancer:
The provided poses are courtesy of Adrian Hilton, Surrey UK. Those
poses are taken from Surrey JP datasets, and might be available on
request to: a.hilton@surrey.ac.uk.

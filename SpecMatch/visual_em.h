/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
/*
 *  visual_em.h
 *  match
 *
 *  Created by Diana Mateus and David Knossow on 9/30/07.
 *  Copyright 2007 INRIA. All rights reserved.
 *
 * This code provides the Graphical interace to control display of the matching.
 *
 */
#ifndef VISUALEM_H
#define VISUALEM_H


#include <iostream>


#ifdef MACOSX
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif


#include<QGLViewer/qglviewer.h>

#ifndef QT_VERSION
#include <qglobal.h>
#endif

#if QT_VERSION < 0X040000
# include <qpopupmenu.h>
#else
# include <QMenu>
# include <QKeyEvent>
#endif
#include <qmap.h>

#include "thread_em.h"

#include "match_em.h"

#include "drawprim.h"

#ifdef ENABLE_DISP_HISTOGRAM
#if QT_VERSION < 0x040000
#define QT3
#include "DispHistograms.h"
#else
#define QT4
#include "DispHistograms.Qt4.h"
#endif
#endif


class VisualMatch:public QGLViewer {



  private:
    // Enable/Disable animation
    int animate_on;

    //Prepares the indicated dimension to be incremented/decreased
    // with the +/- functions
    int current_sel_dim;

    //selected dimensions to show
    int seld[3];

    // Translation to apply to one the model for ddisplay purpose
    float model_sep[3];

    //Sampling of matching to show.
    int line_sample;

    //to make whatever fit into the displayed unitary ball
    float scale;
 
    //Variables for drawing global covariance ellipses
    bool show_ellipses;

    //Show matches between 3D shapes.
    bool show_matches;

    //Show best matches, if available.
    bool show_best_class;

    // Show best N observations attached to each model.
    bool show_best_observations;

    //Rescale the covariance, when displaying it.
    FL *ellip_scale;

    FL rotangle;
    FL rotvec[3];
    FL rotmat[9];

    //Variables for threading the visualization and the EM computations.
    ThreadMatch *C_EM;

    // Pointer on shape 1 (voxels, meshes, embeddings...)
    FL *X;
    // Pointer on shape 2 (voxels, meshes, embeddings...)
    FL *Y;

    // Number of eigen functions to treat (set by the --dim option)
    int num_of_eig;

    // Number of coordinates for each eigenvectors for shape 1.
    int nX;
    // Number of coordinates for each eigenvectors for shqpe 2.
    int nY;

    // Pointer on the second shape to wich T has been applied.
    FL *Yt;

    // Weighted contributions of X to be actually compared with Y
    FL *W;

    // outliers : partition values to small
    int *maskX;
    int *maskY;

    // Max number of class to select as best matches
    int nmaxmatch;

    // Pointer to match-em data.
    // Best model for each observation
    int *agmatch;
    // Best N observations for each cluster.
    int *nagmatch;

    // Is matching allready performed
    bool match_available;

    // sigma of inliers
    FL sigma;

    //Thread flow
    EM_MODE em_mode;
    bool copied;
    bool modified_data;
    bool disable_draw;
    bool drawing;
    bool EM_done;


    // Find the scale for displaying purposes.
    int findScale();
    //Compute the ellipse scale from the covariance matrix
    int covarianceEllipsoid();

    int rotmatrix2rotvec();
    int toggleEllipses();
    int toggleMatches();

#ifdef ENABLE_DISP_HISTOGRAM
    //Histograms
    HistoWidget *C_Histo_disp_X;
    HistoWidget *C_Histo_disp_Y;
    HistoWidget *C_Histo_disp_FY;
    void showHistograms();
#endif

    CvMat *covmat_;

  protected: virtual void draw();
    virtual void init();
    virtual void animate();
    virtual void keyPressEvent(QKeyEvent * e);

    // Enable or Disable the saving of images.
    bool save_on;
    
    // In case of testing the EM procedure, we add outliers manually.
    // old_nX is the number of point without outliers.
    int old_nX ;
    
 public:
    VisualMatch();
    ~VisualMatch();

    // Intialize matching
    int visualEM(INIT_MODE init_m, EM_MODE em_m, int *int_arg =
		 NULL, FL * fl_arg = NULL);
    
    
    // Load data from files
    int loadData(int nx, int ny, int dim, FL * inX, FL * inY,
		 char *filename = NULL);
    
    // Call match-em function
    int setSigma(FL val);
    // Set the outlier term in the assignment procedure.
    int setOutlierConstant(FL val);
    //Set the parameters to build the histograms.
    int setHistoParams(int nbins, HISTOGRAM_DISTANCE histDist,
		       ASSIGNMENT_METHOD assignMethod, FL disc_ratio);

    //Set the rigid transformation to estimate between the embeddings.
    int setTransfoMode(TRANSFO_MODE mode );

    //Set the sampling rate to display the matches.
    int setLineSample(int val);


    //Set the less memory greedy algorithm ON or OFF.
    void setAlphaLineMode(bool on);

    // Most of the time, the constant eigenvector is stored, 
    //for the matching purpose, remove it.
    void setRemoveFirst(bool remove_first);


    // Copy data for display purposes
    void copyData();

    // Release copied data for display purposes
    void releaseCopiedData();

    //Set the translation between shapes, for display purpose
    void setModelSep (float x, float y, float z) ;


    
        
};
#endif

/********************************************************************************/
/* This file is part of SpecMatch.						*/
/* SpecMatch : Spectral  Matching						*/
/* SpecMatch is free software: you can redistribute it and/or modifyit		*/
/* under the terms of the GNU General Public License as published by the	*/
/* Free Software Foundation, either version 3 of the License, or (at your	*/
/* option) any later version.							*/
/*										*/
/* SpecMatch is distributed in the hope that it will be useful, but WITHOUT	*/
/* ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or	*/
/* FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License	*/
/* for more details.								*/
/*										*/
/* You should have received a copy of the GNU General Public License		*/
/* along with SpecMatch.  If not, see <http://www.gnu.org/licenses/>.		*/
/*										*/
/*   Copyright (C) 2007, 2008 INRIA						*/
/*   Authors : Diana Mateus, David Knossow, Radu Horaud				*/
/*   Contact :	mateus@in.tum.de						*/
/*										*/
/* It has been deposited to the "Agence pour la Protection des Programmes"      */
/* It has an Inter Deposit Digital Number:					*/
/* IDDN.FR.001.100003.000.S.P.2009.000.10800					*/
/*										*/
/********************************************************************************/
#ifndef SHAPE_H
#define SHAPE_H
/*
 *  shape.h
 *  match
 *
 *  Created by Diana Mateus and David Knossow on 7/30/07.
 *  Copyright 2007 INRIA. All rights reserved.
 *
 * This class is a container for all data required to perform the matching.
 * It contains 3D data such as Voxels/Meshes/Embeddings
 * It allows to read initialization matrices (.initT and T) to initialize the matching
 * It also store the matching output.
 * 
 */


#include <qmap.h>
#include <fstream>

#include "utilities.h"
#include "mesh.h"
#include "voxel.h"


#include "embedding.h"





class Shape {

  protected:
    //filenames
    // Store the filename for initial RIGID transfo matrix (.T file)
    char *transfo_filename;

    // Store the filename for initial permutation/sign flip matrix (.initT file).
    char *init_transfo_filename;

    // Store the filename for the matching between the 2 set of loaded points.
    char *binmatch_filename;

    // Match
    // number of matches stored in the binmatch file
    int nmatch;
    // Matches stored as 2xnmatches array 
    int *match;


    // indicates which set corresponds to which colum in the binary match, 
    //or wich should be read in rows(0) or in columns(1) for the probabilistic matching
    int *match_id;

    // Transfo
    // Do we need to update InitT or not along time. 
    bool apply_init_transfo_once;

    // Store the dimension of the probabilistic match matrix.
    int pm_dim[2];

    // Store probabilistic matches as an array
    double *proba_vec;
    // Store probabilistic matches as a matrix
    double **proba_matrix;

    // Store the size of the T and initT matrices
    int dimx, dimy;
    // Store the size of initT read from a file
    int mat_init_transfo_dim;
    // Store the size of T read from a file (or copied from initT)0
    int mat_transfo_dim;
    // Store the RIGID transfo matrix
    FL *transfo;
    // Store the permutation/sign flip matrix
    FL *init_transfo;

    //Embedding
    // Stores the embeddings once the dimension selection had been done
    FL *coords_transfo_1;

    // Stores the embeddings once the dimension selection had 
    // been done and initT had been applied to the original embedding
    FL *coords_transfo_2_t_dim_sel;

    // Stores the embeddings once T had been applied to the embedding "coords_transfo_2_t_dim_sel"    
    FL *coords_transfo_2;



    // sequence of matches
    // Controls the snapshots of matches (one snap per match)
    bool match_saved;


    // TEMPORAL EVOLUTION OF MATCHES FOR 2 SAME POSES
    // File pattern for match (temporal evolution of the matching for the same 2 poses)
    char *match_pattern;

    // Time id for the matching evolution
    int match_it;

    // Time id for the first match of the sequence
    int match_first_it;

    // Time id for the last match of the sequence
    int match_last_it;
    // Time between two matches. (time step)
    int match_delta;

    // MATCHES FOR DIFFERENT POSES
    // FIlename patterns for the sequence of frames.
    char *voxel_pattern;
    char *mesh_pattern;
    char *embed_pattern;
        // Obvious
    int setPattern(char *&internal_pattern, char *pattern);

    //saving
    QString save_filename;


    
    //Enable/Disable the sort of matches (with respect to sort of meshes)
    bool enable_reshape_binary_match ;

  public:
     Shape();
    ~Shape();

    // Release all allocated memory
    void releaseAll();

    // Obvious
    Voxel *C_Voxel1;
    Voxel *C_Voxel2;

    // Obvious
    Mesh *C_Mesh1;
    Mesh *C_Mesh2;

    Mesh *C_Mesh3;
    Mesh *C_Mesh4;


    
    // Obvious  
    Embedding *C_Embed1;
    Embedding *C_Embed2;

    // Allocate both meshes and voxel classes. 
    // We might want to implement a smarter way for this allocation.
    void allocate3DShape();

    // Allocate the embedding classes
    void allocateEmbedding();

    //read 
    // Read the matching from a file.
    int readBinaryMatch(const char *filename = NULL);

    // Read the probabilistic matching from a file.
    int readProbabilisticMatch(const char *filename = NULL);

    // Read the intial rigid and permutation/sign flips matrices from a file.
    int readTransfo(const char *filename = NULL, int in_dimx =
		    0, int in_dimy = 0, bool remove_first = false);

    // Number of matches read from the matching file.
    int getNumOfMatch();
    // Returns a pointer on match
    int *getMatch();
    // Returns a copy of match. out_match must be preallocated.
    int getMatch(int **out_match);

    // returns a pointer on match_id
    int *getMatchId();

    // Copy the Transformation matrix into out_transfo, 
    // removing(/not) first column/row and selecting a sub matrix.
    int getTransfo(FL ** out_transfo, bool remove_first, int out_dim);

    //sequences of matches (exclusive)
    int setSequenceLimits(int first, int last, int delta = 1);
    int setMatchfilePattern(char *pattern);
    int setVoxelPattern(char *pattern);
    int setMeshPattern(char *pattern);
    int setEmbedPattern(char *pattern);
        // Initialize applyInitTOnce 
    void setApplyInitTOnce(bool status);

    // Computes T x initT x Coords (transform the embedding 
    // given the estimated (or read) transformation file.
    int transformEmbedCoord(bool solelyApplyInitT = false);

    // Copy only selected dimensions. 
    // This selection is performed during the intialization
    int performDimensionSelection(FL *);

    //Body Part Performance estimation
    double computePerformance(int BodyID);

    // if sorting is required, the matches are reshaped to fit 
    // the reodering.
    void reshapeBinaryMatch (int *sort1, int* sort2) ;


    // Enable or disable the sort of points on the mesh.
    void enableReshapeBinaryMatch (bool status) ;
};
#endif

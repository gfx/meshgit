import pickle

from vec3f import *
from pyglet.gl import *

class Camera(object):
    def __init__( self, position, focus, up, scale=1.0, angle=45.0 ):
        self.ex,self.ey,self.ez = position
        self.fx,self.fy,self.fz = focus
        self.ux,self.uy,self.uz = up
        self.angle = angle
        self.scale = scale
    
    def resize( self, width, height ):
        glMatrixMode( GL_PROJECTION )
        glLoadIdentity()
        gluPerspective( self.angle, float(width) / float(height), 0.1, 100.0 )
        self.render()
    
    def render( self ):
        glMatrixMode( GL_MODELVIEW )
        glLoadIdentity()
        gluLookAt( self.ex,self.ey,self.ez, self.fx,self.fy,self.fz, self.ux,self.uy,self.uz )
        glScalef( self.scale, self.scale, self.scale )
    
    def get_dist( self ):
        x,y,z = self.ex-self.fx,self.ey-self.fy,self.ez-self.fz
        return math.sqrt( x*x+y*y+z*z )
    
    def get_fwd( self ):
        e = (self.ex,self.ey,self.ez)
        f = (self.fx,self.fy,self.fz)
        return Vec3f.t_norm( Vec3f.t_sub( f, e ) )
    
    def get_up( self ):
        f = self.get_fwd()
        u = Vec3f.t_norm( (self.ux,self.uy,self.uz) )
        r = Vec3f.t_norm( Vec3f.t_cross( f, u ) )
        return Vec3f.t_norm( Vec3f.t_cross( r, f ) )
    
    def get_right( self ):
        f = self.get_fwd()
        u = Vec3f.t_norm( (self.ux,self.uy,self.uz) )
        return Vec3f.t_norm( Vec3f.t_cross( f, u ) )
    

import bpy

def layers( li_layer ):
    return [ (i in li_layer) for i in range(20) ]
def viewlayers( li_layer ):
    bpy.context.scene.layers = layers( li_layer )
def selectall():
    bpy.ops.object.select_all(action='SELECT')
def deselectall():
    bpy.ops.object.select_all(action='DESELECT')

viewlayers( [0] )
bpy.ops.import_mesh.ply(filepath='m0.ply')
viewlayers( [1] )
bpy.ops.import_mesh.ply(filepath='ma.ply')
viewlayers( [2] )
bpy.ops.import_mesh.ply(filepath='mb.ply')

viewlayers( [5] )
bpy.ops.import_mesh.ply(filepath='m0ab.ply')

viewlayers( [10] )
bpy.ops.import_mesh.ply(filepath='m0_.ply')
viewlayers( [11] )
bpy.ops.import_mesh.ply(filepath='ma_.ply')
viewlayers( [12] )
bpy.ops.import_mesh.ply(filepath='mb_.ply')

viewlayers( [15] )
bpy.ops.import_mesh.ply(filepath='m0a_.ply')
viewlayers( [16] )
bpy.ops.import_mesh.ply(filepath='m0b_.ply')


viewlayers( range(20) )
deselectall()
viewlayers( [5] )
selectall()
bpy.context.scene.objects.active = bpy.data.objects['m0ab']


class DoneMergeOperator( bpy.types.Operator ):
    bl_idname = "object.donemerge"
    bl_label = 'Done Merging'
    bl_description = 'NOTE: Select the final merge object before clicking!!!'
    #bl_space_type = "PROPERTIES"
    #bl_region_type = "WINDOW"
    #bl_context = "object"
    
    @classmethod
    def poll( cls, context ):
        return context.active_object is not None and len(context.selected_objects) == 1
    
    def execute( self, context ):
        #if len(bpy.context.selected_objects) != 1:
        #    return('FINISHED')
        bpy.ops.object.shade_smooth()
        bpy.ops.export_mesh.ply(filepath='m0ab_.ply',use_modifiers=False,use_normals=False,use_uv_coords=False,use_colors=False)
        bpy.ops.wm.quit_blender()

def register():
    bpy.utils.register_class( DoneMergeOperator )
def unregister():
    bpy.utils.unregister_class( DoneMergeOperator )

register()

#menu_func = ( lambda self, context: self.layout.operator('doneMergeButton') )
#bpy.types.VIEW3D_PT_tools_objectmode.prepend( menu_func )
#bpy.types.register( DoneMergeOperator )


# -*- coding: utf-8 -*-

import os, glob, sys, subprocess, time, json, re
from array import array

################################################################################
### list, Vector, tuple functions

# http://rightfootin.blogspot.com/2006/09/more-on-python-flatten.html
def flatten(l, ltypes=(list, tuple)):
    """
    flattens nested lists to a single list :)
    """
    ltype = type(l)
    l = list(l)
    i = 0
    while i < len(l):
        while isinstance(l[i], ltypes):
            if not l[i]:
                l.pop(i)
                i -= 1
                break
            else:
                l[i:i + 1] = l[i]
        i += 1
    return ltype(l)

def string_to_vector( s ):
    sp = s.split(',')
    return (float(sp[0]),float(sp[1]),float(sp[2]))

################################################################################
### process functions

def exec_wait( arglists ):
    return subprocess.Popen( flatten( arglists ), stderr=subprocess.STDOUT, stdout=subprocess.PIPE ).communicate()

################################################################################
### file functions

def file_to_json( jsonfn ):
    if isinstance(jsonfn,list):
        fn = os.path.join(jsonfn[0],jsonfn[1])
    else:
        fn = jsonfn
    with open( os.path.abspath( fn ) ) as f:
        j = json.load( f )
    return j

def json_to_file( jsonfn, j ):
    j_string = json.dumps( j, indent=4, sort_keys=True )
    with open( jsonfn, 'w' ) as f:
        f.write( j_string )

def array_to_file( filename, a, c ):
    header = str.encode('B' + a.typecode)
    l = int(len(a)/c)
    dim = array('i', [l, 1, c])
    
    with open( filename, "wb" ) as f:
        f.write(header)
        dim.tofile(f)
        a.tofile(f)

def file_to_array( filename, array_type, c ):
    a = array( array_type )
    dim = array('i')
    
    with open(filename, "rb") as f:
        header = f.read(2)
        assert header[0] == 'B' and header[1] == array_type, "unexpected header in positions file (%s): '%s'" % (filename,header)
        dim.read( f, 3 )
        assert dim[1] == 1 and dim[2] == c, "unexpected dim in positions file (%s): %d, %d, %d" % (filename,dim[0],dim[1],dim[2])
        a.read( f, dim[0] * c )
    
    return a

#### refactor code to use above functions (or similar naming conventions)
def loadjson( jsonfn ):
    with open( os.path.abspath( jsonfn ) ) as f:
        j = json.load( f )
    return j

def loadjsonfromstepfn( stepfn ):
    return loadjson( "%s.json" % stepfn )

def loadjsonfromblendfn( blendfn ):
    (stepfn,ext) = os.path.splitext( blendfn )
    return loadjsonfromstepfn( stepfn )

def writejson( jsonfn, j ):
    j_string = json.dumps( j, indent=4, sort_keys=True )
    with open( jsonfn, 'w' ) as f:
        f.write( j_string )

def load_positions( posfn ):
    return file_to_array( posfn, 'f', 3 )

def load_selected( selfn ):
    return file_to_array( selfn, 'b', 1 )
####

# returns true if filename0 and filename1 are identical
def compare_files(filename0, filename1):
    same = True
    with open(filename0, "rb" ) as f0:
        with open(filename1, "rb" ) as f1:
            b0 = f0.read(1)
            b1 = f1.read(1)
            while b0 == b1 and b0 != "" and b1 != "":
                b0 = f0.read(1)
                b1 = f1.read(1)
            if b0 != b1:
                same = False
    return same

def count_files( filepattern ):
    return len( glob.glob( filepattern ) )

def get_last_stepfn():
    files = glob.glob( allstepfiles )
    if( len(files) == 0 ):
        return None
    files.sort()
    return files[len(files) - 1]

def remove_files( filepattern ):
    for fn in glob.glob( filepattern ):
        os.remove( fn )



#include <string>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "mesh.h"

using namespace std;


// mesh data
float *lvp0, *lvp1;                                         // vert positions
float *lvn0, *lvn1;                                         // vert normals
float *lfp0, *lfp1;                                         // face positions
float *lfn0, *lfn1;                                         // face normals
int mnlifv0, mnlifv1, mnlavf0, mnlavf1, mnlaff0, mnlaff1;   // counts: face verts, vert adj, face adj
int nv0, nv1, nf0, nf1;                                     // geometry counts

bool pairwise_optimization;                                 // precomputed distances and dotted norms?
float **mvd01, **mvn01, **mfd01, **mfn01;                   // matrices of pairwise distances and dotted norms

int **lsiv01, **lsif01;                                     // indices of geometry in sorted order (by distance)
int *nlifv0, **lifv0;                                       // face vert inds
int *nlifv1, **lifv1;
int *nlavf0, **lavf0;                                       // faces adjacent to verts
int *nlavf1, **lavf1;
int *nlaff0, **laff0;                                       // faces adjacent to faces
int *nlaff1, **laff1;

// mesh matches
int *vmatch01;
int *vmatch10;
int *fmatch01;
int *fmatch10;

// costs
float cost_vdel, cost_vadd, cost_vdist, cost_vnorm;
float cost_fdel, cost_fadd, cost_fvdist, cost_fvunk, cost_fvmis, cost_fnorm, cost_fmism, cost_fmunk;
int cost_k, cost_maxiters;

int knn = 200;

float v_dist( int iv0, int iv1 )
{
    if( pairwise_optimization ) return mvd01[iv0][iv1];
    
    float *vp0 = &lvp0[iv0*3];
    float *vp1 = &lvp1[iv1*3];
    return sqrt(
        ( vp0[0] - vp1[0] ) * ( vp0[0] - vp1[0] ) +
        ( vp0[1] - vp1[1] ) * ( vp0[1] - vp1[1] ) +
        ( vp0[2] - vp1[2] ) * ( vp0[2] - vp1[2] )
    );
}
float v_norm( int iv0, int iv1 )
{
    if( pairwise_optimization ) return mvn01[iv0][iv1];
    
    float *vn0 = &lvn0[iv0*3];
    float *vn1 = &lvn1[iv1*3];
    return ( 1.0 - ( vn0[0] * vn1[0] + vn0[1] * vn1[1] + vn0[2] * vn1[2] ) ); // /2.0
}

float f_dist( int if0, int if1 )
{
    if( pairwise_optimization ) return mfd01[if0][if1];
    
    float *fp0 = &lfp0[if0*3];
    float *fp1 = &lfp1[if1*3];
    return sqrt( (fp0[0]-fp1[0])*(fp0[0]-fp1[0]) + (fp0[1]-fp1[1])*(fp0[1]-fp1[1]) + (fp0[2]-fp1[2])*(fp0[2]-fp1[2]) );
}
float f_norm( int if0, int if1 )
{
    if( pairwise_optimization ) return mfn01[if0][if1];
    
    float *fn0 = &lfn0[if0*3];
    float *fn1 = &lfn1[if1*3];
    return ( 1.0 - (fn0[0]*fn1[0] + fn0[1]*fn1[1] + fn0[2]*fn1[2]) ); // / 2.0;
}

// modified from: http://www.algolist.net/Algorithms/Sorting/Quicksort
void quickSort( float *vals, int *inds, int left, int right )
{
    int i = left, j = right;
    int tmp;
    int pivot = inds[(left + right) / 2];
    float pivot_val = vals[pivot];
    
    /* partition */
    while( i <= j )
    {
        while( vals[inds[i]] < pivot_val ) i++;
        while( vals[inds[j]] > pivot_val ) j--;
        if (i <= j)
        {
            tmp = inds[i];
            inds[i] = inds[j];
            inds[j] = tmp;
            i++;
            j--;
        }
    };
    
    /* recursion */
    if( left < j )  quickSort( vals, inds, left, j );
    if( i < right ) quickSort( vals, inds, i, right );
}

void shuffle( int *inds, int c )
{
    srand( time(0) );
    for( int i1 = 0; i1 < c; i1++ ) {
        int i2 = rand() % c;
        int t = inds[i1];
        inds[i1] = inds[i2];
        inds[i2] = t;
    }
}



#define CHECK_MARKER(expect)    \
    fscanf(fp,"%s",s); \
    marker.assign(s); \
    if( marker.compare(expect) ) { \
        printf( "error loading: \"%s\" != \"%s\"\n", expect, marker.data() ); \
        exit(1); \
    }

void load_mesh_data( const char *fname )
{
    // terribly inefficient loading, but ok since this is only loaded once
    
    int *t;
    char s[80];
    string marker( 80, 0 );
    
    FILE *fp = fopen( fname, "rt" );
    
    // load cost parameters
    CHECK_MARKER( "costs" );
    fscanf( fp, "%f %f", &cost_vdel, &cost_vadd );
    fscanf( fp, "%f %f", &cost_vdist, &cost_vnorm );
    fscanf( fp, "%f %f", &cost_fdel, &cost_fadd );
    fscanf( fp, "%f %f %f", &cost_fvdist, &cost_fvunk, &cost_fvmis );
    fscanf( fp, "%f %f %f", &cost_fnorm, &cost_fmism, &cost_fmunk );
    fscanf( fp, "%d %d", &cost_k, &cost_maxiters );
    
    // load vert and face counts
    CHECK_MARKER( "counts" );
    fscanf( fp, "%d %d %d %d", &nv0, &nv1, &nf0, &nf1 );
    fscanf( fp, "%d %d %d %d %d %d", &mnlifv0, &mnlifv1, &mnlavf0, &mnlavf1, &mnlaff0, &mnlaff1 );
    
    printf( "  nv0 = %d, nv1 = %d\n", nv0, nv1 );
    printf( "  nf0 = %d, nf1 = %d\n", nf0, nf1 );
    
    lvp0 = (float*)malloc( nv0 * 3 * sizeof(float) );
    lvn0 = (float*)malloc( nv0 * 3 * sizeof(float) );
    lvp1 = (float*)malloc( nv1 * 3 * sizeof(float) );
    lvn1 = (float*)malloc( nv1 * 3 * sizeof(float) );
    
    lfp0 = (float*)malloc( nf0 * 3 * sizeof(float) );
    lfn0 = (float*)malloc( nf0 * 3 * sizeof(float) );
    lfp1 = (float*)malloc( nf1 * 3 * sizeof(float) );
    lfn1 = (float*)malloc( nf1 * 3 * sizeof(float) );
    
    nlifv0 = (int*)malloc( nf0 * sizeof(int) ); lifv0 = (int**)malloc( nf0 * sizeof(int*) );
    nlifv1 = (int*)malloc( nf1 * sizeof(int) ); lifv1 = (int**)malloc( nf1 * sizeof(int*) );
    nlavf0 = (int*)malloc( nv0 * sizeof(int) ); lavf0 = (int**)malloc( nv0 * sizeof(int*) );
    nlavf1 = (int*)malloc( nv1 * sizeof(int) ); lavf1 = (int**)malloc( nv1 * sizeof(int*) );
    nlaff0 = (int*)malloc( nf0 * sizeof(int) ); laff0 = (int**)malloc( nf0 * sizeof(int*) );
    nlaff1 = (int*)malloc( nf1 * sizeof(int) ); laff1 = (int**)malloc( nf1 * sizeof(int*) );
    
    CHECK_MARKER( "verts0" );
    for( int i = 0; i < nv0; i++ )
        fscanf( fp, "%f %f %f %f %f %f", &lvp0[i*3+0], &lvp0[i*3+1], &lvp0[i*3+2], &lvn0[i*3+0], &lvn0[i*3+1], &lvn0[i*3+2] );
    CHECK_MARKER( "verts1" );
    for( int i = 0; i < nv1; i++ )
        fscanf( fp, "%f %f %f %f %f %f", &lvp1[i*3+0], &lvp1[i*3+1], &lvp1[i*3+2], &lvn1[i*3+0], &lvn1[i*3+1], &lvn1[i*3+2] );
    CHECK_MARKER( "faces0" );
    for( int i = 0; i < nf0; i++ )
        fscanf( fp, "%f %f %f %f %f %f", &lfp0[i*3+0], &lfp0[i*3+1], &lfp0[i*3+2], &lfn0[i*3+0], &lfn0[i*3+1], &lfn0[i*3+2] );
    CHECK_MARKER( "faces1" );
    for( int i = 0; i < nf1; i++ )
        fscanf( fp, "%f %f %f %f %f %f", &lfp1[i*3+0], &lfp1[i*3+1], &lfp1[i*3+2], &lfn1[i*3+0], &lfn1[i*3+1], &lfn1[i*3+2] );
    
    // load face vert indices
    CHECK_MARKER( "face0verts" );
    t = (int*)malloc( nf0 * mnlifv0 * sizeof(int) );
    for( int i = 0; i < nf0; i++ )
    {
        lifv0[i] = &t[ i * mnlifv0 ];
        fscanf( fp, "%i", &nlifv0[i] );
        for( int i_ = 0; i_ < nlifv0[i]; i_++ ) fscanf( fp, "%i", &lifv0[i][i_] );
    }
    CHECK_MARKER( "face1verts" );
    t = (int*)malloc( nf1 * mnlifv1 * sizeof(int) );
    for( int i = 0; i < nf1; i++ )
    {
        lifv1[i] = &t[ i * mnlifv1 ];
        fscanf( fp, "%i", &nlifv1[i] );
        for( int i_ = 0; i_ < nlifv1[i]; i_++ ) fscanf( fp, "%i", &lifv1[i][i_] );
    }
    
    // load vert adj face inds
    CHECK_MARKER( "vert0faces" );
    t = (int*)malloc( nv0 * mnlavf0 * sizeof(int) );
    for( int i = 0; i < nv0; i++ )
    {
        lavf0[i] = &t[ i * mnlavf0 ];
        fscanf( fp, "%i", &nlavf0[i] );
        for( int i_ = 0; i_ < nlavf0[i]; i_++ ) fscanf( fp, "%i", &lavf0[i][i_] );
    }
    CHECK_MARKER( "vert1faces" );
    t = (int*)malloc( nv1 * mnlavf1 * sizeof(int) );
    for( int i = 0; i < nv1; i++ )
    {
        lavf1[i] = &t[ i * mnlavf1 ];
        fscanf( fp, "%i", &nlavf1[i] );
        for( int i_ = 0; i_ < nlavf1[i]; i_++ ) fscanf( fp, "%i", &lavf1[i][i_] );
    }
    
    // load face adj face inds
    CHECK_MARKER( "face0faces" );
    t = (int*)malloc( nf0 * mnlaff0 * sizeof(int) );
    for( int i = 0; i < nf0; i++ )
    {
        laff0[i] = &t[ i * mnlaff0 ];
        fscanf( fp, "%i", &nlaff0[i] );
        for( int i_ = 0; i_ < nlaff0[i]; i_++ ) fscanf( fp, "%i", &laff0[i][i_] );
    }
    CHECK_MARKER( "face1faces" );
    t = (int*)malloc( nf1 * mnlaff1 * sizeof(int) );
    for( int i = 0; i < nf1; i++ )
    {
        laff1[i] = &t[ i * mnlaff1 ];
        fscanf( fp, "%i", &nlaff1[i] );
        for( int i_ = 0; i_ < nlaff1[i]; i_++ ) fscanf( fp, "%i", &laff1[i][i_] );
    }
    
    // load initial matchings
    vmatch01 = (int*)malloc( nv0 * sizeof(int) );
    vmatch10 = (int*)malloc( nv1 * sizeof(int) );
    fmatch01 = (int*)malloc( nf0 * sizeof(int) );
    fmatch10 = (int*)malloc( nf1 * sizeof(int) );
    CHECK_MARKER( "vmatch01" );
    for( int iv0 = 0; iv0 < nv0; iv0++ ) fscanf( fp, "%i", &vmatch01[iv0] );
    CHECK_MARKER( "vmatch10" );
    for( int iv1 = 0; iv1 < nv1; iv1++ ) fscanf( fp, "%i", &vmatch10[iv1] );
    CHECK_MARKER( "fmatch01" );
    for( int if0 = 0; if0 < nf0; if0++ ) fscanf( fp, "%i", &fmatch01[if0] );
    CHECK_MARKER( "fmatch10" );
    for( int if1 = 0; if1 < nf1; if1++ ) fscanf( fp, "%i", &fmatch10[if1] );
    
    fscanf( fp, "%s", s ); marker.assign(s);
    if( marker.compare("end") != 0 ) { printf( "error loading: end != \"%s\"\n", marker.data() ); exit(1); }
    
    fclose(fp);
    
    printf( "loaded!\n" );
    
    knn = nv0 < knn ? nv0 : knn;
    knn = nv1 < knn ? nv1 : knn;
    knn = nf0 < knn ? nf0 : knn;
    knn = nf1 < knn ? nf1 : knn;
    
    
    pairwise_optimization = ( nv0+nv1 < 12000 && nf0+nf1 < 12000 );
    if( pairwise_optimization )
    {
        printf( "precomputing pairwise distances and dotted normals...\n" );
        // compute vertex distance matrix and dotted-normal matrix
        float *vd = (float*)malloc( nv0 * nv1 * sizeof(float) );
        float *vn = (float*)malloc( nv0 * nv1 * sizeof(float) );
        mvd01 = (float**)malloc( nv0 * sizeof(float*) );
        mvn01 = (float**)malloc( nv0 * sizeof(float*) );
        for( int iv0 = 0; iv0 < nv0; iv0++ )
        {
            mvd01[iv0] = &vd[iv0*nv1]; // (float*)malloc( nv1 * sizeof(float) );
            mvn01[iv0] = &vn[iv0*nv1]; // (float*)malloc( nv1 * sizeof(float) );
            float *vp0 = &lvp0[iv0*3];
            float *vn0 = &lvn0[iv0*3];
            for( int iv1 = 0; iv1 < nv1; iv1++ )
            {
                float *vp1 = &lvp1[iv1*3];
                float *vn1 = &lvn1[iv1*3];
                mvd01[iv0][iv1] = sqrt(
                    ( vp0[0] - vp1[0] ) * ( vp0[0] - vp1[0] ) +
                    ( vp0[1] - vp1[1] ) * ( vp0[1] - vp1[1] ) +
                    ( vp0[2] - vp1[2] ) * ( vp0[2] - vp1[2] )
                );
                mvn01[iv0][iv1] = ( 1.0 - ( vn0[0] * vn1[0] + vn0[1] * vn1[1] + vn0[2] * vn1[2] ) ); // / 2.0;
            }
        }
        
        // compute face "distance" matrix and dotted-normal matrix
        float *fd = (float*)malloc( nf0 * nf1 * sizeof(float) );
        float *fn = (float*)malloc( nf0 * nf1 * sizeof(float) );
        mfd01 = (float**)malloc( nf0 * sizeof(float*) );
        mfn01 = (float**)malloc( nf0 * sizeof(float*) );
        for( int if0 = 0; if0 < nf0; if0++ )
        {
            mfd01[if0] = &fd[if0*nf1]; // (float*)malloc( nf1 * sizeof(float) );
            mfn01[if0] = &fn[if0*nf1]; // (float*)malloc( nf1 * sizeof(float) );
            float *fp0 = &lfp0[if0*3];
            float *fn0 = &lfn0[if0*3];
            for( int if1 = 0; if1 < nf1; if1++ )
            {
                float *fp1 = &lfp1[if1*3];
                float *fn1 = &lfn1[if1*3];
                mfd01[if0][if1] = sqrt( (fp0[0]-fp1[0])*(fp0[0]-fp1[0]) + (fp0[1]-fp1[1])*(fp0[1]-fp1[1]) + (fp0[2]-fp1[2])*(fp0[2]-fp1[2]) );
                mfn01[if0][if1] = ( 1.0 - (fn0[0]*fn1[0] + fn0[1]*fn1[1] + fn0[2]*fn1[2]) ); // / 2.0;
            }
        }
        printf( "  done!\n" );
    }
    
    
    printf( "computing sorted list of indices...\n" );
    printf( "  verts...\n" );
    // compute list of inds in sorted order (closest first)
    int *vi = (int*)malloc( nv0 * knn * sizeof(int) );
    lsiv01 = (int**)malloc( nv0 * sizeof(int*) );
    
    float *vdists = (float*)malloc( nv1 * sizeof(float) );
    int *vinds = (int*)malloc( nv1 * sizeof(int) );
    
    for( int iv0 = 0; iv0 < nv0; iv0++ )
    {
        if(vmatch01[iv0] != -1) continue;
        //printf( "%03d%%\b\b\b\b", (int)(100.0f * (float)iv0 / (float)nv0) );
        fflush(stdout);
        
        #pragma omp parallel for default(none) \
            private(iv1) \
            shared(nv1,iv0,vdists,vinds)
        for( int iv1 = 0; iv1 < nv1; iv1++ )
        {
            vdists[iv1] = v_dist( iv0, iv1 );
            vinds[iv1] = iv1;
        }
        shuffle( vinds, nv1 );
        quickSort( vdists, vinds, 0, nv1-1 );
        lsiv01[iv0] = &vi[iv0*knn];
        for( int i = 0; i < knn; i++ ) lsiv01[iv0][i] = vinds[i];
    }
    
    printf( "  faces...\n" );
    int *fi = (int*)malloc( nf0 * knn * sizeof(int) );
    lsif01 = (int**)malloc( nf0 * sizeof(int*) );
    
    float *fdists = (float*)malloc( nf1 * sizeof(float) );
    int *finds = (int*)malloc( nf1 * sizeof(int) );
    
    for( int if0 = 0; if0 < nf0; if0++ )
    {
        if(fmatch01[if0] != -1) continue;
        //printf( "%03d%%\b\b\b\b", (int)(100.0f * (float)if0 / (float)nf0) );
        fflush(stdout);
        
        #pragma omp parallel for default(none) \
            private(if1) \
            shared(nf1,if0,fdists,finds)
        for( int if1 = 0; if1 < nf1; if1++ )
        {
            fdists[if1] = f_dist( if0, if1 );
            finds[if1] = if1;
        }
        shuffle( finds, nf1 );
        quickSort( fdists, finds, 0, nf1-1 );
        lsif01[if0] = &fi[if0*knn];
        for( int i = 0; i < knn; i++ ) lsif01[if0][i] = finds[i];
    }
    printf( "  done!\n" );
    
    // NOTE: not worrying about freeing vert and face data...
}

void save_matching_data( const char *fname )
{
    FILE *fp = fopen( fname, "wt" );
    
    for( int iv0 = 0; iv0 < nv0; iv0++ )
        fprintf( fp, "%i\n", vmatch01[iv0] );
    for( int if0 = 0; if0 < nf0; if0++ )
        fprintf( fp, "%i\n", fmatch01[if0] );
    
    fclose(fp);
}


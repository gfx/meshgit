#include <iostream>
#include <vector>
#include <set>
#include <math.h>
#include <memory>
#include <string>

#include "mesh.h"


using namespace std;


void build_correspondence_greedy_closestverts()
{
    float *v01dist;
    int *v01i;
    
    v01dist = (float*)malloc( nv0 * nv1 * sizeof(float) );
    v01i = (int*)malloc( nv0 * nv1 * sizeof(int) );
    
    for( int i = 0; i < nv0 * nv1; i++ )
    {
        int iv0 = i / nv1;
        int iv1 = i % nv1;
        
        v01i[i] = i;
        
        v01dist[i] = cost_vdist * v_dist(iv0,iv1) + cost_vnorm * v_norm(iv0,iv1);
    }
    
    quickSort( v01dist, v01i, 0, nv0 * nv1 - 1 );
    
    for( int i_ = 0; i_ < nv0 * nv1; i_++ )
    {
        int i = v01i[i_];
        if( v01dist[i] >= 10.0 ) break;
        int iv0 = i / nv1;
        int iv1 = i % nv1;
        
        if( vmatch01[iv0] != -1 || vmatch10[iv1] != -1 ) continue;
        
        vmatch01[iv0] = iv1;
        vmatch10[iv1] = iv0;
    }
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main( int argc, char *argv )
{
    printf( "loading...\n" );
    load_mesh_data( "greedy_input.txt" );
    
    printf( "building...\n" );
    build_correspondence_greedy_closestverts();
    
    printf( "saving...\n" );
    save_matching_data( "greedy_output.txt" );
    
    return 0;
}













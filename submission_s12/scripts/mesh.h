#ifndef MESH
#define MESH

// mesh data
extern int nv0, nv1, nf0, nf1;                         // geometry counts
extern int mnlifv0, mnlifv1, mnlavf0, mnlavf1, mnlaff0, mnlaff1;   // counts: face verts, vert adj, face adj
extern bool pairwise_optimization;                     // precomputed distances and dotted norms?
extern float **mvd01, **mvn01, **mfd01, **mfn01;       // matrices of pairwise distances and dotted norms
extern int **lsiv01, **lsif01;                         // indices of geometry in sorted order (by distance)
extern int *nlifv0, **lifv0;                           // face vert inds
extern int *nlifv1, **lifv1;
extern int *nlavf0, **lavf0;                           // faces adjacent to verts
extern int *nlavf1, **lavf1;
extern int *nlaff0, **laff0;                           // faces adjacent to faces
extern int *nlaff1, **laff1;

// mesh matches
extern int *vmatch01;
extern int *vmatch10;
extern int *fmatch01;
extern int *fmatch10;

// costs
extern float cost_vdel, cost_vadd, cost_vdist, cost_vnorm;
extern float cost_fdel, cost_fadd, cost_fvdist, cost_fvunk, cost_fvmis, cost_fnorm, cost_fmism, cost_fmunk;
extern int cost_k, cost_maxiters;

extern int knn;


float v_dist( int iv0, int iv1 );
float v_norm( int iv0, int iv1 );
float f_dist( int iv0, int iv1 );
float f_norm( int iv0, int iv1 );

void quickSort( float *vals, int *inds, int left, int right );

void load_mesh_data( const char *fname );

void save_matching_data( const char *fname );



#endif

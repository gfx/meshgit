#!/usr/bin/env python -B
# -*- coding: utf-8 -*-

"""
selectively compiles the steps of sequence into a single near .json file
"""

import os, glob, sys, subprocess, time, select, json, re, array, math
import config, miscfuncs, scenefuncs


print "Compiling snapshots..."

near = {}

for l in open( "steps_processed.txt", "r" ):
    num = l.split(' ')[0]
    jsonfn = "step%s.json" % num
    json = miscfuncs.file_to_json( jsonfn )
    scenefuncs.add_to_scene( near, json )

miscfuncs.json_to_file( "steps_processed.json", near )


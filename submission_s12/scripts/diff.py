import os, re, json
from mesh import Mesh
import meshgit2

fn0 = '/Users/jdenning/Projects/meshgit/models/toy-cube/cube.ply'
fn1 = '/Users/jdenning/Projects/meshgit/models/toy-cube/cube+fextrude2.ply'
fn2 = '/Users/jdenning/Projects/meshgit/models/toy-cube/test2.ply'
#fn0 = '/Users/jdenning/Projects/meshgit/models/sintel/sintel00.ply'
#fn1 = '/Users/jdenning/Projects/meshgit/models/sintel/sintel01.ply'
#fn2 = '/Users/jdenning/Projects/meshgit/models/sintel/sintel0001.ply'

m0,m1 = Mesh.fromPLY( fn0 ), Mesh.fromPLY( fn1 )

md = meshgit2.diff_meshes( m0, m1 )
mr = md['del']
ma = md['add']
mc = md['chg']

m2 = Mesh()
m2.append( m0.copy(), vt=[-5,0,0] )
m2.append( m1.copy(), vt=[5,0,0] )
m2.append( mc.copy(), vt=[0,0,0] )
m2.append( mr.copy(), vt=[0,-5,0] )
m2.append( ma.copy(), vt=[0,5,0] )

m2.toPLY( fn2 )


print( "bpy.ops.object.select_all(action='SELECT'); bpy.ops.object.delete(); bpy.ops.import_mesh.ply(filepath='%s')" % fn2 )
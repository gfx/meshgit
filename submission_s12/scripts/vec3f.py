import math

class Vec3f(object):
    def __init__( self, x, y, z ):
        self.x,self.y,self.z = x,y,z
    
    def set_xyz( self, x, y, z ):
        self.x,self.y,self.z = x,y,z
    
    def set_t( self, v ):
        self.x,self.y,self.z = v
    
    @classmethod
    def t_length( cls, u ):
        x,y,z = u
        return math.sqrt( x*x+y*y+z*z )
    
    def length( self ):
        return math.sqrt( self.x*self.x + self.y*self.y + self.z*self.z )
    
    @classmethod
    def t_distance( cls, u, v ):
        #ux,uy,uz = u
        #vx,vy,vz = v
        #return math.sqrt( (ux-vx)*(ux-vx)+(uy-vy)*(uy-vy)+(uz-vz)*(uz-vz) )
        return math.sqrt( (u[0]-v[0])**2 + (u[1]-v[1])**2 + (u[2]-v[2])**2 )
    @classmethod
    def t_distance2( cls, u, v ):
        dx,dy,dz = u[0]-v[0],u[1]-v[1],u[2]-v[2]
        return dx*dx+dy*dy+dz*dz
    
    @classmethod
    def t_knn( cls, k, u, lv ):
        k = min( k, len(lv) )
        lv_ = [ (i,Vec3f.t_distance2( u, v )) for i,v in enumerate(lv) ]
        lv_ = sorted( lv_, key=lambda x:x[1] )
        return [ (i,lv[i]) for i,_ in lv_[:k] ]
    
    @classmethod
    def t_add( cls, u, v ):
        ux,uy,uz = u
        vx,vy,vz = v
        return ( ux+vx, uy+vy, uz+vz )
    @classmethod
    def v_add( cls, u, v ):
        return Vec3f( u.x+v.x, u.y+v.y, u.z+v.z )
    @classmethod
    def t_add_between( cls, u, v, t ):
        tu,tv = 1 - t,t
        return ( u[0]*tu+v[0]*tv,u[1]*tu+v[1]*tv,u[2]*tu+v[2]*tv )
    def add_v( self, v ):
        self.x += v.x
        self.y += v.y
        self.z += v.z
        return self
    def add_t( self, v ):
        vx,vy,vz = v
        self.x += vx
        self.y += vy
        self.z += vz
        return self
    
    @classmethod
    def t_interp( cls, u, v, s ):
        ux,uy,uz = u
        vx,vy,vz = v
        s1 = 1.0 - s
        return ( ux*s1+vx*s, uy*s1+vy*s, uz*s1+vz*s )
    
    @classmethod
    def t_sub( cls, u, v ):
        ux,uy,uz = u
        vx,vy,vz = v
        return ( ux-vx, uy-vy, uz-vz )
    @classmethod
    def v_sub( cls, u, v ):
        return Vec3f( u.x-v.x, u.y-v.y, u.z-v.z )
    def sub_v( self, v ):
        self.x -= v.x
        self.y -= v.y
        self.z -= v.z
        return self
    def sub_t( self, v ):
        vx,vy,vz = v
        self.x -= vx
        self.y -= vy
        self.z -= vz
        return self
    
    @classmethod
    def t_scale( cls, u, s ):
        x,y,z = u
        return (x*s,y*s,z*s)
    @classmethod
    def v_scale( cls, u, s ):
        return Vec3f( u.x*s,u.y*s,u.z*s )
    def scale( self, s ):
        self.x *= s
        self.y *= s
        self.z *= s
        return self
    
    @classmethod
    def t_average( cls, lu ):
        ax,ay,az = 0.0,0.0,0.0
        c = float(len(lu))
        for u in lu:
            ax += u[0]
            ay += u[1]
            az += u[2]
        return ( ax/c, ay/c, az/c )
    
    @classmethod
    def t_negate( cls, u ):
        x,y,z = u
        return ( -x, -y, -z )
    @classmethod
    def v_negate( cls, u ):
        return Vec3f( -u.x, -u.y, -u.z )
    def negate( self ):
        self.x = -self.x
        self.y = -self.y
        self.z = -self.z
        return self
    
    @classmethod
    def t_cross( cls, u, v ):
        ux,uy,uz = u
        vx,vy,vz = v
        return ( uy*vz-uz*vy, uz*vx-ux*vz, ux*vy-uy*vx )
    @classmethod
    def v_cross( cls, u, v ):
        return Vec3f( u.y*v.z-u.z*v.y, u.z*v.x-u.x*v.z, u.x*v.y-u.y*v.x )
    
    @classmethod
    def t_dot( cls, u, v ):
        ux,uy,uz = u
        vx,vy,vz = v
        return ux*vx + uy*vy + uz*vz
    @classmethod
    def v_dot( cls, u, v ):
        return u.x*v.x + u.y*v.y + u.z*v.z
    
    @classmethod
    def t_norm( cls, u ):
        x,y,z = u
        l = math.sqrt(x*x+y*y+z*z)
        if l == 0.0: return (0.0,0.0,0.0)
        return ( x/l, y/l, z/l )
    @classmethod
    def v_norm( cls, u ):
        l = math.sqrt(u.x*u.x+u.y*u.y+u.z*u.z)
        return Vec3f( u.x/l, u.y/l, u.z/l )
    def norm( self ):
        l = math.sqrt(self.x*self.x+self.y*self.y+self.z*self.z)
        self.x /= l
        self.y /= l
        self.z /= l
        return self
    
    
    @classmethod
    def t_project_to_triangle( cls, u, v0, v1, v2 ):
        pass

#include <iostream>
#include <vector>
#include <set>
#include <math.h>
#include <memory>
#include <string>

#include "mesh.h"


using namespace std;



bool is_in( int *list, int size, int find )
{
    for( int i = 0; i < size; i++ ) if( list[i] == find ) return true;
    return false;
}


float compute_cost( const vector<int>& liv0, const vector<int>& lif0, int *vmatch01_, int *vmatch10_, int *fmatch01_, int *fmatch10_ )
{
    float cost = 0.0;
    
    for( int i = 0; i < liv0.size(); i++ )
    {
        int iv0 = liv0[i];
        int iv1 = vmatch01_[iv0];
        
        // assuming that vert matching is consistent!
        if( iv1 == -1 ) { cost += cost_vdel + cost_vadd; continue; } // unmatched vertex
        
        // calc cost of substitution
        cost += cost_vdist * v_dist( iv0, iv1 ); // mvd01[iv0][iv1];
        cost += cost_vnorm * v_norm( iv0, iv1 ); // mvn01[iv0][iv1];
    }
    
    for( int i = 0; i < lif0.size(); i++ )
    {
        int if0 = lif0[i];
        int if1 = fmatch01_[if0];
        int *ifv0 = lifv0[if0], nifv0 = nlifv0[if0];
        int *aff0 = laff0[if0], naff0 = nlaff0[if0];
        int *ifv1, nifv1;
        int *aff1, naff1;
        
        float fdist = 0.0, fnorm = 0.0;
        int svunk = 0, svmis = 0;
        int smunk = 0, smism = 0;
        
        if( if1 == -1 )  // unmatched face
        {
            /*for( int i = 0; i < nifv0; i++ )
            {
                int iv0 = ifv0[i];
                int iv1 = vmatch01_[iv0];
                if( iv1 == -1 ) svunk++;
                else svmis++;
            }
            
            for( int i = 0; i < naff0; i++ )
            {
                int if0 = aff0[i];
                int if1 = fmatch01_[if0];
                if( if1 == -1 ) smunk++;
                else smism++;
            }*/
            
            cost += cost_fdel + cost_fadd;
            continue;
            
            
        } else {
            ifv1 = lifv1[if1];
            nifv1 = nlifv1[if1];
            aff1 = laff1[if1];
            naff1 = nlaff1[if1];
            
            fdist = f_dist( if0, if1 ); //0.0;
            fnorm = f_norm( if0, if1 );
            
            for( int i = 0; i < nifv0; i++ )
            {
                int iv0 = ifv0[i];
                int iv1 = vmatch01_[iv0];
                if( iv1 == -1 ) svunk++;
                else if( !is_in( ifv1, nifv1, iv1 ) ) svmis++;
                //vdist += v_dist( iv0, iv1 );
            }
            if( 0 ) {
                for( int i = 0; i < nifv1; i++ )
                {
                    int iv1 = ifv1[i];
                    int iv0 = vmatch10_[iv1];
                    if( iv0 == -1 ) svunk++;
                    else if( !is_in( ifv0, nifv0, iv0 ) ) svmis++;
                    //vdist += mvd01[iv0][iv1]; // already counted above
                }
            }
            for( int i = 0; i < naff0; i++ )
            {
                int if0 = aff0[i];
                int if1 = fmatch01_[if0];
                if( if1 == -1 ) smunk++;
                else if( !is_in( aff1, naff1, if1 ) ) smism++;
            }
            if( 0 ) {
                for( int i = 0; i < naff1; i++ )
                {
                    int if1 = aff1[i];
                    int if0 = fmatch10_[if1];
                    if( if0 == -1 ) smunk++;
                    else if( !is_in( aff0, naff0, if0 ) ) smism++;
                }
            }
        }
        
        
        cost += cost_fvdist * fdist;// * nifv0;
        cost += cost_fnorm * fnorm;
        
        cost += cost_fvunk * (float)svunk;
        cost += cost_fvmis * (float)svmis;
        
        cost += cost_fmunk * (float)smunk;
        cost += cost_fmism * (float)smism;
    }
    //printf( "3\n" );
    
    return cost;
}

float compute_cost_after_v_sub( int iv0, int iv1, vector<int>& liv0, vector<int>& lif0, int *vmatch01_, int *vmatch10_, int *fmatch01_, int *fmatch10_ )
{
    float cost_delta = 0.0;
    
    liv0.clear();
    lif0.clear();
    
    int *avf0 = lavf0[iv0], navf0 = nlavf0[iv0];
    int *avf1 = lavf1[iv1], navf1 = nlavf1[iv1];
    
    // build list of affected verts and faces from sub'ing iv0->iv1
    liv0.push_back( iv0 );
    for( int i = 0; i < navf0; i++ )
        lif0.push_back( avf0[i] );
    // add on any faces adj to v1 that map back to m0!!!!!
    for( int i = 0; i < navf1; i++ )
    {
        int if0 = fmatch10_[avf1[i]];
        if( if0 != -1 ) lif0.push_back( if0 );
    }
    
    cost_delta -= compute_cost( liv0, lif0, vmatch01_, vmatch10_, fmatch01_, fmatch10_ );
    vmatch01_[iv0] = iv1; vmatch10_[iv1] = iv0;
    cost_delta += compute_cost( liv0, lif0, vmatch01_, vmatch10_, fmatch01_, fmatch10_ );
    vmatch01_[iv0] = vmatch10_[iv1] = -1;
    
    return cost_delta;
}

float compute_cost_after_f_sub( int if0, int if1, vector<int>& liv0, vector<int>& lif0, int *vmatch01_, int *vmatch10_, int *fmatch01_, int *fmatch10_ )
{
    float cost_delta = 0.0;
    
    liv0.clear();
    lif0.clear();
    
    int *ifv0 = lifv0[if0], nifv0 = nlifv0[if0];
    int *ifv1 = lifv1[if1], nifv1 = nlifv1[if1];
    int *aff0 = laff0[if0], naff0 = nlaff0[if0];
    int *aff1 = laff1[if1], naff1 = nlaff1[if1];
    
    // build list of affected verts and faces from sub'ing if0->if1
    for( int i = 0; i < nifv0; i++ )
        liv0.push_back( ifv0[i] );
    // add any vertex in m0 that maps to a vertex in m1 which is adj to if1
    for( int i = 0; i < nifv1; i++ )
    {
        int iv0 = vmatch10_[ifv1[i]];
        if( iv0 != -1 ) liv0.push_back( iv0 );
    }
    lif0.push_back( if0 );
    for( int i = 0; i < naff0; i++ )
        lif0.push_back( aff0[i] );
    // add any face in m0 that maps to a face in m1 which is adj to if1
    for( int i = 0; i < naff1; i++ )
    {
        int if0_ = fmatch10_[aff1[i]];
        if( if0_ != -1 ) lif0.push_back( if0_ );
    }
    
    cost_delta -= compute_cost( liv0, lif0, vmatch01_, vmatch10_, fmatch01_, fmatch10_ );
    fmatch01_[if0] = if1; fmatch10_[if1] = if0;
    cost_delta += compute_cost( liv0, lif0, vmatch01_, vmatch10_, fmatch01_, fmatch10_ );
    fmatch01_[if0] = fmatch10_[if1] = -1;
    
    return cost_delta;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

float build_correspondence_greedy()
{
    char spin_text[] = "|||///---\\\\\\"; int cspins = 12, ispin = 0;
    float cost_init = 0.0;
    float cost_cur;
    int nsv0 = 0, nsf0 = 0, unmatched = 0;
    
    int k = cost_k;
    
    vector<int> liv0(nv0);
    vector<int> lif0(nf0);
    
    int jobs = 50; //20; if( nv0 + nf0 < 200 ) jobs = 1;
    
    int *vmatch01_, *vmatch10_, *fmatch01_, *fmatch10_;
    
    int **lvmatch01_ = (int**)malloc( jobs * sizeof(int*) );
    int **lvmatch10_ = (int**)malloc( jobs * sizeof(int*) );
    int **lfmatch01_ = (int**)malloc( jobs * sizeof(int*) );
    int **lfmatch10_ = (int**)malloc( jobs * sizeof(int*) );
    int *lbest_action = (int*)malloc( jobs * sizeof(int) );
    int *lbest_i0 = (int*)malloc( jobs * sizeof(int) );
    int *lbest_i1 = (int*)malloc( jobs * sizeof(int) );
    float *lbest_delta = (float*)malloc( jobs * sizeof(float) );
    for( int j = 0; j < jobs; j++ )
    {
        lvmatch01_[j] = (int*)malloc( nv0 * sizeof(int) ); memcpy( lvmatch01_[j], vmatch01, nv0 * sizeof(int) );
        lvmatch10_[j] = (int*)malloc( nv1 * sizeof(int) ); memcpy( lvmatch10_[j], vmatch10, nv1 * sizeof(int) );
        lfmatch01_[j] = (int*)malloc( nf0 * sizeof(int) ); memcpy( lfmatch01_[j], fmatch01, nf0 * sizeof(int) );
        lfmatch10_[j] = (int*)malloc( nf1 * sizeof(int) ); memcpy( lfmatch10_[j], fmatch10, nf1 * sizeof(int) );
    }
    
    for( int iv0 = 0; iv0 < nv0; iv0++ )
    {
        liv0.push_back( iv0 );
        if( vmatch01[iv0] == -1 ) nsv0++;
    }
    for( int if0 = 0; if0 < nf0; if0++ )
    {
        lif0.push_back( if0 );
        if( fmatch01[if0] == -1 ) nsf0++;
    }
    
    unmatched = nsv0 + nsf0;
    if( cost_maxiters != -1 && unmatched > cost_maxiters ) unmatched = cost_maxiters;
    
    cost_init = compute_cost( liv0, lif0, vmatch01, vmatch10, fmatch01, fmatch10 );
    
    liv0.resize(10);
    lif0.resize(10);
    
    printf( "  init cost = %f\n", cost_init );
    printf( "  nsv0 = %d\n", nsv0 );
    printf( "  nsf0 = %d\n", nsf0 );
    
    cost_cur = cost_init;
    
    int *sv0 = (int*)malloc( nsv0 * sizeof(int) );
    int *sf0 = (int*)malloc( nsf0 * sizeof(int) );
    for( int iv0 = 0, i = 0; iv0 < nv0; iv0++ ) if( vmatch01[iv0] == -1 ) sv0[i++] = iv0;
    for( int if0 = 0, i = 0; if0 < nf0; if0++ ) if( fmatch01[if0] == -1 ) sf0[i++] = if0;
    
    FILE *fp = fopen( "heartbeat", "wt" );
    
    printf( "  running...\n" );
    for( int run = 0; run < unmatched; run++ )
    {
        ispin = (ispin+1) % cspins;
        //printf( "%03d%%%c\b\b\b\b\b", (int)(100.0f * (float)run / (float)unmatched), spin_text[ispin] );
        //fflush(stdout);
        
        // this heart may beat too slowly if we have a lot of data! :(
        // heartbeat is in case openMP busts
        fseek( fp, 0, SEEK_SET );
        fprintf( fp, "running\n%d\n", run );
        
        // initialize bests
        for( int j = 0; j < jobs; j++ )
        {
            lbest_action[j] = 0;
            lbest_i0[j] = -1;
            lbest_i1[j] = -1;
            lbest_delta[j] = 0.0;
        }
        
        #pragma omp parallel for default(none) \
            private(j) \
            shared(nsv0,sv0,nv1,k,lsiv01,lbest_delta,lbest_action,lbest_i0,lbest_i1,jobs,lvmatch01_,lvmatch10_,lfmatch01_,lfmatch10_,knn)
        for( int j = 0; j < jobs; j++ )
        {
            vector<int> liv0(10);
            vector<int> lif0(10);
            int *vmatch01_ = lvmatch01_[j];
            int *vmatch10_ = lvmatch10_[j];
            int *fmatch01_ = lfmatch01_[j];
            int *fmatch10_ = lfmatch10_[j];
            int sis = (int)((float)nsv0 * (float)j / (float)jobs );
            int sie = (int)((float)nsv0 * (float)(j+1) / (float)jobs );
            for( int si = sis; si < nsv0 && si < sie; si++ )
            {
                int iv0 = sv0[si];
                int c = 0;
                for( int i = 0; i < knn; i++ )
                {
                    int iv1 = lsiv01[iv0][i];
                    if( vmatch10_[iv1] != -1 ) continue;
                    if( i != c ) lsiv01[iv0][c] = lsiv01[iv0][i];
                    
                    float delta = compute_cost_after_v_sub( iv0, iv1, liv0, lif0, vmatch01_, vmatch10_, fmatch01_, fmatch10_ );
                    if( delta < lbest_delta[j] || lbest_action[j] == 0 )
                    {
                        lbest_delta[j] = delta;
                        lbest_action[j] = 1;
                        lbest_i0[j] = iv0;
                        lbest_i1[j] = iv1;
                    }
                    if( ++c == k ) break;
                }
            }
        }
        
        #pragma omp parallel for default(none) \
            private(j) \
            shared(nsf0,sf0,nf1,k,lsif01,lbest_delta,lbest_action,lbest_i0,lbest_i1,jobs,lvmatch01_,lvmatch10_,lfmatch01_,lfmatch10_,knn)
        for( int j = 0; j < jobs; j++ )
        {
            vector<int> liv0(10);
            vector<int> lif0(10);
            int *vmatch01_ = lvmatch01_[j];
            int *vmatch10_ = lvmatch10_[j];
            int *fmatch01_ = lfmatch01_[j];
            int *fmatch10_ = lfmatch10_[j];
            int sis = (int)((float)nsf0 * (float)j / (float)jobs );
            int sie = (int)((float)nsf0 * (float)(j+1) / (float)jobs );
            for( int si = sis; si < nsf0 && si < sie; si++ )
            {
                int if0 = sf0[si];
                int c = 0;
                for( int i = 0; i < knn; i++ )
                {
                    int if1 = lsif01[if0][i];
                    if( fmatch10_[if1] != -1 ) continue;
                    if( i != c ) lsif01[if0][c] = lsif01[if0][i];
                    
                    float delta = compute_cost_after_f_sub( if0, if1, liv0, lif0, vmatch01_, vmatch10_, fmatch01_, fmatch10_ );
                    if( delta < lbest_delta[j] || lbest_action[j] == 0 )
                    {
                        //printf( "%d ", if0 );
                        lbest_delta[j] = delta;
                        lbest_action[j] = 2;
                        lbest_i0[j] = if0;
                        lbest_i1[j] = if1;
                    }
                    if( ++c == k ) break;
                }
            }
        }
        
        float best_delta = 0.0;
        int best_action = 0;                // 0: nothing, 1: vert, 2: face
        int best_i0 = -1, best_i1 = -1;     // inds to make substitution if best_action in [1,2]
        for( int j = 0; j < jobs; j++ )
        {
            if( lbest_action[j] == 0 ) continue;
            if( lbest_delta[j] >= best_delta ) continue;
            best_delta = lbest_delta[j];
            best_action = lbest_action[j];
            best_i0 = lbest_i0[j];
            best_i1 = lbest_i1[j];
        }
        if( best_action == 0 )
        {
            printf( "    early exit!\n" );
            break;
        }
        
        cost_cur += best_delta;
        switch( best_action ) {
            case 1:
                //printf( "v %d:%d", best_i0, best_i1 );
                vmatch01[best_i0] = best_i1; vmatch10[best_i1] = best_i0;
                for( int j = 0; j < jobs; j++ )
                {
                    lvmatch01_[j][best_i0] = best_i1; lvmatch10_[j][best_i1] = best_i0;
                }
                for( int i = 0; i < nsv0; i++ ) if( sv0[i] == best_i0 )
                {
                    sv0[i] = sv0[--nsv0];
                    break;
                }
                break;
            
            case 2:
                //printf( "f %d:%d", best_i0, best_i1 );
                fmatch01[best_i0] = best_i1; fmatch10[best_i1] = best_i0;
                for( int j = 0; j < jobs; j++ )
                {
                    lfmatch01_[j][best_i0] = best_i1; lfmatch10_[j][best_i1] = best_i0;
                }
                for( int i = 0; i < nsf0; i++ ) if( sf0[i] == best_i0 )
                {
                    sf0[i] = sf0[--nsf0];
                    break;
                }
                break;
            
            default:
                printf( "best_action == %d???\n", best_action );
                exit(1);
        }
        
        //printf( ".\n" );
        
    }
    
    fseek( fp, 0, SEEK_SET );
    fprintf( fp, "done!\n" );
    fclose(fp);
    
    printf( "  delta cost = %f\n", cost_cur - cost_init );
    printf( "  nsv0 = %d\n", nsv0 );
    printf( "  nsf0 = %d\n", nsf0 );
    return cost_cur;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int main( int argc, char *argv )
{
    printf( "loading...\n" );
    load_mesh_data( "greedy_input.txt" );
    
    printf( "building...\n" );
    float cost = build_correspondence_greedy();
    printf( "cost = %f\n", cost );
    
    printf( "saving...\n" );
    save_matching_data( "greedy_output.txt" );
    
    return 0;
}













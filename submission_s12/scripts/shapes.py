import time
import pyglet
from pyglet.gl import *

class Shape(object):
    def __init__( self, shapetype, verts, colors, norms=None, show_edges=False ):
        self.setdata( shapetype, verts, colors, norms, show_edges )
    
    def clone( self ):
        t = self.shapetype
        v = list( self.verts )
        c = list( self.colors )
        n = list( self.norms ) if n else None
        if n is None:   return Shape( t, v, c )
        else:           return Shape( t, v, c, n )
    
    def setdata( self, shapetype, verts, colors, norms=None, show_edges=False ):
        self.shapetype = shapetype
        self.verts = verts
        self.colors = colors
        self.norms = norms
        self.show_edges = show_edges
        
        self._setdata()
        
    def _setdata( self ):
        l = len(self.verts) / 3
        if len(self.colors) == 3:
            self.colors = self.colors*l
        
        if self.norms:
            self.vertex_list = pyglet.graphics.vertex_list( l, ('v3f\static',self.verts), ('c3f\static',self.colors), ('n3f\static',self.norms) )
        else:
            self.vertex_list = pyglet.graphics.vertex_list( l, ('v3f\static',self.verts), ('c3f\static',self.colors) )
        
        if self.shapetype == GL_QUADS:
            q = l / 4
            iw = [ [
                i*12+0,i*12+1,i*12+2, i*12+3,i*12+4,i*12+5,
                i*12+3,i*12+4,i*12+5, i*12+6,i*12+7,i*12+8,
                i*12+6,i*12+7,i*12+8, i*12+9,i*12+10,i*12+11,
                i*12+9,i*12+10,i*12+11, i*12+0,i*12+1,i*12+2,
                ] for i in xrange(q) ]
            lw = [ self.verts[c] for x in iw for c in x ]
            lew = len(lw) / 3
            self.lv_wires = pyglet.graphics.vertex_list( lew, ('v3f\static',lw), ('c3f\static',[0.20]*(lew*3)) )
        
        elif self.shapetype == GL_TRIANGLES:
            q = l / 3
            iw = [ [
                i*9+0,i*9+1,i*9+2, i*9+3,i*9+4,i*9+5,
                i*9+3,i*9+4,i*9+5, i*9+6,i*9+7,i*9+8,
                i*9+6,i*9+7,i*9+8, i*9+0,i*9+1,i*9+2,
                ] for i in xrange(q) ]
            lw = [ self.verts[c] for x in iw for c in x ]
            lew = len(lw) / 3
            self.lv_wires = pyglet.graphics.vertex_list( lew, ('v3f\static',lw), ('c3f\static',[0.20]*(lew*3)) )
        
        else:
            self.lv_wires = None
        
    
    
    def render( self ):
        #pyglet.graphics.draw( self.l, self.shapetype, ('v3f', self.verts ), ( 'c3f', self.colors ) )
        self.vertex_list.draw( self.shapetype )
        if self.show_edges:
            self.render_wires()
    
    def render_wires( self ):
        self.lv_wires.draw( GL_LINES )
    
    def render_verts( self ):
        self.vertex_list.draw( GL_POINTS )

class Shape_Cycle( object ):
    def __init__( self, lshapes, fps, bounce ):
        self.lshapes = lshapes
        self.nshapes = len(lshapes)
        self.fps = fps
        self.bounce = bounce
    
    def _get_i( self ):
        i = int(time.time() * self.fps)
        if self.bounce:
            i = i % (self.nshapes*2-2)
            if i >= self.nshapes:
                i = self.nshapes*2-2 - i
            return i
        return i % self.nshapes
    
    def render( self ):
        i = self._get_i()
        if self.lshapes[i]:
            self.lshapes[i].render()
    
    def render_wires( self ):
        i = self._get_i()
        if self.lshapes[i]:
            self.lshapes[i].render_wires()
    
    def render_verts( self ):
        i = self._get_i()
        if self.lshapes[i]:
            self.lshapes[i].render_verts()

class Shape_Selectable( object ):
    def __init__( self, shape ):
        self.shape_orig = shape
        self.shape = shape.clone()
    
    def render( self ):
        self.shape.render()
    
    def render_wires( self ):
        self.shape.render_wires()
    
    def render_verts( self ):
        self.shape.render_verts()
    
    def clear_selection( self ):
        for i in xrange(len(self.shape.colors)):
            self.shape.colors[i] = self.shape_orig.colors[i]
        self.shape._setdata()
    
    def select( self, i ):
        self.shape.colors[i*3+0] = 1
        self.shape.colors[i*3+1] = 1
        self.shape.colors[i*3+1] = 0
        self.shape._setdata()

class Shape_Combine( object ):
    def __init__( self, lshapes ):
        self.lshapes = lshapes
    
    def render( self ):
        for s in self.lshapes:
            s.render()
    
    def render_wires( self ):
        for s in self.lshapes:
            s.render_wires()
    
    def render_verts( self ):
        for s in self.lshapes:
            s.render_verts()


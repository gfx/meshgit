import os, json, re, math
from mesh import *

"""
build correspondences between two sets of vertices
"""

# correspondence := closest vertex
def build_closestverts( mesh0, mesh1 ):
    lv0,lv1,lf0,lf1 = mesh0.lv,mesh1.lv,mesh0.lf,mesh1.lf
    mv01,mv10,mf01,mf10 = {},{},{},{}
    
    print( "computing distance matrix..." )
    dmat = [ [iv0, sorted([ [iv1,Vertex.distance2(v0,v1)] for (iv1,v1) in enumerate(lv1) ], key=lambda x:x[1] )] for iv0,v0 in enumerate(lv0) ]
    cnt = min(len(lv0),len(lv1))
    
    print( "building vertex correspondences..." )
    while cnt:
        # find closest pair of verts
        mini,mind = 0,dmat[0][1][0][1]
        for i,d in enumerate(dmat):
            iv0,ld2 = d
            if ld2[0][1] < mind:
                mini = i
                mind = ld2[0][1]
        iv0 = dmat[mini][0]
        iv1 = dmat[mini][1][0][0]
        d2 = dmat[mini][1][0][1]
        
        #print( '%i:%i = %f' % (iv0,iv1,d2) )
        mv01[iv0],mv10[iv1] = iv1,iv0
        
        # remove used verts
        dmat = [ [r[0],[ c for c in r[1] if c[0] != iv1 ]] for i_r,r in enumerate(dmat) if i_r != mini ]
        cnt -= 1
    
    print( "building face correspondences..." )
    lsf1 = [ f1.su_v() for f1 in lf1 ]
    for if0,f0 in enumerate(lf0):
        smf1 = set([ mv01[i_v] for i_v in f0 if i_v in mv01 ])                  # find corresponding verts
        if len(smf1) != len(f0): continue                                       # find enough corresponding verts?
        if smf1 not in lsf1: continue                                           # find corresponding face?
        if1 = lsf1.index(smf1)
        
        mf01[if0],mf10[if1] = if1,if0
    
    print( "done!" )
    return { 'mv01':mv01, 'mv10':mv10, 'mf01':mf01, 'mf10':mf10, }



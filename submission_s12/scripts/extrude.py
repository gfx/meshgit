import json, bpy, re, time, os, sys, hashlib

def remap_a2b( map_a2b, seta ):
    return [ map_a2b[i] for i in seta ]
def remap_b2a( map_a2b, seta ):
    v = list(map_a2b.values())
    return [ v.index(ia) for ia in seta ]

def select( metadata, meta, mesh, sset ):
    vuid,euid,fuid = meta['vuid'],meta['euid'],meta['fuid']
    
    assert vuid != None, "vuid == None!"
    
    bpy2 = bpy  # WEIRD BUG!?
    
    bpy2.ops.object.mode_set(mode = 'EDIT')
    bpy2.ops.mesh.select_all(action = 'DESELECT')
    
    bpy2.ops.object.mode_set(mode = 'OBJECT')
    
    bpy2.ops.wm.context_set_value(data_path="tool_settings.mesh_select_mode",value="(True,True,True)")
    for i_v,v in enumerate(mesh.vertices):
        v.select = ( vuid[i_v] in sset )
    for i_e,e in enumerate(mesh.edges):
        if euid[i_e] in sset:
            for i_v in e.vertices:
                mesh.vertices[i_v].select = True
    for i_f,f in enumerate(mesh.faces):
        if fuid[i_f] in sset:
            for i_v in f.vertices:
                mesh.vertices[i_v].select = True
    
    bpy2.ops.wm.context_set_value(data_path="tool_settings.mesh_select_mode",value="(False,True,True)")
    for i_e,e in enumerate(mesh.edges):
        e.select = ( euid[i_e] in sset )
    
    bpy2.ops.wm.context_set_value(data_path="tool_settings.mesh_select_mode",value="(False,False,True)")
    for i_f,f in enumerate(mesh.faces):
        f.select = ( fuid[i_f] in sset )
    
    bpy2.ops.object.mode_set(mode = 'EDIT')

def expand_set( meta, set0 ):
    vuid,euid,fuid = meta['vuid'],meta['euid'],meta['fuid']
    set1 = set(set0)
    for i_f,f in enumerate(meta['find']):
        if fuid[i_f] not in set0: continue
        for i_v in f:
            set1.add( vuid[i_v] )
        for i_v0 in f:
            for i_v1 in f:
                if i_v0 == i_v1: continue
                for i_e,e in enumerate(meta['eind']):
                    if i_v0 in e and i_v1 in e:
                        set1.add( euid[i_e] )
    for i_e,e in enumerate(meta['eind']):
        if euid[i_e] not in set0: continue
        for i_v in e:
            set1.add( vuid[i_v] )
    return set1

def reduce_set( metadata, meta, mesh, set0, t ):
    setr = set( [ v for k,v in metadata['uid_%s'%t].items() ] )
    return set0 & setr

#def translate( metadata, meta, mesh, set0, revcp ):
    #set1 = set()
    #for u in set0:
    #    ut = "%d"%u
    #    if ut in revcp:
    #        for uc in revpc[ut]:
    #        set.add(
    #return set( [ revcp["%s"%u] ] )

metadata = json.loads( bpy.data.texts['meta'].as_string() )
rootmetadata = json.loads( bpy.data.texts['rootmeta'].as_string() )
revbmetadata = json.loads( bpy.data.texts['revbmeta'].as_string() )
revcmetadata = json.loads( bpy.data.texts['revcmeta'].as_string() )

meta = metadata['list'][-1]
rootmeta = rootmetadata['list'][-1]
revbmeta = revbmetadata['list'][-1]
revcmeta = revcmetadata['list'][-1]

mesh = bpy.data.meshes[0]

#revcp = {"A_57":"A_-1","A_56":"A_-1","A_55":"A_-1","A_54":"A_-1","A_53":"A_-1","A_52":"A_-1","A_51":"A_-1","A_50":"A_-1","A_59":"A_-1","A_58":"A_-1","A_144":"A_-1","A_145":"A_-1","A_146":"A_-1","A_147":"A_-1","A_140":"A_-1","A_141":"A_-1","A_142":"A_-1","A_143":"A_-1","A_148":"A_-1","A_149":"A_-1","A_184":"A_-1","A_121":"A_-1","A_62":"A_-1","A_63":"A_-1","A_60":"A_-1","A_61":"A_-1","A_66":"A_-1","A_67":"A_-1","A_64":"A_-1","A_65":"A_-1","A_68":"A_-1","A_69":"A_-1","A_131":"A_-1","A_130":"A_-1","A_133":"A_-1","A_132":"A_-1","A_135":"A_-1","A_134":"A_-1","A_137":"A_-1","A_136":"A_-1","A_139":"A_-1","A_138":"A_-1","A_180":"A_-1","A_203":"A_-1","A_202":"A_-1","A_201":"A_-1","A_200":"A_-1","A_207":"A_-1","A_206":"A_-1","A_205":"A_-1","A_204":"A_-1","A_209":"A_-1","A_208":"A_-1","A_213":"A_-1","A_79":"A_-1","A_78":"A_-1","A_75":"A_-1","A_74":"A_-1","A_77":"A_-1","A_76":"A_-1","A_71":"A_-1","A_70":"A_-1","A_73":"A_-1","A_72":"A_-1","A_7":"A_-1","A_6":"A_-1","A_5":"A_-1","A_4":"A_-1","A_3":"A_-1","A_2":"A_-1","A_1":"A_-1","A_0":"A_-1","A_128":"A_-1","A_129":"A_-1","A_9":"A_-1","A_8":"A_-1","A_214":"A_-1","A_179":"A_-1","A_216":"A_-1","A_217":"A_-1","A_210":"A_-1","A_211":"A_-1","A_212":"A_-1","A_178":"A_-1","A_183":"A_-1","A_177":"A_-1","A_176":"A_-1","A_119":"A_-1","A_118":"A_-1","A_117":"A_-1","A_116":"A_-1","A_115":"A_-1","A_114":"A_-1","A_113":"A_-1","A_112":"A_-1","A_111":"A_-1","A_110":"A_-1","A_197":"A_-1","A_196":"A_-1","A_195":"A_-1","A_194":"A_-1","A_193":"A_-1","A_192":"A_-1","A_191":"A_-1","A_190":"A_-1","A_199":"A_-1","A_198":"A_-1","A_182":"A_-1","A_19":"A_-1","A_18":"A_-1","A_13":"A_-1","A_12":"A_-1","A_11":"A_-1","A_10":"A_-1","A_17":"A_-1","A_16":"A_-1","A_15":"A_-1","A_14":"A_-1","A_108":"A_-1","A_109":"A_-1","A_186":"A_-1","A_181":"A_-1","A_100":"A_-1","A_101":"A_-1","A_102":"A_-1","A_103":"A_-1","A_104":"A_-1","A_105":"A_-1","A_106":"A_-1","A_107":"A_-1","A_80":"A_-1","A_81":"A_-1","A_82":"A_-1","A_83":"A_-1","A_84":"A_-1","A_85":"A_-1","A_86":"A_-1","A_87":"A_-1","A_88":"A_-1","A_89":"A_-1","A_185":"A_-1","A_26":"A_-1","A_27":"A_-1","A_24":"A_-1","A_25":"A_-1","A_22":"A_-1","A_23":"A_-1","A_20":"A_-1","A_21":"A_-1","A_187":"A_-1","A_28":"A_-1","A_29":"A_-1","A_188":"A_-1","A_189":"A_-1","A_93":"A_-1","A_92":"A_-1","A_91":"A_-1","A_90":"A_-1","A_97":"A_-1","A_96":"A_-1","A_95":"A_-1","A_94":"A_-1","A_175":"A_-1","A_174":"A_-1","A_99":"A_-1","A_98":"A_-1","A_171":"A_-1","A_170":"A_-1","A_173":"A_-1","A_172":"A_-1","A_31":"A_-1","A_30":"A_-1","A_33":"A_-1","A_32":"A_-1","A_35":"A_-1","A_34":"A_-1","A_37":"A_-1","A_36":"A_-1","A_39":"A_-1","A_38":"A_-1","A_168":"A_-1","A_169":"A_-1","A_166":"A_-1","A_167":"A_-1","A_164":"A_-1","A_165":"A_-1","A_162":"A_-1","A_163":"A_-1","A_160":"A_-1","A_161":"A_-1","A_122":"A_-1","A_123":"A_-1","A_215":"A_-1","A_120":"A_-1","A_44":"A_-1","A_45":"A_-1","A_46":"A_-1","A_47":"A_-1","A_40":"A_-1","A_41":"A_-1","A_42":"A_-1","A_43":"A_-1","A_126":"A_-1","A_48":"A_-1","A_49":"A_-1","A_127":"A_-1","A_124":"A_-1","A_125":"A_-1","A_159":"A_-1","A_158":"A_-1","A_153":"A_-1","A_152":"A_-1","A_151":"A_-1","A_150":"A_-1","A_157":"A_-1","A_156":"A_-1","A_155":"A_-1","A_154":"A_-1"}
#revpc = {"A_-1":["A_0","A_1","A_2","A_3","A_4","A_5","A_6","A_7","A_8","A_9","A_10","A_11","A_12","A_13","A_14","A_15","A_16","A_17","A_18","A_19","A_20","A_21","A_22","A_23","A_24","A_25","A_26","A_27","A_28","A_29","A_30","A_31","A_32","A_33","A_34","A_35","A_36","A_37","A_38","A_39","A_40","A_41","A_42","A_43","A_44","A_45","A_46","A_47","A_48","A_49","A_50","A_51","A_52","A_53","A_54","A_55","A_56","A_57","A_58","A_59","A_60","A_61","A_62","A_63","A_64","A_65","A_66","A_67","A_68","A_69","A_70","A_71","A_72","A_73","A_74","A_75","A_76","A_77","A_78","A_79","A_80","A_81","A_82","A_83","A_84","A_85","A_86","A_87","A_88","A_89","A_90","A_91","A_92","A_93","A_94","A_95","A_96","A_97","A_98","A_99","A_100","A_101","A_102","A_103","A_104","A_105","A_106","A_107","A_108","A_109","A_110","A_111","A_112","A_113","A_114","A_115","A_116","A_117","A_118","A_119","A_120","A_121","A_122","A_123","A_124","A_125","A_126","A_127","A_128","A_129","A_130","A_131","A_132","A_133","A_134","A_135","A_136","A_137","A_138","A_139","A_140","A_141","A_142","A_143","A_144","A_145","A_146","A_147","A_148","A_149","A_150","A_151","A_152","A_153","A_154","A_155","A_156","A_157","A_158","A_159","A_160","A_161","A_162","A_163","A_164","A_165","A_166","A_167","A_168","A_169","A_170","A_171","A_172","A_173","A_174","A_175","A_176","A_177","A_178","A_179","A_180","A_181","A_182","A_183","A_184","A_185","A_186","A_187","A_188","A_189","A_190","A_191","A_192","A_193","A_194","A_195","A_196","A_197","A_198","A_199","A_200","A_201","A_202","A_203","A_204","A_205","A_206","A_207","A_208","A_209","A_210","A_211","A_212","A_213","A_214","A_215","A_216","A_217"]}

opB = [
    "splitedge",
    ['A_60', 'A_66', 'A_69', 'A_75', 'A_94', 'A_99', 'A_110', 'A_112', 'A_122', 'A_124', 'A_146', 'A_148']
    ]
opC = [
    "extrude",
    ['A_164', 'A_165', 'A_166', 'A_167', 'A_168', 'A_169', 'A_170', 'A_171', 'A_172', 'A_173', 'A_174', 'A_175', 'A_176', 'A_179', 'A_182', 'A_183', 'A_184', 'A_185', 'A_186', 'A_187', 'A_188', 'A_189', 'A_190', 'A_191', 'A_192', 'A_193', 'A_194', 'A_195', 'A_196', 'A_197', 'A_198', 'A_199', 'A_200', 'A_201', 'A_202', 'A_203', 'A_204', 'A_205', 'A_206', 'A_207', 'A_208', 'A_209', 'A_210', 'A_211', 'A_212', 'A_213', 'A_214', 'A_215', 'A_216', 'A_217'],
    ['A_177', 'A_178', 'A_180', 'A_181']
    ]

set0,set1 = opC[1:]
set2 = opB[1:]

set0 = expand_set( revcmeta, set0 )
set1 = expand_set( revcmeta, set1 )
set2 = expand_set( revbmeta, set2 )


select( metadata, meta, mesh, set1 )

import bpy as bpy3  # WEIRD BUG!?
#bpy3.ops.mesh.extrude( type='REGION' )

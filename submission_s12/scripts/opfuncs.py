# -*- coding: utf-8 -*-

import os, glob, sys, subprocess, time, json, re
from array import array
from config import *
import scenefuncs

################################################################################
### filtered and passed operations

#####################
# known ignorable ops

ignorable = [
    #"bpy.ops.mesh.edgering_select",
    #"bpy.ops.mesh.loop_select",
    #"bpy.ops.mesh.select_all",
    #"bpy.ops.mesh.select_inverse",
    #"bpy.ops.mesh.select_less",
    #"bpy.ops.mesh.select_more",
    #"bpy.ops.mesh.select_shortest_path",
    #"bpy.ops.view3d.select",
    #"bpy.ops.view3d.select_border",
    #"bpy.ops.view3d.select_circle",
    #"bpy.ops.view3d.select_lasso",
    
    "bpy.ops.object.mode_set",
    "bpy.ops.object.subdivision_set",
    
    "bpy.ops.screen.actionzone",
    "bpy.ops.screen.area_move",
    "bpy.ops.screen.frame_offset",
    "bpy.ops.screen.region_scale",
    "bpy.ops.screen.userpref_show",
    
    "bpy.ops.transform.create_orientation",
    
    "bpy.ops.view2d.pan",
    "bpy.ops.view2d.scroll_down",
    "bpy.ops.view2d.scroll_left",
    "bpy.ops.view2d.scroll_right",
    "bpy.ops.view2d.scroll_up",
    "bpy.ops.view2d.scroller_activate",
    
    "bpy.ops.view3d.cursor3d",
    "bpy.ops.view3d.dolly",
    "bpy.ops.view3d.edit_mesh_extrude_move_normal",
    "bpy.ops.view3d.enable_manipulator",
    "bpy.ops.view3d.fly",
    "bpy.ops.view3d.layers",
    "bpy.ops.view3d.manipulator",
    "bpy.ops.view3d.move",
    "bpy.ops.view3d.rotate",
    "bpy.ops.view3d.smoothview",
    "bpy.ops.view3d.snap_cursor_to_selected",
    "bpy.ops.view3d.snap_cursor_to_center",
    "bpy.ops.view3d.toolshelf",
    "bpy.ops.view3d.viewnumpad",
    "bpy.ops.view3d.view_orbit",
    "bpy.ops.view3d.view_persportho",
    "bpy.ops.view3d.view_selected",
    "bpy.ops.view3d.zoom",
    "bpy.ops.view3d.zoom_border",
    "bpy.ops.view3d.view_all",
    "bpy.ops.view3d.localview",
    
    "bpy.ops.wm.context_set_enum",
    "bpy.ops.wm.context_set_value",
    "bpy.ops.wm.context_toggle",
    "bpy.ops.wm.context_toggle_enum",
    "bpy.ops.wm.keyconfig_activate",
    "bpy.ops.wm.save_homefile",
    "bpy.ops.wm.splash",
    
    "bpy.ops.view3d.background_image_add",
    "bpy.ops.file.parent",
    "bpy.ops.file.highlight",
    "bpy.ops.file.select",
    "bpy.ops.file.select_execute",
    "bpy.ops.file.cancel",
    "bpy.ops.image.open",
    "bpy.ops.file.execute",
    "bpy.ops.wm.context_cycle_enum",
    
    #"interface",
    "undo",
    "redo",
    ]

# leave any trailing space and any quotations!
reignorableinterface = [
    '(Vertex|Edge|Face) select mode',
    '(Translate|Rotate|Scale) manipulator mode',
    '(Global|Normal|View|Gimbal|Local)',
    ]

# literal, ignorable parameters for interface operation
# leave any trailing space and any quotations!
litignorableinterface = [
    '"Limit selection to visible (clipped with depth buffer)"',
    '"3D Cursor "',
    '"Median Point "',
    '"Bounding Box Center "',
    ]

####################################
# passable ops and their translation

repassableinterface = {
    '"[XYZ]: -?[0-9]+\.?[0-9]+"': 'transform.set',
    '"[XYZ]"': 'modifier.change_property',
    }

repassableparams = {
    'interface': {
        '"[XYZ]: -?[0-9]+\.?[0-9]+"': 'transform.set',
        '"[XYZ]"': 'modifier.change_property',
        },
    'bpy.ops.mesh.delete': {
        '\\A\\Z': 'topo.delete.vert',  # regex '\A\Z' matches empty string
        'type=\'EDGE\'': 'topo.delete.edge',
        'type=\'FACE\'': 'topo.delete.face',
        'type=\'ALL\'': 'topo.delete.all',
        }
    }

passable = {
    "initial":                                    "meta.initial",
    "bpy.ops.object.editmode_toggle":             "meta.mode.edit",
    "bpy.ops.sculpt.sculptmode_toggle":           "meta.mode.sculpt",
    "bpy.ops.paint.weight_paint_toggle":          "meta.mode.weight_paint",
    
    "bpy.ops.ed.undo":                            "undo.undo",
    "bpy.ops.ed.redo":                            "undo.redo",
    
    "bpy.ops.mesh.edgering_select":               "select.edgering",
    "bpy.ops.mesh.loop_select":                   "select.loop",
    "bpy.ops.mesh.select_all":                    "select.toggle_all",
    "bpy.ops.mesh.select_inverse":                "select.inverse",
    "bpy.ops.mesh.select_less":                   "select.less",
    "bpy.ops.mesh.select_more":                   "select.more",
    "bpy.ops.mesh.select_shortest_path":          "select.shortest_path",
    "bpy.ops.view3d.select":                      "select.select",
    "bpy.ops.view3d.select_border":               "select.border",
    "bpy.ops.view3d.select_circle":               "select.circle",
    "bpy.ops.view3d.select_lasso":                "select.lasso",
    "bpy.ops.mesh.select_linked":                 "select.linked",
    
    "bpy.ops.object.select_all":                  "select.all_objects",
    
    "bpy.ops.object.modifier_add":                "modifier.add",
    "bpy.ops.object.modifier_apply":              "modifier.apply",
    "bpy.ops.object.modifier_copy":               "modifier.copy",
    "bpy.ops.object.modifier_move_down":          "modifier.move_down",
    "bpy.ops.object.modifier_move_up":            "modifier.move_up",
    "bpy.ops.modifier.toggle_mirror_x":           "modifier.mirror.toggle_x",
    "bpy.ops.modifier.toggle_mirror_y":           "modifier.mirror.toggle_y",
    "bpy.ops.modifier.toggle_mirror_z":           "modifier.mirror.toggle_z",
    "bpy.ops.modifier.set_merge_limit":           "modifier.mirror.set_merge_limit",
    "bpy.ops.object.modifier_remove":             "modifier.remove",
    
    "bpy.ops.transform.trackball":                "transform.trackball",
    "bpy.ops.transform.translate":                "transform.translate",
    "bpy.ops.transform.rotate":                   "transform.rotate",
    "bpy.ops.transform.resize":                   "transform.resize",
    "bpy.ops.transform.rotate":                   "transform.rotate",
    "bpy.ops.transform.edge_slide":               "transform.edge_slide",
    "bpy.ops.transform.set_x":                    "transform.set_x",
    "bpy.ops.transform.set_y":                    "transform.set_y",
    "bpy.ops.transform.set_z":                    "transform.set_z",
    "bpy.ops.transform.tosphere":                 "transform.to_sphere",
    "bpy.ops.mesh.normals_make_consistent":       "transform.make_normals_consistent",
    "bpy.ops.transform.shrink_fatten":            "transform.scale_along_normals",
    "bpy.ops.transform.mirror":                   "transform.mirror",
    "bpy.ops.view3d.snap_selected_to_cursor":     "transform.snap_to_cursor",
    
    "bpy.ops.mesh.primitive_circle_add":          "topo.add.circle",
    "bpy.ops.mesh.primitive_cone_add":            "topo.add.cone",
    "bpy.ops.mesh.primitive_cube_add":            "topo.add.cube",
    "bpy.ops.mesh.primitive_cylinder_add":        "topo.add.cylinder",
    "bpy.ops.mesh.primitive_grid_add":            "topo.add.grid",
    "bpy.ops.mesh.primitive_ico_sphere_add":      "topo.add.sphere.ico",
    "bpy.ops.mesh.primitive_uv_sphere_add":       "topo.add.sphere.uv",
    "bpy.ops.mesh.primitive_monkey_add":          "topo.add.monkey",
    "bpy.ops.mesh.primitive_plane_add":           "topo.add.plane",
    "bpy.ops.mesh.primitive_torus_add":           "topo.add.torus",
    
    "bpy.ops.mesh.edge_face_add":                 "topo.add.edge_face",
    "bpy.ops.mesh.fill":                          "topo.add.fill",
    "bpy.ops.mesh.tris_convert_to_quads":         "topo.convert.tris_to_quads",
    "bpy.ops.mesh.quads_convert_to_tris":         "topo.convert.quads_to_tris",
    #"bpy.ops.mesh.delete":                        "topo.delete.selected",
    "bpy.ops.mesh.delete_all":                    "topo.delete.all",
    "bpy.ops.mesh.delete_vert":                   "topo.delete.vert",
    "bpy.ops.mesh.delete_edge":                   "topo.delete.edge",
    "bpy.ops.mesh.delete_face":                   "topo.delete.face",
    "bpy.ops.object.delete":                      "topo.delete.object",
    "bpy.ops.mesh.duplicate":                     "topo.duplicate",
    "bpy.ops.mesh.duplicate_move":                "topo.duplicate",
    "bpy.ops.mesh.dupli_extrude_cursor":          "topo.duplicate",
    "bpy.ops.object.duplicate":                   "topo.duplicate_object",
    "bpy.ops.object.duplicate_move":              "topo.duplicate_object",
    "bpy.ops.mesh.edge_flip":                     "topo.edge_flip",
    "bpy.ops.mesh.extrude":                       "topo.extrude",
    "bpy.ops.mesh.extrude_edges_move":            "topo.extrude",
    "bpy.ops.mesh.extrude_faces_move":            "topo.extrude",
    "bpy.ops.mesh.extrude_region_move":           "topo.extrude",
    "bpy.ops.mesh.extrude_vertices_move":         "topo.extrude",
    "bpy.ops.mesh.spin":                          "topo.spin",
    "bpy.ops.mesh.loopcut":                       "topo.loopcut",
    "bpy.ops.mesh.loopcut_slide":                 "topo.loopcut",
    "bpy.ops.mesh.merge":                         "topo.merge",
    "bpy.ops.object.join":                        "topo.merge_object",
    "bpy.ops.mesh.remove_doubles":                "topo.merge_doubles",
    "bpy.ops.mesh.knife_cut":                     "topo.knife_cut",
    "bpy.ops.mesh.rip_move":                      "topo.rip",
    "bpy.ops.mesh.solidify":                      "topo.solidify",
    "bpy.ops.mesh.split":                         "topo.split",
    "bpy.ops.mesh.subdivide":                     "topo.subdivide",
    
    "redo_op":                                    "topo.op.redo",
    }

# regex to split the lines of steps.txt into:  filename, operation, and params
re_split_stepline = '(step[0-9]{6}\.blend): ([^(]+)\((.*)\)'



################################################################################
### operation functions

class UnknownOp(Exception):
    def __init__(self,value):
        self.value = value
    def __str__(self):
        return repr(self.value)

def translateop( stepline ):
    global re_split_stepline
    global ignorable, litignorableinterface, reignorableinterface
    global passable, repassableinterface
    
    splittext = re.split( re_split_stepline, stepline )
    
    # sanity checks
    if splittext[0] != '' or splittext[4] != '\n':
        print( "problem with re_split:" )
        print( "  step = '%s'", stepline )
        print( "  [0]  = '%s'", splittext[0] )
        print( "  [4]  = '%s'", splittext[4] )
        return None
    
    # grab components of step
    stepfile = splittext[1]
    op = splittext[2]
    opparam = splittext[3]
    (pathname, ext) = os.path.splitext(stepfile)
    
    passed = None
    
    if op in repassableparams:
        for k,v in repassableparams[op].iteritems():
            if re.search( k, opparam ):
                op = v
                passed = True
                break
    # translate op + sanity check
    if passed == None:
        if op == 'interface':
            #for k,v in repassableinterface.iteritems():
            #    if re.search( k, opparam ):
            #        op = v
            #        passed = True
            #        break
            if passed == None:
                for k in reignorableinterface:
                    if re.search( k, opparam ):
                        passed = False
                        break
            if passed == None:
                if opparam in litignorableinterface:
                    passed = False
                else:
                    print( "WARNING: Ignoring unknown interface param (%s): '%s'" % (stepline, opparam) )
                    passed = False
                    litignorableinterface.append( opparam )
        elif op in passable:
            op = passable[op]
            passed = True
        elif op in ignorable:
            passed = False
        else:
            raise UnknownOp( "unknown operation: %s" % op )
            #return None
    
    return (passed, op, opparam, pathname)

def filterundonework( ops, stepnumbers, tvmodel ):
    
    # TODO: remove redone undos!!!
    
    opsf = []
    stepnumbersf = []
    tvmodelf = []
    
    for i in range(len(ops)):
        op = ops[i]
        number = stepnumbers[i]
        scene = tvmodel[i]
        
        if op == 'undo.undo':
            j = len(tvmodelf) - 1
            while j >= 0:
                if scenefuncs.samescene( scene, tvmodelf[j] ): break
                j -= 1
            if j < 0:
                j = len(tvmodelf) - 1
                print( "WARNING: Could not find undo match for %d; matching to %d" % (number, stepnumbersf[j]) )
            else:
                #print( "matched undo in %d to %d" % (number, steps[j]) )
                opsf = opsf[:(j+1)]
                stepnumbersf = stepnumbersf[:(j+1)]
                tvmodelf = tvmodelf[:(j+1)]
        else:
            opsf.append( op )
            stepnumbersf.append( number )
            tvmodelf.append( scene )
    
    return ( opsf, stepnumbersf, tvmodelf )

def filterundonework_inds( ops, stepnumbers, tvmodel ):
    
    # TODO: remove redone undos!!!
    
    opsf = []
    stepnumbersf = []
    tvmodelf = []
    
    inds = []
    
    for i in range(len(ops)):
        op = ops[i]
        number = stepnumbers[i]
        scene = tvmodel[i]
        
        if op == 'undo.undo':
            j = len(tvmodelf) - 1
            while j >= 0:
                if scenefuncs.samescene( scene, tvmodelf[j] ): break
                j -= 1
            if j < 0:
                j = len(tvmodelf) - 1
                print( "WARNING: Could not find undo match for %d; matching to %d" % (number, stepnumbersf[j]) )
            else:
                #print( "matched undo in %d to %d" % (number, steps[j]) )
                opsf = opsf[:(j+1)]
                stepnumbersf = stepnumbersf[:(j+1)]
                tvmodelf = tvmodelf[:(j+1)]
                
                inds = inds[:(j+1)]
        else:
            opsf.append( op )
            stepnumbersf.append( number )
            tvmodelf.append( scene )
            
            inds.append( i )
    
    return inds

# clusters repeated operations, optionally renaming the cluster ops
def cluster_repeats( tvmodel, ops, steps, rematch, sumselections, replacewith = None ):
    ntvmodel = []
    nops = []
    nsteps = []
    selected = None
    
    i = len(tvmodel) - 1
    while i >= 0:
        op = ops[i]
        if re.search( rematch, op ):
            selected = tvmodel[i]
            if replacewith:
                op = replacewith
        
        nops.insert(0,op)
        nsteps.insert(0,steps[i])
        
        if not selected:
            ntvmodel.insert(0,tvmodel[i])
            i -= 1
            continue
        
        sfn = {}
        if sumselections:
            fn = config.get_selectedfn_only( selected )
            config.accum_selected_filenames( sfn, fn )
        
        j = i - 1
        while j >= 0:
            if not re.search(rematch, ops[j]):
                break
            if sumselections:
                fn = config.get_selectedfn_only( tvmodel[j] )
                config.accum_selected_filenames( sfn, fn )
            j -= 1
        
        if sumselections and "shapes" in selected:
            summed = config.get_accum_selected_arrays( sfn )
            if not summed:
                print( 'no accumulated selections found?' )
                print( '    i = %d, op = "%s", step = %d' % (i,op,steps[i]) )
                quit()
            for shape in selected["shapes"]:
                if not "selected_filename" in shape:
                    continue
                
                # strip off step from name
                name = re.sub( '_[0-9]{6}$', '', shape["name"] )
                
                # sanity check
                assert name in summed, "could not find %s in summed selections!" % name
                
                filename = "%s_summed" % shape["selected_filename"]
                shape["selected_filename"] = filename
                config.array_to_file( filename, summed[name], 1 )
        
        ntvmodel.insert(0,selected)
        i = j
        selected = None
    
    return ( ntvmodel, nops, nsteps )

# clusters pair operations, optionally renaming the cluster ops
def cluster_pairs( tvmodel, ops, steps, rematch0, rematch1, replacewith = None ):
    ntvmodel = []
    nops = []
    nsteps = []
    selected = None
    
    i = len(tvmodel) - 1
    while i >= 1:
        op = ops[i]
        if re.search( rematch1, op ) and re.search( rematch0, ops[i-1] ):
            selected = tvmodel[i]
            if replacewith:
                op = replacewith
        
        ntvmodel.insert(0,tvmodel[i])
        nops.insert(0,op)
        nsteps.insert(0,steps[i])
        
        if selected:
            i -= 2
            selected = None
        else:
            i -= 1
    
    return ( ntvmodel, nops, nsteps )




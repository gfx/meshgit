#!/usr/bin/env python

import os, re, sys, math, random, itertools, pickle #, json
import numpy
from pyglet.window import key
from mesh import *
from viewer import *
from shapes import *
from vec3f import *
from mesh_correspondence import *


########################################################################################################################
########################################################################################################################

def random_color( lavoid=None ):
    lbc = lavoid or [] #[ [0,0,0] ]
    threshold = 0.4
    while True:
        co = [ random.uniform(0.2,1.0), random.uniform(0.2,1.0), random.uniform(0.2,1.0) ]
        if all( sum([(c0-c1)*(c0-c1) for c0,c1 in zip(co,bc)]) > threshold for bc in lbc ):
            break
    return co

def random_color_subset():
    c = [ [1,1,1], [1,1,0], [1,0,1], [0,1,1], [1,0,0], [0,1,0], [0,0,1] ]
    return random.choice( c )

def get_color( u, su0, su1, su2 ):
    lavoid = [ [1,0,0],[0,1,0],[0,0,1],[1,1,0],[0,1,1],[1,0,1],[0,0,0] ]
    lavoid += [ [0.5,0,0], [0,0.5,0], [0,0,0.5] ]
    r = 1 if u in su0 else 0
    g = 1 if u in su1 else 0
    b = 1 if u in su2 else 0
    if r+g+b >= 2: return random_color( lavoid )
    return [r*0.5,g*0.5,b*0.5]


# orange: [ 0.75, 0.40, 0.00 ]
# yellow: [ 0.90, 0.90, 0.10 ]

# light
color_schemes = {
    'light': {
        '=':  [ 0.50, 0.50, 0.50 ],
        '+':  [ 0.20, 0.90, 0.20 ],
        '-':  [ 0.10, 0.50, 0.10 ],
        #'>':  [ 0.50, 0.50, 0.50 ],
        '>':  [ 0.40, 0.40, 0.90 ],
        '+a': [ 0.20, 0.90, 0.20 ],
        '-a': [ 0.10, 0.50, 0.10 ],
        '+b': [ 0.90, 0.90, 0.10 ],
        '-b': [ 0.60, 0.50, 0.00 ],
        '*':  [ 0.50, 0.00, 0.00 ],
        '/':  [ 0.75, 0.40, 0.00 ],
        '!':  [ 0.05, 0.90, 0.70 ],
        },
    'std': {
        '=':  [ 0.50, 0.50, 0.50 ],
        '+':  [ 0.20, 0.90, 0.20 ],
        '-':  [ 0.90, 0.20, 0.20 ],
        '>':  [ 0.50, 0.50, 0.90 ],
        '+a': [ 0.20, 0.90, 0.20 ],
        '-a': [ 0.90, 0.20, 0.20 ],
        '+b': [ 0.05, 0.40, 0.05 ],
        '-b': [ 0.40, 0.05, 0.05 ],
        '*':  [ 0.90, 0.90, 0.10 ],
        '/':  [ 0.75, 0.40, 0.00 ],
        '!':  [ 0.05, 0.90, 0.70 ],
        },
    }
color_scheme = color_schemes['std']


########################################################################################################################

# NOTE: assuming that mesh1 has been aligned to mesh0
def partition_parts( mesh0, mesh1 ):
    dsu_fp = {} # partitions set
    
    print( 'partitioning parts...' )
    
    su_v0 = set([ v.u_v for v in mesh0.lv ])
    su_f0 = set([ f.u_f for f in mesh0.lf ])
    su_v1 = set([ v.u_v for v in mesh1.lv ])
    su_f1 = set([ f.u_f for f in mesh1.lf ])
    
    su_f  = (su_f0 | su_f1)
    su_fd = (su_f0 - su_f1)
    su_fa = (su_f1 - su_f0)
    su_fu = (su_f0 & su_f1)
    su_fm = set( u_f for u_f in su_fu if len(set(mesh0.get_f(u_f).lu_v) & set(mesh1.get_f(u_f).lu_v)) != max(len(mesh0.get_f(u_f).lu_v),len(mesh1.get_f(u_f).lu_v)) )
    su_fu = su_fu - su_fm
    
    print( su_fm )
    
    # build adjacency datastructure
    adj_e = {}
    adj_f = {}
    for f in mesh0.lf + mesh1.lf:
        if f.u_f not in adj_f: adj_f[f.u_f] = { 'e': set() }
        lluv = zip( f.lu_v, f.lu_v[1:] + [f.lu_v[0]] )
        lsuv = [ frozenset(luv) for luv in lluv ]
        
        if f.u_f in su_fm: tf = 'm'
        elif f.u_f in su_fd: tf = 'd'
        elif f.u_f in su_fa: tf = 'a'
        else: tf = 'u'
        
        for suv in lsuv:
            if suv not in adj_e: adj_e[suv] = { 'f': set(), 'a':0, 'd':0, 'm':0, 'u':0 }
            adj_e[suv]['f'].add(f.u_f)
            adj_e[suv][tf] = 1
            adj_f[f.u_f]['e'].add(suv)
    
    
    # find border to deleted faces
    su_f_untouched = set( su_fd )
    while su_f_untouched:
        su_fp,su_epb = set(),set()
        
        su_fpdn = set([su_f_untouched.pop()])
        while su_fpdn:
            u_f = su_fpdn.pop()
            su_fp.add(u_f)
            for su_v in adj_f[u_f]['e']:
                e = adj_e[su_v]
                if e['a'] or e['u']:
                    su_epb.add(su_v)
                else:
                    for u_f in e['f']:
                        if u_f not in su_f_untouched: continue
                        su_fpdn.add(u_f)
                        su_f_untouched.remove(u_f)
        
        dsu_fp[ frozenset(su_epb) ] = su_fp
    
    # find border to added faces
    su_f_untouched = set( su_fa )
    while su_f_untouched:
        su_fp,su_epb = set(),set()
        
        su_fpdn = set([su_f_untouched.pop()])
        while su_fpdn:
            u_f = su_fpdn.pop()
            su_fp.add(u_f)
            for su_v in adj_f[u_f]['e']:
                e = adj_e[su_v]
                if e['d'] or e['m']:
                    su_epb.add(su_v)
                else:
                    for u_f in e['f']:
                        if u_f not in su_f_untouched: continue
                        su_fpdn.add(u_f)
                        su_f_untouched.remove(u_f)
        
        # TODO: need to partition disjoint border sets
        
        # if edge-border has been seen before, combine!
        su_epb = frozenset(su_epb)
        if su_epb in dsu_fp: dsu_fp[ su_epb ] |= su_fp
        else: dsu_fp[ su_epb ] = su_fp
    
    #print( str(dsu_fp) )
    return dsu_fp


########################################################################################################################
# diff commands
########################################################################################################################


# NOTE: assuming that mesh1 has been aligned to mesh0
def diff_commands( mesh0, mesh1 ):
    print( 'diffing commands...' )
    
    su_v0 = set([ v.u_v for v in mesh0.lv ])
    su_f0 = set([ f.u_f for f in mesh0.lf ])
    su_v1 = set([ v.u_v for v in mesh1.lv ])
    su_f1 = set([ f.u_f for f in mesh1.lf ])
    
    cmd_del_f = [ 'delface %s' % u_f for u_f in (su_f0-su_f1) ]
    cmd_del_v = [ 'delvert %s' % u_v for u_v in (su_v0-su_v1) ]
    cmd_chg_v = [ 'move %s:[%s]' % (u_v,','.join([str(c) for c in mesh1.get_v(u_v).p])) for u_v in (su_v1 & su_v0) if Vertex.distance2(mesh0.get_v(u_v),mesh1.get_v(u_v)) > 0.0001 ]
    cmd_mod_f = [ 'modface %s:[%s]' % (u_f,','.join(mesh1.get_f(u_f).lu_v)) for u_f in (su_f1 & su_f0) if len(set(mesh0.get_f(u_f).lu_v) & set(mesh1.get_f(u_f).lu_v)) != max(len(mesh0.get_f(u_f).lu_v),len(mesh1.get_f(u_f).lu_v)) ]
    cmd_add_v = [ 'addvert %s:[%s]' % (u_v,','.join([str(c) for c in mesh1.get_v(u_v).p])) for u_v in (su_v1-su_v0) ]
    cmd_add_f = [ 'addface %s:[%s]' % (u_f,','.join(mesh1.get_f(u_f).lu_v)) for u_f in (su_f1-su_f0) ]
    
    return cmd_del_f + cmd_del_v + cmd_chg_v + cmd_mod_f + cmd_add_v + cmd_add_f

# NOTE: assuming that mesh1 has been aligned to mesh0
def diff_commands_by_partition( mesh0, mesh1 ):
    
    print( 'diffing commands by partition...' )
    
    dp = partition_parts( mesh0, mesh1 )
    
    su_v0 = set([ v.u_v for v in mesh0.lv ])
    su_f0 = set([ f.u_f for f in mesh0.lf ])
    su_v1 = set([ v.u_v for v in mesh1.lv ])
    su_f1 = set([ f.u_f for f in mesh1.lf ])
    
    su_vd = su_v0 - su_v1
    su_va = su_v1 - su_v0
    su_vu = su_v0 & su_v1
    
    su_fd = su_f0 - su_f1
    su_fa = su_f1 - su_f0
    su_fu = su_f0 & su_f1
    
    vcmds = {
        'uvd': [ 'delvert %s' % u_v for u_v in su_vd ],
        'uva': [ 'addvert %s:[%s]' % (u_v,','.join([str(c) for c in mesh1.get_v(u_v).p])) for u_v in su_va ],
        'uvm': [ 'move %s:[%s]' % (u_v,','.join([str(c) for c in mesh1.get_v(u_v).p])) for u_v in su_vu if Vertex.distance2(mesh0.get_v(u_v),mesh1.get_v(u_v)) > 0.0001 ],
        }
    
    fcmds = []
    for sfp in dp.values():
        fcmds.append( {
            'sf': sfp,
            'ufd': [ 'delface %s' % u_f for u_f in sfp if u_f in su_fd ],
            'ufa': [ 'addface %s:[%s]' % (u_f,','.join(mesh1.get_f(u_f).lu_v)) for u_f in sfp if u_f in su_fa ],
            'ufm': [ 'modface %s:[%s]' % (u_f,','.join(mesh1.get_f(u_f).lu_v)) for u_f in sfp if u_f in su_f0 and u_f in su_f1 and len(set(mesh0.get_f(u_f).lu_v) & set(mesh1.get_f(u_f).lu_v)) != max(len(mesh0.get_f(u_f).lu_v),len(mesh1.get_f(u_f).lu_v)) ]
            } )
    
    return (vcmds,fcmds)


########################################################################################################################
# perform commands on mesh
########################################################################################################################


def do_commands( mesh, cmds ):
    for cmd in cmds:
        _,op,uid,_,csv,_ = re.split( '^([a-zA-Z]+) ([^:]+)(:\[(.+)\])?$', cmd )
        if csv: csv = csv.split(',')
        
        if op == 'delface':
            mesh.del_face( uid )
        elif op == 'delvert':
            mesh.del_vert( uid )
        elif op == 'move':
            mesh.get_v( uid ).p = [float(v) for v in csv]
        elif op == 'modface':
            su_v = set(mesh.get_lu_v())
            assert all( u_v in su_v for u_v in csv )
            mesh.lf[uid].lu_v = csv
        elif op == 'addvert':
            mesh.add_vert( u_v=uid, p=[float(v) for v in csv] )
        elif op == 'addface':
            su_v = set(mesh.get_lu_v())
            assert all( u_v in su_v for u_v in csv )
            mesh.add_face( u_f=uid, lu_v=csv )
        else:
            assert False, 'unknown command: "%s"' % cmd
    
    try: mesh.validate()
    except:
        print( cmds )
        exit()
    
    return mesh


########################################################################################################################
# "ui"
########################################################################################################################

# NOTE: this will only merge not-overlapping edits
def run_merge( fn0, fn1, fn2, fn012 ):
    # load 3 vers
    m0 = Mesh.fromPLY( fn0 )
    m1 = Mesh.fromPLY( fn1 )
    m2 = Mesh.fromPLY( fn2 )
    
    m1a = Mesh.bc_closestverts( m0, m1 )
    m2a = Mesh.bc_closestverts( m0, m2 )
    
    # compute diffs
    #c01 = diff_commands( m0, m1a )
    #c02 = diff_commands( m0, m2a )
    llc01v,llc01f = diff_commands_by_partition( m0, m1a )
    llc02v,llc02f = diff_commands_by_partition( m0, m2a )
    c01,c02 = [],[]
    c01.extend( llc01v['uvm'] )
    c01.extend( llc01v['uva'] )
    c02.extend( llc02v['uvm'] )
    c02.extend( llc02v['uva'] )
    for lc01 in llc01f:
        overlap = False
        for lc02 in llc02f:
            if lc01['sf'] & lc02['sf']:
                overlap = True
                break
        if overlap: continue
        for k,v in lc01.items():
            if k != 'sf': c01.extend(v)
    for lc02 in llc02f:
        overlap = False
        for lc01 in llc01f:
            if lc01['sf'] & lc02['sf']:
                overlap = True
                break
        if overlap: continue
        for k,v in lc02.items():
            if k != 'sf': c02.extend(v)
    c01.extend( llc01v['uvd'] )
    c02.extend( llc02v['uvd'] )
    
    print()
    print( c01 )
    print()
    print( c02 )
    print()
    
    # apply diffs to ancestor
    m012 = m0.clone()  # create exact copy of m0 to edit
    do_commands( m012, c01 )
    do_commands( m012, c02 )
    
    # save merged mesh to file
    m012.toPLY( fn012 )

def run_diff( fn0, fn1, fnd, fnc, fna ):
    # load 3 vers
    m0 = Mesh.fromPLY( fn0 )
    m1 = Mesh.fromPLY( fn1 )
    
    m1a = Mesh.bc_closestverts( m0, m1 )
    
    c01 = diff_commands( m0, m1a )
    su_df = set([ cmd.split(' ')[1].split(':')[0] for cmd in c01 if cmd.startswith('delface') ])
    su_af = set([ cmd.split(' ')[1].split(':')[0] for cmd in c01 if cmd.startswith('addface') ])
    su_cf = set(m0.get_lu_f()) - su_df
    
    md = m0.clone()
    md.lf = [ f for f in md.lf if f.u_f in su_df ]
    md.trim()
    
    mc = m0.clone()
    mc.lf = [ f for f in mc.lf if f.u_f in su_cf ]
    mc.trim()
    
    ma = m0.clone(clone_faces=False)
    do_commands( ma, [ cmd for cmd in c01 if cmd.startswith('add') ] )
    ma.trim()
    
    md.toPLY( fnd )
    mc.toPLY( fnc )
    ma.toPLY( fna )

####################################################################################################################

def run_diff_viewer2( fn0, fn1, method=None ):
    m0,m1 = [ Mesh.fromPLY( fn ) for fn in [fn0,fn1] ]
    scaling_factor = scale( [ m0, m1 ] )
    m1 = get_corresponding_mesh1( build_correspondence( m0, m1, method ), allow_face_sub=False )
    su_v0 = set( m0.get_lu_v() )
    dvd1 = dict( [ ( v.u_v, Vec3f.t_distance(v.p,m0.opt_get_v(v.u_v).p) ) for v in m1.lv  if v.u_v in su_v0 ] )
    scale( [m0,m1], 1.0 / scaling_factor )
    
    scaling_factor = scale( [ m0, m1 ], scaling_factor='unitcube' )
    su_v0 = set( m0.get_lu_v() )
    dvd1 = dict( [ ( v.u_v, Vec3f.t_distance(v.p,m0.opt_get_v(v.u_v).p) ) for v in m1.lv  if v.u_v in su_v0 ] )
    scale( [m0,m1], 1.0 / scaling_factor )
    
    for m in [m0,m1]:
        m.optimize()
        m.rebuild_edges()
    
    c01 = diff_commands( m0, m1 )
    su_df = set([ cmd.split(' ')[1].split(':')[0] for cmd in c01 if cmd.startswith('delface') ])
    su_af = set([ cmd.split(' ')[1].split(':')[0] for cmd in c01 if cmd.startswith('addface') ])
    su_mf = set([ cmd.split(' ')[1].split(':')[0] for cmd in c01 if cmd.startswith('modface') ])
    su_cf = set(m0.get_lu_f()) - su_df - su_mf
    su_mv = set([ cmd.split(' ')[1].split(':')[0] for cmd in c01 if cmd.startswith('move') ])
    
    m0.recolor( color_scheme['='] )
    m1.recolor( color_scheme['='] )
    
    mc = m0.clone().filter_faces( su_cf | su_mf, True ).trim()
    md = m0.clone().filter_faces( su_df, True ).trim().recolor( color_scheme['-'] )
    
    mc2 = m1.clone().filter_faces( su_cf, True ).trim()
    ma2 = m1.clone().filter_faces( su_af, True ).trim().recolor( color_scheme['+'] )
    mm2 = m1.clone().filter_faces( su_mf, True ).trim().recolor( color_scheme['='] )
    mm2.optimize()
    
    for v in mc2.lv + mm2.lv:
        if v.u_v in su_mv:
            val = min( 1.0, dvd1[v.u_v] * 25.0 )
            v.c = Vec3f.t_interp( color_scheme['='], color_scheme['>'], val )
    
    #for u_f in su_mf:
        #lu_v0 = m0.opt_get_f(u_f).lu_v
        #lu_v1 = m1.opt_get_f(u_f).lu_v
        #for u_v in set(lu_v1)-set(lu_v0):
            #mm2.opt_get_v(u_v).c = color_scheme['!']
    #levd = []
    #levc = []
    #for u_f in su_mf:
        #lu_v0 = m0.opt_get_f(u_f).lu_v
        #lu_v1 = m1.opt_get_f(u_f).lu_v
        #lu_v1.extend(lu_v1)
        
        #for u0,u1 in zip(lu_v0,lu_v0[1:]+[lu_v0[0]]):
            #if u0 in lu_v1 or u1 in lu_v1: continue
            #levd += m0.opt_get_v(u0).p + m0.opt_get_v(u1).p
            #lecd += [ 1.0,0.0,0.0, 1.0,0.0,0.0 ]
    #sevd = Shape( GL_LINES, levd, levc, show_edges=True )
    
    mc.rebuild_edges()
    mc2.rebuild_edges()
    md.rebuild_edges()
    ma2.rebuild_edges()
    mm2.rebuild_edges()
    
    meshes = [ mc.copy().extend(md), mc2.copy().extend(ma2).extend(mm2) ]
    shapes = [ mesh.toShape() for mesh in meshes ]
    #shapes[0] = Shape_Combine( shapes[0], sevd )
    window = Viewer( shapes, caption='MeshGit Diff2 Viewer: %s -> %s' % (fn0,fn1) )
    window.app_run()

def run_diff_viewer2_iter( fn0, fn1, method=None ):
    m0,m1 = [ Mesh.fromPLY( fn ) for fn in [fn0,fn1] ]
    scaling_factor = scale( [ m0, m1 ] )
    
    lmf10 = []
    mf10 = {}
    c01 = None
    
    for maxiters in xrange(5):
        print( '*' * 20 )
        print( 'maxiters = %i' % maxiters )
        print( '*' * 20 )
        print( '' )
        print( '' )
        
        if maxiters == 20: c01.lastpass = True
        c01 = build_correspondence( m0, m1, method=method, maxiters=min(maxiters,1), ci=c01 )
        
        for if1,if0 in c01.mf10.iteritems():
            if if1 in mf10 and mf10[if1][0] == if0:
                mf10[if1][1] += 1
            else:
                mf10[if1] = [if0,1]
        for if1,d in mf10.iteritems():
            if if1 in c01.mf10 and c01.mf10[if1] == d[0]: continue
            mf10[if1] = [ d[0],0 ]
        
        #print( mf10 )
        lmf10 += [ copy.deepcopy(mf10) ]
        
        if c01.done:
            print( '*' * 20 )
            print( 'CONVERGED!!!' )
            print( '*' * 20 )
            print( '' )
            print( '' )
            break
    
    scale( [m0,m1], 1.0 / scaling_factor )
    m1 = get_corresponding_mesh1( c01, allow_face_sub=False )
    for m in [m0,m1]:
        m.optimize()
        m.rebuild_edges()
    lcmds = diff_commands( m0, m1 )
    
    M = max([ x[1][1] for x in mf10.items() ])
    L = len(lmf10)
    
    color_ramp = [
        [ 0.35, 0.35, 0.35 ],
        [ 0.90, 0.90, 0.10 ],
        [ 0.70, 0.70, 0.70 ],
        #[ 0.75, 0.50, 0.00 ],
        ]
    #color_ramp = [
        #[ 0.5, 0.5, 0.5 ],
        #[ 1.0, 1.0, 0.0 ],
        #[ 1.0, 0.5, 0.0 ],
        #[ 0.6, 0.25, 0.5 ],
        #[ 0.4, 0.1, 0.6 ],
        #[ 0.0, 0.0, 0.6 ],
        #]
    
    lm2 = []
    lm3 = []
    for i,mf10 in enumerate(lmf10):
        mf01 = dict( (d[0], [if1,d[1]]) for if1,d in mf10.iteritems() )
        
        m2 = Mesh()
        for i_f,f in enumerate(m0.lf):
            if i_f in mf01 and mf01[i_f] != 0:
                val = mf01[i_f][1]
            else:
                val = 0
            c = color_ramp[min(val,len(color_ramp)-1)]
            lv = [ m0.opt_get_v(u_v).copy().recolor(c) for u_v in f.lu_v ]
            lu_v = [ v.u_v for v in lv ]
            m2.lv += lv
            m2.lf += [ Face(u_f=f.u_f,lu_v=lu_v) ]
        m2.rebuild_edges()
        lm2 += [m2]
        
        m3 = Mesh()
        for i_f,f in enumerate(m1.lf):
            if i_f in mf10 and mf10[i_f] != 0:
                val = mf10[i_f][1]
            else:
                val = 0
            c = color_ramp[min(val,len(color_ramp)-1)]
            lv = [ m1.opt_get_v(u_v).copy().recolor(c) for u_v in f.lu_v ]
            lu_v = [ v.u_v for v in lv ]
            m3.lv += lv
            m3.lf += [ Face(u_f=f.u_f,lu_v=lu_v) ]
        m3.rebuild_edges()
        lm3 += [m3]
    
    su_df = set([ cmd.split(' ')[1].split(':')[0] for cmd in lcmds if cmd.startswith('delface') ])
    su_af = set([ cmd.split(' ')[1].split(':')[0] for cmd in lcmds if cmd.startswith('addface') ])
    su_cf = set(m0.get_lu_f()) - su_df
    su_mv = set([ cmd.split(' ')[1].split(':')[0] for cmd in lcmds if cmd.startswith('move') ])
    
    m0.recolor( color_scheme['='] )
    m1.recolor( color_scheme['='] )
    
    mc = m0.clone().filter_faces( su_cf, True ).trim()
    md = m0.clone().filter_faces( su_df, True ).trim().recolor( color_scheme['-'] )
    
    mc2 = m1.clone().filter_faces( su_cf, True ).trim()
    ma2 = m1.clone().filter_faces( su_af, True ).trim().recolor( color_scheme['+'] )
    
    for v in mc2.lv:
        if v.u_v in su_mv:
            v.c = color_scheme['>']
    
    m0.rebuild_edges()
    m1.rebuild_edges()
    mc.rebuild_edges()
    mc2.rebuild_edges()
    md.rebuild_edges()
    ma2.rebuild_edges()
    
    mc.extend(md)
    mc2.extend(ma2)
    
    #shapes = [ mesh.toShape() for mesh in ([m0]+lm2+[mc,m1]+lm3+[mc2]) ]
    #window = Viewer( shapes, caption='MeshGit Diff2 Iter Viewer: %s -> %s' % (fn0,fn1), ncols=L+2, size=(300,300) )
    cycle0 = Shape_Cycle( [ m.toShape() for m in lm2 ], 4.0, False )
    cycle1 = Shape_Cycle( [ m.toShape() for m in lm3 ], 4.0, False )
    shapes = [ m0.toShape(), cycle0, mc.toShape(), m1.toShape(), cycle1, mc2.toShape() ]
    
    window = Viewer( shapes, caption='MeshGit Diff2 Iter Viewer: %s -> %s' % (fn0,fn1), ncols=3 )
    window.app_run()


def run_diff_viewer3( fn0, fn1, fn2, method=None ):
    m0,ma,mb = [ Mesh.fromPLY( fn ) for fn in [fn0,fn1,fn2] ]
    scaling_factor = scale( [ m0, ma, mb ] )
    
    ma = get_corresponding_mesh1( build_correspondence( m0, ma, method ), allow_face_sub=False )
    mb = get_corresponding_mesh1( build_correspondence( m0, mb, method ), allow_face_sub=False )
    
    for m in [m0,ma,mb]:
        m.optimize()
        m.rebuild_edges()
    
    scale( [m0,ma,mb], 1.0 / scaling_factor )
    
    scaling_factor = scale( [m0,ma,mb], scaling_factor='unitcube' )
    su_v0 = set( m0.get_lu_v() )
    dvda = dict( [ ( v.u_v, Vec3f.t_distance(v.p,m0.opt_get_v(v.u_v).p) ) for v in ma.lv if v.u_v in su_v0 ] )
    dvdb = dict( [ ( v.u_v, Vec3f.t_distance(v.p,m0.opt_get_v(v.u_v).p) ) for v in mb.lv if v.u_v in su_v0 ] )
    scale( [m0,ma,mb], 1.0 / scaling_factor )
    
    c0a = diff_commands( m0, ma )
    su_dfa = set([ cmd.split(' ')[1].split(':')[0] for cmd in c0a if cmd.startswith('delface') ])
    su_afa = set([ cmd.split(' ')[1].split(':')[0] for cmd in c0a if cmd.startswith('addface') ])
    su_mfa = set([ cmd.split(' ')[1].split(':')[0] for cmd in c0a if cmd.startswith('modface') ])
    su_cfa = set(m0.get_lu_f()) - su_dfa - su_mfa
    su_mva = set([ cmd.split(' ')[1].split(':')[0] for cmd in c0a if cmd.startswith('move') ])
    su_ava = set([ cmd.split(' ')[1].split(':')[0] for cmd in c0a if cmd.startswith('addvert') ])
    
    c0b = diff_commands( m0, mb )
    su_dfb = set([ cmd.split(' ')[1].split(':')[0] for cmd in c0b if cmd.startswith('delface') ])
    su_afb = set([ cmd.split(' ')[1].split(':')[0] for cmd in c0b if cmd.startswith('addface') ])
    su_mfb = set([ cmd.split(' ')[1].split(':')[0] for cmd in c0b if cmd.startswith('modface') ])
    su_cfb = set(m0.get_lu_f()) - su_dfb - su_mfb
    su_mvb = set([ cmd.split(' ')[1].split(':')[0] for cmd in c0b if cmd.startswith('move') ])
    su_avb = set([ cmd.split(' ')[1].split(':')[0] for cmd in c0b if cmd.startswith('addvert') ])
    
    
    mda = m0.clone().filter_faces( su_dfa, True ).trim().recolor( color_scheme['-a'] )
    mca = ma.clone().filter_faces( su_cfa, True ).trim().recolor( color_scheme['='] )
    maa = ma.clone(clone_faces=False)
    do_commands( maa, [ cmd for cmd in c0a if cmd.startswith('add') ] )
    maa.trim().recolor( color_scheme['+a'] )
    mma = ma.clone().filter_faces( su_mfa, True ).trim().recolor( color_scheme['='] )
    mma.optimize()
    
    mdb = m0.clone().filter_faces( su_dfb, True ).trim().recolor( color_scheme['-b'] )
    mcb = mb.clone().filter_faces( su_cfb, True ).trim().recolor( color_scheme['='] )
    mab = mb.clone(clone_faces=False)
    do_commands( mab, [ cmd for cmd in c0b if cmd.startswith('add') ] )
    mab.trim().recolor( color_scheme['+b'] ) #mab.recolor( [0.4,0.75,0.0] )
    mmb = mb.clone().filter_faces( su_mfb, True ).trim().recolor( color_scheme['='] )
    mmb.optimize()
    
    
    # color moved verts
    for v in mca.lv + mma.lv:
        if v.u_v in su_mva:
            val = min( 1.0, dvda[v.u_v] * 25.0 )
            v.c = Vec3f.t_interp( color_scheme['='], color_scheme['>'], val )
    for v in mcb.lv + mmb.lv:
        if v.u_v in su_mvb:
            val = min( 1.0, dvdb[v.u_v] * 25.0 )
            v.c = Vec3f.t_interp( color_scheme['='], color_scheme['>'], val )
    
    # color sub'd faces
    for u_f in su_mfa:
        lu_v0 = m0.opt_get_f(u_f).lu_v
        lu_va = ma.opt_get_f(u_f).lu_v
        for u_v in set(lu_va) - set(lu_v0):
            mma.opt_get_v(u_v).c = color_scheme['!']
    for u_f in su_mfb:
        lu_v0 = m0.opt_get_f(u_f).lu_v
        lu_vb = mb.opt_get_f(u_f).lu_v
        for u_v in set(lu_vb) - set(lu_v0):
            mmb.opt_get_v(u_v).c = color_scheme['!']
    
    m0c0 = m0.clone().filter_faces( su_dfa | su_dfb | su_mfa | su_mfb, False ).trim()
    m0cad = m0.clone().filter_faces( su_dfa, True ).filter_faces( su_dfb | su_mfb, False ).trim().recolor( color_scheme['-a'] )
    m0cam = m0.clone().filter_faces( su_mfa, True ).filter_faces( su_dfb | su_mfb, False ).trim().recolor( color_scheme['='] )
    m0cbd = m0.clone().filter_faces( su_dfb, True ).filter_faces( su_dfa | su_mfa, False ).trim().recolor( color_scheme['-b'] )
    m0cbm = m0.clone().filter_faces( su_mfb, True ).filter_faces( su_dfa | su_mfa, False ).trim().recolor( color_scheme['-b'] )
    m0cc = m0.clone().filter_faces( (su_dfa | su_mfa) & (su_dfb | su_mfb), True).trim().recolor( color_scheme['*'] )
    
    for m in [maa,mca,m0c0,m0cad,m0cam,mma,m0cbd,m0cbm,m0cc,mcb,mab,mmb]:
        m.rebuild_edges()
    
    meshes = [
        #ma.copy(),m0.copy(),mb.copy(),
        #maa.copy().extend(mca), #maa.copy().extend(mda),
        mca.copy().extend(maa).extend(mma),
        m0c0.copy().extend(m0cad).extend(m0cam).extend(m0cbd).extend(m0cbm).extend(m0cc),
        mcb.copy().extend(mab).extend(mmb), #mab.copy().extend(mdb),
        ]
    
    shapes = [ mesh.toShape() if mesh else None for mesh in meshes ]
    window = Viewer( shapes, ncols=3, caption='MeshGit Diff3 Viewer: %s <- %s -> %s' % (fn1,fn0,fn2) )
    window.app_run()


def run_diff_viewer_series( lfn, method=None ):
    cm = len(lfn)
    lm = [ Mesh.fromPLY(fn) for fn in lfn ]
    
    lc01 = []
    ldv1 = []
    for i in xrange(len(lm)-1):
        print( 'comparing %i -> %i' % (i,i+1) )
        scaling_factor = scale( [ lm[i], lm[i+1] ] )
        lm[i+1] = get_corresponding_mesh1( build_correspondence( lm[i], lm[i+1], method ), allow_face_sub=False )
        scale( [ lm[i], lm[i+1] ], 1.0 / scaling_factor )
        lc01 += [ diff_commands( lm[i], lm[i+1] ) ]
    for i in xrange(len(lm)-1):
        scaling_factor = scale( [lm[i],lm[i+1]], scaling_factor='unitcube' )
        m0 = lm[i]
        su_v0 = set( m0.get_lu_v() )
        ldv1 += [ dict( [ ( v.u_v, Vec3f.t_distance(v.p,m0.opt_get_v(v.u_v).p) ) for v in lm[i+1].lv  if v.u_v in su_v0 ] ) ]
        scale( [ lm[i], lm[i+1] ], 1.0 / scaling_factor )
    
    #scale( lm, 1.0 / scaling_factor )
    for m in lm:
        m.optimize()
        m.rebuild_edges()
    
    #lc01 = [ diff_commands( m0, m1 ) for m0,m1 in zip(lm[:-1],lm[1:]) ]
    
    lsu_df = [ set([ cmd.split(' ')[1].split(':')[0] for cmd in c01 if cmd.startswith('delface') ]) for c01 in lc01 ]
    lsu_af = [ set([ cmd.split(' ')[1].split(':')[0] for cmd in c01 if cmd.startswith('addface') ]) for c01 in lc01 ]
    lsu_mv = [ set([ cmd.split(' ')[1].split(':')[0] for cmd in c01 if cmd.startswith('move') ]) for c01 in lc01 ]
    
    ls = []
    for i,m in enumerate(lm):
        su_df = lsu_df[i] if i < cm-1 else set()
        su_af = lsu_af[i-1] if i > 0 else set()
        su_mv = lsu_mv[i-1] if i > 0 else set()
        c01 = lc01[i] if i < cm-1 else []
        
        m.recolor( color_scheme['='] )
        
        md = m.clone()
        md.lf = [ f for f in md.lf if f.u_f in su_df ]
        md.trim()
        md.recolor( color_scheme['-'] )
        
        mc = m.clone().filter_faces( su_af | su_df, False ).trim().recolor( color_scheme['='] )
        md = m.clone().filter_faces( su_df - su_af, True ).trim().recolor( color_scheme['-'] )
        ma = m.clone().filter_faces( su_af - su_df, True ).trim().recolor( color_scheme['+'] )
        mad = m.clone().filter_faces( su_df & su_af, True ).trim(). recolor( color_scheme['/'] )
        if i > 0:
            for v in mc.lv:
                if v.u_v in su_mv:
                    val = min( 1.0, ldv1[i-1][v.u_v] * 25.0 )
                    v.c = Vec3f.t_interp( color_scheme['='], color_scheme['>'], val )
                    #v.c = color_scheme['>']
        mc.rebuild_edges()
        md.rebuild_edges()
        ma.rebuild_edges()
        mad.rebuild_edges()
        
        ls += [ mc.extend(md).extend(ma).extend(mad).toShape() ]
    
    window = Viewer( ls, caption='MeshGit Series Diff Viewer: %s' % ' -> '.join( lfn ) )
    window.app_run()


def run_build_and_save( fn0, fn1, fnsave, method=None ):
    m0,m1 = [ Mesh.fromPLY( fn ) for fn in [fn0,fn1] ]
    scaling_factor = scale( [ m0, m1 ] )
    ci = build_correspondence( m0, m1, method )
    m1 = get_corresponding_mesh1( ci )
    data = [ ci.mv01, ci.mf01 ]
    pickle.dump( data, open( fnsave, 'wb' ) )

def run_diff_viewer_saved( fn0, fn1, fnsave ):
    m0,m1 = [ Mesh.fromPLY( fn ) for fn in [fn0,fn1] ]
    mv01,mf01 = pickle.load( open( fnsave, 'rb' ) )
    
    ci = CorrespondenceInfo( m0, m1 )
    for iv0,iv1 in mv01.iteritems():
        ci.acv( iv0, iv1 )
    for if0,if1 in mf01.iteritems():
        ci.acf( if0, if1 )
    
    m1 = get_corresponding_mesh1( ci, allow_face_sub=False )
    
    for m in [m0,m1]:
        m.optimize()
        m.rebuild_edges()
    
    c01 = diff_commands( m0, m1 )
    su_df = set([ cmd.split(' ')[1].split(':')[0] for cmd in c01 if cmd.startswith('delface') ])
    su_af = set([ cmd.split(' ')[1].split(':')[0] for cmd in c01 if cmd.startswith('addface') ])
    su_mf = set([ cmd.split(' ')[1].split(':')[0] for cmd in c01 if cmd.startswith('modface') ])
    su_cf = set(m0.get_lu_f()) - su_df
    su_mv = set([ cmd.split(' ')[1].split(':')[0] for cmd in c01 if cmd.startswith('move') ])
    
    m0.recolor( color_scheme['='] )
    m1.recolor( color_scheme['='] )
    
    mc = m0.clone().filter_faces( su_cf, True ).trim()
    md = m0.clone().filter_faces( su_df, True ).trim().recolor( color_scheme['-'] )
    
    mc2 = m1.clone().filter_faces( su_cf, True ).trim()
    ma2 = m1.clone().filter_faces( su_af, True ).trim().recolor( color_scheme['+'] )
    
    for v in mc2.lv:
        if v.u_v in su_mv:
            v.c = color_scheme['>']
    
    mc.rebuild_edges()
    mc2.rebuild_edges()
    md.rebuild_edges()
    ma2.rebuild_edges()
    
    meshes = [ mc.copy().extend(md), mc2.copy().extend(ma2) ]
    shapes = [ mesh.toShape() for mesh in meshes ]
    window = Viewer( shapes, caption='MeshGit Diff-Saved Viewer: %s -> %s (%s)' % (fn0,fn1,fnsave) )
    window.app_run()



#######################################################################################################################

def run_adj_viewer( fn ):
    lc = [[0.0,0.0,0.0],[0.25,0.0,0.5],[0.0,0.0,0.9],[0.0,0.9,0.0],[0.75,0.75,0.0],[0.9,0.5,0.0],[1.0,0.0,0.0],[1.0,1.0,1.0]]
    
    m0 = Mesh.fromPLY( fn )
    m0.optimize()
    m1 = m0.clone().recolor_adj_face_count(lc)
    m2 = Mesh()
    for f in m0.lf:
        u_f = f.u_f
        c = lc[ min( len(lc)-1, len(m0._la_f[u_f]['si_f']) ) ]
        lv = [ m0.opt_get_v(u_v).copy().recolor(c) for u_v in f.lu_v ]
        lu_v = [ v.u_v for v in lv ]
        m2.lv += lv
        m2.lf += [ Face(u_f=u_f, lu_v=lu_v) ]
    
    shapes = [ mesh.toShape( smooth=False ) if mesh else None for mesh in [ m0,m1,m2 ] ]
    window = Viewer( shapes, caption='MeshGit Adjacency Viewer', ncols=3 )
    window.app_run()

def scale( lm, scaling_factor=None ):
    if scaling_factor == 'unitcube':
        md = 0.0
        for m in lm:
            if len(m.lv) == 0: continue
            vm = vM = m.lv[0].p
            for v in m.lv:
                vm = [ min(vm[i],v.p[i]) for i in xrange(3) ]
                vM = [ max(vM[i],v.p[i]) for i in xrange(3) ]
            bb = [ vM[0]-vm[0], vM[1]-vm[1], vM[2]-vm[2] ]
            md = max(md,max(bb))
        assert md > 0.0
        scaling_factor = 1.0 / md
    
    elif scaling_factor is None:
        
        ma = 0.0
        for m in lm:
            m.rebuild_edges()
            m.optimize()
            s = 0.0
            c = 0
            for e in m.le:
                u_v0,u_v1 = e.lu_v
                s += Vec3f.t_distance( m.opt_get_v(u_v0).p, m.opt_get_v(u_v1).p )
                c += 1
            ma = max(ma,s/float(c))
        assert c != 0
        
        scaling_factor = 1.0 / ma
    
    for m in lm:
        for v in m.lv:
            v.p = Vec3f.t_scale( v.p, scaling_factor )
    
    return scaling_factor


def run_align_viewer2( fn0, fn1, method=None ):
    m0,m1 = [ Mesh.fromPLY( fn ) for fn in [fn0,fn1] ]
    scaling_factor = scale( [ m0, m1 ] )
    m1 = get_corresponding_mesh1( build_correspondence( m0, m1, method ) )
    scale( [m0,m1], 1.0 / scaling_factor )    
    
    print( 'optimizing...' )
    for m in [m0,m1]: m.optimize()
    
    print( 'creating meshes...' )
    suv0,suv1 = set(m0.get_lu_v()),set(m1.get_lu_v())
    suf0,suf1 = set(m0.get_lu_f()),set(m1.get_lu_f())
    suvu,suvi = suv0|suv1, suv0&suv1
    sufu,sufi = suf0|suf1, suf0&suf1
    
    uvc = dict( (uv,get_color(uv,suv0,suv1,set())) for uv in suvu )
    ufc = dict( (uf,get_color(uf,suf0,suf1,set())) for uf in sufu )
    
    ma,mb = m0.clone(),m1.clone()
    for v in ma.lv: v.c = uvc[v.u_v]
    for v in mb.lv: v.c = uvc[v.u_v]
    ma.trim().recalc_vertex_normals()
    mb.trim().recalc_vertex_normals()
    
    md,me = Mesh(),Mesh()
    for f in m0.lf:
        u_f = f.u_f
        c = ufc[u_f]
        lv = [ m0.opt_get_v(u_v).copy().recolor(c) for u_v in f.lu_v ]
        lu_v = [ v.u_v for v in lv ]
        md.lv += lv
        md.lf += [ Face(u_f=u_f, lu_v=lu_v) ]
    for f in m1.lf:
        u_f = f.u_f
        c = ufc[u_f]
        lv = [ m1.opt_get_v(u_v).copy().recolor(c) for u_v in f.lu_v ]
        lu_v = [ v.u_v for v in lv ]
        me.lv += lv
        me.lf += [ Face(u_f=u_f, lu_v=lu_v) ]
    
    steps = 20
    mf = [ Mesh() for x in xrange(steps) ]
    cv = 0
    for u_f in sufi:
        f0,f1 = m0.opt_get_f(u_f),m1.opt_get_f(u_f)
        if len(f0.lu_v) != len(f1.lu_v): continue
        if not all( u_v0 in suv1 for u_v0 in f0.lu_v ): continue
        if not all( u_v1 in suv0 for u_v1 in f1.lu_v ): continue
        if set(f0.lu_v) != set(f1.lu_v): continue
        clv = len(f0.lu_v)
        l_v0 = m0.opt_get_uf_lv( u_f )
        l_v1 = m1.opt_get_uf_lv( u_f )
        for i,m in enumerate(mf):
            t = float(i) / float(steps-1)
            for j in xrange(clv):
                #p = Vec3f.t_add_between( l_v0[j].p, l_v1[j].p, t )
                #lp = Vec3f.t_distance( l_v0[j].p, l_v1[j].p )
                j1 = m1._get_i_v( l_v0[j].u_v )
                p = Vec3f.t_add_between( l_v0[j].p, m1.lv[j1].p, t )
                lp = Vec3f.t_distance( l_v0[j].p, m1.lv[j1].p ) * 0.5
                c = [ min(1.0,lp*2.5), min(1.0,lp*10.0), min(lp*25.0,1.0) ]
                m.lv.append( Vertex( p=p, c=c ) )
            m.lf.append( Face( lu_v=[ m.lv[j].u_v for j in xrange(cv,cv+clv) ]) )
        cv += clv
    for i in xrange(steps):
        mf[i].recalc_vertex_normals()
    
    shapes = [ mesh.toShape( smooth=False ) if mesh else None for mesh in [ ma,mb,None, md,me,None ] ]
    shapes[2] = Shape_Cycle( [ m.toShape( smooth=False ) for m in mf ], fps=5, bounce=True )
    window = Viewer( shapes, caption='MeshGit Align Viewer', ncols=3 )
    window.app_run()

def run_align_viewer3( fn0, fn1, fn2, method=None ):
    m0,m1,m2 = [ Mesh.fromPLY( fn ) for fn in [fn0,fn1,fn2] ]
    scaling_factor = scale( [ m0, m1, m2 ] )
    m1 = get_corresponding_mesh1( build_correspondence( m0, m1, method ) )
    m2 = get_corresponding_mesh1( build_correspondence( m0, m2, method ) )
    scale( [m0,m1,m2], 1.0 / scaling_factor )
    
    m0.optimize()
    m1.optimize()
    m2.optimize()
    
    suv0,suv1,suv2 = set(m0.get_lu_v()),set(m1.get_lu_v()),set(m2.get_lu_v())
    suf0,suf1,suf2 = set(m0.get_lu_f()),set(m1.get_lu_f()),set(m2.get_lu_f())
    suvu,suvi = suv0|suv1|suv2, suv0&suv1&suv2
    sufu,sufi = suf0|suf1|suf2, suf0&suf1&suf2
    
    uvc = dict( (uv,get_color(uv,suv0,suv1,suv2)) for uv in suvu )
    ufc = dict( (uf,get_color(uf,suf0,suf1,suf2)) for uf in sufu )
    
    ma,mb,mc = m0.clone(),m1.clone(),m2.clone()
    for v in ma.lv: v.c = uvc[v.u_v]
    for v in mb.lv: v.c = uvc[v.u_v]
    for v in mc.lv: v.c = uvc[v.u_v]
    ma.trim().recalc_vertex_normals()
    mb.trim().recalc_vertex_normals()
    mc.trim().recalc_vertex_normals()
    
    md,me,mf = Mesh(),Mesh(),Mesh()
    for f in m0.lf:
        u_f = f.u_f
        c = ufc[u_f]
        lv = [ m0.opt_get_v(u_v).copy().recolor(c) for u_v in f.lu_v ]
        lu_v = [ v.u_v for v in lv ]
        md.lv += lv
        md.lf += [ Face(u_f=u_f, lu_v=lu_v) ]
    for f in m1.lf:
        u_f = f.u_f
        c = ufc[u_f]
        lv = [ m1.opt_get_v(u_v).copy().recolor(c) for u_v in f.lu_v ]
        lu_v = [ v.u_v for v in lv ]
        me.lv += lv
        me.lf += [ Face(u_f=u_f, lu_v=lu_v) ]
    for f in m2.lf:
        u_f = f.u_f
        c = ufc[u_f]
        lv = [ m2.opt_get_v(u_v).copy().recolor(c) for u_v in f.lu_v ]
        lu_v = [ v.u_v for v in lv ]
        mf.lv += lv
        mf.lf += [ Face(u_f=u_f,lu_v=lu_v) ]
    #md.lf = [ f for f in m3.lf if 
    
    shapes = [ mesh.toShape( smooth=False ) if mesh else None for mesh in [ mb,ma,mc, me,md,mf ] ]
    window = Viewer( shapes, caption='MeshGit Align Viewer', ncols=3 )
    window.app_run()

def split_mesh( mesh, l_sufo ):
    return [ mesh.clone().filter_faces(su,fo) for su,fo in l_sufo ]

########################################################################################################################

kp = None
def run_merge_viewer( fn0, fn1, fn2, fn012, method=None ):
    global kp
    #m0 = Mesh.fromPLY( fn0 ).recolor( [0.5,0.5,0.5] )
    #ma = Mesh.bc_closestverts( m0, Mesh.fromPLY( fn1 ) ).recolor( [0.5,0.5,0.5] )
    #mb = Mesh.bc_closestverts( m0, Mesh.fromPLY( fn2 ) ).recolor( [0.5,0.5,0.5] )
    
    m0,ma,mb = [ Mesh.fromPLY( fn ) for fn in [fn0,fn1,fn2] ]
    scaling_factor = scale( [ m0, ma, mb ] )
    ci0a = build_correspondence( m0, ma, method )
    ci0b = build_correspondence( m0, mb, method )
    
    if ci0a.mismatch >= 5.0 or ci0b.mismatch >= 5.0:
        print( '' )
        print( '*' * 55 )
        print( '*' * 55 )
        if ci0a.mismatch >= 5.0:
            print( 'WARNING: Mismatch heuristic for mesh0 -> mesha is high!' )
        if ci0b.mismatch >= 5.0:
            print( 'WARNING: Mismatch heuristic for mesh0 -> meshb is high!' )
        print( '*' * 55 )
        print( '*' * 55 )
        print( '' )
    
    ma = get_corresponding_mesh1( ci0a, allow_face_sub=False )
    mb = get_corresponding_mesh1( ci0b, allow_face_sub=False )
    for m in [m0,ma,mb]:
        m.optimize()
        m.rebuild_edges()
    
    llc0av,llc0af = diff_commands_by_partition( m0, ma )
    llc0bv,llc0bf = diff_commands_by_partition( m0, mb )
    
    c0a,c0b = llc0av['uvm']+llc0av['uva'],llc0bv['uvm']+llc0bv['uva']
    #c0a.extend( lc0a['ufd']+lc0a['ufa'] if not any(lc0a['sf'] & lc0b['sf'] for lc0b in llc0b) for lc0a in llc0a )
    #c0b.extend( lc0b['ufd']+lc0b['ufa'] if not any(lc0a['sf'] & lc0b['sf'] for lc0a in llc0a) for lc0b in llc0b )
    
    # if two edits conflict, show to user
    w,h=len(llc0af),len(llc0bf)
    x,y=w-1,h-1
    
    mconflicts = [ [ 1 if (lc0a['sf'] & lc0b['sf']) else 0 for lc0b in llc0bf ] for lc0a in llc0af ]
    choosea = [ not any(lc0a['sf'] & lc0b['sf'] for lc0b in llc0bf) for lc0a in llc0af ]
    chooseb = [ not any(lc0a['sf'] & lc0b['sf'] for lc0a in llc0af) for lc0b in llc0bf ]
    
    m012 = m0.clone()  # create exact copy of m0 to edit
    do_commands( m012, llc0av['uva'] + llc0bv['uva'] )
    for i,lc0a in enumerate(llc0af):
        if not any( mconflicts[i] ):
            do_commands( m012, lc0a['ufd'] + lc0a['ufa'] + lc0a['ufm'] )
    for i,lc0b in enumerate(llc0bf):
        if not any( mconflicts[r][i] for r in xrange(w) ):
            do_commands( m012, lc0b['ufd'] + lc0b['ufa'] + lc0b['ufm'] )
    
    window = Viewer( None, ncols=3, caption='MeshGit Merge Viewer', on_key_callback=merge_on_key )
    window.modelscale = scaling_factor
    
    saveresult = True
    while any( any(r) for r in mconflicts ):
        x = (x + 1) % w
        if x == 0: y = (y + 1) % h
        
        if not mconflicts[x][y]: continue
        
        lc0a = llc0af[x]
        ma2 = do_commands( m0.clone(), lc0a['ufd']+llc0av['uvm'] ).trim()
        ma3 = do_commands( m0.clone(clone_faces=False), llc0av['uva']+lc0a['ufa']+llc0av['uvm'] ).trim().recolor([0.0,0.75,0.4])
        su_dfa = set([ cmd.split(' ')[1].split(':')[0] for cmd in lc0a['ufd'] ])
        su_afa = set([ cmd.split(' ')[1].split(':')[0] for cmd in lc0a['ufa'] ])
        su_cfa = set(m0.get_lu_f()) - su_dfa
        
        lc0b = llc0bf[y]
        mb2 = do_commands( m0.clone(), lc0b['ufd']+llc0bv['uvm'] ).trim()
        mb3 = do_commands( m0.clone(clone_faces=False), llc0bv['uva']+lc0b['ufa']+llc0bv['uvm'] ).trim().recolor([0.4,0.75,0.0])
        su_dfb = set([ cmd.split(' ')[1].split(':')[0] for cmd in lc0b['ufd'] ])
        su_afb = set([ cmd.split(' ')[1].split(':')[0] for cmd in lc0b['ufa'] ])
        su_cfb = set(m0.get_lu_f()) - su_dfb
            
        m0c0 = m0.clone().filter_faces( su_dfa | su_dfb, False ).trim()
        m0ca = m0.clone().filter_faces( su_dfa, True ).filter_faces( su_dfb, False ).trim().recolor([ 0.75, 0.00, 0.40 ])
        m0cb = m0.clone().filter_faces( su_dfb, True ).filter_faces( su_dfa, False ).trim().recolor([ 0.75, 0.40, 0.00 ])
        
        m0cc0 = m0.clone().filter_faces( su_dfa & su_dfb, True ).trim().recolor([ 0.50, 0.00, 0.00 ])
        m0cc1 = m0.clone().filter_faces( su_dfa & su_dfb, True ).trim().recolor([ 0.60, 0.22, 0.00 ])
        m0cc2 = m0.clone().filter_faces( su_dfa & su_dfb, True ).trim().recolor([ 0.70, 0.45, 0.00 ])
        m0cc3 = m0.clone().filter_faces( su_dfa & su_dfb, True ).trim().recolor([ 0.80, 0.67, 0.00 ])
        m0cc4 = m0.clone().filter_faces( su_dfa & su_dfb, True ).trim().recolor([ 0.90, 0.90, 0.00 ])
        
        for m in [m0ca,m0cb]:
            m.rebuild_edges()
        
        m0c0b = m0c0.extend(m0ca).extend(m0cb)
        
        for m in [ma2,ma3,mb2,mb3,m0c0b,m0cc0,m0cc1,m0cc2,m0cc3,m0cc4]:
            m.rebuild_edges()
        
        
        shapes = [
            None, m012.toShape(), None,
            ma2.extend(ma3).toShape(),
            Shape_Cycle( [
                m0c0b.clone().extend(m0cc0).toShape(),
                m0c0b.clone().extend(m0cc1).toShape(),
                m0c0b.clone().extend(m0cc2).toShape(),
                m0c0b.clone().extend(m0cc3).toShape(),
                m0c0b.clone().extend(m0cc4).toShape(),
                ], 20, True ),
            mb2.extend(mb3).toShape()
            ]
        
        window.setup( shapes, ncols=3 )
        window.app_run()
        if window.has_exit or window.context is None:
            print( 'quitting!' )
            return 1
        
        if kp == 1:
            print( 'ver a' )
            choosea[x] = True
            do_commands( m012, llc0af[x]['ufd'] + llc0af[x]['ufa'] )
            for i in xrange(h):
                if mconflicts[x][i] == 0: continue
                mconflicts[x][i] = 0
            for i in xrange(w):
                if mconflicts[i][y] == 0: continue
                mconflicts[i][y] = 0
                if not any(mconflicts[i][i2] for i2 in xrange(h)):
                    choosea[i] = True
                    do_commands( m012, llc0af[i]['ufd'] + llc0af[i]['ufa'] )
        
        elif kp == 2:
            print( 'ver b' )
            chooseb[y] = True
            do_commands( m012, llc0bf[y]['ufd'] + llc0bf[y]['ufa'] )
            for i in xrange(w):
                if mconflicts[i][y] == 0: continue
                mconflicts[i][y] = 0
            for i in xrange(h):
                if mconflicts[x][i] == 0: continue
                mconflicts[x][i] = 0
                if not any(mconflicts[i2][i] for i2 in xrange(w)):
                    chooseb[i] = True
                    do_commands( m012, llc0bf[i]['ufd'] + llc0bf[i]['ufa'] )
        
        elif kp == 3:
            print( 'ver 0' )
            for i in xrange(w): mconflicts[i][y] = 0
            for i in xrange(h): mconflicts[x][i] = 0
            #for i in xrange(w):
                #if mconflicts[i][y] == 0: continue
                #if not any(mconflicts[i][i2] for i2 in xrange(h)):
                    #choosea[i] = True
                    #do_commands( m012, llc0af[i]['ufd'] + llc0af[i]['ufa'] )
            #for i in xrange(h):
                #if mconflicts[x][i] == 0: continue
                #if not any(mconflicts[i2][i] for i2 in xrange(w)):
                    #chooseb[i] = True
                    #do_commands( m012, llc0bf[i]['ufd'] + llc0bf[i]['ufa'] )
        
        elif kp == 4:
            print( 'merge-by-hand' )
            
            m0_ = m0.clone()
            m0a_ = m0.clone()
            m0b_ = m0.clone()
            m0ab = m0.clone()
            for lc0a in llc0af:
                m0_ = do_commands( m0_, lc0a['ufd'] )
                m0a_ = do_commands( m0a_, lc0a['ufd'] )
            for lc0b in llc0bf:
                m0_ = do_commands( m0_, lc0b['ufd'] )
                m0b_ = do_commands( m0b_, lc0b['ufd'] )
            m0_.trim()
            m0a_.trim()
            m0b_.trim()
            
            ma_ = ma.clone( clone_faces=False )
            ma_ = do_commands( ma_, llc0av['uva'] )
            m0ab = do_commands( m0ab, llc0av['uva'] )
            for lc0a in llc0af:
                ma_ = do_commands( ma_, lc0a['ufa'] )
                m0ab = do_commands( m0ab, lc0a['ufa'] )
            ma_.trim()
            
            mb_ = mb.clone( clone_faces=False )
            mb_ = do_commands( mb_, llc0bv['uva'] )
            m0ab = do_commands( m0ab, llc0bv['uva'] )
            for lc0b in llc0bf:
                mb_ = do_commands( mb_, lc0b['ufa'] )
                m0ab = do_commands( m0ab, lc0b['ufa'] )
            mb_.trim()
            
            m0ab.trim()
            
            scale( [m0,ma,mb,m0_,ma_,mb_,m0a_,m0b_,m0ab], 1.0/scaling_factor )
            m0.toPLY( 'm0.ply' )
            ma.toPLY( 'ma.ply' )
            mb.toPLY( 'mb.ply' )
            m0_.toPLY( 'm0_.ply' )
            ma_.toPLY( 'ma_.ply' )
            mb_.toPLY( 'mb_.ply' )
            m0a_.toPLY( 'm0a_.ply' )
            m0b_.toPLY( 'm0b_.ply' )
            m0ab.toPLY( 'm0ab.ply' )
            
            if os.path.exists( 'm0ab_.ply' ):
                os.unlink( 'm0ab_.ply' )
            
            subprocess.Popen( ['./merge_blender.sh'] ).communicate()
            
            if os.path.exists( 'm0ab_.ply' ):
                m012 = Mesh.fromPLY( 'm0ab_.ply' )
                saveresult = False
                break
            else:
                x -= 1
                scale( [m0,ma,mb], scaling_factor )
        
        else:
            print( 'skipping' )
    
    if kp != 4:
        for c,lc0a in zip(choosea,llc0af):
            if not c: continue
            c0a.extend( lc0a['ufd']+lc0a['ufa'] )
        for c,lc0b in zip(chooseb,llc0bf):
            if not c: continue
            c0b.extend( lc0b['ufd']+lc0b['ufa'] )
        #c0a.extend( llc0av['uvd'] )
        #c0b.extend( llc0bv['uvd'] )
        
        # apply diffs to ancestor
        m012 = m0.clone()  # create exact copy of m0 to edit
        do_commands( m012, c0a )
        do_commands( m012, c0b )
        m012.trim()
        
        scale( [m012], 1.0/scaling_factor )
    
    m012.toPLY( fn012 ) # save merged mesh to file
    
    suvm = set([ cmd.split(' ')[1].split(':')[0] for cmd in (llc0av['uvm'] + llc0bv['uvm']) if cmd.startswith('move') ])
    
    scale( [m0,ma,mb], 1.0 / scaling_factor )
    
    scaling_factor = scale( [m0,m012], scaling_factor='unitcube' )
    su_v0 = set( m0.get_lu_v() )
    dvd = dict( [ ( v.u_v, Vec3f.t_distance(v.p,m0.opt_get_v(v.u_v).p) ) for v in m012.lv if v.u_v in su_v0 ] )
    scale( [m0,m012], 1.0 / scaling_factor )
    
    suf0,sufa,sufb = set(m0.get_lu_f()),set(ma.get_lu_f()),set(mb.get_lu_f())
    m012_0,m012_a,m012_b = [ m012.clone() for i in xrange(3) ]
    m012_0.filter_faces(suf0,True).trim().optimize()
    for v in m012_0.lv:
        if v.u_v in suvm:
            val = min( 1.0, dvd[v.u_v] * 25.0 )
            v.c = Vec3f.t_interp( color_scheme['='], color_scheme['>'], val )
        else:
            v.c = color_scheme['=']
    m012_a.filter_faces(sufa-suf0,True).trim().recolor( color_scheme['+a'] ).optimize()
    m012_b.filter_faces(sufb-suf0,True).trim().recolor( color_scheme['+b'] ).optimize()
    m012_ = m012_0.extend(m012_a,ignore_edges=True).extend(m012_b,ignore_edges=True)
    m012_.trim().recalc_vertex_normals().optimize()
    
    
    window.setup( [
            None, m012.toShape(), m012_.toShape(),
            ma.toShape(), m0.toShape(), mb.toShape()
        ], ncols = 3 )
    window.app_run()
    if not (window.has_exit or window.context is None):
        window.close()
    
    return 0

def merge_on_key( key ):
    global kp
    if key == '_1':     # ver a
        kp = 1
        return 1
    elif key == '_2':   # ver b
        kp = 2
        return 1
    elif key == '_3':   # ver 0 (neither)
        kp = 3
        return 1
    elif key == '_4':   # merge by hand
        kp = 4
        return 1
    elif key == '_0':   # skip
        kp = 5
        return 1
    kp = None
    return 0

########################################################################################################################

def run_view( lfn ):
    lm = [ Mesh.fromPLY(fn) for fn in lfn ]
    #scaling_factor = scale( lm )
    print( 'stats:' )
    for fn,m in zip(lfn,lm):
        print( '  %05d %05d: %s' % (len(m.lv),len(m.lf),fn) )
    #ls = [ m.recalc_vertex_normals().toShape(smooth=True) for m in lm ]
    ls = [ m.recalc_vertex_normals().toShape(smooth=False) for m in lm ]
    window = Viewer( ls, caption='MeshGit Viewer' )
    window.app_run()

def run_warp_view( fn0, fn1 ):
    m0,m1 = Mesh.fromPLY(fn0),Mesh.fromPLY(fn1)
    m2,m3 = warp_meshes( m0, m1 )
    ls = [ m.recalc_vertex_normals().toShape(smooth=False) for m in [m0,m1,m2,m3] ]
    #ls = [ m.recalc_vertex_normals().toShape(smooth=False) for m in [m1,m2] ]
    window = Viewer( ls, caption='MeshGit Viewer (Warped)' )
    window.app_run()

########################################################################################################################

def run_strips_align( fn0, fn1 ):
    m0,m1 = [ Mesh.fromPLY( fn ) for fn in [fn0,fn1] ]
    m0.optimize()
    m1.optimize()
    stats0,stats1 = get_mesh_stats(m0), get_mesh_stats(m1)
    adj_e0,adj_f0,mra0,lring_sf0 = [ stats0[k] for k in ['adj_e','adj_f','mra','lring_sf'] ]
    adj_e1,adj_f1,mra1,lring_sf1 = [ stats1[k] for k in ['adj_e','adj_f','mra','lring_sf'] ]
    len0,len1 = len(lring_sf0),len(lring_sf1)
    
    lluv0 = [ [ Vertex.average( m0.opt_get_uf_lv(u_f) ) for u_f in ring_sf0 ] for ring_sf0 in lring_sf0 ]
    lluv1 = [ [ Vertex.average( m1.opt_get_uf_lv(u_f) ) for u_f in ring_sf1 ] for ring_sf1 in lring_sf1 ]
    
    lrc0 = [ [1.0,0.0,0.0] for r in lring_sf0 ]
    lrc1 = [ [0.0,1.0,0.0] for r in lring_sf1 ]
    
    m0.optimize()
    m1.optimize()
    
    print( 'aligning strips...' )
    align = [None]*len0
    
    if False:
        print( '  computing cost matrix...' )
        matcosts = [[0.0]*(len0+len1) for i in xrange(len0+len1)]
        # Q1
        for i in xrange(len1):
            for j in xrange(len1):
                matcosts[i+len0][j] = 15.0 * len(lring_sf1[j]) if i == j else float('inf')
        # Q3
        for i in xrange(len0):
            for j in xrange(len0):
                matcosts[i][j+len1] = 15.0 * len(lring_sf0[i]) if i == j else float('inf')
        # Q4 : do nothing
        # Q2
        t = time.time()
        pl = -1.0
        for i,luv0 in enumerate(lluv0):
            pc = float(i)/float(len0)*100.0
            if int(pl/5.0) != int(pc/5.0):
                td = time.time()-t
                print( '    %02i%% (%.2fm, eta %.2fm)...' % (pc,td,(td*100.0/pc-td) if pc > 0.0 else -1.0) )
                pl = pc
            # get verts of ring
            for j,luv1 in enumerate(lluv1):
                d = 0.0
                #d += abs(len(lring_sf0[i])-len(lring_sf1[j]))
                d += abs(sum(mra0[i])-sum(mra1[j])) * 4.0
                
                if abs(len(luv0)-len(luv1)) > 20 or d > 20.0:
                    d = float('inf')
                else:
                    d += compute_cost(
                        len(luv0), len(luv1),
                        lambda _i: 1.0,
                        lambda _i,_j: Vec3f.t_distance2( luv0[_i], luv1[_j] ),          # TODO: incl star cost!
                        lambda _j: 1.0
                        ) * 5.0
                
                matcosts[i][j] = d
        
        
        print( "  running..." )
        t = time.time()
        inds = hungarian( numpy.array( matcosts ) )
        print( "    finished in %f" % (time.time()-t) )
        
        for i,j in enumerate(inds):
            if j >= len1: continue
            if i >= len0: continue
            align[i] = j
            lrc0[i] = lrc1[j] = random_color()
    else:
        fp = open( 'align','rt' )
        for i,l in enumerate(fp.readlines()):
            if i >= len0: continue
            j = int(l)
            if j == -1: continue
            align[i] = j
            lrc0[i] = lrc1[j] = random_color()
    
    print( '  meshing...' )
    ma,mb = Mesh(),Mesh()
    #dfc0 = dict( (f.u_f,[1.0,1.0,1.0]) for f in m0.lf )
    #dfc1 = dict( (f.u_f,[1.0,1.0,1.0]) for f in m1.lf )
    dfc0 = dict( (f.u_f,[0.0,0.0,0.0]) for f in m0.lf )
    dfc1 = dict( (f.u_f,[0.0,0.0,0.0]) for f in m1.lf )
    
    for i,ring_sf in enumerate(lring_sf0):
        c = lrc0[i]
        for u_f in ring_sf:
            #dfc0[u_f][0] *= c[0]
            #dfc0[u_f][1] *= c[1]
            #dfc0[u_f][2] *= c[2]
            dfc0[u_f][0] += c[0] * 0.5
            dfc0[u_f][1] += c[1] * 0.5
            dfc0[u_f][2] += c[2] * 0.5
    for f in m0.lf:
        lv = [ m0.get_v(u_v).copy().recolor( dfc0[f.u_f] ) for u_v in f.lu_v ]
        ma.lv += lv
        ma.lf.append( Face(u_f=f.u_f,lu_v=[ v.u_v for v in lv ]) )
    
    for i,ring_sf in enumerate(lring_sf1):
        c = lrc1[i]
        for u_f in ring_sf:
            #dfc1[u_f][0] *= c[0]
            #dfc1[u_f][1] *= c[1]
            #dfc1[u_f][2] *= c[2]
            dfc1[u_f][0] += c[0] * 0.5
            dfc1[u_f][1] += c[1] * 0.5
            dfc1[u_f][2] += c[2] * 0.5
    for f in m1.lf:
        lv = [ m1.get_v(u_v).copy().recolor( dfc1[f.u_f] ) for u_v in f.lu_v ]
        mb.lv += lv
        mb.lf.append( Face(u_f=f.u_f,lu_v=[ v.u_v for v in lv ]) )
    
    shapes = [ mesh.toShape() for mesh in [ma,mb] ]
    window = Viewer( shapes, caption='MeshGit Aligned Strips Viewer' )
    window.app_run()

def run_strips_viewer( fn ):
    m = Mesh.fromPLY( fn )
    stats = get_mesh_stats( m )
    adj_e,adj_f,mra,lring_sf = [ stats[k] for k in ['adj_e','adj_f','mra','lring_sf'] ]
    
    lrc = [ random_color() for r in lring_sf ]
    
    m0 = Mesh()
    dfc = dict( (f.u_f,[1.0,1.0,1.0]) for f in m.lf )
    for i,ring_sf in enumerate(lring_sf):
        c = lrc[i]
        for u_f in ring_sf:
            dfc[u_f][0] *= c[0]
            dfc[u_f][1] *= c[1]
            dfc[u_f][2] *= c[2]
    for f in m.lf:
        lv = [ m.get_v(u_v).copy().recolor( dfc[f.u_f] ) for u_v in f.lu_v ]
        m0.lv += lv
        m0.lf += [ Face(u_f=f.u_f,lu_v=[ v.u_v for v in lv ]) ]
    
    meshes = []
    rtouched = [False for i in xrange(len(lring_sf))]
    while not all(rtouched):
        mesh = Mesh()
        ftouched = dict( (f.u_f,False) for f in m.lf )
        for ir in xrange(len(lring_sf)):
            if rtouched[ir]: continue
            liw = [ir]
            while liw:
                liw = sorted(liw,key=lambda x:len(lring_sf[x]))
                i = liw.pop()
                #if rtouched[i]: continue
                if any( ftouched[u_f] for u_f in lring_sf[i] ): continue
                rtouched[i] = True
                c = lrc[i]
                for u_f in lring_sf[i]:
                    ftouched[u_f] = True
                    f = m.get_f(u_f)
                    lv = [ m.get_v(u_v).copy().recolor(c) for u_v in f.lu_v ]
                    mesh.lv += lv
                    mesh.lf += [ Face(lu_v=[ v.u_v for v in lv ]) ]
                liw.extend( [j for j in xrange(len(lring_sf)) if mra[i][j]] )
        meshes += [mesh]
    
    #shapes = [ m0.toShape(), Shape_Cycle( [ mesh.toShape() for mesh in meshes ], fps=1.0, bounce=False ) ]
    shapes = [ mesh.toShape() for mesh in [m0]+meshes ]
    window = Viewer( shapes, caption='MeshGit Strips Viewer', ncols=5 )
    window.app_run()
    
    #print( '-' * 80 )
    
    if False:
        print( 'graph = ximport("graph")' )
        print( 'g = graph.create(iterations=1000, distance=1.0, layout="spring")' )
        print( 'g.styles.default.fontsize = 10' )
        print( 'g.styles.default.align = CENTER' )
        for i,ring_sf in enumerate(lring_sf):
            print( 'g.add_node("R%03i",label="R%03i:%i,%i",radius=5.00)' % (i,i,len(ring_sf),sum(mra[i])) )
        for i in xrange(len(lring_sf)):
            for j in xrange(i+1,len(lring_sf)):
                if not mra[i][j]: continue
                print( 'g.add_edge("R%03i", "R%03i", length=%f, label="*")' % (i,j,random.uniform(50.0,200.0)) )
        print( 'g.layout.reset()' )
        print( 'g.solve()' )
        print( 'g.draw()' )
        print( '#graph._ctx.canvas.save("file.pdf")' )
        print( '-'*80 )
    


########################################################################################################################
# main
########################################################################################################################


def print_usage( n ):
    print( 'Usage: %s [cmd] ...' % n )
    print( 'Command   : Args' )
    print( '-' * 80 )
    print( '   help      :' )
    print( '               Prints this help out to console' )
    print( '   view      : [fn0] <fn1> <...>' )
    print( '               Views the fni files' )
    print( '   merge     : [fn0] [fn1] [fn2] [fn012]' )
    print( '               Merges edits from fn0->fn1 and fn0->fn2, saving to fn012' )
    print( '   alignview : [fn0] [fn1] <fn2> <method=[method]>' )
    print( '               Computes and displays the alignment fn0->fn1 and fn0->fn2 if specified' )
    print( '   diff      : [fn0] [fn1] [fnd] [fnc] [fna]' )
    print( '               Computes changes from fn0->fn1, saving to dels to fnd, chgs to fnc, and adds to fna' )
    print( '   diffview  : [fn0] [fn1] <fn2> <method=[method]>' )
    print( '               Displays the difference from fn0->fn1 and fn0->fn2 if specified' )
    print( '   mergeview : [fn0] [fn1] [fn2] [fn012] <method=[method]>' )
    print( '               Merges edits from fn0->fn1 and fn0->fn2, saving to fn012' )
    print( '   strips    : [fn]' )
    print( '               EXPERIMENTAL: Computes strip-graph of fn' )
    print( '   alstrips  : [fn0] [fn1]' )
    print( '               EXPERIMENTAL' )
    print( '   adjview   : [fn] ')


if __name__ == '__main__':
    if len(sys.argv) == 1:
        print_usage( sys.argv[0] )
        exit()
    
    if sys.argv[1] == 'help':
        print_usage( sys.argv[0] )
        exit()
    
    elif sys.argv[1] == 'view':
        if len(sys.argv) < 3:
            print( 'Incorrect number of args for view' )
            exit()
        run_view( sys.argv[2:] )
        exit()
    
    elif sys.argv[1] == 'warp':
        if len(sys.argv) != 4:
            print( 'Incorrect number of args for warp' )
            exit()
        run_warp_view( sys.argv[2], sys.argv[3] )
    
    elif sys.argv[1] == 'strips':
        if len(sys.argv) != 3:
            print( 'Incorrect number of args for strips' )
            exit()
        run_strips_viewer( sys.argv[2] )
        exit()
    
    elif sys.argv[1] == 'alstrips':
        if len(sys.argv) != 4:
            print( 'Incorrect number of args for alstrips' )
            exit()
        run_strips_align( sys.argv[2], sys.argv[3] )
        exit()
    
    elif sys.argv[1] == 'merge':
        if len(sys.argv) != 6:
            print( 'Incorrect number of args for merge' )
            exit()
        run_merge( sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5] )
        exit()
    
    elif sys.argv[1] == 'diff':
        if len(sys.argv) != 7:
            print( 'Incorrect number of args for diff' )
            exit()
        run_diff( sys.argv[2], sys.argv[3], sys.argv[4], sys.argv[5], sys.argv[6] )
        exit()
    
    elif sys.argv[1] == 'iterdiff':
        if len(sys.argv) not in [4,5]:
            print( 'Incorrect number of args for iterdiff' )
            exit(1)
        lfn = [ a for a in sys.argv[2:] if not a.startswith('method=') ]
        method = ([ a.replace('method=','') for a in sys.argv[2:] if a.startswith('method=') ] + [None])[0]
        run_diff_viewer2_iter( lfn[0], lfn[1], method=method )
        exit()
        
    elif sys.argv[1] == 'seriesdiff':
        if len(sys.argv) < 3:
            print( 'Incorrect number of args for seriesdiff' )
            exit(1)
        
        lfn = [ a for a in sys.argv[2:] if not a.startswith('method=') ]
        method = ([ a.replace('method=','') for a in sys.argv[2:] if a.startswith('method=') ] + [None])[0]
        
        run_diff_viewer_series( lfn, method=method )
        exit()
    
    elif sys.argv[1] == 'alignview':
        if len(sys.argv) not in [4,5,6]:
            print( 'Incorrect number of args for alignview' )
            exit()
        
        lfn = [ a for a in sys.argv[2:] if not a.startswith('method=') ]
        method = ([ a.replace('method=','') for a in sys.argv[2:] if a.startswith('method=') ] + [None])[0]
        
        if len(lfn) == 2:
            run_align_viewer2( lfn[0], lfn[1], method=method )
        else:
            run_align_viewer3( lfn[0], lfn[1], lfn[2], method=method )
        exit()
        
    elif sys.argv[1] == 'diffview':
        if len(sys.argv) not in [4,5,6]:
            print( 'Incorrect number of args for diffview' )
            exit()
        
        lfn = [ a for a in sys.argv[2:] if not a.startswith('method=') ]
        method = ([ a.replace('method=','') for a in sys.argv[2:] if a.startswith('method=') ] + [None])[0]
        
        if len(lfn) == 2:
            run_diff_viewer2( lfn[0], lfn[1], method=method )
        else:
            run_diff_viewer3( lfn[0], lfn[1], lfn[2], method=method )
        
        exit()
    
    elif sys.argv[1] == 'buildandsave':
        if len(sys.argv) not in [5,6]:
            print( 'Incorrect number of args for buildandsave' )
            exit()
        
        lfn = [ a for a in sys.argv[2:] if not a.startswith('method=') ]
        method = ([ a.replace('method=','') for a in sys.argv[2:] if a.startswith('method=') ] + [None])[0]
        
        run_build_and_save( lfn[0], lfn[1], lfn[2], method=method )
        
        exit()
    
    elif sys.argv[1] == 'diffsaved':
        if len(sys.argv) != 5:
            print( 'Incorrect number of args for diffsaved' )
            exit()
        
        run_diff_viewer_saved( sys.argv[2], sys.argv[3], sys.argv[4] )
        
        exit()
    
    elif sys.argv[1] == 'mergeview':
        if len(sys.argv) not in [6,7]:
            print( 'Incorrect number of args for mergeview' )
            exit()
        
        lfn = [ a for a in sys.argv[2:] if not a.startswith('method=') ]
        method = ([ a.replace('method=','') for a in sys.argv[2:] if a.startswith('method=') ] + [None])[0]
        
        if len(lfn) != 4:
            print( 'Incorrect number of args for mergeview' )
            exit()
        
        sts = run_merge_viewer( lfn[0], lfn[1], lfn[2], lfn[3], method )
        exit(sts)
    
    elif sys.argv[1] == 'git-diff':
        # path old-file old-hex old-mode new-file new-hex new-mode
        
        if len(sys.argv) != 10:
            print( 'Incorrect number of args for git-diff' )
            exit()
        
        p = sys.argv[9]
        f0 = os.path.join(p,sys.argv[3])
        f1 = os.path.join(p,sys.argv[6])
        run_diff_viewer2( f0, f1 )
        exit()
        
    elif sys.argv[1] == 'git-merge':
        if len(sys.argv) != 6:
            print( 'Incorrect number of args for git-merge' )
            exit()
        
        p = sys.argv[5]
        f0 = os.path.join(p,sys.argv[2])
        fa = os.path.join(p,sys.argv[3])
        fb = os.path.join(p,sys.argv[4])
        sts = run_merge_viewer( f0, fa, fb, fa )
        exit(sts)
    
    elif sys.argv[1] == 'adjview':
        if len(sys.argv) != 3:
            print( 'Incorrect number of args for adjview' )
            exit()
        run_adj_viewer( sys.argv[2] )
        exit()
    
    print( 'Unknown command: %s' % sys.argv[1] )
    exit()

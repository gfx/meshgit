% !TEX root =  meshgit.tex

\section{Diffing and Merging Polygonal Meshes} \label{sect:tools}

\paragraph{Mesh Edit Operations.}
The mesh edit distance is computed by determining a set of mesh editing operations that transform a mesh into another. In this section, we describe how these sets of operations can be used to visualize the difference between meshes and merge edits together.
We visualize the mesh differences similarly to text diffs. In order to provide as much context as possible, we display all versions of the meshes side-by-side with vertices or faces colored to indicate the mesh edit operations. We have experimented with many color schemes, and report here the one we found the most informative.

\subsection{Mesh Diff}

\paragraph{Two-way Diff.}
In a two-way diff, \eg{} \fig{fig:teaser}.a, the original mesh $M$ is displayed on the left and the derivative mesh $M'$ on the right.
Face deletions ($\opd{f}$) are indicated by coloring red the corresponding face $f$ in $M$.  Face additions ($\opa{f'}$) are indicated by coloring green the face $f'$ in $M'$.  Face substitutions ($\opc{f}{f'}$) are only highlighted if changes to vertex adjacency happen; in this case, we mark $f$ in $M$ in red and $f'$ in $M'$ in green, similarly to face additions and deletion. 
In our visualizations, we simplify the presentation by not drawing the vertices directly.  Instead, the color of a vertex is linearly interpolated across the adjacent faces, unless the face has been colored red or green.  A vertex $v'$ in $M'$ is colored blue if the vertex has moved during the substitution.  The saturation of blue indicates the strength of the geometric change, with gray indicating no change.

\paragraph{Three-way Diff.}
When a mesh $M$ has two derived versions, $M^a$ and $M^b$, a three-way mesh diff can illustrate the changes between the derivatives and the original, allowing for a comparison of the two sets of edits.  The three meshes are shown side-by-side, with the original $M$ in the middle, $M^a$ on the left and $M^b$ on the right.
We use a similar color scheme as with a two-way diff, but the brightness of the color indicates from which derivative the operation comes: light red and green are for $M^a$, and dark red and green are for $M^b$.
When both derivatives modify the same face $f$ in $M$ by deleting it or changing the vertex adjacency, the overlapping change is indicated by coloring yellow $f$ in $M$.

\paragraph{Series Diff.}
An artist can also use \MeshGit{} to visualize the progression of work on a mesh, as shown in \fig{fig:diff_durano_series}. In this example, the mesh was constructed as a series of twelve snapshots, starting from the head, then working the body, and then adding the tail, arms, wings, and finally some extra details. Each snapshot is visualized similarly to a three-way diff.  A face $f$ in $M$ is colored green if it was added to the current snapshot or changed from the previous, red if it is deleted in the next snapshot or changed, and orange if the face was added and then deleted or changed both times. An alternative approach to visualizing mesh construction sequences is demonstrated in \emph{MeshFlow} \cite{meshflow}, that while providing a richer display, also requires full instrumentation of the modeling software.

\subsection{Mesh Edit Merge}

\parasection{Merging Editing Operations.}
Given an original mesh $M$ and two derivative meshes $M^a$ and $M^b$, one may wish to incorporate the changes made in both derivatives into a single resulting mesh. For example, in \fig{fig:teaser}.b, one derivative has finger nails added to the hand, while the other has refined and sculpted the palm. Presently, the only way to merge mesh edits such as these is for an artist to determine the changes done and then manually perform the modifications.

\paragraph{Merging Workflow.}
\MeshGit{} supports a merging workflow similar to text editing. We first compute the two sets of editing operations required to transform $M$ into $M^a$ and $M^b$ respectively. If the two sets of editing operations do not change the \meshtopo{} of the same parts of the original mesh, or move the same vertices, \MeshGit{} merges them automatically by simply performing all operations. However, if the sets of operations \emph{overlap} on the original, then they are in conflict. In this case, it is unclear how to merge the edits automatically while respecting artists intentions. For this reason, we follow text editing workflows, and ask the user to either choose which set of operations to apply or merge the conflict by hand.

\paragraph{Merging Non-Conflicting Edits.}
An example of our automatic merging is shown in \fig{fig:teaser}, where the changes do not overlap in the original mesh. In this case, \MeshGit{} merges the changes automatically. Another example is shown in \fig{fig:shaolin}. In one version the skirt is shortened, thus changing the \meshtopo{}, while in the other the body is sculpted by moving vertices.  It should be noted that the original mesh was entirely connected, not made of disconnected components. These changes can be safely merged by our algorithm, since the sculpting does not change the \meshtopo{}, and therefore the two sets are not conflicting.

\input{fig-shaolin}

\paragraph{Reducing Conflicts}
In our previous definition, if even a single operation is in conflict, none of the edits can be safely applied. We could ask the user to resolve the conflict by picking single operations from each set, a situation that would be obviously too cumbersome. To reduce the number of conflicts and reduce the granularity of users' decisions, we partition editing operations into groups that can be safely applied individually. This is akin to grouping text characters into line in text merging. The key observation is that edits cause \meshtopo{} changes over boundaries where changing substitutions occur. An edge is on the boundary if two of its adjacent faces undergo different operations, \ie{} deleted, added, substituted with a change in the vertex adjacency, or just substituted. We partition the sets of editing operations along these boundaries, essentially grouping edits in small non-intersecting patches. We then detect conflicts between the revisions at the granularity of these patches and ask users to resolve the conflicts at the same granularity.

Figure~\fignum{fig:ufo} shows an example with a conflicting edit on a spaceship model. In one version, the spaceship's top if refined, and detail is added to the body. In the other, details are added to the body and feet to the bottom. In this case, \MeshGit successfully detects conflicts to the body and merges all other changes automatically. To resolve the conflicts, the user can pick which version of edits to apply and use \MeshGit to properly apply the edits, as shown in the figure, or simply resolve the conflict manually.

\input{fig-diff-station}

\subsection{Performace}

We tested \MeshGit{} on a variety of meshes, shown throughout the paper, running our implementation on a quad-core 2.93GHz Intel Core i7 with 16GB RAM and an ATI Radeon HD 5750 graphics card. All of the meshes and source code are available as supplemental material.

Our implementation takes as input meshes containing vertex positions and the vertex indices for the faces. We chose meshes from difference sources and when available included all versions. The \emph{durano} \cite{model:durano} models are a series of saved snapshots taken through the mesh construction history. The \emph{sintel} \cite{model:sintel} meshes are a set of mesh variations, where there is no clear original mesh. The \emph{chair} \cite{model:chairs} are an ancestor mesh and one derivative mesh. For the \emph{hand} \cite{model:malebase}, \emph{shaolin} \cite{model:shaolin}, and \emph{spaceship} we model two derivative meshes from the original one to demonstrate merging.     

As summarized in \tbl{tbl:modelstats}, the number of faces and vertices of the tested meshes vary widely from hundreds to thousands. For the larger meshes, most of the time is spent computing the mesh edit distance. As also reported in the table, \MeshGit took from a few seconds to a few minutes to perform all operations in our implementation. This provides a practical solution for typical modeling workflows. We further expect that these timings to be significantly improved by a more optimized implementation of our code.

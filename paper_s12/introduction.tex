% !TEX root =  meshgit.tex

\section{Introduction} \label{sec:intro}

\paragraph{Version Control.}
When managing digital files, version control greatly simplifies the work of individuals
and is indispensable for collaborative work.
Version control systems such as Subversion \cite{svn} and Git \cite{git} have a large variety of features.
For text files, the features that have the most impact on workflow are the ability to store multiple versions of files, 
to visually compare, \ie{} diff, the content of two revisions, and to merge the changes of two revisions into a final one.
For 3D graphics files, version control is commonly used to maintain multiple versions of scene files, but artists are not able to 
diff and merge most scene data.

We focus on polygonal meshes used in today's subdivision modeling workflows, for which there is no practical approach to diff and merge. Text-based diffs of mesh files are unintuitive, and merging these files often breaks the models. Current common practice for diffing is simply to view meshes side-by-side, and merging is done manually. Meshes used for subdivision tend to have relatively low face count, and both the geometry of the vertices and \meshtopo{} have a significant impact on the subdivided mesh. Recent work has shown how to approximately find correspondences in the shape of complex meshes \cite{shaperegtut}, and smoothly blend portion of them using remeshing techniques \cite{snappaste}. While these algorithms could be adapted to diff and merge complex meshes, they are not directly applicable to our problem since we want precise diffs capturing both geometry and \meshtopo{} and robust merges that do not alter the mesh \meshtopo{}.

\paragraph{\MeshGit{}.}
We present \MeshGit{}, an algorithm that supports diffing and merging polygonal meshes. Figure~\fignum{fig:teaser} shows the results of diffing two version of a model from a public repository and an automatic merge of two non-conflicting edits. We take inspiration from text editing tools in both the underlying formalization of the problem and the proposed user workflow (see \fig{fig:diff3-text}). Inspired by the string edit distance \cite{sed}, we introduce the \emph{mesh edit distance} as a measure of the dissimilarity between meshes. This distance is defined as the set of editing operations with the lowest cost that transform one mesh into the other. In our case, we consider addition, deletion, and substitution of both vertices and faces. The mesh editing distance is related to the graph edit distance, a problem known to be NP-complete. We introduce an iterative greedy algorithm to compute the mesh edit distance by finding a set of editing operations between two meshes that minimizes the introduced metric.

\input{fig-diff3-text}

\paragraph{Diff and Merge.}
From the editing operations computed, we can easily display a \emph{meaningful} visual difference between meshes by just showing the addition, deletion, and changes to vertices and faces, just like standard diff tools for text editing. For merging, we compute the difference between two versions and the original, as is done explicitly in Git \cite{git} and implicitly in other systems \cite{svn}. We partition the edits into groups of operations that when applied individually respect the mesh \meshtopo{}. This limits the granularity of the edits in the same way that grouping characters into lines does for text merging. To merge the versions, we apply groups of operations to the original mesh to obtain the merged model. Some groups can be applied automatically, while others are conflicted and require manual resolution. We robustly detect conflicts by determining whether two groups of operations from the different versions are derived from the same parts of the original, \ie{} they intersect on the original. In \MeshGit{}, non-conflicting groups are applied automatically, while for conflicting edits, the user can either choose to pick which version to apply or has to manually resolve the conflict. We took this approach, as commonly done in text merging, since it is unclear how to merge conflicted topology changes in a way that respect the artists' intentions.

\paragraph{Contributions.}
In summary, this paper proposes a practical framework for diffing and merging polygonal meshes typically used in subdivision surface modeling. \MeshGit{} does this by (1) defining a mesh edit distance and describing a practical algorithm to compute it, (2) defining a partitioning rule to detect and reduce the granularity of editing conflicts, and (3) deriving diffing and merging tools that support a familiar text-editing-inspired workflow. We believe these three are the main contribution of this paper. We evaluate \MeshGit{} for a variety of meshes and found it to work well for all. The remainder of this paper will describe the algorithm, present the diffing and merging tool, and analyze their performance.
 

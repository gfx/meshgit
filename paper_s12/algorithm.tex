% !TEX root =  meshgit.tex

\section{\MeshGit: Computing the Mesh Edit Distance} \label{sec:med}

Text diffing and merging is based on computing the string edit distance \cite{sed} between two text strings. This distance is computed by determining the set of operations needed to turn one string into the other. The operations considered are character insertion, deletion, and substitution. If we associate a cost with each of these operations, we can define the string edit distance as the set of operations with minimal cost. In \MeshGit{} we take a similar approach.

\subsection{Mesh Edit Distance}

\paragraph{Mesh Edit Distance.}

We define the \emph{mesh edit distance} as the cost of transforming one mesh into another. We consider six operations: addition, deletion, and substitution of both vertices and faces. Formally, a mesh $M=\{V,F\}$ is a set of vertices $v \in V$ and faces $f \in F$. The mesh $M$ is modified to be the mesh $M'$ by applying a set of vertex operations, which we indicate as \opc{v}{v'} for vertex substitution, \opa{v'} for vertex addition, \opd{v} for vertex deletion, and face operations \opc{f}{f'}, \opa{f'}, and \opd{f}. In our notation, $\epsilon$ is a ``dummy'' variable used to indicate addition and deletion with the same notation as substitution; this is standard notation and will become useful when describing our algorithm.
The set of operations needed to turn a mesh into another is not unique.  For example, we can always obtain $M'$ from $M$ by deleting all vertices and faces of $M$ and adding all vertices and faces of $M'$. To compute a meaningful set of operations, we associate a cost to each operation and define the computation of the \emph{mesh edit distance} as determining the set of operations with minimal cost.

\paragraph{Vertex Operations' Costs.}

We set the cost of substituting a vertex $v$ in $M$ with a vertex $v'$ in $M'$ as the Euclidean distances of their positions $\mathbf{x}_v$ and $\mathbf{x}_v'$ and their normals $\mathbf{n}_v$, $\mathbf{n}_v'$. This cost ensures that vertices in one mesh are assigned to closeby vertices in the other, and that there is no cost for assigning a vertex to a co-located one\footnote{Note that our implementation assumes that vertices are defined with respect to the same coordinate system during editing, since this is common practice in mesh modeling and since most modeling software stores transformation matrices separately. If necessary, we could run an initial global alignment based on ICP \cite{gicp}. Furthermore, we normalize the size of both meshes to the average edge length so that vertex distances are normalized to the size of the mesh.}. The inclusion of the vertex orientation, computed as the average of the adjacent face normals, helps in cases where many small faces are located close to each other. We set the cost of vertex addition and deletions as a constant $\alpha$ larger than the maximum vertex change cost to favor substitutions over additions and deletions.
In summary,\vspace{-0.25in}

\begin{align*}
c(\opa{v'}) & = \alpha \\
c(\opd{v}) & = \alpha \\
c(\opc{v}{v'}) & = || \mathbf{x}_v - \mathbf{x}_{v'} || + (1 - \mathbf{n}_v \cdot \mathbf{n}_{v'})
\end{align*}

\paragraph{Face Operations' Costs.}

We set the cost of substituting a face $f$ in $M$ with a face $f'$ in $M'$ as the Euclidean distance between the faces' centers $\mathbf{x}_f$, $\mathbf{x}_f'$ and their normals $\mathbf{n}_f$, $\mathbf{n}_f'$, just like vertices. We set the cost of adding and deleting faces to a constant $\beta$ larger than the maximum cost for face substitution, to favor the latter over the formers. \vspace{-0.25in}

\begin{align*}
c(\opa{f'}) & = \beta \\
c(\opd{f}) & = \beta \\
c(\opc{f}{f'}) & = || \mathbf{x}_f - \mathbf{x}_{f'} || + (1 - \mathbf{n}_f \cdot \mathbf{n}_{f'})% + \\
%               & + \gamma (n^v_{\opc{f}{f'}} + n^f_{\opc{f}{f'}})
\end{align*}

%\note{we could try to set face add/delete cost to $\alpha$ time number of vertices for this face. low priority.}.

\input{fig-med}

\paragraph{Adjacency Costs.}
%
The costs defined above are not sufficient to produce intuitive visual differences since they do not account for changes in faces' and vertices' adjacencies when making substitutions.  For example, \fig{fig:med} shows two ways to modify a mesh $M$ to become a mesh $M'$.  These two sets of operations differ only by the substitution for the face $A$, where in one case the adjacency is maintained while in the other it is not. In this case, the geometric term would not distinguish between these two cases. We found that adding an adjacency cost we obtain visual differences that captures better the structure of the edits.

We define the adjacency cost for a substitution-pair of faces $f$ and $f'$ with two terms. The first is the number of vertices and faces $n^m_{\opc{f}{f'}}$ in the neighborhood of $f$ and $f'$ that are mismatched after the substitution, \ie{} they correspond to different vertices and faces in the two meshes. The other $n^d_{\opc{f}{f'}}$ is the number of vertex and face deletions in the neighborhood of  $f$ and $f'$. Both penalize differences in adjacencies between the two meshes. The first term accounts for differences caused by substitutions while the second for differences caused by deletions. Notice that we do not consider directly additions since they are accounted for in substitution mismatches and that this cost depends on the substitutions of both faces and vertices. This adjacency term is similar to the substitution affinity in \cite{gedspectral}. For convenience of notation, we express this cost with respect to the face substitution $\opc{f}{f'}$ as: \vspace{-0.25in}

\begin{align*}
	c_a(\opc{f}{f'}) & = \gamma \cdot n^m_{\opc{f}{f'}} + \delta \cdot n^d_{\opc{f}{f'}}
\end{align*}

\paragraph{Overall Cost.}
We compute the cost $C$ of a set of operations $O$ is the sum of all costs expressed as: \vspace{-0.25in}

{
\small
\begin{align*}
	C(O) & = \alpha ( n_\opd{v} + n_\opa{v'}) + \sum_{\{\opc{v}{v'}\}} \left[ ||\mathbf{x}_v - \mathbf{x}_{v'}|| + (1 - \mathbf{n}_v \cdot \mathbf{n}_{v'}) \right] + \\
		& + \beta(n_\opd{f} + n_\opa{f'}) + \sum_{\{\opc{f}{f'}\}} \left[  ||\mathbf{x}_f - \mathbf{x}_{f'}|| + (1 - \mathbf{n}_f \cdot \mathbf{n}_{f'}) \right] + \\
		& + \gamma \cdot n^m_{\opc{f}{f'}} + \delta \cdot n^d_{\opc{f}{f'}}
\end{align*}
}
where $n_\opd{v}$, $n_\opa{v'}$, $n_\opd{f}$ and $n_\opa{f'}$ are the number of vertex and face additions and deletions, and the geometric and adjacency terms are sums computed over the set of substitutions $\{\opc{v}{v'}\}$ and $\{\opc{f}{f'}\}$.
In the formulation above, the reader should consider that many substitutions have no cost. These correspond to parts where the meshes are identical. Our diffing and merging will only consider substitutions that correspond to actual changes in either vertices or faces.
All results in this paper are obtained with $\alpha=0.67$, $\beta=1.67$, $\gamma=0.67$ and $\delta=0.33$.
We determine these constants by diffing a large variety of meshes and choosing the values that produce the most informative visualizations.

\input{fig-iterdiff}

\subsection{Algorithm}

%\paragraph{Computing the Mesh Edit Distance.}
\paragraph{Iterative Greedy Algorithm.}

Our \med{} is related to the graph edit distance \cite{ged} defined between two attributed graphs. Computing the latter is known to be NP-complete. We compute the mesh edit distance with an iterative greedy algorithm, which provides a practical solution for our problem domain that we found to work well in all tested cases.

We initialize our algorithm with an operation set $O$ that transforms $M$ to $M'$ by deleting all of $M$ and adding all of $M'$. This is the set of operations that has the highest cost. The algorithm then iteratively executes a greedy step and a backtracking step. The greedy step minimizes the cost $C(O)$ of the set of operations $O$ by greedily changing pairs of deletions in $M$ and additions in $M'$ to substitutions.  The backtracking step removes substitutions that could cause the greedy algorithm to get stuck in local minima. We repeat these steps until no further improvements are found, or a maximum number of iterations has been reached (20 in our case). Figure~\fignum{fig:iterdiff} illustrates how the operation set evolves for subsequent iterations.

%\input{algorithm-iter-greedy}

%See \fig{fig:diff_sintel_heads} for a comparison of different matching algorithms.
%
%\jon{is there a way to get it linear, or should we minimize the claims on complexity?}
%\jon{is deleting / adding all the highest cost?}

\paragraph{Greedy Step.}

%The pseudocode for the greedy step of our algorithm is shown in \alg{alg:greedy}. 
The input to our greedy step are two meshes $M$ and $M'$ and the current set of operations $O$. In this step, we iteratively update the operation set $O$ by changing a pair of vertex addition \opa{v'} and deletion \opd{v} to a vertex substitution \opc{v}{v'} or a pair of face addition \opa{f'} and deletion \opd{f} to a face substitution \opc{f}{f'}. At each iteration, we greedily pick the vertex or face substitution that reduce the cost $C(O)$ the most. We iterate until no substitution is found that is cheaper to perform than leaving some vertices or faces added or deleted.

In practice we found that the greedy step finds substitutions ordered as growing patches. This is due to the adjacency cost that favors operations that consider vertices or faces adjacent to the ones already substitutes.

%\input{fig-alg-parameters}

%\input{algorithm-greedy}

\paragraph{Backtracking Step.}

The greedy step described above is prone to getting stuck in local minima, since no backtracking is allowed. While we found that method worked well for many cases, there were a few instances for which the algorithm generated suboptimal results, as shown in \fig{fig:iterdiff}. In our experiments, we found that local minima are often caused by the order in which the greedy algorithm grows patches. Suppose that a patch of adjacent faces is grown until it ``meets'' another patch.  If bridging the gap between the patches with substitutions changes adjacency significantly, which is expensive compared to just adding a new face, the added or deleted faces in the a gap might be left as such.  We observed that when this happens patches are small relative to the number of faces in the mesh. To ease this problem, we backtrack substitutions in two manners. First, we change all face substitutions that have an adjacency cost back to face addition and deletion. Second, we detect small patches (5\% of the size of the mesh) whose border has adjacency cost and completely undo them. We found that when we removed these substitutions, the greedy step would usually find better substitutions, minimizing the cost of $O$ to better approximate the true \med{}. We thus incorporated this backtracking step in our iterative scheme.

\input{fig-diff-chairs}
\input{fig-diff-durano-series.tex}


\subsection{Discussion}

\parasection{Time Complexity}
Given the geometric terms for vertices and faces in the cost function, we can prune the search space considerably.  In the greedy step, we only consider the $k$ nearest neighbors for each deleted vertex or face in $M$ from the set of added vertices or faces in $M'$. This reduces the computation time considerably. Furthermore, we compute the change in the cost function with local updates, since a substitution will only affect the costs in the local neighborhood. With these two optimizations, our algorithm has a time complexity of $O(n \log n)$, resulting in a practical solution that we found to perform well even with large meshes of several thousand vertices and faces.

%\input{fig-diff-sintel-heads}


\paragraph{Limitations.}
The \med{} includes the Euclidean distance between substituted vertices and faces. If the model was sculpted entirely and by a significant amount, the mesh edit distance may not capture well this type of edit. Furthermore, the greedy step of our algorithm does not prune the search space by connectivity.  For example, in \fig{fig:diff_chairs}, many disconnected components make up the two chairs.  While most changes are properly determined, the supports between the back legs are not properly diffed, since in the edited mesh they almost overlap and the geometric terms take over. Pruning the search space to just connected components would prevent this error, but it would also prevent edits that partition or bridge components from being detected.  For an example of such an edit, the center back support is broken into two parts and our algorithm can currently detect it.
Possible solutions might included the use of a geodesic distance or diffusion distance for the geometric terms, the addition of a term to detect connected components, or a multi-resolution approach that might strike a balance between maintaining connectivity of the meshes while capturing strong transformation edits. We leave these investigations as future work.


function test_icp()

fn0 = 'meshgit_big_01blend_ver0ply.mat';
fna = 'meshgit_big_01blend_veraply.mat';
fnb = 'meshgit_big_01blend_verbply.mat';

m0=load(fn0);
ma=load(fna);
mb=load(fnb);

[Ra0,Ta0] = icp(m0.V',ma.V');
[Rb0,Tb0] = icp(m0.V',mb.V');

va0 = Ra0 * ma.V' + repmat( Ta0, 1, length( ma.V ) );
vb0 = Rb0 * mb.V' + repmat( Tb0, 1, length( mb.V ) );

%v = cat(2, cat(2, m0.V', va0), vb0);
%scatter3( v(1,:),v(2,:),v(3,:),10,'filled' )

subplot(1,3,1)
v = m0.V';
scatter3( v(1,:), v(2,:), v(3,:),10,'filled')
subplot(1,3,2)
scatter3( va0(1,:), va0(2,:), va0(3,:),10,'filled')
subplot(1,3,3)
scatter3( vb0(1,:), vb0(2,:), vb0(3,:),10,'filled')

% !TEX root =  meshgit.tex

\input{fig-diff-durano-series}

\section{Diffing and Merging} \label{sect:tools}

\paragraph{Mesh Diff.}
We visualize the mesh differences similarly to text diffs.  In order to provide as much context as possible, we display all versions of the mesh side-by-side with vertices and faces colored to indicated the type or magnitude of the differences. A \emph{two-way diff}  illustrates the differences between two versions of a mesh, the original $M$ and the derivate $M'$, as in \fig{fig:teaser}.a. We display adjacency changes by coloring in red the deleted faces in $M$ (unmatched or with mismatched adjacencies in $M$) and in green the added faces in $M'$ (unmatched or with mismatched adjacencies in $M'$). We display geometric changes by coloring vertices in blue with a saturation proportional to magnitude of the movement. In our visualizations, we simplify the presentation by not drawing the vertices directly but linearly interpolating their colors across the adjacent faces, unless the face has been colored red or green. Unmodified faces and vertices are colored gray. When a mesh $M$ has two derived versions, 
$M^a$ and $M^b$, a \emph{three-way diff} illustrates the changes between both derivatives and the original, as shown in \fig{fig:teaser}.b. We use a color scheme similar to the above, but the brightness of the color indicates from which derivative the operation comes. When a face has been modified in both derivatives it is indicated in yellow (\fig{fig:ufo}).

An artist can also use \MeshGit{} to visualize the \emph{progression} of work on a mesh, as shown in \fig{fig:diff_durano_series}. Each mesh snapshot is visualized similarly to a three-way diff.  For each snapshot, a face is colored green if it was added, red if it is deleted, and orange if the face was added and then deleted. An alternative approach to visualizing mesh construction sequences is demonstrated in \emph{MeshFlow} \cite{meshflow}, that while providing a richer display, also requires full instrumentation of the modeling software.

\paragraph{Mesh Merge.}
Given a mesh $M$ and two derivative meshes $M^a$ and $M^b$, one may wish to incorporate the changes made in both derivatives into a single resulting mesh. For example, in \fig{fig:teaser}.b, one derivative has finger nails added to the hand, while the other has refined and sculpted the palm. Presently, the only way to merge mesh edits such as this is for an artist to determine the changes done and then manually perform the merge of modifications by hand.
\MeshGit{} supports a merging workflow similar to text editing. We first compute two sets of mesh transformations in order to transform $M$ into $M^a$ and into $M^b$. If the two sets of transformations do not modify the same elements of the original mesh, \MeshGit{} merges them automatically by simply performing both sets of transformations on $M$. However, if the sets \emph{overlap} on $M$, then they are in conflict. In this case, it is unclear how to merge the changes automatically while respecting artists intentions. For this reason, we follow text editing workflows, and ask the user to either choose which set of operations to apply or to merge the conflict by hand.
We reduce the number of conflicts, thus the granularity of users' decisions, by partitioning the mesh transformations into groups that can be safely applied individually. This is akin to grouping text characters into lines in text merging.

An example of our automatic merging is shown in \fig{fig:teaser}.b, where the changes do not overlap in the original mesh. In this case, \MeshGit{} merges the changes automatically. Another example is shown in \fig{fig:shaolin}. In one version the body is sculpted by moving vertices, while in the other the skirt is removed and the boots are replaced with sandals, thus also changing the face adjacencies. These two sets of differences do not affect the same elements on the original since sculpting affects only the geometric properties of the vertices. \MeshGit{} can safely merge these edits. The top subfigure of \fig{fig:shaolin} show the resulting merged mesh with colors indicating the applied transformations. On the right we show recursively applying Catmull-Clark subdivision rules twice to demonstrate that adjacencies are well maintained.

\input{fig-shaolin}

To handle conflicts gracefully, we make the observation that edits that change adjacencies will partition the mesh into regions,
such that each region contains faces that are all added, deleted, have some geometric changes, or are unchanged.
If we apply all edits of one region, we obtain a resulting merge that is valid and respects the artists changes to adjacencies.
Therefore, we partition the edits by finding connected regions \rev{}{of matched elements (similar to the backtracking step)} that have adjacency changes on the boundaries, 
and detect conflicts between the revisions at the granularity of these regions. This is akin to grouping text changes into line,
rather than applying them as individual characters.

Figure~\fignum{fig:ufo} shows an example with a conflicting edit on a spaceship model.
In one version, features are added to the spaceship's body and the base of the body has been enlarged.
In the other, the cockpit exterior is detailed and wings are added to the base and top of the body.
In this case, the extended base in the first version and the added lower wings in the second version are conflicting edits.
\MeshGit{} successfully detected the conflicts to the body and merged all other changes automatically (\rev{center column}{top center}).
To resolve the conflicts, the user can pick which version of edits to apply and use \MeshGit{} to properly apply the edits, as shown in the figure, or simply resolve the conflict manually.
The top three subfigures show three possible ways to resolve the conflicted merge.

\input{fig-diff-station}

%%% The ``\documentclass'' command has one parameter, based on the kind of
%%% document you are preparing.
%%%
%%% [annual] - Technical paper accepted for presentation at the ACM SIGGRAPH 
%%%   or SIGGRAPH Asia annual conference.
%%% [sponsored] - Short or full-length technical paper accepted for 
%%%   presentation at an event sponsored by ACM SIGGRAPH
%%%   (but not the annual conference Technical Papers program).
%%% [abstract] - A one-page abstract of your accepted content
%%%   (Technical Sketches, Posters, Emerging Technologies, etc.). 
%%%   Content greater than one page in length should use the "[sponsored]"
%%%   parameter.
%%% [preprint] - A preprint version of your final content.
%%% [review] - A technical paper submitted for review. Includes line
%%%   numbers and anonymization of author and affiliation information.
\documentclass[annual]{supplemental}
\TOGonlineid{0060}
\TOGvolume{0}
\TOGnumber{0}
\TOGarticleDOI{1111111.2222222}
%\TOGprojectURL{}
%\TOGvideoURL{}
%\TOGdataURL{}
%\TOGcodeURL{}

%\documentclass[10pt]{article}
%\pagestyle{plain}
%\usepackage[utf8]{inputenc}



\usepackage[scaled=.92]{helvet}
\usepackage{times}

%% The 'graphicx' package allows for the inclusion of EPS figures.

\usepackage{graphicx}

%% use this for zero \parindent and non-zero \parskip, intelligently.

\usepackage{parskip}

%% Optional: the 'caption' package provides a nicer-looking replacement
%% for the standard caption environment. With 'labelfont=bf,'textfont=it',
%% caption labels are bold and caption text is italic.

\usepackage[labelfont=bf,textfont=it]{caption}
\usepackage{xspace}
\usepackage{paralist}
\usepackage{pgfplots}
\usepackage{amsmath}
\usepackage{booktabs}
\usepackage{multirow}
\usepackage{color}
\usepackage{fixltx2e}	% prevents figures to be out of order in PDF from LaTeX
\usepackage{algorithm2e}
\usepackage{verbatim}
\usepackage{rotating}
\usepackage{array}
\usepackage{minibox}
\usepackage{rotating}
\usepackage{url}
\urlstyle{same}

% squeezing
\usepackage[all=normal,floats,paragraphs]{savetrees}

%\usepackage[draft]{hyperref} % hack to find a link that crosses a page-break (causing pdflatex to barf)
% add draft to acmsiggraph.cls:497,499

%%%%%% MACROS %%%%%%%%

\input{macros}

\newcommand{\MeshGit}{\emph{MeshGit}\xspace}

\renewcommand{\topfraction}{0.9}	% max fraction of floats at top
\setcounter{topnumber}{2}
\setcounter{totalnumber}{4}     % 2 may work better
\renewcommand{\textfraction}{0.07}	% allow minimal text w. figs
\renewcommand{\floatpagefraction}{0.7}	% require fuller float pages
\renewcommand{\dblfloatpagefraction}{0.7}	% require fuller float pages
\renewcommand{\thefootnote}{\fnsymbol{footnote}}



%opening
\title{MeshGit: Diffing and Merging Meshes for Polygonal Modeling \\ Supplemental Material}
\author{}
\pdfauthor{}

\newcommand{\quarterpagewidth}{0.8in} %1.0
\newcommand{\thirdpagewidth  }{1.8in} %2.0
\newcommand{\halfpagewidth   }{2.8in} %3.0
\newcommand{\fullpagewidth   }{5.8in} %6.0



\begin{document}

\maketitle

%\begin{abstract}
%This is the supplemental document for \MeshGit{}
%\end{abstract}

\section{Details: Backtracking Step and $\alpha$ Weight}

When the iterative greedy algorithm begins, all elements (vertices and faces) in both meshes are unmatched.  At this point, the
only potential matches for any given element are those most nearest ($k$ nearest neighbors, in our implementation).  Due to the
geometric term, the closest elements are matched first, regardless of the adjacencies.  If a region of the mesh has not been
geometrically changed much, the elements in this region are matched appropriately.  On the other hand, if a region of the mesh
has been sculpted so the vertices and faces are geometrically closer to non-corresponding vertices and faces, matches are made
that are incompatible with other regions of the mesh.

For example, \fig{fig:tannerneck} shows how the neck region self-intersects in the derivative but does not in the original.
As the greedy step progresses, due to the low geometric costs, this region is matched incorrectly when compared to the matches
made on cheek and collar bones.  Because the matches in region are misaligned, the adjacency costs for
continuing to grow this region are higher than those of correctly aligned matches when the mesh adjacency is irregular.
Therefore, when a large enough region is matched with proper alignment, the majority of the mesh is accordingly properly
matched.

However, due to greedy step making small, local changes, the cost of leaving misaligned matches is smaller than removing the
matches.  In other words, the greedy algorithm is stuck in a local minimum.
This leaves (usually) small regions where the matches are incompatible with the other regions of the connected component.

Under such conditions, we found that a backtracking step can often bring the cost equation out of the local minimum, allowing
the greedy algorithm to continue finding a better mesh edit distance.  Unfortunately, when the mesh has been heavily sculpted
(as seen in \fig{fig:tannerneck}), the geometric term can pull the greedy algorithm back into the local minimum.
We therefore augment the cost function to weigh the geometric cost less after each iteration.  This effectively places more
importance on the adjacencies, but without compromising the results.

At each iteration, we reduce the magnitude of the backtracking step by reducing $\beta$, the minimum ratio of the number
of adjacent matched faces to the number of faces of the connected component, by half.  The geometric term is weighted
by $\alpha$, where $\alpha$ is reduced by 25\% each iteration.  The results of using different values for reducing $\alpha$ is
shown in \fig{fig:tannerneck}.

One may argue that $\alpha$ should be set to a smaller value initially rather than iteratively reducing it.
However, when $\alpha$ is set too ``low'', on some meshes the greedy algorithm tends to perform much like a topological matching
algorithm (\eg{} \cite{eppstein09}).  The effect $\alpha$ has on the results of the greedy algorithm is seen in \fig{fig:sintel}.
Notice how when $\alpha = 0.50$, the greedy algorithm weighs heavily the adjacencies, and the result has ``pulled'' the matches
for the face down toward the chin.  In fact, there is a noticable lack of matches around extraordinary vertices (vertices that
are adjacent to five or more faces, for example) found at the cheek bone, between nostril and corner of eye, and at the nostrils.
We found that setting a ``normalized'' value for $\alpha$ initially and iteratively reducing its value strikes a balance between
matching geometric changes and adjacencies changes.


\section{Additional Results}

The following figures demonstrate \MeshGit{} producing mesh diffs with larger and more complex examples.  While we found these results to
be compelling, we leave them here rather than including them in the paper, because we wanted to use in the paper simple
examples to help the reader understand the main contributions and limitations of the proposed algorithm.

All of the examples shown in the paper and in this supplemental paper are included in the supplemental materials as meshes in
Stanford PLY file formats, along with C++ source code and executable binaries (OSX~10.7, Linux) for \MeshGit{}.

While stated in the paper, we feel it is important to note that \MeshGit{} is meant for producing intuitive diffs of meshes
constructed with polygonal modeling software and merge edits that respect polygon adjacencies, which is key for modeling with
subdivision surfaces. These are invaluable features for collaborative modeling.
To our knowledge, \MeshGit{} is the first algorithm to provide such these features.

However, when the adjacencies change drastically, or when adjacencies are only important to convey a shape, as it is usually
with 3D~reconstruction or digital sculpting sessions with dynamic remeshing, a shape matching algorithm may provide a more
intuitive difference visualization.  For example, spectral matching \cite{cour06} and shape blending \cite{kim11}.
Figures~\ref{fig:dragon} and \ref{fig:head} are examples where the meshes have similar shapes but have very different adjacencies.


\bibliographystyle{acmsiggraph}
\bibliography{bib}

\vspace{1in}

{
\ifdefined\cs
	\renewcommand{\cs}[1]{\hspace{0.1in}}
\else
	\newcommand{\cs}[1]{\hspace{0.1in}}
\fi
\newcommand{\foo}{---\hspace{5px}}
\newcommand{\figref}[1]{\fignum{#1}\hspace{3px}}
\begin{table}[h!]
\begin{center}
	\scalebox{1.0}{
	\begin{tabular}{@{}l@{\cs}r@{\cs}r@{\cs}r@{\cs}r@{\cs}r@{\cs}l@{}}
			\toprule
			{\textbf{Model}} 	& {\textbf{Fig.}} 	& \multicolumn{3}{c}{\textbf{Number of Faces}}		& {\textbf{Matching}}		& {\textbf{Citations}} \\
								&						& original	& ver.~1		& ver.~2				& \hspace{10px}time		& \\
			\midrule
			\emph{Dragon}		& \figref{fig:dragon}		& \foo{}		& { 88028}		& { 96616}		& { 321.1s}				& \cite{model:dragon} \\
			\emph{Head}			& \figref{fig:head}		& {136614}	& { 93312}		& \foo{}			& { 121.0s,498.6s}		& \cite{model:head0,model:head1} \\
			\emph{Office}		& \figref{fig:office}		& { 57292}	& { 64927}		& \foo{}			& { 365.6s}				& \cite{model:office0,model:office1} \\
			\emph{Shuttle}		& \figref{fig:shuttle}	& \foo{}		& {166974}		& {193970}		& { 612.3s}				& \cite{model:shuttle0,model:shuttle1} \\
			\emph{Sintel}		& \figref{fig:sintel}		& \foo{}		& {  1810}		& {  1712}		& {   3.0s}				& \cite{model:sintel} \\
			\emph{Tanner}		& \figref{fig:tanner}		& { 10123}	& {  8607}		& {  7665}		& {  30.6s}				& \cite{model:tanner0,model:tanner1,model:tanner2} \\
			\emph{Woman 0,1}		& \figref{fig:woman01}	& \foo{}		& { 17720}		& {  8616}		& {  36.7s}				& \cite{model:woman0,model:woman1} \\
			\emph{Woman 2,3}		& \figref{fig:woman23}	& \foo{}		& {  9246}		& { 19383}		& {  34.4s}				& \cite{model:woman2,model:woman3} \\
			\bottomrule
	\end{tabular}
	}
\end{center}
\vspace{-0.05in}
\caption{Statistics of input meshes and computing the mesh edit distance. The right column shows the matching time of the iterative greedy algorithm to compute the mesh edit distance between the original mesh and a derivative (or between ver.~1 and ver.~2).  The first time for Head model is for greedy step only, and the second time is for full iterative greedy algorithm.}
\label{tbl:modelstats}
\vspace{-0.12in}
\end{table}
}




%%%%%%%%%%%%%%%% TANNER NECK %%%%%%%%%%%%%%%%
{\newpage{}
% 190 40 210 50
\newcommand{\imgheight}{1.5in}
\sffamily\setlength\fboxsep{0pt}\setlength\fboxrule{0.5pt}
\begin{figure}[p!]
	\begin{center}
	\begin{tabular}{@{}l@{\hspace{5px}}c@{\hspace{5px}}c@{\hspace{5px}}c@{\hspace{5px}}c@{}}
		{} & & \figlbl{version~2} & \figlbl{version~3} & \\
		
		\raisebox{0.6in}{$m_\alpha = 0.50$} &
		\includegraphics[height=\imgheight,trim= 190px  40px 1170px  50px,clip=true]{figures/tanner/front12_050.png} &
		\fbox{\includegraphics[height=\imgheight,trim=  10px  80px  970px  80px,clip=true]{figures/tanner/neck12_050.png}} &
		\fbox{\includegraphics[height=\imgheight,trim= 970px  80px   10px  80px,clip=true]{figures/tanner/neck12_050.png}} &
		\includegraphics[height=\imgheight,trim=1150px  40px  210px  50px,clip=true]{figures/tanner/front12_050.png} \\
		
		\raisebox{0.6in}{$m_\alpha = 0.75$} &
		\includegraphics[height=\imgheight,trim= 190px  40px 1170px  50px,clip=true]{figures/tanner/front12_075.png} &
		\fbox{\includegraphics[height=\imgheight,trim=  10px  80px  970px  80px,clip=true]{figures/tanner/neck12_075.png}} &
		\fbox{\includegraphics[height=\imgheight,trim= 970px  80px   10px  80px,clip=true]{figures/tanner/neck12_075.png}} &
		\includegraphics[height=\imgheight,trim=1150px  40px  210px  50px,clip=true]{figures/tanner/front12_075.png} \\
		
		\raisebox{0.6in}{$m_\alpha = 0.80$} &
		\includegraphics[height=\imgheight,trim= 190px  40px 1170px  50px,clip=true]{figures/tanner/front12_080.png} &
		\fbox{\includegraphics[height=\imgheight,trim= 10px  80px 970px  80px,clip=true]{figures/tanner/neck12_080.png}} &
		\fbox{\includegraphics[height=\imgheight,trim=970px  80px  10px  80px,clip=true]{figures/tanner/neck12_080.png}} &
		\includegraphics[height=\imgheight,trim=1150px  40px  210px  50px,clip=true]{figures/tanner/front12_080.png} \\
		
		\raisebox{0.6in}{$m_\alpha = 0.90$} &
		\includegraphics[height=\imgheight,trim= 190px  40px 1170px  50px,clip=true]{figures/tanner/front12_090.png} &
		\fbox{\includegraphics[height=\imgheight,trim= 10px  80px 970px  80px,clip=true]{figures/tanner/neck12_090.png}} &
		\fbox{\includegraphics[height=\imgheight,trim=970px  80px  10px  80px,clip=true]{figures/tanner/neck12_090.png}} &
		\includegraphics[height=\imgheight,trim=1150px  40px  210px  50px,clip=true]{figures/tanner/front12_090.png} \\
		
		\raisebox{0.6in}{$m_\alpha = 1.00$} &
		\includegraphics[height=\imgheight,trim= 190px  40px 1170px  50px,clip=true]{figures/tanner/front12_100.png} &
		\fbox{\includegraphics[height=\imgheight,trim= 10px  80px 970px  80px,clip=true]{figures/tanner/neck12_100.png}} &
		\fbox{\includegraphics[height=\imgheight,trim=970px  80px  10px  80px,clip=true]{figures/tanner/neck12_100.png}} &
		\includegraphics[height=\imgheight,trim=1150px  40px  210px  50px,clip=true]{figures/tanner/front12_100.png} \\
	\end{tabular}
	\end{center}
	
	\caption{Tanner: the effect on matching of varying how much $\alpha$ is reduced each iteration ($\alpha_{n+1} = m_\alpha * \alpha_{n}$).  The derivative mesh (version~2) has been heavily sculpted.  Despite the neck region self-intersecting in the derivative mesh, \MeshGit{} is able to intuitively match this region when the geometric terms are reduced (small $\alpha$ value).}
	\label{fig:tannerneck}
\end{figure}
}




%%%%%%%%%%%%%%%% SINTEL %%%%%%%%%%%%%%%%
{\newpage{}
\sffamily\setlength\fboxsep{0pt}\setlength\fboxrule{0.5pt}
\begin{figure}[p!]
	\begin{center}
	\begin{tabular}{@{}l@{\hspace{5px}}c@{\hspace{5px}}c@{}}
		& \figlbl{version 1} & \figlbl{version 2} \\
		\raisebox{0.6in}{$\alpha = 0.50$} &
		\includegraphics[width=\thirdpagewidth,trim= 10px 10px 970px 10px,clip=true]{figures/sintel_iters/greedy_050.png} &
		\includegraphics[width=\thirdpagewidth,trim=970px 10px  10px 10px,clip=true]{figures/sintel_iters/greedy_050.png} \\
		\raisebox{0.6in}{$\alpha = 0.60$} &
		\includegraphics[width=\thirdpagewidth,trim= 10px 10px 970px 10px,clip=true]{figures/sintel_iters/greedy_060.png} &
		\includegraphics[width=\thirdpagewidth,trim=970px 10px  10px 10px,clip=true]{figures/sintel_iters/greedy_060.png} \\
		\raisebox{0.6in}{$\alpha = 0.80$} &
		\includegraphics[width=\thirdpagewidth,trim= 10px 10px 970px 10px,clip=true]{figures/sintel_iters/greedy_080.png} &
		\includegraphics[width=\thirdpagewidth,trim=970px 10px  10px 10px,clip=true]{figures/sintel_iters/greedy_080.png} \\
		\raisebox{0.6in}{$\alpha = 1.00$} &
		\includegraphics[width=\thirdpagewidth,trim= 10px 10px 970px 10px,clip=true]{figures/sintel_iters/greedy_100.png} &
		\includegraphics[width=\thirdpagewidth,trim=970px 10px  10px 10px,clip=true]{figures/sintel_iters/greedy_100.png} \\
	\end{tabular}
	\end{center}
	
	\caption{Sintel: the effect on matching after greedy algorithm (only) by varying the $\alpha$ value.  When $\alpha$ is too low, the greedy algorithm weights too heavily the adjacencies and has trouble matching well the irregular points on the mesh (extraordinary vertices).}
	\label{fig:sintel}
\end{figure}
}




%%%%%%%%%%%%%%%% DRAGON %%%%%%%%%%%%%%%%
{\newpage{}
\sffamily\setlength\fboxsep{0pt}\setlength\fboxrule{0.5pt}
\begin{figure}[t!]
	\begin{center}
	\begin{tabular}{@{}c@{\hspace{5px}}c@{}}
		\figlbl{version~1} & \figlbl{version~2} \\
		\includegraphics[width=\halfpagewidth,trim= 10px 310px 970px 240px,clip=true]{figures/dragon_front.png} &
		\includegraphics[width=\halfpagewidth,trim=970px 310px  10px 240px,clip=true]{figures/dragon_front.png} \\
		\includegraphics[width=\halfpagewidth,trim= 10px  10px 970px 300px,clip=true]{figures/dragon_back.png} &
		\includegraphics[width=\halfpagewidth,trim=970px  10px  10px 300px,clip=true]{figures/dragon_back.png} \\
	\end{tabular}
	\end{center}
	
	\caption{Dragon: example of two meshes with similar shape but different adjacencies.}
	\label{fig:dragon}
\end{figure}
}


%%%%%%%%%%%%%%%% HEAD %%%%%%%%%%%%%%%%
{
\sffamily\setlength\fboxsep{0pt}\setlength\fboxrule{0.5pt}
\begin{figure}[b!]
	\begin{center}
	\begin{tabular}{@{}c@{\hspace{5px}}c@{}}
		\figlbl{original} & \figlbl{derivative} \\
		\includegraphics[width=\halfpagewidth,trim= 10px 10px 970px 10px,clip=true]{figures/head.png} &
		\includegraphics[width=\halfpagewidth,trim=970px 10px  10px 10px,clip=true]{figures/head.png} \\
	\end{tabular}
	\end{center}
	
	\caption{Head: example of two meshes with similar shape but different adjacencies.  This figure shows the matching after the greedy step only.  The derivative mesh is a retopology of the original. \MeshGit{} does not handle such global modifications, but instead matches local patches that have similar adjacencies.  The backtracking step removes all of the matches, because the size of the patches are small relative to the entire head.}
	\label{fig:head}
\end{figure}
}


%%%%%%%%%%%%%%%% OFFICE %%%%%%%%%%%%%%%%
{\newpage{}
\sffamily\setlength\fboxsep{0pt}\setlength\fboxrule{0.5pt}
\begin{figure}[p!]
	\begin{center}
	\begin{tabular}{@{}c@{\hspace{5px}}c@{}}
		\figlbl{original} & \figlbl{derivative} \\
		\includegraphics[width=\halfpagewidth,trim= 10px 360px 970px 430px,clip=true]{figures/office_out0.png} &
		\includegraphics[width=\halfpagewidth,trim=970px 360px  10px 430px,clip=true]{figures/office_out0.png} \\
		\includegraphics[width=\halfpagewidth,trim= 10px 300px 970px 400px,clip=true]{figures/office_out1.png} &
		\includegraphics[width=\halfpagewidth,trim=970px 300px  10px 400px,clip=true]{figures/office_out1.png} \\
		\fbox{\includegraphics[width=\halfpagewidth,trim= 10px  80px 970px  80px,clip=true]{figures/office_in0.png}} &
		\fbox{\includegraphics[width=\halfpagewidth,trim=970px  80px  10px  80px,clip=true]{figures/office_in0.png}} \\
		\fbox{\includegraphics[width=\halfpagewidth,trim= 10px  80px 970px  80px,clip=true]{figures/office_in1.png}} &
		\fbox{\includegraphics[width=\halfpagewidth,trim=970px  80px  10px  80px,clip=true]{figures/office_in1.png}} \\
	\end{tabular}
	\end{center}
	
	\caption{Office: mesh diff between two large meshes of 57k and 65k triangles, respectively.  In the derivative mesh, the artist added a section to the building, seen in the second row, as well as corrected the ``topology'' of inside regions, seen on the walkway of bottom row.}
	\label{fig:office}
\end{figure}
}


%%%%%%%%%%%%%%%% SHUTTLE %%%%%%%%%%%%%%%%
{\newpage{}
\sffamily\setlength\fboxsep{0pt}\setlength\fboxrule{0.5pt}
\begin{figure}[p!]
	\begin{center}
	\begin{tabular}{@{}c@{\hspace{5px}}c@{}}
		\figlbl{version~1} & \figlbl{version~2} \\
		\includegraphics[width=\halfpagewidth,trim= 10px 210px 970px 190px,clip=true]{figures/shuttle_front.png} &
		\includegraphics[width=\halfpagewidth,trim=970px 210px  10px 190px,clip=true]{figures/shuttle_front.png} \\
		\includegraphics[width=\halfpagewidth,trim= 10px 280px 970px 320px,clip=true]{figures/shuttle_back.png} &
		\includegraphics[width=\halfpagewidth,trim=970px 280px  10px 320px,clip=true]{figures/shuttle_back.png} \\
		\includegraphics[width=\halfpagewidth,trim= 10px  90px 970px  50px,clip=true]{figures/shuttle_under.png} &
		\includegraphics[width=\halfpagewidth,trim=970px  90px  10px  50px,clip=true]{figures/shuttle_under.png} \\
	\end{tabular}
	\end{center}
	
	\caption{Shuttle: mesh diff between the two largest meshes of 167k and 194k polygons.  The meshes comprise 2254 and 3352 individual components, respectively. }
	\label{fig:shuttle}
\end{figure}
}


%%%%%%%%%%%%%%%% TANNER %%%%%%%%%%%%%%%%
{\newpage{}
% 0,640,1280
\sffamily\setlength\fboxsep{0pt}\setlength\fboxrule{0.5pt}
\begin{figure}[p!]
	\begin{center}
	\begin{tabular}{@{}c@{\hspace{5px}}c@{\hspace{5px}}c@{}}
		\figlbl{version~1} & \figlbl{version~2} & \figlbl{version~3} \\
		\includegraphics[width=\thirdpagewidth,trim=  10px 10px 1290px 50px,clip=true]{figures/tanner/front.png} &
		\includegraphics[width=\thirdpagewidth,trim= 650px 10px  650px 50px,clip=true]{figures/tanner/front.png} &
		\includegraphics[width=\thirdpagewidth,trim=1290px 10px   10px 50px,clip=true]{figures/tanner/front.png} \\
		\includegraphics[width=\thirdpagewidth,trim=  10px 10px 1290px 50px,clip=true]{figures/tanner/back.png} &
		\includegraphics[width=\thirdpagewidth,trim= 650px 10px  650px 50px,clip=true]{figures/tanner/back.png} &
		\includegraphics[width=\thirdpagewidth,trim=1290px 10px   10px 50px,clip=true]{figures/tanner/back.png} \\
	\end{tabular}
	\end{center}
	
	\caption{Tanner}
	\label{fig:tanner}
\end{figure}
}


%%%%%%%%%%%%%%%% WOMAN 01 %%%%%%%%%%%%%%%%
{\newpage{}
\sffamily\setlength\fboxsep{0pt}\setlength\fboxrule{0.5pt}
\begin{figure}[p!]
	\begin{center}
	\begin{tabular}{@{}c@{\hspace{5px}}c@{}}
		\figlbl{version~1} & \figlbl{version~2} \\
		\includegraphics[width=\halfpagewidth,trim= 10px 10px 970px 10px,clip=true]{figures/woman_01_front.png} &
		\includegraphics[width=\halfpagewidth,trim=970px 10px  10px 10px,clip=true]{figures/woman_01_front.png} \\
		\includegraphics[width=\halfpagewidth,trim= 10px 10px 970px 10px,clip=true]{figures/woman_01_back.png} &
		\includegraphics[width=\halfpagewidth,trim=970px 10px  10px 10px,clip=true]{figures/woman_01_back.png} \\
	\end{tabular}
	\end{center}
	
	\caption{Ishtarian Woman 0 and 1}
	\label{fig:woman01}
\end{figure}
}


%%%%%%%%%%%%%%%% WOMAN 23 %%%%%%%%%%%%%%%%
{\newpage{}
\sffamily\setlength\fboxsep{0pt}\setlength\fboxrule{0.5pt}
\begin{figure}[p!]
	\begin{center}
	\begin{tabular}{@{}c@{\hspace{5px}}c@{}}
		\figlbl{version~2} & \figlbl{version~3} \\
		\includegraphics[width=\halfpagewidth,trim= 10px 10px 970px 10px,clip=true]{figures/woman_23_front.png} &
		\includegraphics[width=\halfpagewidth,trim=970px 10px  10px 10px,clip=true]{figures/woman_23_front.png} \\
		\includegraphics[width=\halfpagewidth,trim= 10px 10px 970px 10px,clip=true]{figures/woman_23_back.png} &
		\includegraphics[width=\halfpagewidth,trim=970px 10px  10px 10px,clip=true]{figures/woman_23_back.png} \\
	\end{tabular}
	\end{center}
	
	\caption{Ishtarian Woman 2 and 3}
	\label{fig:woman23}
\end{figure}
}


\end{document}

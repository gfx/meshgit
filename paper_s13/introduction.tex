% !TEX root =  meshgit.tex

\section{Introduction} \label{sec:intro}


%\paragraph{Version Control.}

When managing digital files, version control greatly simplifies the work of individuals
and is indispensable for collaborative work.
Version control systems such as Subversion and Git %\cite{svn,git}
have a large variety of features.
For text files, the features that have the most impact on workflow are the ability to store multiple versions of files, 
to visually compare, \ie{} diff, the content of two revisions, and to merge the changes of two revisions into a final one.
For 3D graphics files, version control is commonly used to maintain multiple versions of scene files, but artists are not able to 
diff and merge most scene data. 

We focus on polygonal meshes used in today's subdivision and low-polygon modeling workflows, for which there is no practical approach to diff and merge.
Text-based diffs of mesh files are unintuitive, and merging these files often breaks the models.
Current common practice for diffing is simply to view meshes side-by-side, and merging is done manually.
While this might be sufficient, albeit cumbersome, when a couple of artists are working on a model, version control becomes necessary as the number of artists increases and for crowd-sourcing efforts, just like text editing.
Meshes used for subdivision tend to have relatively low face count, and both the geometry of the vertices and adjacencies of the faces have a significant impact on the subdivided mesh.
Recent work has shown how to approximately find correspondences in complex meshes \cite{shaperegtut}, and smoothly blend portion of them using remeshing techniques \cite{snappaste}.
These algorithms are unfortunately not directly applicable to our problem since we want diffs that captures all differences precisely and robust merges that do not alter the mesh adjacencies.
\cite{dobos12_techbrief} recently propose a version control system that works at the granularity of single object components, \ie{} at the granularity of singular meshes in a scene graph.
We are instead interested in determining differences of elements of each mesh, namely vertices and faces and their adjacency.    

\paragraph{\MeshGit{}.}

We present \MeshGit{}, an algorithm that supports diffing and merging polygonal meshes.
\Fig{fig:teaser} shows the results of diffing two versions of a model and an automatic merge of two non-conflicting edits.
We take inspiration from text editing tools in both the underlying formalization of the problem and the proposed user workflow (see \fig{fig:diff3-text}).
Inspired by the string edit distance \cite{sed}, we introduce the \med{} as a measure of the dissimilarity between meshes.
This distance is defined as the minimum cost of matching vertices and faces of one mesh to those of another mesh.
The mesh edit distance is related to the maximum common subgraph-isomorphism problem, a problem known to be NP-hard.
We propose an iterative greedy algorithm to efficiently approximate the mesh edit distance.

\input{fig-diff3-text}

%\paragraph{Diff and Merge.}

Once the matching from one mesh to another is computed, we translate the found correspondences into a set of mesh transformations that can transform the first mesh into the second.
We consider vertex translations, additions, and deletions and face additions and deletions.
With this set of transformations, we can easily display a \emph{meaningful} visual difference between the meshes by just showing the modifications to vertices and faces, just like standard diff tools for text editing.
For merging, we compute the difference between two versions and the original.
We partition the transformations into groups that, when applied individually, respect the mesh adjacencies.
This partitioning limits the granularity of the edits in the same way that grouping characters into lines does for text merging.
To merge the changes from the two versions, we apply groups of transformations to the original mesh to obtain the merged model.
Some groups can be applied automatically, while others are conflicted and require manual resolution.
We robustly detect conflicts by determining whether two groups from the different versions modify the same parts of the original, \ie{} they intersect on the original.
In \MeshGit{}, non-conflicting groups are applied automatically, while for conflicting edits, the user can either choose a version to apply or resolve the conflict manually.
We took this approach, as commonly done in text merging, since it is unclear how to merge conflicting transformations in a way that respects the artists' intentions.

\paragraph{\MeshGit{} Uses.}
We evaluate \MeshGit{} for a wide variety of meshes taken from user editing sessions in subdivision modeling workflows.
Our tests include meshes that are a mixture of triangles and quads and can have highly regular or irregular adjacencies. 
We found that \MeshGit{} worked well for all these tested meshes.
We choose these types of meshes since they are commonly used by artists today in production environments.
To allow readers to use \MeshGit{} in their daily workflows, we include source code and executable in supplemental material.

While \MeshGit{} works well in our context, we do not expect the computed diffs to be as informative in other modeling workflows where mesh adjacencies are not of paramount importance, \eg{}
free-form sculpting with dynamic topology or smooth shape manipulation with remeshing.
In these workflows, artists are only concerned with manipulating geometry, while the system can change mesh adjacency if needed.
For example, \fig{fig:diff_dragon} shows an example of two versions of a mesh obtained with workflows that allow for remeshing.
While \MeshGit{} computes correct mesh differences, these are, in our opinion, less informative for artists than just a geometry-only diff.  
\rev{}{These workflows are out of the scope of \MeshGit{}, and we leave this for future work.}

\paragraph{Contributions.}

In summary, this paper proposes a practical framework for diffing and merging polygonal meshes typically used in low-polygon and subdivision surface modeling.
\MeshGit{} does this by (1) defining a mesh edit distance and describing a practical algorithm to \rev{compute}{approximate} it, (2) defining a partitioning rule to reduce the granularity of mesh transformation conflicts, and (3) deriving diffing and merging tools for polygonal meshes that support a familiar text-editing-inspired workflow.
We believe these are the main contributions of this paper.
The remainder of this paper will describe the algorithm, present the diffing and merging tool, and analyze their performance.
 

% !TEX root =  meshgit.tex

\section{Mesh Edit Distance} \label{sec:med}

To display meaningful visual differences and provide robust merges, we need to determine which parts of a mesh have
changed between revisions, and whether the changes have altered the geometry or adjacency of the mesh elements. Inspired
by the string edit distance \cite{sed} used in text version control, we formalize this problem as determining the
partial correspondence between two \rev{matches}{meshes} by minimizing a cost function we term \emph{mesh edit distance}. In this
function, vertices and faces that are unaltered between revisions incur no cost, while we penalize changes in vertex and
face geometry and adjacency. Optimizing this function is equivalent to determining a partial matching between two
meshes, where vertices and faces are either unchanged, altered (either geometrically or in terms of their adjacency), or
added and deleted.

\paragraph{Mesh Edit Distance}
Given two versions of a mesh $M$ and $M'$, we want to determine which elements of one corresponds to  which elements in
the other. In our metric, we consider vertices and faces as the mesh elements. An element $e$ of $M$ is matched if it
has a corresponding one $e'$ in $M'$, while it is unmatched otherwise. A mesh matching is the set of correspondences $O$
between all elements in $M$ to the elements in $M'$. The matching is bidirectional and, in general, partial, in that
some elements will be unmatched, corresponding to addition and deletion of elements during editing. To choose between
the many possible matching, we minimize the \emph{mesh edit distance} $C(O)$, written as the sum of three terms \vspace{-0.1in}

\[													% if you delete the blank line above, the preceding text will not have line #s
	C(O) = C_u(O) + C_g(O) + C_a(O)
\]

\vspace{-0.15in}%
\paragraph{Unmatched Cost $C_u$.}
We penalize unmatched elements, either vertices or faces, by adding a constant cost of $1$ for each element.
Without this cost, one could simply consider all elements of $M$ as deleted and all elements of $M'$ as added.
This can be written as \vspace{-0.15in}

\[													% if you delete the blank line above, the preceding text will not have line #s
	C_u(O) = N_u + N'_u
\]

\vspace{-0.1in}%
where $N_u$ and $N'_u$ are the number of unmatched elements in $M$ and $M'$ respectively.

\paragraph{Geometric Cost $C_g$.}
Matched elements incur two costs. The first captures changes in the geometry of each element, namely its position and
normal. In our initial implementation, we consider meshes with attributes, where vertex positions and face normals are
given, vertex normals are the average normals of the adjacent faces, and face positions are the average position of
adjacent vertices. The geometric cost is given by \vspace{-0.1in}

\[													% if you delete the blank line above, the preceding text will not have line #s
	C_g(O) = \sum_{e \in E} \biggl[
				\frac{d(\mathbf{x}_e, \mathbf{x}_{e'})}{d(\mathbf{x}_e, \mathbf{x}_{e'}) + 1} +
				(1-\mathbf{n}_e \cdot \mathbf{n}_{e'})
			\biggr]
\]

\vspace{-0.1in}%
where $E$ is the set of matched elements $e$ in $M$ with corresponding elements $e'$ in $M'$, 
$\mathbf{x}$ and $\mathbf{n}$ are the position and normal of an element, and $d$ is the Euclidean distance.
We only write this term for $M$ since it is identical in $M'$.

The position term is an increasing, limited function on the Euclidean distance between the elements locations. This
favors matching elements of $M$ to close-by elements in $M'$ and has no cost for matching co-located
elements. We limit the position term to allow for the matching of distant elements, albeit at a penalty. 
We also include an orientation term computed as the dot product between the elements' normals to help in
cases where many small elements are located close to one another.
\rev{In computing the position term, we normalize both meshes so that the average edge has unit length to make it
comparable to the orientation term.}{
To make the position and orientation terms comparable, we normalize both meshes so the average edge over both meshes has unit length.}
\rev[16:redundancy]{}{By including position and orientation costs for vertices and faces, \MeshGit{} can compute directly a cost for matching two elements.}

It should be noted that our implementation assumes that vertices are defined with respect to the same coordinate system during
editing.  We believe this is an acceptable assumption since this is common practice in mesh modeling as gross transformations
and posing of the mesh are generally stored as a separate transformation matrix or armature by the modeling software.  However,
if necessary, we could run an initial global alignment based on ICP \cite{gicp} or a shape-based alignment \cite{dubrovina10}
or allow for a rough manual alignment by painting on corresponding regions. We leave this for future work.

\paragraph{Adjacency Cost $C_a$.}
The geometric costs alone are not sufficient to produce intuitive visual differences since it does not take into account
changes in the elements adjacencies. The exact matching subfigure in \fig{fig:compare}, discussed in the following section, 
shows a more complex example of the benefit of explicitly including element adjacencies.
We assign adjacency costs to pairs of adjacent elements $(e_1,e_2)$ in $M$ and $(e'_1,e'_2)$ in $M'$. 
We consider all adjacencies of faces and vertices (\ie{} face-to-face, face-to-vertex, and vertex-to-vertex).
We include costs for adjacencies that are mismatched between versions and costs for adjacencies that are matched but with 
strongly \rev{difference}{different} geometries. The adjacency term can be written as \vspace{-0.15in}

\begin{align*}										% if you delete the blank line above, the preceding text will not have line #s
	& C_a(O) = \sum_{(e_1,e_2) \in U} \frac{1}{v(e_1)+v(e_2)} + \sum_{(e'_1,e'_2) \in U'} \frac{1}{v(e'_1)+v(e'_2)} + \\
	& \ \ \       + \sum_{(e_1,e_2) \in A} \frac{w(e_1,e_2,e'_1,e'_2)}{v(e_1)+v(e_2)} 
	       + \sum_{(e'_1,e'_2) \in A'} \frac{w(e_1,e_2,e'_1,e'_2)}{v(e'_1)+v(e'_2)} \\
	& \text{with} \ \ \ \  w(e_1,e_2,e'_1,e'_2) = \frac{|d(\mathbf{x}_{e_1},\mathbf{x}_{e_2}) - d(\mathbf{x}_{e'_1},\mathbf{x}_{e'_2})|}
			{d(\mathbf{x}_{e_1},\mathbf{x}_{e_2}) + d(\mathbf{x}_{e'_1},\mathbf{x}_{e'_2})}
\end{align*}

\vspace{-0.1in}%
where $v(e)$ is the valance of a node $e$,
$U$ are the sets of adjacent element pairs $(e_1,e_2)$ in $M$ that do not have matching adjacent pairs
in $M'$, $U'$ is the corresponding set in $M'$, $A$ is the set of adjacent element pairs $(e_1,e_2)$ in $M$
that have matched elements in $M'$, and $A'$ is the corresponding set on $M'$.

The adjacency cost has two terms. The first one, defined symmetrically over both meshes, penalizes mismatches 
in adjacencies between the two meshes when two adjacent elements in a mesh end up not adjacent in the other.
This can happen either if one of them is unmatched or if they are both matched but to non-adjacent elements.
The cost of each mismatch is the inverse of the valence in the graph, \ie{} the size of the local neighborhoods. 
This can be thought of as a normalization that ensures that elements with a large number of adjacencies
(such as extraordinary vertices or poles) are not weighted significantly higher than elements with only a few 
adjacencies (such as vertices at the edges of the model).
Moreover, this normalization works well with meshes that contain a mixture of triangles and quads or has highly regular or irregular adjancencies without the need for user-tunable parameters.

The second term, also defined symmetrically over both meshes, penalizes adjacent pairs that have very different
locations in the two versions with a cost that is proportional to the relative change in location, normalized
by the element valencies.
This term ensures match adjacent pairs of elements to a pair of elements that are relatively the same distance apart, which helps when the mesh has been heavily sculpted.
The term is divided by the size of the local neighborhoods so high-valence elements are not weighted more heavily than low-valence elements.
Note that there is no cost for matched adjacencies when the distance between elements has not changed.


\section{Algorithm}

\paragraph{Equivalent Graph Matching Problem.}
Minimizing the mesh edit distance to determine the optimal mesh matching can be formulated as a
matching problem on a appropriately constructed graph. Given a mesh, we define such a graph by first creating
attributed nodes for each mesh element, where the attributes are the element's geometric properties. We then create an
undirected edge between two nodes in the graph for each adjacency relation between pairs of elements in the mesh.  We
can then determine a good mesh matching by minimizing the mesh edit distance over the graph. Unfortunately, this
matching problem is related to solving a maximum common subgraph isomorphism problem \cite{ged,bunke98}, 
that is known to be \rev{NP-Complete}{NP-Hard} in the general case \rev{\cite{}}{}.  And, while many polynomial-time graph-matching approximation
algorithms have been proposed \cite{gedsurvey}, we found that they do not work well in our problem domain, 
because they either ignore adjacency (i.e. edges in the graph), approximate the adjacencies too greatly, 
or do not scale to thousands of nodes.  In \MeshGit{}, we propose to compute an approximate mesh matching using an 
iterative greedy algorithm that minimizes our cost function.
We include source code and executable for our implementation in supplemental material.

\subsection{Iterative Greedy Algorithm}

% We initialize the matching $O$ with all elements in $M$ and $M'$ unmatched. This initial matching represents a
% transformation from $M$ to $M'$ where all elements of $M$ are removed and all elements of $M'$ are added. By
% construction, this is the highest cost matching.
We initialize the matching $O$ by quickly determining which parts of the mesh have not moved.
The algorithm then iteratively executes a greedy step and a backtracking step. The greedy step minimizes the cost $C(O)$
of the matching $O$ by greedily \rev{assigning}{matching} (or removing \rev{assignment}{the matching between}) elements in $M$ to elements in $M'$.
The backtracking step removes matches that are likely to push the greedy algorithm into local minima of the cost function.
We iteratively repeat these two steps a fixed small number of times (4 in our case).
\Fig{fig:iterdiff} illustrates how $O$ evolves for subsequent iterations.

\input{fig-iterdiff}

\paragraph{Initialization.}
We initialize the matching $O$ by setting each element in one mesh to match its nearest neighbor in the other
mesh if their geometric distance is smaller than an a threshold ($0.1$ in our case). We leave unmatched all other elements. 
This initialization speeds up the matching in that it quickly match elements that have not changed geometrically
and it is experimentally equivalent to initializing with the empty matching. Note that if incorrect assignments
happen, they will be later undone.

\paragraph{Greedy Step.}
The greedy step updates the matching $O$ by consecutively assigning unmatched elements or removing the assignment of
matched ones.  We greedily choose the change that reduces the cost $C(O)$ the most, and we remain in the greedy step
until no change is found that is cheaper to perform than keeping the current matching.
Notice that this may leave some elements unmatched. In practice we found that the greedy step proceeds by growing
patches. This is due to the adjacency term that favors assigning vertices and faces that are adjacent to already matched
ones.

The greedy step may produce unintuitive results since it can get stuck in local minima, 
it may produce face matchings with vertices in an incorrect order, or require duplicating or merging elements. 
\rev{To}{We} handle the local minima with the backtracking step discussed below.
A face match is ill-formed when the vertices are also matched but in an incorrect order.
For example, suppose that a face $f$, defined by vertices $(a,b,c,d)$, matches a face $f'$, defined by
vertices $(a',b',c',d')$, where $a$ matches $a'$, $b$ to $b'$, $c$ to $d'$, and $d$ to $c'$.  We eliminate these
cases by unmatching the vertices of these faces.
While allowing for duplication or merging of elements may be desirable for visualizing certain mesh operations (\eg{}
a loop cut), we take a simplified approach and seek to only visualize added, deleted, or moved elements.
We thus remove such matches by finding and unmatching all adjacent pairs in one mesh that match elements in another mesh 
that are not adjacent, all matching faces with unmatched vertices, and all matched vertices with no matching faces.
We leave visualizing element duplication and merging for future work.

\paragraph{Backtracking Step.}
While we found that in many cases the greedy step alone works well, we encountered a few instances where the algorithm
gets stuck in a local minimum, as shown in \fig{fig:iterdiff}, caused by the order in which the greedy step grows
patches. The geometric term favors assigning nearby elements.  However, if part of the mesh has been sculpted, the
geometric term might favor greedy element assignments that incur small adjacency costs locally, but large overall
adjacency costs as more elements are later added to the matching. This is the case when a region of connected faces 
that have been matched meets the rest of the mesh over mismatched adjacencies. 
These disconnected regions are usually quite small relative to the size of the whole connected component upon which they reside. 
%
These regions are not due to the mesh edit distance we introduced, but to suboptimal initial greedy assignments, 
favored by the geometric term, in sculpted meshes that may also have edits that affect adjacencies.
To eliminate these small regions, we backtrack by removing the assignments of all elements in \rev{}{matching} regions whose size is small
relative to the component size.  The size of a region or component is defined as the number faces in the region or component, respectively.
The threshold ratio is initially set to 8\%.  We run iteratively the greedy and backtracking step four times in total.
To help with convergence and avoiding getting stuck in the same local minimum, 
at each iteration we reduce the geometric cost by a quarter and the backtracking threshold ratio by half. 

\paragraph{Time Complexity.}
The cost of our algorithm is dominated by the iterative search for the minimum cost operations in the greedy step. Since
we perform $O(n)$ assignments, each of which considers $O(n)$ possible cases, a naive implementation of the greedy step
would run in $O(n^2)$ time. Given the geometric terms for vertices and faces in the cost function, we can prune the
search space considerably.  In our implementation, we only consider the $k$ nearest neighbors for each unmatched vertex
or face and the neighbors within $r$ hops in the graph.  We set $k=10$ and $r=2$.
Because these prunings can severely decrease the search space, if an element $e_1$ is unmatched but an adjacent element $e_2$
is matched to $e'_2$, we also search the $k$ nearest neighbors and $r$-ball graph neighborhood of $e'_2$ for potential matches
for $e_1$.  Such a locality of searching considerably reduces the computation time without compromising results even when the
meshes have been heavily sculpted. This reduces the overall cost to $O(n \log n)$.
Furthermore, we compute the change in the cost function with local updates only, since assigning or removing matches
only affects the costs in their local neighborhoods.

\input{fig-compare.tex}

\subsection{Editing Operations}

Given a matching $O$ from a mesh $M$ to another mesh $M'$, we can define a corresponding set of low-level editing operations 
that will transform $M$ into $M'$. Unmatched elements in $M$ are considered deleted, while unmatched elements in $M'$ are added.  
Matched vertices that have a geometric cost are considered transformed (\ie{} translated), while those without geometric costs 
are considered unmodified (thus not highlighted in diffs nor acted on during merging).
Matched faces are considered edited only when they have mismatched adjacencies; in this case, we can consider them as deleted 
from the ancestor and added back in the derivative.  Notice that we do not explicitly account for changes in face geometry since 
they are implicitly taken into account in edits to vertex geometry.

Although the set of mesh transformations produced by this process are very low-level compared to the mesh editing operations in 
a typical 3D modeling software (\eg{} extrude, edge-split, merge vertices), we found that this provides intuitive visualizations 
and allows to robustly merge meshes.  We leave the determination of high-level editing operations to future work.

\subsection{Discussion}

\paragraph{Comparison.}
\fig{fig:compare} shows the results of using different shape matching algorithms to show visual differences.
We included our method, an ``exact'' match based where each element is just match to the closest one (\ie{}
our initialization step only), bipartite graph matching \cite{riesen09}, spectral graph matching \cite{cour06}, shape blending
\cite{kim11}, topological matching \cite{eppstein09}, and iterative closest point with graph cuts \cite{icpcuts}.  The
shape blending and iterative closest point algorithms match vertices only; to generate the visualization, face matches were
inferred. The bipartite, spectral, and topological matching algorithms matched faces instead; we infer from them vertex
matches to visualize our results.  We use the same matching costs for all methods, when applicable.
The input meshes are versions 3 and 4 of the modeling series shown in \fig{fig:diff_durano_series}\footnote{Version 4 in
\fig{fig:compare} was modified to contain only the largest connected component, since the shape blending algorithm
requires a single connected mesh.}. 

\input{fig-diff-chairs.tex}

Matching based on only the closest element within a given radius marks more changes than are actually performed since
adjacency cannot be \rev{}{used} to guide the match in sculpted areas. 
The bipartite graph matching algorithm matched elements, regardless of the implied changes to adjacent elements,
producing a large number of mismatched adjacencies.
The spectral matching and shape blending algorithms do consider adjacencies, but only implicitly, resulting in many
mismatched adjacencies where the graph spectrum changes due  to additional features or blending the matches becomes
fuzzy with additional edge loops or sculpting.  The topological matching algorithm produced topologically consistent
matches regardless of the implied changes to geometry of the vertices, leading to matches that are clumped or shifted
toward the initial seed matching. The iterative closest point with graph cuts algorithm worked to align chunks of the
mesh, but heavy sculpting causes the algorithm to require too many cuts. We found these trends to be present in a
variety of other examples.

It is our opinion that \MeshGit{} is able to better visualize complex edits that include both geometry and adjacency
changes, since it strikes a balance between accounting for both types of changes, compared to other methods that favor
one over the other. This in turn allows us to produce intuitive visualizations as seen throughout the paper. In our
opinion, this is due to the fact that the shape matching algorithms we compared with were not designed specifically for
our problem domain, but for other applications for which they remain remarkably effective. Since there are tradeoffs in
determining good matches in the case of heavily edited meshes, each algorithm makes a tradeoff specific to their problem
domain, and only \MeshGit{} was specifically designed to address version control issues of polygonal meshes.

\paragraph{Limitations.}
The main limitation of \MeshGit{} is that the inclusion of the geometric term has limitation when matching of components
that were very close in one mesh, but have been heavily transformed in the other, if sharp adjacency changes occur also.
Meshes that are heavily sculpted are still handled well since in most cases the adjacency changes are limited. An
example of this limitation is shown in \fig{fig:diff_chairs}, where some of the components of the original chair are
split into separate components that are translated and rotated significantly (\eg{} the front left leg and the left arm
rest). While \MeshGit{} matches well parts of the chairs, the most complex transformations are not detected.
Performing hierarchical matching by matching connected components first followed by the elements of each components can
help, but it would make edits that partition or bridge components difficult to detect.  For an example of such an edit,
the center back support is broken into two parts, and our algorithm can currently detect it. These issues might be
alleviated by using a geodesic or diffusion distance in the geometric term, or additional terms inspired by iterative
closest point \cite{gicp} could be added. At the same time though, we think that changes such as these might make more
common edits undetected, so we leave the exploration of these modifications to future work.

Furthermore, we believe that while \MeshGit{} is very effective for mesh edited in typical subdivision modeling workflows,
it is not as effective on fundamentally different editing workflows, namely the ones that make heavy use of remeshing,
where artists are only concerned about mesh geometry and not adjacency.
Figure~\fignum{fig:diff_dragon} shows one such example. In these cases, the differences shown by \MeshGit{} may be correct, but, 
in our opinion, are less informative for artists, since \MeshGit{} is concerned about changes in both geometry and adjacency,
while artists in these workflows are only concerned about overall shape. We believe that these different workflows are better 
served by algorithms specifically designed for them and leave this to future work.

\input{fig-dragon}

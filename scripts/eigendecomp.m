function eigendecomp()
% [authors of MeshGit], 2012-Jan-12
% http://www.mathworks.com/matlabcentral/fx_files/13464/1/content/manifold/tp4.html
% http://www.mathworks.com/matlabcentral/fileexchange/5355

name0 = 'head_b_tri';
name1 = 'head_g_tri';

addpath( path, '/Users/jdenning/Projects/meshgit.new/models/sintel/' );
addpath( path, '/Users/jdenning/Projects/meshgit.new/toolbox_graph/toolbox_graph/' );
addpath( path, '/Users/jdenning/Projects/meshgit.new/toolbox_graph/toolbox_graph/off/' );
addpath( path, '/Users/jdenning/Projects/meshgit.new/toolbox_graph/toolbox_graph/toolbox/' );

[vertex0,face0,normal0] = read_mesh([name0 '.ply']);
[vertex1,face1,normal1] = read_mesh([name1 '.ply']);

% clf; plot_mesh(vertex0,face0); shading interp; camlight;
% clf; plot_mesh(vertex1,face1); shading interp; camlight;

options.symmetrize=1;
options.normalize=0;
L00 = compute_mesh_laplacian(vertex0,face0,'combinatorial',options);
L01 = compute_mesh_laplacian(vertex1,face1,'combinatorial',options);
[U0,S0,V0] = svds(L00);
[U1,S1,V1] = svds(L01);

clf;

subplot(2,6,1);
options.face_vertex_color = rescale(U0(:,1), 0,255);
plot_mesh(vertex0,face0,options); shading interp; lighting none;
subplot(2,6,2);
options.face_vertex_color = rescale(U0(:,2), 0,255);
plot_mesh(vertex0,face0,options); shading interp; lighting none;
subplot(2,6,3);
options.face_vertex_color = rescale(U0(:,3), 0,255);
plot_mesh(vertex0,face0,options); shading interp; lighting none;
subplot(2,6,4);
options.face_vertex_color = rescale(U0(:,4), 0,255);
plot_mesh(vertex0,face0,options); shading interp; lighting none;
subplot(2,6,5);
options.face_vertex_color = rescale(U0(:,5), 0,255);
plot_mesh(vertex0,face0,options); shading interp; lighting none;
subplot(2,6,6);
options.face_vertex_color = rescale(U0(:,6), 0,255);
plot_mesh(vertex0,face0,options); shading interp; lighting none;

subplot(2,6,7);
options.face_vertex_color = rescale(U1(:,1), 0,255);
plot_mesh(vertex1,face1,options); shading interp; lighting none;
subplot(2,6,8);
options.face_vertex_color = rescale(U1(:,2), 0,255);
plot_mesh(vertex1,face1,options); shading interp; lighting none;
subplot(2,6,9);
options.face_vertex_color = rescale(U1(:,3), 0,255);
plot_mesh(vertex1,face1,options); shading interp; lighting none;
subplot(2,6,10);
options.face_vertex_color = rescale(U1(:,4), 0,255);
plot_mesh(vertex1,face1,options); shading interp; lighting none;
subplot(2,6,11);
options.face_vertex_color = rescale(U1(:,5), 0,255);
plot_mesh(vertex1,face1,options); shading interp; lighting none;
subplot(2,6,12);
options.face_vertex_color = rescale(U1(:,6), 0,255);
plot_mesh(vertex1,face1,options); shading interp; lighting none;



0;


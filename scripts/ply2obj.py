#!/usr/bin/env python

import sys, os, math
from ply import *

if len(sys.argv) != 2:
    print( 'Usage: %s [plyfilename]' % sys.argv[0] )
    exit( 1 )

fnply = sys.argv[1]
fn = os.path.splitext( fnply )[0]
fnobj = '%s.obj' % fn

obj = ply_load( fnply )

print( '%d %d' % (len(obj['lv']),len(obj['lf'])) )

# compute average edge length
el = 0.0
ec = 0
for li_v in obj['lf']:
    lv = [ obj['lv'][i_v] for i_v in li_v ]
    for v0,v1 in zip(lv,lv[1:]+[lv[0]]):
        d2 = (v0[0]-v1[0])**2 + (v0[1]-v1[1])**2 + (v0[2]-v1[2])**2
        d = math.sqrt(d2)
        el += d
        ec += 1
print( 'avg edge length: %f' % (el/float(ec)) )

fp = open( fnobj, 'wt' )
fp.write( '# ply->obj: "%s"\n' % fnply )
for v in obj['lv']:
    #t = tuple( e*10.0 for e in v )
    fp.write( 'v %f %f %f\n' % tuple(v) )
for li_v in obj['lf']:
    ls_i_v = [ '%d'%(i_v+1) for i_v in li_v ]
    fp.write( 'f %s\n' % ' '.join(ls_i_v) )
fp.close()

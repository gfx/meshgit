import math
from vec3f import *

class SortedTruncatedList(object):
    def __init__( self, k ):
        self.k = k
        self.c = 0
        self.lt = [None] * k
    
    def add( self, val, tag ):
        if self.c < self.k:
            self.lt[self.c] = [val,tag]
            self.c += 1
            
        else:
            maxval = self.lt[0][0]
            ir = 0
            for it,t in enumerate(self.lt):
                if t[0] > maxval:
                    maxval = t[0]
                    ir = it
            if val < maxval:
                self.lt[ir] = [val,tag]
    
    def getmax( self ):
        if self.c == 0:
            return None
        maxval = self.lt[0][0]
        ir = 0
        for it in xrange(self.c):
            t = self.lt[it]
            if t[0] > maxval:
                maxval = t[0]
                ir = it
        return self.lt[ir]
    
    def getlist( self ):
        return sorted( [ self.lt[it] for it in xrange(self.c) ], key=lambda v:v[0] )
        #return [ self.lt[it] for it in xrange(self.c) ]

class LRPart(object):
    def __init__( self, d, lv, _depth=0 ):
        if len(lv) == 0:
            self.empty = True
            self.lv = None
            self.pl = None
            self.pr = None
            self.x = None
            self.d = d
            return
        
        if len(lv) == 1 or _depth == 3:
            self.empty = False
            self.lv = lv
            self.pl = None
            self.pr = None
            self.x = lv[0]['t'][d]
            self.d = d
            return
        
        x = sum( v['t'][d] for v in lv ) / float(len(lv))
        d1 = (d+1)%3
        
        lvl = filter( lambda v:v['t'][d] < x, lv )
        lvr = filter( lambda v:v['t'][d] >= x, lv )
        #lvl = [ v for v in lv if v['t'][d] < x ]
        #lvr = [ v for v in lv if v['t'][d] >= x ]
        
        if not lvl:
            _depth += 1
        else:
            _depth = 0
        
        #print( "%i,%f  %s:%s"%(d,x,[v['t'] for v in lvl],[v['t'] for v in lvr]) )
        
        self.empty = False
        self.lv = None
        self.x = x
        self.d = d
        self.v = None
        self.pl = LRPart( d1, lvl, _depth=_depth )
        self.pr = LRPart( d1, lvr, _depth=_depth )
        
    
    def knn( self, t, k ):
        dist = []
        
        if self.empty: return dist
        
        dx = t[self.d] - self.x
        
        if self.lv is not None:
            dist += [ { 'i':v['i'], 'd':Vec3f.t_distance2( t, v['t'] ) } for v in self.lv ]
            dist = sorted( dist, key=lambda v:v['d'] )[:k]
        else:
            if dx < 0:
                dist += self.pl.knn( t, k )
                dist = sorted( dist, key=lambda v:v['d'] )[:k]
                if len(dist) < k or abs(dx) < dist[-1]:
                    dist += self.pr.knn( t, k )
                    dist = sorted( dist, key=lambda v:v['d'] )[:k]
            else:
                dist += self.pr.knn( t, k )
                dist = sorted( dist, key=lambda v:v['d'] )[:k]
                if len(dist) < k or abs(dx) < dist[-1]:
                    dist += self.pl.knn( t, k )
                    dist = sorted( dist, key=lambda v:v['d'] )[:k]
        
        return dist
    
    def knn_stl( self, t, stl ):
        if self.empty: return
        
        dx = t[self.d] - self.x
        
        if self.lv is not None:
            for v in self.lv:
                stl.add( Vec3f.t_distance2( t, v['t'] ), v['i'] )
        else:
            if dx < 0:
                self.pl.knn_stl( t, stl )
                m = stl.getmax()
                if m is None or abs(dx) < m[0]:
                    self.pr.knn_stl( t, stl )
            if dx > 0:
                self.pl.knn_stl( t, stl )
                m = stl.getmax()
                if m is None or abs(dx) < m[0]:
                    self.pl.knn_stl( t, stl )
    
    def rball( self, t, r ):
        dist = []
        if self.empty: return dist
        dx = t[self.d] - self.x
        
        if self.lv is not None:
            d = [ Vec3f.t_distance2( t, v['t'] ) for v in self.lv ]
            dist += [ { 'i':v['i'], 'd':d } for v,d in zip(self.lv,d) if d <= r ]
        else:
            if dx < 0 or abs(dx) <= r:
                dist += self.pl.rball( t, r )
            if dx >= 0 or abs(dx) <= r:
                dist += self.pr.rball( t, r )
        return dist


class KDTree(object):
    def __init__( self, lt ):
        self.lv = sorted( [ { 'i':i, 't':t } for i,t in enumerate(lt) ], key=lambda v:v['t'][0] )
        self.root = LRPart( 0, self.lv )
    
    # returns k nearest neighbors as list of dicts.  'i': index, 'd': distance
    def knn( self, t, k ):
        return self.root.knn( t, k )
    
    def knn_stl( self, t, k ):
        stl = SortedTruncatedList( k )
        self.root.knn_stl( t, stl )
        return [ { 'i':v[1], 'd':v[0] } for v in stl.getlist() ]
    
    def rball( self, t, r ):
        lr = self.root.rball( t, r )
        return sorted( lr, key=lambda r:r['d'] )



if __name__ == '__main__':
    lt = [
        [0,0,0],
        [1,1,1],
        [2,2,2],
        [0,0,2],
        [0,0,-2],
        [2,0,0],
        [-2,0,0],
        [0,2,0],
        [0,-2,0],
        ]
    kdt = KDTree( lt )
    print( lt )
    print( "%dnn %s: %s" % (1,[1,1,1], kdt.knn( [1,1,1], 1 )) )
    print( "%dnn %s: %s" % (2,[1,1,1], kdt.knn( [1,1,1], 2 )) )
    print( "%dnn %s: %s" % (3,[1,1,1], kdt.knn( [1,1,1], 3 )) )
    print( "%dnn %s: %s" % (3,[1,1,0], kdt.knn( [1,1,0], 3 )) )
    print( "%fball %s: %s" % (1.0,[1,1,1], kdt.rball( [1,1,1], 1.0 )) )
    print( "%fball %s: %s" % (2.0,[1,1,1], kdt.rball( [1,1,1], 2.0 )) )
    print( "%fball %s: %s" % (10.0,[1,1,1], kdt.rball( [1,1,1], 10.0 )) )







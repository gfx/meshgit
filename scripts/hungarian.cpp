// from http://community.topcoder.com/tc?module=Static&d1=tutorials&d2=hungarianAlgorithm

#include <memory>
#include <iostream>
#include <algorithm>
#include <math.h>

using namespace std;

//#define N 55             //max number of vertices in one part
#if 1
typedef float           unit;
#define UNITMULT        1.0f
#define INF             INFINITY
#define EPSILON         0.00001f
#else
typedef int             unit;
#define UNITMULT        1000.0
#define INF             1000000
#define EPSILON         0
#endif

// int cost[N][N];          //cost matrix
// int n, max_match;        //n workers and n jobs
// int lx[N], ly[N];        //labels of X and Y parts
// int xy[N];               //xy[x] - vertex that is matched with x,
// int yx[N];               //yx[y] - vertex that is matched with y
// bool S[N], T[N];         //sets S and T in algorithm
// int slack[N];            //as in the algorithm description
// int slackx[N];           //slackx[y] such a vertex, that
//                          // l(slackx[y]) + l(y) - w(slackx[y],y) = slack[y]
// int prev[N];             //array for memorizing alternating paths

int N = 1;

unit *cost;
unit *lx, *ly;
unit *slack;
int *slackx;
int n, max_match;
int *xy;
int *yx;
bool *S,*T;
int *prev;

void malloc_arrays()
{
    cost   = (unit*)malloc( N*N*sizeof(unit) );
    lx     = (unit*)malloc( N*sizeof(unit) );
    ly     = (unit*)malloc( N*sizeof(unit) );
    slack  = (unit*)malloc( N*sizeof(unit) );
    slackx = (int*) malloc( N*sizeof(int) );
    xy     = (int*) malloc( N*sizeof(int) );
    yx     = (int*) malloc( N*sizeof(int) );
    S      = (bool*)malloc( N*sizeof(bool) );
    T      = (bool*)malloc( N*sizeof(bool) );
    prev   = (int*) malloc( N*sizeof(int) );
    
    memset(cost,0,N*N*sizeof(unit));
    memset(xy,0,N*sizeof(int));
    memset(yx,0,N*sizeof(int));
    memset(S,false,N*sizeof(bool));
    memset(T,false,N*sizeof(bool));
    memset(slack,0,N*sizeof(unit));
    memset(slackx,0,N*sizeof(int));
    memset(prev,0,N*sizeof(int));
}

void init_labels()
{
    memset(lx, 0, N*sizeof(unit));
    memset(ly, 0, N*sizeof(unit));
    for (int x = 0; x < n; x++)
        for (int y = 0; y < n; y++)
            lx[x] = max(lx[x], cost[y*N+x]);
}

void update_labels()
{
    int x, y;
    unit delta = INF;             //init delta as infinity
    for (y = 0; y < n; y++)            //calculate delta using slack
        if (!T[y])
            delta = min(delta, slack[y]);
    for (x = 0; x < n; x++)            //update X labels
        if (S[x]) lx[x] -= delta;
    for (y = 0; y < n; y++)            //update Y labels
        if (T[y]) ly[y] += delta;
    for (y = 0; y < n; y++)            //update slack array
        if (!T[y])
            slack[y] -= delta;
}

void add_to_tree(int x, int prevx) 
//x - current vertex,prevx - vertex from X before x in the alternating path,
//so we add edges (prevx, xy[x]), (xy[x], x)
{
    S[x] = true;                    //add x to S
    prev[x] = prevx;                //we need this when augmenting
    for (int y = 0; y < n; y++)    //update slacks, because we add new vertex to S
        if (lx[x] + ly[y] - cost[y*N+x] < slack[y])
        {
            slack[y] = lx[x] + ly[y] - cost[y*N+x];
            slackx[y] = x;
        }
}

void augment()                         //main function of the algorithm
{
    if (max_match == n) return;        //check wether matching is already perfect
    
    int x, y, root;                    //just counters and root vertex
    int q[N], wr = 0, rd = 0;          //q - queue for bfs, wr,rd - write and read
                                       //pos in queue
    memset(S, false, N*sizeof(bool));       //init set S
    memset(T, false, N*sizeof(bool));       //init set T
    memset(prev, -1, N*sizeof(int));    //init set prev - for the alternating tree
    for (x = 0; x < n; x++)            //finding root of the tree
        if (xy[x] == -1)
        {
            q[wr++] = root = x;
            prev[x] = -2;
            S[x] = true;
            break;
        }

    for (y = 0; y < n; y++)            //initializing slack array
    {
        slack[y] = lx[root] + ly[y] - cost[y*N+root];
        slackx[y] = root;
    }
    while (true)                                                        //main cycle
    {
        while (rd < wr)                                                 //building tree with bfs cycle
        {
            x = q[rd++];                                                //current vertex from X part
            for (y = 0; y < n; y++)                                     //iterate through all edges in equality graph
            {
                //if (cost[y*N+x] == lx[x] + ly[y] &&  !T[y])
                unit d = cost[y*N+x] - lx[x] - ly[y];
                if( -EPSILON <= d && d <= EPSILON && !T[y] )
                {
                    if (yx[y] == -1) break;                             //an exposed vertex in Y found, so
                                                                        //augmenting path exists!
                    T[y] = true;                                        //else just add y to T,
                    q[wr++] = yx[y];                                    //add vertex yx[y], which is matched
                                                                        //with y, to the queue
                    add_to_tree(yx[y], x);                              //add edges (x,y) and (y,yx[y]) to the tree
                }
            }
            if (y < n) break;                                           //augmenting path found!
        }
        if (y < n) break;                                               //augmenting path found!

        update_labels();                                                //augmenting path not found, so improve labeling
        wr = rd = 0;                
        for (y = 0; y < n; y++)        
        //in this cycle we add edges that were added to the equality graph as a
        //result of improving the labeling, we add edge (slackx[y], y) to the tree if
        //and only if !T[y] &&  slack[y] == 0, also with this edge we add another one
        //(y, yx[y]) or augment the matching, if y was exposed
            if (!T[y] &&  slack[y] == 0)
            {
                if (yx[y] == -1)                                        //exposed vertex in Y found - augmenting path exists!
                {
                    x = slackx[y];
                    break;
                }
                else
                {
                    T[y] = true;                                        //else just add y to T,
                    if (!S[yx[y]])    
                    {
                        q[wr++] = yx[y];                                //add vertex yx[y], which is matched with
                                                                        //y, to the queue
                        add_to_tree(yx[y], slackx[y]);                  //and add edges (x,y) and (y,
                                                                        //yx[y]) to the tree
                    }
                }
            }
        if (y < n) break;                                               //augmenting path found!
    }

    if (y < n)                                                          //we found augmenting path!
    {
        max_match++;                                                    //increment matching
        //in this cycle we inverse edges along augmenting path
        for (int cx = x, cy = y, ty; cx != -2; cx = prev[cx], cy = ty)
        {
            ty = xy[cx];
            yx[cy] = cx;
            xy[cx] = cy;
        }
        augment();                                                      //recall function, go to step 1 of the algorithm
    }
}//end of augment() function

unit hungarian()
{
    unit ret = 0;                      //weight of the optimal matching
    max_match = 0;                    //number of vertices in current matching
    memset(xy, -1, N*sizeof(int));    
    memset(yx, -1, N*sizeof(int));
    init_labels();                    //step 0
    augment();                        //steps 1-3
    for (int x = 0; x < n; x++)       //forming answer there
        ret += cost[xy[x]*N+x];
    return ret;
}


int main()
{
    FILE *fp;
    float t;
    
    fp = fopen("costmatrix.mtx","rt");
    
    fscanf( fp, "%d %d", &N, &N );
    
    n = max_match = N;
    
    cout << "size: " << N << "x" << N << "\n";
    malloc_arrays();
    
    cout << "loading...\n";
    for( int i = 0; i < N; i++ )
        for( int j = 0; j < N; j++ ) {
            fscanf( fp, "%f", &t );
            cost[j*N+i] = (unit)(t*UNITMULT);
        }
    
    fclose(fp);
    
    cout << "running...\n";
    unit h = hungarian() / UNITMULT;
    cout << "hungarian: " << h << "\n";
    
    for( int x = 0; x < n; x++ )
        cout << x << ": " << xy[x] << ",";
    cout << "\n";
    
    fp = fopen( "assignment.ind", "wt" );
    for( int x = 0; x < n; x++ )
        fprintf( fp, "%d\n", xy[x] );
    fclose(fp);
    
    return 0;
}

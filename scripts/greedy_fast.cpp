#include <iostream>
#include <vector>
#include <set>
#include <string>

#include <math.h>
#include <memory.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "mesh.h"
#include "meshop.h"

using namespace std;


// forward declarations
void initialize_meshop();
float build_correspondence_greedy();

float compute_neighborhood_cost( const vector<int>& liv0, const vector<int>& lif0 );
void recompute_cost_of_pulled();
void compute_op_cost( MeshOp *meshop );


#define TIMER_START( c )        c = clock();
#define TIMER_DELTA( c )        ((double)(clock() - c) / (double)CLOCKS_PER_SEC)



int main( int argc, char **argv )
{
    clock_t c;
    
    TIMER_START(c);
    printf( "loading...\n" );
    load_mesh_data( "greedy_input.txt" );
    printf( "  %.2lfs\n", TIMER_DELTA(c) );
    
    TIMER_START(c);
    printf( "computing knn...\n" );
    compute_knn();
    printf( "  %.2lfs\n", TIMER_DELTA(c) );
    
    TIMER_START(c);
    printf( "initializing...\n" );
    initialize_meshop();
    printf( "  %.2lfs\n", TIMER_DELTA(c) );
    
    TIMER_START(c);
    printf( "building...\n" );
    float cost = build_correspondence_greedy();
    //printf( "  cost = %f\n", cost );
    printf( "  %.2lfs\n", TIMER_DELTA(c) );
    
    TIMER_START(c);
    printf( "saving...\n" );
    save_matching_data( "greedy_output.txt" );
    printf( "  %.2lfs\n", TIMER_DELTA(c) );
    
    printf( "leaving!\n" );
    
    return 0;
}




void initialize_meshop()
{
    vector<int> liv0(20);
    vector<int> lif0(20);
    
    MeshOp::init( nv0+nf0, nv1+nf1 );
    
    for( int iv0 = 0; iv0 < nv0; iv0++ )
    {
        if( vmatch01[iv0] != -1 ) continue;
        for( int i = 0; i < nlsiv01[iv0]; i++ )
        {
            int iv1 = lsiv01[iv0][i];
            if( vmatch10[iv1] != -1 ) continue;
            
            liv0.clear(); lif0.clear();
            
            int *avf0 = lavf0[iv0], navf0 = nlavf0[iv0];
            int *avf1 = lavf1[iv1], navf1 = nlavf1[iv1];
            liv0.push_back( iv0 );
            for( int i = 0; i < navf0; i++ )
                lif0.push_back( avf0[i] );
            for( int i = 0; i < navf1; i++ )
            {
                int if0 = fmatch10[avf1[i]];
                if( if0 != -1 ) lif0.push_back( if0 );
            }
            
            MeshOp::create( iv0, iv1, liv0, lif0 );
        }
    }
    recompute_cost_of_pulled();
    MeshOp::insort_sorted_pull();
    
    for( int iv1 = 0; iv1 < nv1; iv1++ )
    {
        if( vmatch10[iv1] != -1 ) continue;
        for( int i = 0; i < nlsiv10[iv1]; i++ )
        {
            int iv0 = lsiv10[iv1][i];
            if( vmatch01[iv0] != -1 ) continue;
            if( MeshOp::get_meshop( iv0, iv1 ) != 0 ) continue;
            
            liv0.clear(); lif0.clear();
            
            int *avf0 = lavf0[iv0], navf0 = nlavf0[iv0];
            int *avf1 = lavf1[iv1], navf1 = nlavf1[iv1];
            liv0.push_back( iv0 );
            for( int i = 0; i < navf0; i++ )
                lif0.push_back( avf0[i] );
            for( int i = 0; i < navf1; i++ )
            {
                int if0 = fmatch10[avf1[i]];
                if( if0 != -1 ) lif0.push_back( if0 );
            }
            
            MeshOp::create( iv0, iv1, liv0, lif0 );
        }
    }
    recompute_cost_of_pulled();
    MeshOp::insort_sorted_pull();
    
    for( int if0 = 0; if0 < nf0; if0++ )
    {
        if( fmatch01[if0] != -1 ) continue;
        for( int i = 0; i < nlsif01[if0]; i++ )
        {
            int if1 = lsif01[if0][i];
            if( fmatch10[if1] != -1 ) continue;
            
            liv0.clear(); lif0.clear();
            
            int *ifv0 = lifv0[if0], nifv0 = nlifv0[if0];
            int *ifv1 = lifv1[if1], nifv1 = nlifv1[if1];
            int *aff0 = laff0[if0], naff0 = nlaff0[if0];
            int *aff1 = laff1[if1], naff1 = nlaff1[if1];
            
            // build list of affected verts and faces from sub'ing if0->if1
            for( int i = 0; i < nifv0; i++ )
                liv0.push_back( ifv0[i] );
            // add any vertex in m0 that maps to a vertex in m1 which is adj to if1
            for( int i = 0; i < nifv1; i++ )
            {
                int iv0 = vmatch10[ifv1[i]];
                if( iv0 != -1 ) liv0.push_back( iv0 );
            }
            lif0.push_back( if0 );
            for( int i = 0; i < naff0; i++ )
                lif0.push_back( aff0[i] );
            // add any face in m0 that maps to a face in m1 which is adj to if1
            for( int i = 0; i < naff1; i++ )
            {
                int if0_ = fmatch10[aff1[i]];
                if( if0_ != -1 ) lif0.push_back( if0_ );
            }
            
            MeshOp::create( if0+nv0, if1+nv1, liv0, lif0 );
        }
    }
    recompute_cost_of_pulled();
    MeshOp::insort_sorted_pull();
    
    for( int if1 = 0; if1 < nf1; if1++ )
    {
        if( fmatch10[if1] != -1 ) continue;
        for( int i = 0; i < nlsif10[if1]; i++ )
        {
            int if0 = lsif10[if1][i];
            if( fmatch01[if0] != -1 ) continue;
            if( MeshOp::get_meshop( if0+nv0, if1+nv1 ) != 0 ) continue;
            
            liv0.clear(); lif0.clear();
            
            int *ifv0 = lifv0[if0], nifv0 = nlifv0[if0];
            int *ifv1 = lifv1[if1], nifv1 = nlifv1[if1];
            int *aff0 = laff0[if0], naff0 = nlaff0[if0];
            int *aff1 = laff1[if1], naff1 = nlaff1[if1];
            
            // build list of affected verts and faces from sub'ing if0->if1
            for( int i = 0; i < nifv0; i++ )
                liv0.push_back( ifv0[i] );
            // add any vertex in m0 that maps to a vertex in m1 which is adj to if1
            for( int i = 0; i < nifv1; i++ )
            {
                int iv0 = vmatch10[ifv1[i]];
                if( iv0 != -1 ) liv0.push_back( iv0 );
            }
            lif0.push_back( if0 );
            for( int i = 0; i < naff0; i++ )
                lif0.push_back( aff0[i] );
            // add any face in m0 that maps to a face in m1 which is adj to if1
            for( int i = 0; i < naff1; i++ )
            {
                int if0_ = fmatch10[aff1[i]];
                if( if0_ != -1 ) lif0.push_back( if0_ );
            }
            
            MeshOp::create( if0+nv0, if1+nv1, liv0, lif0 );
        }
    }
    
    recompute_cost_of_pulled();
    MeshOp::insort_sorted_pull();
}

float build_correspondence_greedy()
{
    double t_kill   = 0.0;
    double t_recalc = 0.0;
    double t_pull   = 0.0;
    double t_insort = 0.0;
    double t_sort   = 0.0;
    int iterations  = 0;
    
    clock_t c;
    
    MeshOp *meshop;
    
    while( (meshop = MeshOp::pop_cost()) != 0 )
    {
        if( meshop->get_cost() >= 0 ) break;
        
        if( meshop->get_i0() < nv0 ) {
            int iv0 = meshop->get_i0();
            int iv1 = meshop->get_i1();
            
            if( vmatch01[iv0] != -1 || vmatch10[iv1] != -1 ) continue;
            
            // make match
            vmatch01[iv0] = iv1;
            vmatch10[iv1] = iv0;
            
            // kill other potentials of iv0 or iv1
            TIMER_START(c);
            MeshOp::kill_i0( iv0 );
            MeshOp::kill_i1( iv1 );
            t_kill += TIMER_DELTA(c);
            
            int *avf0 = lavf0[iv0], navf0 = nlavf0[iv0];
            int *avf1 = lavf1[iv1], navf1 = nlavf1[iv1];
            
            // pull for cost recomutation
            TIMER_START(c);
            for( int i = 0; i < navf0; i++ ) MeshOp::pull_i0( avf0[i]+nv0 );
            for( int i = 0; i < navf1; i++ ) MeshOp::pull_i1( avf1[i]+nv1 );
            t_pull += TIMER_DELTA(c);
            
        } else {
            int if0 = meshop->get_i0() - nv0;
            int if1 = meshop->get_i1() - nv1;
            
            if( fmatch01[if0] != -1 || fmatch10[if1] != -1 ) continue;
            
            // make match
            fmatch01[if0] = if1;
            fmatch10[if1] = if0;
            
            // kill other potentials of if0 or if1
            TIMER_START(c);
            MeshOp::kill_i0( if0+nv0 );
            MeshOp::kill_i1( if1+nv1 );
            t_kill += TIMER_DELTA(c);
            
            int *afv0 = lifv0[if0], nafv0 = nlifv0[if0];
            int *afv1 = lifv1[if1], nafv1 = nlifv1[if1];
            int *aff0 = laff0[if0], naff0 = nlaff0[if0];
            int *aff1 = laff1[if1], naff1 = nlaff1[if1];
            
            // pull for cost recomutation
            TIMER_START(c);
            for( int i = 0; i < nafv0; i++ ) MeshOp::pull_i0( afv0[i] );
            for( int i = 0; i < nafv1; i++ ) MeshOp::pull_i1( afv1[i] );
            for( int i = 0; i < naff0; i++ ) MeshOp::pull_i0( aff0[i]+nv0 );
            for( int i = 0; i < naff1; i++ ) MeshOp::pull_i1( aff1[i]+nv1 );
            t_pull += TIMER_DELTA(c);
            
        }
        
        TIMER_START(c);
        recompute_cost_of_pulled();
        t_recalc += TIMER_DELTA(c);
        
        TIMER_START(c);
        meshop = MeshOp::sort_pull_quicksort();
        t_sort += TIMER_DELTA(c);
        
        TIMER_START(c);
        MeshOp::insort_cost( meshop );
        t_insort += TIMER_DELTA(c);
        
        iterations++;
    }
    
    printf( "  t_kill     = %.2lfs\n", t_kill );
    printf( "  t_pull     = %.2lfs\n", t_pull );
    printf( "  t_recalc   = %.2lfs\n", t_recalc );
    printf( "  t_sort     = %.2lfs\n", t_sort );
    printf( "  t_insort   = %.2lfs\n", t_insort );
    printf( "  iterations = %d\n", iterations );
    
    return 0.0f;
}




bool is_in( int *list, int size, int find )
{
    for( int i = 0; i < size; i++ ) if( list[i] == find ) return true;
    return false;
}

float compute_neighborhood_cost( const vector<int> &liv0, const vector<int> &lif0 )
{
    float cost = 0.0;
    
    for( int i = 0; i < liv0.size(); i++ )
    {
        int iv0 = liv0[i];
        int iv1 = vmatch01[iv0];
        
        // assuming that vert matching is consistent!
        if( iv1 == -1 ) {
            // unmatched vertex
            cost += cost_vdel + cost_vadd;
        } else {
            // calc cost of substitution
            cost += cost_vdist * v_dist( iv0, iv1 ); // mvd01[iv0][iv1];
            cost += cost_vnorm * v_norm( iv0, iv1 ); // mvn01[iv0][iv1];
        }
    }
    
    for( int i = 0; i < lif0.size(); i++ )
    {
        int if0 = lif0[i];
        int if1 = fmatch01[if0];
        int *ifv0 = lifv0[if0], nifv0 = nlifv0[if0];
        int *aff0 = laff0[if0], naff0 = nlaff0[if0];
        int *ifv1, nifv1;
        int *aff1, naff1;
        
        float fdist = 0.0, fnorm = 0.0;
        int svunk = 0, svmis = 0;
        int smunk = 0, smism = 0;
        
        if( if1 == -1 )  // unmatched face
        {
            /*for( int i = 0; i < nifv0; i++ )
            {
                int iv0 = ifv0[i];
                int iv1 = vmatch01[iv0];
                if( iv1 == -1 ) svunk++;
                else svmis++;
            }
            
            for( int i = 0; i < naff0; i++ )
            {
                int if0 = aff0[i];
                int if1 = fmatch01[if0];
                if( if1 == -1 ) smunk++;
                else smism++;
            }*/
            
            cost += cost_fdel + cost_fadd;
            
        } else {
            ifv1 = lifv1[if1];
            nifv1 = nlifv1[if1];
            aff1 = laff1[if1];
            naff1 = nlaff1[if1];
            
            fdist = f_dist( if0, if1 ); //0.0;
            fnorm = f_norm( if0, if1 );
            
            for( int i = 0; i < nifv0; i++ )
            {
                int iv0 = ifv0[i];
                int iv1 = vmatch01[iv0];
                if( iv1 == -1 ) svunk++;
                else if( !is_in( ifv1, nifv1, iv1 ) ) svmis++;
                //vdist += v_dist( iv0, iv1 );
            }
            if( 0 ) {
                for( int i = 0; i < nifv1; i++ )
                {
                    int iv1 = ifv1[i];
                    int iv0 = vmatch10[iv1];
                    if( iv0 == -1 ) svunk++;
                    else if( !is_in( ifv0, nifv0, iv0 ) ) svmis++;
                    //vdist += mvd01[iv0][iv1]; // already counted above
                }
            }
            for( int i = 0; i < naff0; i++ )
            {
                int if0 = aff0[i];
                int if1 = fmatch01[if0];
                if( if1 == -1 ) smunk++;
                else if( !is_in( aff1, naff1, if1 ) ) smism++;
            }
            if( 0 ) {
                for( int i = 0; i < naff1; i++ )
                {
                    int if1 = aff1[i];
                    int if0 = fmatch10[if1];
                    if( if0 == -1 ) smunk++;
                    else if( !is_in( aff0, naff0, if0 ) ) smism++;
                }
            }
        }
        
        
        cost += cost_fvdist * fdist;// * nifv0;
        cost += cost_fnorm * fnorm;
        
        cost += cost_fvunk * (float)svunk;
        cost += cost_fvmis * (float)svmis;
        
        cost += cost_fmunk * (float)smunk;
        cost += cost_fmism * (float)smism;
    }
    
    return cost;
}

void recompute_cost_of_pulled()
{
    for( MeshOp *m = MeshOp::get_pull_head(); m != 0; m = m->get_n_pull() )
       compute_op_cost( m );
}


void compute_op_cost( MeshOp *meshop )
{
    float cost = 0.0f;
    
    vector<int> liv0(20);
    vector<int> lif0(20);
    
    if( meshop->get_i0() < nv0 ) {
        
        int iv0 = meshop->get_i0(); int iv1 = meshop->get_i1();
        liv0 = meshop->get_liv0(); lif0 = meshop->get_lif0();
        
        cost -= compute_neighborhood_cost( liv0, lif0 );
        vmatch01[iv0] = iv1; vmatch10[iv1] = iv0;
        cost += compute_neighborhood_cost( liv0, lif0 );
        vmatch01[iv0] = vmatch10[iv1] = -1;
        
    } else {
        
        int if0 = meshop->get_i0()-nv0; int if1 = meshop->get_i1()-nv1;
        liv0 = meshop->get_liv0(); lif0 = meshop->get_lif0();
        
        cost -= compute_neighborhood_cost( liv0, lif0 );
        fmatch01[if0] = if1; fmatch10[if1] = if0;
        cost += compute_neighborhood_cost( liv0, lif0 );
        fmatch01[if0] = fmatch10[if1] = -1;
        
    }
    
    meshop->set_cost( cost );
}












import bpy,math,os,subprocess

def exec_wait( arglist ):
    return subprocess.Popen( arglist, stderr=subprocess.STDOUT, stdout=subprocess.PIPE ).communicate()


def do_diff( view ):
    
    fn0 = 'fn0.ply'
    fn1 = 'fn1.ply'
    fnc = 'fnc.txt'
    
    # remember how things are
    prevm = bpy.context.mode.split('_')[0]
    preva = bpy.context.scene.objects.active

    if len(bpy.context.selected_objects) != 2: return
    if not preva.select: return
    
    # get ready to export
    if bpy.context.active_object: bpy.ops.object.mode_set(mode='OBJECT')
    
    obj_orig = preva
    obj_deri = [ o for o in bpy.context.selected_objects if o != obj_orig ][0]
    
    print( 'orig: %s -> %s' % (obj_orig.name,fn0) )
    print( 'deri: %s -> %s' % (obj_deri.name,fn1) )
    
    bpy.ops.object.select_all( action='DESELECT' )
    for o,fn in [(obj_orig,fn0), (obj_deri,fn1)]:
        o.select = True
        bpy.context.scene.objects.active = o
        lb_smooth = [ f.use_smooth for f in o.data.faces ]
        bpy.ops.object.shade_smooth()
        
        bpy.ops.export_mesh.ply( filepath=fn, check_existing=False, use_modifiers=True, use_normals=False, use_uv_coords=False, use_colors=False )
        
        for f,b in zip(o.data.faces,lb_smooth): f.use_smooth = b
        o.select = False
    
    if view:
        exec_wait( [ './scripts/meshgit.py', 'diff', fn0, fn1 ] )
    else:
        print( 'matching...' )
        exec_wait( [ './scripts/meshgit.py', 'save', fn0, fn1, fnc ] )
        print( 'done!' )
        
        fp = open( fnc, 'rt' )
        c_v0,c_f0 = [ int(s) for s in fp.readline()[:-1].split(' ')]
        cost = float(fp.readline()[:-1])
        map_v01 = [ int(fp.readline()[:-1]) for i in range(c_v0) ]
        map_f01 = [ int(fp.readline()[:-1]) for i in range(c_f0) ]
        fp.close()
        
        print( 'cost = %f' % cost )
        c_v1,c_f1 = len(obj_deri.data.vertices),len(obj_deri.data.faces)
        
        map_v10,map_f10 = [-1]*c_v1,[-1]*c_f1
        
        for i_v0,i_v1 in enumerate(map_v01):
            if i_v1 != -1:
                map_v10[i_v1] = i_v0
        for i_f0,i_f1 in enumerate(map_f01):
            if i_f1 != -1:
                map_f10[i_f1] = i_f0
        
        # prep materials, make sure each obj has the proper materials!
        mats,mata,matd = [ bpy.data.materials[n_mat] for n_mat in ['same','added','deleted'] ]
        for o in [obj_orig,obj_deri]:
            for m in [mats,mata,matd]:
                if m not in list(o.data.materials):
                    o.data.materials.append(m)
        
        # assign materials to faces according to matching
        i_ms,i_ma,i_md = [list(obj_orig.data.materials).index(m) for m in [mats,mata,matd]]
        for i_f0,f0 in enumerate(obj_orig.data.faces):
            if map_f01[i_f0] != -1:
                f0.material_index = i_ms
            else:
                f0.material_index = i_md
        i_ms,i_ma,i_md = [list(obj_deri.data.materials).index(m) for m in [mats,mata,matd]]
        for i_f1,f1 in enumerate(obj_deri.data.faces):
            if map_f10[i_f1] != -1:
                f1.material_index = i_ms
            else:
                f1.material_index = i_ma
    
    # reset back to how things were
    obj_deri.select = True
    obj_orig.select = True
    bpy.context.scene.objects.active = obj_orig
    bpy.ops.object.mode_set(mode=prevm)
    
    obj_orig.data.update()
    obj_deri.data.update()


do_diff( view=True )




#
#class MGVertex(object):
#    
#    gu_v = -1
#    @classmethod get_u_v( cls ):
#        cls.gu_v += 1
#        return 'v%05i' % cls.gu_v
#    
#    def __init__( self, co, u_v=None ):
#        self.u_v = u_v or MGVertex.get_u_v()
#        self.co = co
#
#class MGEdge( object ):
#    pass
#
#class MGFace( object ):
#    pass
#
#class MGMesh(object):
#    def __init__( self, lco_v, lliv_e, lliv_f ):
#        self.lv = [ MGVertex( co ) for co in lco_v ]
#        self.le = [ MGEdge( liv_e ) for liv_e in lliv_e ]
#        self.lf = [ MGFace( liv_f ) for liv_f in lliv_f ]
#    
#    @classmethod
#    def from_bpymesh( self, mesh ):
#        lco_v = [ v.co for v in mesh.vertices ]
#        lliv_e = [ e.vertices for e in mesh.edges ]
#        lliv_f = [ f.vertices for f in mesh.faces ]
#        mgmesh = MGMesh( lco_v, lliv_e, lliv_f )
#        return mgmesh
#    
#    def to_bpymesh( self ):
#        lco_v = [ v.co for v in self.lv ]
#        lliv_f = [ f.li_v for f in self.lf ]
#        
#
#class Vertex:
#    gu_v = -1
#    
#    @classmethod
#    def getu_v( cls ):
#        cls.gu_v += 1
#        return 'v%02i' % cls.gu_v
#    
#    def __init__( self, u_v=None, p=None, n=None, c=None ):
#        self.u_v = u_v or Vertex.getu_v()
#        self.p = p or [0,0,0]
#        self.n = n or [1,0,0]
#        self.c = c or [0.5,0.5,0.5]
#    # returns Vertex with new UID
#    def copy( self ):
#        return Vertex( p=list(self.p), n=list(self.n), c=list(self.c) )
#    # returns exact copy of self (same UID)
#    def clone( self ):
#        return Vertex( u_v=str(self.u_v), p=list(self.p), n=list(self.n), c=list(self.c) )
#        #return copy.deepcopy( self )
#    
#    def getx( self ):
#        return self.p[0]
#    def gety( self ):
#        return self.p[1]
#    def getz( self ):
#        return self.p[2]
#    
#    @classmethod
#    def distance( cls, v0, v1 ):
#        return math.sqrt( sum([ (a-b)*(a-b) for a,b in zip(v0.p,v1.p) ]) )
#    @classmethod
#    def distance2( cls, v0, v1 ):
#        #return sum([ (a-b)*(a-b) for a,b in zip(v0.p,v1.p) ])
#        x,y,z=v0.p[0]-v1.p[0],v0.p[1]-v1.p[1],v0.p[2]-v1.p[2]
#        return x*x+y*y+z*z
#    
#    @classmethod
#    def average( cls, lv ):
#        l = len(lv)
#        x = sum(v.p[0] for v in lv) / float(l)
#        y = sum(v.p[1] for v in lv) / float(l)
#        z = sum(v.p[2] for v in lv) / float(l)
#        return x,y,z
#    
#    def translate( self, t ):
#        self.p = [ pc+tc for pc,tc in zip(self.p,t) ]
#        return self
#    
#    def recolor( self, c ):
#        self.c = c
#        return self
#    
#
#class Face:
#    gu_f = -1
#    
#    @classmethod
#    def getu_f( cls ):
#        cls.gu_f += 1
#        return 'f%02i' % cls.gu_f
#    
#    def __init__( self, u_f=None, lu_v=None ):
#        self.u_f = u_f or Face.getu_f()
#        self.lu_v = lu_v
#    
#    def size(self):
#        return len(self.lu_v)
#    
#    def copy( self ):
#        return Face( lu_v=list(self.lu_v) )
#    
#    def clone( self ):
#        return Face( u_f=str(self.u_f), lu_v=list(self.lu_v) )
#    
#    def get_quad_lu_v( self ):
#        if len(self.lu_v) == 4: return self.lu_v
#        if len(self.lu_v) == 3: return self.lu_v + [self.lu_v[0]]
#        if len(self.lu_v) == 2: return [self.lu_v[0],self.lu_v[0],self.lu_v[1],self.lu_v[1]]
#        assert False, 'ngons not supported at this time'
#    
#    # compares uids of verts to determine if same
#    def same( self, f ):
#        if isinstance(f,Face): return set(self.lu_v) == set(f.lu_v)
#        if type(f) is str: return self.u_f == f
#        if type(f) is list: return set(self.lu_v) == set(f)
#        assert False, 'unknown type for comparison: %s, %s' % (f,type(f))
#    
#    @classmethod
#    def calc_normal( cls, lv ):
#        norm = (0.0,0.0,0.0)
#        l = len(lv)
#        for i in xrange(l):
#            i0,i1,i2 = i,(i+1)%l,(i+2)%l
#            norm = Vec3f.t_add( norm, cls._calc_normal( [ lv[i0], lv[i1], lv[i2] ] ) )
#        return Vec3f.t_norm( norm )
#    
#    @classmethod
#    def _calc_normal( cls, lv ):
#        ax,ay,az = [ lv[2].p[0]-lv[1].p[0], lv[2].p[1]-lv[1].p[1], lv[2].p[2]-lv[1].p[2] ]
#        bx,by,bz = [ lv[0].p[0]-lv[1].p[0], lv[0].p[1]-lv[1].p[1], lv[0].p[2]-lv[1].p[2] ]
#        al,bl = math.sqrt(ax*ax+ay*ay+az*az),math.sqrt(bx*bx+by*by+bz*bz)
#        ax,ay,az = (ax/al,ay/al,az/al) if al != 0.0 else (0.0,0.0,0.0)
#        bx,by,bz = (bx/bl,by/bl,bz/bl) if bl != 0.0 else (0.0,0.0,0.0)
#        cx,cy,cz = ay*bz-az*by,az*bx-ax*bz,ax*by-ay*bx
#        cl = math.sqrt(cx*cx+cy*cy+cz*cz)
#        return (cx/cl,cy/cl,cz/cl) if cl != 0.0 else (0.0,0.0,0.0)
#    
#    @classmethod
#    def calc_area( cls, lv ):
#        # heron's formula
#        # note: "incorrect" if quad and not planar
#        la = Vec3f.t_distance( lv[0], lv[1] )
#        lb = Vec3f.t_distance( lv[1], lv[2] )
#        lc = Vec3f.t_distance( lv[2], lv[0] )
#        lp = (la + lb + lc) / 2.0
#        a = math.sqrt( lp*(lp-la)*(lp-lb)*(lp-lc) )
#        if len(lv) == 4:
#            la = Vec3f.t_distance( lv[2], lv[3] )
#            lb = Vec3f.t_distance( lv[3], lv[0] )
#            lc = Vec3f.t_distance( lv[0], lv[2] )
#            lp = (la + lb + lc) / 2.0
#            a += math.sqrt( lp*(lp-la)*(lp-lb)*(lp-lc) )
#        return a
#
#class Edge:
#    gu_e = -1
#    
#    @classmethod
#    def getu_e( cls ):
#        cls.gu_e += 1
#        return 'e%02i' % cls.gu_e
#    
#    def __init__( self, u_e=None, lu_v=None ):
#        self.u_e = u_e or Edge.getu_e()
#        self.lu_v = lu_v
#    def copy( self ):
#        return Edge( lu_v=list(self.lu_v) )
#    def clone( self ):
#        return Edge( u_e=str(self.u_e), lu_v=list(self.lu_v) )
#    
#    def same( self, e ):
#        if isinstance(e,Edge): return set(self.lu_v) == set(e.lu_v)
#        if type(e) is str: return self.u_e == e
#        if type(e) is list: return set(self.lu_v) == set(e)
#        assert False, 'unknown type for comparison: %s, %s' % (e,type(e))
#
#class Mesh:
#    
#    #######################################
#    # constructors
#    #######################################
#    
#    
#    def __init__( self, lv=None, le=None, lf=None ):
#        self.lv = lv or []
#        self.le = le
#        self.lf = lf or []
#        self._hlv = None
#        self._hlf = None
#    
#    def copy( self, copy_verts=True, copy_faces=True, ignore_edges=False ):
#        assert copy_verts or not copy_faces, 'must copy verts to copy faces'
#        mu_viv = self._get_u_v_i_v() if copy_faces else None
#        lv = [ v.copy() for v in self.lv ] if copy_verts else None
#        le = [ Edge( lu_v=[ lv[mu_viv[u_v]].u_v for u_v in e.lu_v ] ) for e in self.le ] if copy_faces and not ignore_edges else None
#        lf = [ Face( lu_v=[ lv[mu_viv[u_v]].u_v for u_v in f.lu_v ] ) for f in self.lf ] if copy_faces else None
#        return Mesh( lv=lv, le=le, lf=lf )
#    
#    def clone( self, clone_verts=True, clone_faces=True ):
#        assert clone_verts or not clone_faces, 'must clone verts to clone faces'
#        lv = [ v.clone() for v in self.lv ] if clone_verts else None
#        le = [ e.clone() for e in self.le ] if clone_faces and self.le else None
#        lf = [ f.clone() for f in self.lf ] if clone_faces else None
#        return Mesh( lv=lv, le=le, lf=lf )
#    
#    def clone_filter_faces( self, su_f, filterto ):
#        lv = [ v.clone() for v in self.lv ]
#        if filterto:
#            lf = [ f for f in self.lf if f.u_f in su_f ]
#        else:
#            lf = [ f for f in self.lf if f.u_f not in su_f ]
#        return Mesh( lv=lv, lf=lf )
#    
#    ###########################################
#    # skeletonize the mesh.  returned mesh is reduced mesh of self, labeled similarly.
#    # Approximate Topological Matching of Quadrilateral Meshes
#    # David Eppstein and Michael T. Goodrich and Ethan Kim and Rasmus Tamstorf
#    ###########################################
#    
#    # note: this function does not perform the full skeletonization as described
#    # in the paper.  the skeleton form is only used to speed-up the initial seeding.
#    # here, we use a quad-only form of the mesh
#    
#    def skeletonize( self ):
#        
#        DebugWriter.start( 'skeletonizing mesh' )
#        
#        self.optimize()
#        
#        # for now, just remove any non-quad faces
#        mesh = self.clone()
#        mesh.lf = [ f for f in mesh.lf if len(f.lu_v) == 4 ]
#        mesh.trim()
#        mesh.rebuild_edges()
#        mesh.optimize()
#        
#        DebugWriter.report( 'counts', {
#            'verts': [ len(self.lv), len(mesh.lv) ],
#            'faces': [ len(self.lf), len(mesh.lf) ],
#            } )
#        
#        DebugWriter.end()
#        
#        return mesh
#    
#    
#    
#    #######################################
#    # import / export (converters)
#    #######################################
#    
#    
#    @classmethod
#    def fromPLY( cls, fn ):
#        DebugWriter.start( 'loading mesh from ply' )
#        plyobj = ply.ply_load( fn )
#        lv = [ Vertex( p=v[0:3] ) for v in plyobj['lv'] ]
#        lf = [ Face( lu_v=[ lv[i_v].u_v for i_v in f ] ) for f in plyobj['lf'] ]
#        m = Mesh( lv=lv, lf=lf )
#        DebugWriter.end()
#        return m
#    
#    def toPLY( self, fn, use_colors=True ):
#        DebugWriter.start( 'writing mesh to ply' )
#        plyobj = {}
#        plyobj['lv'] = [ v.p for v in self.lv ]
#        if use_colors: plyobj['lc'] = [ [ int(c*255) for c in v.c ] for v in self.lv ]
#        plyobj['lf'] = [ ([f.size()]+self._get_f_li_v(f)) for f in self.lf ]
#        ply.ply_save( fn, plyobj )
#        DebugWriter.end()
#    
#    def toShape( self, smooth=False, use_alpha=False ):
#        DebugWriter.start( 'converting mesh to shape' )
#        
#        self.optimize()
#        
#        lflv = [ ( f, self.opt_get_f_lv_quad(f) ) for f in self.lf ]
#        
#        verts   = [ c for f,lv in lflv for v in lv for c in v.p ]
#        colors  = [ c for f,lv in lflv for v in lv for c in v.c ]
#        if smooth:  norms = [ c for f,lv in lflv for v in lv for c in v.n ]
#        else:       norms = [ c for f,lv in lflv for v in lv for c in Face.calc_normal(lv) ]
#        
#        s = Shape( GL_QUADS, verts, colors, norms, use_alpha=use_alpha ) if verts else None
#        
#        DebugWriter.end()
#        
#        return s
#    
#    def __str__(self):
#        return 'lv: [%s]\nlf: [%s]' % (
#            ','.join([v.u_v for v in self.lv]),
#            ','.join(['%s:[%s]' % ( f.u_f, ','.join([u_v for u_v in f.lu_v]) ) for f in self.lf] )
#            )
#    
#    
#    #######################################
#    # catmull-clark subd
#    #######################################
#    
#    
#    def catmull_clark_subd( self, levels ):
#        
#        if levels <= 0:
#            #lu_v = self.get_lu_v()
#            #lu_f = self.get_lu_f()
#            #lu_e = [ e.u_e for e in self.le ]
#            #drmap = dict( (u,u) for u in lu_v+lu_f+lu_e )
#            #return drmap,self.recalc_vertex_normals()
#            return self.recalc_vertex_normals() #.rebuild_edges()
#        
#        DebugWriter.start( 'subdividing: catmull-clark' )
#        
#        d_map = {}
#        
#        self.optimize()
#        self.rebuild_edges()
#        fn_uv = self.opt_get_v
#        fn_uf = self.opt_get_f
#        
#        stats = self.get_stats()
#        dsv_e = dict( ( frozenset(e.lu_v), e ) for e in self.le )
#        
#        du_v = dict( ( v.u_v, {
#                'v':        v,
#                'u_v':      v.u_v,
#                'su_f':     stats['adj_v'][v.u_v]['su_af'],
#                'su_e':     set(),
#            } ) for v in self.lv )
#        
#        du_e = {}
#        for e in self.le:
#            du_e[e.u_e] = {
#                'e':        e,
#                'u_e':      e.u_e,
#                'lu_v':     e.lu_v,
#                'color':    None,
#                'centroid': None,
#                'u_ev':     None,
#                'su_f':     set(),
#            }
#            for u_v in e.lu_v:
#                du_v[u_v]['su_e'].add( e.u_e )
#        
#        du_f = {}
#        for f in self.lf:
#            du_f[f.u_f] = {
#                'f':        f,
#                'u_f':      f.u_f,
#                'lu_v':     f.lu_v,
#                'color':    Vec3f.t_average( [ fn_uv(u_v).c for u_v in f.lu_v ] ),
#                'centroid': Vec3f.t_average( [ fn_uv(u_v).p for u_v in f.lu_v ] ),
#                'lu_e':     [ dsv_e[ frozenset([uv0,uv1]) ].u_e for (uv0,uv1) in zip_self_offset(f.lu_v) ],
#                'lu_af':    [ [ u_af for u_af in stats['adj_f'][f.u_f]['su_afe'] if uv0 in fn_uf(u_af).lu_v and uv1 in fn_uf(u_af).lu_v ] for (uv0,uv1) in zip_self_offset(f.lu_v) ],
#                'u_fv':     None,
#                }
#            for u_e in du_f[f.u_f]['lu_e']:
#                du_e[u_e]['su_f'].add( f.u_f )
#        
#        lv,lf = [],[]
#        
#        # compute face points
#        for finfo in du_f.values():
#            fv = Vertex( p=finfo['centroid'],c=finfo['color'] )
#            lv += [ fv ]
#            finfo['u_fv'] = fv.u_v
#            d_map[ fv.u_v ] = finfo['lu_v'][0] #finfo['u_f']
#        
#        # compute edge points
#        for einfo in du_e.values():
#            lp = [ fn_uv(u_v).p for u_v in einfo['lu_v'] ]
#            einfo['centroid'] = Vec3f.t_average( lp )
#            einfo['color'] = Vec3f.t_average( [ fn_uv(u_v).c for u_v in einfo['lu_v'] ] )
#            if len(einfo['su_f']) == 2:
#                lp += [ du_f[u_f]['centroid'] for u_f in einfo['su_f'] ]
#            ev = Vertex( p=Vec3f.t_average( lp ), c=einfo['color'] )
#            einfo['u_ev'] = ev.u_v
#            lv += [ ev ]
#            d_map[ ev.u_v ] = einfo['lu_v'][0] #einfo['u_e']
#        
#        # reposition original verts
#        for v in self.lv:
#            nv = v.clone()
#            su_f = du_v[nv.u_v]['su_f']
#            su_e = du_v[nv.u_v]['su_e']
#            P = nv.p
#            if len(su_f) == len(su_e):
#                F = Vec3f.t_average( [ du_f[u_f]['centroid'] for u_f in su_f ] )
#                R = Vec3f.t_average( [ du_e[u_e]['centroid'] for u_e in su_e ] )
#                n = len( su_f )
#                nv.p = Vec3f.t_scale( Vec3f.t_add( Vec3f.t_add( F, Vec3f.t_scale( R, 2.0 ) ), Vec3f.t_scale( P, float(n-3) ) ), 1.0/float(n) )
#            else:
#                lev = [ du_e[u_e]['centroid'] for u_e in su_e if du_e[u_e]['su_f'] == 1 ]
#                nv.p = Vec3f.t_average( [P] + lev )
#            lv += [nv]
#            d_map[ nv.u_v ] = nv.u_v
#        
#        # create new faces
#        for finfo in du_f.values():
#            u_fv = finfo['u_fv']
#            lu_vv = finfo['lu_v']
#            lu_ev = [ du_e[u_e]['u_ev'] for u_e in finfo['lu_e'] ]
#            c = len(lu_vv)
#            
#            lnf = [ Face( lu_v=[
#                lu_ev[(i + c - 1 ) % c],
#                lu_vv[i],
#                lu_ev[i],
#                u_fv,
#                ] ) for i in xrange(c) ]
#            lf += lnf
#            for nf in lnf: d_map[nf.u_f] = finfo['u_f']
#        
#        rmesh = Mesh( lv=lv, lf=lf ).catmull_clark_subd( levels - 1 ).rebuild_edges()
#        DebugWriter.end()
#        
#        ## reverse mapping
#        #drmap = dict( (v,[]) for v in d_map.values()
#        #for k,v in d_map.iteritems(): drmap[v] += [ k ]
#        #(rmap,rmesh) = Mesh( lv=lv, lf=lf ).catmull_clark_subd( levels - 1 )
#        ## composite mappings
#        #nmap = dict( (k,[rmap[v] for v in lv]) for k,lv in drmap.iteritems() )
#        #return (nmap,rmesh)
#        
#        #DebugWriter.start( 'relabeling' )
#        #for v in rmesh.lv:
#            #v.u_v = d_map[v.u_v]
#        #for e in rmesh.le:
#            ##e.u_e = d_map[e.u_e]
#            #e.lu_v = [ d_map[u_v] for u_v in e.lu_v ]
#        #for f in rmesh.lf:
#            #f.u_f = d_map[f.u_f]
#            #f.lu_v = [ d_map[u_v] for u_v in f.lu_v ]
#        #DebugWriter.end()
#        
#        
#        return rmesh
#    
#    
#    #######################################
#    # misc
#    #######################################
#    
#    
#    def get_vert_bounding_box( self ):
#        mp = [ float('Inf') ] * 3
#        Mp = [ -float('Inf') ] * 3
#        for v in self.lv:
#            mp = Vec3f.t_min( mp, v.p )
#            Mp = Vec3f.t_max( Mp, v.p )
#        return (mp,Mp)
#    
#    def recalc_vertex_normals( self ):
#        DebugWriter.start( 'recalculating vertex normals' )
#        
#        vec_add = Vec3f.t_add
#        vec_norm = Vec3f.t_norm
#        face_norm = Face._calc_normal
#        
#        self.optimize()
#        
#        ln = [[0,0,0] for v in self.lv]
#        lnc = [0]*len(self.lv)
#        for i_f,f in enumerate(self.lf):
#            li_v = self._opt_get_f_li_v( f )
#            lv = [ self.lv[i_v] for i_v in li_v ]
#            
#            #n = Face.calc_normal( lv )
#            n = (0.0,0.0,0.0)
#            for tlv in zip( lv, lv[1:]+[lv[0]], lv[2:]+lv[:2] ):
#                n = vec_add( n, face_norm( tlv ) )
#            n = vec_norm( n )
#            
#            for i_v in li_v:
#                ln[i_v][0] += n[0]
#                ln[i_v][1] += n[1]
#                ln[i_v][2] += n[2]
#                lnc[i_v] += 1
#        
#        for i_v,n in enumerate(ln):
#            if not lnc[i_v]: continue
#            n[0] /= lnc[i_v]
#            n[1] /= lnc[i_v]
#            n[2] /= lnc[i_v]
#            l = math.sqrt( n[0]*n[0] + n[1]*n[1] + n[2]*n[2] )
#            if l == 0: self.lv[i_v].n = [ 0.0, 0.0, 0.0 ]
#            else: self.lv[i_v].n = [ n[0]/l,n[1]/l,n[2]/l ]
#        
#        DebugWriter.end()
#        
#        return self
#    
#    def rebuild_edges( self ):
#        DebugWriter.start( 'rebuilding edges' )
#        
#        self.le = []
#        sv = set()
#        for i_f,f in enumerate(self.lf):
#            for lu_v in zip(f.lu_v[:-1],f.lu_v[1:]) + [(f.lu_v[-1],f.lu_v[0])]:
#                nsv = frozenset( lu_v )
#                if nsv in sv: continue
#                sv.add( nsv )
#                self.le.append( Edge( lu_v=lu_v ) )
#        
#        DebugWriter.end()
#        
#        return self
#    
#    #######################################
#    # getters
#    #######################################
#    
#    
#    # get dict from vert uid to vert ind
#    def _get_u_v_i_v( self ):
#        #return { v.u_v:i_v for (i_v,v) in enumerate(self.lv) }
#        return dict( (v.u_v,i_v) for (i_v,v) in enumerate(self.lv) )
#    
#    # get vert ind from vert uid
#    def _get_i_v( self, u_v ):
#        return [ i_v for i_v,v in enumerate(self.lv) if v.u_v == u_v ][0]
#    
#    # get a list of vert inds for a given face
#    def _get_f_li_v( self, f ):
#        if type(f) is str: f = [ f for f in self.lf if f.u_f == f ][0].lu_v
#        elif type(f) is int: f = self.lf[f].lu_v
#        elif isinstance(f,Face): f = f.lu_v
#        uviv = self._get_u_v_i_v()
#        li_v = [ uviv[u_v] for u_v in f ]
#        
#        return li_v
#    
#    def get_v( self, u_v ):
#        return [ v for v in self.lv if v.u_v == u_v ][0]
#    
#    def get_f( self, u_f ):
#        return [ f for f in self.lf if f.u_f == u_f ][0]
#    
#    def get_lu_f( self ):
#        return [ f.u_f for f in self.lf ]
#    
#    def get_lu_v( self ):
#        return [ v.u_v for v in self.lv ]
#    
#    def elv( self ):
#        for i_v,v in enumerate( self.lv ):
#            yield (i_v,v)
#    def elf( self ):
#        for i_f,f in enumerate( self.lf ):
#            yield (i_f,f)
#    
#    def get_stats( self, build_strips=False ):
#        DebugWriter.start( 'constructing mesh stats' )
#        
#        DebugWriter.start( 'building adjacency data' )
#        
#        DebugWriter.start( 'initializing adjacency structs' )
#        adj_v = dict( ( v.u_v, {
#            'u_v':   v.u_v,                                                                 # uid of vert (redundant from key)
#            'su_nv': set(),                                                                 # neighboring verts (share edge)
#            'su_ae': set(),                                                                 # adj edges as set of frozensets of vert uids
#            'su_af': set(),                                                                 # set of adj faces
#            } ) for v in self.lv )
#        adj_e = {}                                                                          # keyed by frozenset of vert uids for each edge
#        adj_f = dict( ( f.u_f, {
#            'u_f': f.u_f,                                                                   # uid of face (redundant from key)
#            #'lsuv': [ frozenset(luv) for luv in zip( f.lu_v, f.lu_v[1:] + [f.lu_v[0]] ) ],  # list of edges as frozensets of vert uids
#            'lsuv': [ frozenset(luv) for luv in zip_self_offset( f.lu_v ) ],                # list of edges as frozensets of vert uids
#            'su_afe': set(),                                                                # set of adj faces, sharing an edge
#            'su_afv': set(), #set( f2.u_f for f2 in self.lf if len(set(f2.lu_v) & set(f.lu_v)) ),   # set of adj faces, sharing a vertex
#            } ) for f in self.lf )
#        DebugWriter.end()
#        
#        DebugWriter.start( 'vert-edge and vert-face adjacencies, edge-rings' )
#        for f in self.lf:
#            lsuv = adj_f[f.u_f]['lsuv']
#            
#            for i,suv in enumerate(lsuv):
#                v0,v1 = suv
#                adj_v[v0]['su_ae'].add( suv )
#                adj_v[v0]['su_af'].add( f.u_f )
#                adj_v[v1]['su_ae'].add( suv )
#                adj_v[v1]['su_af'].add( f.u_f )
#                
#                ae = adj_e.setdefault( suv, {                                   # get edge, initialized as blank
#                    'lu_af': [],                                                # list of adj faces (by uid).  TODO: can be set?
#                    'le_r': [],                                                 # edge-ring. NOTE: only applicable if adj to quad
#                    } )
#                ae['lu_af'].append( f.u_f )                                     # add face to list of adjacent faces
#            
#            # build edge-ring data.  NOTE: only defined for quads!!
#            if len(lsuv) == 4:
#                for i,suv in enumerate(lsuv):
#                    adj_e[suv]['le_r'].append( lsuv[(i+2)%4] )
#        DebugWriter.end()
#        
#        DebugWriter.start( 'face-vert-face' )
#        for f in self.lf:
#            s = set()
#            for u_v in f.lu_v:
#                s |= adj_v[u_v]['su_af']
#            s.remove( f.u_f )
#            adj_f[f.u_f]['su_afv'] = s
#        DebugWriter.end()
#        
#        DebugWriter.start( 'finding adj faces over a shared edge' )
#        # find adjacent faces over a shared edge
#        for suv,einfo in adj_e.iteritems():
#            lu_f = einfo['lu_af']
#            for u_f in lu_f:
#                adj_f[u_f]['su_afe'] |= ( set(lu_f) - set([u_f]) )              # NOTE: not adjacent to self
#        DebugWriter.end()
#        
#        DebugWriter.start( 'vert-vert neighborhood' )
#        for u_v,vinfo in adj_v.iteritems():
#            su_e = vinfo['su_ae']
#            vinfo['su_nv'] = set( u for suv in su_e for u in suv ) - set([u_v])
#            assert len(vinfo['su_ae']) == len(vinfo['su_nv']), 'Not Same Length for %s!\n%s\n%s' % (u_v,su_e,vinfo['su_nv'])
#        DebugWriter.end()
#        
#        DebugWriter.end()
#        
#        
#        ##########
#        # strips #
#        
#        if build_strips:
#            DebugWriter.start( 'Building strips data' )
#            lring_sf = list()
#            for u_f,f in adj_f.items():
#                lsuv = f['lsuv']
#                for suv0,suv1 in zip(lsuv[:2],lsuv[2:]):
#                    sf = set([u_f])
#                    # now, crawl the ring (both ways), touching each edge
#                    for pe,ce,u_cf in [(suv1,suv0,u_f), (suv0,suv1,u_f)]:
#                        while True:
#                            ler = adj_e[ce]['le_r']
#                            lf = adj_e[ce]['lu_af']
#                            added = False
#                            for ne,u_nf in zip(ler,lf):
#                                if u_nf == u_cf: continue
#                                if u_nf in sf: break
#                                sf.add(u_nf)
#                                pe,ce = ce,ne
#                                u_cf = u_nf
#                                added = True
#                            if not added: break
#                    if not any( sf == ring_sf for ring_sf in lring_sf ):
#                        lring_sf.append( sf )
#            DebugWriter.end()
#            
#            DebugWriter.start( 'Computing strip adjacency matrix' )
#            mra = [[False for j in xrange(len(lring_sf))] for i in xrange(len(lring_sf))]
#            for i,ri in enumerate(lring_sf):
#                for j,rj in enumerate(lring_sf):
#                    if j <= i: continue
#                    for u_fi in ri:
#                        for u_fj in rj:
#                            if u_fj in adj_f[u_fi]['su_afe']:
#                                mra[i][j] = mra[j][i] = True
#                                break
#                        if mra[i][j]: break
#            DebugWriter.report( 'strips', len(lring_sf) )
#            DebugWriter.end()
#        else:
#            lring_sf = None
#            mra = None
#        
#        # strips #
#        ##########
#        
#        
#        DebugWriter.end()
#        
#        return {
#            'adj_v':    adj_v,
#            'adj_e':    adj_e,
#            'adj_f':    adj_f,
#            'mra':      mra,
#            'lring_sf': lring_sf,
#            }
#    
#    
#    
#    #######################################
#    # optimized funcs
#    #######################################
#    
#    
#    def optimize( self ):
#        hlv = md5.md5(str(self.lv))
#        hlf = md5.md5(str(self.lf))
#        if self._hlv == hlv and self._hlf == hlf: return
#        
#        DebugWriter.start( 'optimizing' )
#        
#        self._hlv = hlv
#        self._hlf = hlf
#        
#        self._uv_iv = dict( (v.u_v,i_v) for i_v,v in enumerate(self.lv) )
#        self._uf_if = dict( (f.u_f,i_f) for i_f,f in enumerate(self.lf) )
#        self.su_v = set([v.u_v for v in self.lv])
#        self.su_f = set([f.u_f for f in self.lf])
#        self.lfsu_v = [ set(f.lu_v) for f in self.lf ]
#        
#        # adjacency info by uid
#        self._la_v_u = dict( (v.u_v,{'si_f':set(),'si_v':set()}) for v in self.lv )
#        self._la_f_u = dict( (f.u_f,{'si_f':set()}) for f in self.lf )
#        # adjacency info by idx
#        self._la_v = dict( (i,{'si_f':set(),'ci_f':0,'si_v':set()}) for i,v in enumerate(self.lv) )
#        self._la_f = dict( (i,{'si_f':set(),'ci_f':0,'si_v':set()}) for i,f in enumerate(self.lf) )
#        
#        for i_f,f in enumerate(self.lf):
#            for u_v in f.lu_v:
#                i_v = self._uv_iv[u_v]
#                self._la_v_u[u_v]['si_f'].add( i_f )
#                self._la_v[i_v]['si_f'].add( i_f )
#            for u_v0,u_v1 in zip(f.lu_v,f.lu_v[1:]+[f.lu_v[0]]):
#                i_v0 = self._uv_iv[u_v0]
#                i_v1 = self._uv_iv[u_v1]
#                self._la_v_u[u_v0]['si_v'].add( i_v1 )
#                self._la_v_u[u_v1]['si_v'].add( i_v0 )
#                self._la_v[i_v0]['si_v'].add( i_v1 )
#                self._la_v[i_v1]['si_v'].add( i_v0 )
#        for i_f,f in enumerate(self.lf):
#            u_f = f.u_f
#            l = len(f.lu_v)
#            self._la_f[i_f]['si_v'] = set( self._uv_iv[u_v] for u_v in f.lu_v )
#            for i_v0,u_v0 in enumerate(f.lu_v):
#                #i_v0 = self._opt_get_i_v( u_v0 )
#                i_v1 = (i_v0 + 1) % l
#                u_v1 = f.lu_v[i_v1]
#                si_f = ( self._la_v_u[u_v0]['si_f'] & self._la_v_u[u_v1]['si_f'] ) - set([i_f])
#                for i_fa in si_f:
#                    self._la_f_u[u_f]['si_f'].add( i_fa )
#                    self._la_f[i_f]['si_f'].add( i_fa )
#        for i_v,a_v in self._la_v.items():
#            a_v['ci_f'] = len(a_v['si_f'])
#        for i_f,a_f in self._la_f.items():
#            a_f['ci_f'] = len(a_f['si_f'])
#        
#        DebugWriter.end()
#        return self
#    
#    def opt_get_v( self, u_v ):
#        return self.lv[ self._uv_iv[u_v] ]
#    
#    def _opt_get_i_v( self, u_v ):
#        return self._uv_iv[u_v]
#    
#    def opt_get_f( self, u_f ):
#        return self.lf[ self._uf_if[u_f] ]
#    
#    def _opt_get_i_f( self, u_f ):
#        return self._uf_if[u_f]
#    
#    def opt_get_uf_lv( self, u_f ):
#        return [ self.lv[self._uv_iv[u_v]] for u_v in self.lf[self._uf_if[u_f]].lu_v ]
#    
#    def opt_get_uf_le( self, u_f ):
#        pass
#    
#    def opt_get_f_lv( self, f ):
#        return [ self.lv[self._uv_iv[u_v]] for u_v in f.lu_v ]
#    
#    def opt_get_f_lv_quad( self, f ):
#        return [ self.lv[self._uv_iv[u_v]] for u_v in f.get_quad_lu_v() ]
#    
#    def _opt_get_f_li_v( self, f ):
#        if type(f) is str:          lu_v = self.lf[self._uf_if[u_f]].lu_v
#        elif type(f) is int:        lu_v = self.lf[f].lu_v
#        elif isinstance(f,Face):    lu_v = f.lu_v
#        return [ self._uv_iv[u_v] for u_v in lu_v ]
#    
#    def opt_face_normal( self, f ):
#        lv = [ self.lv[iv] for iv in self._opt_get_f_li_v( f ) ]
#        return Face.calc_normal( lv )
#    
#    
#    #######################################
#    # operators
#    #######################################
#    
#    
#    def translate( self, vt ):
#        for v in self.lv: v.translate( vt )
#        return self
#    
#    def recolor( self, c ):
#        for v in self.lv: v.c = list(c)
#        return self
#    
#    def recolor_adj_face_count( self, lc ):
#        self.optimize()
#        
#        #lc = [[0,0,0],[1,0,0],[0,1,0],[1,1,0],[0,0,1],[1,0,1],[0,1,1],[1,1,1]]
#        #lc = [[0.0,0.0,0.0],[1.0,0.0,0.0],[0.9,0.5,0.0],[1.0,1.0,0],[0.0,1.0,0.0],[0.0,0.0,1.0],[0.0,0.0,0.5]]
#        for v in self.lv:
#            v.recolor( lc[ min( len(lc), len(self._la_v_u[v.u_v]['si_f']) ) ] )
#        return self
#
#    
#    # append data from another mesh to self
#    def append( self, mesh, vt=None, vc=None ):
#        self.optimize()
#        su_v,su_f = self.su_v,self.su_f
#        lfsu_v = self.lfsu_v
#        
#        c = mesh.clone()
#        if vt: c.translate(vt)
#        if vc: c.recolor(vc)
#        
#        self.lv += [ v for v in c.lv if v.u_v not in su_v ]
#        self.lf += [ f for f in c.lf if f.u_f not in su_f and set(f.lu_v) not in lfsu_v ]
#        
#        return self
#    
#    def extend( self, mesh, vt=None, vc=None, ignore_edges=False ):
#        return self.append( mesh.copy( ignore_edges=ignore_edges ), vt=vt, vc=vc )
#    
#    def filter_faces( self, su_f, filterto ):
#        if filterto:
#            self.lf = [ f for f in self.lf if f.u_f in su_f ]
#        else:
#            self.lf = [ f for f in self.lf if f.u_f not in su_f ]
#        return self
#    
#    def add_vert( self, u_v=None, p=None, n=None, c=None ):
#        v = Vertex( u_v=u_v, p=p, n=n, c=c )
#        self.lv += [ v ]
#        return v.u_v
#    
#    def add_face( self, u_f=None, lu_v=None ):
#        su_v = set([ v.u_v for v in self.lv ])
#        assert all( [ u_v in su_v for u_v in lu_v ] ), 'adding face with unknown vert uid'
#        f = Face( u_f=u_f, lu_v=lu_v )
#        self.lf += [ f ]
#        return f.u_f
#    
#    def del_face( self, df ):
#        self.lf = [ f for f in self.lf if not f.same(df) ]
#        return self
#    
#    def del_vert( self, u_v ):
#        self.lv = [ v for v in self.lv if not v.u_v == u_v ]
#        return self
#    
#    # delete any verts that aren't used by a face
#    def trim( self ):
#        su_v = set([ u_v for f in self.lf for u_v in f.lu_v ])
#        self.lv = [ v for v in self.lv if v.u_v in su_v ]
#        return self
#    
#    def validate( self ):
#        suv = set([ v.u_v for v in self.lv ])
#        unk_v = [ (f.u_f,u_v) for f in self.lf for u_v in f.lu_v if u_v not in suv ]
#        if not unk_v: return
#        print( '*'*80 )
#        print( [ v.u_v for v in self.lv ] )
#        print( '*'*80 )
#        print( [ f.u_f for f in self.lf ] )
#        print( '*'*80 )
#        print( unk_v )
#        assert False
#    
#    
#    ###########################################
#    # correspondence building functions
#    ###########################################
#    
#    
#    @classmethod
#    def bc_closestverts( cls, mesh0, mesh1 ):
#        return cls.bc_closestverts2( mesh0, mesh1 )
#    
#    #@classmethod
#    #def bc_closestverts4( cls, mesh0, mesh1 ):
#    #    ci = build_correspondence( mesh0, mesh1 )
#    #    return get_corresponding_mesh( ci )
#    
#    # returns a mesh that's the same as mesh1 but labeled to correspond to mesh0
#    @classmethod
#    def bc_closestverts1( cls, mesh0, mesh1 ):
#        lv0,lv1,lf0,lf1 = mesh0.lv,mesh1.lv,mesh0.lf,mesh1.lf
#        mv01,mv10,mf01,mf10 = {},{},{},{}
#        threshold = 0.25
#        
#        mesh0.optimize()
#        mesh1.optimize()
#        
#        print( "computing %ix%i distances..." % (len(lv0),len(lv1)) )
#        l01d = []
#        for (iv0,v0) in enumerate(lv0):
#            for (iv1,v1) in enumerate(lv1):
#                d2 = Vertex.distance2( v0, v1 )
#                if d2 >= threshold: continue
#                l01d += [ [iv0,iv1,d2] ]
#        
#        print( "sorting %i distances..." % len(l01d) )
#        l01d = sorted( l01d, key=lambda e:e[2] )
#        
#        print( "building vertex correspondences..." )
#        u0,u1 = [0]*len(lv0),[0]*len(lv1)
#        for (iv0,iv1,d2) in l01d:
#            if u0[iv0] or u1[iv1]: continue
#            mv01[iv0],mv10[iv1] = iv1,iv0
#            u0[iv0] = 1
#            u1[iv1] = 1
#        
#        print( "building face correspondences..." )
#        lsf1 = [ set(f1.lu_v) for f1 in lf1 ]
#        for if0,f0 in enumerate(lf0):
#            smf1 = set([ mesh1.lv[mv01[i_v]].u_v for i_v in mesh0._opt_get_f_li_v(f0) if i_v in mv01 ])              # find corresponding verts
#            if len(smf1) != len(f0.lu_v): continue                              # find enough corresponding verts?
#            if smf1 not in lsf1: continue                                       # find corresponding face?
#            if1 = lsf1.index( smf1 )                                            # get ind
#            mf01[if0],mf10[if1] = if1,if0                                       # # record face pair mapping
#        
#        print( "done!" )
#        
#        # create new mesh that is same as mesh1 but labeled to correspond with mesh0
#        mesh = Mesh()
#        mesh.lv = [ Vertex( u_v=mesh0.lv[mv10[i_v]].u_v, p=v.p, n=v.n, c=v.c ) if i_v in mv10 else v.copy() for i_v,v in enumerate(mesh1.lv) ]
#        mesh.lf = [ mesh0.lf[mf10[i_f]].clone() if i_f in mf10 else Face( lu_v=[ mesh.lv[i_v].u_v for i_v in mesh1._opt_get_f_li_v(f) ] ) for i_f,f in enumerate(mesh1.lf) ]
#        mesh.trim()
#        
#        return mesh
#    
#    # returns a mesh that's the same as mesh1 but labeled to correspond to mesh0
#    @classmethod
#    def bc_closestverts2( cls, mesh0, mesh1 ):
#        lv0,lv1,lf0,lf1 = mesh0.lv,mesh1.lv,mesh0.lf,mesh1.lf
#        mv01,mv10,mf01,mf10 = {},{},{},{}
#        threshold = 1.0
#        k = 20
#        
#        mesh0.optimize()
#        mesh1.optimize()
#        
#        la_v0,la_v1 = mesh0._la_v_u,mesh1._la_v_u
#        la_f0,la_f1 = mesh0._la_f_u,mesh1._la_f_u
#        
#        
#        print( "computing vertex normals..." )
#        mesh0.recalc_vertex_normals()
#        mesh1.recalc_vertex_normals()
#        ln0,ln1 = [ v.n for v in lv0 ],[v.n for v in lv1 ]
#        
#        print( "computing %ix%i distances..." % (len(lv0),len(lv1)) )
#        l01d = []
#        for (iv0,v0) in enumerate(lv0):
#            n0 = ln0[iv0]
#            cf0 = len( la_v0[v0.u_v]['si_f'] )
#            for (iv1,v1) in enumerate(lv1):
#                n1 = ln1[iv1]
#                cf1 = len( la_v1[v1.u_v]['si_f'] )
#                
#                dv2 = Vertex.distance2( v0, v1 )
#                dn2 = 1.0 - abs((n0[0]*n1[0])+(n0[1]*n1[1])+(n0[2]*n1[2]))
#                dc2 = (cf0-cf1)*(cf0-cf1)
#                
#                # 200.0 1.0 0.6
#                d2 = dv2 * 100.0 + dc2 * 0.1 + dn2 * 0.2
#                if d2 >= threshold: continue
#                
#                l01d += [ [iv0,iv1,d2] ]
#        
#        #l01d = []
#        #print( "build kdtree..." )
#        #kdt = KDTree( [ v.p for v in mesh1.lv ] )
#        #print( "computing %i-nn for %i vertices..." % (k,len(lv0)) )
#        #l01d = [ [iv0,v1['i'],v1['d'] ] for iv0,v0 in enumerate(lv0)  for v1 in kdt.knn_stl( v0.p, k ) ]
#        
#        print( "sorting %i distances..." % len(l01d) )
#        l01d = sorted( l01d, key=lambda e:e[2] )
#        
#        print( "building initial vertex correspondences..." )
#        for (iv0,iv1,d2) in l01d:
#            if iv0 in mv01 or iv1 in mv10: continue
#            mv01[iv0],mv10[iv1] = iv1,iv0
#        
#        converged = False
#        runs = 0
#        while not converged and runs < 20:
#            converged = True
#            runs += 1
#            
#            print( "using faces to find more vertex correspondences..." )
#            chgcnta = [0]*len(lv0)
#            chgcntb = [0]*len(lv1)
#            change = True
#            while change:
#                change = False
#                #for if0,f0 in enumerate(lf0):
#                    #if len(f0.lu_v) != 4: continue
#                    #li_v0 = mesh0._opt_get_f_li_v(f0)
#                    #li_v1 = [ mv01[i_v] if i_v in mv01 else None for i_v in li_v0 ]
#                    #if sum( 1 if i_v is None else 0 for i_v in li_v1 ) != 1: continue
#                    ## 3 out of 4 verts has correspondences
#                    #i = li_v1.index( None )
#                    ##li_v01t = [ [iv0,iv1] for iv0,iv1 in zip(li_v0,li_v1) if iv1 is not None ]
#                    #su_v1t = set([ lv1[i_v].u_v for i_v in li_v1 if i_v is not None ])
#                    #i_v0 = li_v0[i]
#                    #i_v1 = None
#                    #for if1,f1 in enumerate(lf1):
#                        #if len(set(f1.lu_v) & su_v1t) == 3:
#                            #i_v1 = mesh1._opt_get_i_v(list(set(f1.lu_v)-su_v1t)[0])
#                            #break
#                    #if i_v1 is None: continue
#                    #if chgcnta[i_v0] >= 2: continue
#                    #if chgcntb[i_v1] >= 2: continue
#                    
#                    #if i_v1 in mv10: del mv01[mv10[i_v1]]
#                    #mv01[i_v0] = i_v1
#                    #mv10[i_v1] = i_v0
#                    #chgcnta[i_v0] += 1
#                    #chgcntb[i_v1] += 1
#                    ##print( "a %04i : %04i" % (i_v0,i_v1) )
#                    #change = True
#                    #converged = False
#                
#                #for if1,f1 in enumerate(lf1):
#                    #if len(f1.lu_v) != 4: continue
#                    #li_v1 = mesh1._opt_get_f_li_v(f1)
#                    #li_v0 = [ mv10[i_v] if i_v in mv10 else None for i_v in li_v1 ]
#                    #if sum( 1 if i_v is None else 0 for i_v in li_v0 ) != 1: continue
#                    ## 3 out of 4 verts has correspondences
#                    #i = li_v0.index( None )
#                    ##li_v10t = [ [iv0,iv1] for iv0,iv1 in zip(li_v0,li_v1) if iv1 is not None ]
#                    #su_v0t = set([ lv0[i_v].u_v for i_v in li_v0 if i_v is not None ])
#                    #i_v1 = li_v1[i]
#                    #i_v0 = None
#                    #for if0,f0 in enumerate(lf0):
#                        #if len(set(f0.lu_v) & su_v0t) == 3:
#                            #i_v0 = mesh0._opt_get_i_v(list(set(f0.lu_v)-su_v0t)[0])
#                            #break
#                    #if i_v0 is None: continue
#                    #if chgcnta[i_v0] >= 2 or chgcntb[i_v1] >= 2: continue
#                    
#                    #if i_v0 in mv01: del mv10[mv01[i_v0]]
#                    #mv10[i_v1] = i_v0
#                    #mv01[i_v0] = i_v1
#                    #chgcnta[i_v0] += 1
#                    #chgcntb[i_v1] += 1
#                    ##print( "b %04i : %04i" % (i_v0,i_v1) )
#                    #change = True
#                    #converged = False
#                
#                for if0,f0 in enumerate(lf0):
#                    if if0 in mf01: continue
#                    if len(f0.lu_v) != 4: continue
#                    li_v0 = mesh0._opt_get_f_li_v(f0)
#                    li_v1 = [ mv01[i_v] if i_v in mv01 else None for i_v in li_v0 ]
#                    if sum( 1 if i_v is None else 0 for i_v in li_v1 ) != 1: continue
#                    # 3 out of 4 verts has correspondences
#                    i = li_v1.index( None )
#                    #li_v01t = [ [iv0,iv1] for iv0,iv1 in zip(li_v0,li_v1) if iv1 is not None ]
#                    su_v1t = set([ lv1[i_v].u_v for i_v in li_v1 if i_v is not None ])
#                    i_v0 = li_v0[i]
#                    i_v1 = None
#                    for if1,f1 in enumerate(lf1):
#                        if len(f1.lu_v) != 4: continue
#                        if len(set(f1.lu_v) & su_v1t) == 3:
#                            i_v1 = mesh1._opt_get_i_v(list(set(f1.lu_v)-su_v1t)[0])
#                            break
#                    if i_v1 is None: continue
#                    if chgcnta[i_v0] >= 3: continue
#                    if chgcntb[i_v1] >= 3: continue
#                    
#                    if i_v1 in mv10:
#                        continue
#                        #del mv01[mv10[i_v1]]
#                    
#                    mv01[i_v0] = i_v1
#                    mv10[i_v1] = i_v0
#                    chgcnta[i_v0] += 1
#                    chgcntb[i_v1] += 1
#                    #print( "a %04i : %04i" % (i_v0,i_v1) )
#                    change = True
#                    converged = False
#            
#            print( "building face correspondences..." )
#            lsf1 = [ set(f1.lu_v) for f1 in lf1 ]
#            for if0,f0 in enumerate(lf0):
#                if if0 in mf01: continue
#                smf1 = set([ mesh1.lv[mv01[i_v]].u_v for i_v in mesh0._get_f_li_v(f0) if i_v in mv01 ])              # find corresponding verts
#                if len(smf1) != len(f0.lu_v): continue                              # find enough corresponding verts?
#                if smf1 not in lsf1: continue                                       # find corresponding face?
#                if1 = lsf1.index( smf1 )                                            # get ind
#                if not same_lists(
#                    [ mv01[mesh0._opt_get_i_v(u_v)] for u_v in lf0[if0].lu_v ],
#                    [ mesh1._opt_get_i_v(u_v) for u_v in lf1[if1].lu_v ]
#                    ): continue
#                if if1 in mf10: continue
#                
#                converged = False
#                mf01[if0],mf10[if1] = if1,if0                                       # # record face pair mapping
#            
#            for if0,f0 in enumerate(lf0):
#                if if0 in mf01: continue
#                liv0 = mesh0._opt_get_f_li_v( f0 )
#                if not all( i_v in mv01 for i_v in liv0 ): continue
#                
#                liv1 = [ mv01[i_v] for i_v in liv0 ]
#                lsf1 = [ la_v1[lv1[i_v].u_v]['si_f'] for i_v in liv1 ]
#                sf1 = lsf1[0]
#                for sf in lsf1: sf1 &= sf
#                #if len(sf) > 1: print( sf )
#                if len(sf1) != 1: continue
#                if1 = sf1.pop()
#                if if1 in mf10: continue
#                
#                mf01[if0],mf10[if1] = if1,if0
#                converged = False
#            
#            for if0,if1 in mf01.items():
#                assert if1 in mf10
#                assert mf10[if1] == if0
#            
#            print( "using adjacent faces to find new faces..." )
#            changed = True
#            while changed:
#                changed = False
#                for if0,f0 in enumerate(lf0):
#                    if if0 not in mf01: continue
#                    if1,f1 = mf01[if0],lf1[mf01[if0]]
#                    saf0 = la_f0[f0.u_f]['si_f']
#                    saf1 = la_f1[f1.u_f]['si_f']
#                    if len(saf0) != 4 or len(saf1) != 4:
#                        continue
#                    saf0u = [ i_f for i_f in saf0 if i_f not in mf01 ]
#                    saf1u = [ i_f for i_f in saf1 if i_f not in mf10 ]
#                    #print( "%s:%s  %s:%s  %s" % (saf0,saf0u,saf1,saf1u,'*'*10 if len(saf0u)==1 and len(saf1u)==1 else ''))
#                    if len(saf0u) != 1 or len(saf1u) != 1:
#                        continue
#                    if0,if1 = saf0u[0],saf1u[0]
#                    mf01[if0],mf10[if1] = if1,if0
#                    changed = True
#                    converged = False
#            
#            if True:
#                # use face mapping to rebuild vertex mapping
#                print( "rebuilding vertex correspondences..." )
#                
#                lmv01,lmv10 = mv01,mv10
#                mv01,mv10 = {},{}
#                
#                conflicts = 0
#                for if0,f0 in enumerate(lf0):
#                    if if0 not in mf01: continue
#                    if1 = mf01[if0]
#                    f1 = lf1[if1]
#                    
#                    li_v0 = mesh0._opt_get_f_li_v( f0 )
#                    li_v1 = mesh1._opt_get_f_li_v( f1 )
#                    # find "cheapest" assignment of verts
#                    
#                    cheapest_cost = -1
#                    cheapest_p = None
#                    for li_v1p in permutations(li_v1):
#                        cost = sum( (1.0+Vertex.distance( lv0[li_v0[i]], lv1[li_v1p[i]] ))**2 for i in xrange(len(li_v1p)) )
#                        if cheapest_cost != -1 and cost >= cheapest_cost: continue
#                        
#                        conflict = False
#                        for i in xrange(len(li_v1p)):
#                            if li_v0[i] in mv01 and mv01[li_v0[i]] != li_v1p[i]:
#                                conflict = True
#                                break
#                            if li_v1p[i] in mv10 and mv10[li_v1p[i]] != li_v0[i]:
#                                conflict = True
#                                break
#                        if conflict: continue
#                        
#                        cheapest_cost = cost
#                        cheapest_p = li_v1p
#                    
#                    if cheapest_p == None:
#                        conflicts += 1
#                        #del mf10[if1]
#                        #del mf01[if0]
#                        continue
#                    
#                    li_v1 = cheapest_p
#                    
#                    for i in xrange(len(li_v1)):
#                        mv01[li_v0[i]] = li_v1[i]
#                        mv10[li_v1[i]] = li_v0[i]
#                    
#                if conflicts:
#                    print( '    %i conflicts detected' % conflicts )
#                    converged = False
#                if mv01 != lmv01 or mv10 != lmv10: converged = False
#                
#                for if0,if1 in mf01.items():
#                    assert if1 in mf10
#                    assert mf10[if1] == if0
#            
#            # single faces may be blocking better correspondences
#            print( 'removing single faces...' )
#            remcount = 0
#            for if0,f0 in enumerate(lf0):
#                if if0 not in mf01: continue
#                if1 = mf01[if0]
#                f1 = lf1[if1]
#                
#                li_f0 = [ i_f for i_f in la_f0[f0.u_f]['si_f'] if i_f in mf01 ]
#                li_f1 = [ i_f for i_f in la_f1[f1.u_f]['si_f'] if i_f in mf10 ]
#                if len(li_f0) == 0 and len(li_f1) == 0:
#                    remcount += 1
#                    del mf10[if1]
#                    del mf01[if0]
#                    converged = False
#            print( '    removed %i single faces' % remcount )
#            
#            for if0,if1 in mf01.items():
#                assert if1 in mf10
#                assert mf10[if1] == if0
#        
#        print( "done!" )
#        
#        # create new mesh that is same as mesh1 but labeled to correspond with mesh0
#        mesh = Mesh()
#        mesh.lv = [ Vertex( u_v=lv0[mv10[i_v]].u_v, p=v.p, n=v.n, c=v.c ) if i_v in mv10 else v.copy() for i_v,v in enumerate(lv1) ]
#        lfinfo = [ (lf0[mf10[i_f1]].u_f if i_f1 in mf10 else None,[ mesh.lv[i_v1].u_v for i_v1 in mesh1._opt_get_f_li_v(f1) ]) for i_f1,f1 in enumerate(lf1) ]
#        mesh.lf = [ Face( u_f=u_f, lu_v=lu_v ) for u_f,lu_v in lfinfo ]
#        #mesh.lf = [ lf0[mf10[i_f1]].clone() if i_f1 in mf10 else Face( lu_v=[ mesh.lv[i_v1].u_v for i_v1 in mesh1._opt_get_f_li_v(f1) ] ) for i_f1,f1 in enumerate(lf1) ]
#        mesh.trim()
#        mesh.validate()
#        
#        return mesh
#    
#    
#    # returns a mesh that's the same as mesh1 but labeled to correspond to mesh0
#    @classmethod
#    def bc_closestverts3( cls, mesh0, mesh1 ):
#        lv0,lv1,lf0,lf1 = mesh0.lv,mesh1.lv,mesh0.lf,mesh1.lf
#        mv01,mv10,mf01,mf10 = {},{},{},{}
#        threshold = 1.0
#        k = 20
#        
#        mesh0.optimize()
#        mesh1.optimize()
#        
#        la_v0,la_v1 = mesh0._la_v_u,mesh1._la_v_u
#        la_f0,la_f1 = mesh0._la_f_u,mesh1._la_f_u
#        
#        
#        print( "computing vertex normals..." )
#        mesh0.recalc_vertex_normals()
#        mesh1.recalc_vertex_normals()
#        ln0,ln1 = [ v.n for v in lv0 ],[v.n for v in lv1 ]
#        
#        print( "computing %ix%i distances..." % (len(lv0),len(lv1)) )
#        l01d = []
#        for (iv0,v0) in enumerate(lv0):
#            n0 = ln0[iv0]
#            cf0 = len( la_v0[v0.u_v]['si_f'] )
#            for (iv1,v1) in enumerate(lv1):
#                n1 = ln1[iv1]
#                cf1 = len( la_v1[v1.u_v]['si_f'] )
#                
#                dv2 = Vertex.distance2( v0, v1 )
#                dn2 = 1.0 - abs((n0[0]*n1[0])+(n0[1]*n1[1])+(n0[2]*n1[2]))
#                dc2 = (cf0-cf1)*(cf0-cf1)
#                
#                # 200.0 1.0 0.6
#                d2 = dv2 * 100.0 + dc2 * 0.1 + dn2 * 0.2
#                if d2 >= threshold: continue
#                
#                l01d += [ [iv0,iv1,d2] ]
#        
#        print( "sorting %i distances..." % len(l01d) )
#        l01d = sorted( l01d, key=lambda e:e[2] )
#        
#        print( "building initial vertex correspondences..." )
#        for (iv0,iv1,d2) in l01d:
#            if iv0 in mv01 or iv1 in mv10: continue
#            mv01[iv0],mv10[iv1] = iv1,iv0
#        
#        print( "using faces to find more vertex correspondences..." )
#        #chgcnta,chngcntb = [0]*len(lv0),[0]*len(lv1)
#        change = True
#        while change:
#            change = False
#            for if0,f0 in enumerate(lf0):
#                if if0 in mf01: continue
#                if len(f0.lu_v) != 4: continue
#                li_v0 = mesh0._opt_get_f_li_v(f0)
#                li_v1 = [ mv01[i_v] if i_v in mv01 else None for i_v in li_v0 ]
#                if sum( 1 if i_v is None else 0 for i_v in li_v1 ) != 1: continue
#                # 3 out of 4 verts has correspondences
#                i = li_v1.index( None )
#                su_v1t = set([ lv1[i_v].u_v for i_v in li_v1 if i_v is not None ])
#                i_v0 = li_v0[i]
#                i_v1 = None
#                for if1,f1 in enumerate(lf1):
#                    if len(f1.lu_v) != 4: continue
#                    if len(set(f1.lu_v) & su_v1t) == 3:
#                        i_v1 = mesh1._opt_get_i_v(list(set(f1.lu_v)-su_v1t)[0])
#                        break
#                if i_v1 is None: continue
#                #if chgcnta[i_v0] >= 3 or chgcntb[i_v1] >= 3: continue
#                if i_v1 in mv10: continue                               #del mv01[mv10[i_v1]]
#                
#                mv01[i_v0] = i_v1
#                mv10[i_v1] = i_v0
#                #chgcnta[i_v0] += 1
#                #chgcntb[i_v1] += 1
#                change = True
#        
#        print( "building initial face correspondences..." )
#        lsf1 = [ set(f1.lu_v) for f1 in lf1 ]
#        for if0,f0 in enumerate(lf0):
#            if if0 in mf01: continue
#            smf1 = set([ mesh1.lv[mv01[i_v]].u_v for i_v in mesh0._get_f_li_v(f0) if i_v in mv01 ])              # find corresponding verts
#            if len(smf1) != len(f0.lu_v): continue                              # find enough corresponding verts?
#            if smf1 not in lsf1: continue                                       # find corresponding face?
#            if1 = lsf1.index( smf1 )                                            # get ind
#            if not same_lists(
#                [ mv01[mesh0._opt_get_i_v(u_v)] for u_v in lf0[if0].lu_v ],
#                [ mesh1._opt_get_i_v(u_v) for u_v in lf1[if1].lu_v ]
#                ): continue
#            if if1 in mf10: continue
#            mf01[if0],mf10[if1] = if1,if0                                       # record face pair mapping
#        for if0,f0 in enumerate(lf0):
#            if if0 in mf01: continue
#            liv0 = mesh0._opt_get_f_li_v( f0 )
#            if not all( i_v in mv01 for i_v in liv0 ): continue
#            
#            liv1 = [ mv01[i_v] for i_v in liv0 ]
#            lsf1 = [ la_v1[lv1[i_v].u_v]['si_f'] for i_v in liv1 ]
#            sf1 = lsf1[0]
#            for sf in lsf1: sf1 &= sf
#            if len(sf1) != 1: continue
#            if1 = sf1.pop()
#            if if1 in mf10: continue
#            mf01[if0],mf10[if1] = if1,if0
#        
#        for if0,if1 in mf01.items():
#            assert if1 in mf10
#            assert mf10[if1] == if0
#        
#        # single faces may be blocking better correspondences
#        print( 'removing single faces...' )
#        remcount = 0
#        for if0,f0 in enumerate(lf0):
#            if if0 not in mf01: continue
#            if1 = mf01[if0]
#            f1 = lf1[if1]
#            
#            li_f0 = [ i_f for i_f in la_f0[f0.u_f]['si_f'] if i_f in mf01 ]
#            li_f1 = [ i_f for i_f in la_f1[f1.u_f]['si_f'] if i_f in mf10 ]
#            if len(li_f0) == 0 and len(li_f1) == 0:
#                remcount += 1
#                del mf10[if1]
#                del mf01[if0]
#                converged = False
#        print( '    removed %i single faces' % remcount )
#        
#        mf01_prev,mf10_prev = None,None
#        runs = 0
#        while ( mf01 != mf01_prev or mf10 != mf10_prev ) and runs < 50:
#            mf01_prev,mf10_prev = dict(mf01),dict(mf10)
#            runs += 1
#            
#            print( "using adjacent faces to find new faces..." )
#            addcount = 0
#            for if0,f0 in enumerate(lf0):
#                if if0 not in mf01: continue
#                if1,f1 = mf01[if0],lf1[mf01[if0]]
#                saf0 = la_f0[f0.u_f]['si_f']
#                saf1 = la_f1[f1.u_f]['si_f']
#                #if len(saf0) != 4 or len(saf1) != 4: continue                   # TODO: only quads right now...
#                saf0u = [ i_f for i_f in saf0 if i_f not in mf01 ]
#                saf1u = [ i_f for i_f in saf1 if i_f not in mf10 ]
#                if len(saf0u) != 1 or len(saf1u) != 1: continue
#                if0,if1 = saf0u[0],saf1u[0]
#                mf01[if0],mf10[if1] = if1,if0
#                changed = True
#                addcount += 1
#            for if0,f0 in enumerate(lf0):
#                if if0 in mf01: continue
#                saif0 = la_f0[f0.u_f]['si_f']
#                lsaif1 = [ la_f1[lf1[mf01[i_f0]].u_f]['si_f'] for i_f0 in saif0 if i_f0 in mf01 ]                       # find corresponding faces to adj faces
#                if not lsaif1: continue
#                sif = lsaif1[0]
#                for saif1 in lsaif1: sif &= saif1
#                sif = [ i_f for i_f in sif if i_f not in mf10 ]
#                if len(sif) != 1: continue
#                if1 = sif.pop()
#                if if1 in mf10: continue
#                mf01[if0],mf10[if1] = if1,if0
#                changed = True
#                addcount += 1
#            for if1,f1 in enumerate(lf1):
#                if if1 not in mf10: continue
#                if0,f0 = mf10[if1],lf0[mf10[if1]]
#                saf1 = la_f1[f1.u_f]['si_f']
#                saf0 = la_f0[f0.u_f]['si_f']
#                saf1u = [ i_f for i_f in saf1 if i_f not in mf10 ]
#                saf0u = [ i_f for i_f in saf0 if i_f not in mf01 ]
#                if len(saf0u) != 1 or len(saf1u) != 1: continue
#                if0,if1 = saf0u[0],saf1u[0]
#                mf01[if0],mf10[if1] = if1,if0
#                changed = True
#                addcount += 1
#            for if1,f1 in enumerate(lf1):
#                if if1 in mf10: continue
#                saif1 = la_f1[f1.u_f]['si_f']
#                lsaif0 = [ la_f0[lf0[mf10[i_f1]].u_f]['si_f'] for i_f1 in saif1 if i_f1 in mf10 ]                       # find corresponding faces to adj faces
#                if not lsaif0: continue
#                sif = lsaif0[0]
#                for saif0 in lsaif0: sif &= saif0
#                sif = [ i_f for i_f in sif if i_f not in mf01 ]
#                if len(sif) != 1: continue
#                if0 = sif.pop()
#                if if0 in mf01: continue
#                mf01[if0],mf10[if1] = if1,if0
#                changed = True
#                addcount += 1
#            print( '    added %i faces' % addcount )
#            
#            
#            for if0,if1 in mf01.items():
#                assert if1 in mf10
#                assert mf10[if1] == if0
#        
#        # use face mapping to rebuild vertex mapping
#        print( "rebuilding vertex correspondences..." )
#        cmv01,cmv10 = {},{}
#        cheapest = -1
#        lif01 = mf01.items()
#        for runs in xrange(50):
#            conflicts = 0
#            mv01,mv10 = {},{}
#            
#            random.shuffle(lif01)
#            for if0,if1 in lif01:
#                li_v0 = mesh0._opt_get_f_li_v( lf0[if0] )
#                li_v1 = mesh1._opt_get_f_li_v( lf1[if1] )
#                # find "cheapest" assignment of verts
#                
#                cheapest_cost = -1
#                cheapest_p = None
#                for li_v1p in permutations(li_v1):
#                    ld = [ min(1.0, Vertex.distance( lv0[li_v0[i]], lv1[li_v1p[i]] )*1.0 ) for i in xrange(len(li_v1p)) ]
#                    li_v1p = [ i_v1 if d < 1.0 else None for i_v1,d in zip(li_v1p,ld) ]
#                    cost =  sum(
#                        ld[i] +
#                        (0.0 if li_v0[i] in mv01 and mv01[li_v0[i]]==li_v1p[i] else 2.0) +
#                        (0.0 if li_v1p[i] in mv10 and mv10[li_v1p[i]]==li_v0[i] else 2.0)
#                        for i in xrange(len(li_v1p))
#                        )
#                    if cheapest_cost != -1 and cost >= cheapest_cost: continue
#                    
#                    conflict = False
#                    for i in xrange(len(li_v1p)):
#                        if li_v0[i] in mv01 and li_v1p[i] is not None and mv01[li_v0[i]] != li_v1p[i]:
#                            #del mv10[mv01[li_v0[i]]]
#                            #del mv01[li_v0[i]]
#                            conflict = True
#                            break
#                        if li_v1p[i] in mv10 and mv10[li_v1p[i]] != li_v0[i]:
#                            #del mv01[mv10[li_v1p[i]]]
#                            #del mv10[li_v1p[i]]
#                            conflict = True
#                            break
#                    if conflict: continue
#                    
#                    cheapest_cost = cost
#                    cheapest_p = li_v1p
#                    if cheapest_cost == 0: break
#                
#                if cheapest_p == None:
#                    conflicts += 1
#                    if cheapest != -1 and conflicts > cheapest: break
#                    continue
#                
#                li_v1 = cheapest_p
#                
#                for i in xrange(len(li_v1)):
#                    if li_v1[i] is None: continue
#                    if li_v0[i] in mv01 or li_v1[i] in mv10: continue
#                    mv01[li_v0[i]] = li_v1[i]
#                    mv10[li_v1[i]] = li_v0[i]
#            
#            if cheapest != -1 and conflicts > cheapest: continue
#            
#            print( '    %i conflicts detected' % conflicts )
#            cmv01,cmv10 = mv01,mv10
#            cheapest = conflicts
#            if cheapest == 0: break
#        
#        mv01,mv10 = cmv01,cmv10
#        
#        #mv01,mv10 = {},{}
#        #for iv0,v0 in enumerate(lv0):
#            #sif0 = la_v0[v0.u_v]['si_f']
#            #sif1 = [ mf01[if0] for if0 in sif0 if if0 in mf01 ]
#            #if len(sif1)<3: continue
#            #iv1_votes = {}
#            ##iv1 = None
#            #for sif1_3 in combinations( sif1, 3 ):
#                #suv = set(lf1[sif1_3[0]].lu_v)
#                #for if1 in sif1_3: suv &= set(lf1[if1].lu_v)
#                #if len(suv) != 1: continue
#                #iv1 = mesh1._opt_get_i_v(suv.pop())
#                #if iv1 in mv10:
#                    #iv1 = None
#                    #continue
#                #if iv1 in iv1_votes:
#                    #iv1_votes[iv1] += 1
#                #else:
#                    #iv1_votes[iv1] = 1
#            #if not iv1_votes: continue
#            #iv1,cv = None,-1
#            #for _iv1,_cv in iv1_votes.items():
#                #if _cv > cv: iv1,cv=_iv1,_cv
#            #if iv1 is None: continue
#            #mv01[iv0],mv10[iv1] = iv1,iv0
#            
#        
#        for iv0,iv1 in mv01.items():
#            assert iv1 in mv10
#            assert mv10[iv1] == iv0
#        for if0,if1 in mf01.items():
#            assert if1 in mf10
#            assert mf10[if1] == if0
#        
#        print( "done!" )
#        
#        # create new mesh that is same as mesh1 but labeled to correspond with mesh0
#        mesh = Mesh()
#        mesh.lv = [ Vertex( u_v=lv0[mv10[i_v1]].u_v, p=v1.p, n=v1.n, c=v1.c ) if i_v1 in mv10 else v1.copy() for i_v1,v1 in enumerate(lv1) ]
#        lfinfo = [ (lf0[mf10[i_f1]].u_f if i_f1 in mf10 else None,[ mesh.lv[i_v1].u_v for i_v1 in mesh1._opt_get_f_li_v(f1) ]) for i_f1,f1 in enumerate(lf1) ]
#        mesh.lf = [ Face( u_f=u_f, lu_v=lu_v ) for u_f,lu_v in lfinfo ]
#        #mesh.lf = [ lf0[mf10[i_f1]].clone() if i_f1 in mf10 else Face( lu_v=[ mesh.lv[i_v1].u_v for i_v1 in mesh1._opt_get_f_li_v(f1) ] ) for i_f1,f1 in enumerate(lf1) ]
#        mesh.trim()
#        mesh.validate()
#        
#        return mesh
#    
#
#
import sys, subprocess, os, scipy.io

from debugwriter import DebugWriter
from mesh import *
from vec3f import *

from correspondencebuilder_logic import CorrespondenceBuilder_Logic as CB_Logic
from correspondencebuilder_hungarian import CorrespondenceBuilder_Hungarian as CB_Hungarian


class CorrespondenceBuilder_Blended(object):
    
    @staticmethod
    def build_verts( opts ):
        CB_Blended = CorrespondenceBuilder_Blended
        
        DebugWriter.start( 'loading blended map and matching' )
        
        ci = opts['ci']
        m0,m1 = ci.m0,ci.m1
        nv0,nv1 = ci.cv0,ci.cv1
        
        # load potential matchings from m0 -> m1
        if os.path.exists( 'durano03_durano04_mod.blendedmap' ):
            fn = 'durano03_durano04_mod.blendedmap'
        else:
            fn = 'durano02_durano13.blendedmap'
        fp = open( fn, 'rt' )
        liv1 = [ int(s) for s in fp.read().split('\n') if s ]
        fp.close()
        
        assert len(liv1) == nv0
        
        # reverse matching.  note: multiple i_v0 can point to same i_v1
        div01 = dict( (i_v0,i_v1) for i_v0,i_v1 in enumerate(liv1) )
        div10 = dict()
        for i_v0,i_v1 in div01.iteritems():
            div10[i_v1] = div10.get( i_v1, [] ) + [i_v0]
        print( div10 )
        
        # match each i_v1 in div10 to its nearest potential
        for i_v1,li_v0 in div10.iteritems():
            l = [ (i_v0, Vec3f.t_distance2( m0.lv[i_v0].p, m1.lv[i_v1].p )) for i_v0 in li_v0 ]
            l = sorted( l, key=lambda x:x[1] )
            i_v0 = l[0][0]
            ci.acv( i_v0, i_v1 )
            print '%d: %s' % (i_v1,str(l))
        
        #for i_v0,i_v1 in liv01:
        #    if i_v1 not in ci.mv10:
        #        ci.acv( i_v0, i_v1 )
        
        DebugWriter.end()
        
        CB_Logic.build_face_correspondences( opts )
        #CB_Hungarian.build_face_correspondences_hungarian_verts( opts )

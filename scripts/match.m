function M = match(g0,g1)
n = length(g0.V);
m = length(g1.V);

M = zeros(m,n);

X = fuzzyGED(g0,g1);
disp(X);

% while max(X) > 0
%     [~,ij] = max(X);
%     i = floor((ij-1)/n)+1;
%     j = mod((ij-1),n)+1;
%     
%     M(i,j) = 1;
%     
%     for k = 1:m
%         X((i-1)*n+k) = 0;
%     end
%     for k = 1:n
%         X((k-1)*n+j) = 0;
%     end
% end

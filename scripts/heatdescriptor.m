function H = heatdescriptor()
% [authors of MeshGit], 2012-Jan-12
% http://www.mathworks.com/matlabcentral/fx_files/13464/1/content/manifold/tp4.html
% http://www.mathworks.com/matlabcentral/fileexchange/5355

name = 'head_b_tri';
options.name = name;

%addpath( path, '/Users/jdenning/Projects/meshgit.new/models/sintel/' );
%addpath( path, '/Users/jdenning/Projects/meshgit.new/toolbox_graph/toolbox_graph/' );
%addpath( path, '/Users/jdenning/Projects/meshgit.new/toolbox_graph/toolbox_graph/off/' );
%addpath( path, '/Users/jdenning/Projects/meshgit.new/toolbox_graph/toolbox_graph/toolbox/' );

[vertex,face,~] = read_mesh([name '.ply']);

L = compute_mesh_laplacian(vertex,face,'combinatorial',options);
[U,S,V] = svds(L);

for i=1:10
    t = i * 10;
    H = heatkernel(U,diag(S),t,size(U,2)-1);
    subplot(10,1,i)
    options.face_vertex_color = rescale(H(:,1), 0,255);
    clf; plot_mesh(vertex,face,options); shading interp; lighting none;% axis tight;
end

0;

end

function H = heatkernel(U,S,t,n)
    s = size(U,1);
    H = zeros(s);
    
    for k=2:n
        H = H + exp( -t * S(k) ) * U(:,k) * U(:,k)';
    end
    
end

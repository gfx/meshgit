import os, re, sys, math, random, itertools, pickle, numpy

from mesh import *
from viewer import *
from shapes import *
from vec3f import *
from colors import *

from correspondenceinfo import *

from meshgit_misc import *
from meshgit_med_ops import *

def run_build_and_save( fn0, fn1, fnsave, method=None ):
    m0,m1 = [ Mesh.fromPLY( fn ) for fn in [fn0,fn1] ]
    scaling_factor = scale( [ m0, m1 ] )
    ci = build_correspondence( m0, m1, method )
    m1 = ci.get_corresponding_mesh1( )
    data = [ ci.mv01, ci.mf01 ]
    pickle.dump( data, open( fnsave, 'wb' ) )

def run_diff_viewer_saved( fn0, fn1, fnsave ):
    m0,m1 = [ Mesh.fromPLY( fn ) for fn in [fn0,fn1] ]
    mv01,mf01 = pickle.load( open( fnsave, 'rb' ) )
    
    ci = CorrespondenceInfo( m0, m1 )
    for iv0,iv1 in mv01.iteritems():
        ci.acv( iv0, iv1 )
    for if0,if1 in mf01.iteritems():
        ci.acf( if0, if1 )
    
    m1 = ci.get_corresponding_mesh1( allow_face_sub=False )
    
    for m in [m0,m1]:
        m.optimize()
        m.rebuild_edges()
    
    c01 = diff_commands( m0, m1 )
    su_df = set([ cmd.split(' ')[1].split(':')[0] for cmd in c01 if cmd.startswith('delface') ])
    su_af = set([ cmd.split(' ')[1].split(':')[0] for cmd in c01 if cmd.startswith('addface') ])
    su_mf = set([ cmd.split(' ')[1].split(':')[0] for cmd in c01 if cmd.startswith('modface') ])
    su_cf = set(m0.get_lu_f()) - su_df
    su_mv = set([ cmd.split(' ')[1].split(':')[0] for cmd in c01 if cmd.startswith('move') ])
    
    m0.recolor( color_scheme['='] )
    m1.recolor( color_scheme['='] )
    
    mc = m0.clone().filter_faces( su_cf, True ).trim()
    md = m0.clone().filter_faces( su_df, True ).trim().recolor( color_scheme['-'] )
    
    mc2 = m1.clone().filter_faces( su_cf, True ).trim()
    ma2 = m1.clone().filter_faces( su_af, True ).trim().recolor( color_scheme['+'] )
    
    for v in mc2.lv:
        if v.u_v in su_mv:
            v.c = color_scheme['>']
    
    mc.rebuild_edges()
    mc2.rebuild_edges()
    md.rebuild_edges()
    ma2.rebuild_edges()
    
    meshes = [ mc.copy().extend(md), mc2.copy().extend(ma2) ]
    shapes = [ mesh.toShape() for mesh in meshes ]
    window = Viewer( shapes, caption='MeshGit Diff-Saved Viewer: %s -> %s (%s)' % (fn0,fn1,fnsave) )
    window.app_run()



#######################################################################################################################

def run_adj_viewer( fn ):
    lc = [[0.0,0.0,0.0],[0.25,0.0,0.5],[0.0,0.0,0.9],[0.0,0.9,0.0],[0.75,0.75,0.0],[0.9,0.5,0.0],[1.0,0.0,0.0],[1.0,1.0,1.0]]
    
    m0 = Mesh.fromPLY( fn )
    m0.optimize()
    m1 = m0.clone().recolor_adj_face_count(lc)
    m2 = Mesh()
    for f in m0.lf:
        u_f = f.u_f
        c = lc[ min( len(lc)-1, len(m0._la_f[u_f]['si_f']) ) ]
        lv = [ m0.opt_get_v(u_v).copy().recolor(c) for u_v in f.lu_v ]
        lu_v = [ v.u_v for v in lv ]
        m2.lv += lv
        m2.lf += [ Face(u_f=u_f, lu_v=lu_v) ]
    
    shapes = [ mesh.toShape( smooth=False ) if mesh else None for mesh in [ m0,m1,m2 ] ]
    window = Viewer( shapes, caption='MeshGit Adjacency Viewer', ncols=3 )
    window.app_run()


def run_warp_view( fn0, fn1 ):
    m0,m1 = Mesh.fromPLY(fn0),Mesh.fromPLY(fn1)
    m2,m3 = warp_meshes( m0, m1 )
    ls = [ m.recalc_vertex_normals().toShape(smooth=False) for m in [m0,m1,m2,m3] ]
    #ls = [ m.recalc_vertex_normals().toShape(smooth=False) for m in [m1,m2] ]
    window = Viewer( ls, caption='MeshGit Viewer (Warped)' )
    window.app_run()

########################################################################################################################

def run_strips_align( fn0, fn1 ):
    m0,m1 = [ Mesh.fromPLY( fn ) for fn in [fn0,fn1] ]
    m0.optimize()
    m1.optimize()
    stats0,stats1 = m0.get_stats(build_strips=True), m1.get_stats(build_strips=True)
    adj_e0,adj_f0,mra0,lring_sf0 = [ stats0[k] for k in ['adj_e','adj_f','mra','lring_sf'] ]
    adj_e1,adj_f1,mra1,lring_sf1 = [ stats1[k] for k in ['adj_e','adj_f','mra','lring_sf'] ]
    len0,len1 = len(lring_sf0),len(lring_sf1)
    
    lluv0 = [ [ Vertex.average( m0.opt_get_uf_lv(u_f) ) for u_f in ring_sf0 ] for ring_sf0 in lring_sf0 ]
    lluv1 = [ [ Vertex.average( m1.opt_get_uf_lv(u_f) ) for u_f in ring_sf1 ] for ring_sf1 in lring_sf1 ]
    
    lrc0 = [ [1.0,0.0,0.0] for r in lring_sf0 ]
    lrc1 = [ [0.0,1.0,0.0] for r in lring_sf1 ]
    
    m0.optimize()
    m1.optimize()
    
    print( 'aligning strips...' )
    align = [None]*len0
    
    if False:
        print( '  computing cost matrix...' )
        matcosts = [[0.0]*(len0+len1) for i in xrange(len0+len1)]
        # Q1
        for i in xrange(len1):
            for j in xrange(len1):
                matcosts[i+len0][j] = 15.0 * len(lring_sf1[j]) if i == j else float('inf')
        # Q3
        for i in xrange(len0):
            for j in xrange(len0):
                matcosts[i][j+len1] = 15.0 * len(lring_sf0[i]) if i == j else float('inf')
        # Q4 : do nothing
        # Q2
        t = time.time()
        pl = -1.0
        for i,luv0 in enumerate(lluv0):
            pc = float(i)/float(len0)*100.0
            if int(pl/5.0) != int(pc/5.0):
                td = time.time()-t
                print( '    %02i%% (%.2fm, eta %.2fm)...' % (pc,td,(td*100.0/pc-td) if pc > 0.0 else -1.0) )
                pl = pc
            # get verts of ring
            for j,luv1 in enumerate(lluv1):
                d = 0.0
                #d += abs(len(lring_sf0[i])-len(lring_sf1[j]))
                d += abs(sum(mra0[i])-sum(mra1[j])) * 4.0
                
                if abs(len(luv0)-len(luv1)) > 20 or d > 20.0:
                    d = float('inf')
                else:
                    d += compute_cost(
                        len(luv0), len(luv1),
                        lambda _i: 1.0,
                        lambda _i,_j: Vec3f.t_distance2( luv0[_i], luv1[_j] ),          # TODO: incl star cost!
                        lambda _j: 1.0
                        ) * 5.0
                
                matcosts[i][j] = d
        
        
        print( "  running..." )
        t = time.time()
        inds = hungarian( numpy.array( matcosts ) )
        print( "    finished in %f" % (time.time()-t) )
        
        for i,j in enumerate(inds):
            if j >= len1: continue
            if i >= len0: continue
            align[i] = j
            lrc0[i] = lrc1[j] = random_color()
    else:
        fp = open( 'align','rt' )
        for i,l in enumerate(fp.readlines()):
            if i >= len0: continue
            j = int(l)
            if j == -1: continue
            align[i] = j
            lrc0[i] = lrc1[j] = random_color()
    
    print( '  meshing...' )
    ma,mb = Mesh(),Mesh()
    #dfc0 = dict( (f.u_f,[1.0,1.0,1.0]) for f in m0.lf )
    #dfc1 = dict( (f.u_f,[1.0,1.0,1.0]) for f in m1.lf )
    dfc0 = dict( (f.u_f,[0.0,0.0,0.0]) for f in m0.lf )
    dfc1 = dict( (f.u_f,[0.0,0.0,0.0]) for f in m1.lf )
    
    for i,ring_sf in enumerate(lring_sf0):
        c = lrc0[i]
        for u_f in ring_sf:
            #dfc0[u_f][0] *= c[0]
            #dfc0[u_f][1] *= c[1]
            #dfc0[u_f][2] *= c[2]
            dfc0[u_f][0] += c[0] * 0.5
            dfc0[u_f][1] += c[1] * 0.5
            dfc0[u_f][2] += c[2] * 0.5
    for f in m0.lf:
        lv = [ m0.get_v(u_v).copy().recolor( dfc0[f.u_f] ) for u_v in f.lu_v ]
        ma.lv += lv
        ma.lf.append( Face(u_f=f.u_f,lu_v=[ v.u_v for v in lv ]) )
    
    for i,ring_sf in enumerate(lring_sf1):
        c = lrc1[i]
        for u_f in ring_sf:
            #dfc1[u_f][0] *= c[0]
            #dfc1[u_f][1] *= c[1]
            #dfc1[u_f][2] *= c[2]
            dfc1[u_f][0] += c[0] * 0.5
            dfc1[u_f][1] += c[1] * 0.5
            dfc1[u_f][2] += c[2] * 0.5
    for f in m1.lf:
        lv = [ m1.get_v(u_v).copy().recolor( dfc1[f.u_f] ) for u_v in f.lu_v ]
        mb.lv += lv
        mb.lf.append( Face(u_f=f.u_f,lu_v=[ v.u_v for v in lv ]) )
    
    shapes = [ mesh.toShape() for mesh in [ma,mb] ]
    window = Viewer( shapes, caption='MeshGit Aligned Strips Viewer' )
    window.app_run()

def run_strips_viewer( fn ):
    m = Mesh.fromPLY( fn )
    stats = m.get_stats(build_strips=True)
    adj_e,adj_f,mra,lring_sf = [ stats[k] for k in ['adj_e','adj_f','mra','lring_sf'] ]
    
    lrc = [ random_color() for r in lring_sf ]
    
    m0 = Mesh()
    dfc = dict( (f.u_f,[1.0,1.0,1.0]) for f in m.lf )
    for i,ring_sf in enumerate(lring_sf):
        c = lrc[i]
        for u_f in ring_sf:
            dfc[u_f][0] *= c[0]
            dfc[u_f][1] *= c[1]
            dfc[u_f][2] *= c[2]
    for f in m.lf:
        lv = [ m.get_v(u_v).copy().recolor( dfc[f.u_f] ) for u_v in f.lu_v ]
        m0.lv += lv
        m0.lf += [ Face(u_f=f.u_f,lu_v=[ v.u_v for v in lv ]) ]
    
    meshes = []
    rtouched = [False for i in xrange(len(lring_sf))]
    while not all(rtouched):
        mesh = Mesh()
        ftouched = dict( (f.u_f,False) for f in m.lf )
        for ir in xrange(len(lring_sf)):
            if rtouched[ir]: continue
            liw = [ir]
            while liw:
                liw = sorted(liw,key=lambda x:len(lring_sf[x]))
                i = liw.pop()
                #if rtouched[i]: continue
                if any( ftouched[u_f] for u_f in lring_sf[i] ): continue
                rtouched[i] = True
                c = lrc[i]
                for u_f in lring_sf[i]:
                    ftouched[u_f] = True
                    f = m.get_f(u_f)
                    lv = [ m.get_v(u_v).copy().recolor(c) for u_v in f.lu_v ]
                    mesh.lv += lv
                    mesh.lf += [ Face(lu_v=[ v.u_v for v in lv ]) ]
                liw.extend( [j for j in xrange(len(lring_sf)) if mra[i][j]] )
        meshes += [mesh]
    
    #shapes = [ m0.toShape(), Shape_Cycle( [ mesh.toShape() for mesh in meshes ], fps=1.0, bounce=False ) ]
    shapes = [ mesh.toShape() for mesh in [m0]+meshes ]
    window = Viewer( shapes, caption='MeshGit Strips Viewer', ncols=5 )
    window.app_run()
    
    #print( '-' * 80 )
    
    if False:
        print( 'graph = ximport("graph")' )
        print( 'g = graph.create(iterations=1000, distance=1.0, layout="spring")' )
        print( 'g.styles.default.fontsize = 10' )
        print( 'g.styles.default.align = CENTER' )
        for i,ring_sf in enumerate(lring_sf):
            print( 'g.add_node("R%03i",label="R%03i:%i,%i",radius=5.00)' % (i,i,len(ring_sf),sum(mra[i])) )
        for i in xrange(len(lring_sf)):
            for j in xrange(i+1,len(lring_sf)):
                if not mra[i][j]: continue
                print( 'g.add_edge("R%03i", "R%03i", length=%f, label="*")' % (i,j,random.uniform(50.0,200.0)) )
        print( 'g.layout.reset()' )
        print( 'g.solve()' )
        print( 'g.draw()' )
        print( '#graph._ctx.canvas.save("file.pdf")' )
        print( '-'*80 )
    




MeshGit ReadMe


Summary:
    MeshGit takes for input .ply files and outputs a visualization and/or a .ply file.


========================================================================================================================
Citation of code distributed in supplemental:
    hungarian.cpp and hungarian2.cpp:
        http://community.topcoder.com/tc?module=Static&d1=tutorials&d2=hungarianAlgorithm
        Author: "x-ray"
        License: ?
    
    munkres.cpp, matrix.h:
        Author: John Weaver
        License: GNU GPL
    
    fheap.h, fheap.cpp:
        http://en.wikipedia.org/wiki/Fibonacci_heap
        http://www.cse.yorku.ca/~aaw/Jason/FibonacciHeapAlgorithm.html
        Author: Erel Segal   http://tora.us.fm/rentabrain
    
    hungarian.py:
        https://github.com/scikit-learn/scikit-learn
        "Based on original code by Brain Clapper, adapted to numpy to scikits learn coding standards by G. Varoquaux"
        Copyright (c) 2008 Brian M. Clapper <bmc@clapper.org>, G Varoquaux
        Author: Brian M. Clapper, G Varoquaux
        License: BSD
    
    SMAC Matlab code:
        Graph Matching Toolbox in MATLAB
        Author: Timothee Cour
        Related Publication
        Timothee Cour, Praveen Srinivasan, Jianbo Shi. Balanced Graph Matching. Advances in Neural Information
            Processing Systems (NIPS), 2006
        License: GNU GPL
    
    code snippets are individually commented

License of ProggyTiny.ttf:
    Copyright (c) 2004, 2005 Tristan Grimmer

Bib/Licenses of meshes:
    Shaolin:
        author = {Edimar Dos Reis Silva},
        annote = {License: CC-BY},
        title = {Shaolin model},
        howpublished = {www.blendswap.com/3D-models/characters/shaolin},
        year = {2011},
    Lost Angel (sandals for Shaolin):
        author = {Edimar Dos Reis Silva},
        annote = {License: CC-0},
        title = {Lost Angel model},
        howpublished = {http://www.blendswap.com/blends/characters/lost-angel},
        year = {2011},
    Sintel:
        author = {{Blender Foundation}},
        annote = {License: CC-BY},
        title = {Sintel model},
        howpublished = {www.sintel.org},
        year = {2011},
    Chair:
        author = {{``Lumpycow''}},
        annote = {License: CC-BY},
        title = {Chair model},
        howpublished = {www.blendswap.com/3D-models/furniture/lumpycow\_household\_brokenchair/},
        year = {2010},
    Durano:
        author = {Pablo Vazquez},
        annote = {License: {CC-BY}?},
        title = {Durano model},
        howpublished = {Venom's Lab Blender Open Movie Workshop, vol.~4},
        year = {2009},
    Creature:
        author = {Andreas Goralczyk},
        annote = {License: CC-BY},
        title = {Creature model},
        howpublished = {Creature Factory Blender Open Movie Workshop, vol.~2},
        year = {2008}
    Hand:
        author = {Jonathan Williamson},
        annote = {License: {CC-BY}},
        title = {Hand Model},
        howpublished = {cgcookie.com/blender/2009/ 11/14/model-male-base-mesh/},
        year = {2009},
    Spaceship:
        author = {Fran\c{c}ois Grassard},
        annote = {License: {CC-BY}},
        title = {Small Spaceship (low poly)},
        howpublished = {www.blendswap.com/blends/vehicles/small-spaceship-low-poly/},
        year= {2011},
========================================================================================================================


Required (possibly non-standard) Python Libraries:
    scipy  : http://scipy.org/
    numpy  : http://numpy.scipy.org/
    pyglet : http://www.pyglet.org/
    
    pickle, json (or simplejson), md5, itertools, array, signal

MeshGit has been tested to work on OSX 10.6 and Kubuntu 11.10 Linux
    Apparently Pyglet under OSX 10.7 does work, but requires some effort :(
        see: http://blog.shuningbian.net/2012/01/getting-pyglet-setup-in-os-x-1072.html



Before running MeshGit:
    Compile the C++ code in the scripts folder with make (requires gcc,g++).
    
    The python scripts, executable binaries, and .ttf file must in one of the Python's sys.path paths.  For example, if
    the file structure looks like:
        models/
            model_0.ply
            model_a.ply
            model_b.ply
            ...
        scripts/
            meshgit.py
            viewer.py
            ProggyTiny.ttf
            ...
    
    you can run MeshGit from the models/ path as follows:
        models/$ ../scripts/meshgit.py view model_?.ply
    
    NOTE: screenshots, GUI states, cache/temp files, etc., are all written to the local path, which may overwrite other
    cache or state files.
    
    To use the SMAC method, you will need to have Matlab installed.  The Matlab code for this method is distributed with
    this project as supplemental material.  This code is under GNU General Public License (see above).
    
    The Shape Blend and ICP+Cuts methods load the results from files included in the models folder.  We have provided
    the configuration and mesh files necessary to rerun the experiment, but we have not included the binaries or any
    source code for these methods.  If ran, the discrete mapping of Shape Blend output must be written to
    durano03_durano14_mod.blendedmap, and the icpgc_test.py must be ran to convert ICP+Cuts results to matching.




Getting help:
    To get command-line help, run:
        scripts/$ meshgit.py help
    
    The viewer has many commands and many options that can be toggled, cycled, or set.  For help while in viewer, press
    the F1 key and look in terminal.  NOTE: Many of the commands require input through the terminal window.


How to reproduce the figures from the paper:
    fig 1a:
        models/$ ../scripts/meshgit.py diff durano08.ply durano11.ply
    
    fig 1b:
        models/$ ../scripts/meshgit.py merge hand0.ply handa.ply handb.ply handm.ply
    
    fig 4:
        models/$ ../scripts/meshgit.py diff_iter sintel0.ply sintel1.ply
    
    fig 5: smac_faces method REQUIRES Matlab (see above)!  this figure may take a while!
        models/$ ../scripts/meshgit.py diff_methods durano03.ply durano04_mod.ply greedy_iter hungarian_faces smac_faces
            blended_verts disney icpgc_verts
    
    fig 6:
        models/$ ../scripts/meshgit.py diff chair0.ply chair1.ply
    
    fig 7:
        models/$ ../scripts/meshgit.py diff_series durano??.ply
    
    fig 8:
        models/$ ../scripts/meshgit.py merge shaolin0.ply shaolina.ply shaolinb.ply shaolinm.ply
    
    fig 9: 1. diff only; 2. merge conflicts; 3. see all possible merge combinations
        models/$ ../scripts/meshgit.py diff spaceship0.ply spaceshipa.ply spaceshipb.ply
        models/$ ../scripts/meshgit.py merge_combinations spaceship0.ply spaceshipa.ply spaceshipb.ply
        models/$ ../scripts/meshgit.py merge spaceship0.ply spaceshipa.ply spaceshipb.ply spaceshipm.ply
    
    fig 10:
        models/$ ../scripts/meshgit.py diff creature0.ply creature1.ply












#!/usr/bin/env python2.7

"""
ply2matlab2 computes data needed to build correspondence between two meshes using the
spectral matching with affine constraint (SMAC) method

SMAC paper:             http://www.seas.upenn.edu/~timothee/papers/nips2006_spectral_matching.pdf
matlab implementation:  http://www.seas.upenn.edu/~timothee/software/graph_matching/graph_matching.html

jon denning, fabio pellacini, 2011-dec-26
"""

import os, sys, re, math, random
import scipy.io
from ply import *
from vec3f import *



# settings

k = 5
cnull = 10.0
caddrem = 0.0



def get_sparse_edges( lf ):
    # build sparse edge matrix from list of faces
    le = set()
    for f in lf:
        for v0,v1 in zip(f,f[1:]+[f[0]]):
            if v0 > v1: v0,v1 = v1,v0
            le.add( frozenset([v0,v1]) )
    lei,lej,les = [cv],[cv],[0]
    for fsv01 in le:
        v0,v1 = fsv01
        lei.append( v0 )
        lej.append( v1 )
        les.append( 1 )
        lei.append( v1 )
        lej.append( v0 )
        les.append( 1 )
    return ( lei, lej, les )

def compute_face_centers( lv, lf ):
    return [ Vec3f.t_average( [ lv[i] for i in f ] ) for f in lf ]

def get_face_adj_matrix( lf ):
    cf = len(lf)
    lfle = [ set( [ (min(v0,v1),max(v0,v1)) for v0,v1 in zip(f,f[1:]+[f[0]]) ] ) for f in lf ]
    de = dict( (e,[]) for fle in lfle for e in fle )
    for i_f,fle in enumerate(lfle):
        for e in fle: de[e].append(i_f)
    
    mfa = [ [False] * cf for i in xrange(cf) ]
    for e,li_f in de.items():
        for i in li_f:
            for j in li_f:
                mfa[i][j] = mfa[j][i] = True
    
    return mfa


if len(sys.argv) != 3:
    print( 'usage: %s [filename0] [filename1]' % sys.argv[0] )
    exit( 1 )
fnply0,fnply1 = sys.argv[1],sys.argv[2]



# load meshes

ply0 = ply_load( fnply0 )
lv0 = ply0['lv']
lf0 = [ f[1:] for f in ply0['lf'] ]
lfcv0 = compute_face_centers( lv0,lf0 )
cv0,cf0 = len(ply0['lv']),len(ply0['lf'])
mfa0 = get_face_adj_matrix( lf0 )

ply1 = ply_load( fnply1 )
lv1 = ply1['lv']
lf1 = [ f[1:] for f in ply1['lf'] ]
lfcv1 = compute_face_centers( lv1,lf1 )
cv1,cf1 = len(ply1['lv']),len(ply1['lf'])
mfa1 = get_face_adj_matrix( lf1 )


# compute feasibility (sparse) and affinity (dense) matrices for matching lf0 to lf1
# use dummy nodes to allow for added / deleted faces
#
#        feasibility matrix
#    cf0 < cf1        cf0 > cf1
# +-----+---+---+   +---+-------+
# |  S  | I | 0 |   |   |       |
# +-----+---+---+   | S |   I   |
# |     |       |   |   |       |
# |  I  |   I   |   +---+-------+
# |     |       |   | I |       |
# +-----+-------+   +---+   I   |
#                   | 0 |       |
#                   +---+-------+

print( 'computing feasibility matrix for %ix%i matches...' % (cf0,cf1) )
F = []

for i,fcv0 in enumerate(lfcv0):                                     # using k-NN to determine feasibility
    lj = Vec3f.t_knn( k, fcv0, lfcv1 )
    F.extend( [ (i,j) for j,_ in lj ] )
# dummy nodes
#F.extend( [ (i,i+cf1) for i in xrange(cf0) ] )                      # removed faces
#F.extend( [ (cf0+j,j) for j in xrange(cf1) ] )                      # added faces
#F.extend( [ (cf0+ij,cf1+ij) for ij in xrange(max(cf0,cf1)) ] )      # null matchings

n12 = len(F)                                                        # number of feasible matches
print( '  %i feasible matches' % n12 )


print( 'computing affinity matrix for %ix%i matches...' % (n12,n12) )
A = [ [0.0] * n12 for i in xrange(n12) ]

for p in xrange(n12):
    i0,j0 = F[p]
    for q in xrange(p+1):                                                   # only compute lower triangle
        i1,j1 = F[q]
        # compute affinity for matching both i0->j0 and i1->j1
        # by computing distance between i0,i1 and j0,j1
        if i0 < cf0 and j0 < cf1 and i1 < cf0 and j1 < cf1:
            if (i0 == i1 and j0 != j1) or (i0 != i1 and j0 == j1):
                A[p][q] = 0.0
            else:
                di01 = Vec3f.t_distance2( lfcv0[i0], lfcv0[i1] ) * 10.0
                dj01 = Vec3f.t_distance2( lfcv1[j0], lfcv1[j1] ) * 10.0
                A[p][q] = math.exp(-( di01 - dj01 )**2)
                A[p][q] += math.exp(-Vec3f.t_distance2( lfcv0[i0], lfcv1[j0] ))
                A[p][q] += math.exp(-Vec3f.t_distance2( lfcv0[i1], lfcv1[j1] ))
                if mfa0[i0][i1] and mfa1[j0][j1]: A[p][q] += 5.0
        elif i0 >= cf0 and j0 >= cf1 and i1 >= cf0 and j1 >= cf1:
            A[p][q] = cnull
        else:
            A[p][q] = caddrem

#print( 'writing data to matlab file...' )
#fp = open( 'meshgit_SMAC_loaddata.m', 'wt' )
#fp.write( 'function [n1,n2,E12,n12,W] = meshgit_SMAC_loaddata()\n' )
#fp.write( 'n1 = %i;\n' % cf0 )
#fp.write( 'n2 = %i;\n' % cf1 )
#fp.write( 'n12 = %i;\n' % n12 )
#fp.write( 'E12i = [%s];\n' % (';'.join([ str(i+1) for i,j in F ])) )
#fp.write( 'E12j = [%s];\n' % (';'.join([ str(j+1) for i,j in F ])) )
#fp.write( 'E12s = [%s];\n' % (';'.join([ '1' ] * n12)) )
#fp.write( 'E12 = sparse(E12i,E12j,E12s);\n' )
#fp.write( 'W = [%s];\n' % (';'.join([ ' '.join([ str(w) for w in Ar ]) for Ar in A ])) )
#fp.close()

data = {}
data['n1'] = cf0
data['n2'] = cf1
data['n12'] = n12
data['E12i'] = [ i+1 for i,j in F ]
data['E12j'] = [ j+1 for i,j in F ]
#data['E12s'] = [1] * n12
data['W'] = A
scipy.io.savemat( 'SMACdata.mat', data )


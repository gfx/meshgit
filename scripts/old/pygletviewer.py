import math, sys, time
from mesh import *
from vec3f import *
from camera import *
from shapes import *

import pyglet
from pyglet.gl import *
from pyglet.window import key

# The code below is based on the following:
#   original c-based: Richard Campbell '99, http://nehe.gamedev.net
#   python/pyopengl port: John Ferguson '00, hakuin@voicenet.com
#   pyglet port: Jess Hill (Jestermon) '09, jestermon.weebly.com, jestermonster@gmail.com

# refs:
#   http://pyglet.org/doc/programming_guide.pdf
#   http://swiftcoder.wordpress.com/2008/12/08/pong/
#   http://tartley.com/files/stretching_pyglets_wings/presentation/
#   http://jestermon.weebly.com/nehe-pyglet-tutorials.html

class LightingModes:
    NO_LIGHT, SIMPLE, SIMPLE_FALLOFF = xrange(3)
    @classmethod
    def Cycle( cls, v ): return (v+1)%3

class BackgroundColors:
    BLACK, GRAY, WHITE = xrange(3)
    @classmethod
    def Cycle( cls, v ): return (v+1)%3

class Viewer( pyglet.window.Window ):
    
    def __init__( self, shapes, ncols=None, caption=None, on_key_callback=None ):
        if caption is None: caption = "pyglet 3D Viewer"
        
        config = Config( sample_buffers=1, samples=4, depth_size=16, double_buffer=True )
        try:
            super(Viewer,self).__init__(resizable=True, config=config, caption=caption)
        except:
            super(Viewer,self).__init__(resizable=True, caption=caption)
        
        self.shapes = [None]
        self.ncols = 1
        self.nrows = 1
        
        self.on_key_callback = on_key_callback
        self._setup()
        self._initgl()
        
        pyglet.clock.schedule_interval( self.update, 1.0/60.0 )
        
        if shapes: self.setup( shapes, ncols=ncols )
    
    def app_run( self ):
        pyglet.app.run()
        #self.close()
    
    def setup( self, shapes, ncols=None ):
        if not ncols: ncols = len(shapes)
        self.ncols = ncols
        self.nrows = int(len(shapes)/ncols)
        self.shapes = shapes
        
        self.vw,self.vh = 500,500
        self.width,self.height = self.vw*self.ncols,self.vh*self.nrows
        self.set_location( (self.screen.width - self.width) / 2, (self.screen.height - self.height) / 2 )
    
    def _setup( self ):
        self.tx,self.ty,self.cd = -math.pi/4.0,math.pi/4.0+0.05,6.0
        self.camera = Camera( (0.0,0.0,-6.0), (0.0,0.0,0.0), (0.0,0.0,1.0) )
        
        self.light = LightingModes.SIMPLE
        self.lighting = True
        self.axes = False
        self.grid = False
        self.wires = True
        self.separated = True
        self.bg = BackgroundColors.GRAY
        self.mouse = True
        self.dividers = True
        
        self.mx,self.my,self.mb = -50,50,0
        
        self.keymap = pyglet.window.key.KeyStateHandler()
        self.push_handlers(self.keymap)
        
        self.shape_axes = Shape( GL_LINES, [ 0,0,0, 10,0,0, 0,0,0, 0,10,0, 0,0,0, 0,0,10 ], [ 1,0,0, 1,0,0, 0,1,0, 0,1,0, 0,0,1, 0,0,1 ] )
        gridlines = [ c for x in [ [ i,10,0, i,-10,0, 10,i,0, -10,i,0 ] for i in range(-10,11) ] for c in x ]
        self.shape_grid = Shape( GL_LINES, gridlines, [ 0.15, 0.15, 0.15 ] )
        
        pyglet.font.add_file('ProggyTiny.ttf')
        self.label = pyglet.text.Label(self.caption, x=0, y=0, color=(255, 255, 0, 255), anchor_x='left',anchor_y='top', font_size=12,font_name='ProggyTinyTT' )
    
    def _initgl( self ):
        glClearDepth( 1.0 )
        
        glDepthFunc( GL_LEQUAL )
        glEnable( GL_DEPTH_TEST )
        glPolygonOffset( 10.0, 10.0 )
        
        glShadeModel( GL_SMOOTH )
        
        glHint( GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST )
        glLightfv( GL_LIGHT1, GL_AMBIENT, self.vec( 0.4, 0.4, 0.4, 1.0 ) )
        glLightfv( GL_LIGHT1, GL_DIFFUSE, self.vec( 0.5, 0.5, 0.5, 1.0 ) )
        glLightfv( GL_LIGHT1, GL_POSITION, self.vec( 0.0, 0.1, 0.0, 1.0 ) )
        glEnable( GL_LIGHT1 )
        
        glLightModeli( GL_LIGHT_MODEL_TWO_SIDE, GL_TRUE )
        glDisable( GL_CULL_FACE )
        
        glEnable(GL_COLOR_MATERIAL)
        glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE )
    
    def update( self, dt ):
        self.handle_keys()
        self.draw_scene()
    
    def on_draw(self):
        self.draw_scene()
    
    def on_resize( self, w, h ):
        self.ReSizeGLScene( w, h )
    
    def UpdateGL( self ):
        if self.light: glEnable( GL_LIGHTING )
        else: glDisable( GL_LIGHTING )
        
        e = (self.cd,0,0)
        e = self.rot_about_direction( e, (0,1,0), self.tx )
        e = self.rot_about_direction( e, (0,0,1), self.ty )
        e = (e[0]+self.camera.fx,e[1]+self.camera.fy,e[2]+self.camera.fz)
        self.camera.ex,self.camera.ey,self.camera.ez = e
        
        #ux,uy,uz = self.camera.get_up()
        #e = [ self.camera.ex+ux*0.1, self.camera.ey+uy*0.1, self.camera.ez+uz*0.1 ]
        #e = [ self.camera.ex, self.camera.ey, self.camera.ez ]
        glLightfv( GL_LIGHT1, GL_POSITION, self.vec( 0.0, 0.1, 0.0, 1.0 ) )
        
        if self.bg == BackgroundColors.BLACK:   glClearColor( 0.0, 0.0, 0.0, 1.0 )
        elif self.bg == BackgroundColors.GRAY:  glClearColor( 0.25, 0.25, 0.25, 1.0 )
        elif self.bg == BackgroundColors.WHITE: glClearColor( 1.0, 1.0, 1.0, 1.0 )
        
        if self.light == LightingModes.NO_LIGHT:
            glDisable( GL_LIGHTING )
        elif self.light == LightingModes.SIMPLE:
            glEnable( GL_LIGHTING )
            glLightf( GL_LIGHT1, GL_CONSTANT_ATTENUATION, 1.0 )
            glLightf( GL_LIGHT1, GL_QUADRATIC_ATTENUATION, 0.0 )
        elif self.light == LightingModes.SIMPLE_FALLOFF:
            glLightf( GL_LIGHT1, GL_CONSTANT_ATTENUATION, 0.0 )
            glLightf( GL_LIGHT1, GL_QUADRATIC_ATTENUATION, 0.075 )
    
    def ReSizeGLScene( self, width, height ):
        if height == 0: height = 1
        self.vw,self.vh = width/self.ncols,height/self.nrows
    
    
    def vec( self, *args ):
        return (GLfloat * len(args))(*args)
    
    def _light_toggle( self ):
        self.lighting = not self.lighting
        if self.lighting:
            if self.light != LightingModes.NO_LIGHT:
                glEnable( GL_LIGHTING )
        else:
            if self.light != LightingModes.NO_LIGHT:
                glDisable( GL_LIGHTING )
    
    def rot_about_direction( self, p, d, theta ):
        x,y,z = p
        u,v,w = d
        uxvywz,uvw,uvw2 = (u*x+v*y+w*z),math.sqrt(u*u+v*v+w*w),u*u+v*v+w*w
        ct,st = math.cos(theta),math.sin(theta)
        return (
            (u*uxvywz*(1-ct) + uvw2*x*ct + uvw*(v*z-w*y)*st) / uvw2,
            (v*uxvywz*(1-ct) + uvw2*y*ct + uvw*(w*x-u*z)*st) / uvw2,
            (w*uxvywz*(1-ct) + uvw2*z*ct + uvw*(u*y-v*x)*st) / uvw2,
            )
    
    def tumble( self, dtx, dty ):
        self.tx = max( -math.pi/2+0.01, min( math.pi/2-0.01, self.tx+dtx ) )
        self.ty += dty
    
    def dolly( self, dinout ):
        self.cd *= dinout
    
    
    
    def draw_scene( self ):
        if self.context is None or self.has_exit: return
        
        self.UpdateGL()
        glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT )
        
        if self.separated:
            self.camera.resize( self.vw, self.vh )
            for i,shape in enumerate(self.shapes):
                if not shape: continue
                x,y = i % self.ncols,(self.nrows-1-int(i/self.ncols))
                glViewport( self.vw*x, self.vh*y, self.vw, self.vh )
                self.draw_axes()
                self.draw_grid()
                shape.render()
                self.draw_wires( shape )
        else:
            self.camera.resize( self.width, self.height )
            glViewport( 0, 0, self.width, self.height )
            self.draw_axes()
            self.draw_grid()
            for i,shape in enumerate(self.shapes):
                if not shape: continue
                shape.render()
                self.draw_wires( shape )
                
        
        glClear( GL_DEPTH_BUFFER_BIT )
        glViewport( 0, 0, self.width, self.height )
        glMatrixMode( GL_PROJECTION )
        glLoadIdentity()
        glOrtho( 0, self.width, -self.height, 0, -1, 1 )
        glMatrixMode( GL_MODELVIEW )
        glLoadIdentity()
        
        self.draw_dividers()
        self.draw_textinfo()
        self.draw_mouse()
    
    def draw_dividers( self ):
        if not self.dividers: return
        if not self.separated: return
        glLineWidth( 3.0 )
        glBegin( GL_LINES )
        glColor3f( 0.15, 0.15, 0.15 )
        for x in xrange(self.ncols):
            glVertex2f( x*self.vw, 0 )
            glVertex2f( x*self.vw, -self.height )
        for y in xrange(self.nrows):
            glVertex2f( 0, -y*self.vh )
            glVertex2f( self.width, -y*self.vh )
        glEnd()
    
    def draw_axes( self ):
        if not self.axes: return
        self._light_toggle()
        glLineWidth( 2.0 )
        self.shape_axes.render()
        self._light_toggle()    
    
    def draw_grid( self ):
        if not self.grid: return
        self._light_toggle()
        glLineWidth( 0.25 )
        self.shape_grid.render()
        self._light_toggle()
    
    def draw_wires( self, shape ):
        if not self.wires: return
        self._light_toggle()
        glLineWidth( 2.0 )
        shape.render_wires()
        self._light_toggle()
    
    def draw_textinfo( self ):
        self._light_toggle()
        self.label.draw()
        self._light_toggle()
    
    def draw_mouse( self ):
        if not self.mouse: return
        if self.mx < 0 or self.mx >= self.width: return
        if self.my > 0 or self.my <= -self.height: return
        if self.mb != 4: return
        
        self._light_toggle()
        glPointSize( 10.0 )
        glBegin( GL_POINTS )
        if self.separated:
            for x in xrange(self.ncols):
                for y in xrange(self.nrows):
                    if x == 0 and y == 0:   glColor3f( 1, 1, 0 )
                    else:                   glColor3f( 0, 1, 1 )
                    glVertex2f( self.mx + self.vw*x, self.my + self.vh*y )
                    glVertex2f( self.mx + self.vw*x, self.my - self.vh*y )
                    glVertex2f( self.mx - self.vw*x, self.my - self.vh*y )
                    glVertex2f( self.mx - self.vw*x, self.my + self.vh*y )
        else:
            glColor3f( 1, 1, 0 )
            glVertex2f( self.mx, self.my )
        glEnd()
        self._light_toggle()
        
    
    def on_key_press( self, symbol, modifiers ):
        if self.on_key_callback:
            if self.on_key_callback( key.symbol_string( symbol ) ):
                pyglet.app.exit()
        
        if symbol == key.ESCAPE:self.close()    #pyglet.app.exit()
        if symbol == key.L:     self.light = LightingModes.Cycle( self.light )
        if symbol == key.A:     self.axes = not self.axes
        if symbol == key.B:     self.bg = BackgroundColors.Cycle( self.bg )
        if symbol == key.D:     self.dividers = not self.dividers
        if symbol == key.G:     self.grid = not self.grid
        if symbol == key.O:     self.separated = not self.separated
        if symbol == key.W:     self.wires = not self.wires
        if symbol == key.NUM_1: self.tx,self.ty = 0.0,0.0
        if symbol == key.NUM_3: self.tx,self.ty = 0.0,math.pi/2.0
        if symbol == key.NUM_7: self.tx,self.ty = -math.pi/2.0,0.0
        if symbol == key.NUM_DECIMAL:
            self.camera.ex,self.camera.ey,self.camera.ez = self.camera.fx-self.camera.ex,self.camera.fy-self.camera.ey,self.camera.fz-self.camera.ez
            self.camera.fx,self.camera.fy,self.camera.fz = 0,0,0
            
    
    def handle_keys( self ):
        if self.keymap[key.DOWN]:           self.tumble( -0.03,  0.00 )
        if self.keymap[key.UP]:             self.tumble(  0.03,  0.00 )
        if self.keymap[key.RIGHT]:          self.tumble(  0.00, -0.05 )
        if self.keymap[key.LEFT]:           self.tumble(  0.00,  0.05 )
        if self.keymap[key.NUM_ADD]:        self.dolly( 0.952380952381 )
        if self.keymap[key.NUM_SUBTRACT]:   self.dolly( 1.05 )
    
    def _screen_to_gl( self, x, y, b ):
        return ( x, y - self.height, b )
    
    def on_mouse_press( self, x, y, button, modifiers ):
        self.mx,self.my,self.mb = self._screen_to_gl( x, y, button )
    
    def on_mouse_release( self, x, y, button, modifiers ):
        self.mx,self.my,self.mb = self._screen_to_gl( x, y, 0 )
    
    def on_mouse_drag( self, x, y, dx, dy, buttons, modifiers ):
        self.mx,self.my,self.mb = self._screen_to_gl( x, y, buttons )
        if buttons == 2:
            if modifiers == 0:
                self.tumble( dy / 100.0, -dx / 100.0 )
            else:
                rx,ry,rz = self.camera.get_right()
                ux,uy,uz = self.camera.get_up()
                d = self.camera.get_dist()
                rv = -dx * d * 0.002
                uv = -dy * d * 0.002
                self.camera.fx += rx*rv + ux*uv
                self.camera.fy += ry*rv + uy*uv
                self.camera.fz += rz*rv + uz*uv
    
    def on_mouse_motion( self, x, y, dx, dy ):
        self.mx,self.my,self.mb = self._screen_to_gl( x, y, 0 )
    
    def on_mouse_leave( self, x, y ):
        self.mx,self.my,self.mb = self._screen_to_gl( x, y, 0 )
        #self.mx,self.my = -50,-50
    
    def on_mouse_scroll( self, x, y, scroll_x, scroll_y ):
        if scroll_y > 0: r = 0.952380952381
        else: r = 1.05
        for i in xrange(abs(scroll_y) * 3):
            self.dolly( r )



if __name__ == '__main__':
    shapes = [
        Shape( GL_POLYGON, [ 0.0,1.0,0.0, 1.0,-1.0,0.0, -1.0,-1.0,0.0 ], [ 1.0,0.0,0.0, 0.0,1.0,0.0, 0.0,0.0,1.0 ] ),
        Shape( GL_QUADS, [ -1.0,1.0,0.0, 1.0,1.0,0.0, 1.0,-1.0,0.0, -1.0,-1.0,0.0 ], [ 0.3,0.5,1.0 ] ),
        Shape( GL_QUADS, [
                 1.0, 1.0, 1.0,   1.0,-1.0, 1.0,  -1.0,-1.0, 1.0,  -1.0, 1.0, 1.0,   # front
                 1.0, 1.0,-1.0,   1.0,-1.0,-1.0,  -1.0,-1.0,-1.0,  -1.0, 1.0,-1.0,   # back
                 1.0, 1.0, 1.0,   1.0, 1.0,-1.0,  -1.0, 1.0,-1.0,  -1.0, 1.0, 1.0,   # right
                 1.0,-1.0, 1.0,   1.0,-1.0,-1.0,  -1.0,-1.0,-1.0,  -1.0,-1.0, 1.0,   # left
                 1.0, 1.0, 1.0,   1.0,-1.0, 1.0,   1.0,-1.0,-1.0,   1.0, 1.0,-1.0,   # top
                -1.0, 1.0, 1.0,  -1.0,-1.0, 1.0,  -1.0,-1.0,-1.0,  -1.0, 1.0,-1.0,   # bottom
            ], [ 0.5,0.5,0.5 ], norms=[
                 0.0, 0.0,-1.0,   0.0, 0.0,-1.0,   0.0, 0.0,-1.0,   0.0, 0.0,-1.0,
                 0.0, 0.0,-1.0,   0.0, 0.0,-1.0,   0.0, 0.0,-1.0,   0.0, 0.0,-1.0,
                 0.0, 1.0, 0.0,   0.0, 1.0, 0.0,   0.0, 1.0, 0.0,   0.0, 1.0, 0.0,
                 0.0, 1.0, 0.0,   0.0, 1.0, 0.0,   0.0, 1.0, 0.0,   0.0, 1.0, 0.0,
                 1.0, 0.0, 0.0,   1.0, 0.0, 0.0,   1.0, 0.0, 0.0,   1.0, 0.0, 0.0,
                 1.0, 0.0, 0.0,   1.0, 0.0, 0.0,   1.0, 0.0, 0.0,   1.0, 0.0, 0.0,
            ]),
        ]
    window = Viewer( shapes )
    window.app_run()


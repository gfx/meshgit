# -*- coding: utf-8 -*-

import os, glob, sys, subprocess, time, json, re, platform
from array import array

################################################################################
### config


##########################
# may need to modify below

# OSX - use symlink to blender.app application bundle

blenderpath = {
    "Darwin":  "../iblender/blender.app/Contents/MacOS",
    "Linux":   "../iblender",
    "Windows": "",                                         # TODO
    }[ platform.system() ]



#################################
# should not have to modify these

blenderexe  = os.path.join( blenderpath, "blender" )

# instrumentation cmdline options
blenderopts_instrument = ["--instrument","--window-geometry","0","0","1024","768"]

# exporting cmdline options
blenderopts_export_json   = ['--python', '../scripts/blender_export_near.py', '-b']
blenderopts_export_json2  = ['--python', '../scripts/blender_export_near.py', '--window-geometry','0','0','100','100']

# all files below are local
modelfile     = "model.blend"
stepsfile     = "steps.txt"
stepcountfile = "stepcount.txt"
stepfile      = "step%06d.blend"
stepfilere    = "step[0-9]{6}\\.blend"  # escaped backslash!
plyfile       = "step%06d.blend.ply"
jsonfile      = "step%06d.json"
allstepfiles  = "step*.blend"
allplyfiles   = "step*.blend*.ply"
alljsonfiles  = "step??????.json"
allblendfiles = "*.blend"

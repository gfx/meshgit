import json, re, time, os, sys, hashlib, copy

"""
notes:
* index (idx) is internal stuff.  any function that takes idx as param or
  returns an idx is considered an internal function (prefixed with an _)
* uids are unique across all geometry (vert, edge, face)

todo:
* test to extrude tri->tetrahedron, quad->cube
* add meshprov fns to extrude loop of edges (blender style extrude)
* have meshprov fns return enough info to pass to inverse function to undo the
  work as well as to update correspondence mapping
* mod meshprov object vars to be internal (with _ prefix)

variable naming conventions
  b: branch, n: name
  i: index, u: uid, t: type (string specifying type as 'v','e','f')
  v: vert, e: edge, f: face, g: geometry (v,e,f)
  li: list of indices, lu: list of uids, ...
  co: coordinates
"""

def test1():
    print( "Creating a quad..." )
    print( "  verts: av000, av003, av001, av007" )
    print( "  edges: ae002, ae004, ae005, ae008" )
    print( "  faces: af006" )
    m = MeshProvenance()
    print( m.new_v() )
    print( m.extrude('av000',[],[],[]) )
    print( m.extrude('av000',[],['ae002'],[]) )
    print( m.extrude('av001',['ae005'],[],['af006']) )
    return m

def test2():
    print( "Create a quad, then collapse it to a vert..." )
    m = test1()
    print( m.collapse('ae002') )
    print( m.collapse('ae005') )
    print( m.collapse('ae004') )
    return m

def test3():
    m = test1()
    m.extrude_edgeloop_macro( ['ae002','ae004','ae005','ae008'],['af006'] )
    return m

def test4():
    print( "Testing naive merge..." )
    mg = MeshGit()
    
    # create trunk branch
    tb = mg.get_trunk()
    tm = tb.mesh
    tm.new_v()
    tm.extrude('av000',[],[],[])
    tm.extrude('av000',[],['ae002'],[])
    tm.extrude('av001',['ae005'],[],['af006'])
    
    # create extrude branch
    eb = mg.new_branch(tb,'extrude',prefix='b')
    em = eb.mesh
    em.extrude_edgeloop_macro( ['ae002','ae005','ae004','ae008'],['af006'] )
    em.extrude_blender_macro( ['af006'] )
    em.extrude_blender_macro( ['bf028'] )
    #em.extrude_edgeloop_macro( ['be013','be011','be017','be021'],['af006'] )
    
    tm.split_edge( 'ae002' )
    tm.split_edge( 'ae005' )
    # split face?
    
    print( "trunk:" )
    tm.listinfo()
    print( "extrude:" )
    em.listinfo()
    
    print( "extrude.ops:" )
    print( "\n".join([ str(op[:2]) for op in em.ops ]) )
    
    # merge extrude branch into trunk
    mg.merge_branch_naive( eb )
    tm.listinfo()
    
    mg.get_trunk().mesh.nodebox()
    return mg

class MeshGit:
    def __init__( self ):
        self.lb = [ MeshGitBranch( 'trunk', None ) ]
    
    def get_trunk( self ):
        return self.lb[0]
    
    def get_branch( self, n_b ):
        ln_b = [ b.name for b in self.lb ]
        if n_b not in ln_b: return None
        return self.lb[ ln_b.index(n_b) ]
    
    # NOTE: should not have to keep around copies of parent... just undo the
    # operations of parent until rev matches to get original branch
    def new_branch( self, bo, n_bn, prefix=None ):
        if type(bo) is str: bo = self.get_branch( n_bo )
        bn = MeshGitBranch( n_bn, bo, prefix=prefix )
        self.lb += [ bn ]
        return bn
    
    # merges branch into its parent branch by executing ops from branch in the
    # parent.  NOTE: does not look for conflicts!
    def merge_branch_naive( self, b1 ):
        if type(b1) is str: b1 = self.get_branch( b1 )
        b0 = b1.parent['branch']                        # get parent branch
        m0,m1 = b0.mesh,b1.mesh                         # mesh of branches
        mr = b1.parent['copy']
        
        mapping = {}
        for ur in mr.udata.keys():
            u_r0 = list(set([ _u for _u in m0.get_descendents(ur) if m0.is_alive(_u) ]) - set([ _u for _u in mr.get_descendents(ur) if mr.is_alive(_u) ]))
            mapping[ur] = u_r0 + [ur]
        
        in_macro = 0
        for op in m1.ops:
            n_op = op[0]
            
            if in_macro:
                if n_op == 'done_macro': in_macro -= 1
                else: in_macro += n_op.endswith("_macro") and 1 or 0
                continue
            
            if len(op) == 1:
                print( str(op) ) 
            lp_op = op[1]
            r_op = op[2]
            
            lp_opn = []
            
            # translate op params
            print( "translating..." )
            for p_op in lp_op:
                if p_op is None:
                    p_opn = None
                else:
                    if type(p_op) is str and len(mapping[p_op]) == 1:
                        p_opn = mapping[p_op][0]
                    else:
                        p_opn = [ _p for l in [ mapping[_p_op] for _p_op in p_op ] for _p in l ]
                lp_opn += [p_opn]
            print( "fr: %s" % str(lp_op) )
            print( "to: %s" % str(lp_opn) )
            
            r = m0.execute( n_op, lp_opn )
            
            m0.nodebox()
            
            # update mapping using r_op and r
            # every new v,e,f in r_op points to correponding new v,e,f in r
            print( "r    = %s" % str(r) )
            print( "r_op = %s" % str(r_op) )
            
            for u_g10,u_g11 in [ lg for lg in r_op if len(lg) == 2 and lg[0] != '-' ]:
                u_g00 = mapping[u_g10]
                mapping[u_g11] = [ l[1] for l in r if len(l) == 2 and l[0] in u_g00 ]
            mapping = { k:[v for v in lv if m0.is_alive(v)] for k,lv in mapping.items() }
            
            print( "mapping = %s" % str(mapping) )
            
            in_macro += n_op.endswith("_macro") and 1 or 0
        
        m1.merged = True
        return m0
    
    # merges branch into its parent branch
    # looks for conflicts
    def merge_branch_conflict( self, b1 ):
        if type(b1) is str: b1 = self.get_branch( b1 )
        b0 = b1.parent.branch                           # get parent branch
        m0,m1 = b0.mesh,b1.mesh                         # mesh of branches
        lo0,lo1 = b0.ops[b1.parent.rev:], b1.ops        # list of ops to merge
        
        m0,m1,mm = [ copy.deepcopy( b1.parent.copy ) for i in range(3) ]
        
        # init mappings, which should all be the same since they all start from
        # same root
        map_m0,map_0m,map_m1,map_1m = [ { k:[k] for k,v in mm.udata.items() if v['alive'] } for i in range(4) ]
        
        # now, execute the commands!
        

class MeshGitBranch:
    def __init__( self, name, b_parent, prefix = None ):
        self.name = name
        if b_parent is None:
            # only done with trunk!
            self.parent = None
            self.mesh = MeshProvenance()
        else:
            self.parent = {
                'branch': b_parent,
                'name': b_parent.name,
                'rev': b_parent.mesh.get_revision(),
                'copy': copy.deepcopy( b_parent.mesh ),
                }
            self.mesh = copy.deepcopy( b_parent.mesh )
            self.mesh.ops = []
        if prefix is not None: self.mesh.prefix = prefix
        self.merged = False

class MeshProvenance:
    def __init__( self, mesh=None ):
        # init main data
        self.udata = {}         # provenance (uid) data
        self.prefix = 'a'       # uid prefix
        self.mdata = { 'livu': [], 'lieu': [], 'lifu': [], 'v': [], 'e': [], 'f': [] }
        self.ops = []
        self.op_invs = []
        if mesh: self._init_blender_mesh( mesh )
    
    # init mdata to given blender mesh data
    def _init_blender_mesh( self, mesh ):
        self.mdata['v'] = [ {
            'co': list( v.co ),
            'lie': [ i_e for i_e,e in enumerate(mesh.edges) if i_v in e.vertices ],
            'lif': [ i_f for i_f,f in enumerate(mesh.faces) if i_v in f.vertices ],
            'uv': self._get_new_uid('v'),
            } for i_v,v in enumerate(mesh.vertices) ]
        self.mdata['e'] = [ {
            'liv': list( e.vertices ),
            'lif': [ i_f for i_f,f in enumerate(mesh.faces) if all([i_v in f.vertices for i_v in e.vertices]) ],
            'ue': self._get_new_uid('e'),
            } for i_e,e in enumerate(mesh.edges) ]
        self.mdata['f'] = [ {
            'liv': list( f.vertices ),
            'lie': [ i_e for i_e,e in enumerate(mesh.edges) if all([i_v in f.vertices for i_v in e.vertices]) ],
            'uf': self._get_new_uid('f'),
            } for i_f,f in enumerate(mesh.faces) ]
        self.mdata['livu'] = [ v['uv'] for v in self.mdata['v'] ]
        self.mdata['lieu'] = [ e['ue'] for e in self.mdata['e'] ]
        self.mdata['lifu'] = [ f['uf'] for f in self.mdata['f'] ]
    
    def get_revision( self ):
        return len(self.ops)
    
    # a hack to return some hashing of the mdata (useful for finding undo?)
    # NOTE: do not include uid info... may change
    def get_mdata_hash( self ):
        md5 = hashlib.md5()
        for k in ['v','e','f']:
            d = self.mdata[k]
            lk2 = [ k2 for k2 in d.keys() if not k2[0] == 'u' ]
            for k2 in lk2:
                md5.update( bytes( str( d[k2] ), 'UTF-8' ) )
        return md5.hexdigest()
    
    # creates new uid of type t with specified parent as uid
    # NOTE: does not add to mdata!  this is a help fn for the _new_v, _new_e,
    # and _new_f fns below
    # NOTE: geometry may have multiple parents (merge)
    def _get_new_uid( self, t, u_parents = None ):
        if u_parents is None: u_parents = []
        if type(u_parents) is str: u_parents = [u_parents]
        
        nuid = "%s%s%03d" % ( self.prefix, t, len(self.udata) )
        self.udata[nuid] = { 'parents': u_parents, 'children': [], 'type': t, 'alive':True }
        for u_p in u_parents:
            self.udata[u_p]['children'] += [nuid]
        return nuid
    
    def get_parents( self, u ):
        if type(u) is list:
            return [ self.udata[us]['parents'] for us in u ]
        return self.udata[u]['parents']
    
    def get_children( self, u ):
        if type(u) is list:
            return [ self.udata[us]['children'] for us in u ]
        return self.udata[u]['children']
    
    def get_descendents( self, u ):
        if type(u) is list:
            return list(set([ _u for __u in [ self.get_descendents( _u ) for _u in u ] for _u in __u ]))
        lu_c = self.get_children( u )
        return list(set(lu_c + [ _u for __u in [ self.get_descendents(u_c) for u_c in lu_c ] for _u in __u ]))
    
    def is_alive( self, u ):
        return self.udata[u]['alive']
        
    def is_dead( self, u ):
        return not self.udata[u]['alive']
    
    def get_type( self, u ):
        return self.udata[u]['type']
    
    def _get_i( self, u ):
        t = self.udata[u]['type']
        litu = "li%su" % t
        return self.mdata[litu].index(u)
    
    # returns the vert, edge, or face with the specified uid
    def get_g( self, u ):
        t = self.udata[u]['type']
        i = self._get_i( u )
        return self.mdata[t][i]
    
    # return uid of specified geometry
    def _get_u( self, t, i ):
        return self.mdata[t][i]['u%s'%t]
    def _get_u_v( self, i_v ):
        return self.mdata['v'][i_v]['uv']
    def _get_u_e( self, i_e ):
        return self.mdata['e'][i_e]['ue']
    def _get_u_f( self, i_f ):
        return self.mdata['f'][i_f]['uf']
    
    def _sanitycheck( self, cond, cause, description ):
        if cond: return
        print( description )
        self.listinfo()
        assert False, "Sanity check failed after: %s!" % cause
    
    def sanitycheck( self, cause=None ):
        if cause is None: cause = "unspecified"
        for i_v,v in enumerate(self.mdata['v']):
            if self.is_dead(v['uv']): continue
            for i_e in v['lie']:
                e = self.mdata['e'][i_e]
                self._sanitycheck( self.is_alive(e['ue']), cause, "uv %s has dead ue %s" % (v['uv'],e['ue']) )
                self._sanitycheck( i_v in e['liv'], cause, "uv %s not in adj v lst of ue %s" % (v['uv'],e['ue']) )
            for i_f in v['lif']:
                f = self.mdata['f'][i_f]
                self._sanitycheck( self.is_alive(f['uf']), cause, "uv %s has dead uf %s" % (v['uv'],f['uf']) )
                self._sanitycheck( i_v in f['liv'], cause, "uv %s not in adj v lst of uf %s" % (v['uv'],f['uf']) )
        for i_e,e in enumerate(self.mdata['e']):
            if self.is_dead(e['ue']): continue
            luv = [ self.mdata['v'][i_v]['uv'] for i_v in e['liv'] ]
            # NOTE: (temp) allow for empty edges
            self._sanitycheck( len(e['liv']) in [0,2], cause, "ue %s has incorrect num of verts, %s" % (e['ue'],",".join(luv)) )
            for i_v in e['liv']:
                v = self.mdata['v'][i_v]
                self._sanitycheck( self.is_alive(v['uv']), cause, "ue %s has dead uv %s" % (e['ue'],v['uv']) )
                self._sanitycheck( i_e in v['lie'], cause, "ue %s not in adj e lst of uv %s" % (e['ue'],v['uv']) )
            for i_f in e['lif']:
                f = self.mdata['f'][i_f]
                self._sanitycheck( self.is_alive(f['uf']), cause, "ue %s has dead uf %s" % (e['ue'],f['uf']) )
                self._sanitycheck( i_e in f['lie'], cause, "ue %s not in adj e lst of uf %s" % (e['ue'],f['uf']) )
        for i_f,f in enumerate(self.mdata['f']):
            if self.is_dead(f['uf']): continue
            luv = [ self.mdata['v'][i_v]['uv'] for i_v in f['liv'] ]
            lue = [ self.mdata['e'][i_e]['ue'] for i_e in f['lie'] ]
            self._sanitycheck( len(f['liv']) == len(set(f['liv'])), cause, 'uf %s has duplicate verts, [%s]' % (f['uf'],",".join(luv)) )
            self._sanitycheck( len(f['lie']) == len(set(f['lie'])), cause, 'uf %s has duplicate edges, [%s]' % (f['uf'],",".join(lue)) )
            self._sanitycheck( len(f['liv']) == len(f['lie']), cause, "uf %s has diff num of verts and edges, [%s], [%s]" % (f['uf'],",".join(luv),",".join(lue)) )
            # NOTE: (temp) allow for empty faces
            self._sanitycheck( len(f['liv']) == 0 or len(f['liv']) >= 3, cause, "uf %s has incorrect num of verts and edges, [%s], [%s]" % (f['uf'],",".join(luv),",".join(lue)) )
            for i_v in f['liv']:
                v = self.mdata['v'][i_v]
                self._sanitycheck( self.is_alive(v['uv']), cause, "uf %s has dead uv %s" % (f['uf'],v['uv']) )
                self._sanitycheck( i_f in v['lif'], cause, "uf %s not in adj f lst of uv %s" % (f['uf'],v['uv']) )
            for i_e in f['lie']:
                e = self.mdata['e'][i_e]
                self._sanitycheck( self.is_alive(e['ue']), cause, "uf %s has dead ue %s" % (f['uf'],e['ue']) )
                self._sanitycheck( i_f in e['lif'], cause, "uf %s not in adj f lst of ue %s" % (f['uf'],e['ue']) )
            # also check that verts are of edges (edges form loop)
    
    def listinfo( self ):
        self.listinfo_verts()
        self.listinfo_edges()
        self.listinfo_faces()
    def listinfo_verts( self ):
        for v in self.mdata['v']:
            if not self.udata[v['uv']]['alive']: continue
            print( "v %s:\tlie = [%s],\tlif = [%s]" % (
                v['uv'],
                (",".join([self._get_u_e(i_e) for i_e in v['lie']])),
                (",".join([self._get_u_f(i_f) for i_f in v['lif']]))
                ) )
    def listinfo_edges( self ):
        for e in self.mdata['e']:
            if not self.udata[e['ue']]['alive']: continue
            print( "e %s:\tliv = [%s],\tlif = [%s]" % (
                e['ue'],
                (",".join([self._get_u_v(i_v) for i_v in e['liv']])),
                (",".join([self._get_u_f(i_f) for i_f in e['lif']]))
                ) )
    def listinfo_faces( self ):
        for f in self.mdata['f']:
            if not self.udata[f['uf']]['alive']: continue
            print( "f %s:\tliv = [%s],\tlie = [%s]" % (
                f['uf'],
                (",".join([self._get_u_v(i_v) for i_v in f['liv']])),
                (",".join([self._get_u_e(i_e) for i_e in f['lie']]))
                ) )
    
    def nodebox( self, faces=False ):
        print( '*' * 80 )
        print( 'graph =  ximport("graph")' )
        print( 'g = graph.create(iterations=500, distance=0.8)' )
        print( 'e_len,e_weight = 10.0,0.25' )
        print( 'f_len,f_weight = 2.0,1.0' )
        print( 'faces = True' )
        for v in self.mdata['v']:
            if self.is_dead( v['uv'] ): continue
            print( 'g.add_node( "%s" )' % v['uv'] )
        for e in self.mdata['e']:
            if self.is_dead( e['ue'] ): continue
            if len(e['liv']) != 2: continue
            uv0,uv1 = [ self.mdata['v'][i_v]['uv'] for i_v in e['liv'] ]
            print( 'g.add_edge( "%s", "%s", label="%s", length=e_len, weight=e_weight )' % (uv0,uv1,e['ue']) )
        print( 'if faces:' )
        for f in self.mdata['f']:
            if self.is_dead( f['uf'] ): continue
            if len(f['liv']) < 3: continue
            print( '    g.add_node( "%s", style="dark" )' % f['uf'] )
            for i_v in f['liv']:
                u_v = self.mdata['v'][i_v]['uv']
                print( '    g.add_edge( "%s", "%s", length=f_len, weight=f_weight )' % (f['uf'],u_v) )
        print( 'g.solve()' )
        print( 'g.draw()' )
        print( '*' * 80 )
    
    # creates new vert
    def _new_v( self, uv = None, up = None, co = None, lie = None, lif = None ):
        if uv is None: uv = "-1"
        if up is None: up = []
        if co is None: co = [0,0,0]
        if lie is None: lie = []
        if lif is None: lif = []
        i_vn = len(self.mdata['v'])
        if uv == "-1": uv = self._get_new_uid( 'v', up )
        self.mdata['v'] += [{ 'co': co, 'lie': [], 'lif': [], 'uv': uv }]
        self.mdata['livu'] += [uv]
        self.sanitycheck( "_new_v: before updating list" )
        self._upd_lst( 'v', i_vn, 'e', lie )
        self._upd_lst( 'v', i_vn, 'f', lif )
        self.sanitycheck( "_new_v: after updating list" )
        return i_vn
    
    # creates new edge
    def _new_e( self, ue = None, up = None, liv = None, lif = None ):
        if ue is None: ue = "-1"
        if up is None: up = []
        if liv is None: liv = []
        if lif is None: lif = []
        i_en = len(self.mdata['e'])
        if ue == "-1": ue = self._get_new_uid( 'e', up )
        self.mdata['e'] += [{ 'liv': [], 'lif': [], 'ue': ue }]
        self.mdata['lieu'] += [ue]
        self.sanitycheck( "_new_e: before updating list" )
        self._upd_lst( 'e', i_en, 'v', liv )
        self._upd_lst( 'e', i_en, 'f', lif )
        self.sanitycheck( "_new_e: after updating list" )
        return i_en
    
    
    # creates new face
    def _new_f( self, uf = None, up = None, liv = None, lie = None ):
        if uf is None: uf = "-1"
        if up is None: up = []
        if liv is None: liv = []
        if lie is None: lie = []
        i_fn = len(self.mdata['f'])
        if uf == "-1": uf = self._get_new_uid( 'f', up )
        self.mdata['f'] += [{ 'liv': [], 'lie': [], 'uf': uf }]
        self.mdata['lifu'] += [uf]
        self.sanitycheck( "_new_f: before updating list" )
        self._upd_lst( 'f', i_fn, 'v', liv )
        self._upd_lst( 'f', i_fn, 'e', lie )
        self.sanitycheck( "_new_f: after updating list" )
        return i_fn
    
    # replaces idx adj list
    def _upd_lst( self, tg, ig, tl, nl ):
        g = self.mdata[tg][ig]
        litg = "li%s" % tg
        litl = "li%s" % tl
        # remove ig from adjacent geometry's adj list
        for i_gl in g[litl]:
            if ig in self.mdata[tl][i_gl][litg]:
                self.mdata[tl][i_gl][litg].remove( ig )
        g[litl] = nl
        # add ig to adjacent geometry's adj list
        for i_gl in g[litl]:
            if ig not in self.mdata[tl][i_gl][litg]:
                self.mdata[tl][i_gl][litg].append( ig )
    
    # add idx to adjacency list
    def _add_lst( self, tg, ig, tl, i_n ):
        g = self.mdata[tg][ig]
        litg = "li%s" % tg
        litl = "li%s" % tl
        if i_n not in g[litl]:
            g[litl].append( i_n )
        if ig not in self.mdata[tl][i_n][litg]:
            self.mdata[tl][i_n][litg].append(ig)
    
    # rem idx from adjacency list
    def _rem_lst( self, tg, ig, tl, i_n ):
        g = self.mdata[tg][ig]
        litg = "li%s" % tg
        litl = "li%s" % tl
        if i_n in g[litl]:
            g[litl].remove( i_n )
        if ig in self.mdata[tl][i_n][litg]:
            self.mdata[tl][i_n][litg].remove(ig)
    
    ############################################################################
    # user-accessible operations
    # --------------------------------------------------------------------------
    # all ops should return a list of updates for maintaining correspondences
    # between different vers of mesh as follows:
    #   [ ug ]                : added ug to uid list
    #   [ "-", ug ]           : removed ug
    #   [ up, ug ]            : added ug with parent up
    #   [ [up0,up1,...], ug ] : added ug with parents up0,up1,...
    
    def execute( self, n_op, lp_op ):
        fn = getattr( self, n_op )
        return fn( *lp_op )
    
    # add new geometry to mesh
    def new_v( self, co=None ):
        i_v = self._new_v( co=co )
        u_v = self._get_u_v(i_v)
        
        res = [u_v]
        self.ops += [ ['new_v',[co],res] ]
        print( "new uv %s" % u_v )
        return res
    
    def new_e( self, luv=None ):
        if luv: liv = [ self.mdata['livu'].index(uv) for uv in luv ]
        else: liv = None
        i_e = self._new_e( liv=liv )
        u_e = self._get_u_e( i_e )
        
        res = [u_e]
        self.ops += [ ['new_e',[luv],[u_e]] ]
        print( "new ue %s" % u_e )
        return res
    
    def new_f( self, luv=None ):
        if luv: liv = [ self.mdata['livu'].index(uv) for uv in luv ]
        else: liv = None
        i_f = self._new_f( liv=liv )
        u_f = self._get_u_f( i_f )
        
        res = [u_f]
        self.ops += [ ['new_f',[luv],res] ]
        print( "new uf %s" % u_f )
        return res
    
    # delete vertex by marking as dead.  NOTE: removing from idx list reqs
    # rewriting all other idx lists (changes indices of items after removed item)
    def del_v( self, u_v ):
        i_v = self.mdata['livu'].index(u_v)
        v = self.mdata['v'][i_v]
        
        for i_e in list(v['lie']):
            self._rem_lst( 'e', i_e, 'v', i_v )
        for i_f in list(v['lif']):
            self._rem_lst( 'f', i_f, 'v', i_v )
        
        self.udata[u_v]['alive'] = False
        
        res = ['-',u_v]
        self.ops += [ ['del_v',[u_v],res] ]
        print( "del uv %s" % u_v )
        return res
        
    # delete edge by marking as dead.  NOTE: removing from idx list reqs
    # rewriting all other idx lists (changes indices of items after removed item)
    def del_e( self, u_e ):
        i_e = self.mdata['lieu'].index(u_e)
        e = self.mdata['e'][i_e]
        
        for i_v in list(e['liv']):
            self._rem_lst( 'v', i_v, 'e', i_e )
        for i_f in list(e['lif']):
            self._rem_lst( 'f', i_f, 'e', i_e )
        
        self.udata[u_e]['alive'] = False
        
        res = ['-',u_e]
        self.ops += [ ['del_e',[u_e],res] ]
        return res
    
    # delete face by marking as dead.  NOTE: removing from idx list reqs
    # rewriting all other idx lists (changes indices of items after removed item)
    def del_f( self, u_f ):
        res = []
        self.ops += [ ['del_f',[u_f],res] ]
        
        i_f = self.mdata['lifu'].index(u_f)
        f = self.mdata['f'][i_f]
        
        for i_v in list(f['liv']):
            self._rem_lst( 'v', i_v, 'f', i_f )
        for i_e in list(f['lie']):
            self._rem_lst( 'e', i_e, 'f', i_f )
        
        self.udata[u_f]['alive'] = False
        res += ['-',u_f]
        
        return res
    
    # collapses edge to a vert
    # TODO: add param to specify the vert to collapse to
    def collapse( self, u_e ):
        res = []
        self.ops += [ ['collapse',[u_e],res] ]
        
        i_e = self.mdata['lieu'].index(u_e)
        e = self.mdata['e'][i_e]
        
        # collapse i_v1 to i_v0
        i_v0,i_v1 = e['liv']
        v1 = self.mdata['v'][i_v1]
        
        # update all edges adj to i_v1
        # may leave two edges with same verts (tri->edge); cleaned up in next loop
        for i_ec in list(v1['lie']):
            ec = self.mdata['e'][i_ec]
            if i_ec == i_e: continue
            self._upd_lst( 'e', i_ec, 'v', [ i_v0 if (i_vc == i_v1) else i_vc for i_vc in ec['liv'] ] )
        
        # update all faces adj to i_v1
        # may turn tri->edge
        for i_f in list(v1['lif']):
            f = self.mdata['f'][i_f]
            if i_e not in f['lie']:
                self._upd_lst( 'f', i_f, 'v', [ i_v0 if (i_vc == i_v1) else i_vc for i_vc in f['liv'] ] )
            self._rem_lst( 'f', i_f, 'e', i_e )
            
            if len(f['lie']) == 2:
                i_e0,i_e1 = f['lie']
                e1 = self.mdata['e'][i_e1]
                for i_fc in list(e1['lif']):
                    if i_fc == i_f: continue
                    fc = self.mdata['f'][i_fc]
                    self._upd_lst( 'f', i_fc, 'e', [ i_e0 if (i_ec == i_e1) else i_ec for i_ec in fc['liv'] ] )
                
                u_e1,u_f = e1['ue'],f['uf']
                self.del_e( u_e1 )
                self.del_f( f['uf'] )
                #res += [ ['-',u_e1],['-',u_f] ]
        
        u_v1,u_e = v1['uv'],e['ue']
        self.del_v( v1['uv'] )
        self.del_e( e['ue'] )
        #res += [ ['-',u_v1],['-',u_e] ]
        
        self.sanitycheck( "collapsed ue %s" % e['ue'] )
        return res
    
    # extrudes vert to an edge
    #   u_v0: uid of vert to extrude
    #   lu_g1: list of uids that goes with new vert
    #   lu_gb: list of uids that goes with both verts (duplicated)
    #   lu_gi: list of uids that has vert extruded to edge (ex: tri->quad)
    # this fn do the following:
    #   turn pt into edge, edge into tri, or tri into quad
    #   cause surface to have volume (solidify).  reqs multiple calls.
    #   reverse a collapse
    def extrude( self, u_v0, lu_g1, lu_gb, lu_gi ):
        res = []
        self.ops += [ ['extrude',[u_v0, list(lu_g1), list(lu_gb), list(lu_gi)],res] ]
        
        i_v0 = self.mdata['livu'].index(u_v0)
        v0 = self.mdata['v'][i_v0]
        
        # create new geometry
        i_v1 = self._new_v( up=u_v0 )
        v1 = self.mdata['v'][i_v1]
        u_v1 = v1['uv']
        res += [[u_v0,u_v1]]
        self.sanitycheck("extrude: add vert %s" % u_v1)
        
        i_e01 = self._new_e( up=u_v0, liv=[i_v0,i_v1] )
        e01 = self.mdata['e'][i_e01]
        u_e01 = e01['ue']
        res += [[u_v0,u_e01]]
        self.sanitycheck("extrude: add edge %s-%s = %s" % (u_v0,u_v1,u_e01))
        
        lu_g1,lu_gb,lu_gi = set(lu_g1),set(lu_gb),set(lu_gi)
        
        # if a face is in list, make sure its edges are, too
        for i_f in v0['lif']:
            f = self.mdata['f'][i_f]
            if f['uf'] in lu_g1:
                for i_e in f['lie']:
                    lu_g1.add( self.mdata['e'][i_e]['ue'] )
            if f['uf'] in lu_gb:
                for i_e in f['lie']:
                    lu_gb.add( self.mdata['e'][i_e]['ue'] )
            if f['uf'] in lu_gi:
                for i_e in f['lie']:
                    lu_gi.add( self.mdata['e'][i_e]['ue'] )
        
        # should assert that there are no faces in the intersections of the lists!
        
        lt_gb = [ self.udata[u_g]['type'] for u_g in lu_gb ]
        lu_eb = [ u_g for i,u_g in enumerate(lu_gb) if lt_gb[i] == 'e' ]
        li_eb = [ self.mdata['lieu'].index(u_g) for u_g in lu_eb ]
        lu_fb = [ u_g for i,u_g in enumerate(lu_gb) if lt_gb[i] == 'f' ]
        li_fb = [ self.mdata['lifu'].index(u_g) for u_g in lu_fb ]
        
        lt_g1 = [ self.udata[u_g]['type'] for u_g in lu_g1 ]
        lu_e1 = [ u_g for i,u_g in enumerate(lu_g1) if lt_g1[i] == 'e' ]
        li_e1 = [ self.mdata['lieu'].index(u_g) for u_g in lu_e1 ]
        lu_f1 = [ u_g for i,u_g in enumerate(lu_g1) if lt_g1[i] == 'f' ]
        li_f1 = [ self.mdata['lifu'].index(u_g) for u_g in lu_f1 ]
        
        lt_gi = [ self.udata[u_g]['type'] for u_g in lu_gi ]
        lu_ei = [ u_g for i,u_g in enumerate(lu_gi) if lt_gi[i] == 'e' ]
        li_ei = [ self.mdata['lieu'].index(u_g) for u_g in lu_ei ]
        lu_fi = [ u_g for i,u_g in enumerate(lu_gi) if lt_gi[i] == 'f' ]
        li_fi = [ self.mdata['lifu'].index(u_g) for u_g in lu_fi ]
        
        # a face in gi had a vert extruded
        for i_f in list(v0['lif']):
            if i_f in li_fi:
                self._add_lst( 'f', i_f, 'v', i_v1 )
                self._add_lst( 'f', i_f, 'e', i_e01 )
                self.sanitycheck( "extrude: added vert,edge for li_fi" )
        
        # look at all edges adj to vertex
        for i_e in list(v0['lie']):
            e = self.mdata['e'][i_e]
            u_e = e['ue']
            
            if i_e in li_eb:
                # edge is to be extruded
                # find other vert, create new edge, create face between
                # update any faces of edge in gb, g1 to use this new edge
                
                i_v2 = e['liv'][1 - e['liv'].index(i_v0)]
                i_e12 = self._new_e( up=u_e, liv=[i_v1,i_v2] )
                self.sanitycheck( "extrude: add edge for li_eb" )
                i_f012 = self._new_f( up=u_e, liv=[i_v0,i_v1,i_v2], lie=[i_e,i_e12,i_e01] )
                self.sanitycheck( "extrude: add face for li_eb" )
                
                res += [ [u_e,self._get_u_e(i_e12)], [u_e,self._get_u_f(i_f012)] ]
                
                for i_f in list(e['lif']):
                    f = self.mdata['f'][i_f]
                    u_f = f['uf']
                    
                    if i_f in li_fb:
                        li_v = list(set(f['liv']) - set(i_v0)) + [i_v1]
                        li_e = list(set(f['lie']) - set(i_e)) + [i_e12]
                        i_fn = self._new_f( up=u_f, liv=li_v, lie=li_e )
                        res += [ [u_f, self._get_u_f(i_fn)] ]
                    
                    elif i_f in li_f1:
                        self._upd_lst( 'f', i_f, 'v', [ i_v1 if ( i_vc == i_v0 ) else i_vc for i_vc in f['liv'] ] )
                        self._upd_lst( 'f', i_f, 'e', [ i_e12 if ( i_ec == i_e ) else i_ec for i_ec in f['lie'] ] )
                
            elif i_e in li_e1:
                # edge is to change from i_v0 to i_v1
                # update edge to use new vert
                # all adjacent faces to edge needs to be updated to use new vert
                
                self._upd_lst( 'e', i_e, 'v', [ i_v1 if ( i_vc == i_v0 ) else i_vc for i_vc in e['liv'] ] )
                for i_f in list(e['lif']):
                    if i_f in li_fi: continue
                    f = self.mdata['f'][i_f]
                    u_f = f['uf']
                    self._upd_lst( 'f', i_f, 'v', [ i_v1 if ( i_vc == i_v0 ) else i_vc for i_vc in f['liv'] ] )
        
        self.sanitycheck( "extrude u_v0 %s" % u_v0 )
        return res
    
    # finds and extrudes bordering edgeloop (blender-style extrude)
    # assumes simple selection (no holes)
    def extrude_blender_macro( self, lu_g1 ):
        res = []
        self.ops += [ ['extrude_blender_macro', [list(lu_g1)], res] ]
        
        lu_g1 = [ u_g for u_g in lu_g1 if self.udata[u_g]['type'] == 'f' ]
        lu_el = []
        for u_f in lu_g1:
            f = self.mdata['f'][ self._get_i(u_f) ]
            llif_e = [ self.mdata['e'][i_e]['lif'] for i_e in f['lie'] ]
            lluf_e = [ [ self.mdata['f'][i_fe]['uf'] for i_fe in lif_e ] for lif_e in llif_e ]
            
            li_e = [ i_e for i_e,luf_e in zip(f['lie'],lluf_e) if sum([ uf_e in lu_g1 for uf_e in luf_e ]) == 1 ]
            lu_e = [ self.mdata['e'][i_e]['ue'] for i_e in f['lie'] ]
            lu_el += [ u_e for u_e in lu_e ]
        
        res = self.extrude_edgeloop_macro( lu_el, lu_g1 )
        
        self.ops += [ ['done_macro'] ]
        self.sanitycheck( 'extrude_blender_macro' )
        return res
    
    # assumes edgeloop does not self-intersect
    def extrude_edgeloop_macro( self, lu_el, lu_g1 ):
        # reduce lu_el to only edges
        lu_el = [ u_el for u_el in lu_el if self.udata[u_el]['type'] == 'e' ]
        
        res = []
        self.ops += [ ['extrude_edgeloop_macro', [list(lu_el),list(lu_g1)], res] ]
        print( str(self.ops[-1]) )
        
        
        li_elu = [ self._get_i(u_el) for u_el in lu_el ]
        
        i_e0 = li_elu[0]
        e0 = self.mdata['e'][i_e0]
        li_vl = [ e0['liv'][0], e0['liv'][1] ]
        i_ec,i_v0,i_vc = i_e0,li_vl[0],li_vl[1]
        li_els = [ i_e0 ]
        while i_v0 != i_vc:
            vc = self.mdata['v'][i_vc]
            li_ec = [ i_e for i_e in vc['lie'] if i_e in li_elu and i_e not in li_els ]
            if len(li_ec) != 1:
                print( "Specified edgeloop is not an edgeloop!" )
            i_ec = li_ec[0]
            li_els += [ i_ec ]
            li_vl += [ i_v for i_v in self.mdata['e'][i_ec]['liv'] if i_v != i_vc ]
            i_vc = li_vl[-1]
        
        lu_els = [ self.mdata['e'][i_e]['ue'] for i_e in li_els ]
        
        print( "li_vl = [%s]" % ",".join([str(i_vl) for i_vl in li_vl]) )
        
        lu_gi = []
        for i,(i_vl,i_el) in enumerate(zip(li_vl,li_els)):
            vl = self.mdata['v'][i_vl]
            el = self.mdata['e'][i_el]
            if i == 0:
                r = self.extrude( vl['uv'], lu_g1, lu_els, [] )
                lu_els.pop(-1)
            else:
                r = self.extrude( vl['uv'], lu_g1, lu_els, lu_gi )
            # add new faces to lu_gi
            lu_gi += [ un for up,un in r if self.get_type(un) == 'f' ]
            lu_g1 += [ un for up,un in r if self.get_type(un) == 'e' ]
            if lu_els: lu_els.pop(0)
            elp = el
            res += r
        
        self.ops += [ ['done_macro'] ]
        self.sanitycheck( "extrude_edgeloop_macro" )
        return res
    
    # v-e-v into v-e-v-e-v
    # similar to extrude, but parent of new v is old e
    def split_edge( self, u_e02 ):
        res = []
        self.ops += [ ['split_edge', [u_e02], res] ]
        
        i_e02 = self.mdata['lieu'].index(u_e02)
        e02 = self.mdata['e'][i_e02]
        i_v0,i_v2 = e02['liv']
        v0,v1 = [ self.mdata['v'][i_v] for i_v in [i_v0,i_v2] ]
        u_v0,u_v2 = [ v['uv'] for v in [v0,v1] ]
        
        # create new geometry
        i_v1 = self._new_v( up=u_e02 )
        v1 = self.mdata['v'][i_v1]
        u_v1 = v1['uv']
        res += [[u_e02,u_v1]]
        self.sanitycheck("split_edge: add vert %s" % u_v1)
        
        i_e01 = self._new_e( up=u_e02, liv=[i_v0,i_v1] )
        e01 = self.mdata['e'][i_e01]
        u_e01 = e01['ue']
        res += [[u_e02,u_e01]]
        self.sanitycheck("split_edge: add edge %s-%s = %s" % (u_v0,u_v1,u_e01))
        
        i_e12 = self._new_e( up=u_e02, liv=[i_v1,i_v2] )
        e12 = self.mdata['e'][i_e12]
        u_e12 = e12['ue']
        res += [[u_e02,u_e12]]
        self.sanitycheck("split_edge: add edge %s-%s = %s" % (u_v1,u_v2,u_e12))
        
        # now, replace e02 with e01-v1-e12
        for i_f in list(e02['lif']):
            self._add_lst( 'f', i_f, 'v', i_v1 )
            self._add_lst( 'f', i_f, 'e', i_e01 )
            self._add_lst( 'f', i_f, 'e', i_e12 )
            self._rem_lst( 'f', i_f, 'e', i_e02 )
        
        self.sanitycheck("split_edge: added new geometry to faces")
        
        self.del_e( u_e02 )
        #res += [["-",u_e02]]
        
        self.sanitycheck("split_edge: del ue %s" % u_e02)
        return res
    
    # splits face into two by creating edge between u_v0 and u_v1
    def split_face( self, u_f, u_v0, u_v1 ):
        pass
    
    # joins two adj edges (reverses split_edge)
    # v-e-v-e-v into v-e-v
    def join_edges( self, u_e0, u_e1 ):
        pass
    
    # joins two adj faces (reverses split_face)
    def join_faces( self, u_f0, u_f1 ):
        pass
    
    # same as extrude, but does not create edge between u_v0 and u_v1
    #   u_v0: the vert to duplicate
    #   lu_g1: the geometry that goes with u_v1
    #   lu_gb: the geometry that goes with both (duplicated)
    # this fn can do the following:
    #   duplicate geometry (reqs multiple calls)
    #   reverse a merge op
    def rip( self, u_v0, lu_g1, lu_gb ):
        pass
    
    # merges two vertices that are not connected by an edge (edge: collapse)
    def merge( self, u_v0, u_v1 ):
        pass

#!/bin/bash

ver0=meshgit_$1.blend_ver0.ply
vera=meshgit_$1.blend_vera.ply
verb=meshgit_$1.blend_verb.ply
merg=merged_$1.ply

python meshgit2.py mergeview $ver0 $vera $verb $merg
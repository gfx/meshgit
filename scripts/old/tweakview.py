#!/usr/bin/env python

import os, subprocess, shutil, random, itertools, glob


# ./meshgit2.py diffsaved shaolin_0.ply shaolin_a.ply shaolin_0a.data



lfiles = [
    ( 'hand', '0', 'a' ),
    ( 'hand', '0', 'b' ),
    ( 'shaolin', '0', 'a' ),
    ( 'shaolin', '0', 'b' ),
    ( 'durano', '00', '01' ),
    ( 'durano', '01', '02' ),
    ( 'durano', '02', '03' ),
    ( 'durano', '03', '04' ),
    ]

#lopts = list( (a,b,c,d,e,f)
#    for a in [ 1.0, 2.5, 5.0, 10.0, 20.0 ]
#    for b in [ 1.0, 2.5, 5.0, 10.0, 20.0 ]
#    for c in [ 0.0, 1.0 ]
#    for d in [ 0.0, 1.0 ]
#    for e in [ 0.5, 1.0, 2.5, 5.0 ]
#    for f in [ 0.5, 1.0, 2.5, 5.0 ]
#    )

b = 'hand'
e0 = '0'
e1 = 'a'

fn0 = '%s_%s.ply' % (b,e0)
fn1 = '%s_%s.ply' % (b,e1)
fnb = '%s_%s%s_' % (b,e0,e1)
fnstate = 'state_%s.dat' % b

dirlist = glob.glob('%s_*.data' % fnb )
#lfnappend = [ fn[len(fnb)+1:-5] for fn in dirlist ]
lfnappend = [
    '10_20_1_0_1_1_0_0_0_0',
    '1_2_1_0_1_1_0_0_0_0',
    '2_10_1_1_1_1_2_2_2_2',
    '5_20_1_0_1_1_0_1_0_2',
    '5_20_1_0_1_1_0_2_0_5'
    ]

for fnappend in lfnappend:
    #fnappend = '_%s' % '_'.join( str(int(opt)) for opt in opts )
    fn01 = '%s_%s.data' % (fnb,fnappend)
    
    
    print( '*' * 80 )
    print( '*' * 80 )
    print( fnappend )
    subprocess.Popen( ['./meshgit2.py','diffsaved',fn0,fn1,fn01] ).communicate()

# v_adddel,f_adddel,vdist,vnorm,fdist,fnorm,vunm,vmis,funm,fmis
#opts = [ 5.0, 20.0, 2.0, 0.0, 2.0, 2.0, 4.5, 9.0, 4.5, 9.0 ]
#random.shuffle(lopts)

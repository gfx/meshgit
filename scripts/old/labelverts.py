#!/usr/bin/env python -B
# -*- coding: utf-8 -*-

"""
gives vertices a unique label

1. loads steps
2. filtering out non-edits and non-selections
3. cluster (filter) out undone work
4. loops over each step:
4.1. load scene
4.2. builds object correspondences with prev scene
4.3. builds vertex correspondences with prev scene
5. saves object, vertex ids

NOTE: must update script with any new commands that are performed

"""

import os, glob, sys, subprocess, time, select, json, re, array, math, argparse
import struct
from operator import itemgetter
import config, scenefuncs, opfuncs, miscfuncs


args = { 'filein': 'steps.txt' }


# load binary data
def loadbinary(filename):
    with open(filename, 'rb') as f:
        magic = f.read(1); assert magic == 'B'
        type = f.read(1)
        size, _, nc = struct.unpack('iii', f.read(12))
        a = array.array(type, f.read()).tolist()
        assert len(a) == size*nc
        return a

class UID(object):
    def __init__(self):
        self.id = 0
    def n(self):
        self.id += 1
        return self.id-1
    def t(i):
        return "%04d" % i



################################################################################
### read steps.txt
### filter out ignorable operations
### translate op names to user-readable

print( "Loading steps, filtering out non-edits and non-selections..." )

passed = []
total = 0

print( args['filein'] )

for step in open( args['filein'], "r" ):
    result = opfuncs.translateop( step )
    if not result: quit()
    
    (oppass,op,opparam,stepfn) = result
    
    if oppass:
        passed.append( [op, opparam, stepfn, total] )
    
    total += 1



################################################################################
### filter undone work

print( "Clustering and filtering undone work..." )

ops      = [ c[0] for c in passed ]
opparams = [ c[1] for c in passed ]
stepfns  = [ c[2] for c in passed ]
tvscene  = [ miscfuncs.loadjsonfromstepfn(fn) for fn in stepfns ]
isteps   = [ int(c[3]) for c in passed ]

#(ops,isteps,tvscene) = opfuncs.filterundonework( ops, isteps, tvscene )
passedinds = opfuncs.filterundonework_inds( ops, isteps, tvscene )
step_c   = len( passedinds )
ops      = [ ops[i] for i in passedinds ]
isteps   = [ isteps[i] for i in passedinds ]
tvscene  = [ tvscene[i] for i in passedinds ]
stepfns  = [ stepfns[i] for i in passedinds ]
opparams = [ opparams[i] for i in passedinds ]





################################################################################
### 

print( "Iterating through steps..." )

accum_ids = []      # accumulated ids, for each step: [ objids, vertids, triids, quadids ]

scene_all_with_ids = {}

# last step info
lmeta = {}          # meta data, such as current mode, active obj, etc.
lmodels = []        # set of objects
lobj_names = []     # shortcut to names of objects
lobj_count = 0      # shortcut to count of objects
lobjids = []        # set of object ids
lvertids = []       # set of vertex ids
ltriids = []        # set of tri ids
lquadids = []       # set of quad ids

ouid = UID()        # next obj unique id to use
vuid = UID()        # next vert unique id to use
fuid = UID()        # next face unique id to use

faceids = []

bins = ["selected","sel_lines","sel_tris","sel_quads","lines","triangles","quads"]
# NOTE: to determine connectivity, use lines


for op,scene,istep,stepind in zip( ops, tvscene, isteps, range(step_c) ):
    scene["meta_data"]["op"] = op
    
    print( "istep = %d, op = \"%s\", mode = \"%s\"" % (istep,op,scene["meta_data"]["mode"]) )
    
    # reset current step info
    cmeta = scene["meta_data"]
    cmodels = []
    cobj_names = []
    cobj_count = 0
    cobjids = []
    cvertids = []
    ctriids = []
    cquadids = []
    cfaceids = []
    
    #####################################################
    # load and generate all useful data for current scene
    
    cmodels = [None] * len(scene["surfaces"])
    for surface in scene["surfaces"]:
        imodel = int(surface["name"][:3])
        
        shape = [s for s in scene["shapes"] if s["name"] == surface["shape_ref"]][0]
        sname = shape["name"]
        model = { "name": sname }
        
        # grab selection and index data
        for b in bins:
            bf = "%s_filename" % b
            if bf in shape:
                bfn = shape[bf]
                model[bf] = bfn
                model[b] = loadbinary(bfn)
            else:
                model[b] = shape[b]
        
        # count!
        model["vert_count"] = len( model["selected"] )
        model["line_count"] = int(len( model["lines"] ) / 2)
        model["tri_count"] = int(len( model["triangles"] ) / 3)
        model["quad_count"] = int(len( model["quads"] ) / 4)
        model["face_count"] = model["tri_count"] + model["quad_count"]
        model["sel_vert_inds"] = sorted( [ ind for (ind,sel) in enumerate( model["selected"] ) if sel ] )
        model["sel_tri_inds"] = sorted( [ ind for (ind,sel) in enumerate( model["sel_tris"] ) if sel ] )
        model["sel_quad_inds"] = sorted( [ ind for (ind,sel) in enumerate( model["sel_quads"] ) if sel ] )
        
        cmodels[imodel] = model
    
    assert len( cmodels ) == len( scene["shapes"] ), "accounting error: object count and shape count don't match"
    
    cobj_names = [ model["name"] for model in cmodels ]
    cobj_count = len( cmodels )
    
    
    
    ###################
    # assign object ids
    
    if lobj_count == 0:
        # prev step had no objects, so just assign new oids to all current objs
        
        cobjids = [ ouid.n() for i in range( cobj_count ) ]
        
    elif op in ["topo.delete.object"]:
        # remove obj ids of selected objects from prev step
        # NOTE: assuming that order of shapes is not altered!!!!!!
        
        robjind = [ lobj_names.index( n ) for n in lmeta["selected_objects"] ]
        cobjids = [ i for (ind,i) in enumerate(lobjids) if not ind in robjind ]
        
    else:
        # new objs get new ouids, copy obj ids from prev step for rest
        # in blender, new objects are placed at beginning of list
        # NOTE: assuming that order of shapes is not altered!!!!!!
        
        assert cobj_count >= lobj_count, "op error: reduced object count not handled properly, op = \"%s\", cobj_count = %d, lobj_count = %d" % (op,cobj_count,lobj_count)
        
        cobjids = [ ouid.n() for i in range( cobj_count - lobj_count ) ] + lobjids
    
    assert len(cobjids) == cobj_count, "accounting error: object id count and obj count don't match, op = \"%s\"" % op
    
    
    
    ##########################
    # assign vert and face ids
    
    # start by copying ids over from prev state.  if the obj didn't exist in
    # prev state, give all verts and faces uids
    
    for (cobjid,model) in zip(cobjids,cmodels):
        if cobjid in lobjids:
            pind = lobjids.index( cobjid )
            cvertids += [ lvertids[pind] ]
            #ctriids += [ lfaceids[0][pind] ]
            #cquadids += [ lfaceids[1][pind] ]
        else:
            print( "creating new vert, tri, quad ids" )
            cvertids += [ [ vuid.n() for i in range( model["vert_count"] ) ] ]
            #ctriids += [ [ fuid.n() for i in range( model["tri_count"] ) ] ]
            #cquadids += [ [ fuid.n() for i in range( model["quad_count"] ) ] ]
    
    #print( cvertids )
    #print( ctriids )
    #print( cquadids )
    
    # vert / face ids change only when an object is being edited
    if cmeta["mode"] == 'EDIT_MESH' and "mode" in lmeta and lmeta["mode"] == 'EDIT_MESH':
        
        # info of edit objec
        eind = cobj_names.index( "%s_s_%06d" % (cmeta["edit_object"],istep) )
        eid = cobjids[eind]
        eobj = cmodels[eind]
        
        # info of previous object corresponding to edited obj
        pind = lobjids.index( eid )
        pobj = lmodels[pind]
        pvids = lvertids[pind]
        ptids = lfaceids[0][pind]
        pqids = lfaceids[1][pind]
        psel = pobj["selected"]
        psel_lines = pobj["sel_lines"]
        psel_tris = pobj["sel_tris"]
        psel_quads = pobj["sel_quads"]
        pselinds = pobj["sel_vert_inds"]
        plines = pobj["lines"]
        ptris = pobj["triangles"]
        pquads = pobj["quads"]
        plinecount = pobj["line_count"]
        ptricount = pobj["tri_count"]
        pquadcount = pobj["quad_count"]
        
        # find all "border" vertices.  these are the verts that are selected
        # but are ends of non-selected lines, tris, or quads
        pbvlines = miscfuncs.flatten([ plines[eind*2:eind*2+2] for eind in range(plinecount) if not psel_lines[eind] ])
        pbvtris = miscfuncs.flatten([ ptris[find*3:find*3+3] for find in range(ptricount) if not psel_tris[find] ])
        pbvquads = miscfuncs.flatten([ pquads[find*4:find*4+4] for find in range(pquadcount) if not psel_quads[find] ])
        pborderverts = list(set(pbvlines + pbvtris + pbvquads)  & set(pselinds))
        
        print( "prev counts: verts = %d, sel verts = %d, border verts = %d" % (len(pvids),len(pselinds),len(pborderverts)) )
        print( "curr counts: verts = %d, sel verts = %d" % (len(eobj["selected"]),len(eobj["sel_vert_inds"])) )
        
        idschanged = False
        vertids = []
        #triids = []
        #quadids = []
        
        if op.startswith( "select" ) or op.startswith( "transform" ) or op.startswith( "modifier" ):
            pass
        elif op in ["topo.delete.face","topo.delete.edge"]:
            # a selected vertex is deleted if it's not a border vert.
            
            vertids = [ i for (ind,i) in enumerate(pvids) if not psel[i] or ind in pborderverts ]
            #triids = [] # TODO
            #quadids = [] # TODO
            idschanged = True
            
        elif op in ["topo.delete.vert","topo.delete.selected"]:
            # selected verts are deleted.  faces using a selected vert are deleted
            
            vertids = [ i for (ind,i) in enumerate(pvids) if not psel[i] ]
            #triids = [] # TODO
            #quadids = [] # TODO
            idschanged = True
            
        elif op in ["topo.delete.all"]:
            # all verts, edges, and faces are deleted
            
            idschanged = True
        
        elif op in ["topo.duplicate", "topo.duplicate_object", "topo.add.circle", "topo.add.cone", "topo.add.cube", "topo.add.cylinder", "topo.add.grid", "topo.add.sphere.ico", "topo.add.sphere.uv", "topo.add.monkey", "topo.add.plane", "topo.add.torus" ]:
            # currently selected verts,faces are new.
            # append new uids (always at end of list)
            
            vertids = pvids + [ vuid.n() for i in range( len(eobj["sel_vert_inds"]) ) ]
            #triids = ptids + [ fuid.n() for i in range( len(eobj["sel_tri_inds"]) ) ]
            #quadids = pqids + [ fuid.n() for i in range( len(eobj["sel_quad_inds"]) ) ]
            idschanged = True
            
        elif op in ["topo.extrude"]:
            # verts along border are duplicated, unless entire connected
            # component is selected, where all verts are duplicated
            
            # NOTE: assuming, for now, that connected component is NOT entirely
            # selected
            
            vertids = pvids + [ vuid.n() for i in range( len(pborderverts) ) ]
            #triids = [] # TODO
            #quadids = [] # TODO
            idschanged = True
        
        elif op in ["topo.merge"]:
            # all select verts but the smallest (first in sorted list) is deleted
            
            vertids = [ i for (ind,i) in enumerate(pvids) if not ind in pselinds[1:] ]
            #triids = [] # TODO
            #quadids = [] # TODO
            idschanged = True
        
        elif op in ["topo.subdivide"]:
            #TODO
            
            vertids = pvids + [ vuid.n() for i in range( len(eobj["sel_vert_inds"]) - len(pselinds) ) ]
            #triids = [] # TODO
            #quadids = [] # TODO
            idschanged = True
            
        elif op in ["topo.loopcut", "topo.knife_cut"]:
            #TODO
            
            vertids = pvids + [ vuid.n() for i in range( len(eobj["sel_vert_inds"]) ) ]
            #triids = [] # TODO
            #quadids = [] # TODO
            idschanged = True
        
        else:
            print( "skipping unknown command: %s  <--------- *************************" % op )
        
        if idschanged:
            # replace set copies with new id sets
            cvertids[eid] = vertids
            #ctriids[eid] = triids
            #cquadids[eid] = quadids
    
    
    # for now, consider the vert ids as a unique identifier
    # this is not particularly good, but it may do for now
    ctriids = [None] * len(cmodels)
    cquadids = [None] * len(cmodels)
    for (imodel,model) in enumerate(cmodels):
        etris = model["triangles"]
        equads = model["quads"]
        
        ctriids[imodel] = [None] * model["tri_count"]
        cquadids[imodel] = [None] * model["quad_count"]
        
        for i in range(model["tri_count"]):
            vs = tuple(sorted([ cvertids[imodel][ind] for ind in etris[i*3:i*3+3] ]))
            if not vs in faceids:
                faceids += [vs]
            ctriids[imodel][i] = faceids.index(vs)
        for i in range(model["quad_count"]):
            vs = tuple(sorted([ cvertids[imodel][ind] for ind in equads[i*4:i*4+4] ]))
            if not vs in faceids:
                faceids += [vs]
            cquadids[imodel][i] = faceids.index(vs)
    
    # make sure everything is accounted for
    for (imodel,model) in enumerate(cmodels):
        assert len(cvertids[imodel]) == model["vert_count"], "accounting error: op = \"%s\", model imodel = %d, id count = %d, vert count = %d" % (op,imodel,len(cvertids[imodel]),model["vert_count"])
        assert len(ctriids[imodel]) == model["tri_count"], "accounting error: op = \"%s\", model imodel = %d, id count = %d, tri count = %d" % (op,imodel,len(ctriids[imodel]),model["tri_count"])
        assert len(cquadids[imodel]) == model["quad_count"], "accounting error: op = \"%s\", model imodel = %d, id count = %d, quad count = %d" % (op,imodel,len(ctriids[imodel]),model["quad_count"])
    
    
    ########################
    
    lmodels = cmodels
    lmeta = cmeta
    lobj_names = cobj_names
    lobj_count = cobj_count
    lobjids = cobjids
    lvertids = cvertids
    lfaceids = ( ctriids, cquadids )
    
    accum_ids += [ [ cobjids, cvertids, ctriids, cquadids ] ]
    
    for (imodel,model) in enumerate(cmodels):
        i = cobj_names.index( model["name"] )
        upd_shape = [ shape for shape in scene["shapes"] if shape["name"] == model["name"] ][0]
        upd_shape["obj_id"] = cobjids[imodel]
        upd_shape["vert_ids"] = cvertids[imodel]
        upd_shape["tri_ids"] = ctriids[imodel]
        upd_shape["quad_ids"] = cquadids[imodel]
    
    stepfn = stepfns[stepind]
    miscfuncs.writejson( stepfn + "_ids.json", scene )
    
    scenefuncs.append_name_ref( scene, "_%06d" % stepind )
    scenefuncs.add_to_scene( scene_all_with_ids, scene )
    

################################################################################
### write data out to steps_processed.txt

#for istep in range(step_c):
#    oids,vids,tids,qids = accum_ids[istep]
#    print( ops[istep], oids, vids, tids, qids )




miscfuncs.json_to_file( "steps_all_ids.json", scene_all_with_ids )



print( "done!" )





# -*- coding: utf-8 -*-

import os, glob, sys, subprocess, time, json, re
from array import array
from config import *
import miscfuncs

################################################################################
### scene functions

# returns true if two scenes are superficially the same
def samescene( scene0, scene1 ):
    if 'shapes' in scene0 and not 'shapes' in scene1:
        return False
    if not 'shapes' in scene0 and 'shapes' in scene1:
        return False
    
    if 'shapes' in scene0 and 'shapes' in scene1:
        shapes0 = scene0['shapes']
        shapes1 = scene1['shapes']
        if len(shapes0) != len(shapes1):
            return False
        for i in range( 0, len(shapes0) ):
            if not miscfuncs.compare_files( shapes0[i]["pos_filename"], shapes1[i]["pos_filename"] ):
                return False
            
            se0 = "selected_filename" in shapes0[i]
            se1 = "selected_filename" in shapes1[i]
            if se0 and se1:
                if not miscfuncs.compare_files( shapes0[i]["selected_filename"], shapes1[i]["selected_filename"] ):
                    return False
            elif se0 or se1:
                return False
            
            # cannot compare lines, triangles, and quads in same way, as the order might of indices might be different!
            # instead, we'll compare file sizes (good enough)
            if os.path.getsize( shapes0[i]["triangles_filename"] ) != os.path.getsize( shapes1[i]["triangles_filename"] ):
                return False
            if os.path.getsize( shapes0[i]["lines_filename"] ) != os.path.getsize( shapes1[i]["lines_filename"] ):
                return False
            if os.path.getsize( shapes0[i]["quads_filename"] ) != os.path.getsize( shapes1[i]["quads_filename"] ):
                return False
    
    # passed all superficially false checks
    return True

# gets specific shape data from scene
def get_shapedata_only( scene, info ):
    if not "shapes" in scene:
        return {}
    data = {}
    for shape in scene["shapes"]:
        if info in shape:
            data[shape["name"]] = shape[info]
    return data

def get_selectedfn_only( scene ):
    return get_shapedata_only( scene, "selected_filename" )

def get_posfn_only( scene ):
    return get_shapedata_only( scene, "pos_filename" )

# accumulates the selected filenames
def accum_selected_filenames( summedfn, selectedfn ):
    for name,filename in selectedfn.items():
        name = re.sub( '_[0-9]{6}$', '', name )
        if name in summedfn:
            summedfn[name].append( filename )
        else:
            summedfn[name] = [ filename ]

# accumulates selected geometry
def get_accum_selected_arrays( summedfn ):
    summed = {}
    for n,fns in summedfn.items():
        ss = None
        for fn in fns:
            s = load_selected( fn )
            if not ss:
                ss = s
            else:
                if len(ss) != len(s):
                    print( "selected arrays are different sizes for (%s)! %d != %d" % (n, len(ss),len(s)) )
                    print( "fn: %s" % fn )
                    print( "fns:",fns )
                    return None
                for i in range(len(s)):
                    ss[i] = ss[i] or s[i]
        summed[n] = ss
    return summed

def add_to_scene( scene, addscene ):
    keys = scene.keys()
    for k,v in addscene.items():
        if isinstance(v,list) and len(v) > 0:
            if k in keys:  scene[k].extend(v)
            else:          scene[k] = v

def nameendswith( e, check ):
    try:     return e["name"].endswith( check )
    except:  return False

def updatepaths( e, pathjoin ):
    ne = {}
    for k,v in e.items():
        if not k.endswith("_filename"):
            ne[k] = v
        else:
            ne[k] = os.path.join( pathjoin, v )
    return ne

def updaterefs( e, ending, newending ):
    lending = len(ending)
    ne = {}
    for k,v in e.items():
        if isinstance(v,unicode):
            v = str(v)
        if not isinstance(v,str) or not v.endswith(ending):
            ne[k] = v
        else:
            ne[k] = "%s%s" % ( v[:-lending], newending )
    return ne

def grab_from_scene( scene, ending, newending, offset, pathjoin ):
    lending = len(ending)
    grabbed = {}
    
    for k,v in scene.items():
        for e in v:
            if not nameendswith( e, ending ): continue
            
            # update name by replacing num with tonum and adding letter
            #e["name"] = "%s_%s" % (e["name"][:-lending], newending)
            e = updaterefs( e, ending, newending )
            
            if k == "transforms":
                if e["type"] == "identitytransform":
                    e = {
                        "name":e["name"],
                        "type":"rigidtransform",
                        "frame": {
                            "o": [offset.x,offset.y,offset.z],
                            "x":[1,0,0], "y":[0,1,0], "z":[0,0,1]
                            }
                        }
                else:
                    f = e["frame"]
                    o = f["o"]
                    e = {
                        "name":e["name"],
                        "type":e["type"],
                        "frame": {
                            "o": [o[0] + offset.x,o[1] + offset.y,o[2] + offset.z],
                            "x":f["x"], "y":f["y"], "z":f["z"]
                            }
                        }
            
            #if k == "shapes" or k == "textures":
            e = updatepaths( e, pathjoin )
            
            add_to_scene( grabbed, { k: [ e ] } )
    
    return grabbed

def append_name_ref( scene, append ):
    for k0,v0 in scene.items():
        if k0 in ['name','meta_data']:
            continue
        for v1 in v0:
            for k2,v2 in v1.items():
                if k2.endswith( "_ref" ) or k2 == "name":
                    v1[k2] = "%s%s" % (v2,append)

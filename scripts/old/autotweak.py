#!/usr/bin/env python

import os, subprocess, shutil, random, itertools


# ./meshgit2.py diffsaved shaolin_0.ply shaolin_a.ply shaolin_0a.data



def runit( opts, lfiles, fnappend ):
    fp = open( 'opts.txt', 'wt' )
    fp.write( '\n'.join( str(opt) for opt in opts) )
    fp.close()

    for b,e0,e1 in lfiles:
        fn0 = '%s_%s.ply' % (b,e0)
        fn1 = '%s_%s.ply' % (b,e1)
        fn01 = '%s_%s%s_%s.data' % (b,e0,e1,fnappend)
        fnstate = 'state_%s.dat' % b
        
        if os.path.exists( fn01 ): continue
        subprocess.Popen( ['./meshgit2.py', 'buildandsave', fn0, fn1, fn01, 'method=itergreedyopts' ] ).communicate()



lfiles = [
    ( 'hand', '0', 'a' ),
    ( 'hand', '0', 'b' ),
    ( 'shaolin', '0', 'a' ),
    ( 'shaolin', '0', 'b' ),
    ( 'durano', '00', '01' ),
    ( 'durano', '01', '02' ),
    ( 'durano', '02', '03' ),
    ( 'durano', '03', '04' ),
    ]

# v_adddel,f_adddel,vdist,vnorm,fdist,fnorm,vunm,vmis,funm,fmis
#opts = [ 5.0, 20.0, 2.0, 0.0, 2.0, 2.0, 4.5, 9.0, 4.5, 9.0 ]
lopts = list( (a,b,c,d,e,f)
    for a in [ 1.0, 2.5, 5.0, 10.0, 20.0 ]
    for b in [ 1.0, 2.5, 5.0, 10.0, 20.0 ]
    for c in [ 0.0, 1.0 ]
    for d in [ 0.0, 1.0 ]
    for e in [ 0.5, 1.0, 2.5, 5.0 ]
    for f in [ 0.5, 1.0, 2.5, 5.0 ]
    )
random.shuffle(lopts)

for v_adddel,f_adddel,vnorm,un,vmis,fmis in lopts:
    opts = [ v_adddel, f_adddel, 1.0, vnorm, 1.0, 1.0, vmis*un, vmis, fmis*un, fmis ]
    fnappend = '_%s' % '_'.join( str(int(opt)) for opt in opts )
    print( '*' * 80 )
    print( '*' * 80 )
    print( fnappend )
    runit( opts, lfiles, fnappend )
#runit( opts, lfiles )

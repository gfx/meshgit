#!/usr/bin/env python -B
# -*- coding: utf-8 -*-

"""
processes the .json files to be viewed in near gl viewer
- removes non-changing ops
- removes undone work (does not, yet, undo redone undos)
- perform some lower-order clustering (repeated transforms, selects, etc.)
- perform, if specified, some higher-order clustering

NOTE: selections for certain ops are accumulated and stored in _summed files
"""

import os, glob, sys, subprocess, time, select, json, re, array, math
from operator import itemgetter
import config, scenefuncs, opfuncs, miscfuncs

if len(sys.argv) != 2:
    print( "Usage: %s [1|0:high_cluster]" % sys.argv[0] )
    quit()

performcluster = int(sys.argv[1])


#####################################################################################
### read steps.txt
### filter out ignorable operations
### translate op names to user-readable
### report statistics

print "Loading steps..."

total = 0
passed = []
ignored = []

ignorable = opfuncs.ignorable
passable = opfuncs.passable

# read steps file for processing / filtering
uops = {}
for step in open( config.stepsfile, "r" ):
    result = opfuncs.translateop( step )
    if not result:
        quit()
    
    (oppass,op,opparam,stepfn) = result
    
    if oppass:
        passed.append( [op, opparam, stepfn, total] )
    else:
        ignored.append( [op, opparam, stepfn, total] )
    
    total += 1
    
    # step unique?
    if not op in uops:
        uops[op] = 1
    else:
        uops[op] = uops[op] + 1

# format data for reporting
# sort the dictionary of unique ops
uopsa = sorted(uops.items(), key=itemgetter(1), reverse=True)
longest = max( [len(op[0]) for op in uopsa] )
pformat = "    {0:%d}   {1:d}" % longest
igstats = [pformat.format(op[0],op[1]) for op in uopsa if op[0] in ignorable]
pastats = [pformat.format(op[0],op[1]) for op in uopsa if not op[0] in ignorable]

# report
print( "Counts:" )
print( "    Total     %d" % total )
print( "    Ignored   %d" % len(ignored) )
print( "    Passed    %d" % len(passed) )
print( "Ignored stats:" )
for op in igstats: print( op )
print( "Passed stats:" )
for op in pastats: print( op )



#####################################################################################
### filter undone work

print "Filtering undone work..."

ops     = [c[0] for c in passed]
tvmodel = [miscfuncs.loadjsonfromstepfn(c[2]) for c in passed]
steps   = [int(c[3]) for c in passed]

(ops,steps,tvmodel) = opfuncs.filterundonework( ops, steps, tvmodel )



######################################################################################
### cluster repeated lower ops (transform, add face, delete selected, etc.)

print "Clustering repeated and paired lower-order ops..."
(tvmodel,ops,steps) = opfuncs.cluster_repeats(tvmodel,ops,steps,"^select",False,"select")
(tvmodel,ops,steps) = opfuncs.cluster_pairs(tvmodel,ops,steps,"^select","^transform")
(tvmodel,ops,steps) = opfuncs.cluster_pairs(tvmodel,ops,steps,"^select","^topo.add.edge_face")
(tvmodel,ops,steps) = opfuncs.cluster_pairs(tvmodel,ops,steps,"^select","^topo.delete.selected")
(tvmodel,ops,steps) = opfuncs.cluster_pairs(tvmodel,ops,steps,"^select","^topo.convert.tris_to_quads")
(tvmodel,ops,steps) = opfuncs.cluster_pairs(tvmodel,ops,steps,"^select","^topo.loopcut")
(tvmodel,ops,steps) = opfuncs.cluster_repeats(tvmodel,ops,steps,"^transform",True,"transform")
(tvmodel,ops,steps) = opfuncs.cluster_pairs(tvmodel,ops,steps,"^topo.extrude","^transform", "topo.extrude")

if performcluster:
    print "Clustering higher-order ops..."
    (tvmodel,ops,steps) = opfuncs.cluster_repeats(tvmodel,ops,steps,"^topo.add.edge_face",False)
    (tvmodel,ops,steps) = opfuncs.cluster_repeats(tvmodel,ops,steps,"^topo.delete.selected",False)
    (tvmodel,ops,steps) = opfuncs.cluster_repeats(tvmodel,ops,steps,"^topo.convert.tris_to_quads",False)
    (tvmodel,ops,steps) = opfuncs.cluster_repeats(tvmodel,ops,steps,"^topo.loopcut",False)





#######################################################################################
### write data out to steps_processed.txt

with open( "steps_processed.txt", "w" ) as f:
    for step,op in zip(steps,ops):
        f.write( "%06d %s\n" % (step,op) )
    
    #for i in range(len(ops)):
    #    f.write( "%06d %s\n" % (steps[i],ops[i]) )
    #    #f.write( "%s\n" % ops[i] )

print( "done!" )





#!/usr/bin/env python

import os, sys, re
from ply import *

if len(sys.argv) != 2:
    print( 'usage: %s [filename]' % sys.argv[0] )
    exit( 1 )

fnply = sys.argv[1]
funcm = re.sub( r'\.', r'', fnply )
fnmatlab = '%s.m' % funcm

ply = ply_load( fnply )
cv = len(ply['lv'])
lf = [ f[1:] for f in ply['lf'] ]

#le = [ [0]*cv for i in xrange(cv) ]
#for f in lf:
#    for v0,v1 in zip(f,f[1:]+[f[0]]):
#        le[v0][v1] = le[v1][v0] = 1
le = set()
for f in lf:
    for v0,v1 in zip(f,f[1:]+[f[0]]):
        if v0 > v1: v0,v1 = v1,v0
        le.add( frozenset([v0,v1]) )
lei,lej,les = [cv],[cv],[0]
for fsv01 in le:
    v0,v1 = fsv01
    lei.append( v0 )
    lej.append( v1 )
    les.append( 1 )
    lei.append( v1 )
    lej.append( v0 )
    les.append( 1 )

slf = []
for f in lf:
    if len(f) == 3:
        slf.append( ' '.join([str(e+1) for e in f+[f[0]]]) )
    else:
        slf.append( ' '.join([str(e+1) for e in f]) )


fp = open( fnmatlab, 'wt' )
fp.write( 'function [V,E,F] = %s()\n' % funcm )
fp.write( 'V = [%s]\';\n' % (';'.join([ ' '.join([str(e) for e in v]) for v in ply['lv'] ]),) )
#fp.write( 'E = [%s];\n'   % (';'.join([ ' '.join([str(e) for e in er]) for er in le ])) )
fp.write( 'Ei = [%s];\n' % (';'.join([str(e+1) for e in lei]),) )
fp.write( 'Ej = [%s];\n' % (';'.join([str(e+1) for e in lej]),) )
fp.write( 'Es = [%s];\n' % (';'.join([str(e) for e in les]),) )
fp.write( 'E = sparse( Ei, Ej, Es );' )
fp.write( 'F = [%s];\n'   % (';'.join(slf),) )
fp.close()
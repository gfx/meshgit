import math, sys, time, copy, numpy, md5, scipy.io, os, array, signal

from mesh import *
from vec3f import *
from camera import *
from shapes import *
from kdtree import *
from hungarian import *
from miscfuncs import *
from debugwriter import *

from correspondencebuilder_blended      import CorrespondenceBuilder_Blended        as CB_Blended
from correspondencebuilder_disney       import CorrespondenceBuilder_Disney         as CB_Disney
from correspondencebuilder_greedy       import CorrespondenceBuilder_Greedy         as CB_Greedy
from correspondencebuilder_hungarian    import CorrespondenceBuilder_Hungarian      as CB_Hungarian
from correspondencebuilder_icpgraphcuts import CorrespondenceBuilder_ICPGraphCuts   as CB_ICPGC
from correspondencebuilder_logic        import CorrespondenceBuilder_Logic          as CB_Logic
from correspondencebuilder_smac         import CorrespondenceBuilder_SMAC           as CB_SMAC
from correspondencebuilder_strips       import CorrespondenceBuilder_Strips         as CB_Strips

"""
handles the correspondences (matching) from one mesh (mesh0) to another (mesh1).

this is done by a mapping from mesh0->mesh1 (with a corresponding map from mesh1->mesh0)

this is done by uniquely labeling every vertex and face in mesh0, and then labeling every vert and face in mesh1 with
either a completely unique label (indicating an added vert/face) or with a label that matches a vert/face in mesh0
(indicating a substitution).  a vert/face in mesh0 has no matching vert/face in mesh1 indicates it has been deleted.
"""


class CorrespondenceInfo(object):
    
    def __init__( self, mesh0, mesh1 ):
        if mesh0 is None and mesh1 is None: return
        assert mesh0 is not None and mesh1 is not None
        
        mesh0.optimize().recalc_vertex_normals()
        mesh1.optimize().recalc_vertex_normals()
        
        self.m0,self.m1 = mesh0,mesh1                                               # the meshes
        self.lv0,self.lv1 = mesh0.lv,mesh1.lv                                       # the vertices (List of Vert)
        self.cv0,self.cv1 = len(self.lv0),len(self.lv1)                             # count of vertices
        self.ln0,self.ln1 = [ v.n for v in self.lv0 ],[v.n for v in self.lv1 ]      # the vertex normals
        self.lf0,self.lf1 = mesh0.lf,mesh1.lf                                       # the faces (List of Face)
        self.cf0,self.cf1 = len(self.lf0),len(self.lf1)                             # count of faces
        self.la_v0,self.la_v1 = mesh0._la_v,mesh1._la_v                             # vertex adjacency info
        self.la_f0,self.la_f1 = mesh0._la_f,mesh1._la_f                             # face adjacency info
        
        self.mv01,self.mv10 = {},{}                                                 # vertex mapping
        self.mf01,self.mf10 = {},{}                                                 # face mapping
        self.dmf01 = dict()
        self.norder = 0
        self.mismatch = float('inf')                                                # Not used??
        self.done = False                                                           # indicates if converged
        self.lmethods = []
    
    def __hash__( self ):
        m = md5.new()
        m.update( str(self.mv01) )
        m.update( str(self.mv10) )
        m.update( str(self.mf01) )
        m.update( str(self.mf10) )
        return int( m.hexdigest(), 16 )
    
    def hash_no_mismatches( self ):
        # find all elements with mismatched adjacencies
        # hash everything else
        ci = self
        
        l_v01 = [ (i_v0,i_v1,ci.la_v0[i_v0],ci.la_v1[i_v1]) for (i_v0,i_v1) in ci.mv01.iteritems() ]
        l_v10 = [ (i_v0,i_v1,ci.la_v0[i_v0],ci.la_v1[i_v1]) for (i_v1,i_v0) in ci.mv10.iteritems() ]
        l_f01 = [ (i_f0,i_f1,ci.la_f0[i_f0],ci.la_f1[i_f1]) for (i_f0,i_f1) in ci.mf01.iteritems() ]
        l_f10 = [ (i_f0,i_f1,ci.la_f0[i_f0],ci.la_f1[i_f1]) for (i_f1,i_f0) in ci.mf10.iteritems() ]
        
        ivrem0 = set( iv0 for (iv0,iv1,la_v0,la_v1) in l_v01 if
            ( not all ( ci.mv10[iav1] in la_v0['si_v'] for iav1 in la_v1['si_v'] if iav1 in ci.mv10 ) )
            or
            ( not all ( ci.mf10[iaf1] in la_v0['si_f'] for iaf1 in la_v1['si_f'] if iaf1 in ci.mf10 ) )
            )
        ivrem1 = set( iv1 for (iv0,iv1,la_v0,la_v1) in l_v10 if
            ( not all ( ci.mv10[iav1] in la_v0['si_v'] for iav1 in la_v1['si_v'] if iav1 in ci.mv10 ) )
            or
            ( not all ( ci.mf10[iaf1] in la_v0['si_f'] for iaf1 in la_v1['si_f'] if iaf1 in ci.mf10 ) )
            )
        
        ifrem0 = set( if0 for (if0,if1,la_f0,la_f1) in l_f01 if
            ( not all ( ci.mv10[iav1] in la_f0['si_v'] for iav1 in la_f1['si_v'] if iav1 in ci.mv10 ) )
            or
            ( not all ( ci.mf10[iaf1] in la_f0['si_f'] for iaf1 in la_f1['si_f'] if iaf1 in ci.mf10 ) )
            )
        ifrem1 = set( if1 for (if0,if1,la_f0,la_f1) in l_f01 if
            ( not all ( ci.mv10[iav1] in la_f0['si_v'] for iav1 in la_f1['si_v'] if iav1 in ci.mv10 ) )
            or
            ( not all ( ci.mf10[iaf1] in la_f0['si_f'] for iaf1 in la_f1['si_f'] if iaf1 in ci.mf10 ) )
            )
        
        ivrem0 |= set( iv0 for (iv0,iv1,la_v0,la_v1) in l_v01 if 
            ( not any( iaf0 in ci.mf01 for iaf0 in la_v0['si_f'] ) )
            or
            ( not any( iaf1 in ci.mf10 for iaf1 in la_v1['si_f'] ) )
            )
        ivrem0 |= set( iv1 for (iv0,iv1,la_v0,la_v1) in l_v10 if 
            ( not any( iaf0 in ci.mf01 for iaf0 in la_v0['si_f'] ) )
            or
            ( not any( iaf1 in ci.mf10 for iaf1 in la_v1['si_f'] ) )
            )
        
        dv01_clean = dict([ (i_v0,i_v1) for (i_v0,i_v1) in ci.mv01.iteritems() if i_v0 not in ivrem0 and i_v1 not in ivrem1 ])
        dv10_clean = dict([ (i_v1,i_v0) for (i_v1,i_v0) in ci.mv10.iteritems() if i_v0 not in ivrem0 and i_v1 not in ivrem1 ])
        df01_clean = dict([ (i_f0,i_f1) for (i_f0,i_f1) in ci.mf01.iteritems() if i_f0 not in ifrem0 and i_f1 not in ifrem1 ])
        df10_clean = dict([ (i_f1,i_f0) for (i_f1,i_f0) in ci.mf10.iteritems() if i_f0 not in ifrem0 and i_f1 not in ifrem1 ])
        
        m = md5.new()
        m.update( str(dv01_clean) )
        m.update( str(dv10_clean) )
        m.update( str(df01_clean) )
        m.update( str(df10_clean) )
        return int( m.hexdigest(), 16 )
        
    
    def clone( self ):
        ci = CorrespondenceInfo(None,None)
        
        # ref the same data (will not change)
        ci.m0,ci.m1 = self.m0,self.m1
        ci.lv0,ci.lv1 = self.lv0,self.lv1
        ci.cv0,ci.cv1 = self.cv0,self.cv1
        ci.ln0,ci.ln1 = self.ln0,self.ln1
        ci.lf0,ci.lf1 = self.lf0,self.lf1
        ci.cf0,ci.cf1 = self.cf0,self.cf1
        ci.la_v0,ci.la_v1 = self.la_v0,self.la_v1
        ci.la_f0,ci.la_f1 = self.la_f0,self.la_f1
        
        # make copy of correspondences
        ci.mv01,ci.mv10 = dict(self.mv01),dict(self.mv10)
        ci.mf01,ci.mf10 = dict(self.mf01),dict(self.mf10)
        ci.dmf01 = dict(self.dmf01)
        ci.norder = self.norder
        ci.mismatch = self.mismatch
        ci.done = self.done
        ci.lmethods = list( self.lmethods )
        
        return ci
    
    def copyvals( self, other ):
        assert len(self.lv0) == len(other.lv0)
        assert len(self.lv1) == len(other.lv1)
        assert len(self.lf0) == len(other.lf0)
        assert len(self.lf1) == len(other.lf1)
        self.mv01,self.mv10 = dict(other.mv01),dict(other.mv10)
        self.mf01,self.mf10 = dict(other.mf01),dict(other.mf10)
        self.dmf01 = dict(other.dmf01)
        self.norder = other.norder
        self.mismatch = other.mismatch
        self.done = other.done
        self.lmethods = list( other.lmethods )
        return self
    
    # reverse direction of correspondence from mesh0->mesh1 to mesh1->mesh0
    def reverse( self ):
        self.m0,self.m1 = self.m1,self.m0
        self.lv0,self.lv1 = self.lv1,self.lv0
        self.cv0,self.cv1 = self.cv1,self.cv0
        self.ln0,self.ln1 = self.ln1,self.ln0
        self.lf0,self.lf1 = self.lf1,self.lf0
        self.cf0,self.cf1 = self.cf1,self.cf0
        self.la_v0,self.la_v1 = self.la_v1,self.la_v0
        self.la_f0,self.la_f1 = self.la_f1,self.la_f0
        
        self.dmf01 = dict( (self.mf01[i],v) for i,v in self.dmf01.iteritems() )
        
        self.mv01,self.mv10 = self.mv10,self.mv01
        self.mf01,self.mf10 = self.mf10,self.mf01
        
        return self
    
    
    ####################################################################################################################
    # Build Correspondences (Special Constructor)
    ####################################################################################################################
    
    
    
    @classmethod
    def build( cls, mesh0, mesh1, method=None, maxiters=None ):
        
        # set defaults
        if method is None: method = 'greedy_iter'
        maxiters = int(maxiters) if maxiters is not None else 10000
        
        dmethods = {
            'blended_verts':    CB_Blended.build_verts,
            'closest':          None,
            'disney':           CB_Disney.build,
            'greedy_iter':      CB_Greedy.build_iterative,
            'greedy_seeded':    None,
            'greedy_stepped':   None,
            'greedy_super':     None,
            'hungarian_both':   None, #CorrespondenceBuilder_Hungarian.build_both,
            'hungarian_faces':  CB_Hungarian.build_faces,
            'hungarian_verts':  CB_Hungarian.build_verts,
            'icpgc_verts':      CB_ICPGC.build_verts,
            'seediter':         None,
            'smac_faces':       CB_SMAC.build_faces,
            'smac_verts':       CB_SMAC.build_verts,
            'strips':           CB_Strips.build,
            }
        lmethodnames = [ n for n,f in dmethods.iteritems() if f is not None ]
        assert method in lmethodnames, 'unknown or unimplemented method "%s" (%s)' % (method,','.join(lmethodnames))
        methodfn = dmethods[method]
        
        ci = CorrespondenceInfo( mesh0, mesh1 )
        opts = {
            'ci': ci,
            'maxiters': maxiters,
            }
        ci.opts = opts
        ci.set_default_costs( maxiters )
        
        DebugWriter.start( 'building correspondences' )
        DebugWriter.report( 'stats', {
            'verts': [ ci.cv0, ci.cv1 ],
            'faces': [ ci.cf0, ci.cf1 ],
            } )
        DebugWriter.report( 'method', method )
        
        methodfn( opts )
        ci.lmethods.append( method )
        ci.done = opts['done'] if 'done' in opts else True
        
        DebugWriter.end().blank(3)
        
        #ci.compute_mismatch_cost( opts )
        return ci
    
    def set_default_costs( self, maxiters ):
        weights = [
            #  0    1     2     3     4     5     6     7     8      9    10    11    12    13    14
            # geo   v+    v-    f+    f-    vd    vn    fd    fn    nvu   nvm   nfu   nfm   fvd   ffd
            [ 1.0, 20.0, 20.0, 50.0, 50.0, 30.0, 30.0, 30.0, 30.0, 10.0, 20.0, 10.0, 20.0,  0.0,  0.0 ],        # 0 sig12 paper weights
            [ 1.0, 20.0, 20.0, 40.0, 40.0, 10.0, 10.0, 10.0, 10.0, 10.0, 25.0, 10.0, 25.0,  0.0,  0.0 ],        # 1 these work generally well!
            [ 1.0,  1.0,  1.0,  4.0,  4.0,  1.0,  1.0,  1.0,  1.0,  0.1,  1.0,  0.1,  1.0,  0.0,  0.0 ],        # 2 wow!?
            [ 1.0,  1.5,  1.5,  4.0,  4.0,  1.0,  1.0,  1.0,  1.0,  0.1,  1.5,  0.1,  1.5,  0.0,  0.0 ],        # 3 great   <---
            [ 1.0,  0.1,  0.1,  4.0,  4.0,  0.2,  0.2,  2.0,  2.0,  0.2,  0.2,  0.2,  2.0,  0.0,  0.0 ],        # 4 ??
            
            [ 1.0,  1.5,  1.5,  4.0,  4.0,  1.0,  1.0,  1.0,  1.0,  0.1,  0.5,  0.1,  0.5,  0.5,  0.5 ],        # 5 hmm
            [ 1.0,  1.5,  1.5,  6.0,  6.0,  1.0,  1.0,  4.0,  4.0,  0.1,  0.5,  0.4,  2.0,  0.5,  2.0 ],        # 6 this could work...
            [ 1.0,  1.5,  1.5,  6.0,  6.0,  1.0,  1.0,  4.0,  4.0,  0.2,  0.5,  0.8,  2.0,  0.5,  2.0 ],        # 7 ??
            [ 1.0,  1.5,  1.5,  4.0,  4.0,  1.0,  1.0,  1.0,  1.0,  0.1,  1.5,  0.1,  1.5,  1.0,  1.0 ],        # 8 modified 3
            [ 1.0,  4.0,  4.0,  4.0,  4.0,  0.2,  0.2,  0.2,  0.2,  1.0,  2.0,  1.0,  2.0,  5.0,  5.0 ],        # 9 hmm
            
            [ 1.0,  1.5,  1.5,  4.0,  4.0,  1.0,  1.0,  1.0,  1.0,  0.2,  2.0,  0.2,  2.0,  0.0,  0.0 ],        # 10 fixed accounting error  <=====
            [ 1.0,  1.5,  1.5,  4.0,  4.0,  1.5,  1.5,  1.5,  1.5,  0.5,  2.0,  0.5,  2.0,  0.0,  0.0 ],        # 11
            
            [ 1.0,  5.0,  5.0,  5.0,  5.0,  1.0,  1.0,  1.0,  1.0,  0.5,  2.0,  0.5,  2.0,  0.0,  0.0 ],        # 12
            [ 1.0,  5.0,  0.0,  5.0,  0.0,  1.0,  1.0,  1.0,  1.0,  0.5,  2.0,  0.5,  2.0,  0.5,  0.5 ],        # 13 <========
            [ 1.0, 10.0,  0.0, 10.0,  0.0,  1.0,  1.0,  1.0,  1.0,  0.7,  2.0,  0.7,  2.0,  0.5,  0.5 ],        # 14 with division by n
            
            [ 1.0,  5.0,  0.0,  5.0,  0.0,  1.0,  1.0,  1.0,  1.0,  1.0,  3.5,  1.0,  3.5,  0.0,  0.0 ],        # 15  <--- this one
            [ 1.0,  2.5,  0.0,  2.5,  0.0,  1.0,  1.0,  1.0,  1.0,  0.5, 1.75,  0.5, 1.75,  0.0,  0.0 ],        # 16
            #                    ^- not used                 doubled -^---^      ^----^- not used
            
            ][16]
        
        geo = weights[0]
        costs = {
            'cvadd':    weights[1] / geo,
            'cvdel':    weights[2] / geo,
            'cvmism':   0.0,
            'cfadd':    weights[3] / geo,
            'cfdel':    weights[4] / geo,
            'cvdist':   weights[5],
            'cvnorm':   weights[6],
            'cfdist':   weights[7],
            'cfnorm':   weights[8],
            'cfvunk':   weights[9] / geo,
            'cfvmis':   weights[10] / geo,
            'cfmunk':   weights[11] / geo,
            'cfmism':   weights[12] / geo,
            'cfvdist':  weights[13] / geo,
            'cffdist':  weights[14] / geo,
            'k':        20,
            'maxiters': maxiters,
            }
        
        self.opts.setdefault( 'costs', costs )
        
    
    ####################################################################################################################
    # Cost Computation Functions
    ####################################################################################################################
    
    
    
    def compute_mismatch_cost( self, opts ):
        costs_prev = opts.get('costs',None)
        opts['costs'] = {
            'cvadd': 0.0, 'cvdel': 0.0,
            'cvdist': 0.0, 'cvnorm': 0.0, 'cvmism': 5.0,
            'cfadd': 0.0, 'cfdel': 0.0,
            'cfdist': 0.0, 'cfvunk': 0.0, 'cfvmis': 10.0,
            'cfnorm': 0.0, 'cfvdist': 0.0, 'cffdist': 0.0,
            'cfmism': 10.0, 'cfmunk': 0.0
            }
        c = self.compute_cost( opts )
        c = float(c) / float(self.cf0+self.cf1)
        self.mismatch = c
        
        DebugWriter.report( 'mismatch heuristic', c )
        
        opts['costs'] = costs_prev
        return c
    
    def compute_cost( self, opts ):
        DebugWriter.start( 'computing cost' )
        ci,costs = self,opts['costs']
        
        cost = 0.0
        
        m0,m1 = ci.m0,ci.m1
        mv01,mv10 = ci.mv01,ci.mv10
        mf01,mf10 = ci.mf01,ci.mf10
        lv0,lv1 = ci.lv0,ci.lv1
        lf0,lf1 = ci.lf0,ci.lf1
        la_v0,la_v1 = ci.la_v0,ci.la_v1
        la_f0,la_f1 = ci.la_f0,ci.la_f1
        
        l_v01 = [ (i_v0,i_v1,la_v0[i_v0],la_v1[i_v1]) for (i_v0,i_v1) in mv01.iteritems() ]
        l_v10 = [ (i_v0,i_v1,la_v0[i_v0],la_v1[i_v1]) for (i_v1,i_v0) in mv10.iteritems() ]
        l_f01 = [ (i_f0,i_f1,la_f0[i_f0],la_f1[i_f1]) for (i_f0,i_f1) in mf01.iteritems() ]
        l_f10 = [ (i_f0,i_f1,la_f0[i_f0],la_f1[i_f1]) for (i_f1,i_f0) in mf10.iteritems() ]
        
        # compute unmatched elements term
        n_unmatched_verts = (ci.cv0-len(mv01)) + (ci.cv1-len(mv10))
        n_unmatched_faces = (ci.cf0-len(mf01)) + (ci.cf1-len(mf10))
        term_elements = n_unmatched_verts + n_unmatched_faces
        
        # compute geometric terms
        term_distance = 0.0
        term_norm = 0.0
        for (i_v0,i_v1) in mv01.iteritems():
            d = Vec3f.t_distance( lv0[i_v0].p, lv1[i_v1].p )
            n = Vec3f.t_dot( lv0[i_v0].n, lv1[i_v1].n )
            term_distance += d / (1.0+d)
            term_norm += 1.0 - n
        for (i_f0,i_f1) in mf01.iteritems():
            lv_f0 = [ lv0[i_v] for i_v in m0._opt_get_f_li_v(i_f0)]
            lv_f1 = [ lv1[i_v] for i_v in m1._opt_get_f_li_v(i_f1)]
            p0 = Vec3f.t_average( [ v.p for v in lv_f0 ] )
            p1 = Vec3f.t_average( [ v.p for v in lv_f1 ] )
            n0,n1 = Face.calc_normal(lv_f0),Face.calc_normal(lv_f1)
            d = Vec3f.t_distance( p0, p1 )
            n = Vec3f.t_dot( n0, n1 )
            term_distance += d / (1.0+d)
            term_norm += 1.0 - n
        
        # compute adjacencies terms
        term_unmatched_adjacencies = 0.0
        term_mismatched_adjacencies = 0.0
        for i_v0 in xrange(ci.cv0):
            c_v0 = len(la_v0[i_v0]['si_v']) + len(la_v0[i_v0]['si_f'])
            for ia_v0 in la_v0[i_v0]['si_v']:
                ca_v0 = len(la_v0[ia_v0]['si_v']) + len(la_v0[ia_v0]['si_f'])
                if i_v0 not in mv01 or ia_v0 not in mv01:
                    term_unmatched_adjacencies += 1.0 / float(c_v0+ca_v0)
                    continue
                i_v1,ia_v1 = mv01[i_v0],mv01[ia_v0]
                if ia_v1 not in la_v1[i_v1]['si_v']:
                    term_mismatched_adjacencies += 1.0 / float(c_v0+ca_v0)
                    continue
                pass
            for ia_f0 in la_v0[i_v0]['si_f']:
                ca_f0 = len(la_f0[ia_f0]['si_v']) + len(la_f0[ia_f0]['si_f'])
                if i_v0 not in mv01 or ia_f0 not in mf01:
                    term_unmatched_adjacencies += 1.0 / float(c_v0+ca_f0)
                    continue
                i_v1,ia_f1 = mv01[i_v0],mf01[ia_f0]
                if ia_f1 not in la_v1[i_v1]['si_f']:
                    term_mismatched_adjacencies += 1.0 / float(c_v0+ca_f0)
                    continue
                pass
        for i_v1 in xrange(ci.cv1):
            c_v1 = len(la_v1[i_v1]['si_v']) + len(la_v1[i_v1]['si_f'])
            for ia_v1 in la_v1[i_v1]['si_v']:
                ca_v1 = len(la_v1[ia_v1]['si_v']) + len(la_v1[ia_v1]['si_f'])
                if i_v1 not in mv10 or ia_v1 not in mv10:
                    term_unmatched_adjacencies += 1.0 / float(c_v1+ca_v1)
                    continue
                i_v0,ia_v0 = mv10[i_v1],mv10[ia_v1]
                if ia_v0 not in la_v0[i_v0]['si_v']:
                    term_mismatched_adjacencies += 1.0 / float(c_v1+ca_v1)
                    continue
                pass
            for ia_f1 in la_v1[i_v1]['si_f']:
                ca_f1 = len(la_f1[ia_f1]['si_v']) + len(la_f1[ia_f1]['si_f'])
                if i_v1 not in mv10 or ia_f1 not in mf10:
                    term_unmatched_adjacencies += 1.0 / float(c_v1+ca_f1)
                    continue
                i_v0,ia_f0 = mv10[i_v1],mf10[ia_f1]
                if ia_f0 not in la_v0[i_v0]['si_f']:
                    term_mismatched_adjacencies += 1.0 / float(c_v1+ca_f1)
                    continue
                pass
        for i_f0 in xrange(ci.cf0):
            c_f0 = len(la_f0[i_f0]['si_v']) + len(la_f0[i_f0]['si_f'])
            for ia_v0 in la_f0[i_f0]['si_v']:
                ca_v0 = len(la_v0[ia_v0]['si_v']) + len(la_v0[ia_v0]['si_f'])
                if i_f0 not in mf01 or ia_v0 not in mv01:
                    term_unmatched_adjacencies += 1.0 / float(c_f0+ca_v0)
                    continue
                i_f1,ia_v1 = mf01[i_f0],mv01[ia_v0]
                if ia_v1 not in la_f1[i_f1]['si_v']:
                    term_mismatched_adjacencies += 1.0 / float(c_f0+ca_v0)
                    continue
                pass
            for ia_f0 in la_f0[i_f0]['si_f']:
                ca_f0 = len(la_f0[ia_f0]['si_v']) + len(la_f0[ia_f0]['si_f'])
                if i_f0 not in mf01 or ia_f0 not in mf01:
                    term_unmatched_adjacencies += 1.0 / float(c_f0+ca_f0)
                    continue
                i_f1,ia_f1 = mf01[i_f0],mf01[ia_f0]
                if ia_f1 not in la_f1[i_f1]['si_f']:
                    term_mismatched_adjacencies += 1.0 / float(c_f0+ca_f0)
                    continue
                pass
        for i_f1 in xrange(ci.cf1):
            c_f1 = len(la_f1[i_f1]['si_v']) + len(la_f1[i_f1]['si_f'])
            for ia_v1 in la_f1[i_f1]['si_v']:
                ca_v1 = len(la_v1[ia_v1]['si_v']) + len(la_v1[ia_v1]['si_f'])
                if i_f1 not in mf10 or ia_v1 not in mv10:
                    term_unmatched_adjacencies += 1.0 / float(c_f1+ca_v1)
                    continue
                i_f0,ia_v0 = mf10[i_f1],mv10[ia_v1]
                if ia_v0 not in la_f0[i_f0]['si_v']:
                    term_mismatched_adjacencies += 1.0 / float(c_f1+ca_v1)
                    continue
                pass
            for ia_f1 in la_f1[i_f1]['si_f']:
                ca_f1 = len(la_f1[ia_f1]['si_v']) + len(la_f1[ia_f1]['si_f'])
                if i_f1 not in mf10 or ia_f1 not in mf10:
                    term_unmatched_adjacencies += 1.0 / float(c_f1+ca_f1)
                    continue
                i_f0,ia_f0 = mf10[i_f1],mf10[ia_f1]
                if ia_f0 not in la_f0[i_f0]['si_f']:
                    term_mismatched_adjacencies += 1.0 / float(c_f1+ca_f1)
                    continue
                pass
        
        cost += costs['cvdel'] * term_elements
        cost += term_distance + term_norm
        cost += costs['cfvunk'] * term_unmatched_adjacencies
        cost += costs['cfvmis'] * term_mismatched_adjacencies
        
        DebugWriter.report( 'cost', cost )
        DebugWriter.end()
        
        return cost
    
    def compute_cost_old( self, opts, si_lv0=None, si_lf0=None ):
        DebugWriter.start( 'computing cost' )
        ci = self
        opts_ = self.get_optimizations( opts )
        costs = opts['costs']
        costcache = opts.setdefault( 'costcache', {'di_v01':dict(),'di_f01':dict()} )
        di_v01_costcache = costcache['di_v01']
        di_f01_costcache = costcache['di_f01']
        opts.setdefault( 'dirty', { 'si_v0_dirty': set(), 'si_v1_dirty': set(), 'si_f0_dirty': set(), 'si_f1_dirty': set(), } )
        si_v0_dirty = opts['dirty']['si_v0_dirty']
        si_v1_dirty = opts['dirty']['si_v1_dirty']
        si_f0_dirty = opts['dirty']['si_f0_dirty']
        si_f1_dirty = opts['dirty']['si_f1_dirty']
        
        di_v01_costaccum = costcache.setdefault('di_v01_costaccum',{})
        di_f01_costaccum = costcache.setdefault('di_f01_costaccum',{})
        
        cvadd,cvdel = costs['cvadd'],costs['cvdel']
        cvdist,cvnorm,cvmism = costs['cvdist'],costs['cvnorm'],costs['cvmism']
        cfadd,cfdel = costs['cfadd'],costs['cfdel']
        cfdist,cfvunk,cfvmis = costs['cfdist'],costs['cfvunk'],costs['cfvmis']
        cfnorm = costs['cfnorm']
        cfmism,cfmunk = costs['cfmism'],costs['cfmunk']
        cfvdist,cffdist = costs['cfvdist'],costs['cffdist']
        
        # optimize
        la_v0,la_v1 = ci.la_v0,ci.la_v1
        la_f0,la_f1 = ci.la_f0,ci.la_f1
        lv0,lv1 = ci.lv0,ci.lv1
        lf0,lf1 = ci.lf0,ci.lf1
        mv01,mv10 = ci.mv01,ci.mv10
        mf01,mf10 = ci.mf01,ci.mf10
        
        mvd,mvn,mfd,mfn = opts_['mvd'],opts_['mvn'],opts_['mfd'],opts_['mfn']       # pre-computed distances
        li_fsi_v0,li_fsi_v1 = opts_['li_fsi_v0'],opts_['li_fsi_v1']                 # inds of verts for each face
        lcv0,lcv1,lcn0,lcn1 = opts_['lcv0'],opts_['lcv1'],opts_['lcn0'],opts_['lcn1']
        
        limit = (si_lv0 is not None) or (si_lf0 is not None)
        if limit:
            mv01_ = dict( (iv0,ci.mv01[iv0]) for iv0 in si_lv0 if iv0 in mv01 )
            mf01_ = dict( (if0,ci.mf01[if0]) for if0 in si_lf0 if if0 in mf01 )
        else:
            mv01_ = mv01
            mf01_ = mf01
        
        cost = 0.0
        
        # calc costs for adding and deleting geometry
        if not limit:
            cost += cvdel * float( ci.cv0 - len(mv01) )
            cost += cvadd * float( ci.cv1 - len(mv10) )
            cost += cfdel * float( ci.cf0 - len(mf01) )
            cost += cfadd * float( ci.cf1 - len(mf10) )
            
            #cost += (cvadd + cvdel) * float(sum( len(la_v0[iv0]['si_f']) for iv0 in xrange(ci.cv0) if iv0 not in mv01 ))
            #cost += (cfadd + cfdel) * float(sum( len(la_f0[if0]['si_f']) for if0 in xrange(ci.cf0) if if0 not in mf01 ))
            
            #cost += cfdel * float(sum( len(li_fsi_v0[i_f]) for i_f,_ in ci.elf0() if i_f not in mf01 ))
            #cost += cfdel * float(sum( len(li_fsi_v1[i_f]) for i_f,_ in ci.elf1() if i_f not in mf10 ))
        else:
            #cost += cvdel * float(sum( i_v in mv01 for i_v in si_lv0 ))
            #cost += cfdel * float(sum( len(li_fsi_v0[i_f]) for i_f in si_lv0 if i_f not in mf01 ))
            #cost += cfadd * float(sum( len(li_fsi_v1[i_f]) for i_f in si_lv1 if i_f not in mf10 ))
            pass
        # calc costs for substituting geometry
        for iv0,iv1 in mv01_.iteritems():
            _cost = 0.0
            if iv0 not in si_v0_dirty and iv1 not in si_v1_dirty and (iv0,iv1) in di_v01_costcache:
                _cost += di_v01_costcache[(iv0,iv1)]
            else:
                v0,v1 = lv0[iv0],lv1[iv1]
                
                # get adjacent faces to v0 and v1 and corresponding faces to adj faces of v0
                si_af0 = la_v0[iv0]['si_f']
                si_af1 = la_v1[iv1]['si_f']
                si_af01 = set( mf01[i_af0] for i_af0 in si_af0 if i_af0 in mf01 )
                si_af10 = set( mf10[i_af1] for i_af1 in si_af1 if i_af1 in mf10 )
                
                #cmism = len(si_af0) + len(si_af1) - len(si_af0 & si_af10) - len(si_af1 & si_af01)
                
                #_cost += cvdist * mvd[iv0][iv1]
                #_cost += cvnorm * mvn[iv0][iv1]
                _cost += cvdist * Vec3f.t_distance( v0.p, v1.p )
                _cost += cvnorm * ( 1.0 - Vec3f.t_dot( v0.n, v1.n ) )
                #_cost += cvmism * float(cmism)
                #_cost += 0.0 if (iv0,iv1) not in di_v01_costaccum else di_v01_costaccum[(iv0,iv1)]
                
                di_v01_costcache[(iv0,iv1)] = _cost
            
            cost += _cost
        
        for if0,if1 in mf01_.iteritems():
            _cost = 0.0
            if if0 not in si_f0_dirty and if1 not in si_f1_dirty and (if0,if1) in di_f01_costcache:
                _cost += di_f01_costcache[(if0,if1)]
            else:
                f0,f1 = lf0[if0],lf1[if1]
                
                si_v0,si_v1 = li_fsi_v0[if0],li_fsi_v1[if1]
                si_af0 = la_f0[if0]['si_f']
                si_af1 = la_f1[if1]['si_f']
                si_af01 = set( mf01[i_af0] for i_af0 in si_af0 if i_af0 in mf01 )
                si_af10 = set( mf10[i_af1] for i_af1 in si_af1 if i_af1 in mf10 )
                
                cv = len(si_v0)#+len(si_v1)
                cf = len(si_af0)#+len(si_af1)
                
                #cunk = sum( iv0 not in mv01 for iv0 in si_v0 ) + sum( iv1 not in mv10 for iv1 in si_v1 )
                #cmis = len(si_v0)+len(si_v1) - sum( (iv0 in mv01 and mv01[iv0] in si_v1) for iv0 in si_v0 ) - sum( (iv1 in mv10 and mv10[iv1] in si_v0) for iv1 in si_v1 ) - cunk
                cvunk = sum( iv0 not in mv01 for iv0 in si_v0 )
                cvmis = len(si_v0) - sum( (iv0 in mv01 and mv01[iv0] in si_v1) for iv0 in si_v0 ) - cvunk
                cfunk = len(si_af0) - len(si_af01)
                cfmis = len(si_af0) - len(si_af0 & si_af10) - cfmunk
                
                _cost = 0.0
                #_cost += cfdist * mfd[if0][if1]
                #_cost += cfnorm * mfn[if0][if1]
                _cost += cfdist * Vec3f.t_distance( lcv0[if0], lcv1[if1] )
                _cost += cfnorm * ( 1.0 - Vec3f.t_dot( lcn0[if0], lcn1[if1] ) )
                _cost += cfvunk * float(cvunk)# / float(cv)
                _cost += cfvmis * float(cvmis)# / float(cv)
                _cost += cfmunk * float(cfunk)# / float(cf)
                _cost += cfmism * float(cfmis)# / float(cf)
                _cost += 0.0 if (if0,if1) not in di_f01_costaccum else di_f01_costaccum[(if0,if1)]
                
                di_f01_costcache[(if0,if1)] = _cost
                
            cost += _cost
        
        DebugWriter.end()
        
        return cost
    
    #def compute_cost_after_v_action( self, costs, opt, prev_cost, i0, i1 ):
    def compute_cost_delta_acv( self, opts, i_v0, i_v1 ):
        ci = self
        opts_ = self.get_optimizations( opts )
        
        di_mv01,di_mv10 = ci.mv01,ci.mv10
        di_mf01,di_mf10 = ci.mf01,ci.mf10
        li_va0,li_va1 = opts_['li_va0'],opts_['li_va1']
        li_fa0,li_fa1 = opts_['li_fa0'],opts_['li_fa1']
        costs = opts['costs']
        
        si_lv0 = set([i_v0])
        si_lf0 = ci.la_v0[i_v0]['si_f'] | set( di_mf10[_i_f1] for _i_f1 in li_va1[i_v1]['si_af'] if _i_f1 in di_mf10 )
        
        delta_cost = 0.0
        
        delta_cost -= ci.compute_cost( opts, si_lv0=si_lv0, si_lf0=si_lf0 )
        delta_cost -= costs['cvdel'] + costs['cvadd']
        
        di_mv01[i_v0] = i_v1
        di_mv10[i_v1] = i_v0
        delta_cost += ci.compute_cost( opts, si_lv0=si_lv0, si_lf0=si_lf0 )
        di_mv01.pop(i_v0)
        di_mv10.pop(i_v1)
        
        return delta_cost
    
    #def compute_cost_after_f_action( self, costs, opt, prev_cost, i0, i1 ):
    def compute_cost_delta_acf( self, opts, i_f0, i_f1 ):
        ci = self
        opts_ = self.get_optimizations( opts )
        
        di_mv01,di_mv10 = ci.mv01,ci.mv10
        di_mf01,di_mf10 = ci.mf01,ci.mf10
        li_va0,li_va1 = opts_['li_va0'],opts_['li_va1']
        li_fa0,li_fa1 = opts_['li_fa0'],opts_['li_fa1']
        costs = opts['costs']
        
        si_lv0 = li_fa0[i_f0]['si_v'] | set( di_mv10[_i_v1] for _i_v1 in li_fa1[i_f1]['si_afv'] if _i_v1 in di_mv10 )
        si_lf0 = set([i_f0]) | li_fa0[i_f0]['si_afv'] | set( di_mf10[_i_f1] for _i_f1 in li_fa1[i_f1]['si_afv'] if _i_f1 in di_mf10 )
        
        delta_cost = 0.0
        
        delta_cost -= ci.compute_cost( opts, si_lv0=si_lv0, si_lf0=si_lf0 )
        #delta_cost -= costs['cfdel'] * len(li_fa0[i_f0]['si_v']) + costs['cfadd'] * len(li_fa1[i_f1]['si_v'])
        delta_cost -= costs['cfdel'] + costs['cfadd']
        
        di_mf01[i_f0] = i_f1
        di_mf10[i_f1] = i_f0
        delta_cost += ci.compute_cost( opts, si_lv0=si_lv0, si_lf0=si_lf0 )
        di_mf01.pop(i_f0)
        di_mf10.pop(i_f1)
        
        return delta_cost
    
    def get_optimizations( self, opts ):
        if 'optimizations' not in opts:
            ci = self
            m0,m1 = ci.m0,ci.m1
            
            DebugWriter.start( 'gathering mesh statistics' )
            
            map_fn( Mesh.optimize, [m0,m1] )                        # optimize meshes
            du_vi0,du_vi1 = m0._uv_iv,m1._uv_iv                     # dictionaries to translate vert uids to inds
            du_fi0,du_fi1 = m0._uf_if,m1._uf_if                     # dictionaries to translate face uids to inds
            
            stats0,stats1 = map( Mesh.get_stats, [m0,m1] )          # get adjacency data
            du_va0,du_va1 = stats0['adj_v'],stats1['adj_v']         # ...  NOTE: keyed by uids!
            du_fa0,du_fa1 = stats0['adj_f'],stats1['adj_f']         # ...  NOTE: keyed by uids!
            
            # translate du_va0,du_va1,du_fa0,du_fa1 to be keyed by inds
            li_va0 = [ {
                'u_v': v.u_v,
                'i_v': i_v,
                'si_nv': set( du_vi0[_u_v] for _u_v in du_va0[v.u_v]['su_nv'] ),
                'si_ae': set( (du_vi0[_u_v0],du_vi0[_u_v1]) for _u_v0,_u_v1 in du_va0[v.u_v]['su_ae'] ),
                'si_af': set( du_fi0[_u_f] for _u_f in du_va0[v.u_v]['su_af'] ),
                } for i_v,v in m0.elv() ]
            li_va1 = [ {
                'u_v': v.u_v,
                'i_v': i_v,
                'si_nv': set( du_vi1[_u_v] for _u_v in du_va1[v.u_v]['su_nv'] ),
                'si_ae': set( (du_vi1[_u_v0],du_vi1[_u_v1]) for _u_v0,_u_v1 in du_va1[v.u_v]['su_ae'] ),
                'si_af': set( du_fi1[_u_f] for _u_f in du_va1[v.u_v]['su_af'] ),
                } for i_v,v in m1.elv() ]
            li_fa0 = [ {
                'u_f': f.u_f,
                'i_f': i_f,
                'si_v': set( du_vi0[_u_v] for _u_v in f.lu_v ),
                'si_ae': set( frozenset([du_vi0[_u_v0],du_vi0[_u_v1]]) for _u_v0,_u_v1 in du_fa0[f.u_f]['lsuv'] ),
                'si_afe': set( du_fi0[_u_f] for _u_f in du_fa0[f.u_f]['su_afe'] ),
                'si_afv': set( du_fi0[_u_f] for _u_f in du_fa0[f.u_f]['su_afv'] ),
                } for i_f,f in m0.elf() ]
            li_fa1 = [ {
                'u_f': f.u_f,
                'i_f': i_f,
                'si_v': set( du_vi1[_u_v] for _u_v in f.lu_v ),
                'si_ae': set( frozenset([du_vi1[_u_v0],du_vi1[_u_v1]]) for _u_v0,_u_v1 in du_fa1[f.u_f]['lsuv'] ),
                'si_afe': set( du_fi1[_u_f] for _u_f in du_fa1[f.u_f]['su_afe'] ),
                'si_afv': set( du_fi1[_u_f] for _u_f in du_fa1[f.u_f]['su_afv'] ),
                } for i_f,f in m1.elf() ]
            
            DebugWriter.end()
            
            
            #DebugWriter.start( 'localizing matching data' )
            di_mv01,di_mv10 = ci.mv01,ci.mv10
            di_mf01,di_mf10 = ci.mf01,ci.mf10
            si_uv0 = set( iv0 for iv0,_ in m0.elv() if iv0 not in di_mv01 )
            si_uv1 = set( iv1 for iv1,_ in m1.elv() if iv1 not in di_mv10 )
            si_uf0 = set( if0 for if0,_ in m0.elf() if if0 not in di_mf01 )
            si_uf1 = set( if1 for if1,_ in m1.elf() if if1 not in di_mf10 )
            #DebugWriter.end()
            
            
            DebugWriter.start( 'pre-computing distances between unmatched elements' )
            lv0,lv1 = m0.lv,m1.lv
            mvd,mvn = None,None
            #mvd = [ [0.0] * ci.cv1 for i in xrange(ci.cv0) ]
            #mvn = [ [0.0] * ci.cv1 for i in xrange(ci.cv0) ]
            #for i0,i1 in product( si_uv0, si_uv1 ):
                #v0 = lv0[i0]
                #v1 = lv1[i1]
                #mvd[i0][i1] = Vec3f.t_distance( v0.p, v1.p )
                #mvn[i0][i1] = 1.0 - Vec3f.t_dot( v0.n, v1.n )
            #for i0 in xrange(ci.cv0):
                #if i0 not in ci.mv01: continue
                #i1 = ci.mv01[i0]
                #v0 = lv0[i0]
                #v1 = lv1[i1]
                #mvd[i0][i1] = Vec3f.t_distance( v0.p, v1.p )
                #mvn[i0][i1] = 1.0 - Vec3f.t_dot( v0.n, v1.n )
            # compute face centers
            lcv0 = [ Vertex.average( m0.opt_get_uf_lv(f0.u_f) ) for i_f0,f0 in ci.elf0() ]
            lcv1 = [ Vertex.average( m1.opt_get_uf_lv(f1.u_f) ) for i_f1,f1 in ci.elf1() ]
            lcn0 = [ m0.opt_face_normal(i_f) for i_f,_ in m0.elf() ]
            lcn1 = [ m1.opt_face_normal(i_f) for i_f,_ in m1.elf() ]
            mfd,mfn = None,None
            #mfd = [ [0.0] * ci.cf1 for x in xrange(ci.cf0) ]
            #mfn = [ [0.0] * ci.cf1 for i in xrange(ci.cf0) ]
            #for i0,i1 in product( si_uf0, si_uf1 ):
                #mfd[i0][i1] = Vec3f.t_distance( lcv0[i0], lcv1[i1] )
                #mfn[i0][i1] = 1.0 - Vec3f.t_dot( lcn0[i0], lcn1[i1] )
            #for i0 in xrange(ci.cf0):
                #if i0 not in ci.mf01: continue
                #i1 = ci.mf01[i0]
                #mfd[i0][i1] = Vec3f.t_distance( lcv0[i0], lcv1[i1] )
                #mfn[i0][i1] = 1.0 - Vec3f.t_dot( lcn0[i0], lcn1[i1] )
            DebugWriter.end()
            
            
            opts['optimizations'] = {
                'stats0': stats0, 'stats1': stats1,
                'mvd': mvd, 'mvn': mvn, 'mfd': mfd, 'mfn': mfn,
                'li_va0': li_va0, 'li_va1': li_va1, 'li_fa0': li_fa0, 'li_fa1': li_fa1,
                'li_fsi_v0': [ li_fa0[i_f]['si_v'] for i_f,_ in m0.elf() ],
                'li_fsi_v1': [ li_fa1[i_f]['si_v'] for i_f,_ in m1.elf() ],
                'lcv0': lcv0, 'lcn0': lcn0, 'lcv1': lcv1, 'lcn1': lcn1,
                }
        
        return opts['optimizations']
    
    #def compute_cost_noopt( self, costs ):
        #ci = self
        
        ## addition / deletion costs
        #cvadd,cvdel = costs['cvadd'],costs['cvdel']
        #cfadd,cfdel = costs['cfadd'],costs['cfdel']
        
        ## geometric costs
        #cvdist,cvnorm = costs['cvdist'],costs['cvnorm']
        #cfdist,cfnorm = costs['cfdist'],costs['cfnorm']
        
        ## adjacency costs
        #cfvunk,cfvmis = costs['cfvunk'],costs['cfvmis']
        #cfmism,cfmunk = costs['cfmism'],costs['cfmunk']
        
        ## opts
        #la_v0,la_v1 = ci.la_v0,ci.la_v1
        #la_f0,la_f1 = ci.la_f0,ci.la_f1
        #lv0,lv1 = ci.lv0,ci.lv1
        #lf0,lf1 = ci.lf0,ci.lf1
        #mv01,mv10 = ci.mv01,ci.mv10
        #mf01,mf10 = ci.mf01,ci.mf10
        
        #cost = 0.0
        
        ## calc costs for adding and deleting geometry
        #cost += cvdel * float( ci.cv0 - len(mv01) )
        #cost += cvadd * float( ci.cv1 - len(mv10) )
        #cost += cfdel * float( ci.cf0 - len(mf01) )
        #cost += cfadd * float( ci.cf1 - len(mf10) )
        
        ## calc costs for substituting geometry
        
        #for iv0,iv1 in ci.mv01.iteritems():
            #v0,v1 = lv0[iv0],lv1[iv1]
            
            #cost += cvdist * Vec3f.t_distance( v0.p, v1.p )
            #cost += cvnorm * ( 1.0 - Vec3f.t_dot( v0.n, v1.n ) )
        
        #for if0,if1 in ci.mf01.iteritems():
            #f0,f1 = lf0[if0],lf1[if1]
            
            #liv0 = ci.m0._opt_get_f_li_v( f0 )
            #liv1 = ci.m1._opt_get_f_li_v( f1 )
            #lv0,n0 = ci.m0.opt_get_f_lv( f0 ), ci.m0.opt_face_normal( f0 )
            #lv1,n1 = ci.m1.opt_get_f_lv( f1 ), ci.m1.opt_face_normal( f1 )
            #si_af0 = ci.la_f0[if0]['si_f']
            #si_af1 = ci.la_f1[if1]['si_f']
            
            #cost += cfdist * Vec3f.t_distance( Vec3f.t_average( [ v0.p for v0 in lv0 ] ), Vec3f.t_average( [ v1.p for v1 in lv1 ] ) )
            #cost += cfnorm * ( 1.0 - Vec3f.t_dot( n0, n1 ) )
            
            #cost += cfvunk * float(sum( iv0 not in mv01 for iv0 in liv0 ) + sum( iv1 not in mv10 for iv1 in liv1 ))
            #cost += cfvmis * float(
                #len(liv0) + len(liv1) -
                #sum((iv0 in mv01 and mv01[iv0] in liv1) for iv0 in liv0) -
                #sum((iv1 in mv10 and mv10[iv1] in liv0) for iv1 in liv1)
                #)
            #cost += cfmunk * float(
                #len( [iaf0 for iaf0 in si_af0 if iaf0 not in mf01] )
                #)
            #cost += cfmism * float(
                #len( [iaf0 for iaf0 in si_af0 if iaf0 in mf01 and mf01[iaf0] not in si_af1] )
                #)
        
        #return cost
    
    
    ####################################################################################################################
    # Enumeration functions
    ####################################################################################################################
    
    
    # enumerate verts
    def elv0( self ):
        for i,v in enumerate(self.lv0):
            yield (i,v)
    def elv1( self ):
        for i,v in enumerate(self.lv1):
            yield (i,v)
    
    # enumerate faces
    def elf0( self ):
        for i,f in enumerate(self.lf0):
            yield (i,f)
    def elf1( self ):
        for i,f in enumerate(self.lf1):
            yield (i,f)
    
    # NOTES:
    #   hungarian cost matrix
    #        m    n
    #     n  Q2 | Q1
    #        ---+---
    #     m  Q3 | Q4
    #   Q1 (nxn). r -> @: rem r
    #   Q2 (nxm). r -> c: sub c for r
    #   Q3 (mxm). @ -> c: add c
    #   Q4 (mxn): @ -> @: do nothing

    # enumerating generators: vert quadrants
    def evq1( self ):
        for i,v0 in enumerate(self.lv0):
            for j in xrange(self.cv0):
                yield (i,v0, j+self.cv1,None)
    def evq2( self ):
        for i,v0 in enumerate(self.lv0):
            for j,v1 in enumerate(self.lv1):
                yield (i,v0, j,v1)
    def evq3( self ):
        for i in xrange(self.cv1):
            for j,v1 in enumerate(self.lv1):
                yield (i+self.cv0,None, j,v1)
    def evq4( self ):
        for i in xrange(self.cv1):
            for j in xrange(self.cv0):
                yield (i+self.cv0,None, j+self.cv1,None)
    
    # enumerating generators face quadrants
    def efq1( self ):
        for i,f0 in enumerate(self.lf0):
            for j in xrange(self.cf0):
                yield (i,f0, j+self.cf1,None)
    def efq2( self ):
        for i,f0 in enumerate(self.lf0):
            for j,f1 in enumerate(self.lf1):
                yield (i,f0, j,f1)
    def efq3( self ):
        for i in xrange(self.cf1):
            for j,f1 in enumerate(self.lf1):
                yield (i+self.cf0,None, j,f1)
    def efq4( self ):
        for i in xrange(self.cf1):
            for j in xrange(self.cf0):
                yield (i+self.cf0,None, j+self.cf1,None)
    
    
    
    ####################################################################################################################
    # Assert Validation and Consistency
    ####################################################################################################################
    
    
    # validate that mappings are consistent ( v0->v1 iff v1->v0, etc. )
    def validate( self ):
        for iv0,iv1 in self.mv01.iteritems():
            assert iv1 in self.mv10
            assert self.mv10[iv1] == iv0
        for iv1,iv0 in self.mv10.iteritems():
            assert iv0 in self.mv01
            assert self.mv01[iv0] == iv1
        for if0,if1 in self.mf01.iteritems():
            assert if1 in self.mf10
            assert self.mf10[if1] == if0
        for if1,if0 in self.mf10.iteritems():
            assert if0 in self.mf01
            assert self.mf01[if0] == if1
    
    
    
    ####################################################################################################################
    # Add/Del Correspondence Functions
    ####################################################################################################################
    
    
    
    # add vert correspondence
    def acv( self, iv0, iv1 ):
        assert iv0 not in self.mv01, '(%i,%i), iv0 already assigned to %i' % (iv0,iv1,self.mv01[iv0])
        assert iv1 not in self.mv10, '(%i,%i), iv1 already assigned to %i' % (iv0,iv1,self.mv10[iv1])
        self.mv01[iv0],self.mv10[iv1] = iv1,iv0
        return self
    
    # del vert correspondence
    def dcv0( self, iv0 ):
        if iv0 not in self.mv01: return
        iv1 = self.mv01[iv0]
        del self.mv01[iv0]
        del self.mv10[iv1]
        return self
    def dcv1( self, iv1 ):
        if iv1 not in self.mv10: return
        iv0 = self.mv10[iv1]
        del self.mv01[iv0]
        del self.mv10[iv1]
        return self
    def dcvall( self ):
        self.mv01,self.mv10 = {},{}
    
    # add face correspondence
    def acf( self, if0, if1, increaseorder=True ):
        if if0 in self.mf01:
            assert self.mf01[if0] == if1, 'adding face %i -> %i, but already -> %i' % (if0,if1,self.mf01[if0])
            return
        elif if1 in self.mf10:
            assert self.mf10[if1] == if0, 'adding face %i -> %i, but already -> %i' % (if0,if1,self.mf10[if1])
            return
        #assert if0 not in self.mf01
        #assert if1 not in self.mf10
        self.mf01[if0],self.mf10[if1] = if1,if0
        
        self.dmf01[if0] = self.norder
        if increaseorder:
            self.norder += 1
        
        return self
    
    # del face correspondence
    def dcf0( self, if0 ):
        if if0 not in self.mf01: return
        if1 = self.mf01[if0]
        del self.mf01[if0]
        del self.mf10[if1]
        del self.dmf01[if0]
        return self
    def dcf1( self, if1 ):
        if if1 not in self.mf10: return
        if0 = self.mf10[if1]
        del self.mf01[if0]
        del self.mf10[if1]
        del self.dmf01[if0]
        return self
    def dcfall( self ):
        self.mf01,self.mf10 = {},{}
        self.dmf01 = dict()
        self.norder = 0
    
    
    
    ####################################################################################################################
    #
    ####################################################################################################################
    
    # returns a mesh that's been (re-)labeled to correspond with mesh0
    def get_corresponding_mesh1( self, allow_face_sub=None ):
        if allow_face_sub is None: allow_face_sub = True
        
        mesh0,mesh1 = self.m0,self.m1
        mv01,mv10 = self.mv01,self.mv10
        mf01,mf10 = self.mf01,self.mf10
        
        DebugWriter.start( 'getting corresponding mesh' )
        t = time.time()
        
        if not allow_face_sub:
            CB_Logic.remove_face_matches_with_mismatched_verts( {'ci':self} )
            CB_Logic.remove_vert_matching_all_unmatched_faces( {'ci':self} )
            allow_face_sub=True
        
        mesh = Mesh()
        
        mesh.lv = [ Vertex( u_v=mesh0.lv[mv10[i_v]].u_v, p=v.p, n=v.n, c=v.c ) if i_v in mv10 else v.copy() for i_v,v in mesh1.elv() ]
        if allow_face_sub:
            mesh.lf = [
                Face(
                    u_f=(mesh0.lf[mf10[i_f]].u_f if i_f in mf10 else None),
                    lu_v=[ mesh.lv[i_v].u_v for i_v in mesh1._opt_get_f_li_v(f) ]
                    ) for i_f,f in mesh1.elf()
                ]
        else:
            lsufv = [ set( mesh.lv[i_v].u_v for i_v in mesh1._opt_get_f_li_v(f) ) for i_f,f in mesh1.elf() ]
            fs = [ i_f in mf10 and len(lsufv[i_f].symmetric_difference(set(mesh0.lf[mf10[i_f]].lu_v))) == 0 for i_f,f in mesh1.elf() ]
            mesh.lf = [
                Face(
                    u_f  = ( mesh0.lf[mf10[i_f]].u_f if fs[i_f] else None ),
                    lu_v = [ mesh.lv[i_v].u_v for i_v in mesh1._opt_get_f_li_v(f) ]
                    ) for i_f,f in mesh1.elf()
                ]
        
        mesh.trim()
        
        DebugWriter.end()
        
        return mesh
    
    
    


import bpy



"""
this script will:
    delete any objects in layers 1 and 2
    copy the object from layer0 to layer1 and layer2
"""





def layers( li_layer ):
    return [ (i in li_layer) for i in range(20) ]

def viewlayers( li_layer ):
    bpy.context.scene.layers = layers( li_layer )

def selectall():
    bpy.ops.object.select_all(action='SELECT')
def deselectall():
    bpy.ops.object.select_all(action='DESELECT')


if bpy.context.active_object:
    bpy.ops.object.mode_set(mode='OBJECT')

# make sure that layer 0 has only one object
viewlayers([0])
selectall()
assert len(bpy.context.selected_objects) == 1, 'must only have 1 object'
bpy.context.selected_objects[0].name = 'ver0'
deselectall()

# delete all objects in layers 1 and 2
viewlayers([1,2])
selectall()
bpy.ops.object.delete()


# copy object from layer 0 to layers 1 and 2
viewlayers([0])

selectall()
bpy.ops.object.duplicate()
o = bpy.context.selected_objects[0]
o.layers = layers([1])
o.name = 'ver1'

selectall()
bpy.ops.object.duplicate()
o = bpy.context.selected_objects[0]
o.layers = layers([2])
o.name = 'ver2'


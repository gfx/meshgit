#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
exports any step??????.blend to a near .json file, unless the .json file already exists

NOTE: designed to be run concurrently with a recording session, break by ctrl-c
"""

import os, glob, sys, subprocess, time, select
import config

prevfiles = []

def isdata():
    # return true (?) if character is ready to be read from stdin
    return select.select([sys.stdin], [], [], 0) == ([sys.stdin], [], [])

while( True ):
    
    files = glob.glob( config.allstepfiles )
    
    newfiles = list(set(files) - set(prevfiles))
    newfiles.sort()
    
    if( isdata() ):
        c = sys.stdin.read(1)
        print "read %c\n" % c
        if c == '\x1b': # escape key
            break
    
    if( len(newfiles) == 0 ):
        time.sleep( 1 )
        continue
    
    print( "processing %d files" % (len(newfiles)) )
    
    curcount = 0
    for infile in newfiles:
        
        (pathname, ext) = os.path.splitext(infile)
        jsonfile = "%s.json" % (pathname)
        if( os.path.isfile( jsonfile ) ):
            print( ".json exists for %s" % (infile) )
            curcount += 1
            continue
        
        print( "processing %s (%d/%d)" % (infile, curcount, len(newfiles)) )
        
        #p = subprocess.Popen( "%s %s --python finalize_blender.py -b > /dev/null 2>&1" % (blender, infile), shell = True ) # -d
        #p = subprocess.Popen( "./blender %s --python finalize_blender.py -b" % (infile), shell = True ) # -d
        #sts = os.waitpid(p.pid, 0)[1]
        
        args = [ config.blenderexe, infile, config.blenderopts_export_json2 ]
        config.exec_wait( args )
        
        if not os.path.isfile( jsonfile ):
            print( "seems something is broke! no .json file after execution!" )
            print( "exec:", config.flatten( args ) )
            quit()
        
        curcount += 1
    
    print( "end of list..." )
    prevfiles = files


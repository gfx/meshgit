#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
exports any step??????.blend to a near .json file, unless the .json file already exists

NOTE: designed to be run concurrently with a recording session
"""

import os, glob, sys, subprocess, time, select
import config, miscfuncs

blendfns = [ l.split(': ')[0] for l in open( "steps.txt", "r" ) ]
count = len(blendfns)

for i,blendfn in enumerate(blendfns):
    (fn,ext) = os.path.splitext( blendfn )
    jsonfn = "%s.json" % fn
    
    if os.path.isfile( jsonfn ): continue
    
    print( "%06d / %06d: exporting %s to %s" % (i,count,blendfn,jsonfn) )
    
    args = [ config.blenderexe, blendfn, config.blenderopts_export_json2 ]
    blenderout = miscfuncs.exec_wait( args )
    if not os.path.isfile( jsonfn ):
        print( "" )
        print( "" )
        print( "********************************************************" )
        print( "seems something is broke! no .json file after execution!" )
        print( "" )
        print( "executed cmd:" )
        print( miscfuncs.flatten( args ) )
        print( "" )
        print( "blender output:" )
        print( blenderout[0] )
        sys.exit( -1 )
    

#!/usr/bin/python
# -*- coding: utf-8 -*-

"""
a cheap way to compile all step??????.json files into a single steps_all.json
"""

import os, glob, sys, subprocess, time, select, json, re
from operator import itemgetter
import config, scenefuncs, miscfuncs

def append_name_ref( scene, append ):
    for k0,v0 in scene.iteritems():
        if k0 in ['name','meta_data']:
            continue
        for v1 in v0:
            for k2,v2 in v1.iteritems():
                if k2.endswith( "_ref" ) or k2 == "name":
                    v1[k2] = "%s%s" % (v2,append)

scene = {}

newfiles = glob.glob( "step??????.json" )
newfiles.sort()

print( "len = %d" % len(newfiles) )

for i,infile in enumerate(newfiles):
    #try:
        sscene = miscfuncs.file_to_json( infile )
        print( "%d %s" % (i,infile) )
        
        append_name_ref( sscene, "_%06d" % i )
        
        scenefuncs.add_to_scene( scene, sscene )
        
    #except:
    #    print( "something happened" )
    #    pass
    
miscfuncs.json_to_file( "steps_all.json", scene )


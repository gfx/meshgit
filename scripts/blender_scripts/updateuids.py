import json, bpy, re, time, os, sys, hashlib

# NOTE: need to update to handle more objects!
if len(bpy.data.objects) != 1:
    print( "UNHANDLED NUMBER OF OBJECTS!  ONLY WORKS WITH 1 OBJECT" )
    bpy.ops.wm.quit_blender()


optranslate = {
    "initial":                                    "meta.initial",
    "bpy.ops.object.editmode_toggle":             "meta.mode.edit",
    "bpy.ops.sculpt.sculptmode_toggle":           "meta.mode.sculpt",
    "bpy.ops.paint.weight_paint_toggle":          "meta.mode.weight_paint",
    
    "bpy.ops.ed.undo":                            "undo.undo",
    "bpy.ops.ed.redo":                            "undo.redo",
    
    "bpy.ops.mesh.edgering_select":               "select.edgering",
    "bpy.ops.mesh.loop_select":                   "select.loop",
    "bpy.ops.mesh.select_all":                    "select.toggle_all",
    "bpy.ops.mesh.select_inverse":                "select.inverse",
    "bpy.ops.mesh.select_less":                   "select.less",
    "bpy.ops.mesh.select_more":                   "select.more",
    "bpy.ops.mesh.select_shortest_path":          "select.shortest_path",
    "bpy.ops.view3d.select":                      "select.select",
    "bpy.ops.view3d.select_border":               "select.border",
    "bpy.ops.view3d.select_circle":               "select.circle",
    "bpy.ops.view3d.select_lasso":                "select.lasso",
    "bpy.ops.mesh.select_linked":                 "select.linked",

    "bpy.ops.object.select_all":                  "select.all_objects",
    
    "bpy.ops.object.modifier_add":                "modifier.add",
    "bpy.ops.object.modifier_apply":              "modifier.apply",
    "bpy.ops.object.modifier_copy":               "modifier.copy",
    "bpy.ops.object.modifier_move_down":          "modifier.move_down",
    "bpy.ops.object.modifier_move_up":            "modifier.move_up",
    "bpy.ops.modifier.toggle_mirror_x":           "modifier.mirror.toggle_x",
    "bpy.ops.modifier.toggle_mirror_y":           "modifier.mirror.toggle_y",
    "bpy.ops.modifier.toggle_mirror_z":           "modifier.mirror.toggle_z",
    "bpy.ops.modifier.set_merge_limit":           "modifier.mirror.set_merge_limit",
    "bpy.ops.object.modifier_remove":             "modifier.remove",
    
    "bpy.ops.transform.trackball":                "transform.trackball",
    "bpy.ops.transform.translate":                "transform.translate",
    "bpy.ops.transform.rotate":                   "transform.rotate",
    "bpy.ops.transform.resize":                   "transform.resize",
    "bpy.ops.transform.rotate":                   "transform.rotate",
    "bpy.ops.transform.edge_slide":               "transform.edge_slide",
    "bpy.ops.transform.set_x":                    "transform.set_x",
    "bpy.ops.transform.set_y":                    "transform.set_y",
    "bpy.ops.transform.set_z":                    "transform.set_z",
    "bpy.ops.transform.tosphere":                 "transform.to_sphere",
    "bpy.ops.mesh.normals_make_consistent":       "transform.make_normals_consistent",
    "bpy.ops.transform.shrink_fatten":            "transform.scale_along_normals",
    "bpy.ops.transform.mirror":                   "transform.mirror",
    "bpy.ops.view3d.snap_selected_to_cursor":     "transform.snap_to_cursor",
    
    "bpy.ops.mesh.primitive_circle_add":          "topo.add.circle",
    "bpy.ops.mesh.primitive_cone_add":            "topo.add.cone",
    "bpy.ops.mesh.primitive_cube_add":            "topo.add.cube",
    "bpy.ops.mesh.primitive_cylinder_add":        "topo.add.cylinder",
    "bpy.ops.mesh.primitive_grid_add":            "topo.add.grid",
    "bpy.ops.mesh.primitive_ico_sphere_add":      "topo.add.sphere.ico",
    "bpy.ops.mesh.primitive_uv_sphere_add":       "topo.add.sphere.uv",
    "bpy.ops.mesh.primitive_monkey_add":          "topo.add.monkey",
    "bpy.ops.mesh.primitive_plane_add":           "topo.add.plane",
    "bpy.ops.mesh.primitive_torus_add":           "topo.add.torus",
    
    "bpy.ops.mesh.edge_face_add":                 "topo.add.edge_face",
    "bpy.ops.mesh.fill":                          "topo.add.fill",
    "bpy.ops.mesh.tris_convert_to_quads":         "topo.convert.tris_to_quads",
    "bpy.ops.mesh.quads_convert_to_tris":         "topo.convert.quads_to_tris",
    #"bpy.ops.mesh.delete":                        "topo.delete.selected",
    "bpy.ops.mesh.delete_all":                    "topo.delete.all",
    "bpy.ops.mesh.delete_vert":                   "topo.delete.vert",
    "bpy.ops.mesh.delete_edge":                   "topo.delete.edge",
    "bpy.ops.mesh.delete_face":                   "topo.delete.face",
    "bpy.ops.object.delete":                      "topo.delete.object",
    "bpy.ops.mesh.duplicate":                     "topo.duplicate",
    "bpy.ops.mesh.duplicate_move":                "topo.duplicate",
    "bpy.ops.mesh.dupli_extrude_cursor":          "topo.duplicate",
    "bpy.ops.object.duplicate":                   "topo.duplicate_object",
    "bpy.ops.object.duplicate_move":              "topo.duplicate_object",
    "bpy.ops.mesh.edge_flip":                     "topo.edge_flip",
    "bpy.ops.mesh.extrude":                       "topo.extrude",
    "bpy.ops.mesh.extrude_edges_move":            "topo.extrude",
    "bpy.ops.mesh.extrude_faces_move":            "topo.extrude",
    "bpy.ops.mesh.extrude_region_move":           "topo.extrude",
    "bpy.ops.mesh.extrude_vertices_move":         "topo.extrude",
    "bpy.ops.mesh.spin":                          "topo.spin",
    "bpy.ops.mesh.loopcut":                       "topo.loopcut",
    "bpy.ops.mesh.loopcut_slide":                 "topo.loopcut",
    "bpy.ops.mesh.merge":                         "topo.merge",
    "bpy.ops.object.join":                        "topo.merge_object",
    "bpy.ops.mesh.remove_doubles":                "topo.merge_doubles",
    "bpy.ops.mesh.knife_cut":                     "topo.knife_cut",
    "bpy.ops.mesh.rip_move":                      "topo.rip",
    "bpy.ops.mesh.solidify":                      "topo.solidify",
    "bpy.ops.mesh.split":                         "topo.split",
    "bpy.ops.mesh.subdivide":                     "topo.subdivide",
    
    "redo_op":                                    "topo.op.redo",
    }


def getmeshinfo( mesh ):
    # get selections
    vs,es,fs = [ [ item.select and 1 or 0 for item in items ] for items in [mesh.vertices,mesh.edges,mesh.faces] ]
    vc = [ list(v.co) for v in mesh.vertices ]
    ei = [ list(e.vertices) for e in mesh.edges ]
    fi = [ list(f.vertices) for f in mesh.faces ]
    
    for f in mesh.faces:
        if not f.select: continue
        for i_e,e in enumerate(mesh.edges):
            if e.vertices[0] in f.vertices and e.vertices[1] in f.vertices:
                es[i_e] = 1
    
    meta = { 'vsel':vs, 'esel':es, 'fsel':fs, 'vco':vc, 'eind':ei, 'find':fi, 'vcnt':len(vs), 'ecnt':len(es), 'fcnt':len(fs)  }
    return meta

def calchash( meta ):
    #keys = [ k for k in meta.keys() if not k.startswith('_') ]
    keys = ['vcnt','vsel','vco','ecnt','esel','eind','fcnt','fsel','find'] # 'vuid','euid','fuid']
    md5 = hashlib.md5()
    for k in keys: md5.update( bytes( str( meta[k] ), 'UTF-8' ) )
    return md5.hexdigest()

def getuid( metadata, uidtype, uidfrom ):
    uid = "%s_%d" % (metadata['rev'],metadata['nuid'])
    if uidfrom == -1: uidfrom = "%s_-1" % metadata['rev']
    metadata['nuid'] += 1
    metadata['uid_%s'%uidtype] += [[uidfrom,uid]]
    metadata['uid_child_parent'][uid] = uidfrom
    if uidfrom != "%s_-1" % metadata['rev']:
        if not uidfrom in metadata['uid_parent_children']:
            metadata['uid_parent_children'][uidfrom] = [uid]
        else:
            metadata['uid_parent_children'][uidfrom] += [uid]
    return uid

# returns border,inland selected edges
# edge is inland if it has two selected adjacent faces
def separate_esel( meta ):
    ecnt,eind,esel = meta['ecnt'],meta['eind'],meta['esel']
    fsel = meta['fsel']
    ebsel,eisel = [0]*ecnt,[0]*ecnt
    for ie,(ev0,ev1) in enumerate(eind):
        if not esel[ie]: continue
        faces = get_lif_iv_iv( meta, ev0, ev1 )
        if len(faces) == 2 and fsel[faces[0]] and fsel[faces[1]]:
            eisel[ie] = 1
        else:
            ebsel[ie] = 1
    return (ebsel,eisel)

# returns border,inland selected verts
# vert is inland if all adjacent edges are inland
def separate_vsel( meta, ebsel, eisel ):
    # returns vert selection separated into border, inland selections
    vcnt,vsel = meta['vcnt'],meta['vsel']
    ecnt,eind,esel = meta['ecnt'],meta['eind'],meta['esel']
    fcnt,find,fsel = meta['fcnt'],meta['find'],meta['fsel']
    
    vbsel,visel = [0]*vcnt,[0]*vcnt
    
    for i in range(vcnt):
        if not vsel[i]: continue
        edges = get_lie_iv( meta, i )
        inland = 1
        for ie in edges:
            if not eisel[ie]:
                inland = 0
                break
        if inland: visel[i] = 1
        else: vbsel[i] = 1
    return (vbsel,visel)

# returns indices of edges adjacent to vertex
def get_lie_iv( meta, vifrom ):
    found = []
    for i,e in enumerate(meta['eind']):
        if e[0] == vifrom or e[1] == vifrom: found += [i]
    return found

def get_lie_uv( meta, uv ):
    found = []
    vuid = meta['vuid']
    for i,e in enumerate(meta['eind']):
        if vuid[e[0]] == uv or vuid[e[1]] == uv: found += [i]
    return found

# returns index of first edge found between two vertices given by uids
def get_fie_uv_uv( meta, uidv0, uidv1 ):
    vuids = meta['vuid']
    for i,e in enumerate(meta['eind']):
        if vuids[e[0]] == uidv0 and vuids[e[1]] == uidv1: return (i,e[0],e[1])
        if vuids[e[1]] == uidv0 and vuids[e[0]] == uidv0: return (i,e[1],e[0])
    print( "no edge found uv0=%d, uv1=%d" % (uidv0,uidv1) )
    return None

# NOTE: NOT USED ANYMORE.... NEEDED?
def get_fie_iv_liv( meta, vifrom, vitolist ):
    for i,e in enumerate(meta['eind']):
        if e[0] == vifrom and e[1] in vitolist: return (i,e[1])
        if e[1] == vifrom and e[0] in vitolist: return (i,e[0])
    return None

# returns index of first edge found between two vertices, given by index and uid
def get_fie_iv_uv( meta, ivfrom, uidvto ):
    vuids = meta['vuid']
    ec = []
    for i,e in enumerate(meta['eind']):
        if e[0] == ivfrom:
            if vuids[e[1]] == uidvto: return (i,e[1])
            ec += [vuids[e[1]]]
        if e[1] == ivfrom:
            if vuids[e[0]] == uidvto: return (i,e[0])
            ec += [vuids[e[0]]]
    print( "no edge found iv=%d -- uid=%d out of %s" % (ivfrom,uidvto,str(ec)) )
    return None

def get_fie_iv_iv( meta, iv0, iv1 ):
    for i_e,e in enumerate(meta['eind']):
        if e[0] == iv0 and e[1] == iv1: return i_e
        if e[0] == iv1 and e[1] == iv0: return i_e
    print( "no edge found iv0=%d, iv1=%d" % (iv0,iv1) )
    return None

# returns inds of edges of a face
def get_lie_if( meta, i_f ):
    found = []
    f = meta['find'][i_f]
    for i_e,e in enumerate(meta['eind']):
        if e[0] in f and e[1] in f: found += [i_e]
    return found

# returns indices of faces that have two vertices, given by indices
def get_lif_iv_iv( meta, vind0, vind1 ):
    found = []
    for i,f in enumerate(meta['find']):
        if vind0 in f and vind1 in f: found += [i]
    return found

def get_fif_luv( meta, luv ):
    vuid = meta['vuid']
    for i,f_liv in enumerate(meta['find']):
        f_luv = [ vuid[iv] for iv in f_liv ]
        if all([ uv in f_luv for uv in luv ]): return i
    print( "no face found luv=%s" % str(luv) )
    return None

def get_lif_luv( meta, luv ):
    found = []
    vuid = meta['vuid']
    for i,f_liv in enumerate(meta['find']):
        f_luv = [ vuid[iv] for iv in f_liv ]
        if all([ uv in f_luv for uv in luv ]): found += [i]
    print( "no face found luv=%s" % str(luv) )
    return found

def get_fie_luv( meta, luv ):
    vuid = meta['vuid']
    for i,e_liv in enumerate(meta['eind']):
        e_luv = [ vuid[iv] for iv in e_liv ]
        if all([ uv in e_luv for uv in luv ]): return i
    print( "no edge found luv=%s" % str(luv) )
    return None

# returns indices of faces that has a vert specified by index
def get_lif_iv( meta, iv ):
    return [ i for i,f in enumerate(meta['find']) if iv in f ]

# return index of first face that has three verts specified by indices
def get_fif_iv_iv_iv( meta, i_v0, i_v1, i_v2 ):
    for i_f,f in enumerate(meta['find']):
        if i_v0 in f and i_v1 in f and i_v2 in f: return i_f
    print( "no face found i_v0=%d, i_v1=%d, i_v2=%d" % (i_v0,i_v1,i_v2) )
    return None

# returns index of first face that has two vertices (given by inds) with specified uid
def get_fif_iv_iv_uf( meta, i_v0, i_v1, u_f ):
    for i_f in get_lif_iv_iv( meta, i_v0, i_v1 ):
        if meta['fuid'][i_f] == u_f: return i_f
    print( "no face found i_v0=%d, i_v1=%d, u_f=%d" % (i_v0,i_v1,u_f) )
    return None

# get index of vert by uid
def get_iv_uv( meta, vuid ):
    return meta['vuid'].index(vuid)

# get index of edge by uid
def get_ie_ue( meta, euid ):
    return meta['euid'].index(euid)

# get index of face by uid
def get_if_uf( metadata, meta, fuid ):
    while not fuid in meta['fuid']:
        if fuid == -1: return None
        fuid = getparent_u_u( metadata, fuid )
    return meta['fuid'].index(fuid)
    

# get indices of verts with parent uid as specified
def getchildren_liv_uid( metadata, meta, uid ):
    uidt = "%d"%uid
    if not uidt in metadata['uid_parent_children']:
        print( "%d not in parent->children list" % uid )
        return []
    return [ meta['vuid'].index(uchild) for uchild in metadata['uid_parent_children'][uidt] if uchild in meta['vuid'] ]

def getchildren_lif_uid( metadata, meta, uid ):
    uidt = "%d"%uid
    if not uid in metadata['uid_parent_children']:
        print( "%d not in parent->children list" % uid )
        return []
    return [ meta['fuid'].index(uchild) for uchild in metadata['uid_parent_children'][uidt] if uchild in meta['fuid'] ]

def getparent_u_u( metadata, u ):
    if not u in metadata['uid_child_parent']:
        print( "%d not in child->parent dict" % u )
        return None
    return metadata['uid_child_parent'][u]

################################################################################

# values passed by blender instrumentation
# only oppre has actual data; isave and filename are only used to pass back info to instrumentation
isave,filename,oppre = values


# translate op to something human-readable
_,op,opparams,_ = re.split( "([^(]+)\((.*)\)", str(oppre,'UTF-8') )
if op in optranslate:   op = optranslate[op]
else:                   print( "untranslated op: %s" % op )
if op == 'meta.initial': opparams = "filepath='%s'" % bpy.data.filepath
opparams = opparams.replace('"',"'")

################################################################################

# create metadata if it doesn't exist, yet
if not 'meta' in bpy.data.texts:
    newmetadata = { 'rev': 'A', 'count': 0, 'list': [], 'nuid': 0, 'uid_v': [], 'uid_e': [], 'uid_f': [], 'uid_child_parent': {}, 'uid_parent_children': {} }
    bpy.ops.text.new()
    newtextblock = bpy.data.texts[-1]
    newtextblock.name = 'meta'
    newtextblock.from_string( json.dumps( newmetadata ) )

# load current running metadata
metadata = json.loads( bpy.data.texts['meta'].as_string() )

isnapshot = metadata['count']
metalist = metadata['list']

mesh = bpy.data.objects[0].data
cmeta = getmeshinfo( mesh )
cmeta['_op'] = op
cmeta['_params'] = opparams
cmeta['_time'] = time.time()
cmeta['_i'] = isnapshot
cmeta['_hash'] = calchash( cmeta )


m = MeshProvenance( mesh )


# NOTE: this will be different for undo or redoing an op (changing op params)
if len(metalist):   pmeta = metalist[-1]
else:               pmeta = None


# initialize the uids
if pmeta:
    # copy from previous snapshot
    cmeta['vuid'] = list(pmeta['vuid'])
    cmeta['euid'] = list(pmeta['euid'])
    cmeta['fuid'] = list(pmeta['fuid'])
else:
    # all new geometry (very first snapshot)
    cmeta['vuid'] = [ getuid(metadata,'v',-1) for v in mesh.vertices ]
    cmeta['euid'] = [ getuid(metadata,'e',-1) for e in mesh.edges ]
    cmeta['fuid'] = [ getuid(metadata,'f',-1) for f in mesh.faces ]


# create "shortcuts"
if pmeta:
    pvcnt,pvsel,pvuid = [ pmeta[k] for k in ['vcnt','vsel','vuid'] ]
    pecnt,pesel,peuid,peind = [ pmeta[k] for k in ['ecnt','esel','euid','eind'] ]
    pfcnt,pfsel,pfuid,pfind = [ pmeta[k] for k in ['fcnt','fsel','fuid','find'] ]

cvcnt,cvsel,cvuid = [ cmeta[k] for k in ['vcnt','vsel','vuid'] ]
cecnt,ceuid,ceind = [ cmeta[k] for k in ['ecnt','euid','eind'] ]
cfcnt,cfuid,cfind = [ cmeta[k] for k in ['fcnt','fuid','find'] ]
cesel = cmeta['esel']
cfsel = cmeta['fsel']

# update initial uids
if bpy.context.mode == 'EDIT_MESH':
    
    if op == "topo.delete.vert" or op == "bpy.ops.mesh.delete" and opparams == "type='VERT'":
        j = 0
        for i in range(pvcnt):
            if pvsel[i]: cvuid.pop(j)
            else: j += 1
        j = 0
        for i in range(pecnt):
            if any([ pvsel[k] for k in peind[i] ]): ceuid.pop(j)
            else: j += 1
        j = 0
        for i in range(pfcnt):
            if any([ pvsel[k] for k in pfind[i] ]): cfuid.pop(j)
            else: j += 1
    
    elif op.startswith( "topo.extrude" ):
        # get border and inland selections
        pebsel,peisel = separate_esel( pmeta )
        cebsel,ceisel = separate_esel( cmeta )
        pvbsel,pvisel = separate_vsel( pmeta, pebsel, peisel )
        cvbsel,cvisel = separate_vsel( cmeta, cebsel, ceisel )
        
        plu_vbsel = [ pvuid[i] for i,b in enumerate(pvbsel) if b ]
        plu_ebsel = [ peuid[i] for i,b in enumerate(pebsel) if b ]
        plu_fisel = [ pfuid[i] for i,b in enumerate(pfsel) if b ]
        plu_fnsel = [ pfuid[i] for i,b in enumerate(pfsel) if not b ]
        print( "extrude( %s, %s )" % ( plu_fnsel, plu_fisel ) )
        
        #clu_extrude = [-1] * ( len(plu_vbsel) + len(plu_ebsel) + len(plu_fisel) + len(peisel) + len(pvisel) )
        update_str = []
        
        # "duplicate" all selected geometry
        for i in range(sum(pvsel)): cvuid.append(-1)
        for i in range(sum(pesel)): ceuid.append(-1)
        for i in range(sum(pfsel)): cfuid.append(-1)
        
        # remove inland geometry (all sel faces are inland)
        jv,je,jf = 0,0,0
        for i in range(pvcnt):
            if pvisel[i]:
                update_str += ["-(%s)" % pvuid[i]]
                cvuid.pop( jv )
            else: jv += 1
        for i in range(pecnt):
            if peisel[i]:
                update_str += ["-(%s)" % peuid[i]]
                ceuid.pop( je )
            else: je += 1
        for i in range(pfcnt):
            if pfsel[i]:
                update_str += ["-(%s)" % pfuid[i]]
                cfuid.pop( jf )
            else: jf += 1
        
        # add bridging edges and faces
        for i in range(sum(pvbsel)): ceuid.append(-1)                           # each border vert creates a new edge
        for i in range(sum(pebsel)): cfuid.append(-1)                           # each border edge creates a new face
        
        ####################
        # assign proper uids
        
        # assign new uids for bridging geometry
        # bridge: new border verts and new bridging edges
        for ip in range(pvcnt):
            if not pvbsel[ip]: continue
            #print( "pvuid[%d] = %d" % (ip,pvuid[ip]) )
            icfrom = get_iv_uv( cmeta, pvuid[ip] )
            #print( 'icfrom=%d, uid=%d' % (icfrom,cvuid[icfrom]) )
            eind,icto = get_fie_iv_uv( cmeta, icfrom, -1 )
            cvuid[icto] = getuid( metadata, 'v', cvuid[icfrom] )
            ceuid[eind] = getuid( metadata, 'e', cvuid[icfrom] )
            update_str += ["(%s,%s)" % (cvuid[icfrom],cvuid[icto]), "(%s,%s)" % (cvuid[icfrom],ceuid[eind])]
            
        # bridge: new border edges and new faces
        for pie in range(pecnt):
            if not pebsel[pie]: continue
            cie,civ0,civ1 = get_fie_uv_uv( cmeta, pvuid[peind[pie][0]], pvuid[peind[pie][1]] )
            ci_f = get_fif_iv_iv_uf( cmeta, civ0, civ1, -1 )
            cfuid[ci_f] = getuid( metadata, 'f', ceuid[cie] )
            ci_e = [ i_e for i_e in get_lie_if( cmeta, ci_f ) if cmeta['euid'][i_e] == -1 ][0]
            ceuid[ci_e] = getuid( metadata, 'e', ceuid[cie] )
            update_str += ["(%s,%s)" % (ceuid[cie],ceuid[ci_e]) ]
        
        # transfer uids for inland geometry
        unk_v,unk_e,unk_f = list(cvisel),list(ceisel),list(cfsel)
        pos_v,pos_e,pos_f = list(pvisel),list(peisel),list(pfsel)
        assert sum(unk_v) == sum(pos_v), "sanity check fail: vert counts differ, %i != %i" % (sum(unk_v),sum(pos_v))
        assert sum(unk_e) == sum(pos_e), "sanity check fail: edge counts differ, %i != %i" % (sum(unk_e),sum(pos_v))
        assert sum(unk_f) == sum(pos_f), "sanity check fail: face counts differ, %i != %i" % (sum(unk_f),sum(pos_f))
        
        # the following is a simple hack to fill in unknowns given knowns
        # this will not work correctly for every case (esp. for non-manifolds),
        # but it's good enough for simple modeling edits
        while any(unk_v) or any(unk_e) or any(unk_f):
            chg = 0
            
            # unknown edge with known verts
            for ci_e,cli_v in enumerate(ceind):
                if not unk_e[ci_e]: continue
                if unk_v[cli_v[0]] or unk_v[cli_v[1]]: continue
                clu_v = [ cvuid[ci_v] for ci_v in cli_v ]
                plu_v = list(clu_v)
                for i in range(len(plu_v)):
                    while plu_v[i] not in pvuid:
                        plu_v[i] = getparent_u_u( metadata, plu_v[i] )
                pi_e = get_fie_luv( pmeta, plu_v )
                if pi_e:
                    ceuid[ci_e] = getuid( metadata, 'e', peuid[pi_e] )
                    unk_e[ci_e],pos_e[pi_e] = 0,0
                    update_str += ["(%s,%s)" % (peuid[pi_e],ceuid[ci_e])]
                    chg = 1
            
            # unknown face with at least 3 known verts
            for ci_f,cli_v in enumerate(cfind):
                if not unk_f[ci_f]: continue
                cli_v = [ ci_v for ci_v in cli_v if not unk_v[ci_v] ]
                if len(cli_v) < 3: continue
                clu_v = [ cvuid[ci_v] for ci_v in cli_v ]
                plu_v = list(clu_v)
                for i in range(len(plu_v)):
                    while plu_v[i] not in pvuid:
                        plu_v[i] = getparent_u_u( metadata, plu_v[i] )
                pi_f = get_fif_luv( pmeta, plu_v )
                if pi_f:
                    cfuid[ci_f] = getuid( metadata, 'f', pfuid[pi_f] )
                    unk_f[ci_f],pos_f[pi_f] = 0,0
                    update_str += ["(%s,%s)" % (pfuid[pi_f],cfuid[ci_f])]
                    chg = 1
            
            # known face with 1 unknown vert
            for ci_f,cli_v in enumerate(cfind):
                if unk_f[ci_f]: continue
                cli_uv = [ ci_v for ci_v in cli_v if unk_v[ci_v] ]
                if len(cli_uv) != 1: continue
                pi_f = get_if_uf( metadata, pmeta, cfuid[ci_f] )
                pli_uv = [ pi_v for pi_v in pfind[pi_f] if pos_v[pi_v] ]
                assert len(pli_uv) == 1, "sanity check failed, pli_uv = %s" % str(pli_uv)
                ci_v,pi_v = cli_uv[0],pli_uv[0]
                cvuid[ci_v] = getuid( metadata, 'v', pvuid[pi_v] )
                unk_v[ci_v],pos_v[pi_v] = 0,0
                update_str += ["(%s,%s)" % (pvuid[pi_v],cvuid[ci_v])]
                chg = 1
            
            if not chg:
                print( "error: inf loop of updating with no change!" )
                print( [ i for i,b in enumerate(unk_v) if b ] )
                print( [ i for i,b in enumerate(unk_e) if b ] )
                print( [ i for i,b in enumerate(unk_f) if b ] )
                break
        
        print( "updates: %s" % (",".join(update_str)) )
    
    elif op == "topo.subdivide":
        # assumes a full edge ring is selected (there are no corners)
        
        plu_esel = [ peuid[i] for i,b in enumerate(pesel) if b ]
        print( "splitedge(%s)" % plu_esel )
        
        for i in range(sum(pesel)):
            cvuid.append(-1)
            ceuid.extend([-1,-1,-1])
            cfuid.extend([-1,-1])
        
        je,jf = 0,0
        for i_e in range(pecnt):
            if pesel[i_e]: ceuid.pop(je)
            else: je += 1
        for i_f in range(pfcnt):
            if sum( [ pvsel[pi_v] for pi_v in pfind[i_f] ] ) == 4: cfuid.pop(jf)
            else: jf += 1
        
        for ci_e,cli_v in enumerate(ceind):
            if not cesel[ci_e]: continue
            cli_f = get_lif_iv_iv( cmeta, cli_v[0], cli_v[1] )                  # should be two!
            for ci_f in cli_f:
                if cfuid[ci_f] != -1: continue                                  # already assigned
                f_cli_kv = [ i_v for i_v in cmeta['find'][ci_f] if cvuid[i_v] != -1 ]   # should be two!
                f_clu_kv = [ cvuid[i_v] for i_v in f_cli_kv ]
                pli_f = get_lif_luv( pmeta, f_clu_kv )                          # should be two!
                pi_f = [ i_f for i_f in pli_f if not pfuid[i_f] in cfuid ][0]
                cfuid[ci_f] = getuid( metadata, 'f', pfuid[pi_f] )
        
        for pi_e,pli_v in enumerate(peind):
            if not pesel[pi_e]: continue
            lu_v = [ pvuid[i_v] for i_v in pli_v ]
            cli_v = [ get_iv_uv( cmeta, u_v ) for u_v in lu_v ]
            clli_e = [ get_lie_iv( cmeta, ci_v ) for ci_v in cli_v ]
            ci_mv = None
            for ci_e0 in clli_e[0]:
                for ci_e1 in clli_e[1]:
                    if ceind[ci_e0][0] == ceind[ci_e1][0] or ceind[ci_e0][0] == ceind[ci_e1][1]:
                        ci_mv = ceind[ci_e0][0]
                        break
                    if ceind[ci_e0][1] == ceind[ci_e1][0] or ceind[ci_e0][1] == ceind[ci_e1][1]:
                        ci_mv = ceind[ci_e0][1]
                        break
                if ci_mv != None: break
            assert ci_mv != None, "sanity check failed: could not find midpoint"
            cvuid[ci_mv] = getuid( metadata, 'v', peuid[pi_e] )
            ceuid[ci_e0] = getuid( metadata, 'e', peuid[pi_e] )
            ceuid[ci_e1] = getuid( metadata, 'e', peuid[pi_e] )
        
        for ci_e,cli_v in enumerate(ceind):
            if ceuid[ci_e] != -1: continue
            cli_f = get_lif_iv_iv( cmeta, cli_v[0], cli_v[1] )
            ceuid[ci_e] = getuid( metadata, 'e', getparent_u_u( metadata, cfuid[cli_f[0]] ) )
    
    elif op == "topo.loopcut":
        assert False, "do not use loopcut!"
    
    else:
        pass


# accounting/sanity check!
if len(cvuid) != cmeta['vcnt'] or len(ceuid) != cmeta['ecnt'] or len(cfuid) != cmeta['fcnt']:
    print( "accounting error:" )
    if len(cvuid) != cmeta['vcnt']: print( "  vertex: %d -> %d != %d" % (pmeta['vcnt'],len(cvuid),cmeta['vcnt']) )
    if len(ceuid) != cmeta['ecnt']: print( "  edge: %d -> %d != %d" % (pmeta['ecnt'],len(ceuid),cmeta['ecnt']) )
    if len(cfuid) != cmeta['fcnt']: print( "  face: %d -> %d != %d" % (pmeta['fcnt'],len(cfuid),cmeta['fcnt']) )


# recalculate a hash of snapshot (in case something changed) to help with performing undo
cmeta['_hash'] = calchash( cmeta )

# save
if op == 'meta.initial' or (pmeta and cmeta['_hash'] != pmeta['_hash']):
    # first snapshot or something has changed
    
    metadata['count'] += 1
    metadata['list'].append( cmeta )
    
    while len(metadata['list']) >= 3: metadata['list'].pop(0)

    metadatajson = json.dumps( metadata, separators=[',',':'] )

    bpy.data.texts['meta'].from_string( metadatajson )
    #open( "stepmeta.json", "wt" ).write( metadatajson )

    isave = 0                                                                   # 0: do not save, 1: save each snapshot as separate .blend
    filename = "step%06d.blend" % isnapshot
else:
    # nothing changed; do not save anything
    isave = 0
    filename = ""


values = [ isave, bytes(filename,'UTF-8'), oppre ]


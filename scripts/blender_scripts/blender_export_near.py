# -*- coding: utf-8 -*-

"""
NOTE: MUST BE RUN IN BLENDER

exports current blend scene as .json file with a few default settings
"""

import bpy, os, sys, mathutils, json
from io_scene_near.jscene import json_scene 

if bpy.context.mode == 'PAINT_WEIGHT':
    bpy.ops.paint.weight_paint_toggle()

dirname = os.path.dirname( bpy.data.filepath )
conffn = os.path.join( dirname, "blender_export.conf" )
(pathname, ext) = os.path.splitext( bpy.data.filepath )
filepath = "%s.json" % pathname
basename = os.path.basename(filepath)

print( "exporting: %s" % bpy.data.filepath )
print( "pathname:  %s" % pathname )
print( "filepath:  %s" % filepath )
print( "basename:  %s" % basename )

scene = bpy.context.scene
data = bpy.data

defopts = {
    "diffuse_only":         True,
    "view_as_camera":       False,
    "tesselate":            False,
    "light_power_mult":     100.0,
    "export_selection":     True,
    "disable_mirror_merge": True,
    "apply_mods":           False,
    "apply_scale":          True,
    "scale":                (1,1,1),
    "step":                 pathname[-6:],
    "meta_data":            True,
    "bin_geometry":         False,
    }


if os.path.exists( conffn ):
    opts = json.load( open( conffn, "r" ) )
else:
    opts = {}

for k,v in defopts.items(): opts.setdefault( k, v )



jscene = json_scene(scene, data, dirname, basename, opts);

print( jscene )

scene_string = json.dumps(jscene, indent=4, sort_keys=True)
open( filepath, "w" ).write( scene_string )

print("scene: " + basename + " exported")


bpy.ops.wm.quit_blender()

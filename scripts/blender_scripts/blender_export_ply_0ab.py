import bpy, subprocess, os



"""
"""



path,prefix = os.path.split( bpy.data.filepath )
prefix,ext = os.path.splitext( prefix )

lmeshes = [ 'ver0', 'vera', 'verb' ]
fn0   = os.path.join( path, prefix + '_0.ply' )
fna   = os.path.join( path, prefix + '_a.ply' )
fnb   = os.path.join( path, prefix + '_b.ply' )



def layers( li_layer ):
    return [ (i in li_layer) for i in range(20) ]

def viewlayers( li_layer ):
    bpy.context.scene.layers = layers( li_layer )

def selectall():
    bpy.ops.object.select_all(action='SELECT')
def deselectall():
    bpy.ops.object.select_all(action='DESELECT')

def exec_wait( arglist ):
    return subprocess.Popen( arglist, stderr=subprocess.STDOUT, stdout=subprocess.PIPE ).communicate()


# make sure we're in object mode
prevm = bpy.context.mode.split('_')[0]
preva = bpy.context.scene.objects.active

if bpy.context.active_object:
    bpy.ops.object.mode_set(mode='OBJECT')

deselectall()
for i_l,fn in enumerate([fn0,fna,fnb]):
    mname = lmeshes[i_l]
    o = bpy.data.objects[mname]
    o.select = True
    bpy.context.scene.objects.active = o
    bpy.ops.object.shade_smooth()
    bpy.ops.export_mesh.ply(filepath=fn,use_modifiers=False,use_normals=False,use_uv_coords=False,use_colors=False)
    bpy.ops.object.shade_flat()
    deselectall()

preva.select = True
bpy.context.scene.objects.active = preva
bpy.ops.object.mode_set(mode=prevm)
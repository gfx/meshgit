import bpy, subprocess, os



"""
this script will:
    delete any objects in layers 5,6
    export objects from layers 0,1,2
    run meshgit to merge edits from m0->m1 and m0->m2
    import merged mesh to layer 5
    run meshgit to merge edits from m0->m2 and m0->m1
    import merged mesh to layer 6

NOTE: assumes the meshgit scripts are located in the same folder as the blend file
"""



path,prefix = os.path.split( bpy.data.filepath )
meshgit = os.path.join( path, 'meshgit2.py' )

fn0   = os.path.join( path, prefix + '_ver0.ply' )
fn1   = os.path.join( path, prefix + '_ver1.ply' )
fn2   = os.path.join( path, prefix + '_ver2.ply' )

fn012 = os.path.join( path, prefix + '_merge012.ply' )
fn021 = os.path.join( path, prefix + '_merge021.ply' )




def layers( li_layer ):
    return [ (i in li_layer) for i in range(20) ]

def viewlayers( li_layer ):
    bpy.context.scene.layers = layers( li_layer )

def selectall():
    bpy.ops.object.select_all(action='SELECT')
def deselectall():
    bpy.ops.object.select_all(action='DESELECT')

def exec_wait( arglist ):
    return subprocess.Popen( arglist, stderr=subprocess.STDOUT, stdout=subprocess.PIPE ).communicate()


# make sure we're in object mode
if bpy.context.active_object:
    bpy.ops.object.mode_set(mode='OBJECT')

# make sure that layers 0,1,2 have only one object each
for i in range(3):
    assert len([ o for o in bpy.data.objects if o.layers[i] ]) == 1, 'layer%i must have only 1 object' % i

# delete all objects in layers 5,6
viewlayers([5,6])
selectall()
bpy.ops.object.delete()


# export objects in layers 0,1,2
viewlayers([ i for i in range(20) ])
deselectall()
for i_l,fn in enumerate([fn0,fn1,fn2]):
    viewlayers([i_l])
    selectall()
    bpy.context.scene.objects.active = bpy.context.selected_objects[0]
    bpy.ops.object.shade_smooth()
    bpy.ops.export_mesh.ply(filepath=fn,use_modifiers=False,use_normals=False,use_uv_coords=False,use_colors=False)
    bpy.ops.object.shade_flat()
    deselectall()

print( 'results...' )
print( exec_wait( ['python2.7',meshgit,'merge',fn0,fn1,fn2,fn012] )[0].decode('utf-8') )
print( exec_wait( ['python2.7',meshgit,'merge',fn0,fn2,fn1,fn021] )[0].decode('utf-8') )
print( '' )


# import merged meshes to layers 5,6
viewlayers([5])
bpy.ops.import_mesh.ply(filepath=fn012)
selectall()
bpy.context.selected_objects[0].name = 'merge012'

viewlayers([6])
bpy.ops.import_mesh.ply(filepath=fn021)
selectall()
bpy.context.selected_objects[0].name = 'merge021'



# delete any of the files created
for fn in [ fn0,fn1,fn2,fn012,fn021 ]:
    try: os.remove( fn )
    except: pass


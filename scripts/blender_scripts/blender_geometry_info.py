import bpy

mesh = bpy.data.objects['GEO-body'].data
lv,le,lf = mesh.vertices,mesh.edges,mesh.faces

# histe: "histogram" of vert inds based on count of adj edges
# histf: "histogram" of vert inds based on count of adj faces
# bordv: list of vert inds that are on the bordor (has adj edge with only one adj face)
histe,histf,bordv = {},{},[]
for iv,v in enumerate(lv):
    lae = [ e for e in le if iv in e.vertices ]
    cae = len(lae)
    if cae in histe: histe[cae] += [iv]
    else: histe[cae] = [iv]
    laf = [ f for f in lf if iv in f.vertices ]
    caf = len(laf)
    if caf in histf: histf[caf] += [iv]
    else: histf[caf] = [iv]
    for e in lae:
        v0,v1 = e.vertices
        c = len( [ 1 for f in laf if v0 in f.vertices and v1 in f.vertices ] )
        if c == 1:
            bordv += [iv]
            break


print( { k:len(v) for k,v in hist.items() } )

for iv in hist[5]:
    lv[iv].select = True

import bpy, subprocess, os



"""
this script will:
    delete any objects in layers 11,12
    export objects from layers 0,1,2
    run meshgit to diff m0->m1 and diff m0->m2
    import diffed mesh to layer 10 and 11

NOTE: assumes the meshgit scripts are located in the same folder as the blend file
"""



path,prefix = os.path.split( bpy.data.filepath )
meshgit = os.path.join( path, 'meshgit2.py' )

fn0   = os.path.join( path, prefix + '_ver0.ply' )
fn1   = os.path.join( path, prefix + '_ver1.ply' )
fn2   = os.path.join( path, prefix + '_ver2.ply' )

fn01d = os.path.join( path, prefix + '_diff01d.ply' ) # del
fn01c = os.path.join( path, prefix + '_diff01c.ply' ) # chg (not del or add)
fn01a = os.path.join( path, prefix + '_diff01a.ply' ) # add

fn02d = os.path.join( path, prefix + '_diff02d.ply' ) # del
fn02c = os.path.join( path, prefix + '_diff02c.ply' ) # chg
fn02a = os.path.join( path, prefix + '_diff02a.ply' ) # add

offset = 4




def layers( li_layer ):
    return [ (i in li_layer) for i in range(20) ]

def viewlayers( li_layer ):
    bpy.context.scene.layers = layers( li_layer )

def selectall():
    bpy.ops.object.select_all(action='SELECT')

def deselectall():
    bpy.ops.object.select_all(action='DESELECT')

def exec_wait( arglist ):
    return subprocess.Popen( arglist, stderr=subprocess.STDOUT, stdout=subprocess.PIPE ).communicate()


# make sure we're in object mode
if bpy.context.active_object:
    bpy.ops.object.mode_set(mode='OBJECT')

# make sure that layers 0,1,2 have only one object each
for i in range(3):
    assert len([ o for o in bpy.data.objects if o.layers[i] ]) == 1, 'layer%i must have only 1 object' % i

# delete all objects in layers 5,6
viewlayers([11,12])
selectall()
bpy.ops.object.delete()


# export objects in layers 0,1,2
viewlayers([ i for i in range(20) ])
deselectall()
for i_l,fn in enumerate([fn0,fn1,fn2]):
    viewlayers([i_l])
    selectall()
    bpy.context.scene.objects.active = bpy.context.selected_objects[0]
    bpy.ops.object.shade_smooth()
    bpy.ops.export_mesh.ply(filepath=fn,use_modifiers=False,use_normals=False,use_uv_coords=False,use_colors=False)
    bpy.ops.object.shade_flat()
    deselectall()

matd = bpy.data.materials['deleted']
mata = bpy.data.materials['added']


# perform diffs
print( 'results...' )
print( exec_wait( ['python2.7',meshgit,'diff',fn0,fn1,fn01d,fn01c,fn01a] )[0].decode('utf-8') )
print( exec_wait( ['python2.7',meshgit,'diff',fn0,fn2,fn02d,fn02c,fn02a] )[0].decode('utf-8') )
print( '' )


# import diffed meshes to layers 11,12
viewlayers([11])

bpy.ops.import_mesh.ply(filepath=fn01d)
o = bpy.context.selected_objects[0]
o.data.materials.append(matd)
o.data.update()
o.name = 'diff01d'
bpy.ops.transform.translate(value=(-offset,offset,0))
bpy.ops.object.duplicate()
bpy.ops.transform.translate(value=(0,-offset,0))
bpy.ops.object.duplicate()
bpy.ops.transform.translate(value=(offset,offset,0))
deselectall()

bpy.ops.import_mesh.ply(filepath=fn01c)
o = bpy.context.selected_objects[0]
o.name = 'diff01c'
bpy.ops.transform.translate(value=(-offset,offset,0))
bpy.ops.object.duplicate()
bpy.ops.transform.translate(value=(offset*2,-offset,0))
deselectall()

bpy.ops.import_mesh.ply(filepath=fn01a)
o = bpy.context.selected_objects[0]
o.data.materials.append(mata)
o.data.update()
o.name = 'diff01a'
bpy.ops.transform.translate(value=(-offset,offset,0))
bpy.ops.object.duplicate()
bpy.ops.transform.translate(value=(0,-offset,0))
bpy.ops.object.duplicate()
bpy.ops.transform.translate(value=(offset,-offset,0))
deselectall()

viewlayers([12])

bpy.ops.import_mesh.ply(filepath=fn02d)
o = bpy.context.selected_objects[0]
o.data.materials.append(matd)
o.data.update()
o.name = 'diff02d'
bpy.ops.transform.translate(value=(-offset,offset,0))
bpy.ops.object.duplicate()
bpy.ops.transform.translate(value=(0,-offset,0))
bpy.ops.object.duplicate()
bpy.ops.transform.translate(value=(offset,offset,0))
deselectall()

bpy.ops.import_mesh.ply(filepath=fn02c)
o = bpy.context.selected_objects[0]
o.name = 'diff02c'
bpy.ops.transform.translate(value=(-offset,offset,0))
bpy.ops.object.duplicate()
bpy.ops.transform.translate(value=(offset*2,-offset,0))
deselectall()

bpy.ops.import_mesh.ply(filepath=fn02a)
o = bpy.context.selected_objects[0]
o.data.materials.append(mata)
o.data.update()
o.name = 'diff02a'
bpy.ops.transform.translate(value=(-offset,offset,0))
bpy.ops.object.duplicate()
bpy.ops.transform.translate(value=(0,-offset,0))
bpy.ops.object.duplicate()
bpy.ops.transform.translate(value=(offset,-offset,0))
deselectall()



# delete any of the files created
for fn in [ fn0,fn1,fn2,fn01d,fn01c,fn01a,fn02d,fn02c,fn02a ]:
    try: os.remove( fn )
    except: pass


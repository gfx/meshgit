#include <string>
#include <vector>

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "mesh.h"
#include "quicksort.h"
#include "kdtree.h"

using namespace std;


// mesh data
float *lvp0, *lvp1;                                         // vert positions
float *lvn0, *lvn1;                                         // vert normals
float *lfp0, *lfp1;                                         // face positions (ex: centers)
float *lfn0, *lfn1;                                         // face normals
int mnlifv0, mnlifv1;                                       // verts for faces
int mnlavv0, mnlavv1, mnlavf0, mnlavf1;                     // vert-vert and vert-face adj
int mnlaff0, mnlaff1;                                       // face-face adj
int nv0, nv1, nf0, nf1;                                     // geometry counts

int *nlsiv01, *nlsif01;
int **lsiv01, **lsif01;                                     // indices of geometry in sorted order (by distance)
int *nlsiv10, *nlsif10;
int **lsiv10, **lsif10;                                     // indices of geometry in sorted order (by distance)

int *nlifv0, **lifv0;                                       // face vert inds
int *nlifv1, **lifv1;

int *nlavv0, **lavv0;                                       // vert-vert adjacencies
int *nlavv1, **lavv1;
int *nlavf0, **lavf0;                                       // vert-face adjacencies
int *nlavf1, **lavf1;
int *nlaff0, **laff0;                                       // face-face adjacencies
int *nlaff1, **laff1;

// mesh matches
int *vmatch01;
int *vmatch10;
int *fmatch01;
int *fmatch10;

int *vmatch01_iter;
int *fmatch01_iter;
int iter_current = 0;

// costs
float cost_vdel, cost_vadd, cost_vdist, cost_vnorm;
float cost_fdel, cost_fadd, cost_fdist, cost_fvunk, cost_fvmis, cost_fnorm, cost_fmism, cost_fmunk;
float cost_fvdist, cost_ffdist;
int cost_k, cost_maxiters;

int knn = 50;

// comment following line to not use distance limit function
#define LIMIT_MULT      1.0f  /* 2.5f */


bool pairwise_optimization;                                 // precomputed distances and dotted norms
float **mvd01, **mvn01, **mfd01, **mfn01;                   // matrices of pairwise distances and dotted norms


float delta_fn( float *p0, float *p1 )
{
    // | p0 - p1 | / ( |p0 - p1| + 1.0 )
    float dx = p0[0] - p1[0];
    float dy = p0[1] - p1[1];
    float dz = p0[2] - p1[2];
    float d = sqrt( dx*dx + dy*dy + dz*dz );
#ifdef LIMIT_MULT
    d *= LIMIT_MULT;
    return d / ( d + 1.0f );
    //return 1.0f - 1.0f / (LIMIT_MULT * d + 1.0f);
#else
    return d;
#endif
}
float dist_fn( float *p0, float *p1 )
{
    float dx = p0[0] - p1[0];
    float dy = p0[1] - p1[1];
    float dz = p0[2] - p1[2];
    float d = sqrt( dx*dx + dy*dy + dz*dz );
    return d;
}

float n_fn( float *n0, float *n1 )
{
    // 1.0 - ( n0 dot n1 )
    float d = n0[0]*n1[0] + n0[1]*n1[1] + n0[2]*n1[2];
    return 1.0f - d;
}



float v_dist( int iv0, int iv1 )
{
    if( pairwise_optimization ) return mvd01[iv0][iv1];
    return delta_fn( &lvp0[iv0*3], &lvp1[iv1*3] );
}
float v_norm( int iv0, int iv1 )
{
    if( pairwise_optimization ) return mvn01[iv0][iv1];
    return n_fn( &lvn0[iv0*3], &lvn1[iv1*3] );
}

float f_dist( int if0, int if1 )
{
    if( pairwise_optimization ) return mfd01[if0][if1];
    return delta_fn( &lfp0[if0*3], &lfp1[if1*3] );
}
float f_norm( int if0, int if1 )
{
    if( pairwise_optimization ) return mfn01[if0][if1];
    return n_fn( &lfn0[if0*3], &lfn1[if1*3] );
}

float v0v1_ddist( int iv0, int iv1 ) { return delta_fn( &lvp0[iv0*3], &lvp1[iv1*3] ); }
float v1v0_ddist( int iv1, int iv0 ) { return delta_fn( &lvp0[iv0*3], &lvp1[iv1*3] ); }
float f0f1_ddist( int if0, int if1 ) { return delta_fn( &lfp0[if0*3], &lfp1[if1*3] ); }
float f1f0_ddist( int if1, int if0 ) { return delta_fn( &lfp0[if0*3], &lfp1[if1*3] ); }

float v0v0_ddist( int iv0, int iv1 ) { return delta_fn( &lvp0[iv0*3], &lvp0[iv1*3] ); }
float v0f0_ddist( int i_v, int i_f ) { return delta_fn( &lvp0[i_v*3], &lfp0[i_f*3] ); }
float f0v0_ddist( int i_f, int i_v ) { return delta_fn( &lfp0[i_f*3], &lvp0[i_v*3] ); }
float f0f0_ddist( int if0, int if1 ) { return delta_fn( &lfp0[if0*3], &lfp0[if1*3] ); }

float v1v1_ddist( int iv1, int iv0 ) { return delta_fn( &lvp1[iv0*3], &lvp1[iv1*3] ); }
float v1f1_ddist( int i_v, int i_f ) { return delta_fn( &lvp1[i_v*3], &lfp1[i_f*3] ); }
float f1v1_ddist( int i_f, int i_v ) { return delta_fn( &lfp1[i_f*3], &lvp1[i_v*3] ); }
float f1f1_ddist( int if1, int if0 ) { return delta_fn( &lfp1[if0*3], &lfp1[if1*3] ); }

float v0v1_dist( int iv0, int iv1 ) { return dist_fn( &lvp0[iv0*3], &lvp1[iv1*3] ); }
float v1v0_dist( int iv1, int iv0 ) { return dist_fn( &lvp0[iv0*3], &lvp1[iv1*3] ); }
float f0f1_dist( int if0, int if1 ) { return dist_fn( &lfp0[if0*3], &lfp1[if1*3] ); }
float f1f0_dist( int if1, int if0 ) { return dist_fn( &lfp0[if0*3], &lfp1[if1*3] ); }

float v0v1_norm( int iv0, int iv1 ) { return n_fn( &lvn0[iv0*3], &lvn1[iv1*3] ); }
float v1v0_norm( int iv1, int iv0 ) { return n_fn( &lvn0[iv0*3], &lvn1[iv1*3] ); }
float f0f1_norm( int if0, int if1 ) { return n_fn( &lfn0[if0*3], &lfn1[if1*3] ); }
float f1f0_norm( int if1, int if0 ) { return n_fn( &lfn0[if0*3], &lfn1[if1*3] ); }

float vv0_dist( int iv0, int iv1 ) { return dist_fn( &lvp0[iv0*3], &lvp0[iv1*3] ); }
float vv1_dist( int iv0, int iv1 ) { return dist_fn( &lvp1[iv0*3], &lvp1[iv1*3] ); }
float vf0_dist( int i_v, int i_f ) { return dist_fn( &lvp0[i_v*3], &lfp0[i_f*3] ); }
float vf1_dist( int i_v, int i_f ) { return dist_fn( &lvp1[i_v*3], &lfp1[i_f*3] ); }
float fv0_dist( int i_f, int i_v ) { return dist_fn( &lfp0[i_f*3], &lvp0[i_v*3] ); }
float ff0_dist( int if0, int if1 ) { return dist_fn( &lfp0[if0*3], &lfp0[if0*3] ); }
float fv1_dist( int i_f, int i_v ) { return dist_fn( &lfp1[i_f*3], &lvp1[i_v*3] ); }
float ff1_dist( int if0, int if1 ) { return dist_fn( &lfp1[if0*3], &lfp1[if0*3] ); }


void insert_shuffled( float *lp, int n, KDTree *kdt, int *m )
{
    unsigned int *linds = shuffled_inds( n );
    for( unsigned int _i = 0; _i < n; _i++ ) {
        unsigned int i = linds[_i];
        if( m[i] == -1 )
            kdt->insert( &lp[i*3], i );
    }
    free( linds );
}

void compute_knn_kdt()
{
    int *iknn   = (int*)   malloc( knn*sizeof(int) );
    float *dist = (float*) malloc( knn*sizeof(float) );
    
    printf( "  building KDTree...\n" );
    KDTree *kdt_v0 = new KDTree();
    KDTree *kdt_v1 = new KDTree();
    KDTree *kdt_f0 = new KDTree();
    KDTree *kdt_f1 = new KDTree();
    
    // only insert unmatched verts and faces
    insert_shuffled( lvp0, nv0, kdt_v0, vmatch01 );
    insert_shuffled( lvp1, nv1, kdt_v1, vmatch10 );
    insert_shuffled( lfp0, nf0, kdt_f0, fmatch01 );
    insert_shuffled( lfp1, nf1, kdt_f1, fmatch10 );
    
    //kdt_v0->insert_shuffle( lvp0, nv0 );
    //kdt_v1->insert_shuffle( lvp1, nv1 );
    //kdt_f0->insert_shuffle( lfp0, nf0 );
    //kdt_f1->insert_shuffle( lfp1, nf1 );
    
    printf( "  verts...\n" );
    for( int iv0 = 0; iv0 < nv0; iv0++ )
    {
        //if( vmatch01[iv0] != -1 ) continue;
        int k = kdt_v1->knn( &lvp0[iv0*3], knn, iknn, dist );
        nlsiv01[iv0] = k;
        for( int i = 0; i < k; i++ ) lsiv01[iv0][i] = iknn[i];
    }
    for( int iv1 = 0; iv1 < nv1; iv1++ )
    {
        //if( vmatch10[iv1] != -1 ) continue;
        int k = kdt_v0->knn( &lvp1[iv1*3], knn, iknn, dist );
        nlsiv10[iv1] = k;
        for( int i = 0; i < k; i++ ) lsiv10[iv1][i] = iknn[i];
    }
    printf( "  faces...\n" );
    for( int if0 = 0; if0 < nf0; if0++ )
    {
        //if( fmatch01[if0] != -1 ) continue;
        int k = kdt_f1->knn( &lfp0[if0*3], knn, iknn, dist );
        nlsif01[if0] = k;
        for( int i = 0; i < k; i++ ) lsif01[if0][i] = iknn[i];
    }
    for( int if1 = 0; if1 < nf1; if1++ )
    {
        //if( fmatch10[if1] != -1 ) continue;
        int k = kdt_f0->knn( &lfp1[if1*3], knn, iknn, dist );
        nlsif10[if1] = k;
        for( int i = 0; i < k; i++ ) lsif10[if1][i] = iknn[i];
    }
    
    //printf( "  saving cache...\n" );
    //kdt_v0->save_cache( "v0.kdt" );
    //kdt_v1->save_cache( "v1.kdt" );
    //kdt_f0->save_cache( "f0.kdt" );
    //kdt_f1->save_cache( "f1.kdt" );
}

void load_knn_kdt()
{
    
}

void compute_knn_full()
{
    pairwise_optimization = ( nv0+nv1 < 12000 && nf0+nf1 < 12000 );
    
    if( pairwise_optimization )
    {
        printf( "  allocating matrices for pairwise distances and dotted normals...\n" );
        float *vd = (float*)malloc( nv0 * nv1 * sizeof(float) );
        float *vn = (float*)malloc( nv0 * nv1 * sizeof(float) );
        mvd01 = (float**)malloc( nv0 * sizeof(float*) );
        mvn01 = (float**)malloc( nv0 * sizeof(float*) );
        for( int iv0 = 0; iv0 < nv0; iv0++ ) { mvd01[iv0] = &vd[iv0*nv1]; mvn01[iv0] = &vn[iv0*nv1]; }
        
        float *fd = (float*)malloc( nf0 * nf1 * sizeof(float) );
        float *fn = (float*)malloc( nf0 * nf1 * sizeof(float) );
        mfd01 = (float**)malloc( nf0 * sizeof(float*) );
        mfn01 = (float**)malloc( nf0 * sizeof(float*) );
        for( int if0 = 0; if0 < nf0; if0++ ) { mfd01[if0] = &fd[if0*nf1]; mfn01[if0] = &fn[if0*nf1]; }
        
        printf( "  precomputing pairwise distances and dotted normals...\n" );
        #pragma omp parallel for default(none) \
            shared(nv0,nv1,lvp0,lvn0,lvp1,lvn1,mvd01,mvn01)
        for( int iv0 = 0; iv0 < nv0; iv0++ )
        {
            float *vp0 = &lvp0[iv0*3];
            float *vn0 = &lvn0[iv0*3];
            for( int iv1 = 0; iv1 < nv1; iv1++ )
            {
                float *vp1 = &lvp1[iv1*3];
                float *vn1 = &lvn1[iv1*3];
                mvd01[iv0][iv1] = sqrt(
                    ( vp0[0] - vp1[0] ) * ( vp0[0] - vp1[0] ) +
                    ( vp0[1] - vp1[1] ) * ( vp0[1] - vp1[1] ) +
                    ( vp0[2] - vp1[2] ) * ( vp0[2] - vp1[2] )
                );
                mvn01[iv0][iv1] = ( 1.0 - ( vn0[0] * vn1[0] + vn0[1] * vn1[1] + vn0[2] * vn1[2] ) ); // / 2.0;
            }
        }
        #pragma omp parallel for default(none) \
            shared(nf0,nf1,lfp0,lfn0,lfp1,lfn1,mfd01,mfn01)
        for( int if0 = 0; if0 < nf0; if0++ )
        {
            float *fp0 = &lfp0[if0*3];
            float *fn0 = &lfn0[if0*3];
            for( int if1 = 0; if1 < nf1; if1++ )
            {
                float *fp1 = &lfp1[if1*3];
                float *fn1 = &lfn1[if1*3];
                mfd01[if0][if1] = sqrt( (fp0[0]-fp1[0])*(fp0[0]-fp1[0]) + (fp0[1]-fp1[1])*(fp0[1]-fp1[1]) + (fp0[2]-fp1[2])*(fp0[2]-fp1[2]) );
                mfn01[if0][if1] = ( 1.0 - (fn0[0]*fn1[0] + fn0[1]*fn1[1] + fn0[2]*fn1[2]) ); // / 2.0;
            }
        }
        printf( "    done!\n" );
    }
    
    
    printf( "  computing sorted list of indices...\n" );
    
    // compute list of inds in sorted order (closest first)
    
    float *vdists = (float*)malloc( nv1 * sizeof(float) );
    unsigned int *vinds = (unsigned int*)malloc( nv1 * sizeof(unsigned int) );
    float *fdists = (float*)malloc( nf1 * sizeof(float) );
    unsigned int *finds = (unsigned int*)malloc( nf1 * sizeof(unsigned int) );
    
    printf( "    verts...\n" );
    for( int iv0 = 0; iv0 < nv0; iv0++ )
    {
        if(vmatch01[iv0] != -1) continue;
        
        #pragma omp parallel for default(none) \
            shared(nv1,iv0,vdists,vinds)
        for( unsigned int iv1 = 0; iv1 < nv1; iv1++ )
        {
            vdists[iv1] = v_dist( iv0, iv1 );
            vinds[iv1] = iv1;
        }
        shuffle( vinds, nv1 );
        quicksort_inds( vdists, vinds, nv1 );
        nlsiv01[iv0] = knn;
        for( int i = 0; i < knn; i++ ) lsiv01[iv0][i] = vinds[i];
    }
    
    printf( "    faces...\n" );
    for( int if0 = 0; if0 < nf0; if0++ )
    {
        if(fmatch01[if0] != -1) continue;
        
        #pragma omp parallel for default(none) \
            shared(nf1,if0,fdists,finds)
        for( int if1 = 0; if1 < nf1; if1++ )
        {
            fdists[if1] = f_dist( if0, if1 );
            finds[if1] = if1;
        }
        shuffle( finds, nf1 );
        quicksort_inds( fdists, finds, nf1 );
        nlsif01[if0] = knn;
        for( int i = 0; i < knn; i++ ) lsif01[if0][i] = finds[i];
    }
    
    printf( "    done!\n" );
    
    // NOTE: not worrying about freeing vert and face data...
}

#define MIN( x, y )  ( (x) < (y) ? (x) : (y) )
void compute_knn()
{
    knn = MIN( knn, MIN( MIN( nv0, nv1 ), MIN( nf0, nf1 ) ) );
    
    // allocating
    nlsiv01   = (int*) calloc( nv0, sizeof(int) );
    lsiv01    = (int**)calloc( nv0, sizeof(int*) );
    int *vi01 = (int*) calloc( nv0 * knn, sizeof(int) );                // allocate a block
    for( int iv0 = 0; iv0 < nv0; iv0++ ) lsiv01[iv0] = &vi01[iv0*knn];  // assign into block
    
    nlsiv10   = (int*) calloc( nv1, sizeof(int) );
    lsiv10    = (int**)calloc( nv1, sizeof(int*) );
    int *vi10 = (int*) calloc( nv1 * knn, sizeof(int) );                // allocate a block
    for( int iv1 = 0; iv1 < nv1; iv1++ ) lsiv10[iv1] = &vi10[iv1*knn];  // assign into block
    
    nlsif01   = (int*) calloc( nf0, sizeof(int) );
    lsif01    = (int**)calloc( nf0, sizeof(int*) );
    int *fi01 = (int*) calloc( nf0 * knn, sizeof(int) );                // allocate a block
    for( int if0 = 0; if0 < nf0; if0++ ) lsif01[if0] = &fi01[if0*knn];  // assign into block
    
    nlsif10   = (int*) calloc( nf1, sizeof(int) );
    lsif10    = (int**)calloc( nf1, sizeof(int*) );
    int *fi10 = (int*) calloc( nf1 * knn, sizeof(int) );                // allocate a block
    for( int if1 = 0; if1 < nf1; if1++ ) lsif10[if1] = &fi10[if1*knn];  // assign into block
    
    compute_knn_kdt();
    //compute_knn_full();
}



void vm01_add( int iv0, int iv1 )
{
    if( vmatch01[iv0] != -1 ) { printf( "vm01_add(%d,%d): vmatch01[%d] = %d != -1\n", iv0, iv1, iv0, vmatch01[iv0] ); exit(1); }
    if( vmatch10[iv1] != -1 ) { printf( "vm01_add(%d,%d): vmatch10[%d] = %d != -1\n", iv0, iv1, iv1, vmatch10[iv1] ); exit(1); }
    vmatch01[iv0] = iv1;
    vmatch10[iv1] = iv0;
    vmatch01_iter[iv0] = iter_current++;
}
void vm01_del( int iv0, int iv1 )
{
    if( vmatch01[iv0] != iv1 ) { printf( "vm01_del(%d,%d): vmatch01[%d] = %d != %d\n", iv0, iv1, iv0, vmatch01[iv0], iv1 ); exit(1); }
    if( vmatch10[iv1] != iv0 ) { printf( "vm01_del(%d,%d): vmatch10[%d] = %d != %d\n", iv0, iv1, iv1, vmatch10[iv1], iv0 ); exit(1); }
    vmatch01[iv0] = vmatch10[iv1] = -1;
    vmatch01_iter[iv0] = iter_current++;
}
void fm01_add( int if0, int if1 )
{
    if( fmatch01[if0] != -1 ) { printf( "fm01_add(%d,%d): fmatch01[%d] = %d != -1\n", if0, if1, if0, fmatch01[if0] ); exit(1); }
    if( fmatch10[if1] != -1 ) { printf( "fm01_add(%d,%d): fmatch10[%d] = %d != -1\n", if0, if1, if1, fmatch10[if1] ); exit(1); }
    fmatch01[if0] = if1;
    fmatch10[if1] = if0;
    fmatch01_iter[if0] = iter_current++;
}
void fm01_del( int if0, int if1 )
{
    if( fmatch01[if0] != if1 ) { printf( "fm01_del(%d,%d): fmatch01[%d] = %d != %d\n", if0, if1, if0, fmatch01[if0], if1 ); exit(1); }
    if( fmatch10[if1] != if0 ) { printf( "fm01_del(%d,%d): fmatch10[%d] = %d != %d\n", if0, if1, if1, fmatch10[if1], if0 ); exit(1); }
    fmatch01[if0] = fmatch10[if1] = -1;
    fmatch01_iter[if0] = iter_current++;
}

// gets local neighborhood in mesh0 around i0 and i1
void populate_liv0_lif0( vector<int> &liv0, vector<int> &lif0, int i0, int i1 )
{
    liv0.clear(); lif0.clear();
    
    if( i0 < nv0 ) {
        int iv0 = i0;
        int iv1 = i1;
        
        int *avf0 = lavf0[iv0], navf0 = nlavf0[iv0];
        int *avf1 = lavf1[iv1], navf1 = nlavf1[iv1];
        
        liv0.push_back( iv0 );
        
        for( int i = 0; i < navf0; i++ )
            lif0.push_back( avf0[i] );
        for( int i = 0; i < navf1; i++ )
        {
            int if0 = fmatch10[avf1[i]];
            if( if0 != -1 )
                lif0.push_back( if0 );
        }
    } else {
        int if0 = i0 - nv0;
        int if1 = i1 - nv1;
        
        int *ifv0 = lifv0[if0], nifv0 = nlifv0[if0];
        int *ifv1 = lifv1[if1], nifv1 = nlifv1[if1];
        int *aff0 = laff0[if0], naff0 = nlaff0[if0];
        int *aff1 = laff1[if1], naff1 = nlaff1[if1];
        
        // build list of affected verts and faces from sub'ing if0->if1
        for( int i = 0; i < nifv0; i++ )
            liv0.push_back( ifv0[i] );
        // add any vertex in m0 that maps to a vertex in m1 which is adj to if1
        for( int i = 0; i < nifv1; i++ )
        {
            int iv0 = vmatch10[ifv1[i]];
            if( iv0 != -1 )
                liv0.push_back( iv0 );
        }
        
        lif0.push_back( if0 );
        for( int i = 0; i < naff0; i++ )
            lif0.push_back( aff0[i] );
        // add any face in m0 that maps to a face in m1 which is adj to if1
        for( int i = 0; i < naff1; i++ )
        {
            int if0_ = fmatch10[aff1[i]];
            if( if0_ != -1 )
                lif0.push_back( if0_ );
        }
    }
}

// gets local neighborhood in mesh1 around i0 and i1
void populate_liv1_lif1( vector<int> &liv1, vector<int> &lif1, int i0, int i1 )
{
    liv1.clear(); lif1.clear();
    
    if( i0 < nv1 ) {
        int iv0 = i0;
        int iv1 = i1;
        
        int *avf0 = lavf0[iv0], navf0 = nlavf0[iv0];
        int *avf1 = lavf1[iv1], navf1 = nlavf1[iv1];
        
        liv1.push_back( iv1 );
        
        for( int i = 0; i < navf1; i++ )
            lif1.push_back( avf1[i] );
        for( int i = 0; i < navf0; i++ )
        {
            int if1 = fmatch01[avf0[i]];
            if( if1 != -1 )
                lif1.push_back( if1 );
        }
    } else {
        int if0 = i0 - nv0;
        int if1 = i1 - nv1;
        
        int *ifv0 = lifv0[if0], nifv0 = nlifv0[if0];
        int *ifv1 = lifv1[if1], nifv1 = nlifv1[if1];
        int *aff0 = laff0[if0], naff0 = nlaff0[if0];
        int *aff1 = laff1[if1], naff1 = nlaff1[if1];
        
        // build list of affected verts and faces from sub'ing if0->if1
        for( int i = 0; i < nifv1; i++ )
            liv1.push_back( ifv1[i] );
        // add any vertex in m0 that maps to a vertex in m1 which is adj to if1
        for( int i = 0; i < nifv1; i++ )
        {
            int iv1 = vmatch01[ifv0[i]];
            if( iv1 != -1 )
                liv1.push_back( iv1 );
        }
        
        lif1.push_back( if1 );
        for( int i = 0; i < naff1; i++ )
            lif1.push_back( aff1[i] );
        // add any face in m0 that maps to a face in m1 which is adj to if1
        for( int i = 0; i < naff0; i++ )
        {
            int if1_ = fmatch01[aff0[i]];
            if( if1_ != -1 )
                lif1.push_back( if1_ );
        }
    }
}



#define CHECK_MARKER(expect)    \
    fscanf(fp,"%s",s); \
    marker.assign(s); \
    if( marker.compare(expect) ) { \
        printf( "error loading: \"%s\" != \"%s\"\n", expect, marker.data() ); \
        exit(1); \
    }

void load_mesh_data( const char *fname )
{
    // terribly inefficient loading, but ok since this is only loaded once
    
    int *t;
    char s[80];
    string marker( 80, 0 );
    
    FILE *fp = fopen( fname, "rt" );
    
    // load cost parameters
    CHECK_MARKER( "costs" );
    fscanf( fp, "%f %f", &cost_vdel, &cost_vadd );
    fscanf( fp, "%f %f", &cost_vdist, &cost_vnorm );
    fscanf( fp, "%f %f", &cost_fdel, &cost_fadd );
    fscanf( fp, "%f %f %f", &cost_fdist, &cost_fvunk, &cost_fvmis );
    fscanf( fp, "%f %f %f", &cost_fnorm, &cost_fmism, &cost_fmunk );
    fscanf( fp, "%f %f", &cost_fvdist, &cost_ffdist );
    fscanf( fp, "%d %d", &cost_k, &cost_maxiters );
    
    // load vert and face counts
    CHECK_MARKER( "counts" );
    fscanf( fp, "%d %d %d %d", &nv0, &nv1, &nf0, &nf1 );
    fscanf( fp, "%d %d", &mnlifv0, &mnlifv1);
    fscanf( fp, "%d %d %d %d", &mnlavv0, &mnlavv1, &mnlavf0, &mnlavf1 );
    fscanf( fp, "%d %d", &mnlaff0, &mnlaff1 );
    
    printf( "  nv0 = %d, nv1 = %d\n", nv0, nv1 );
    printf( "  nf0 = %d, nf1 = %d\n", nf0, nf1 );
    
    lvp0 = (float*)malloc( nv0 * 3 * sizeof(float) );
    lvn0 = (float*)malloc( nv0 * 3 * sizeof(float) );
    lvp1 = (float*)malloc( nv1 * 3 * sizeof(float) );
    lvn1 = (float*)malloc( nv1 * 3 * sizeof(float) );
    
    lfp0 = (float*)malloc( nf0 * 3 * sizeof(float) );
    lfn0 = (float*)malloc( nf0 * 3 * sizeof(float) );
    lfp1 = (float*)malloc( nf1 * 3 * sizeof(float) );
    lfn1 = (float*)malloc( nf1 * 3 * sizeof(float) );
    
    nlifv0 = (int*)malloc( nf0 * sizeof(int) ); lifv0 = (int**)malloc( nf0 * sizeof(int*) );
    nlifv1 = (int*)malloc( nf1 * sizeof(int) ); lifv1 = (int**)malloc( nf1 * sizeof(int*) );
    
    nlavv0 = (int*)malloc( nv0 * sizeof(int) ); lavv0 = (int**)malloc( nv0 * sizeof(int*) );
    nlavv1 = (int*)malloc( nv1 * sizeof(int) ); lavv1 = (int**)malloc( nv1 * sizeof(int*) );
    nlavf0 = (int*)malloc( nv0 * sizeof(int) ); lavf0 = (int**)malloc( nv0 * sizeof(int*) );
    nlavf1 = (int*)malloc( nv1 * sizeof(int) ); lavf1 = (int**)malloc( nv1 * sizeof(int*) );
    
    nlaff0 = (int*)malloc( nf0 * sizeof(int) ); laff0 = (int**)malloc( nf0 * sizeof(int*) );
    nlaff1 = (int*)malloc( nf1 * sizeof(int) ); laff1 = (int**)malloc( nf1 * sizeof(int*) );
    
    CHECK_MARKER( "verts0" );
    for( int i = 0; i < nv0; i++ )
        fscanf( fp, "%f %f %f %f %f %f", &lvp0[i*3+0], &lvp0[i*3+1], &lvp0[i*3+2], &lvn0[i*3+0], &lvn0[i*3+1], &lvn0[i*3+2] );
    CHECK_MARKER( "verts1" );
    for( int i = 0; i < nv1; i++ )
        fscanf( fp, "%f %f %f %f %f %f", &lvp1[i*3+0], &lvp1[i*3+1], &lvp1[i*3+2], &lvn1[i*3+0], &lvn1[i*3+1], &lvn1[i*3+2] );
    CHECK_MARKER( "faces0" );
    for( int i = 0; i < nf0; i++ )
        fscanf( fp, "%f %f %f %f %f %f", &lfp0[i*3+0], &lfp0[i*3+1], &lfp0[i*3+2], &lfn0[i*3+0], &lfn0[i*3+1], &lfn0[i*3+2] );
    CHECK_MARKER( "faces1" );
    for( int i = 0; i < nf1; i++ )
        fscanf( fp, "%f %f %f %f %f %f", &lfp1[i*3+0], &lfp1[i*3+1], &lfp1[i*3+2], &lfn1[i*3+0], &lfn1[i*3+1], &lfn1[i*3+2] );
    
    // load face vert indices
    CHECK_MARKER( "face0verts" );
    t = (int*)malloc( nf0 * mnlifv0 * sizeof(int) );
    for( int i = 0; i < nf0; i++ )
    {
        lifv0[i] = &t[ i * mnlifv0 ];
        fscanf( fp, "%i", &nlifv0[i] );
        for( int i_ = 0; i_ < nlifv0[i]; i_++ ) fscanf( fp, "%d", &lifv0[i][i_] );
    }
    CHECK_MARKER( "face1verts" );
    t = (int*)malloc( nf1 * mnlifv1 * sizeof(int) );
    for( int i = 0; i < nf1; i++ )
    {
        lifv1[i] = &t[ i * mnlifv1 ];
        fscanf( fp, "%i", &nlifv1[i] );
        for( int i_ = 0; i_ < nlifv1[i]; i_++ ) fscanf( fp, "%d", &lifv1[i][i_] );
    }
    
    // load vert-vert adjacencies
    CHECK_MARKER( "vert0verts" );
    t = (int*)malloc( nv0 * mnlavv0 * sizeof(int) );
    for( int i = 0; i < nv0; i++ )
    {
        lavv0[i] = &t[ i * mnlavv0 ];
        fscanf( fp, "%i", &nlavv0[i] );
        for( int i_ = 0; i_ < nlavv0[i]; i_++ ) fscanf( fp, "%d", &lavv0[i][i_] );
    }
    CHECK_MARKER( "vert1verts" );
    t = (int*)malloc( nv1 * mnlavv1 * sizeof(int) );
    for( int i = 0; i < nv1; i++ )
    {
        lavv1[i] = &t[ i * mnlavv1 ];
        fscanf( fp, "%i", &nlavv1[i] );
        for( int i_ = 0; i_ < nlavv1[i]; i_++ ) fscanf( fp, "%d", &lavv1[i][i_] );
    }
    
    // load vert-face adjacencies
    CHECK_MARKER( "vert0faces" );
    t = (int*)malloc( nv0 * mnlavf0 * sizeof(int) );
    for( int i = 0; i < nv0; i++ )
    {
        lavf0[i] = &t[ i * mnlavf0 ];
        fscanf( fp, "%i", &nlavf0[i] );
        for( int i_ = 0; i_ < nlavf0[i]; i_++ ) fscanf( fp, "%d", &lavf0[i][i_] );
    }
    CHECK_MARKER( "vert1faces" );
    t = (int*)malloc( nv1 * mnlavf1 * sizeof(int) );
    for( int i = 0; i < nv1; i++ )
    {
        lavf1[i] = &t[ i * mnlavf1 ];
        fscanf( fp, "%i", &nlavf1[i] );
        for( int i_ = 0; i_ < nlavf1[i]; i_++ ) fscanf( fp, "%d", &lavf1[i][i_] );
    }
    
    // load face-face adjacencies
    CHECK_MARKER( "face0faces" );
    t = (int*)malloc( nf0 * mnlaff0 * sizeof(int) );
    for( int i = 0; i < nf0; i++ )
    {
        laff0[i] = &t[ i * mnlaff0 ];
        fscanf( fp, "%i", &nlaff0[i] );
        for( int i_ = 0; i_ < nlaff0[i]; i_++ ) fscanf( fp, "%d", &laff0[i][i_] );
    }
    CHECK_MARKER( "face1faces" );
    t = (int*)malloc( nf1 * mnlaff1 * sizeof(int) );
    for( int i = 0; i < nf1; i++ )
    {
        laff1[i] = &t[ i * mnlaff1 ];
        fscanf( fp, "%i", &nlaff1[i] );
        for( int i_ = 0; i_ < nlaff1[i]; i_++ ) fscanf( fp, "%d", &laff1[i][i_] );
    }
    
    // load initial matchings
    vmatch01 = (int*)malloc( nv0 * sizeof(int) );
    vmatch10 = (int*)malloc( nv1 * sizeof(int) );
    vmatch01_iter = (int*)malloc( nv0 * sizeof(int) );
    fmatch01 = (int*)malloc( nf0 * sizeof(int) );
    fmatch10 = (int*)malloc( nf1 * sizeof(int) );
    fmatch01_iter = (int*)malloc( nf0 * sizeof(int) );
    CHECK_MARKER( "vmatch01" );
    for( int iv0 = 0; iv0 < nv0; iv0++ ) fscanf( fp, "%i", &vmatch01[iv0] );
    CHECK_MARKER( "vmatch10" );
    for( int iv1 = 0; iv1 < nv1; iv1++ ) fscanf( fp, "%i", &vmatch10[iv1] );
    CHECK_MARKER( "vmatch01_iter" );
    for( int iv0 = 0; iv0 < nv0; iv0++ ) fscanf( fp, "%i", &vmatch01_iter[iv0] );
    CHECK_MARKER( "fmatch01" );
    for( int if0 = 0; if0 < nf0; if0++ ) fscanf( fp, "%i", &fmatch01[if0] );
    CHECK_MARKER( "fmatch10" );
    for( int if1 = 0; if1 < nf1; if1++ ) fscanf( fp, "%i", &fmatch10[if1] );
    CHECK_MARKER( "fmatch01_iter" );
    for( int if0 = 0; if0 < nf0; if0++ ) fscanf( fp, "%i", &fmatch01_iter[if0] );
    
    CHECK_MARKER( "iter_current" );
    fscanf( fp, "%i", &iter_current );
    
    fscanf( fp, "%s", s ); marker.assign(s);
    if( marker.compare("end") != 0 ) { printf( "error loading: end != \"%s\"\n", marker.data() ); exit(1); }
    
    fclose(fp);
    
    printf( "  loaded!\n" );
}

void save_matching_data( const char *fname )
{
    FILE *fp = fopen( fname, "wt" );
    
    for( int iv0 = 0; iv0 < nv0; iv0++ )
        fprintf( fp, "%i\n", vmatch01[iv0] );
    for( int if0 = 0; if0 < nf0; if0++ )
        fprintf( fp, "%i\n", fmatch01[if0] );
    for( int iv0 = 0; iv0 < nv0; iv0++ )
        fprintf( fp, "%i\n", vmatch01_iter[iv0] );
    for( int if0 = 0; if0 < nf0; if0++ )
        fprintf( fp, "%i\n", fmatch01_iter[if0] );
    
    fclose(fp);
}


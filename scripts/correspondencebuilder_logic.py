from mesh import *
from debugwriter import *
from miscfuncs import *

class CorrespondenceBuilder_Logic(object):
    
    @staticmethod
    def remove_vert_matching_all_unmatched_faces( opts ):
        ci = opts['ci']
        DebugWriter.start( 'removing vert matches that have all unmatched faces' )
        count = 0
        for (iv0,v0) in ci.elv0():
            if iv0 not in ci.mv01: continue
            if any( if0 in ci.mf01 for if0 in ci.la_v0[iv0]['si_f'] ): continue
            ci.dcv0( iv0 )
            count += 1
        DebugWriter.report( 'count', count )
        DebugWriter.end()
    
    @staticmethod
    def remove_vert_matching_any_unmatched_faces( opts ):
        ci = opts['ci']
        DebugWriter.start( 'removing vert matches that have any unmatched faces' )
        count = 0
        for iv0,v0 in ci.elv0():
            if iv0 not in ci.mv01: continue
            si_f0 = ci.la_v0[iv0]['si_f']
            if all( if0 in ci.mf01 for if0 in si_f0 ): continue
            ci.dcv0(iv0)
            count += 1
        DebugWriter.report( 'count', count )
        DebugWriter.end()
    
    @staticmethod
    def remove_face_matches_with_mismatched_verts( opts ):
        ci = opts['ci']
        m0,m1 = ci.m0,ci.m1
        DebugWriter.start( 'removing face matches that have mismatched verts' )
        for (if0,f0) in ci.elf0():
            if if0 not in ci.mf01: continue
            if1 = ci.mf01[if0]
            f1 = ci.lf1[if1]
            
            if not all( iv0 in ci.mv01 and ci.mv01[iv0] in m1._opt_get_f_li_v(f1) for iv0 in m0._opt_get_f_li_v(f0) ):
                ci.dcf0( if0 )
            
        DebugWriter.end()
    
    @staticmethod
    def remove_twisted_faces( opts ):
        ci = opts['ci']
        
        DebugWriter.start( 'removing twisted faces' )
        
        ivrem,ifrem = set(),set()
        
        # look for face->face with matching verts, but verts are shuffled
        for if0,f0 in ci.elf0():
            if if0 not in ci.mf01: continue
            if1 = ci.mf01[if0]
            f1 = ci.m1.lf[if1]
            liv0 = ci.m0._opt_get_f_li_v(f0)
            liv1 = ci.m1._opt_get_f_li_v(f1)
            liv01 = [ ci.mv01[iv0] for iv0 in ci.m0._opt_get_f_li_v(f0) if iv0 in ci.mv01 ]
            if len(liv01) != len(liv0) or len(liv01) != len(liv1): continue
            if not all( iv01 in liv1 for iv01 in liv01 ): continue                                                      # only check exact match (no mismatch)
            liv01r = list(liv01).reverse()
            cliv1 = len(liv1)
            liv1d = liv1 + liv1
            if any( liv1d[i:i+cliv1] == liv01 or liv1d[i:i+cliv1] == liv01r for i in xrange(cliv1) ): continue
            
            ivrem |= set( liv0 )
            ifrem.add( if0 )
        
        for if0 in ifrem:
           for iv0 in ci.m0._opt_get_f_li_v(if0):
               if iv0 in ci.mv01: ci.dcv0(iv0)
           for iv1 in ci.m1._opt_get_f_li_v(ci.mf01[if0]):
               if iv1 in ci.mv10: ci.dcv1(iv1)
           ci.dcf0(if0)
        
        for iv0 in ivrem:
            if iv0 not in ci.mv01: continue
            ci.dcv0(iv0)
        
        DebugWriter.end()
    
    @staticmethod
    def remove_mismatched( opts ):
        ci = opts['ci']
        
        DebugWriter.start( 'removing mismatched adjacencies' )
        
        l_v01 = [ (i_v0,i_v1,ci.la_v0[i_v0],ci.la_v1[i_v1]) for (i_v0,i_v1) in ci.mv01.iteritems() ]
        l_v10 = [ (i_v0,i_v1,ci.la_v0[i_v0],ci.la_v1[i_v1]) for (i_v1,i_v0) in ci.mv10.iteritems() ]
        l_f01 = [ (i_f0,i_f1,ci.la_f0[i_f0],ci.la_f1[i_f1]) for (i_f0,i_f1) in ci.mf01.iteritems() ]
        l_f10 = [ (i_f0,i_f1,ci.la_f0[i_f0],ci.la_f1[i_f1]) for (i_f1,i_f0) in ci.mf10.iteritems() ]
        
        ivrem0 = set( iv0 for (iv0,iv1,da_v0,da_v1) in l_v01 if
            ( not all ( ci.mv10[iav1] in da_v0['si_v'] for iav1 in da_v1['si_v'] if iav1 in ci.mv10 ) )
            or
            ( not all ( ci.mf10[iaf1] in da_v0['si_f'] for iaf1 in da_v1['si_f'] if iaf1 in ci.mf10 ) )
            )
        
        ifrem0 = set( if0 for (if0,if1,da_f0,da_f1) in l_f01 if
            ( not all ( ci.mv10[iav1] in da_f0['si_v'] for iav1 in da_f1['si_v'] if iav1 in ci.mv10 ) )
            or
            ( not all ( ci.mf10[iaf1] in da_f0['si_f'] for iaf1 in da_f1['si_f'] if iaf1 in ci.mf10 ) )
            )
        
        for iv0 in ivrem0:
            ci.dcv0(iv0)
        for if0 in ifrem0:
            ci.dcf0(if0)
        
        DebugWriter.end()
    
    @staticmethod
    def remove_mismatched_old( opts ):
        ci = opts['ci']
        verts = opts.setdefault( 'verts', False )
        
        DebugWriter.start( 'removing mismatched geometry' )
        
        ivrem = set()
        ifrem = set()
        
        #rem = set( if0 for if0,f0 in ci.elf0() if
                #if0 in ci.mf01 and
                #(
                    #( not all( iv0 in ci.mv01 for iv0 in ci.m0._opt_get_f_li_v(f0) ) )
                    #or ( if0 in ci.mf01 and not all( iv1 in ci.mv10 and ci.mv10[iv1] in ci.m0._opt_get_f_li_v(f0) for iv1 in ci.m1._opt_get_f_li_v(ci.mf01[if0]) ) )
                #)
            #)
        
            
        
        if verts:
            DebugWriter.start( 'verts...' )
            ivrem |= set( iv0 for iv0,v0 in ci.elv0() if
                    iv0 in ci.mv01 and
                    (
                        not all ( ci.mf10[if1] in ci.la_v0[iv0]['si_f'] for if1 in ci.la_v1[ci.mv01[iv0]]['si_f'] if if1 in ci.mf10 ) or
                        not all ( ci.mf01[if0] in ci.la_v1[ci.mv01[iv0]]['si_f'] for if0 in ci.la_v0[iv0]['si_f'] if if0 in ci.mf01 )
                        #( len(ci.la_v0[iv0]['si_f'] & set( ci.mf10[if1] for if1 in ci.la_v1[ci.mv01[iv0]]['si_f'] if if1 in ci.mf10 ) ) == 0 )
                    )
                )
            DebugWriter.end()
        
        DebugWriter.start( 'faces...' )
        ifrem |= set( if0 for if0,f0 in ci.elf0() if
                if0 in ci.mf01 and
                (
                    ( not all( iv0 in ci.mv01 for iv0 in ci.m0._opt_get_f_li_v(f0) ) )
                    or
                    ( not all( iv1 in ci.mv10 and ci.mv10[iv1] in ci.m0._opt_get_f_li_v(f0) for iv1 in ci.m1._opt_get_f_li_v(ci.mf01[if0]) ) )
                    or
                    ( not all( ci.mf10[if1] in ci.la_f0[if0]['si_f'] for if1 in ci.la_f1[ci.mf01[if0]]['si_f'] if if1 in ci.mf10 ) )
                )
            )
        DebugWriter.end()
        
        for if0 in ifrem:
           for iv0 in ci.m0._opt_get_f_li_v(if0):
               if iv0 in ci.mv01: ci.dcv0(iv0)
           for iv1 in ci.m1._opt_get_f_li_v(ci.mf01[if0]):
               if iv1 in ci.mv10: ci.dcv1(iv1)
           ci.dcf0(if0)
        
        for iv0 in ivrem:
            if iv0 not in ci.mv01: continue
            ci.dcv0(iv0)
        
        DebugWriter.end()
    
    
    @staticmethod
    def build_vertex_correspondences( opts ):
        ci = opts['ci']
        
        DebugWriter.start( "building vertex correspondences using corresponding faces" )
        
        for (iv0,v0) in ci.elv0():
            if iv0 in ci.mv01: continue
            # get adj faces
            lf0 = [ if0 for if0 in ci.la_v0[iv0]['si_f'] if if0 in ci.mf01 ]
            lf1 = [ ci.mf01[f0] for f0 in lf0 ]
            if len(lf1) == 0: continue
            sv0 = set( ci.m0._opt_get_f_li_v( ci.lf0[lf0.pop()] ) )
            for if0 in lf0: sv0 &= set( ci.m0._opt_get_f_li_v( ci.lf0[if0] ) )
            lv0 = [ v0 for v0 in sv0 if v0 in ci.mv01 ]
            #if iv0 not in lv0: continue #len(lv0) != 1 or lv0[0] != iv0: continue
            sv1 = set( ci.m1._opt_get_f_li_v( ci.lf1[lf1.pop()] ) )
            for if1 in lf1: sv1 &= set( ci.m1._opt_get_f_li_v( ci.lf1[if1] ) )
            lv1 = [ v1 for v1 in sv1 if v1 not in ci.mv10 ]
            if len(lv1) != 1: continue
            iv1 = lv1[0]
            #if iv1 in ci.mv10: continue
            ci.acv( iv0, iv1 )
        
        for (iv1,v1) in ci.elv1():
            if iv1 in ci.mv10: continue
            # get adj faces
            lf1 = [ if1 for if1 in ci.la_v1[iv1]['si_f'] if if1 in ci.mf10 ]
            lf0 = [ ci.mf10[f1] for f1 in lf1 ]
            if len(lf0) == 0: continue
            sv1 = set( ci.m1._opt_get_f_li_v( ci.lf1[lf1.pop()] ) )
            for if1 in lf1: sv1 &= set( ci.m1._opt_get_f_li_v( ci.lf1[if1] ) )
            lv1 = [ v1 for v1 in sv1 if v1 in ci.mv10 ]
            #if iv1 not in lv1: continue #len(lv1) != 1 or lv1[0] != iv1: continue
            sv0 = set( ci.m0._opt_get_f_li_v( ci.lf0[lf0.pop()] ) )
            for if0 in lf0: sv0 &= set( ci.m0._opt_get_f_li_v( ci.lf0[if0] ) )
            lv0 = [ v0 for v0 in sv0 if v0 not in ci.mv01 ]
            if len(lv0) != 1: continue
            iv0 = lv0[0]
            #if iv1 in ci.mv10: continue
            ci.acv( iv0, iv1 )
        
        #for (if0,f0) in ci.elf0():
            #if if0 not in ci.mf01: continue
            #if1 = ci.mf01[if0]
            #f1 = ci.lf1[if1]
            #lv0 = [ 
        
        DebugWriter.end()
    
    @staticmethod
    def build_face_correspondences( opts ):
        ci = opts['ci']
        
        DebugWriter.start( "building face correspondences (%ix%i)" % (ci.cf0-len(ci.mf01),ci.cf1-len(ci.mf10)) )
        
        # match face0 -> face1 if all verts of face0 match to all verts of face1
        # given face0 has verts0 and verts1 correspond to verts0, if face1 has verts1 then face0 -> face1
        lsf1 = [ set(f1.lu_v) for f1 in ci.lf1 ]
        for if0,f0 in enumerate(ci.lf0):
            if if0 in ci.mf01: continue                                             # alread has a match
            smv0 = set( i_v for i_v in ci.m0._get_f_li_v(if0) if i_v in ci.mv01 )   # get matched verts of face0
            if len(smv0) != len(f0.lu_v): continue                                  # not all verts are matched
            smv1 = set( ci.m1.lv[ci.mv01[i_v]].u_v for i_v in smv0 )                # get uids of matching verts in mesh1
            if smv1 not in lsf1: continue                                           # do verts belong to a face?
            if1 = lsf1.index( smv1 )                                                # get ind of face1
            
            if not same_lists(
                [ ci.mv01[ci.m0._opt_get_i_v(u_v)] for u_v in ci.lf0[if0].lu_v ],
                [ ci.m1._opt_get_i_v(u_v) for u_v in ci.lf1[if1].lu_v ],
                reverse_check=False,
                ): continue
            
            if if1 in ci.mf10: continue
            ci.acf( if0, if1)
        
        for if0,f0 in enumerate(ci.lf0):
            if if0 in ci.mf01: continue
            liv0 = ci.m0._opt_get_f_li_v( if0 )
            if not all( i_v in ci.mv01 for i_v in liv0 ): continue
            
            liv1 = [ ci.mv01[i_v] for i_v in liv0 ]
            lsf1 = [ ci.la_v1[i_v]['si_f'] for i_v in liv1 ]
            sf1 = lsf1[0]
            for sf in lsf1: sf1 &= sf
            if len(sf1) != 1: continue
            if1 = sf1.pop()
            if if1 in ci.mf10: continue
            ci.acf( if0, if1 )
        
        DebugWriter.end()
    
    @staticmethod
    def remove_smaller_connected_faces( opts ):
        ci = opts['ci']
        
        DebugWriter.start( 'removing smaller connected faces' )
        
        sdif0 = set()
        
        DebugWriter.start( 'building 0-1' )
        sscif0 = set()
        ltf0 = [ i not in ci.mf01 for i in xrange(ci.cf0) ]
        while not all(ltf0):
            #print( '    %i...' % len(sscif0) )
            scif0 = set()
            snif0 = set( [ [ i for i,tf0 in enumerate(ltf0) if not tf0 ][0] ] )
            while snif0:
                nif0 = snif0.pop()
                if ltf0[nif0]: continue
                ltf0[nif0] = True
                if nif0 not in ci.mf01: continue
                scif0.add( nif0 )
                snif0 |= ci.la_f0[nif0]['si_f']
            sscif0.add( frozenset(scif0) )
        DebugWriter.printer( '%i connected sets' % len(sscif0) )
        DebugWriter.report( 'sizes', [ len(scif0) for scif0 in sscif0 ] )
        # find largest
        lscif0 = max( sscif0, key=lambda x:len(x) )
        for if0 in xrange(ci.cf0):
            if if0 not in ci.mf01: continue
            if if0 in lscif0: continue
            sdif0.add( if0 )
        DebugWriter.qend()
        
        DebugWriter.start( 'building 1-0' )
        sscif1 = set()
        ltf1 = [ i not in ci.mf10 for i in xrange(ci.cf1) ]
        while not all(ltf1):
            #print( '    %i...' % len(sscif1) )
            scif1 = set()
            snif1 = set( [ [ i for i,tf1 in enumerate(ltf1) if not tf1 ][0] ] )
            while snif1:
                nif1 = snif1.pop()
                if ltf1[nif1]: continue
                ltf1[nif1] = True
                if nif1 not in ci.mf10: continue
                scif1.add( nif1 )
                snif1 |= ci.la_f1[nif1]['si_f']
            sscif1.add( frozenset(scif1) )
        DebugWriter.printer( '%i connected sets' % len(sscif1) )
        DebugWriter.report( 'sizes', [ len(scif1) for scif1 in sscif1 ] )
        # find largest
        lscif1 = max( sscif1, key=lambda x:len(x) )
        for if1 in xrange(ci.cf1):
            if if1 not in ci.mf10: continue
            if if1 in lscif1: continue
            sdif0.add( ci.mf10[if1] )
        DebugWriter.qend()
        
        for if0 in sdif0: ci.dcf0( if0 )
        
        DebugWriter.end()
    
    @staticmethod
    def remove_smaller_connected_faces_threshold( opts ):
        CBLogic = CorrespondenceBuilder_Logic
        
        ci = opts['ci']
        threshold = opts.setdefault( 'threshold', 0.01 )
        
        #cmin = min( min(ci.cf0,ci.cf1) * threshold, 1000 )
        
        DebugWriter.start( 'removing smaller connected faces threshold' )
        #DebugWriter.report( 'threshold', 'min( %0.2f * %d, 1000 ) = %d' % (threshold, min(ci.cf0,ci.cf1), cmin) )
        sdif0 = set()
        
        DebugWriter.start( 'building connected comps' )
        l_sif0_connected_comps = []
        ltf0 = [ False ] * ci.cf0
        while not all(ltf0):
            sif0 = set()
            snif0 = set( [ [ i for i,tf0 in enumerate(ltf0) if not tf0 ][0] ] )
            while snif0:
                nif0 = snif0.pop()
                if ltf0[nif0]: continue
                ltf0[nif0] = True
                sif0.add( nif0 )
                snif0 |= ci.la_f0[nif0]['si_f']
            l_sif0_connected_comps += [sif0]
        l_sif1_connected_comps = []
        ltf1 = [ False ] * ci.cf1
        while not all(ltf1):
            sif1 = set()
            snif1 = set( [ [ i for i,tf1 in enumerate(ltf1) if not tf1 ][0] ] )
            while snif1:
                nif1 = snif1.pop()
                if ltf1[nif1]: continue
                ltf1[nif1] = True
                sif1.add( nif1 )
                snif1 |= ci.la_f1[nif1]['si_f']
            l_sif1_connected_comps += [sif1]
        DebugWriter.end()
        
        DebugWriter.start( 'building 0-1' )
        ltf0 = [ i not in ci.mf01 for i in xrange(ci.cf0) ]
        c,cdel = 0,0
        while not all(ltf0):
            c += 1
            scif0 = set()
            snif0 = set( [ [ i for i,tf0 in enumerate(ltf0) if not tf0 ][0] ] )
            while snif0:
                nif0 = snif0.pop()
                if ltf0[nif0]: continue
                ltf0[nif0] = True
                if nif0 not in ci.mf01: continue
                scif0.add( nif0 )
                snif0 |= ci.la_f0[nif0]['si_f']
            
            # find connected comp this connected edit belongs to
            if0 = scif0.pop()
            scif0.add(if0)
            sif0 = None
            for _sif0 in l_sif0_connected_comps:
                if if0 in _sif0:
                    sif0 = _sif0
                    break
            assert sif0, "oh no!"
            
            if len(scif0) >= len(sif0) * threshold: continue
            
            # mark for deletion
            sdif0 |= scif0
            cdel += 1
        DebugWriter.printer( '%i connected comps' % len(l_sif0_connected_comps) )
        DebugWriter.printer( 'deleting %i / %i connected edits' % (cdel,c) )
        DebugWriter.qend()
        
        DebugWriter.start( 'building 1-0' )
        ltf1 = [ i not in ci.mf10 for i in xrange(ci.cf1) ]
        c,cdel = 0,0
        while not all(ltf1):
            c += 1
            scif1 = set()
            snif1 = set( [ [ i for i,tf1 in enumerate(ltf1) if not tf1 ][0] ] )
            while snif1:
                nif1 = snif1.pop()
                if ltf1[nif1]: continue
                ltf1[nif1] = True
                if nif1 not in ci.mf10: continue
                scif1.add( nif1 )
                snif1 |= ci.la_f1[nif1]['si_f']
            
            # find connected comp this connected edit belongs to
            if1 = scif1.pop()
            scif1.add(if1)
            sif1 = None
            for _sif1 in l_sif1_connected_comps:
                if if1 in _sif1:
                    sif1 = _sif1
                    break
            assert sif1, "oh no!"
            
            if len(scif1) >= len(sif1) * threshold: continue
            
            # mark for deletion
            sdif0 |= set( ci.mf10[if1] for if1 in scif1 )
            cdel += 1
        DebugWriter.printer( '%i connected comps' % len(l_sif1_connected_comps) )
        DebugWriter.printer( 'deleting %i / %i connected edits' % (cdel,c) )
        DebugWriter.qend()
        
        DebugWriter.printer( 'deleting %i faces' % len(sdif0) )
        for if0 in sdif0: ci.dcf0( if0 )
        
        DebugWriter.end()
    
    @staticmethod
    def remove_face_conflicts( opts ):
        ci = opts['ci']
        per = opts.setdefault( 'per', 0.25 )
        
        DebugWriter.start( 'removing heavily conflicting face correspondences' )
        
        # if the corresponding faces to the adj faces of f0 are not adj to f1, remove f0->f1
        sdif0 = set()
        for if0,f0 in ci.elf0():
            if if0 not in ci.mf01: continue
            if1 = ci.mf01[if0]
            
            liaf0 = [ iaf0 for iaf0 in ci.la_f0[if0]['si_f'] if iaf0 in ci.mf01 ]
            liaf1 = [ ci.mf01[iaf0] for iaf0 in liaf0 ]
            if len(liaf1) == 0: continue
            
            #sif1 = ci.la_f1[liaf1[0]]['si_f']
            #for iaf1 in liaf1: sif1 &= ci.la_f1[iaf1]['si_f']
            #if if1 in sif1: continue
            
            if float(sum([ (if1 in ci.la_f1[iaf1]['si_f']) for iaf1 in liaf1])) / float(len(liaf1)) >= per: continue
            
            sdif0.add(if0)
        for if1,f1 in ci.elf1():
            if if1 not in ci.mf10: continue
            if0 = ci.mf10[if1]
            
            liaf1 = [ iaf1 for iaf1 in ci.la_f1[if1]['si_f'] if iaf1 in ci.mf10 ]
            liaf0 = [ ci.mf10[iaf1] for iaf1 in liaf1 ]
            if len(liaf0) == 0: continue
            
            #sif0 = ci.la_f0[liaf0[0]]['si_f']
            #for iaf0 in liaf0: sif0 &= ci.la_f0[iaf0]['si_f']
            #if if0 in sif0: continue
            
            if float(sum([ (if0 in ci.la_f0[iaf0]['si_f']) for iaf0 in liaf0])) / float(len(liaf0)) >= per: continue
            
            sdif0.add(if0)
        
        for if0 in sdif0: ci.dcf0( if0 )
        
        DebugWriter.end()
    
    @staticmethod
    def propagate_face_correspondences( opts ):
        ci = opts['ci']
        
        DebugWriter.start( 'propagating face correspondences' )
        
        m0,m1 = ci.m0,ci.m1
        m0.optimize()
        m1.optimize()
        
        stats0,stats1 = m0.get_stats(), m1.get_stats()
        adj_e0,adj_f0,mra0,lring_sf0 = [ stats0[k] for k in ['adj_e','adj_f','mra','lring_sf'] ]
        adj_e1,adj_f1,mra1,lring_sf1 = [ stats1[k] for k in ['adj_e','adj_f','mra','lring_sf'] ]
        len0,len1 = len(lring_sf0),len(lring_sf1)
        llcv0 = [ [ Vertex.average( m0.opt_get_uf_lv(u_f) ) for u_f in ring_sf0 ] for ring_sf0 in lring_sf0 ]
        llcv1 = [ [ Vertex.average( m1.opt_get_uf_lv(u_f) ) for u_f in ring_sf1 ] for ring_sf1 in lring_sf1 ]
        
        changed = True
        runs = 0
        while changed and runs < 5: # and runs < 50:
            changed = False
            runs += 1
            
            for if0,f0 in ci.elf0():
                if if0 in ci.mf01: continue
                sufa0 = adj_f0[f0.u_f]['su_f']
                lifa0 = [ m0._opt_get_i_f(u_f) for u_f in sufa0 if m0._opt_get_i_f(u_f) in ci.mf01 ]
                lifa1 = [ ci.mf01[i_f] for i_f in lifa0 ]
                #if not lifa1: continue
                if len(lifa1) < ( 2 if runs < 5 else 1 ): continue
                
                # make sure intersection of adj's adj faces is current
                su_f = set([ u_f for u_f in adj_f0[ci.lf0[lifa0[0]].u_f]['su_afe'] if m0._opt_get_i_f(u_f) not in ci.mf01 ])
                for i_f in lifa0:
                    su_f &= set([ u_f for u_f in adj_f0[ci.lf0[i_f].u_f]['su_afe'] if m0._opt_get_i_f(u_f) not in ci.mf01 ])
                if len(su_f) != 1 or su_f.pop() != f0.u_f: continue
                
                su_f = set([ u_f for u_f in adj_f1[ci.lf1[lifa1[0]].u_f]['su_afe'] if m1._opt_get_i_f(u_f) not in ci.mf10 ])
                for i_f in lifa1:
                    su_f &= set([ u_f for u_f in adj_f1[ci.lf1[i_f].u_f]['su_afe'] if m1._opt_get_i_f(u_f) not in ci.mf10 ])
                if len(su_f) != 1: continue
                
                if1 = m1._opt_get_i_f( su_f.pop() )
                if if1 in ci.mf10: continue
                
                ci.acf( if0, if1 )
                changed = True
            
            for if1,f1 in ci.elf1():
                if if1 in ci.mf10: continue
                sufa1 = adj_f1[f1.u_f]['su_f']
                lifa1 = [ m1._opt_get_i_f(u_f) for u_f in sufa1 if m1._opt_get_i_f(u_f) in ci.mf10 ]
                lifa0 = [ ci.mf10[i_f] for i_f in lifa1 ]
                #if not lifa0: continue
                if len(lifa0) < ( 2 if runs < 5 else 1): continue
                
                # make sure intersection of adj's adj faces is current
                su_f = set([ u_f for u_f in adj_f1[ci.lf1[lifa1[0]].u_f]['su_afe'] if m1._opt_get_i_f(u_f) not in ci.mf10 ])
                for i_f in lifa1:
                    su_f &= set([ u_f for u_f in adj_f1[ci.lf1[i_f].u_f]['su_afe'] if m1._opt_get_i_f(u_f) not in ci.mf10 ])
                if len(su_f) != 1 or su_f.pop() != f1.u_f: continue
                
                su_f = set([ u_f for u_f in adj_f0[ci.lf0[lifa0[0]].u_f]['su_afe'] if m0._opt_get_i_f(u_f) not in ci.mf01 ])
                for i_f in lifa0:
                    su_f &= set([ u_f for u_f in adj_f0[ci.lf0[i_f].u_f]['su_afe'] if m0._opt_get_i_f(u_f) not in ci.mf01 ])
                if len(su_f) != 1: continue
                
                if0 = m0._opt_get_i_f( su_f.pop() )
                if if0 in ci.mf01: continue
                
                ci.acf( if0, if1 )
                changed = True
        
        DebugWriter.end()
    
    @staticmethod
    def build_vertex_correspondences_faces( opts ):
        ci = opts['ci']
        
        DebugWriter.start( 'building vertex correspondences by using face correspondences' )
        
        # TODO: check both ways?
        
        count = 0
        for (iv0,v0) in ci.elv0():
            if iv0 in ci.mv01: continue
            la_f0 = ci.la_v0[iv0]['si_f']
            la_f1 = [ ci.mf01[i_f0] for i_f0 in la_f0 if i_f0 in ci.mf01 ]
            if not la_f1: continue
            if1 = la_f1.pop()
            sv1 = set( ci.m1._opt_get_f_li_v( ci.lf1[if1] ) )
            for if1 in la_f1: sv1 &= set( ci.m1._opt_get_f_li_v( ci.lf1[if1] ) )
            lv1 = [ iv1 for iv1 in sv1 if iv1 not in ci.mv10 ]
            if len(lv1) != 1: continue
            iv1 = lv1[0]
            if iv1 in ci.mv10: continue
            ci.acv( iv0, iv1 )
            count += 1
        
        DebugWriter.printer( '%i vertices added' % count )
        
        DebugWriter.end()




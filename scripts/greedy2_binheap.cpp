#include <iostream>
#include <vector>
#include <set>
#include <string>

#include <math.h>
#include <memory.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "mesh.h"
#include "meshop.h"
#include "binheap.h"

using namespace std;

typedef BinaryHeapNode<MeshOp*,float> BHNMeshOp;
typedef BinaryHeap<MeshOp*,float> BHMeshOp;

// forward declarations
void initialize_meshop();
float build_correspondence_greedy();

float compute_neighborhood_cost( int i0 );
void recompute_cost_of_pulled();
float compute_op_cost( MeshOp *meshop );

#define TIMER_INIT()            clock_t __c;
#define TIMER_START()           __c = clock();
#define TIMER_DELTA()           ((double)(clock() - __c) / (double)CLOCKS_PER_SEC)
#define TIME_FN(fn)             TIMER_START(); fn; printf("  %.2lfs\n",TIMER_DELTA());

#define INSERT( i0, i1, adddel )                                            \
            if( MeshOp::get_meshop(i0,i1)==0 ) {                            \
                MeshOp *__m = MeshOp::create(i0,i1,adddel);                 \
                __m->extra = bh_ops.insert( __m, compute_op_cost( __m ) );  \
            }
#define INSERTV( iv0, iv1, adddel )                                             \
            if( ( adddel && vmatch01[iv0] == -1 && vmatch10[iv1] == -1 ) ||     \
                ( !adddel && vmatch01[iv0] == iv1 && vmatch10[iv1] == iv0 ) )   \
                  INSERT( iv0, iv1, adddel );
#define INSERTF( if0, if1, adddel )                                             \
            if( ( adddel && fmatch01[if0] == -1 && fmatch10[if1] == -1 ) ||     \
                ( !adddel && fmatch01[if0] == if1 && fmatch10[if1] == if0 ) )   \
                  INSERT( if0+nv0, if1+nv1, adddel );

#define ALLOW_DELETION
//#define ALLOW_SWAP


BHMeshOp bh_ops;

int main( int argc, char **argv )
{
    float cost;
    TIMER_INIT();
    
#ifdef ALLOW_DELETION
    printf( "ALLOW: DELETION\n" );
#endif
#ifdef ALLOW_SWAP
    printf( "ALLOW: SWAPPING\n" );
#endif
    
    printf( "loading...\n" );
    TIME_FN( load_mesh_data( "greedy_input.txt" ) );
    printf( "computing knn...\n" );
    TIME_FN( compute_knn() );
    printf( "initializing...\n" );
    TIME_FN( initialize_meshop() );
    printf( "building...\n" );
    TIME_FN( cost = build_correspondence_greedy() );
    printf( "  cost = %f\n", cost );
    printf( "saving...\n" );
    TIME_FN( save_matching_data( "greedy_output.txt" ) );
    printf( "leaving!\n" );
    
    return 0;
}

void add_v0_potentials( int iv0 )
{
    int iv1 = vmatch01[iv0];
    
    if( iv1 != -1 ) {
#ifdef ALLOW_DELETION
        INSERTV( iv0, iv1, false );
#endif
        return;
    }
    
    // for each k-nn vert' to vert: potentially vert -> vert'
    for( int i = 0; i < nlsiv01[iv0]; i++ ) INSERTV( iv0, lsiv01[iv0][i], true );
    
    for( int i = 0; i < nlavv0[iv0]; i++ )
    {
        int iav0 = lavv0[iv0][i];
        int iav1 = vmatch01[iav0];
        if( iav1 == -1 ) continue;
        for( int j = 0; j < nlavv1[iav1]; j++ ) INSERTV( iv0, lavv1[iav1][j], true );
    }
    
    // for each face where vert in face: potentially vert -> vert' where vert' in face' and face -> face'
    for( int i = 0; i < nlavf0[iv0]; i++ )
    {
        int iaf0 = lavf0[iv0][i];
        int iaf1 = fmatch01[iaf0];
        if( iaf1 == -1 ) continue;
        for( int j = 0; j < nlifv1[iaf1]; j++ ) INSERTV( iv0, lifv1[iaf1][j], true );
    }
}

void add_v1_potentials( int iv1 )
{
    int iv0 = vmatch10[iv1];
    
    if( iv0 != -1 ) {
#ifdef ALLOW_DELETION
        INSERTV( iv0, iv1, false );
#endif
        return;
    }
    
    // for each k-nn vert' to vert: potentially vert -> vert'
    for( int i = 0; i < nlsiv10[iv1]; i++ ) INSERTV( lsiv10[iv1][i], iv1, true );
    
    for( int i = 0; i < nlavv1[iv1]; i++ )
    {
        int iav1 = lavv1[iv1][i];
        int iav0 = vmatch10[iav1];
        if( iav0 == -1 ) continue;
        for( int j = 0; j < nlavv0[iav0]; j++ ) INSERTV( lavv0[iav0][j], iv1, true );
    }
    
    // for each face where vert in face: potentially vert -> vert' where vert' in face' and face -> face'
    for( int i = 0; i < nlavf1[iv1]; i++ )
    {
        int iaf1 = lavf1[iv1][i];
        int iaf0 = fmatch10[iaf1];
        if( iaf0 == -1 ) continue;
        for( int j = 0; j < nlifv0[iaf0]; j++ ) INSERTV( lifv0[iaf0][j], iv1, true );
    }
}

void add_f0_potentials( int if0 )
{
    int if1 = fmatch01[if0];
    
    if( if1 != -1 ) {
#ifdef ALLOW_DELETION
        INSERTF( if0, if1, false );
#endif
        return;
    }
    
    // add k-nn potentials
    for( int i = 0; i < nlsif01[if0]; i++ )
    {
        int ipf1 = lsif01[if0][i];
        INSERTF( if0, ipf1, true );
    }
    
    // use adjacencies
    for( int i = 0; i < nlifv0[if0]; i++ )
    {
        int iav0 = lifv0[if0][i];
        int iav1 = vmatch01[iav0];
        if( iav1 == -1 ) continue;
        for( int j = 0; j < nlavf1[iav1]; j++ )
        {
            int ipf1 = lavf1[iav1][j];
            INSERTF( if0, ipf1, true );
        }
    }
    for( int i = 0; i < nlaff0[if0]; i++ )
    {
        int iaf0 = laff0[if0][i];
        int iaf1 = fmatch01[iaf0];
        if( iaf1 == -1 ) continue;
        for( int j = 0; j < nlaff1[iaf1]; j++ )
        {
            int ipf1 = laff1[iaf1][j];
            INSERTF( if0, ipf1, true );
        }
    }
}

void add_f1_potentials( int if1 )
{
    int if0 = fmatch10[if1];
    
    if( if0 != -1 ) {
#ifdef ALLOW_DELETION
        INSERTF( if0, if1, false );
#endif
        return;
    }
    
    // add k-nn potentials
    for( int i = 0; i < nlsif10[if1]; i++ )
    {
        int ipf0 = lsif10[if1][i];
        INSERTF( ipf0, if1, true );
    }
    
    // use adjacencies
    for( int i = 0; i < nlifv1[if1]; i++ )
    {
        int iav1 = lifv1[if1][i];
        int iav0 = vmatch10[iav1];
        if( iav0 == -1 ) continue;
        for( int j = 0; j < nlavf0[iav0]; j++ )
        {
            int ipf0 = lavf0[iav0][j];
            INSERTF( ipf0, if1, true );
        }
    }
    for( int i = 0; i < nlaff1[if1]; i++ )
    {
        int iaf1 = laff1[if1][i];
        int iaf0 = fmatch10[iaf1];
        if( iaf0 == -1 ) continue;
        for( int j = 0; j < nlaff0[iaf0]; j++ )
        {
            int ipf0 = laff0[iaf0][j];
            INSERTF( ipf0, if1, true );
        }
    }
}

void initialize_meshop()
{
    MeshOp::init( nv0+nf0, nv1+nf1 );
    
    printf( "  adding initial potential matches...\n" );
    printf( "    V0...\n" );
    for( int iv0 = 0; iv0 < nv0; iv0++ ) add_v0_potentials( iv0 );
    printf( "    V1...\n" );
    for( int iv1 = 0; iv1 < nv1; iv1++ ) add_v1_potentials( iv1 );
    printf( "    F0...\n" );
    for( int if0 = 0; if0 < nf0; if0++ ) add_f0_potentials( if0 );
    printf( "    F1...\n" );
    for( int if1 = 0; if1 < nf1; if1++ ) add_f1_potentials( if1 );
    
    MeshOp::clear_pull();
}

void kill_i0( int i0 )
{
    MeshOp *m = MeshOp::get_i0_head( i0 );
    while( m ) {
        MeshOp *nm = m->get_n_i0();
        m->remove_i0();
        m->remove_i1();
        if( m->extra != 0 ) {
            bh_ops.remove( (BHNMeshOp*)m->extra );
            delete (BHNMeshOp*)m->extra;
            m->extra = 0;
        }
        m = nm;
    }
    MeshOp::clear_pull();
}
void kill_i1( int i1 )
{
    MeshOp *m = MeshOp::get_i1_head( i1 );
    while( m ) {
        MeshOp *nm = m->get_n_i1();
        m->remove_i0();
        m->remove_i1();
        if( m->extra != 0 ) {
            bh_ops.remove( (BHNMeshOp*)m->extra );
            delete (BHNMeshOp*)m->extra;
            m->extra = 0;
        }
        m = nm;
    }
    MeshOp::clear_pull();
}

float build_correspondence_greedy()
{
    double t_kill   = 0.0;
    double t_recalc = 0.0;
    double t_pull   = 0.0;
    double t_insert = 0.0;
    int iterations  = 0;
    
    TIMER_INIT();
    
    MeshOp *meshop, *m;
    BHNMeshOp *node;
    
    while( !bh_ops.empty() )
    {
        node = bh_ops.removeMinimum();
        meshop = node->data();
        
        if( meshop->get_cost() >= 0 ) break;
        
        int i0 = meshop->get_i0();
        int i1 = meshop->get_i1();
        bool adddel = meshop->get_adddel();
        
        // kill other potentials of iv0 or iv1
        TIMER_START();
        kill_i0( i0 );
        kill_i1( i1 );
        t_kill += TIMER_DELTA();
        
#ifndef ALLOW_DELETION
        if( adddel == false ) continue;
#endif
        
        if( i0 < nv0 ) {
            
            int iv0 = i0;
            int iv1 = i1;
            
            // assertion
            // TODO: should this be throwing errors?
            if( adddel ) {
                if( vmatch01[iv0] != -1 || vmatch10[iv1] != -1 ) {
                    printf( "error while adding vert %d <-> %d:\n", iv0, iv1 );
                    printf( "  %d <-> %d\n", iv0, iv1 );
                    printf( "  vmatch01[%d] = %d\n", iv0, vmatch01[iv0] );
                    printf( "  vmatch10[%d] = %d\n", iv1, vmatch10[iv1] );
                    //exit(1);
                    continue;
                } else {
                    //printf( "adding vert %d <-> %d\n", iv0, iv1 );
                }
            } else {
                if( vmatch01[iv0] != iv1 || vmatch10[iv1] != iv0 ) {
                    printf( "error while deleting vert %d x %d:\n", iv0, iv1 );
                    printf( "  %d <x> %d\n", iv0, iv1 );
                    printf( "  vmatch01[%d] = %d != %d\n", iv0, vmatch01[iv0], iv1 );
                    printf( "  vmatch10[%d] = %d != %d\n", iv1, vmatch10[iv1], iv0 );
                    exit(1);
                    continue;
                } else {
                    //printf( "deling vert %d  x  %d\n", iv0, iv1 );
                }
            }
            
            // perform it!
            if( adddel ) {
                vm01_add( iv0, iv1 );
            } else {
                vm01_del( iv0, iv1 );
            }
            
            // add countering changes
            add_v0_potentials( iv0 );
            add_v1_potentials( iv1 );
            
            MeshOp::clear_pull();
            
            // pull neighborhood for cost recomputation
            TIMER_START();
            int *avv0 = lavv0[iv0], navv0 = nlavv0[iv0];
            int *avv1 = lavv1[iv1], navv1 = nlavv1[iv1];
            int *avf0 = lavf0[iv0], navf0 = nlavf0[iv0];
            int *avf1 = lavf1[iv1], navf1 = nlavf1[iv1];
            for( int i = 0; i < navv0; i++ ) MeshOp::pull_i0( avv0[i] );
            for( int i = 1; i < navv1; i++ ) MeshOp::pull_i1( avv1[i] );
            for( int i = 0; i < navf0; i++ ) MeshOp::pull_i0( avf0[i]+nv0 );
            for( int i = 0; i < navf1; i++ ) MeshOp::pull_i1( avf1[i]+nv1 );
            t_pull += TIMER_DELTA();
            
        } else {
            
            int if0 = i0 - nv0;
            int if1 = i1 - nv1;
            
            if( adddel ) {
                if( fmatch01[if0] != -1 || fmatch10[if1] != -1 ) {
                    printf( "error while adding face %d <-> %d:\n", if0, if1 );
                    printf( "  fmatch01[%d] = %d\n", if0, fmatch01[if0] );
                    printf( "  fmatch10[%d] = %d\n", if1, fmatch10[if1] );
                    //exit(1);
                    continue;
                } else {
                    //printf( "adding face %d <-> %d\n", if0, if1 );
                }
            } else {
                if( fmatch01[if0] != if1 || fmatch10[if1] != if0 ) {
                    printf( "error while deleting face %d x %d:\n", if0, if1 );
                    printf( "  fmatch01[%d] = %d != %d\n", if0, fmatch01[if0], if1 );
                    printf( "  fmatch10[%d] = %d != %d\n", if1, fmatch10[if1], if0 );
                    exit(1);
                    continue;
                } else {
                    //printf( "deling face %d  x  %d\n", if0, if1 );
                }
            }
            
            if( adddel ) {
                fm01_add( if0, if1 );
            } else {
                fm01_del( if0, if1 );
            }
            
            // add countering changes
            add_f0_potentials( if0 );
            add_f1_potentials( if1 );
            
            MeshOp::clear_pull();
            
            // pull for cost recomputation
            TIMER_START();
            int *afv0 = lifv0[if0], nafv0 = nlifv0[if0];
            int *afv1 = lifv1[if1], nafv1 = nlifv1[if1];
            int *aff0 = laff0[if0], naff0 = nlaff0[if0];
            int *aff1 = laff1[if1], naff1 = nlaff1[if1];
            for( int i = 0; i < nafv0; i++ ) MeshOp::pull_i0( afv0[i] );
            for( int i = 0; i < nafv1; i++ ) MeshOp::pull_i1( afv1[i] );
            for( int i = 0; i < naff0; i++ ) MeshOp::pull_i0( aff0[i]+nv0 );
            for( int i = 0; i < naff1; i++ ) MeshOp::pull_i1( aff1[i]+nv1 );
            t_pull += TIMER_DELTA();
            
        }
        
        TIMER_START();
        for( m = MeshOp::get_pull_head(); m != 0; m = m->get_n_pull() )
        {
            bh_ops.remove( (BHNMeshOp*)m->extra );
            delete (BHNMeshOp*)m->extra;
        }
        recompute_cost_of_pulled();
        t_recalc += TIMER_DELTA();
        
        TIMER_START();
        for( m = MeshOp::get_pull_head(); m != 0; m = m->get_n_pull() )
        {
            m->extra = bh_ops.insert( m, m->get_cost() );
        }
        t_insert += TIMER_DELTA();
        
        MeshOp::clear_pull();
        
        iterations++;
    }
    
    printf( "  t_kill     = %.2lfs\n", t_kill );
    printf( "  t_pull     = %.2lfs\n", t_pull );
    printf( "  t_recalc   = %.2lfs\n", t_recalc );
    printf( "  t_insert   = %.2lfs\n", t_insert );
    printf( "  iterations = %d\n", iterations );
    
    return 0.0f;
}




bool is_in( int *list, int size, int find )
{
    for( int i = 0; i < size; i++ ) if( list[i] == find ) return true;
    return false;
}


float compute_cost(
    int iv0,
    int *vmatch01, int *fmatch01,
    float d_fn( int i0, int i1 ), float n_fn( int i0, int i1 ),
    float d_vv0_fn( int i0, int i1 ), float d_vf0_fn( int i0, int i1 ),
    float d_vv1_fn( int i0, int i1 ), float d_vf1_fn( int i0, int i1 ),
    int *nlavv0, int **lavv0, int *nlavf0, int **lavf0,
    int *nlavv1, int **lavv1, int *nlavf1, int **lavf1,
    int *nlafv0, int **lafv0, int *nlaff0, int **laff0 )
{
    float cost = 0.0f;
    float deg = nlavv0[iv0]+nlavf0[iv0];
    
    int iv1 = vmatch01[iv0];
    if( iv1 == -1 ) {
        // no match => deleted node
        cost += cost_vadd;
    } else {
        cost += cost_vdist * d_fn( iv0, iv1 ) / 2.0f;
        cost += cost_vnorm * n_fn( iv0, iv1 ) / 2.0f;
    }
    
    for( int i = 0; i < nlavv0[iv0]; i++ )
    {
        int iav0 = lavv0[iv0][i];
        float dega = nlavv0[iav0]+nlavf0[iav0];
        int iav1 = vmatch01[iav0];
        
        if( iv1 == -1 || iav1 == -1 ) {
            cost += cost_fvunk * 2.0 / ( deg+dega ); // / (deg+dega);
        } else {
            if( !is_in( lavv1[iv1], nlavv1[iv1], iav1 ) )
                cost += cost_fvmis * 2.0 / ( deg+dega ); // / (deg+dega);
            
            float d0 = d_vv0_fn( iv0, iav0 );
            float d1 = d_vv1_fn( iv1, iav1 );
            cost += cost_fvdist * fabs( d0 - d1 ) / ( 1.0f + fabs( d0 - d1 ) );
        }
    }
    
    for( int i = 0; i < nlavf0[iv0]; i++ )
    {
        int iaf0 = lavf0[iv0][i];
        float dega = nlafv0[iaf0]+nlaff0[iaf0];
        int iaf1 = fmatch01[iaf0];
        
        if( iv1 == -1 || iaf1 == -1 ) {
            cost += cost_fvunk * 2.0 / ( deg+dega ); // / (deg+dega);
        } else {
            if( !is_in( lavf1[iv1], nlavf1[iv1], iaf1 ) )
                cost += cost_fvmis * 2.0 / ( deg+dega ); // / (deg+dega);
            
            float d0 = d_vf0_fn( iv0, iaf0 );
            float d1 = d_vf1_fn( iv1, iaf1 );
            cost += cost_fvdist * fabs( d0 - d1 ) / ( 1.0f + fabs( d0 - d1 ) );
        }
    }
    
    return cost;
}


void recompute_cost_of_pulled()
{
    for( MeshOp *m = MeshOp::get_pull_head(); m != 0; m = m->get_n_pull() )
       compute_op_cost( m );
}


float compute_op_cost( MeshOp *meshop )
{
    float cost = 0.0f;
    
    int i0 = meshop->get_i0();
    int i1 = meshop->get_i1();
    
    if( i0 < nv0 ) {
        
        int iv0 = i0;
        int iv1 = i1;
        
        cost -= compute_cost( iv0, vmatch01, fmatch01, v0v1_ddist, v0v1_norm, v0v0_ddist, v0f0_ddist, v1v1_ddist, v1f1_ddist, nlavv0, lavv0, nlavf0, lavf0, nlavv1, lavv1, nlavf1, lavf1, nlifv0, lifv0, nlaff0, laff0 );
        cost -= compute_cost( iv1, vmatch10, fmatch10, v1v0_ddist, v1v0_norm, v1v1_ddist, v1f1_ddist, v0v0_ddist, v0f0_ddist, nlavv1, lavv1, nlavf1, lavf1, nlavv0, lavv0, nlavf0, lavf0, nlifv1, lifv1, nlaff1, laff1 );
        
        if( meshop->get_adddel() ) { vmatch01[iv0] = iv1; vmatch10[iv1] = iv0; } else { vmatch01[iv0] = vmatch10[iv1] = -1; }
        
        cost += compute_cost( iv0, vmatch01, fmatch01, v0v1_ddist, v0v1_norm, v0v0_ddist, v0f0_ddist, v1v1_ddist, v1f1_ddist, nlavv0, lavv0, nlavf0, lavf0, nlavv1, lavv1, nlavf1, lavf1, nlifv0, lifv0, nlaff0, laff0 );
        cost += compute_cost( iv1, vmatch10, fmatch10, v1v0_ddist, v1v0_norm, v1v1_ddist, v1f1_ddist, v0v0_ddist, v0f0_ddist, nlavv1, lavv1, nlavf1, lavf1, nlavv0, lavv0, nlavf0, lavf0, nlifv1, lifv1, nlaff1, laff1 );
        
        if( meshop->get_adddel() ) { vmatch01[iv0] = vmatch10[iv1] = -1; } else { vmatch01[iv0] = iv1; vmatch10[iv1] = iv0; }
        
    } else {
        
        int if0 = i0 - nv0;
        int if1 = i1 - nv1;
        
        cost -= compute_cost( if0, fmatch01, vmatch01, f0f1_ddist, f0f1_norm, f0f0_ddist, f0v0_ddist, f1f1_ddist, f1v1_ddist, nlaff0, laff0, nlifv0, lifv0, nlaff1, laff1, nlifv1, lifv1, nlavf0, lavf0, nlavv0, lavv0 );
        cost -= compute_cost( if1, fmatch10, vmatch10, f1f0_ddist, f1f0_norm, f1f1_ddist, f1v1_ddist, f0f0_ddist, f0v0_ddist, nlaff1, laff1, nlifv1, lifv1, nlaff0, laff0, nlifv0, lifv0, nlavf1, lavf1, nlavv1, lavv1 );
        
        if( meshop->get_adddel() ) { fmatch01[if0] = if1; fmatch10[if1] = if0; } else { fmatch01[if0] = fmatch10[if1] = -1; }
        
        cost += compute_cost( if0, fmatch01, vmatch01, f0f1_ddist, f0f1_norm, f0f0_ddist, f0v0_ddist, f1f1_ddist, f1v1_ddist, nlaff0, laff0, nlifv0, lifv0, nlaff1, laff1, nlifv1, lifv1, nlavf0, lavf0, nlavv0, lavv0 );
        cost += compute_cost( if1, fmatch10, vmatch10, f1f0_ddist, f1f0_norm, f1f1_ddist, f1v1_ddist, f0f0_ddist, f0v0_ddist, nlaff1, laff1, nlifv1, lifv1, nlaff0, laff0, nlifv0, lifv0, nlavf1, lavf1, nlavv1, lavv1 );
        
        if( meshop->get_adddel() ) { fmatch01[if0] = fmatch10[if1] = -1; } else { fmatch01[if0] = if1; fmatch10[if1] = if0; }
    }
    
    meshop->set_cost( cost );
    return cost;
}












import sys, subprocess, os, scipy.io

from debugwriter import DebugWriter
from mesh import *
from vec3f import *
from hungarian import *

from correspondencebuilder_logic import CorrespondenceBuilder_Logic as CB_Logic
from correspondencebuilder_hungarian import CorrespondenceBuilder_Hungarian as CB_Hungarian


class CorrespondenceBuilder_ICPGraphCuts(object):
    
    @staticmethod
    def build_verts( opts ):
        DebugWriter.start( 'loading ICP+Graph Cuts matching' )
        
        ci = opts['ci']
        m0,m1 = ci.m0,ci.m1
        nv0,nv1 = ci.cv0,ci.cv1
        
        opts_defaults = {
            'threshold' : float('inf'),
            'cost_add'  : 2.5,
            'cost_del'  : 2.5,
            'cost_move' : 1.0,
            'cost_turn' : 1.0,
            }
        opts = dict_set_defaults( opts, opts_defaults )
        
        threshold = opts['threshold']
        cost_add = opts['cost_add']
        cost_del = opts['cost_del']
        cost_move = opts['cost_move']
        cost_turn = opts['cost_turn']
        
        # load potential matchings from m0 -> m1
        if os.path.exists( 'durano03_durano04_mod.icpmap' ):
            fn = 'durano03_durano04_mod.icpmap'
        else:
            fn = 'durano02_durano13.icpmap'
        fp = open( fn, 'rt' )
        lpv0 = [ [ float(s) for s in l.split(' ') ] for l in fp.read().split('\n') if l ]
        fp.close()
        
        assert len(lpv0) == nv0
        
        DebugWriter.start( 'building %i^2 (%i+%i) cost matrix' % (ci.cv0+ci.cv1,ci.cv0,ci.cv1) )
        
        # init distance matrix to be 0
        m = np.zeros( (ci.cv0+ci.cv1, ci.cv0+ci.cv1) )
        
        # Q1 cost: v0 -> @
        for (i,v0,j,v1) in ci.evq1():
            m[i][j] = cost_add if i==(j-ci.cv1) and i not in ci.mv01 else float('inf')
        
        # Q2 cost: v0 -> v1
        for (i,v0,j,v1) in ci.evq2():
            if i in ci.mv01 or j in ci.mv10:
                m[i][j] = 0.0 if i in ci.mv01 and ci.mv01[i] == j and j in ci.mv10 and ci.mv10[j] == i else float('inf')
            else:
                n0,n1 = ci.ln0[i],ci.ln1[j]
                dv2 = Vertex.distance( v0, v1 )
                dv2 = dv2 / (1.0 + dv2)
                dn2 = 1.0 - Vec3f.t_dot( n0 ,n1 )
                cost = dv2 * cost_move + dn2 * cost_turn
                m[i][j] = cost if cost < threshold else float('inf')
        
        # Q3 cost: @ -> v1
        for (i,v0,j,v1) in ci.evq3():
            m[i][j] = cost_del if (i-ci.cv0)==j and j not in ci.mv10 else float('inf')
        
        DebugWriter.end()
        
        DebugWriter.start( 'running' )
        #inds = hungarian( np.array( m ) )
        inds = hungarian( m )
        DebugWriter.end()
        
        for i,j in enumerate(inds):
            if i >= ci.cv0 or j >= ci.cv1: continue
            if i in ci.mv01 or j in ci.mv10:
                assert ci.mv01[i] == j, 'ci.mv01[%i] == %i != %i' % (i,ci.mv01[i],j)
                assert ci.mv10[j] == i, 'ci.mv10[%i] == %i != %i' % (j,ci.mv10[j],i)
            else:
                ci.acv( i, j )

        
        #CB_Logic.build_face_correspondences( opts )
        CB_Hungarian.build_face_correspondences_hungarian_verts( opts )
        #CB_Hungarian.build_face_correspondences_hungarian_verts( opts )
        
        DebugWriter.end()

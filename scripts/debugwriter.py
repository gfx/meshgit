import os, sys, time

class DebugWriter(object):
    spaces = ' '*2
    nesting = []
    level = 5
    quiet_level = 0
    debug = False
    
    @classmethod
    def quiet( cls ):
        cls.quiet_level += 1
        return cls
    
    @classmethod
    def loud( cls ):
        cls.quiet_level -= 1
        return cls
    
    @classmethod
    def _print( cls, t ):
        if cls.quiet_level and not cls.debug: return
        print( t )
    
    @classmethod
    def spacetext( cls, text ):
        return '%s%s' % (cls.spaces*len(cls.nesting), text)
    
    @classmethod
    def printer( cls, text ):
        if '\n' in text:
            for t in text.split('\n'):
                cls.printer( t )
        else:
            cls._print( cls.spacetext( text ) )
        return cls
    
    @classmethod
    def start( cls, text, quiet=False ):
        if text[-1] != ':':
            cls.printer( '%s...' % text )
        else:
            cls.printer( text )
        
        if quiet: cls.quiet()
        
        cls.nesting += [ (text,time.time(),quiet) ]
        return cls
    
    @classmethod
    def end( cls, text=None, show_time=True ):
        stext,stime,quiet = cls.nesting[-1]
        
        if quiet: cls.loud()
        
        if text is not None:
            cls.printer( text )
        
        if show_time:
            cls.printer( 'finished: %f seconds' % (time.time() - stime) )
        
        cls.nesting.pop()
        return cls
    
    # quiet end
    @classmethod
    def qend( cls, text=None ):
        return cls.end( text=text, show_time=False)
    
    @classmethod
    def report( cls, label, val, space=20, dict_order=None ):
        fmt = {
            int:    '% 5d',
            float:  '% 8.2f'
            }
        
        if type(val) is dict:
            cls.start( '%s:' % label )
            if dict_order is None: dict_order = []
            sk = set(dict_order)
            dict_order += [ k for k in sorted(val.keys()) if k not in sk ] # in case dict_order doesn't contain all keys
            for k in dict_order: cls.report( k, val[k], space=space )
            cls.qend()
            return
        
        elif type(val) is list:     val = ', '.join( fmt.get(type(v),'%s')%(v,) for v in val )
        
        else: val = fmt.get(type(val),'%s')%(val,)
        
        slabel = cls.spacetext( label )
        space = ' '*max(0,space-len(slabel)-2)
        text = '%s: %s%s' % (slabel,space,val)
        cls._print( text )
        return cls
    
    @classmethod
    def divider( cls, char='*', count=80, lines=1 ):
        return cls.printer( '\n'.join([ char*count ] * lines) )
    
    @classmethod
    def blank( cls, lines=1 ):
        cls._print( '\n'*lines )
        return cls




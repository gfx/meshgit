#!/usr/bin/env python

import os, re, sys, math, random, itertools, pickle, numpy #, json
from pyglet.window import key

from mesh import *
from viewer import *
from shapes import *
from vec3f import *
from colors import *

from meshgit_misc import *
from meshgit_med_ops import *
from meshgit_diff import *
from meshgit_merge import *
from meshgit_experimental import *
from meshgit_git import *
from meshgit_view import *
from meshgit_match import *



def print_commands( *args, **opts ):
    global lcommands, ladditional
    
    specific = 'simple' if len(args) == 0 else args[0]
    
    if specific in ['simple','all']:
        boverride = (specific == 'all')
        print( 'Usage: %s [cmd] ...' % sys.argv[0] )
        print( 'Commands:' )
        for scmd,shelp,sahelp,nargs,shelpdetails,fn,bprint in lcommands:
            if not boverride and not bprint: continue
            #print( '  %s%s %s' % (scmd,' '*max(0,15-len(scmd)),sahelp ) )
            #for t in shelp.split('\n'):
               #print( '  %s %s' % (' '*15,t) )
            print( '  %s %s' % (scmd,sahelp ) )
            for t in shelp.split('\n'):
                print( '  %s%s' % (' '*10,t) )
            print( '' )
        print( '[] required, <> optional' )
    
    else:
        licmd = [ i for i,c in enumerate(lcommands) if c[0] == specific ]
        if not licmd:
            print( 'Unknown command %s' % specific )
            exit( 1 )
        for icmd in licmd:
            scmd,shelp,sahelp,shelpdetails,nargs,fn,bprint = lcommands[icmd]
            print( 'Help: %s %s %s' % (sys.argv[0],scmd,sahelp) )
            if shelpdetails: print( '%s' % shelpdetails )
            else: print( '%s' % shelp )
            print( '' )
    
    print( '' )
    for l in ladditional:
        print( l )



# lcommands is a list of tuples as commands that can be used, where each tuple has format:
#   ( 'command', 'help text to display', 'argument help text', 'detailed help', argument count, function to call, print? )
# command       : a string *WITHOUT* spaces!
# help text     : human-readable text describing the command (can contain \n :) )
# arg help      : human-readable text describing how the args should be specified
# detailed help : human-readable text describing in detail the command
# arg count     : an int or range of ints
# function      : the argument to call, passing args
# print         : a bool indicating if it should be printed to console using 'help' command
lcommands = [
    (
        'help',
        'Prints documented help for specified command, or this help list if none is specified.\nIf "all" is specified, all commands (including experimental) will be displayed.',
        '<command>',
        '',
        [0,1], print_commands, True,
    ),(
        'view',
        'Views the specified PLY files.\nIf specified, Catmull-Clark subdivision rules are applied level times to all meshes.',
        '[fn0] <fn1> <...> <subd=[level]>',
        '',
        xrange(1,100), run_view, True,
    ),(
        'view_skeleton',
        'Views the specified PLY files after skeletonization.',
        '[fn0] <fn1> <...>',
        '',
        xrange(1,100), run_view_skeleton, True,
    ),(
        'diff',
        'Visualizes the mesh differences from fn0->fn1',
        '[fn0] [fn1] <method=[method]>',
        'Visualizes the mesh differences from fn0->fn1 ',
        2, run_diff_viewer2, True,
    ),(
        'diff',
        'Visualizes the mesh differences from fn0->fn1 and fn0->fn2',
        '[fn0] [fn1] [fn2] <method=[method]>',
        'Visualizes the mesh differences from fn0->fn1 and fn0->fn2',
        3, run_diff_viewer3, True,
    ),(
        'merge',
        'Merges and visualizes changes from fn0->fn1 and fn0->fn2, saving to fn012.\nIf specified, Catmull-Clark subdivision rules are applied level times to merged mesh.',
        '[fn0] [fn1] [fn2] [fn012] <method=[method]> <subd=[level]>',
        '',
        4, run_merge_viewer, True,
    ),(
        'merge_combinations',
        'Merges and visualizes combinations of changes from fn0->fn1 and fn0->fn2.',
        '[fn0] [fn1] [fn2] <method=[method]>',
        '',
        3, run_merge_combinations, True,
    ),(
        'warp', 'EXPERIMENTAL', '[fn0] [fn1]', '',
        2, run_warp_view, False,
    ),(
        'strips', 'EXPERIMENTAL: Computes strip-graph of fn0', '[fn0]', '',
        1, run_strips_viewer, False,
    ),(
        'align_strips', 'EXPERIMENTAL: Alignment using strips', '[fn0] [fn1]', '',
        2, run_strips_align, False,
    ),(
        'merge_only',
        'Merges edits from fn0->fn1 and fn0->fn2, saving to fn012',
        '[fn0] [fn1] [fn2] [fn012]',
        '',
        4, run_merge, False,
    ),(
        'diff_only',
        'Computes changes from fn0->fn1, saving deletions to fnd, changes to fnc, and additions to fna',
        '[fn0] [fn1] [fnd] [fnc] [fna]',
        '',
        5, run_diff, False,
    ),(
        'adjacency',
        'EXPERIMENTAL',
        '[fn0] [fn1]',
        '',
        2, run_adj_viewer, False,
    ),(
        'diff_series',
        'Computes and visualizes changes from fn0->fn1, fn0->fn1->fn2, fn1->fn2->fn3, ...',
        '[fn0] [fn1] <...> <method=[method]>',
        '',
        xrange(2,100), run_diff_viewer_series, True,
    ),(
        'diff_methods',
        'Computes diffs from fn0->fn1 using different methods',
        '[fn0] [fn1] [method] <method> <...>',
        '',
        xrange(3,100), run_diff_viewer2_methods, True,
    ),(
        'diff_iter',
        'EXPERIMENTAL: computes diffs in iterative manner.  useful for watching how methods build up over time',
        '[fn0] [fn1] <method=[method]>',
        '',
        2, run_diff_viewer2_iter, False,
    ),(
        'diff_anim',
        'Computes diff and applies changes by parts to visualize edits in animation.',
        '[fn0] [fn1] <method=[method]>',
        '',
        2, run_change_viewer, True,
    ),(
        'match',
        'Computes matching from fn0->fn1',
        '[fn0] [fn1] <method=[method]> <scheme=[color scheme]>',
        'Visualizes the matching from fn0->fn1',
        2, run_match_viewer2_lines, True,
    ),(
        'match',
        'Computes matching from fn0->fn1 and fn0->fn2',
        '[fn0] [fn1] [fn2] <method=[method]>',
        'Visualizes the matching from fn0->fn1 and fn0->fn2',
        3, run_match_viewer3, True,
    ),(
        'match_methods',
        'Computes matching from fn0->fn1 using different methods',
        '[fn0] [fn1] [method] <method> <...>',
        '',
        xrange(3,100), run_match_viewer2_methods, True,
    ),(
        'match_order',
        'Computes matching from fn0->fn1',
        '[fn0] [fn1] <method=[method]> <stepsize=[stepsize]>',
        'Visualizes the matching from fn0->fn1',
        2, run_match_viewer2_order, True,
    ),(
        'git-diff',
        'git: diff computation',
        '[path] [old_file] [old_hex] [old_mode] [new_file] [new_hex] [new_mode]',
        '',
        8, run_git_diff, False,
    ),(
        'git-merge',
        'git: merge computation',
        '???',
        '',
        4, run_git_merge, False,
    ),(
        'save',
        'saves correspondences between [fn0] and [fn1] to [fnout]',
        '[fn0] [fn1] [fnout] <allow_mismatched_faces=[1/0]>',
        '',
        3, run_save_correspondences, True,
    ),
    ]
ladditional = [
    'methods:',
    '  blended_verts   : _LOADS_ results from running Blended Intrinsic Map, Kim et al.',
    '  disney          : topological matching (Approximate Topological Matching of Quadrilateral Meshes, Eppstein et al.)',
    ' *greedy_iter     : our method',
    '  hungarian_faces : bipartite graph matching of face-nodes (Approx. Graph Edit Distance Computation by Means of BGM, Riesen and Bunke)',
    '  hungarian_verts : bipartite graph matching of vert-nodes',
    '  icpgc_verts     : _LOADS_ results from running Automatic Registration for Articulated Shapes, Chang and Zwicker',
    '  smac_faces      : spectral matching of face-nodes (Balanced Graph Matching, Cour et al.)',
    '  smac_verts      : spectral matching of vert-nodes',
    'color schemes:',
    ' *muted_verts : same as "verts", but "muted" a bit',
    '  normal      : same colors as mesh diff',
    '  order       : available only for match with two meshes',
    '  position    : colors by position of verts (x,y,z) => (r,g,b)',
    '  random      : colors are randomly chosen',
    '  verts       : vert colors are randomly chosen, faces colored as average of vert colors',
    '*default',
    ]

if __name__ == '__main__':
    if len(sys.argv) == 1:
        print_commands()
        exit(1)
    
    cmd = sys.argv[1]
    lfns = [ a for a in sys.argv[2:] if '=' not in a ]
    dopts = dict( a.split('=') for a in sys.argv[2:] if '=' in a )
    action = None
    
    for c,shelp,sahelp,shelpdetails,nargs,fn,bprint in lcommands:
        if c != cmd: continue
        if type(nargs) is int:
            if len(lfns) != nargs: continue
        else:
            if len(lfns) not in nargs: continue
        fn( *lfns, **dopts )
        exit()
    
    print( 'Unknown command "%s" with %d args' % (cmd,len(lfns)) )
    print( 'For help: %s help' % sys.argv[0] )
    exit(1)


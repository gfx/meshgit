import os,re

from debugwriter import DebugWriter

"""
code to load / save to ply files
makes assumptions on format of header, so be careful!

object to be loaded / saved to ply is a dictionary as:
    {
        'lv': [ [0,0,0], [1,1,1], ... ],    # list of vertices as list of x,y,z components
        'ln': [ [1,0,0], [0,1,0], ... ],    # list of normals for each vertex
        'lc': [ [255,128,64], ... ],        # list of colors for ea vertex.  NOTE: r,g,b vals must be 0-255, not 0-1
        'lf': [ [0,1,2], [1,2,3,4], ... ],  # list of faces as vert indices
    }

NOTE: loading of normals and colors are not supported
"""

def ply_load( fn ):
    DebugWriter.start( 'loading ply: %s' % fn )
    
    verts,faces = [],[]
    
    fp = open( fn, 'rt' )
    assert fp.readline() == 'ply\n', 'not a ply file'
    assert fp.readline() == 'format ascii 1.0\n', 'unrecognized ply format'
    
    cverts,cfaces = 0,0
    done = False
    while not done:                                                         # stupidly parse header
        l = fp.readline()
        if l.startswith( 'comment' ): continue                              # ignore any comments
        elif l.startswith( 'property' ): continue                           # ignore for now
        elif l.startswith( 'element' ):                                     # get count of element
            lp = re.split( 'element ([^ ]+) ([0-9]+)\n', l )
            if lp[1] == 'vertex':
                cverts = int(lp[2])
            elif lp[1] == 'face':
                cfaces = int(lp[2])
            else:
                assert False, 'unknown element type "%s"' % lp[1]
        elif l.startswith( 'end_header' ):                                  # done parsing header
            done = True
        else:
            assert False, 'unrecognized line "%s"' % l
    
    for i_v in xrange(cverts):
        lp = [ p for p in fp.readline().split('\n')[0].split(' ') if p ]
        verts += [ [ float(p) for p in lp ] ]
    for i_f in xrange(cfaces):
        lp = [ p for p in fp.readline().split('\n')[0].split(' ')[1:] if p ]
        faces += [ [ int(p) for p in lp ] ]
    fp.close()
    
    DebugWriter.end()
    
    return { 'lv': verts, 'lf': faces }       # convert to near json format??  build adjacency data?



def ply_save( fn, plyobj ):
    ln,lc = None,None
    
    fp = open( fn, "wt" )
    fp.write( 'ply\n' )
    fp.write( 'format ascii 1.0\n' )
    fp.write( 'comment Created by MeshGit\n' )
    fp.write( 'element vertex %i\n' % len(plyobj['lv']) )
    fp.write( 'property float x\n' )
    fp.write( 'property float y\n' )
    fp.write( 'property float z\n' )
    if 'ln' in plyobj:
        fp.write( 'property float nx\n' )
        fp.write( 'property float ny\n' )
        fp.write( 'property float nz\n' )
        ln = plyobj['ln']
    if 'lc' in plyobj:
        fp.write( 'property uchar red\n' )
        fp.write( 'property uchar green\n' )
        fp.write( 'property uchar blue\n' )
        lc = plyobj['lc']
    fp.write( 'element face %i\n' % len(plyobj['lf']) )
    fp.write( 'property list uchar uint vertex_indices\n' )
    fp.write( 'end_header\n' )
    
    for iv,v in enumerate(plyobj['lv']):
        fp.write( '%s' % ' '.join([ str(c) for c in v[0:3] ]) )
        if ln: fp.write( ' %s' % ' '.join([ str(c) for c in ln[iv] ]) )
        if lc: fp.write( ' %s' % ' '.join([ str(c) for c in lc[iv] ]) )
        fp.write( '\n' )
    for f in plyobj['lf']:
        fp.write( '%s\n' % ' '.join([ str(c) for c in f ]) )
    fp.close()



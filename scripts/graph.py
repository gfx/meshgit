
"""
a simple implementation of simple, undirected graph.
"""

# http://www.python.org/doc/essays/graphs/

class Graph_Undirected(object):
    def __init__( self ):
        self.graph = {}
        self.nodes = set()
    
    def add_node( self, label ):
        assert label not in self.nodes, '%s already a node' % (label,)
        self.graph[ label ] = set()
        self.nodes.add( label )
    
    def add_edge( self, l0, l1 ):
        assert l0 in self.nodes, '%s not a node' % (l0,)
        assert l1 in self.nodes, '%s not a node' % (l1,)
        assert l0 != l1, '%s == %s' % ((l0,),(l1,))                     # don't allow edges with same node for ends
        self.graph[l0].add(l1)
        self.graph[l1].add(l0)
    
    def get_largest_connected_component( self ):
        snodes = set(self.nodes)
        slargest = set()
        
        while snodes:
            squeue = set([snodes.pop()])
            scurrent = set()
            while squeue:
                n = squeue.pop()
                if n in snodes: snodes.remove(n)
                if n in scurrent: continue
                scurrent.add( n )
                squeue.update( self.graph[n] )
            if len(scurrent) > len(slargest):
                slargest = scurrent
        
        return slargest


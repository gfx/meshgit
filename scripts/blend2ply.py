import bpy, subprocess, os



"""
run as:
    blender file.blend --python blend2ply.py
"""



path,fn = os.path.split( bpy.data.filepath )
fnprefix,fnext = os.path.splitext( fn )
fnply = os.path.join( path, fnprefix + '.ply' )


def layers( li_layer ):
    return [ (i in li_layer) for i in range(20) ]

def viewlayers( li_layer ):
    bpy.context.scene.layers = layers( li_layer )

def selectall():
    bpy.ops.object.select_all(action='SELECT')
def deselectall():
    bpy.ops.object.select_all(action='DESELECT')
def selectobj( o ):
    o.select = True
    bpy.context.scene.objects.active = o

def exec_wait( arglist ):
    return subprocess.Popen( arglist, stderr=subprocess.STDOUT, stdout=subprocess.PIPE ).communicate()




if bpy.context.active_object: bpy.ops.object.mode_set(mode='OBJECT')            # make sure we're in object mode
deselectall()

# get list of objects to export
l_objs = [ o for o in bpy.data.objects if o.type == 'MESH' ]

# prep objects for export
for o in l_objs:
    selectobj( o )                                                              # select and make active
    bpy.ops.object.shade_smooth()                                               # prevent vert duplication
    
    # remove subsurf modifier
    l_remmod = [ mod for mod in o.modifiers if mod.type == 'SUBSURF'  ]
    for mod in l_remmod: o.modifiers.remove( mod )
    
    bpy.ops.object.convert( target='MESH', keep_original=False )                # apply other modifiers
    
    deselectall()

# join objects
for o in l_objs: selectobj( o )
bpy.ops.object.join()
o = bpy.context.selected_objects[0]

# rescale object to fit in unit cube
bb = o.bound_box
xmin,xmax = min( p[0] for p in bb ),max( p[0] for p in bb )
ymin,ymax = min( p[1] for p in bb ),max( p[1] for p in bb )
zmin,zmax = min( p[2] for p in bb ),max( p[2] for p in bb )
#o.scale = 1.0 / max([xmax-xmin,ymax-ymin,zmax-zmin])

bpy.ops.object.transform_apply( location=True, rotation=True, scale=True )
o.scale *= 0.1
bpy.ops.object.transform_apply( location=True, rotation=True, scale=True )

# export!
bpy.ops.export_mesh.ply( filepath=fnply, use_modifiers=False, use_normals=False, use_uv_coords=False, use_colors=False )


# we're done!
bpy.ops.wm.quit_blender()









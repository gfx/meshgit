import os, re, json, copy
from mesh import Mesh
import ply, correspondences

#### IGNORING EDGES!!!

fn0 = '/Users/jdenning/Projects/meshgit/models/toy-cube/cube.ply'
fn1 = '/Users/jdenning/Projects/meshgit/models/toy-cube/cube+fextrude.ply'
fn2 = '/Users/jdenning/Projects/meshgit/models/toy-cube/cube+fextrude2.ply'
fn012 = '/Users/jdenning/Projects/meshgit/models/toy-cube/test012.ply'

m0,m1,m2 = Mesh.fromPLY( fn0 ), Mesh.fromPLY( fn1 ), Mesh.fromPLY( fn2 )

c01 = correspondences.build_closestverts( m0, m1 )
c02 = correspondences.build_closestverts( m0, m2 )

# make sure they don't have overlapping edits!
l1i_rf = [ i_f for i_f,f in enumerate(m0.lf) if i_f not in c01['mf01'] ]
l2i_rf = [ i_f for i_f,f in enumerate(m0.lf) if i_f not in c02['mf01'] ]

if set(l1i_rf) & set(l2i_rf):
    print( "conflict detected" )
    exit

m1a = Mesh()
m1a.lv = list(m1.lv)
m1a.le = [ m1.le[i_e] for i_e in xrange(len(m1.le)) if i_e not in c01['me10'] ]
m1a.lf = [ m1.lf[i_f] for i_f in xrange(len(m1.lf)) if i_f not in c01['mf10'] ]
m1a.recolor( [0,255,0] )

m2a = Mesh()
m2a.lv = list(m2.lv)
m2a.le = [ m2.le[i_e] for i_e in xrange(len(m2.le)) if i_e not in c02['me10'] ]
m2a.lf = [ m2.lf[i_f] for i_f in xrange(len(m2.lf)) if i_f not in c02['mf10'] ]
m2a.recolor( [0,0,255] )

m012 = copy.deepcopy( m0 )
m012.lf = [ f for i_f,f in enumerate(m012.lf) if i_f not in l1i_rf and i_f not in l2i_rf ]
m012.append( m1a )
m012.append( m2a )

m012.toPLY( fn012 )


print( "bpy.ops.object.select_all(action='SELECT'); bpy.ops.object.delete(); bpy.ops.import_mesh.ply(filepath='%s')" % fn012 )
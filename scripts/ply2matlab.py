#!/usr/bin/env python

"""
reads mesh from ply
writes out .mat file containing mesh data
"""

import os, sys, re
import scipy.io
from ply import *

if len(sys.argv) != 2:
    print( 'usage: %s [filename]' % sys.argv[0] )
    exit( 1 )

fnply = sys.argv[1]
funcm = re.sub( r'\.', r'', fnply )
fnmatlab = '%s.mat' % funcm

ply = ply_load( fnply )
cv = len(ply['lv'])
lf = [ f[1:] for f in ply['lf'] ]

##le = [ [0]*cv for i in xrange(cv) ]
##for f in lf:
##    for v0,v1 in zip(f,f[1:]+[f[0]]):
##        le[v0][v1] = le[v1][v0] = 1
#le = set()
#for f in lf:
    #for v0,v1 in zip(f,f[1:]+[f[0]]):
        #if v0 > v1: v0,v1 = v1,v0
        #le.add( frozenset([v0,v1]) )
#lei,lej,les = [cv],[cv],[0]
#for fsv01 in le:
    #v0,v1 = fsv01
    #lei.append( v0 )
    #lej.append( v1 )
    #les.append( 1 )
    #lei.append( v1 )
    #lej.append( v0 )


data = {}
data['V'] = ply['lv']
#data['Ei'] = lei
#data['Ej'] = lej
data['F'] = lf
scipy.io.savemat( fnmatlab, data )

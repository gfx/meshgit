#ifndef BINHEAP
#define BINHEAP

template <typename Data, typename Key> class BinaryHeapNode
{
    Key myKey;
    Data myData;
    int side;
    
    BinaryHeapNode<Data,Key> *pNode;
    BinaryHeapNode<Data,Key> *lNode;
    BinaryHeapNode<Data,Key> *rNode;
    
    BinaryHeapNode( Data d, Key k ):
        myKey(k),
        myData(d),
        pNode(NULL),
        lNode(NULL),
        rNode(NULL),
        side(0)
        {}
    
    void insert( BinaryHeapNode<Data,Key> *oNode )
    {
        if( oNode == NULL ) return;
        if( lNode == NULL ) { lNode = oNode; oNode->pNode = this; return; }
        if( rNode == NULL ) { rNode = oNode; oNode->pNode = this; return; }
        
        if( side == 0 ) {
            if( lNode->myKey < oNode->myKey ) lNode->insert( oNode );
            else {
                oNode->insert( lNode );
                lNode = oNode;
                oNode->pNode = this;
            }
        } else {
            if( rNode->myKey < oNode->myKey ) rNode->insert( oNode );
            else {
                oNode->insert( rNode );
                rNode = oNode;
                oNode->pNode = this;
            }
        }
        
        side = (side+1) % 2;
    }
    
    BinaryHeapNode<Data,Key> *remove()
    {
        BinaryHeapNode<Data,Key> *minNode = NULL;
        BinaryHeapNode<Data,Key> *maxNode = NULL;
        
        if( lNode != NULL && rNode != NULL ) {
            if( lNode->myKey < rNode->myKey ) {
                minNode = lNode;
                maxNode = rNode;
            } else {
                minNode = rNode;
                maxNode = lNode;
            }
        } else minNode = ( rNode == NULL ) ? lNode : rNode;
        
        if( minNode != NULL ) {
            minNode->insert( maxNode );
            minNode->pNode = pNode;
        }
        
        if( pNode != NULL ) {
            if( pNode->lNode == this ) pNode->lNode = minNode;
            else pNode->rNode = minNode;
        }
        
        pNode = lNode = rNode = NULL;
        
        return minNode;
    }
    
public:
    Key key() const { return myKey; }
    Data data() const { return myData; }
    template <typename D, typename K> friend class BinaryHeap;
};

template <typename Data, typename Key> class BinaryHeap
{
    BinaryHeapNode<Data,Key> *root;
    
public:
    
    BinaryHeap() : root(NULL) {}
    ~BinaryHeap() {}
    
    bool empty() const { return (root==NULL); }
    
    BinaryHeapNode<Data,Key> *minimum() const { return root; }
    
    BinaryHeapNode<Data,Key> *insert( Data d, Key k )
    {
        BinaryHeapNode<Data,Key> *nNode = new BinaryHeapNode<Data,Key>( d, k );
        if( root == NULL ) root = nNode;
        else if( root->myKey < k ) root->insert( nNode );
        else {
            nNode->insert( root );
            root = nNode;
        }
        return nNode;
    }
    
    void combine( BinaryHeap *o )
    {
        if( o->root == NULL ) return;
        if( this->root == NULL ) root = o->root;
        else if( root->myKey < o->root->myKey ) root->insert( o->root );
        else {
            o->root->insert( root );
            root = o->root;
        }
        o->root = NULL;
    }
    
    BinaryHeapNode<Data,Key> *removeMinimum()
    {
        BinaryHeapNode<Data,Key> *rNode = root;
        root = root->remove();
        return rNode;
    }
    
    void remove( BinaryHeapNode<Data,Key> *rNode )
    {
        if( rNode == root ) root = rNode->remove();
        else rNode->remove();
    }
};


#endif

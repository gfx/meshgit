import sys, subprocess, os, scipy.io

from debugwriter import DebugWriter
from mesh import *

from correspondencebuilder_logic import CorrespondenceBuilder_Logic as CB_Logic
from correspondencebuilder_hungarian import CorrespondenceBuilder_Hungarian as CB_Hungarian


class CorrespondenceBuilder_SMAC(object):
    
    @staticmethod
    def build_verts( opts ):
        CB_SMAC = CorrespondenceBuilder_SMAC
        
        ci = opts['ci']
        maxruns = opts.setdefault( 'maxruns', 1 )
        
        lh = None
        for run in xrange( maxruns ):
            h = ci.__hash__()
            if h == lh: break
            lh = h
            
            CB_SMAC.build_vert_correspondences( opts )
            CB_Logic.build_face_correspondences( opts )
            
            #remove_face_conflicts( ci, 0.75 )
            #propagate_face_correspondences( ci )
            #remove_face_conflicts( ci, 0.75 )
            #build_vertex_correspondences_faces( ci )
    
    @staticmethod
    def build_faces( opts ):
        CB_SMAC = CorrespondenceBuilder_SMAC
        
        CB_SMAC.build_face_correspondences( opts )
        CB_Hungarian.build_vertex_correspondences_hungarian_faces( opts )
        
        return
        
        ci = opts['ci']
        opts.setdefault( 'per', 0.75 )
        maxruns = opts.setdefault( 'maxruns', 1 )
        
        lh = None
        for run in xrange( maxruns ):
            h = ci.__hash__()
            if h == lh: break
            lh = h
            
            CB_SMAC.build_face_correspondences( opts )
            #CB_Logic.remove_face_conflicts( opts )
            #CB_Logic.remove_smaller_connected_faces( opts )
            CB_Logic.build_vertex_correspondences_faces( opts )
            
            #ci.dcvall()
            #CB_Logic.build_vertex_correspondences_faces( opts )
            ##build_vertex_correspondences_hungarian_faces( ci )
            #CB_Logic.build_face_correspondences( opts )
            #CB_Logic.propagate_face_correspondences( opts )
            #CB_Logic.remove_face_conflicts( opts )
            #if runs < 3: remove_smaller_connected_faces( opts )
            #CB_Logic.remove_face_conflicts( opts )
            #CB_Logic.build_face_correspondences_smac( opts )
            #CB_Logic.remove_face_conflicts( opts )
    
    @staticmethod
    def build_vert_correspondences( opts ):
        CB_SMAC = CorrespondenceBuilder_SMAC
        
        ci = opts['ci']
        
        k = 5
        
        #cnull   =     0.0
        #caddrem =    10.0
        #cdist0  =     2.0
        #cdist1  =     2.0
        #cnorm   =     1.0
        #cmis    =     5.0
        cnull   =     0.0
        caddrem =     1.5
        cdist0  =     1.0
        cdist1  =     1.0
        cnorm   =     1.0
        cmis    =     2.0
        
        expmult =    1000.0
        
        lv0,lv1 = [ [v.p for v in lv] for lv in [ ci.lv0,ci.lv1] ]
        ln0,ln1 = [ [v.n for v in lv] for lv in [ ci.lv0,ci.lv1] ]
        lf0,lf1 = [ [ m._opt_get_f_li_v(f) for f in m.lf ] for m in [ci.m0,ci.m1]]
        cv0,cv1,cf0,cf1 = ci.cv0,ci.cv1,ci.cf0,ci.cf1
        
        DebugWriter.start( 'computing feasibility matrix for %ix%i matches' % (cv0,cv1) )
        F = []
        for i,v0 in ci.elv0():
            if i in ci.mv01:
                F.extend( [ (i,ci.mv01[i]) ] )
            else:
                costs = [ (
                    j,
                    Vec3f.t_distance2( v0.p, v1.p ) + (1.0-Vec3f.t_dot(ln0[i],ln1[j])) #0.5 * (1.0-Vec3f.t_dot(ln0[i],ln1[j]))
                    #2.5 * Vec3f.t_distance2( v0.p, v1.p ) + 2.5 / 2.0 * (1.0 - Vec3f.t_dot( ln0[i], ln1[j] ))
                    ) for j,v1 in ci.elv1() if j not in ci.mv10 ]
                costs = sorted( costs, key=lambda x:x[1] )
                F.extend( [ (i,j) for j,_ in costs[:k] ] )
        
        # add, delete, and null nodes
        F.extend( [ (i,i+cv1) for i in xrange(cv0) if i not in ci.mv01 ] )                      # removed
        F.extend( [ (cv0+j,j) for j in xrange(cv1) if j not in ci.mv10 ] )                      # added
        F.extend( [ (cv0+ij,cv1+ij) for ij in xrange(max(cv0,cv1)) ] )      # null matchings
        
        n12 = len(F)                                                        # number of feasible matches
        F = sorted( F, key=lambda x:x[0]+x[1]*n12 )
        DebugWriter.printer( '%i feasible matches' % n12 )
        DebugWriter.end()
        
        A = [ [0.0] * n12 for i in xrange(n12) ]
        
        DebugWriter.start( 'computing affinity matrix for %ix%i matches' % (n12,n12) )
        for p in xrange(n12):
            i0,j0 = F[p]
            for q in xrange(p+1):                                                   # only compute lower triangle
                i1,j1 = F[q]
                
                # compute affinity for matching both i0->j0 and i1->j1
                # by computing distance between i0,i1 and j0,j1
                if i0 < cv0 and j0 < cv1 and i1 < cv0 and j1 < cv1:
                    #if (i0 == i1 and j0 != j1) or (i0 != i1 and j0 == j1):
                    #    #A[p][q] = 0.0
                    #    pass
                    #else:
                        cost = 0.0
                        lai01 = ci.la_v0[i0]['si_f'] & ci.la_v0[i1]['si_f']
                        laj01 = ci.la_v1[j0]['si_f'] & ci.la_v1[j1]['si_f']
                        di01 = Vec3f.t_distance2( lv0[i0], lv0[i1] )
                        dj01 = Vec3f.t_distance2( lv1[j0], lv1[j1] )
                        #cost += cdist0 * abs(di01 - dj01)
                        #cost += cdist1 * Vec3f.t_distance2( lv0[i0], lv1[j0] )
                        #cost += cdist1 * Vec3f.t_distance2( lv0[i1], lv1[j1] )
                        #cost += cnorm * ((1.0 - Vec3f.t_dot( ln0[i0], ln1[j0] )) + (1.0 - Vec3f.t_dot( ln0[i1], ln1[j1] )))
                        #if (lai01 and not laj01) or (laj01 and not lai01): cost += cmis
                        cost += (di01-dj01)*(di01-dj01)
                        A[p][q] = cost
                elif i0 >= cv0 and j0 >= cv1 and i1 >= cv0 and j1 >= cv1:
                    A[p][q] = cnull
                else:
                    A[p][q] = caddrem
                
                A[p][q] = A[q][p] = math.exp( abs(A[p][q]) * -expmult )
        DebugWriter.end()
        
        opts['smacdata'] = { 'n1':cv0, 'n2':cv1, 'n12':n12, 'E12i':[ i for i,_ in F ], 'E12j':[ j for _,j in F ], 'W':A }
        mapping = CB_SMAC.run_smac( opts )
        if not mapping:
            return
        for i,j in enumerate(mapping):
            if j >= ci.cv1: continue
            if i in ci.mv01:
                assert ci.mv01[i] == j
            else:
                ci.acv( i, j )
        
        #data = {
            #'n1': cf0, 'n2': cf1, 'n12': n12,
            #'E12i': [ i+1 for i,_ in F ], 'E12j': [ j+1 for _,j in F ],
            #'W': A,
            #}
        #scipy.io.savemat( 'SMAC_data.mat', data )
        
        ## commenting out the automation, because Matlab does strange things to the console when a problem occurs :P
        ##subprocess.Popen( ['./graphmatch.sh'] ).communicate() #, stderr=subprocess.STDOUT, stdout=subprocess.PIPE ).communicate()
        ##print( 'Run SMAC Graph Match on the SMAC_data.mat.  Place the SMAC_ind.txt file here when finished.' )
        ##raw_input( 'Press Enter when ready...' )
        #DebugWriter.start( 'running SMAC algorithm' )
        #DebugWriter.divider()
        #subprocess.Popen( [ find_syspath_to_file('SMAC.sh') ] ).communicate()
        #DebugWriter.divider()
        #DebugWriter.end()
        
        #if os.path.getsize( 'SMAC_ind.txt' ) == 0:
            #print( 'no assignment made!  error?' )
            #return
        
        #fp = open( 'SMAC_ind.txt', 'rt' )
        #for i in xrange( ci.cv0 ):
            #s = fp.readline()[:-1]
            #if not s: continue
            #j = int(s) - 1
            #if j >= ci.cv1: continue
            #if i in ci.mv01:
                #assert ci.mv01[i] == j
            #else:
                #ci.acv( i, j )
    
    @staticmethod
    def run_smac( opts ):
        DebugWriter.start( 'running SMAC graph matching algorithm' )
        
        data = opts['smacdata']
        data['E12i'] = [ i+1 for i in data['E12i'] ]
        data['E12j'] = [ j+1 for j in data['E12j'] ]
        scipy.io.savemat( 'SMAC_data.mat', data )
        
        # commenting out the automation, because Matlab does strange things to the console when a problem occurs :P
        #subprocess.Popen( ['./graphmatch.sh'] ).communicate() #, stderr=subprocess.STDOUT, stdout=subprocess.PIPE ).communicate()
        #print( 'Run SMAC Graph Match on the SMAC_data.mat.  Place the SMAC_ind.txt file here when finished.' )
        #raw_input( 'Press Enter when ready...' )
        DebugWriter.divider()
        subprocess.Popen( [ find_syspath_to_file('SMAC.sh') ] ).communicate()
        DebugWriter.divider()
        
        if os.path.getsize( 'SMAC_ind.txt' ) == 0:
            print( 'no assignment made!  error?' )
            DebugWriter.end()
            return None
        
        mapping = [-1]*data['n1']
        
        fp = open( 'SMAC_ind.txt', 'rt' )
        for i in xrange( data['n1'] ):
            s = fp.readline()[:-1]
            if not s: continue
            mapping[i] = int(s)-1
        fp.close()
        return mapping
    
    @staticmethod
    def compute_face_centers( lv, lf ):
        return [ Vec3f.t_average( [ lv[i] for i in f ] ) for f in lf ]
    @staticmethod
    def get_face_adj_matrix( lf ):
        cf = len(lf)
        lfle = [ set( [ (min(v0,v1),max(v0,v1)) for v0,v1 in zip(f,f[1:]+[f[0]]) ] ) for f in lf ]
        de = dict( (e,[]) for fle in lfle for e in fle )
        for i_f,fle in enumerate(lfle):
            for e in fle: de[e].append(i_f)
        
        mfa = [ [False] * cf for i in xrange(cf) ]
        for e,li_f in de.items():
            for i in li_f:
                for j in li_f:
                    mfa[i][j] = mfa[j][i] = True
        
        return mfa
    
    @staticmethod
    def build_face_correspondences( opts ):
        CB_SMAC = CorrespondenceBuilder_SMAC
        
        ci = opts['ci']
        
        k = 5
        
        cnull = 0.0
        caddrem = 2.5
        cdist0 = 2.0
        cdist1 = 2.0
        cnorm = 1.0
        cmis = 5.0
        expmult = 1000.0
        
        lv0,lv1 = [ [v.p for v in lv] for lv in [ ci.lv0,ci.lv1] ]
        lf0,lf1 = [ [ m._opt_get_f_li_v(f) for f in m.lf ] for m in [ci.m0,ci.m1]]
        cv0,cv1,cf0,cf1 = ci.cv0,ci.cv1,ci.cf0,ci.cf1
        lfcv0,lfcv1 = CB_SMAC.compute_face_centers(lv0,lf0),CB_SMAC.compute_face_centers(lv1,lf1)
        mfa0,mfa1 = CB_SMAC.get_face_adj_matrix(lf0),CB_SMAC.get_face_adj_matrix(lf1)
        
        DebugWriter.start( 'computing feasibility matrix for %ix%i matches' % (cf0,cf1) )
        F = []
        for i,fcv0 in enumerate(lfcv0):                                     # using k-NN to determine feasibility
            if i in ci.mf01:
                F.extend( [ (i,ci.mf01[i]) ] )
            else:
                c0,n0 = lfcv0[i],ci.m0.opt_face_normal( ci.lf0[i] )
                costs = [ ( j, Vec3f.t_distance2( c0, lfcv1[j] )
                        #(( 2.0 - ( 1.0 + Vec3f.t_dot( n0, ci.m1.opt_face_normal( ci.lf1[j] ) ) )) + Vec3f.t_distance2( c0, lfcv1[j] ))
                        if j not in ci.mf10 else 1000.0
                    ) for j in xrange(cf1) ]
                costs = sorted( costs, key=lambda x:x[1] )
                F.extend( [ (i,j) for j,_ in costs[:k] ] )
        
        # dummy nodes
        F.extend( [ (i,i+cf1) for i in xrange(cf0) ] )                      # removed faces
        F.extend( [ (cf0+j,j) for j in xrange(cf1) ] )                      # added faces
        F.extend( [ (cf0+ij,cf1+ij) for ij in xrange(max(cf0,cf1)) ] )      # null matchings
        
        n12 = len(F)                                                        # number of feasible matches
        F = sorted( F, key=lambda x:x[0]+x[1]*n12 )
        DebugWriter.printer( '%i feasible matches' % n12 )
        DebugWriter.end()
        
        A = [ [ 0.0 ] * n12 for i in xrange(n12) ]
        
        DebugWriter.start( 'computing affinity matrix for %ix%i matches' % (n12,n12) )
        for p in xrange(n12):
            i0,j0 = F[p]
            for q in xrange(p+1):                                                   # only compute lower triangle
                i1,j1 = F[q]
                
                a = 0.0
                
                # compute affinity for matching both i0->j0 and i1->j1
                # by computing distance between i0,i1 and j0,j1
                if i0 < cf0 and j0 < cf1 and i1 < cf0 and j1 < cf1:
                    #if (i0 == i1 and j0 != j1) or (i0 != i1 and j0 == j1):
                    #    a = 0.0
                    #
                    #else:
                        #di01 = Vec3f.t_distance2( lfcv0[i0], lfcv0[i1] )
                        #dj01 = Vec3f.t_distance2( lfcv1[j0], lfcv1[j1] )
                        #ni0 = ci.m0.opt_face_normal( ci.lf0[i0] )
                        #nj0 = ci.m1.opt_face_normal( ci.lf1[j0] )
                        #ni1 = ci.m0.opt_face_normal( ci.lf0[i1] )
                        #nj1 = ci.m1.opt_face_normal( ci.lf1[j1] )
                        #a += abs( di01 - dj01 )
                        #a += Vec3f.t_distance2( lfcv0[i0], lfcv1[j0] )
                        #a += Vec3f.t_distance2( lfcv0[i1], lfcv1[j1] )
                        #a += (1.0 - Vec3f.t_dot( ni0, nj0 )) * (1.0 - Vec3f.t_dot( ni1, nj1 ))
                        
                        ##a += math.exp(-( di01 - dj01 ) )
                        ##a += math.exp(-Vec3f.t_distance2( lfcv0[i0], lfcv1[j0] ))
                        ##a += math.exp(-Vec3f.t_distance2( lfcv0[i1], lfcv1[j1] ))
                        
                        #if (mfa0[i0][i1] and not mfa1[j0][j1]): a += 1.0
                        #if (not mfa0[i0][i1] and mfa1[j0][j1]): a += 1.0
                        
                        di01 = Vec3f.t_distance( lfcv0[i0], lfcv0[i1] )
                        dj01 = Vec3f.t_distance( lfcv1[j0], lfcv1[j1] )
                        
                        a += (di01-dj01)**2
                
                elif i0 >= cf0 and j0 >= cf1 and i1 >= cf0 and j1 >= cf1:
                    a = cnull
                
                else:
                    a = caddrem
                
                A[p][q] = A[q][p] = math.exp( abs(a) * -expmult )
                
        
        DebugWriter.end()
        
        opts['smacdata'] = { 'n1':cf0, 'n2':cf1, 'n12':n12, 'E12i':[ i for i,_ in F ], 'E12j':[ j for _,j in F ], 'W':A }
        mapping = CB_SMAC.run_smac( opts )
        if not mapping:
            return
        for i,j in enumerate(mapping):
            if j >= ci.cf1: continue
            if i in ci.mf01:
                assert ci.mf01[i] == j
            else:
                ci.acf( i, j )
        
        #data = {
            #'n1': cf0, 'n2': cf1, 'n12': n12,
            #'E12i': [ i+1 for i,j in F ], 'E12j': [ j+1 for i,j in F ],
            #'W': A,
            #}
        #scipy.io.savemat( 'SMAC_data.mat', data )
        
        #subprocess.Popen( [ find_syspath_to_file('./graphmatch.sh') ] ).communicate() #, stderr=subprocess.STDOUT, stdout=subprocess.PIPE ).communicate()
        
        #if os.path.getsize( 'SMAC_ind.txt' ) == 0:
            #print( 'no assignment made!  error?' )
            #return
        
        #fp = open( 'SMAC_ind.txt', 'rt' )
        #for i in xrange( ci.cf0 ):
            #s = fp.readline()[:-1]
            #if not s: continue
            #j = int(s) - 1
            #if j >= ci.cf1: continue
            #if i in ci.mf01:
                #assert ci.mf01[i] == j
            #else:
                #ci.acf( i, j )
        


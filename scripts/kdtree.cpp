//#include <fstream.h>
//#include <iostream.h>
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "kdtree.h"
#include "quicksort.h"


KDTree::KDTree()
{
    top = 0;
}

KDBranch::KDBranch( float *p, int i, int d )
{
    //this->p = &(new float[3]);
    this->p[0] = p[0];
    this->p[1] = p[1];
    this->p[2] = p[2];
    this->i = i;
    this->d = d;
    this->l = this->r = 0;
}

/*KDTree *KDTree::load_cache( char *fn )
{
    KDTree *kdt = new KDTree();
    char b;
    ifstream fp( fn, ios::in | ios::binary );
    fp.read( &b, 1 );
    if( b ) kdt->top = KDBranch::load_cache( fp );
    return kdt;
}
void KDTree::save_cache( char *fn )
{
    ofstream fp( fn, ios::out | ios::binary );
    char b = ( top == 0 ? 0 : 1 );
    fp.write( &b, 1 );
    if( top ) top->save_cache( fp );
    fp.close();
}
KDBranch *KDBranch::load_cache( ifstream &fp )
{
    float *p = new float[3];
    int i;
    int d;
    char b;
    
    fp.read( (char*)p, sizeof(float)*3 );
    fp.read( (char*)&i, sizeof(int) );
    fp.read( (char*)&d, sizeof(int) );
    KDBranch *kdb = new KDBranch( p, i, d );
    
    fp.read( &b, 1 );
    if( b ) kdb->l = KDBranch::load_cache( fp );
    fp.read( &b, 1 );
    if( b ) kdb->r = KDBranch::load_cache( fp );
    
    return kdb;
}
void KDBranch::save_cache( ofstream &fp )
{
    char b;
    fp.write( (char*)p, sizeof(float)*3 );
    fp.write( (char*)&i, sizeof(int) );
    fp.write( (char*)&d, sizeof(int) );
    b = ( l == 0 ? 0 : 1 );
    fp.write( &b, 1 );
    if( l ) l->save_cache( fp );
    b = ( r == 0 ? 0 : 1 );
    fp.write( &b, 1 );
    if( r ) r->save_cache( fp );
}*/




void KDTree::insert( float *p, int i )
{
    if( top == 0 ) top = new KDBranch( p, i, 0 );
    else top->insert( p, i );
}
void KDTree::insert_shuffle( float *p, int n )
{
    unsigned int *inds = shuffled_inds( n );
    for( unsigned int i = 0; i < n; i++ )
    {
        unsigned int is = inds[i];
        insert( &p[is*3], is );
    }
    free( inds );
}
void KDBranch::insert( float *p, int i )
{
    float dist_partition = this->p[d] - p[d];
    
    if( dist_partition >= 0 ) {
        if( l == 0 ) l = new KDBranch( p, i, (d+1)%3 );
        else l->insert( p, i );
    } else {
        if( r == 0 ) r = new KDBranch( p, i, (d+1)%3 );
        else r->insert( p, i );
    }
}




int KDTree::knn( float *p, int k, int *knn, float *knn_dist2 )
{
    for( int i = 0; i < k; i++ ) knn[i] = -1;
    if( top == 0 ) return 0;
    int found = 0;
    float max2 = INFINITY;
    top->knn( p, k, knn, knn_dist2, found, max2 );
    return found;
}

#define DOT( x, y )   ( (x[0]-y[0])*(x[0]-y[0]) + (x[1]-y[1])*(x[1]-y[1]) + (x[2]-y[2])*(x[2]-y[2]) )
void KDBranch::knn( float *p, int k, int *knn, float *knn_dist2, int &found, float &max2 )
{
    float dist_partition = this->p[d] - p[d];
    float dist_partition2 = dist_partition * dist_partition;
    float dist2 = DOT( this->p, p );
    
    if( found < k || dist2 <= max2 ) {
        int imax;
        
        if( found == k ) {
            // find and replace max distance
            imax = 0;
            float vmax2 = knn_dist2[0];
            for( int i = 1; i < found; i++ ) if( knn_dist2[i] >= vmax2 ) { imax = i; vmax2 = knn_dist2[i]; }
        } else {
            // insert at end
            imax = found;
        }
        
        knn[imax] = this->i;
        knn_dist2[imax] = dist2;
        
        if( found < k ) found++;
        
        // find new max
        max2 = dist2;
        for( int i = 0; i < found; i++ ) if( knn_dist2[i] > max2 ) max2 = knn_dist2[i];
        
    }
    
    if( dist_partition == 0 ) {
        if( l != 0 ) l->knn( p, k, knn, knn_dist2, found, max2 );
        if( r != 0 ) r->knn( p, k, knn, knn_dist2, found, max2 );
    } else if( dist_partition > 0 ) {
        if( l != 0 ) l->knn( p, k, knn, knn_dist2, found, max2 );
        if( ( found < k || dist_partition2 <= max2 ) && r != 0 ) r->knn( p, k, knn, knn_dist2, found, max2 );
    } else {
        if( r != 0 ) r->knn( p, k, knn, knn_dist2, found, max2 );
        if( ( found < k || dist_partition2 <= max2 ) && l != 0 ) l->knn( p, k, knn, knn_dist2, found, max2 );
    }
    
}




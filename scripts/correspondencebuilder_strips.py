from mesh import *
from hungarian import *
from debugwriter import DebugWriter
from miscfuncs import *

from correspondencebuilder_hungarian import CorrespondenceBuilder_Hungarian as CB_Hungarian
from correspondencebuilder_logic import CorrespondenceBuilder_Logic as CB_Logic

class CorrespondenceBuilder_Strips(object):
    
    @staticmethod
    def build( opts ):
        CB_Strips = CorrespondenceBuilder_Strips
        ci = opts['ci']
        
        DebugWriter.report( 'method', 'strips' )
        
        CB_Strips.build_correspondence_strips( opts )
        #remove_face_conflicts( ci )
        CB_Logic.build_vertex_correspondences_faces( opts )
        CB_Hungarian.build_vertex_correspondences_hungarian_faces( opts )
        opts['per'] = 0.75
        CB_Logic.remove_face_conflicts( opts )
        CB_Hungarian.build_face_correspondences_hungarian( opts )
        CB_Logic.remove_face_conflicts( opts )
    
    @staticmethod
    def build_correspondence_strips( opts ):
        ci = opts['ci']
        
        threshold = 10000.0
        
        ci.dcvall()
        ci.dcfall()
        
        m0,m1 = ci.m0,ci.m1
        m0.optimize()
        m1.optimize()
        
        stats0,stats1 = m0.get_stats(build_strips=True), m1.get_stats(build_strips=True)
        adj_e0,adj_f0,mra0,lring_sf0 = [ stats0[k] for k in ['adj_e','adj_f','mra','lring_sf'] ]
        adj_e1,adj_f1,mra1,lring_sf1 = [ stats1[k] for k in ['adj_e','adj_f','mra','lring_sf'] ]
        len0,len1 = len(lring_sf0),len(lring_sf1)
        llcv0 = [ [ Vertex.average( m0.opt_get_uf_lv(u_f) ) for u_f in ring_sf0 ] for ring_sf0 in lring_sf0 ]
        llcv1 = [ [ Vertex.average( m1.opt_get_uf_lv(u_f) ) for u_f in ring_sf1 ] for ring_sf1 in lring_sf1 ]
        
        print( 'aligning strips...' )
        align = [None]*len0
        
        if True:
            print( '  computing cost matrix...' )
            matcosts = [[0.0]*(len0+len1) for i in xrange(len0+len1)]
            
            for (i,j) in xyrange(len0,len0):                                                            # Q1
                matcosts[i][j+len1] = 15.0 * len(lring_sf0[i]) if i == j else float('inf')
            
            for (i,j) in xyrange(len1,len1):                                                            # Q3
                matcosts[i+len0][j] = 15.0 * len(lring_sf1[j]) if i == j else float('inf')
            
            t,pl = time.time(),-1.0
            for i,lcv0 in enumerate(llcv0):                                                             # Q2
                pc = float(i)/float(len0)*100.0
                if int(pl/5.0) != int(pc/5.0):
                    td = time.time() - t
                    print( '    %02i%% (%.2fs, eta %.2fs)...' % (pc,td,(td*100.0/pc-td) if pc > 0.0 else -1.0) )
                    pl = pc
                # get verts of ring
                for j,lcv1 in enumerate(llcv1):
                    d = abs(sum(mra0[i])-sum(mra1[j])) * 6.0
                    
                    if True and (abs(len(lcv0)-len(lcv1)) > 20 or d > 20.0):
                        d = float('inf')
                    else:
                        
                        ld = [ (_i,_j,Vec3f.t_distance2(cv0,cv1)) for _i,cv0 in enumerate(lcv0) for _j,cv1 in enumerate(lcv1) ]
                        
                        dr = 0.0
                        u0,u1 = [0]*len(lcv0),[0]*len(lcv1)
                        for _i,_j,_d in sorted(ld,key=lambda x:x[2]):
                            if u0[_i] or u1[_j]: continue
                            u0[_i],u1[_j] = 1,1
                            dr += _d
                        dr += len(lcv0)-sum(u0)
                        dr += len(lcv1)-sum(u1)
                        
                        d += dr * 100.0
                        
                        #d += compute_cost(
                        #    len(lcv0), len(lcv1),
                        #    lambda _i: 1.0,
                        #    lambda _i,_j: Vec3f.t_distance2( lcv0[_i], lcv1[_j] ),      # TODO: incl star cost?
                        #    lambda _j: 1.0
                        #    ) * 100.0
                    
                    #matcosts[i][j] = d if d <= threshold else float('inf')
                    matcosts[i][j] = d
            
            
            print( "  running..." )
            t = time.time()
            inds = hungarian( numpy.array( matcosts ) )
            print( "    finished in %f" % (time.time()-t) )
            
            for i,j in enumerate(inds):
                if i >= len0 or j >= len1: continue
                align[i] = j
            
            #fp = open( 'align','wt' )
            #for i in xrange(len0):
            #    fp.write( '%i\n' % (align[i] if align[i] is not None else -1) )
            #fp.close()
        
        else:
            assert False
            #fp = open( 'align','rt' )
            #for i,l in enumerate(fp.readlines()):
            #    if i >= len0: continue
            #    j = int(l)
            #    align[i] = j if j != -1 else None
        
        if not any( align ):
            print( '  WARNING: No strip alignments' )
        
        print( '  building face correspondences from strips...' )
        for ir00,ring_sf00 in enumerate(lring_sf0):
            ir10 = align[ir00]
            if ir10 is None: continue
            ring_sf10 = lring_sf1[ir10]
            for ir01,ring_sf01 in enumerate(lring_sf0):
                if ir01 <= ir00: continue
                ir11 = align[ir01]
                if ir11 is None: continue
                ring_sf11 = lring_sf1[ir11]
                
                luf0 = list(ring_sf00 & ring_sf01)
                if not luf0: continue
                
                luf1 = list(ring_sf10 & ring_sf11)
                if not luf1: continue
                
                lfc0 = [ Vertex.average( m0.opt_get_uf_lv( u_f ) ) for u_f in luf0 ]
                lfc1 = [ Vertex.average( m1.opt_get_uf_lv( u_f ) ) for u_f in luf1 ]
                
                d = [ (i,j,Vec3f.t_distance2(fc0,fc1)) for i,fc0 in enumerate(lfc0) for j,fc1 in enumerate(lfc1) ]
                for i,j,_ in sorted( d, key=lambda x:x[2] ):
                    if0,if1 = m0._opt_get_i_f(luf0[i]),m1._opt_get_i_f(luf1[j])
                    if if0 in ci.mf01 or if1 in ci.mf10: continue
                    ci.acf( if0, if1 )
        
        if True:
            print( '  removing overly conflicting correspondences...' )
            changed = True
            while changed:
                changed = False
                
                for if0,f0 in ci.elf0():
                    if if0 not in ci.mf01: continue
                    if1 = ci.mf01[if0]
                    f1 = m1.lf[if1]
                    
                    li_r0 = [ i_r0 for i_r0,ring_sf0 in enumerate(lring_sf0) if f0.u_f in ring_sf0 ]
                    li_r1 = [ i_r1 for i_r1,ring_sf1 in enumerate(lring_sf1) if f1.u_f in ring_sf1 ]
                    li_r1_ = [ align[i_r0] for i_r0 in li_r0 if align[i_r0] ]
                    if len(set(li_r1) & set(li_r1_)) >= 1: continue
                    changed = True
                    ci.dcf0( if0 )
                    print( '    removing if0 %i' % if0 )
                
                for if0,f0 in ci.elf0():
                    if if0 not in ci.mf01: continue
                    if1 = ci.mf01[if0]
                    f1 = m1.lf[if1]
                    
                    su_fa0 = adj_f0[f0.u_f]['su_afe']
                    su_fa1 = adj_f1[f1.u_f]['su_afe']
                    
                    li_fa0 = [ m0._opt_get_i_f( u_f ) for u_f in su_fa0 ]
                    li_fa1 = [ ci.mf01[i_f] if i_f in ci.mf01 else None for i_f in li_fa0 ]
                    
                    su_fa1_ = set([ m1.lf[i_f].u_f for i_f in li_fa1 if i_f is not None ])
                    if float(len(su_fa1 & su_fa1_)) / float(len(su_fa1)) >= 0.6: continue
                    #if len(su_fa1 & su_fa1_) >= 3: continue
                    changed = True
                    ci.dcf0( if0 )
                
                for if1,f1 in ci.elf1():
                    if if1 not in ci.mf10: continue
                    if0 = ci.mf10[if1]
                    f0 = m0.lf[if0]
                    
                    su_fa0 = adj_f0[f0.u_f]['su_afe']
                    su_fa1 = adj_f1[f1.u_f]['su_afe']
                    
                    li_fa1 = [ m1._opt_get_i_f( u_f ) for u_f in su_fa1 ]
                    li_fa0 = [ ci.mf10[i_f] if i_f in ci.mf10 else None for i_f in li_fa1 ]
                    
                    su_fa0_ = set([ m0.lf[i_f].u_f for i_f in li_fa0 if i_f is not None ])
                    if float(len(su_fa0 & su_fa0_)) / float(len(su_fa0)) >= 0.6: continue
                    #if len(su_fa0 & su_fa0_) >= 3: continue
                    changed = True
                    ci.dcf0( if0 )
            
                for if0,f0 in ci.elf0():
                    if if0 not in ci.mf01: continue
                    if len([ u_f for u_f in adj_f0[f0.u_f]['su_afe'] if m0._opt_get_i_f(u_f) in ci.mf01 ]) > 1: continue
                    changed = True
                    ci.dcf0(if0)
                for if1,f1 in ci.elf1():
                    if if1 not in ci.mf10: continue
                    if len([ u_f for u_f in adj_f1[f1.u_f]['su_afe'] if m1._opt_get_i_f(u_f) in ci.mf10 ]) > 1: continue
                    changed = True
                    ci.dcf1(if1)
            
            changed = True
            while changed:
                changed = False
                for i_r,ring_sf0 in enumerate(lring_sf0):
                    if align[i_r] is None: continue
                    ct,cc = len(ring_sf0),0
                    for u_f0 in ring_sf0:
                        i_f0 = m0._opt_get_i_f(u_f0)
                        if i_f0 in ci.mf01: cc += 1
                    if float(cc) / float(ct) >= 0.25: continue
                    for u_f0 in ring_sf0:
                       i_f0 = m0._opt_get_i_f(u_f0)
                       if i_f0 in ci.mf01:
                           ci.dcf0( i_f0 )
                    print( '    removing %i' % i_r )
                    align[i_r] = None
                    changed = True
                
                #for if0,f0 in ci.elf0():
                    #if if0 not in ci.mf01: continue
                    #if len([ u_f for u_f in adj_f0[f0.u_f]['su_f'] if m0._opt_get_i_f(u_f) in ci.mf01 ]) > 0: continue
                    #changed = True
                    #ci.dcf0(if0)
                #for if1,f1 in ci.elf1():
                    #if if1 not in ci.mf10: continue
                    #if len([ u_f for u_f in adj_f1[f1.u_f]['su_f'] if m1._opt_get_i_f(u_f) in ci.mf10 ]) > 0: continue
                    #changed = True
                    #ci.dcf1(if1)
        
        #if False:
            #print( '  computing cost matrix...' )
            #ngila = [None]*len1
            #for i in xrange(len0):
                #if align[i] is None: continue
                #ngila[align[i]] = i
            
            #map0_ = [ i for i,j in enumerate(align) if j is None ]
            #map1_ = [ j for j,i in enumerate(ngila) if i is None ]
            #len0_,len1_ = len(map0_),len(map1_)
            #llcv0_ = [ llcv0[map0_[i_]] for i_ in xrange(len0_) ]
            #llcv1_ = [ llcv1[map1_[j_]] for j_ in xrange(len1_) ]
            #mra0_ = [ [ mra0[i_][j_] for j_ in xrange(len0_) ] for i_ in xrange(len0_) ]
            #mra1_ = [ [ mra1[i_][j_] for j_ in xrange(len1_) ] for i_ in xrange(len1_) ]
            
            #matcosts = [[0.0]*(len0_+len1_) for i in xrange(len0_+len1_)]
            
            #for (i_,j_) in xyrange(len0_,len0_):                                                            # Q1
                #i = map0_[i_]
                #matcosts[i_][j_+len1_] = 15.0 * len(lring_sf0[i]) if i_ == j_ else float('inf')
            
            #for (i_,j_) in xyrange(len1_,len1_):                                                            # Q3
                #j = map1_[j_]
                #matcosts[i_+len0_][j_] = 15.0 * len(lring_sf1[j]) if i_ == j_ else float('inf')
            
            #t,pl = time.time(),-1.0
            #for i_,lcv0_ in enumerate(llcv0_):                                                             # Q2
                #i = map0_[i_]
                #pc = float(i_)/float(len0_)*100.0
                #if int(pl/5.0) != int(pc/5.0):
                    #td = time.time() - t
                    #print( '    %02i%% (%.2fs, eta %.2fs)...' % (pc,td,(td*100.0/pc-td) if pc > 0.0 else -1.0) )
                    #pl = pc
                ## get verts of ring
                #for j_,lcv1_ in enumerate(llcv1_):
                    #j = map1_[j_]
                    #d = abs(sum(mra0_[i_])-sum(mra1_[j_])) * 6.0
                    
                    #if abs(len(lcv0_)-len(lcv1_)) > 20 or d > 20.0:
                        #d = float('inf')
                    #else:
                        #d += compute_cost(
                            #len(lcv0_), len(lcv1_),
                            #lambda _i: 1.0,
                            #lambda _i,_j: Vec3f.t_distance2( lcv0_[_i], lcv1_[_j] ),      # TODO: incl star cost?
                            #lambda _j: 1.0
                            #) * 100.0
                    #matcosts[i_][j_] = d if d <= 500.0 else float('inf')
            
            #print( "  running..." )
            #t = time.time()
            #inds = hungarian( numpy.array( matcosts ) )
            #print( "    finished in %f" % (time.time()-t) )
            
            #for i_,j_ in enumerate(inds):
                #if i_ >= len0_ or j_ >= len1_: continue
                #i,j = map0_[i_],map1_[j_]
                #if align[i] is not None: continue
                #align[i] = j
            
            #ci.dcfall()
            #print( '  rebuilding face correspondences from strips...' )
            #for ir00,ring_sf00 in enumerate(lring_sf0):
                #ir10 = align[ir00]
                #if ir10 is None: continue
                #ring_sf10 = lring_sf1[ir10]
                #for ir01,ring_sf01 in enumerate(lring_sf0):
                    #if ir01 <= ir00: continue
                    #ir11 = align[ir01]
                    #if ir11 is None: continue
                    #ring_sf11 = lring_sf1[ir11]
                    
                    #luf0 = list(ring_sf00 & ring_sf01)
                    #if not luf0: continue
                    
                    #luf1 = list(ring_sf10 & ring_sf11)
                    #if not luf1: continue
                    
                    #lfc0 = [ Vertex.average( m0.opt_get_uf_lv( u_f ) ) for u_f in luf0 ]
                    #lfc1 = [ Vertex.average( m1.opt_get_uf_lv( u_f ) ) for u_f in luf1 ]
                    
                    #d = [ (i,j,Vec3f.t_distance2(fc0,fc1)) for i,fc0 in enumerate(lfc0) for j,fc1 in enumerate(lfc1) ]
                    #for i,j,_ in sorted( d, key=lambda x:x[2] ):
                        #if0,if1 = m0._opt_get_i_f(luf0[i]),m1._opt_get_i_f(luf1[j])
                        #if if0 in ci.mf01 or if1 in ci.mf10: continue
                        #ci.acf( if0, if1 )
            
            #print( '  removing overly conflicting correspondences...' )
            #changed = True
            #while changed:
                #changed = False
                #for if0,f0 in ci.elf0():
                    #if if0 not in ci.mf01: continue
                    #if1 = ci.mf01[if0]
                    #f1 = m1.lf[if1]
                    
                    #li_r0 = [ i_r0 for i_r0,ring_sf0 in enumerate(lring_sf0) if f0.u_f in ring_sf0 ]
                    #li_r1 = [ i_r1 for i_r1,ring_sf1 in enumerate(lring_sf1) if f1.u_f in ring_sf1 ]
                    #li_r1_ = [ align[i_r0] for i_r0 in li_r0 if align[i_r0] ]
                    #if len(set(li_r1) & set(li_r1_)) >= 1: continue
                    #changed = True
                    #ci.dcf0( if0 )
                    #print( 'removing if0 %i' % if0 )
                
                #for if0,f0 in ci.elf0():
                    #if if0 not in ci.mf01: continue
                    #if1 = ci.mf01[if0]
                    #f1 = m1.lf[if1]
                    
                    #su_fa0 = adj_f0[f0.u_f]['su_f']
                    #su_fa1 = adj_f1[f1.u_f]['su_f']
                    
                    #li_fa0 = [ m0._opt_get_i_f( u_f ) for u_f in su_fa0 ]
                    #li_fa1 = [ ci.mf01[i_f] if i_f in ci.mf01 else None for i_f in li_fa0 ]
                    
                    #su_fa1_ = set([ m1.lf[i_f].u_f for i_f in li_fa1 if i_f ])
                    ##if float(len(su_fa1 & su_fa1_)) / float(len(su_fa1)) >= 0.6: continue
                    #if len(su_fa1 & su_fa1_) >= 3: continue
                    #changed = True
                    #ci.dcf0( if0 )
                
                #for if1,f1 in ci.elf1():
                    #if if1 not in ci.mf10: continue
                    #if0 = ci.mf10[if1]
                    #f0 = m0.lf[if0]
                    
                    #su_fa0 = adj_f0[f0.u_f]['su_f']
                    #su_fa1 = adj_f1[f1.u_f]['su_f']
                    
                    #li_fa1 = [ m1._opt_get_i_f( u_f ) for u_f in su_fa1 ]
                    #li_fa0 = [ ci.mf10[i_f] if i_f in ci.mf10 else None for i_f in li_fa1 ]
                    
                    #su_fa0_ = set([ m0.lf[i_f].u_f for i_f in li_fa0 if i_f ])
                    ##if float(len(su_fa0 & su_fa0_)) / float(len(su_fa0)) >= 0.6: continue
                    #if len(su_fa0 & su_fa0_) >= 3: continue
                    #changed = True
                    #ci.dcf0( if0 )
            
            ##changed = True
            ##while changed:
                ##changed = False
                #for i_r,ring_sf0 in enumerate(lring_sf0):
                    #if align[i_r] is None: continue
                    #ct,cc = len(ring_sf0),0
                    #for u_f0 in ring_sf0:
                        #i_f0 = m0._opt_get_i_f(u_f0)
                        #if i_f0 in ci.mf01: cc += 1
                    #if float(cc) / float(ct) >= 0.55: continue
                    #for u_f0 in ring_sf0:
                       #i_f0 = m0._opt_get_i_f(u_f0)
                       #if i_f0 in ci.mf01:
                           #ci.dcf0( i_f0 )
                    #print( 'removing %i' % i_r )
                    #align[i_r] = None
                    #changed = True
                ##break
                
                #for if0,f0 in ci.elf0():
                    #if if0 not in ci.mf01: continue
                    #if len([ u_f for u_f in adj_f0[f0.u_f]['su_f'] if m0._opt_get_i_f(u_f) in ci.mf01 ]) > 0: continue
                    #changed = True
                    #ci.dcf0(if0)
                #for if1,f1 in ci.elf1():
                    #if if1 not in ci.mf10: continue
                    #if len([ u_f for u_f in adj_f1[f1.u_f]['su_f'] if m1._opt_get_i_f(u_f) in ci.mf10 ]) > 0: continue
                    #changed = True
                    #ci.dcf1(if1)
        
        if not any( align ):
            print( '  WARNING: No strip alignments' )
        
        ci.dcfall()
        print( '  rebuilding face correspondences from strips...' )
        for ir00,ring_sf00 in enumerate(lring_sf0):
            ir10 = align[ir00]
            if ir10 is None: continue
            ring_sf10 = lring_sf1[ir10]
            for ir01,ring_sf01 in enumerate(lring_sf0):
                #if ir01 <= ir00: continue
                ir11 = align[ir01]
                if ir11 is None: continue
                ring_sf11 = lring_sf1[ir11]
                
                luf0 = list(ring_sf00 & ring_sf01)
                if not luf0: continue
                
                luf1 = list(ring_sf10 & ring_sf11)
                if not luf1: continue
                
                lfc0 = [ Vertex.average( m0.opt_get_uf_lv( u_f ) ) for u_f in luf0 ]
                lfc1 = [ Vertex.average( m1.opt_get_uf_lv( u_f ) ) for u_f in luf1 ]
                
                d = [ (i,j,Vec3f.t_distance2(fc0,fc1)) for i,fc0 in enumerate(lfc0) for j,fc1 in enumerate(lfc1) ]
                for i,j,_ in sorted( d, key=lambda x:x[2] ):
                    if0,if1 = m0._opt_get_i_f(luf0[i]),m1._opt_get_i_f(luf1[j])
                    if if0 in ci.mf01 or if1 in ci.mf10: continue
                    ci.acf( if0, if1 )
        
        changed = True
        while changed:
            changed = False
            for if0,f0 in ci.elf0():
                if if0 not in ci.mf01: continue
                if1 = ci.mf01[if0]
                f1 = m1.lf[if1]
                
                li_r0 = [ i_r0 for i_r0,ring_sf0 in enumerate(lring_sf0) if f0.u_f in ring_sf0 ]
                li_r1 = [ i_r1 for i_r1,ring_sf1 in enumerate(lring_sf1) if f1.u_f in ring_sf1 ]
                li_r1_ = [ align[i_r0] for i_r0 in li_r0 if align[i_r0] ]
                if len(set(li_r1) & set(li_r1_)) >= 1: continue
                changed = True
                ci.dcf0( if0 )
                print( '    removing if0 %i' % if0 )
            
            for if0,f0 in ci.elf0():
                if if0 not in ci.mf01: continue
                if1 = ci.mf01[if0]
                f1 = m1.lf[if1]
                
                su_fa0 = adj_f0[f0.u_f]['su_afe']
                su_fa1 = adj_f1[f1.u_f]['su_afe']
                
                li_fa0 = [ m0._opt_get_i_f( u_f ) for u_f in su_fa0 ]
                li_fa1 = [ ci.mf01[i_f] if i_f in ci.mf01 else None for i_f in li_fa0 ]
                
                su_fa1_ = set([ m1.lf[i_f].u_f for i_f in li_fa1 if i_f is not None ])
                if float(len(su_fa1 & su_fa1_)) / float(len(su_fa1)) >= 0.6: continue
                #if len(su_fa1 & su_fa1_) >= 3: continue
                changed = True
                ci.dcf0( if0 )
            
            for if1,f1 in ci.elf1():
                if if1 not in ci.mf10: continue
                if0 = ci.mf10[if1]
                f0 = m0.lf[if0]
                
                su_fa0 = adj_f0[f0.u_f]['su_afe']
                su_fa1 = adj_f1[f1.u_f]['su_afe']
                
                li_fa1 = [ m1._opt_get_i_f( u_f ) for u_f in su_fa1 ]
                li_fa0 = [ ci.mf10[i_f] if i_f in ci.mf10 else None for i_f in li_fa1 ]
                
                su_fa0_ = set([ m0.lf[i_f].u_f for i_f in li_fa0 if i_f is not None ])
                if float(len(su_fa0 & su_fa0_)) / float(len(su_fa0)) >= 0.6: continue
                #if len(su_fa0 & su_fa0_) >= 3: continue
                changed = True
                ci.dcf0( if0 )
            for if0,f0 in ci.elf0():
                if if0 not in ci.mf01: continue
                if len([ u_f for u_f in adj_f0[f0.u_f]['su_afe'] if m0._opt_get_i_f(u_f) in ci.mf01 ]) > 1: continue
                changed = True
                ci.dcf0(if0)
            for if1,f1 in ci.elf1():
                if if1 not in ci.mf10: continue
                if len([ u_f for u_f in adj_f1[f1.u_f]['su_afe'] if m1._opt_get_i_f(u_f) in ci.mf10 ]) > 1: continue
                changed = True
                ci.dcf1(if1)
        
        if not any( align ):
            print( '  WARNING: No strip alignments' )
        
        print( '  using adjacency to find more face correspondences...' )
        changed = True
        runs = 0
        while changed or runs < 5: # and runs < 50:
            changed = False
            runs += 1
            
            for if0,f0 in ci.elf0():
                if if0 in ci.mf01: continue
                sufa0 = adj_f0[f0.u_f]['su_afe']
                lifa0 = [ m0._opt_get_i_f(u_f) for u_f in sufa0 if m0._opt_get_i_f(u_f) in ci.mf01 ]
                lifa1 = [ ci.mf01[i_f] for i_f in lifa0 ]
                #if not lifa1: continue
                if len(lifa1) < ( 2 if runs < 5 else 1 ): continue
                
                # make sure intersection of adj's adj faces is current
                su_f = set([ u_f for u_f in adj_f0[ci.lf0[lifa0[0]].u_f]['su_afe'] if m0._opt_get_i_f(u_f) not in ci.mf01 ])
                for i_f in lifa0:
                    su_f &= set([ u_f for u_f in adj_f0[ci.lf0[i_f].u_f]['su_afe'] if m0._opt_get_i_f(u_f) not in ci.mf01 ])
                if len(su_f) != 1 or su_f.pop() != f0.u_f: continue
                
                su_f = set([ u_f for u_f in adj_f1[ci.lf1[lifa1[0]].u_f]['su_afe'] if m1._opt_get_i_f(u_f) not in ci.mf10 ])
                for i_f in lifa1:
                    su_f &= set([ u_f for u_f in adj_f1[ci.lf1[i_f].u_f]['su_afe'] if m1._opt_get_i_f(u_f) not in ci.mf10 ])
                if len(su_f) != 1: continue
                
                if1 = m1._opt_get_i_f( su_f.pop() )
                if if1 in ci.mf10: continue
                
                ci.acf( if0, if1 )
                changed = True
            
            for if1,f1 in ci.elf1():
                if if1 in ci.mf10: continue
                sufa1 = adj_f1[f1.u_f]['su_afe']
                lifa1 = [ m1._opt_get_i_f(u_f) for u_f in sufa1 if m1._opt_get_i_f(u_f) in ci.mf10 ]
                lifa0 = [ ci.mf10[i_f] for i_f in lifa1 ]
                #if not lifa0: continue
                if len(lifa0) < ( 2 if runs < 5 else 1): continue
                
                # make sure intersection of adj's adj faces is current
                su_f = set([ u_f for u_f in adj_f1[ci.lf1[lifa1[0]].u_f]['su_afe'] if m1._opt_get_i_f(u_f) not in ci.mf10 ])
                for i_f in lifa1:
                    su_f &= set([ u_f for u_f in adj_f1[ci.lf1[i_f].u_f]['su_afe'] if m1._opt_get_i_f(u_f) not in ci.mf10 ])
                if len(su_f) != 1 or su_f.pop() != f1.u_f: continue
                
                su_f = set([ u_f for u_f in adj_f0[ci.lf0[lifa0[0]].u_f]['su_afe'] if m0._opt_get_i_f(u_f) not in ci.mf01 ])
                for i_f in lifa0:
                    su_f &= set([ u_f for u_f in adj_f0[ci.lf0[i_f].u_f]['su_afe'] if m0._opt_get_i_f(u_f) not in ci.mf01 ])
                if len(su_f) != 1: continue
                
                if0 = m0._opt_get_i_f( su_f.pop() )
                if if0 in ci.mf01: continue
                
                ci.acf( if0, if1 )
                changed = True
            
        if not any( align ):
            print( '  WARNING: No strip alignments' )
        
        return ci
    

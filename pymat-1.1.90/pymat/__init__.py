#! /usr/bin/python
'''pymat
pymat is a python module to communicate with Matlab.
'''

__revision__ = '$Id:'
__version__ = '1.1'

from pymat import *

# == Crutch for *indows
__all__ = ['plot', 'pymat']

if __name__ == '__main__':
    pass

"""
Simple plotting interface using MATLAB
"""

import pymat
import Numeric
import types

H = None

def openSession():
  global H
  
  if not H:
    H = pymat.open()

def closeSession():
  global H

  if H:
    pymat.close(H)
    H=None

def eval(str):
  openSession()

  pymat.eval(H, str)

def plot(x, y=None, *args):
  if not y:
    y=x
    x=Numeric.arrayrange(len(y))
  
  openSession()

  pymat.put(H, 'x__', x)
  pymat.put(H, 'y__', y)

  s = 'h__ = plot(x__,y__'
  for item in args:
    s = s + "," + repr(item)
  s = s + ')'

  pymat.eval(H, s)

  return pymat.get(H, 'h__')

def hold(onoff):
  openSession()

  if onoff:
    s = 'hold on'
  else:
    s = 'hold off'

  pymat.eval(H, s)

def subplot(rows,cols,index):
  openSession()

  s = 'subplot(%d,%d,%d)' % (rows,cols,index)
  pymat.eval(H, s)

def close():
  openSession()

  pymat.eval(H, 'close')

def newfigure():
  openSession()

  pymat.eval(H, 'figure')

def figurenum():
  openSession()

  pymat.eval(H, "h__=get(0,'CurrentFigure')")
  h = pymat.get(H, 'h__')
  if len(h):
    return h[0]
  else:
    return None

def title(s, *args):
  openSession()

  s = s % args
  pymat.eval(H, "title('%s')" % s)

def xlabel(s, *args):
  openSession()

  s = s % args
  pymat.eval(H, "xlabel('%s')" % s)

def ylabel(s, *args):
  openSession()

  s = s % args
  pymat.eval(H, "ylabel('%s')" % s)

def axis(val=None):
  openSession()

  if not val:
    pymat.eval(H, 'h__ = axis')
    return pymat.get(H, 'h__')

  if type(val) == types.StringType:
    pymat.eval(H,"axis('%s')" % val)
    return

  pymat.put(H, 'h__', val)
  pymat.eval(H, 'axis(h__)')
  return

def text(x, y, z, s=None):
  openSession()

  if not s:
    s = z
    z = None

  if z:
    pymat.eval(H, "h__ = text(%g,%g,%g,'%s')" % (x,y,z,s))
  else:
    pymat.eval(H, "h__ = text(%g,%g,'%s')" % (x,y,s))
  return pymat.get(H, 'h__')

def errorbar(X,Y,L=None,U=None):
  openSession()

  pymat.put(H, 'x__', X)
  pymat.put(H, 'y__', Y)
  if not L:
    pymat.eval(H, "h__ = errorbar(x__, y__)")
    return pymat.get(H, 'h__')
  else:
    pymat.put(H, 'l__', L)

  if not U:
    pymat.eval(H, "h__ = errorbar(x__, y__, l__)")
    return pymat.get(H, 'h__')
  else:
    pymat.put(H, 'u__', U)

  pymat.eval(H, "h__ = errorbar(x__, y__, l__, u__)")
  return pymat.get(H, 'h__')

def bar(X, Y=None, *args):
  return __doBar('bar', X, Y, args)

def barh(X, Y=None, *args):
  return __doBar('barh', X, Y, args)

def __doBar(barfunc, X, Y, args):
  openSession()

  pymat.put(H, 'x__', X)

  if not Y:
    pymat.eval(H, "h__ = %s(x__)" % barfunc)
    return pymat.get(H, 'h__')

  pymat.put(H, 'y__', Y)
  s = 'h__ = %s(x__, y__' % barfunc
  for arg in args:
    s = s + "," + repr(arg)
  s = s + ')'
  pymat.eval(H,s)

  return pymat.get(H, 'h__')

def patch(X, Y, Z, C=None):
  openSession()

  if not C:
    C=Z
    Z=None

  pymat.put(H, 'x__', X)
  pymat.put(H, 'y__', Y)
  patchstr = 'h__ = patch(x__,y__,'
  if Z:
    pymat.put(H, 'z__', Z)
    patchstr = patchstr+'z__,'

  if type(C)==types.StringType:
    patchstr = patchstr + ("'%c')" % C[0])
  else:
    pymat.put(H, 'c__', C)
    patchstr = patchstr + "c__)"

  pymat.eval(H, patchstr)

  return pymat.get(H, 'h__')

def hist(Y,X=None,**kwargs):
  openSession()

  doPlot = 0

  for k,v in kwargs.items():
    if k=='plot':
      doPlot = v
    else:
      raise RuntimeError, 'HIST: Unknown keyword argument'

  s = "[n__, w__] = hist(y__"
  pymat.put(H, 'y__', Y)
  if X:
    pymat.put(H, 'x__', X)
    s = s + ",x__"
  s = s + ")"

  pymat.eval(H, s)
  N = pymat.get(H, 'n__')
  W = pymat.get(H, 'w__')

  if doPlot:
    s = 'hist(y__'
    if X:
      s = s + ",x__"
    s = s + ")"
    pymat.eval(H,s)

  return N,W

def stem(X,Y=None,S=None):
  openSession()

  pymat.put(H, 'x__', X)
  s = "h__ = stem(x__"

  if Y:
    pymat.put(H, 'y__', Y)
    s = s + ",y__"

  if S:
    assert type(S)==types.StringType

    s = s + (",'%s'" % S)

  s = s + ')'
  pymat.eval(H, s)

  return pymat.get(H, 'h__')

def mesh(X,Y=None,Z=None,C=None):
  openSession()

  pymat.put(H, 'x__', X)
  s = "h__ = mesh(x__"

  if Y:
    pymat.put(H, 'y__', Y)
    s = s + ",y__"

  if Z:
    pymat.put(H, 'z__', Z)
    s = s + ",z__"

  if C:
    pymat.put(H, 'c__', C)
    s = s + ",c__"
    
  s = s + ')'
  pymat.eval(H, s)

  return pymat.get(H, 'h__')

def meshc(X,Y=None,Z=None,C=None):
  openSession()

  pymat.put(H, 'x__', X)
  s = "h__ = meshc(x__"

  if Y:
    pymat.put(H, 'y__', Y)
    s = s + ",y__"

  if Z:
    pymat.put(H, 'z__', Z)
    s = s + ",z__"

  if C:
    pymat.put(H, 'c__', C)
    s = s + ",c__"
    
  s = s + ')'
  pymat.eval(H, s)

  return pymat.get(H, 'h__')

def meshz(X,Y=None,Z=None,C=None):
  openSession()

  pymat.put(H, 'x__', X)
  s = "h__ = meshz(x__"

  if Y:
    pymat.put(H, 'y__', Y)
    s = s + ",y__"

  if Z:
    pymat.put(H, 'z__', Z)
    s = s + ",z__"

  if C:
    pymat.put(H, 'c__', C)
    s = s + ",c__"
    
  s = s + ')'
  pymat.eval(H, s)

  return pymat.get(H, 'h__')

def waterfall(X,Y=None,Z=None,C=None):
  openSession()

  pymat.put(H, 'x__', X)
  s = "h__ = waterfall(x__"

  if Y:
    pymat.put(H, 'y__', Y)
    s = s + ",y__"

  if Z:
    pymat.put(H, 'z__', Z)
    s = s + ",z__"

  if C:
    pymat.put(H, 'c__', C)
    s = s + ",c__"
    
  s = s + ')'
  pymat.eval(H, s)

  return pymat.get(H, 'h__')

def surf(X,Y=None,Z=None,C=None):
  openSession()

  pymat.put(H, 'x__', X)
  s = "h__ = surf(x__"

  if Y:
    pymat.put(H, 'y__', Y)
    s = s + ",y__"

  if Z:
    pymat.put(H, 'z__', Z)
    s = s + ",z__"

  if C:
    pymat.put(H, 'c__', C)
    s = s + ",c__"
    
  s = s + ')'
  pymat.eval(H, s)

  return pymat.get(H, 'h__')

def surfc(X,Y=None,Z=None,C=None):
  openSession()

  pymat.put(H, 'x__', X)
  s = "h__ = surfc(x__"

  if Y:
    pymat.put(H, 'y__', Y)
    s = s + ",y__"

  if Z:
    pymat.put(H, 'z__', Z)
    s = s + ",z__"

  if C:
    pymat.put(H, 'c__', C)
    s = s + ",c__"
    
  s = s + ')'
  pymat.eval(H, s)

  return pymat.get(H, 'h__')

def view(AZ=None,EL=None):
  openSession()

  if not EL is None:
    pymat.put(H, 'az__', [AZ])
    pymat.put(H, 'el__', [EL])
    pymat.eval(H, 'view(az__,el__)')
  elif not AZ is None:
    if type(AZ) == types.IntType:
      pymat.put(H, 'az__', [AZ])
    else:
      pymat.put(H, 'az__', AZ)
    pymat.eval(H, 'view(az__)')

  pymat.eval(H, '[az__,el__] = view')

  return (pymat.get(H, 'az__'), pymat.get(H, 'el__'))


#! /usr/bin/python
import sys
import os
from distutils.core import setup, Extension

##FIXME: try to put the python version in the release name

if sys.platform[:6] == 'linux2':
    matlab_dir ='/opt/matlab6.5'
    matlab_plat = 'glnx86'
    libraries = [
        'eng',
        'mat',
        'mx',
        'ut',
        'stdc++'
        ]
elif sys.platform[:5] == 'win32':
    matlab_dir ='c:\matlab6p5'
    matlab_plat = 'win32\microsoft\msvc60'
    libraries = [
        'libeng',
        'libmx',
        ]

## == Package information
name = 'pymat'
##execfile(os.path.join(name,'__init__.py'))
__version__ = '1.1.90'
version = __version__

description='A python module to communicate with Matlab.'
long_description = '''\
A python module to communicate with Matlab.
'''
author='Andrew Sterian'
author_email="steriana@claymore.engineer.gvsu.edu"
url="http://claymore.engineer.gvsu.edu/~steriana/Python/"
packages = [name]
license = 'see pymat.html'

include_dirs = [
    os.path.join(matlab_dir, 'extern', 'include'),
    ]
library_dirs = [
    os.path.join(matlab_dir, 'extern', 'lib', matlab_plat),
    ]


######################################################################
## BUILD: Fix bdist_rpm
import glob
from distutils.command.bdist_rpm import bdist_rpm
from distutils.core import DEBUG
from distutils.file_util import write_file
class fix_bdist_rpm(bdist_rpm):
    def run (self):
        '''Copied almost directly from the original run().
        The only real change is to convert rpm -> rpmbuild.'''

        if DEBUG:
            print "before _get_package_data():"
            print "vendor =", self.vendor
            print "packager =", self.packager
            print "doc_files =", self.doc_files
            print "changelog =", self.changelog

        # make directories
        if self.spec_only:
            spec_dir = self.dist_dir
            self.mkpath(spec_dir)
        else:
            rpm_dir = {}
            for d in ('SOURCES', 'SPECS', 'BUILD', 'RPMS', 'SRPMS'):
                rpm_dir[d] = os.path.join(self.rpm_base, d)
                self.mkpath(rpm_dir[d])
            spec_dir = rpm_dir['SPECS']

        # Spec file goes into 'dist_dir' if '--spec-only specified',
        # build/rpm.<plat> otherwise.
        spec_path = os.path.join(spec_dir,
                                 "%s.spec" % self.distribution.get_name())
        self.execute(write_file,
                     (spec_path,
                      self._make_spec_file()),
                     "writing '%s'" % spec_path)

        if self.spec_only: # stop if requested
            return

        # Make a source distribution and copy to SOURCES directory with
        # optional icon.
        sdist = self.reinitialize_command('sdist')
        if self.use_bzip2:
            sdist.formats = ['bztar']
        else:
            sdist.formats = ['gztar']
        self.run_command('sdist')

        source = sdist.get_archive_files()[0]
        source_dir = rpm_dir['SOURCES']
        self.copy_file(source, source_dir)

        if self.icon:
            if os.path.exists(self.icon):
                self.copy_file(self.icon, source_dir)
            else:
                raise DistutilsFileError, \
                      "icon file '%s' does not exist" % self.icon


        # build package
        self.announce('building RPMs')
        rpm_cmd = ['rpmbuild']
        if self.source_only: # what kind of RPMs?
            rpm_cmd.append('-bs')
        elif self.binary_only:
            rpm_cmd.append('-bb')
        else:
            rpm_cmd.append('-ba')
        if self.rpm3_mode:
            rpm_cmd.extend(['--define',
                             '_topdir %s/%s' % (os.getcwd(), self.rpm_base),])
        if not self.keep_temp:
            rpm_cmd.append('--clean')
        rpm_cmd.append(spec_path)
        self.spawn(rpm_cmd)

        # XXX this is a nasty hack -- we really should have a proper way to
        # find out the names of the RPM files created; also, this assumes
        # that RPM creates exactly one source and one binary RPM.
        if not self.dry_run:
            if not self.binary_only:
                srpms = glob.glob(os.path.join(rpm_dir['SRPMS'], "*.rpm"))
                assert len(srpms) == 1, \
                       "unexpected number of SRPM files found: %s" % srpms
                self.move_file(srpms[0], self.dist_dir)

            if not self.source_only:
                rpms = glob.glob(os.path.join(rpm_dir['RPMS'], "*/*.rpm"))
                assert len(rpms) == 1, \
                       "unexpected number of RPM files found: %s" % rpms
                self.move_file(rpms[0], self.dist_dir)

    # run()
## END: Fix bdist_rpm
######################################################################

## == Build package
setup(name = name,
      version = version,
      description = description,
      long_description = long_description,
      author = author,
      author_email = author_email,
      url = url,
      license = license,
      packages = packages,
      cmdclass = {'bdist_rpm': fix_bdist_rpm},
      ext_package = name,
      ext_modules = [Extension(name, [os.path.join('src', name+'.cpp')],
                               include_dirs = include_dirs,
                               library_dirs = library_dirs,
                               libraries = libraries)]
     )


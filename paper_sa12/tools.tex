% !TEX root =  meshgit.tex

\section{Diffing and Merging} \label{sect:tools}

In this section, we describe how to visualize the differences between meshes and how to merge two sets of differences into a single mesh.

\input{fig-diff-durano-series}

\subsection{Mesh Diff}

We visualize the mesh differences similarly to text diffs.  In order to provide as much context as possible, we display all versions of the mesh side-by-side with vertices and faces colored to indicated the type or magnitude of the differences.  We have experimented with many color schemes and report here the ones we found the most informative.

\paragraph{Two-way Diff.}
A two-way diff can illustrate the differences between two versions of a mesh.  For example, \fig{fig:diff_chairs} shows the computed differences between two chair meshes.
In a two-way diff, the original mesh $M$ is displayed on the left and the derivative mesh $M'$ on the right. Deleted faces in $M$ (unmatched or with mismatched adjacencies in $M$) are indicated by coloring them red.  Added faces in $M'$ (unmatched or with mismatched adjacencies in $M'$) are colored green.
In our visualizations, we simplify the presentation by not drawing the vertices directly.  Instead, the color of a vertex is linearly interpolated across the adjacent faces, unless the face has been colored red or green.  A vertex in $M'$ is colored blue if it has moved, where the saturation of blue indicates the strength of the geometric change with gray indicating no change.  Unmodified faces and vertices are colored gray.

\paragraph{Three-way Diff.}
When a mesh $M$ has two derived versions, $M^a$ and $M^b$, a three-way mesh diff can illustrate the changes between the derivatives and the original, allowing for a comparison of the two sets of edits.  The three meshes are shown side-by-side, with the original $M$ in the middle, $M^a$ on the left, and $M^b$ on the right. 
We use a similar color scheme as with a two-way diff, but the brightness of the color indicates from which derivative the operation comes: light red and green are for $M^a$, and dark red and green are for $M^b$.
When a face $f$ in $M$ has been modified in both derivatives, this overlap in change is indicated by coloring yellow $f$ in $M$.  An example of a three-way diff is shown in \fig{fig:teaser}.b.

\paragraph{Series Diff.}
An artist can also use \MeshGit{} to visualize the progression of work on a mesh, as shown in \fig{fig:diff_durano_series}. In this example, twelve snapshots of the model were saved during its construction, starting from the head, then working the body, and then adding the tail, arms, wings, and finally some extra details. Each snapshot is visualized similarly to a three-way diff.  For each snapshot, a face $f$ in $M$ is colored green if it was added, red if it is deleted, and orange if the face was added and then deleted. An alternative approach to visualizing mesh construction sequences is demonstrated in \emph{MeshFlow} \cite{meshflow}, that while providing a richer display, also requires full instrumentation of the modeling software.

\subsection{Mesh Edit Merge}

\paragraph{Merging Editing Operations.}
Given a mesh $M$ and two derivative meshes $M^a$ and $M^b$, one may wish to incorporate the changes made in both derivatives into a single resulting mesh. For example, in \fig{fig:teaser}.b, one derivative has finger nails added to the hand, while the other has refined and sculpted the palm. Presently, the only way to merge mesh edits such as this is for an artist to determine the changes done and then manually perform the merge of modifications by hand.

\paragraph{Merging Workflow.}
\MeshGit{} supports a merging workflow similar to text editing. We first compute two sets of mesh transformations in order to transform $M$ into $M^a$ and into $M^b$. If the two sets of transformations do not modify the same elements of the original mesh, \MeshGit{} merges them automatically by simply performing both sets of transformations on $M$. However, if the sets \emph{overlap} on $M$, then they are in conflict. In this case, it is unclear how to merge the changes automatically while respecting artists intentions. For this reason, we follow text editing workflows, and ask the user to either choose which set of operations to apply or to merge the conflict by hand.

\paragraph{Merging Non-Conflicting Edits.}
An example of our automatic merging is shown in \fig{fig:teaser}.b, where the changes do not overlap in the original mesh. In this case, \MeshGit{} merges the changes automatically. Another example is shown in \fig{fig:shaolin}. In one version the body is sculpted by moving vertices, while in the other the skirt is removed and the boots are replaced with sandals, thus also changing the face adjacencies. These two sets of differences do not affect the same elements on the original since sculpting affects only the geometric properties of the vertices. \MeshGit{} can safely merge these edits. The top subfigure of \fig{fig:shaolin} show the resulting merged mesh with colors indicating the applied transformations. On the right we show recursively applying Catmull-Clark subdivision rules twice to demonstrate that adjacencies are well maintained.

\input{fig-shaolin}

\paragraph{Reducing Conflicts.}
In our previous definition, if even a single mesh transformation is in conflict, none of the edits can be safely applied. We could ask the user to resolve the conflict by picking single transformations from each set, a situation that would be obviously too cumbersome. To reduce the number of conflicts and reduce the granularity of users' decisions, we partition mesh transformations into groups that can be safely applied individually. This is akin to grouping text characters into lines in text merging.

The key observation is that edits that cause adjacency changes can be bounded by the outer edges of the involved faces.  For example, an artist may inset a patch of faces on a mesh.  One way to represent this inset operation is to delete the patch from the original mesh and replace it with an inset version of the patch, connecting it to the rest of the mesh in exactly the same way the original patch was.  The vertices and edges surrounding the original patch remain present during the entire process.  We use this observation to guide our edit partitioning rule.  An edge between two vertices is on the boundary if the adjacent faces are unmatched (added or deleted) but the vertices of the edge are matched.  These boundary edges form a boundary loop.  We then partition the sets of editing operations to within these boundary loops, essentially grouping edits in small non-intersecting patches.  We can detect conflicts between the revisions at the granularity of these patches and ask users to resolve the conflicts at the same granularity.

Figure~\ref{fig:ufo} shows an example with a conflicting edit on a spaceship model. In one version, features are added to the spaceship's body and the base of the body has been enlarged. In the other, the cockpit exterior is detailed and wings are added to the base and top of the body. In this case, the extended base in the first version and the added lower wings in the second version are conflicting edits. \MeshGit{} successfully detected the conflicts to the body and merged all other changes automatically. To resolve the conflicts, the user can pick which version of edits to apply and use \MeshGit to properly apply the edits, as shown in the figure, or simply resolve the conflict manually.  The top three subfigures show three possible ways to resolve the conflicted merge.

\input{fig-diff-station}

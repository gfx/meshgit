% !TEX root =  meshgit.tex

\section{Algorithm}

\paragraph{Equivalent Graph Matching Problems.}
Minimizing the mesh edit distance to determine the optimal mesh matching is equivalent to solving a maximum common subgraph isomorphism problem on appropriately constructed graphs. Given a mesh, we define such a graph by first creating attributed nodes for each mesh element, where the attributes are the element's geometric properties. We then create an undirected edge between two nodes in the graph for each adjacency relation between pairs of elements in the mesh.  We can then determine a good mesh matching by minimizing \eqn{eqn:costfn} over the graph. Unfortunately, exact sub-graph matching is known to be NP-Complete in the general case.  And, while many polynomial-time graph-matching approximation algorithms have been proposed, we found that they do not work well in our problem domain, because they either ignore adjacency (i.e. edges in the graph), approximate the adjacencies too greatly, or do not scale to thousands of nodes.  In \MeshGit{}, we propose to compute an approximate mesh matching using an iterative greedy algorithm that minimizes the cost function \eqn{eqn:costfn}.

\subsection{Iterative Greedy Algorithm}

We initialize the matching $O$ with all elements in $M$ and $M'$ unmatched. This initial matching represents a transformation from $M$ to $M'$ where all elements of $M$ are removed and all elements of $M'$ are added. By construction, this is the highest cost matching.
The algorithm then iteratively executes a greedy step and a backtracking step. The greedy step minimizes the cost $C(O)$ of the matching $O$ by greedily assigning (or removing assignment) elements in $M$ to elements in $M'$.  The backtracking step removes matches that are likely to push the greedy algorithm into local minima of the cost function.
We iteratively repeat these two steps until the algorithm converges, where convergence is decided when the current matching is the same as a previous matching to within a $1-\epsilon$ of the total number of elements.  A small, non-zero value for $\epsilon$ allows for the detection of small oscillations in the matching of complex meshes, which is present in the meshes for Figs.~\ref{fig:diff_chairs} and \ref{fig:diff_creature}.  In our implementation, we set $\epsilon = 0.002$.
Figure~\ref{fig:iterdiff} illustrates how $O$ evolves for subsequent iterations.  Now we describe these two steps in more detail.

\input{fig-iterdiff}

\paragraph{Greedy Step.}

The greedy step updates the matching $O$ by consecutively assigning unmatched elements or removing the assignment of matched ones.  We greedily choose the change that reduces the cost $C(O)$ the most, and we remain in the greedy step until no change is found that is cheaper to perform than keeping the current matching.
%Given the current matching $O$, we proceed iteratively by attempting to perform all matching changes and choosing, at each iteration, the change that reduces the cost $C(O)$ the most. We stop when no change is found that is cheaper to perform than keeping the current matching.
Notice that this may leave some elements unmatched. In practice we found that the greedy step proceeds by growing patches. This is due to the adjacency term that favors assigning vertices and faces that are adjacent to already matched ones.

\input{fig-compare.tex}

\paragraph{Backtracking Step.}
While we found that in many cases the greedy step alone works well, we encountered a few instances where the algorithm gets stuck in a local minimum, as shown in \fig{fig:iterdiff}, caused by the order in which the greedy step grows patches. The geometric term favors assigning nearby elements.  However, if part of the mesh has been sculpted, the geometric term might favor greedy element assignments that incur small adjacency costs locally, but large overall adjacency costs as more elements are later added to the matching. This behavior is not due to the mesh edit distance we introduced, but to the greedy nature of the above algorithm. In our case though, we found that there are two common cases that are responsible for the vast majority of high cost matchings, and that we can easily prevent them by backtracking.

The first case is one of faces that are matched together with their vertices, but where the vertex order is different in the two meshes. For example, suppose that a face $f$, defined by vertices $(a,b,c,d)$, matches a face $f'$, defined by vertices $(a',b',c',d')$, where $a$ matches $a'$, $b$ to $b'$, $c$ to $d'$, and $d$ to $c'$.  In this example, the matching incurs heavy adjacency costs with adjacent faces over the $(b',c')$ and $(c',a')$ edges.  We can easily eliminate these cases by removing assignments of all vertices and faces involved.

The second case is when a group of connected faces that have been matched meets the rest of the mesh over mismatched adjacencies. An example of this behavior is shown in the \emph{greedy step only} subfigure of \fig{fig:iterdiff}, where there are small groups of matched faces, such as on the earlobe, that are disconnected from the rest of the mesh due to mismatched adjacencies.  These disconnected groups are due to suboptimal initial greedy assignments, favored by the geometric term, in heavily sculpted meshes that also have edits that affect adjacencies.  These groups of faces are always quite small relative to the size of the whole connected component upon which they reside. We eliminate small patches of disconnected faces by removing the assignments of all their elements.  We choose to backtrack in case the group's size is less than 10\% the size of the connected component.

In our backtracking step, we detect and remove assignments of elements as described in both previous cases. Since performing these two adjustments might leave assignments of high costs, we also remove assigment of elements that have mismatched adjacencies and vertices that have unmatched adjacencies. We found that if we run the greedy algorithm after this backtracking step, we obtain improved matchings in the case of heavily sculpted meshes with adjacency changes. We run both steps in an iterative fashion.

\input{fig-diff-chairs.tex}

\paragraph{Time Complexity.}
The cost of our algorithm is dominated by the iterative search for the minimum cost operations in the greedy step. Since we perform $O(n)$ assignments, each of which considers $O(n)$ possible cases, a naive implementation of the greedy step would run in $O(n^2)$ time. Given the geometric terms for vertices and faces in the cost function, we can prune the search space considerably.  In our implementation, we only consider the $k$ nearest neighbors for each unmatched vertex or face ($k=50$ in our tests). This reduces the computation time considerably. Furthermore, we compute the change in the cost function with local updates only, since assigning or removing the assignment of a pair of elements only affects the costs in their local neighborhoods. With these two optimizations, our algorithm has a time complexity of $O(n \log n)$, resulting in a practical solution that we found to perform well even with large meshes of several thousand vertices and faces.

The iterative greedy algorithm repeats until the algorithm converges.  For the majority of meshes, the algorithm converged within only a couple of iterations.  For meshes with complex edits (\eg{} Figs.~\ref{fig:iterdiff}, \ref{fig:diff_chairs}, and \ref{fig:diff_creature}), the algorithm requires more iterations.  Despite the higher number of iterations in these cases, we found the iterative greedy algorithm is a practical solution to approximate the mesh edit distance and produce intuitive visualizations of mesh differences for all the meshes we tested, as seen in \tbl{tbl:modelstats}.




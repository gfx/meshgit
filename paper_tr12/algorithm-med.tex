% !TEX root =  meshgit.tex

\subsection{Mesh Edit Distance}

Given two versions of a mesh $M$ and $M'$, we want to determine which elements of one corresponds to  which elements in the other. In our metric, we consider vertices and faces as the mesh elements. An element $e$ of $M$ is matched if it has a corresponding one $e'$ in $M'$, while it is unmatched otherwise. A mesh matching is the set of correspondences between all elements in $M$ to the elements in $M'$. The matching is bidirectional and, in general, partial, in that some elements will be unmatched, corresponding to addition and deletion of elements during editing. To choose between the many possible matching, we define a cost function, the \emph{mesh edit distance}, and pick the matching with minimum cost. The mesh edit distance is comprised of three terms.

\paragraph{Unmatched Cost.}
We penalize unmatched elements, either vertices or faces, by adding a constant cost $\alpha$ for each one. Without this cost, one could simply consider all elements of $M$ as deleted and all elements of $M'$ as added.

\paragraph{Geometric Cost.}
Matched elements incur two costs. The first captures changes in the geometry of each element, namely its position and normal. In our initial implementation, we consider meshes with attributes, where vertex positions and face normals are given, vertex normals are the average normals of the adjacent faces, and face positions are the average position of adjacent vertices. The geometric cost of matching $e$ to $e'$ is given by \vspace{-0.15in}

\begin{equation}
	C(\opc{e}{e'}) = \frac{||\mathbf{x}_e - \mathbf{x}_{e'}||}{||\mathbf{x}_e - \mathbf{x}_{e'}|| + 1} + (1-\mathbf{n}_e \cdot \mathbf{n}_{e'}),
	\label{eqn:deltafn}
\end{equation}\vspace{-0.05in}

where $\mathbf{x}_e$ and $\mathbf{n}_e$ are the position and normal of the element $e$ respectively\footnote{Note that our implementation assumes that vertices are defined with respect to the same coordinate system during editing, since this is common practice in mesh modeling and since most modeling software stores transformation matrices separately.  If necessary, we could run an initial global alignment based on ICP \cite{gicp}.  Furthermore, we normalize the size of both meshes to the average edge length so that vertex distances are normalized to the size of the mesh.}.

The position term is an increasing, limited function on the Euclidean distance between the elements positions. This formulation favors matching elements of $M$ to close-by elements in $M'$ and has no cost for matching co-located elements. We limit the position term to allow for the matching of distant elements, albeit at a penalty. We also include an orientation term computed as the dot product between the elements' normals. The inclusion of the orientation helps in cases where many small elements are located close to one another.

\paragraph{Adjacency Cost.}
The geometric costs alone are not sufficient to produce intuitive visual differences since it does not take into account changes in the elements adjacencies. For example, \fig{fig:med} shows two ways to match two meshes.  These two matchings differ only by which faces are matched.  In one case, the adjacency of the first mesh is maintained, while in the other it is not.  In this example, the geometric cost would not distinguish between these two cases. The top-right subfigure in \fig{fig:compare}, discussed in the following section, shows a more complex example of the benefit of explicitly including element adjacencies.

\input{fig-med}

We assign adjacency costs to pairs of adjacent elements $(e_1,e_2)$ in $M$ and in $M'$. We consider all adjacencies of faces and vertices (\ie{} face-to-face, face-to-vertex, and vertex-to-vertex). If either $e_1$ or $e_2$ are unmatched, the adjacency cost is a constant $\beta$ divided by the size of the local neighborhoods $A(e_1,e_2)$ around the pair. The latter is computed as the sum of the number of elements adjacent to $e_1$ and $e_2$. If both $e_1$ and $e_2$ are matched, but their matching elements are not adjacent, the adjacency cost for the pair is $\gamma$ divided by size of the local neighborhoods.
There is no cost for matched adjacencies.
We normalize the constants by the size of the local neighborhoods, so the maximum cost for elements with a large number of adjacencies (such as extraordinary vertices or poles) is the same as those with only a few adjacencies (such as vertices at the edges of the model).

\paragraph{Overall Cost.}
With the costs defined above, the overall cost of a matching $O$ between meshes $M$ and $M'$ can be as the sum of the unmatched, geometric and adjacency costs, as follows \vspace{-0.15in}

\begin{align}
	C(O) & = \alpha(N_u + N'_u) + \nonumber\\
	     & + \sum_{\{\opc{e}{e'}\}} \biggl[ \frac{||\mathbf{x}_e - \mathbf{x}_{e'}||}{||\mathbf{x}_e - \mathbf{x}_{e'}||+1} + (1-\mathbf{n}_e \cdot \mathbf{n}_{e'}) \biggr] + \nonumber\\
	     & + \sum_{(e_1,e_2) \in P_u} \frac{\beta}{A(e_1,e_2)} + \sum_{(e_1,e_2) \in P_m} \frac{\gamma}{A(e_1,e_2)}
	\label{eqn:costfn}
\end{align}

where $N_u$ and $N'_u$ are the number of unmatched elements in $M$ and $M'$ respectively, $\{\opc{e}{e'}\}$ is the set of matched elements, and $P_u$ and $P_m$ are the sets of adjacent element pairs in $M$ and $M'$ that are either unmatched or have different adjacency respectively. All results in this paper are obtained with $\alpha = 2.5$, $\beta = 1.0$, and $\gamma = 3.5$.  We determined these constants by matching a large variety of meshes and choosing the values that produce the most informative visualizations.

\ignorethis{
Furthermore, in \fig{fig:compare}, the bipartite graph matching algorithm computes a matching using only the $\Delta$ function which causes changes in adjacency with some of the faces around the nostril, across the bridge of nose, and along the lower jaw.
These adjacency changes produce visualizations that do not capture an intuitive matching between the two meshes.
In particular, all of the faces in the areas mentioned could be matched without any changes to their adjacencies.  However, the geometric properties changed enough that the algorithm matched closeby elements that considerably changes the adjacencies of these faces.
We found that by adding an adjacency cost, we obtain mesh matches that capture better the structure of the mesh changes.
}

\ignorethis{
\begin{align}
	C(O)
		& = \alpha(n_v^u + n_f^u) \hspace{0.05in}+ \hspace{-0.05in}\sum_{(u,v) \in U} \frac{\beta}{\Lambda(u,v)} \hspace{0.05in}+ \hspace{-0.05in}\sum_{(u,v) \in M} \frac{\gamma}{\Lambda(u,v)} + \notag \\
		& + \hspace{-0.05in}\sum_{\{\opc{n}{n'}\}} \biggl[ \frac{||\mathbf{x}_n - \mathbf{x}_{n'}||}{||\mathbf{x}_n - \mathbf{x}_{n'}||+1} + (1-\mathbf{n}_n \cdot \mathbf{n}_{n'}) \biggr]
	\label{eqn:costfn}
\end{align}
}


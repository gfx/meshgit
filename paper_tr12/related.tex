% !TEX root =  meshgit.tex

\section{Related Work}

\paragraph{Revision Control.}
Recent work by \cite{dobos12} proposes an approach to revision control for 3D assets by explicitly splitting an asset into components.  The edits of two different artists can be merged automatically when the edits do not affect the same component, while they need to be manually resolved otherwise. This effectively sets the granularity of supported mesh transformations to the individual components.  
As a practical demonstration, the open source movie Sintel \cite{model:sintel} was produced using similar techniques. The assets of the film, such as the characters, props, and sets, are stored in separate files and linked together in scene files.  With the files stored in an Subversion repository, the team of artists are able to easily share edits to individual files.
While these types of systems allow for merging changes of different components or files, any change to the same component, regardless of the location of the edit on the component, marks the entire component in conflict.
\MeshGit{} supports arbitrary edits on meshes without explicitly splitting them in components, allowing the merging of changes onto the same mesh (see \fig{fig:teaser}).

\paragraph{Shape Registration.}
A visual difference between two meshes could also be obtained by performing a partial shape registration of the meshes, and then converting that registration to a set of mesh transformations. Various mesh registration algorithms exist, as reviewed recently in \cite{shaperegtut}. Some of these methods \cite{icpcuts,gicp} are variants of iterative closest point \cite{icp} that determine piece-wise rigid transformations for different mesh regions and blend between them. In the case of heavily sculpted meshes, these algorithms require too many cuts and transformations to register the shapes. Others  use spectral methods \cite{spectral,sharma10} to determine a sparse correspondence between two shapes. \cite{sharma11b} uses heat diffusion as descriptors to overcome topological changes with seed-growing and EM stages to build a dense set of correspondences. Typically, these algorithms work by subsampling the mesh geometry since their computational complexity is too high. \cite{zeng10} propose a hierarchical method to performing dense surface registration by first matching sparse features then building dense correspondences using the sparse features to constrain the search space.
\cite{kim11} propose using a weighted combination of intrinsic maps to estimate correspondence between two meshes.
In general, we find that partial shape registration algorithms perform very well for finely tessellated meshes where matching accuracy of mesh adjacencies is not of paramount importance. When applied to our application though, these algorithms either do not scale very well, require the estimation of too many parameters, or are not sensitive enough to adjacency changes to produce precise and meaningful differences for the meshes typically used in subdivision modeling.
%Furthermore, it remains unclear how to robustly convert the partial matches of surfaces to partial matches of mesh vertices and faces for merging.
Furthermore, it remains unclear whether converting these partial matches to transformations is robust for merging.
\MeshGit{} formalizes the problem directly by turning mesh matching solutions into mesh transformations that are easy to visualize and robust to merge. 

\paragraph{Topology Matching.}
\cite{eppstein09} propose an algorithm to match quadrilateral meshes that have been edited by using a matching of unique extraordinary vertices as a seed for a matching-propagation algorithm.  Because the proposed algorithm does not take geometry into account, it is robust to posing and sculpting edits.  Furthermore, coupled with an initial mesh-reducing technique, the proposed algorithm can solve the topological matching very quickly.  However, when applied to the types of edits of the meshes in this paper, we found that the algorithm did not produce an intuitive matching.  The limitations of topology matching is due to ignoring the geometry of the mesh.  \MeshGit{} strikes a balance between geometry and topology to produce intuitive results.

%eg 2010 course provides a great overview: %http://www.mpi-inf.mpg.de/resources/deformableShapeMatching/EG2010_Tutorial/
%quadratic programming, spectral approximation methods
%eigendecomposition
%icp+graph cuts

%features of some:
%fast, handling thousands of verts at interactive rates
%handle matching articulated meshes and missing data
%match a template to input data

%limitations
%produces “shotgun” alignment when topology is not considered
%eigenvectors can change with some mesh edits
%sculpting is difficult / impossible to define with only a small set of affine transformations

%Computing the mesh edit distance may be approximately derived by partial shape registration, where missing data in the registration is interpreted as added and deleted geometry in our metric. \cite{chang08} propose an iterative closest point (ICP) with graph cuts to register articulated meshes.  Their algorithm is robust to missing data and scales well with mesh complexity.  However, they're method fails when the geometry is modified strongly in a non-rigid way, since ICP is the underlying method, requiring a large number of cuts for arbitrarily sculpted meshes such as \fig{fig:shaolin}.

%\cite{dubrovina10} propose using the eigendecomposition of the Laplace-Beltrami operator to perform shape registration.  As the authors note, this method fundamentally does not handle strong topological or geodesic changes, such as by partitioning or joining components or by adding an appendage (partial matching), as these types of edits can change the eigendecomposition of the Laplace-Beltrami operator. These types of changes are shown in \fig{fig:durano}.  Furthermore, as the core solver of their system in integer quadrant program, the complexity of the proposed framework is exponential in the number of possible features inteded to correspond, only scaling well by subsampling the vertices or using other optimization techniques.
%\fabio{exponential?} \jon{check paper again.  this bit is at the very bottom}

\paragraph{Graph Edit Distance.}%
By describing a polygonal mesh as a properly defined attributed graph, we can reformulate the problem of determining the changes needed to turn one mesh into another as the problem of turning one graph into another, which is know as the graph edit distance \cite{ged}.  The graph edit distance is defined as the minimum cost of changing one graph into another, where each change carries a cost.   In \cite{bunke98}, computing the graph edit distance with a special cost function is shown to be equivalent to the maximum common subgraph-isomorphism problem, the problem of finding the largest subgraph of one graph that is isomorphic to a subgraph of another.  While computing the maximum common subgraph is known to be NP-hard, \cite{riesen09} shows that good approximations can be computed using polynomial time algorithms to solve the corresponding graph matching problem.

Several approximation algorithms have been proposed that differ in the expected properties of the input graph. We refer the reader to \cite{ged,gedsurvey} for a recent review. We have experimented with a few of these methods, and found that they do not work well in our problem domain. For example, \cite{riesen09} propose to approximate the distance computation as a bipartite graph matching problem. In doing so, they approximate heavily the adjacency costs, which we found to be problematic. \cite{cour06} and \cite{gedspectral} propose methods based on spectral matching, but we found them to scale poorly with model size
and to be generally problematic when the graph spectrum changes.  In summary, we found that the current approximation algorithms for computing the graph edit distance either scale poorly with the size of the input meshes or produce poor results since they approximate too heavily the adjacency costs.  \MeshGit{} introduces an iterative greedy algorithm that takes into account mesh adjacencies well.

\paragraph{Assembly-Based Modeling.}
\cite{snappaste} allows users to create derivative meshes by smoothy blending separate mesh components either created specifically or found automatically by mesh segmentation. Recently, \cite{datadriven10} and \cite{probabilistic11} demonstrate the feasibility of constructing 3D models from a large dictionary of model parts. These methods work by remeshing components together, so they inherently do not respect face adjacency in the merged regions. This works well for highly tessellated meshes, but not for meshes typically used in subvision surface modeling where we want to maintain precisely the mesh topology designed by artists.

\paragraph{Instrumenting Software.}
An alternative approach to provide diff and merge is to consider full software instrumentation to extract the editing operations.  \cite{vistrails} let the users explore their undos histories. \cite{meshflow} shows rich visual histories of mesh construction by highlighting and visually annotating changes to the mesh. \cite{revg} demonstrates non-linear image editing, including merging. All these approaches record and take advantage of the exact editing operations an artist is performing. These are semantically richer than the simpler editing operation that \MeshGit{} recovers automatically. At the same time, these methods have the burden of a software instrumentation that is not available in today's software and would not allow artists to work with different softwares on the same meshes.
Furthermore, despite having the construction history, it is unclear how to determine a difference between two similar meshes that were constructed independently or where there is no clear common original, such as the meshes in \fig{fig:iterdiff}.%  \MeshGit{} is able to handle such a case.

% http://eclecti.cc/computergraphics/thingidiff-visualizing-3d-model-diffs-with-thingiview-js
% http://www.thingiverse.com/thing:12656
% https://github.com/nrpatel/thingiview.js
% http://n0r.org/thingiview.js/examples/client_side_diff.html#

% http://stackoverflow.com/questions/7863165/diffable-3d-modeling-format

% http://code.google.com/p/artdiff/

% image diff
% https://github.com/blog/817-behold-image-view-modes



